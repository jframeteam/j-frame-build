<?php
/**
 * Form Framework Concept
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */


/** Forms Plugin Class */
class Forms {

    /**
     * @var $Core Core
     */
    private $Core;
    private $formBuildConfig;
    private $formBuild = null;

    /**
     * Forms constructor.
     * @param $Core Core
     */
    function __construct($Core)
    {
        $this->Core = $Core;
    }


    /** Get Methods */

    public function getForm() {
        $formBuild = $this->formBuild;
        $formBuildConfig = $this->formBuildConfig;

        if($formBuild === null) { return false; }


        $formConfig       = (array_key_exists('config',$formBuildConfig) && is_array($formBuildConfig['config'])) ? $formBuildConfig['config'] : array();

        $action           = (array_key_exists('action',   $formConfig) && $formConfig['action' ] !== '')    ? $formConfig['action']                   : '';
        $method           = (array_key_exists('method',   $formConfig) && $formConfig['method']  !== '')    ? $formConfig['method']                   : 'post';
        $enctypeAttribute = ($method == 'post' && (array_key_exists('multipart',$formConfig) && $formConfig['multipart'] === true)) ? ' enctype="multipart/form-data"' : '';
        $idAttribute      = (array_key_exists('id',       $formConfig) && $formConfig['id']      !== '')    ? ' id="' . $formConfig['id'] . '"'       : '';
        $titleAttribute   = (array_key_exists('title',    $formConfig) && $formConfig['title']   !== '')    ? ' title="' . $formConfig['title'] . '"' : '';
        $classes          = (array_key_exists('classes',  $formConfig) && $formConfig['classes'] !== '')    ? ' ' . $formConfig['classes']            : '';

        $tabbed           = (array_key_exists('tabbed',   $formConfig) && is_bool($formConfig['tabbed']))   ? $formConfig['tabbed']                   : false;
        $startHtml        = (array_key_exists('startHtml',$formConfig) && $formConfig['startHtml'] !== '')  ? $formConfig['startHtml']                : '';
        $endHtml          = (array_key_exists('endHtml',  $formConfig) && $formConfig['endHtml']   !== '')  ? $formConfig['endHtml']                  : '';

        $formHtml = '';
        $formHtml .= '<form action="' . $action . '" method="' . $method . '"' . $enctypeAttribute . $idAttribute . $titleAttribute . ' class="form' . $classes . '">';

        $formHtml .= $startHtml;

        if($tabbed) {

        }

        foreach($formBuild['groups'] as $groupHtml) {
            $formHtml .= $groupHtml;
        }

        $formHtml .= $endHtml;

        $formHtml .= '</form>';

        return $formHtml;
    }

    public function getGroups($groupCode = null){
        $return = false;
        $formBuild = $this->formBuild;
        if($formBuild !== null) {
            $getGroups = $formBuild['groups'];

            $return = ($groupCode !== null && array_key_exists($groupCode,$getGroups)) ? $getGroups[$getGroups] : $getGroups;
        }
        return $return;
    }

    public function getFields($fieldCode = null){
        $return = false;
        $formBuild = $this->formBuild;
        if($formBuild !== null) {
            $getFields = $formBuild['fields'];

            $return = ($fieldCode !== null && array_key_exists($fieldCode,$getFields)) ? $getFields[$fieldCode] : $getFields;
        }
        return $return;
    }

    /** Build Methods */

    public function build($formBuildConfig) {

        if(is_array($formBuildConfig) && count($formBuildConfig)) {

            $this->formBuildConfig = $formBuildConfig;

            $formConfig = $formBuildConfig['config'];
            $formGroups = $formBuildConfig['groups'];
            $formFields = $formBuildConfig['fields'];

            $formBuild = array();
            foreach($formGroups as $group) {
                $groupCode = (array_key_exists('code',$group) && $group['code'] !== '') ? $group['code'] : '';

                if($groupCode === '') { contine; }

                $idAttribute      = (array_key_exists('id',     $group) && $group['id']      !== '') ? ' id="' . $group['id'] . '"'               : '';
                $classesAttribute = (array_key_exists('classes',$group) && $group['classes'] !== '') ? ' class="' . $group['classes'] . '"'       : '';
                $legendTag        = (array_key_exists('title',  $group) && $group['title']   !== '') ? '<legend>' . $group['title'] . '</legend>' : '';
                $tabbed           = (array_key_exists('tabbed', $formConfig) && is_bool($formConfig['tabbed'])) ? $formConfig['tabbed']           : false;

                $groupHtml = '';

                if($tabbed) {

                }

                $groupHtml .= '<fieldset' . $idAttribute . $classesAttribute . ' data-groupcode="' . $groupCode  . '">' . $legendTag;

                foreach($formFields as $fieldConfig) {
                    $fieldCode = (array_key_exists('code',$fieldConfig) && $fieldConfig['code'] !== '') ? $fieldConfig['code'] : '';
                    $fieldGroupCode = (array_key_exists('group',$fieldConfig) && $fieldConfig['group'] !== '') ? $fieldConfig['group'] : '';

                    if($fieldCode === '') { contine; }

                    if($fieldGroupCode == $groupCode) {

                        $fieldHtml = $this->buildField($fieldConfig);

                        $formBuild['fields'][$fieldCode] = $fieldHtml;

                        $groupHtml .= $fieldHtml;

                    }
                }

                $groupHtml .= '</fieldset>';

                $formBuild['groups'][$groupCode] = $groupHtml;
            }


            $this->formBuild = $formBuild;

        }

        return $this->getForm();
    }

    public function buildField($fieldConfig) {
        $buildField = '';
        if(is_array($fieldConfig) && count($fieldConfig)) {

            $fieldHtml = '';
            $fieldHtml .= '<div class="form-group" data-fieldcode="' . $fieldConfig['code'] . '">';

            $fieldElementConfig = (array_key_exists('field',$fieldConfig)        && is_array($fieldConfig['field']))    ? $fieldConfig['field']       : array();
            $fieldId            = (array_key_exists('id',   $fieldElementConfig) && $fieldElementConfig['id']   !== '') ? $fieldElementConfig['id']   : '';
            $fieldType          = (array_key_exists('type', $fieldElementConfig) && $fieldElementConfig['type'] !== '') ? $fieldElementConfig['type'] : '';

            $labelConfig = (array_key_exists('label',$fieldConfig) && $fieldConfig['label'] !== false) ? $fieldConfig['label'] : array();

            $fieldBuildedHtml = '';

            $labelHtml = '';
            if(count($labelConfig)) {
                $idAttribute      = (array_key_exists('id',$labelConfig) && $labelConfig['id'] !== '') ? ' id="' . $labelConfig['id'] . '"' : '';
                $classesAttribute = (array_key_exists('classes',$labelConfig) && $labelConfig['classes'] !== '') ? ' class="' . $labelConfig['classes'] . '"' : '';
                $forAttribute     = ($fieldId !== '') ? ' for="' . $fieldId . '"' : '';
                $title            = (array_key_exists('title',$labelConfig) && $labelConfig['title'] !== '') ? $labelConfig['title'] : '';
                $titleAttribute   = ($title !== '') ? ' title="' . $title . '"' : '';


                $labelHtml .= '<label' . $forAttribute . $idAttribute . $classesAttribute . $titleAttribute . '>' . $title . '</label>';
            }

            $fieldBuildedHtml .= $labelHtml;

            $elementBuildedHtml = $this->buildElement($fieldElementConfig);

            $fieldBuildedHtml .= $elementBuildedHtml;

            $wrapper = (array_key_exists('wrapper',$fieldConfig) && is_array($fieldConfig['wrapper']) && count($fieldConfig['wrapper'])) ? $fieldConfig['wrapper'] : array();
            if(count($wrapper)) {
                $tag = (array_key_exists('tag',$wrapper) && $wrapper['tag'] !== '') ? $wrapper['tag'] : 'div';

                $idAttribute      = (array_key_exists('id',$wrapper) && $wrapper['id'] !== '') ? ' id="' . $wrapper['id'] . '"' : '';
                $classesAttribute = (array_key_exists('classes',$wrapper) && $wrapper['classes'] !== '') ? ' class="' . $wrapper['classes'] . '"' : '';

                $fieldHtml .= '<' . $tag . $idAttribute. $classesAttribute . '>';
                $fieldHtml .= $fieldBuildedHtml;
                $fieldHtml .= '</' . $tag . '>';
            } else {
                $fieldHtml .= $fieldBuildedHtml;
            }

            $fieldHtml .= '</div>';

            $buildField = $fieldHtml;
        }

        return $buildField;
    }

    public function buildElement($fieldElementConfig) {
        $fieldType = (array_key_exists('type', $fieldElementConfig) && $fieldElementConfig['type'] !== '') ? $fieldElementConfig['type'] : '';

        $elementBuildedHtml = '';

        switch ($fieldType) {
            case 'textarea':
                $elementBuildedHtml .= $this->buildTextareaElement($fieldElementConfig);
                break;
            case 'select':
                $elementBuildedHtml .= $this->buildSelectElement($fieldElementConfig);
                break;
            case 'checkbox':
                $elementBuildedHtml .= $this->buildCheckboxElement($fieldElementConfig);
                break;
            case 'radio':
                $elementBuildedHtml .= $this->buildRadioElement($fieldElementConfig);
                break;
            default:
                $elementBuildedHtml .= $this->buildInputElement($fieldElementConfig);
        }

        return $elementBuildedHtml;
    }

    public function buildInputElement($fieldElementConfig) {
        $elementHtml = '';
        if(is_array($fieldElementConfig) && count($fieldElementConfig)) {

            $idAttribute       = (array_key_exists('id',$fieldElementConfig) && $fieldElementConfig['id'] !== '') ? ' id="' . $fieldElementConfig['id'] . '"' : '';
            $requiredAttribute = (array_key_exists('required',$fieldElementConfig) && $fieldElementConfig['required'] !== false) ? ' required="required"' : '';

            $type          = (array_key_exists('type',         $fieldElementConfig) && $fieldElementConfig['type']        !== '')      ? $fieldElementConfig['type']          : 'text';
            $default       = (array_key_exists('default',      $fieldElementConfig) && $fieldElementConfig['default']     !== '')      ? $fieldElementConfig['default']       : '';
            $value         = (array_key_exists('value',        $fieldElementConfig) && $fieldElementConfig['value']       !== '')      ? $fieldElementConfig['value']         : $default;
            $name          = (array_key_exists('name',         $fieldElementConfig) && $fieldElementConfig['name']        !== '')      ? $fieldElementConfig['name']          : '';
            $placeholder   = (array_key_exists('placeholder',  $fieldElementConfig) && $fieldElementConfig['placeholder'] !== '')      ? $fieldElementConfig['placeholder']   : '';
            $classes       = (array_key_exists('classes',      $fieldElementConfig) && $fieldElementConfig['classes']     !== '')      ? ' ' . $fieldElementConfig['classes'] : '';
            $addAttributes = (array_key_exists('addAttributes',$fieldElementConfig) && is_array($fieldElementConfig['addAttributes'])) ? $fieldElementConfig['addAttributes'] : array();

            $placeholderAttribute = ($placeholder !== '') ? ' placeholder="' . $placeholder . '"' : '';

            $addAttribute = '';
            if(count($addAttributes)) {
                foreach($addAttributes as $addAttrKey => $addAttrValue) {
                    if($addAttrKey !== '') {
                        $addAttrPrefix = ($addAttribute !== '') ? ' ' : '';
                        $addAttribute .= $addAttrPrefix . $addAttrKey . '=' . $addAttrValue;
                    }
                }
            }

            $elementBuildedHtml = '<input type="' . $type .  '" name="' . $name . '" value="' . $value . '"' . $idAttribute . ' class="form-control' . $classes . '"' . $placeholderAttribute . $addAttribute . $requiredAttribute . ' />';

            $wrapper = (array_key_exists('wrapper',$fieldElementConfig) && is_array($fieldElementConfig['wrapper']) && count($fieldElementConfig['wrapper'])) ? $fieldElementConfig['wrapper'] : array();
            if(count($wrapper)) {
                $tag = (array_key_exists('tag',$wrapper) && $wrapper['tag'] !== '') ? $wrapper['tag'] : 'div';

                $idAttribute      = (array_key_exists('id',$wrapper) && $wrapper['id'] !== '') ? ' id="' . $wrapper['id'] . '"' : '';
                $classesAttribute = (array_key_exists('classes',$wrapper) && $wrapper['classes'] !== '') ? ' class="' . $wrapper['classes'] . '"' : '';

                $elementHtml .= '<' . $tag . $idAttribute. $classesAttribute . '>';
                $elementHtml .= $elementBuildedHtml;
                $elementHtml .= '</' . $tag . '>';
            } else {
                $elementHtml .= $elementBuildedHtml;
            }

        }

        return $elementHtml;
    }

    public function buildTextareaElement($fieldElementConfig) {
        $elementHtml = '';
        if(is_array($fieldElementConfig) && count($fieldElementConfig)) {

            $idAttribute       = (array_key_exists('id',$fieldElementConfig) && $fieldElementConfig['id'] !== '') ? ' id="' . $fieldElementConfig['id'] . '"' : '';
            $requiredAttribute = (array_key_exists('required',$fieldElementConfig) && $fieldElementConfig['required'] !== false) ? ' required="required"' : '';

            $default       = (array_key_exists('default',      $fieldElementConfig) && $fieldElementConfig['default']     !== '')      ? $fieldElementConfig['default']       : '';
            $value         = (array_key_exists('value',        $fieldElementConfig) && $fieldElementConfig['value']       !== '')      ? $fieldElementConfig['value']         : $default;
            $name          = (array_key_exists('name',         $fieldElementConfig) && $fieldElementConfig['name']        !== '')      ? $fieldElementConfig['name']          : '';
            $placeholder   = (array_key_exists('placeholder',  $fieldElementConfig) && $fieldElementConfig['placeholder'] !== '')      ? $fieldElementConfig['placeholder']   : '';
            $classes       = (array_key_exists('classes',      $fieldElementConfig) && $fieldElementConfig['classes']     !== '')      ? ' ' . $fieldElementConfig['classes'] : '';
            $addAttributes = (array_key_exists('addAttributes',$fieldElementConfig) && is_array($fieldElementConfig['addAttributes'])) ? $fieldElementConfig['addAttributes'] : array();

            $placeholderAttribute = ($placeholder !== '') ? ' placeholder="' . $placeholder . '"' : '';

            $addAttribute = '';
            if(count($addAttributes)) {
                foreach($addAttributes as $addAttrKey => $addAttrValue) {
                    if($addAttrKey !== '') {
                        $addAttrPrefix = ($addAttribute !== '') ? ' ' : '';
                        $addAttribute .= $addAttrPrefix . $addAttrKey . '=' . $addAttrValue;
                    }
                }
            }

            $elementBuildedHtml = '<textarea name="' . $name . '"' . $idAttribute . ' class="form-control' . $classes . '"' . $placeholderAttribute . $addAttribute . $requiredAttribute . '>' . $value . '</textarea>';

            $wrapper = (array_key_exists('wrapper',$fieldElementConfig) && is_array($fieldElementConfig['wrapper']) && count($fieldElementConfig['wrapper'])) ? $fieldElementConfig['wrapper'] : array();
            if(count($wrapper)) {
                $tag = (array_key_exists('tag',$wrapper) && $wrapper['tag'] !== '') ? $wrapper['tag'] : 'div';

                $idAttribute      = (array_key_exists('id',$wrapper) && $wrapper['id'] !== '') ? ' id="' . $wrapper['id'] . '"' : '';
                $classesAttribute = (array_key_exists('classes',$wrapper) && $wrapper['classes'] !== '') ? ' class="' . $wrapper['classes'] . '"' : '';

                $elementHtml .= '<' . $tag . $idAttribute. $classesAttribute . '>';
                $elementHtml .= $elementBuildedHtml;
                $elementHtml .= '</' . $tag . '>';
            } else {
                $elementHtml .= $elementBuildedHtml;
            }

        }

        return $elementHtml;
    }

    public function buildSelectElement($fieldElementConfig) {
        $elementHtml = '';
        if(is_array($fieldElementConfig) && count($fieldElementConfig)) {

            $idAttribute       = (array_key_exists('id',$fieldElementConfig) && $fieldElementConfig['id'] !== '') ? ' id="' . $fieldElementConfig['id'] . '"' : '';
            $requiredAttribute = (array_key_exists('required',$fieldElementConfig) && $fieldElementConfig['required'] !== false) ? ' required="required"' : '';

            $default       = (array_key_exists('default',      $fieldElementConfig) && $fieldElementConfig['default']     !== '')      ? $fieldElementConfig['default']       : '';
            $value         = (array_key_exists('value',        $fieldElementConfig) && $fieldElementConfig['value']       !== '')      ? $fieldElementConfig['value']         : $default;
            $optionsArray  = (array_key_exists('options',      $fieldElementConfig) && is_array($fieldElementConfig['options']))       ? $fieldElementConfig['options']       : array();
            $name          = (array_key_exists('name',         $fieldElementConfig) && $fieldElementConfig['name']        !== '')      ? $fieldElementConfig['name']          : '';
            $placeholder   = (array_key_exists('placeholder',  $fieldElementConfig) && $fieldElementConfig['placeholder'] !== '')      ? $fieldElementConfig['placeholder']   : '';
            $classes       = (array_key_exists('classes',      $fieldElementConfig) && $fieldElementConfig['classes']     !== '')      ? ' ' . $fieldElementConfig['classes'] : '';
            $addAttributes = (array_key_exists('addAttributes',$fieldElementConfig) && is_array($fieldElementConfig['addAttributes'])) ? $fieldElementConfig['addAttributes'] : array();

            $placeholderAttribute = ($placeholder !== '') ? ' placeholder="' . $placeholder . '"' : '';

            $addAttribute = '';
            if(count($addAttributes)) {
                foreach($addAttributes as $addAttrKey => $addAttrValue) {
                    if($addAttrKey !== '') {
                        $addAttrPrefix = ($addAttribute !== '') ? ' ' : '';
                        $addAttribute .= $addAttrPrefix . $addAttrKey . '=' . $addAttrValue;
                    }
                }
            }

            $optionsHtml = '';
            if(count($optionsArray)) {
                foreach($optionsArray as $oValue => $oText) {
                    if($oValue !== '') {
                        $selected = ($oValue == $value) ? ' selected="selected"' : '';
                        $optionsHtml .= '<option value="' . $oValue . '"' . $selected . '>' . $oText . '</option>';
                    }
                }
            }

            $elementBuildedHtml = '<select name="' . $name . '"' . $idAttribute . ' class="form-control' . $classes . '"' . $placeholderAttribute . $addAttribute . $requiredAttribute . '>' . $optionsHtml . '</select>';

            $wrapper = (array_key_exists('wrapper',$fieldElementConfig) && is_array($fieldElementConfig['wrapper']) && count($fieldElementConfig['wrapper'])) ? $fieldElementConfig['wrapper'] : array();
            if(count($wrapper)) {
                $tag = (array_key_exists('tag',$wrapper) && $wrapper['tag'] !== '') ? $wrapper['tag'] : 'div';

                $idAttribute      = (array_key_exists('id',$wrapper) && $wrapper['id'] !== '') ? ' id="' . $wrapper['id'] . '"' : '';
                $classesAttribute = (array_key_exists('classes',$wrapper) && $wrapper['classes'] !== '') ? ' class="' . $wrapper['classes'] . '"' : '';

                $elementHtml .= '<' . $tag . $idAttribute. $classesAttribute . '>';
                $elementHtml .= $elementBuildedHtml;
                $elementHtml .= '</' . $tag . '>';
            } else {
                $elementHtml .= $elementBuildedHtml;
            }

        }

        return $elementHtml;
    }

    public function buildCheckboxElement($fieldElementConfig) {
        $elementHtml = '';
        if(is_array($fieldElementConfig) && count($fieldElementConfig)) {

            $idAttribute       = (array_key_exists('id',$fieldElementConfig) && $fieldElementConfig['id'] !== '') ? ' id="' . $fieldElementConfig['id'] . '"' : '';
            $requiredAttribute = (array_key_exists('required',$fieldElementConfig) && $fieldElementConfig['required'] !== false) ? ' required="required"' : '';

            $default       = (array_key_exists('default',        $fieldElementConfig) && $fieldElementConfig['default']         !== '')  ? $fieldElementConfig['default']         : '';
            $value         = (array_key_exists('value',          $fieldElementConfig) && $fieldElementConfig['value']           !== '')  ? $fieldElementConfig['value']           : $default;
            $valChecked    = (array_key_exists('valueChecked',   $fieldElementConfig) && $fieldElementConfig['valueChecked']    !== '')  ? $fieldElementConfig['valueChecked']    : '';
            $valNotChecked = (array_key_exists('valueNotChecked',$fieldElementConfig) && $fieldElementConfig['valueNotChecked'] !== '')  ? $fieldElementConfig['valueNotChecked'] : '';
            $checkboxOnly  = (array_key_exists('checkboxOnly',   $fieldElementConfig) && is_bool($fieldElementConfig['checkboxOnly']))   ? $fieldElementConfig['checkboxOnly']    : false;
            $name          = (array_key_exists('name',           $fieldElementConfig) && $fieldElementConfig['name']            !== '')  ? $fieldElementConfig['name']            : '';
            $classes       = (array_key_exists('classes',        $fieldElementConfig) && $fieldElementConfig['classes']         !== '')  ? $fieldElementConfig['classes']         : '';
            $addAttributes = (array_key_exists('addAttributes',  $fieldElementConfig) && is_array($fieldElementConfig['addAttributes'])) ? $fieldElementConfig['addAttributes']   : array();

            $classAttribute = ($classes !== '') ? ' class="' . $classes . '"' : '';

            $addAttribute = '';
            if(count($addAttributes)) {
                foreach($addAttributes as $addAttrKey => $addAttrValue) {
                    if($addAttrKey !== '') {
                        $addAttrPrefix = ($addAttribute !== '') ? ' ' : '';
                        $addAttribute .= $addAttrPrefix . $addAttrKey . '=' . $addAttrValue;
                    }
                }
            }

            $checked = ($value == $valChecked) ? ' checked="checked"' : '';

            $elementBuildedHtml = '<input type="hidden" name="' . $name . '" value="' . $valNotChecked . '">';
            if($checkboxOnly === false) {
                $elementBuildedHtml .= '<div class="checkbox">';
            }
            $elementBuildedHtml .= '<input type="checkbox" ' . $idAttribute . ' name="' . $name . '" value="' . $value . '"' . $checked . $classAttribute . $requiredAttribute . '>';
            if($checkboxOnly === false) {
                $elementBuildedHtml .= '</div>';
            }

            $wrapper = (array_key_exists('wrapper',$fieldElementConfig) && is_array($fieldElementConfig['wrapper']) && count($fieldElementConfig['wrapper'])) ? $fieldElementConfig['wrapper'] : array();
            if(count($wrapper)) {
                $tag = (array_key_exists('tag',$wrapper) && $wrapper['tag'] !== '') ? $wrapper['tag'] : 'div';

                $idAttribute      = (array_key_exists('id',$wrapper) && $wrapper['id'] !== '') ? ' id="' . $wrapper['id'] . '"' : '';
                $classesAttribute = (array_key_exists('classes',$wrapper) && $wrapper['classes'] !== '') ? ' class="' . $wrapper['classes'] . '"' : '';

                $elementHtml .= '<' . $tag . $idAttribute. $classesAttribute . '>';
                $elementHtml .= $elementBuildedHtml;
                $elementHtml .= '</' . $tag . '>';
            } else {
                $elementHtml .= $elementBuildedHtml;
            }

        }

        return $elementHtml;
    }

    public function buildRadioElement($fieldElementConfig) {
        $elementHtml = '';
        if(is_array($fieldElementConfig) && count($fieldElementConfig)) {

            $idAttribute       = (array_key_exists('id',$fieldElementConfig) && $fieldElementConfig['id'] !== '') ? ' id="' . $fieldElementConfig['id'] . '"' : '';
            $requiredAttribute = (array_key_exists('required',$fieldElementConfig) && $fieldElementConfig['required'] !== false) ? ' required="required"' : '';

            $name          = (array_key_exists('name',           $fieldElementConfig) && $fieldElementConfig['name']            !== '')  ? $fieldElementConfig['name']            : '';
            $classes       = (array_key_exists('classes',        $fieldElementConfig) && $fieldElementConfig['classes']         !== '')  ? $fieldElementConfig['classes']         : '';
            $addAttributes = (array_key_exists('addAttributes',  $fieldElementConfig) && is_array($fieldElementConfig['addAttributes'])) ? $fieldElementConfig['addAttributes']   : array();

            $classAttribute = ($classes !== '') ? ' class="' . $classes . '"' : '';

            $addAttribute = '';
            if(count($addAttributes)) {
                foreach($addAttributes as $addAttrKey => $addAttrValue) {
                    if($addAttrKey !== '') {
                        $addAttrPrefix = ($addAttribute !== '') ? ' ' : '';
                        $addAttribute .= $addAttrPrefix . $addAttrKey . '=' . $addAttrValue;
                    }
                }
            }

            $elementBuildedHtml = '<div class="radio"><input type="radio" ' . $idAttribute . ' name="' . $name . '"' . $classAttribute . $requiredAttribute . '></div>';

            $wrapper = (array_key_exists('wrapper',$fieldElementConfig) && is_array($fieldElementConfig['wrapper']) && count($fieldElementConfig['wrapper'])) ? $fieldElementConfig['wrapper'] : array();
            if(count($wrapper)) {
                $tag = (array_key_exists('tag',$wrapper) && $wrapper['tag'] !== '') ? $wrapper['tag'] : 'div';

                $idAttribute      = (array_key_exists('id',$wrapper) && $wrapper['id'] !== '') ? ' id="' . $wrapper['id'] . '"' : '';
                $classesAttribute = (array_key_exists('classes',$wrapper) && $wrapper['classes'] !== '') ? ' class="' . $wrapper['classes'] . '"' : '';

                $elementHtml .= '<' . $tag . $idAttribute. $classesAttribute . '>';
                $elementHtml .= $elementBuildedHtml;
                $elementHtml .= '</' . $tag . '>';
            } else {
                $elementHtml .= $elementBuildedHtml;
            }

        }

        return $elementHtml;
    }
}
