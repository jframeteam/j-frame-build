<?php
/**
 * Cron File
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** INIT File including */
require_once('init.php');

// http://example.com/core/cron.php?site_key=pits-portal

/** @var $cron Cron */
$cron = $Core->Cron();

/** Get Models with Cron Method */
$cronModels = $cron->listCronModels();

/** Run Model Cron Methods */
$cronResults = $cron->run();



/** Debug Output */
echo '<pre>$cronModels => ' . print_r($cronModels,true) . '</pre>';

if(is_array($cronResults) && count($cronResults)) {
    foreach($cronResults as $cronModelKey => $cronModelResult) {
        if(is_array($cronModelResult) && array_key_exists('success', $cronModelResult) && $cronModelResult['success']) {
            echo '<pre>$cronResults => ' . print_r($cronModelResult, true) . '</pre>';
        }
    }
}
