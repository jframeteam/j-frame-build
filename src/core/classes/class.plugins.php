<?php
/**
 * Plugins Class loads all necessary and allowed Plugins
 *
 * Some Methods implemented from Source by Planet ITservices GmbH & Co. KG
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** Class Plugins */
class Plugins
{
    /** @var $Core Core */
    private $Core;
    public  $plugins;
    private $methods;

    /**
     * Modules constructor.
     * @param $Core Core
     */
    function __construct($Core)
    {
        $this->Core = $Core;

        $this->plugins = array();
        $this->methods = array();

        $this->_registerPlugins();
    }

    /**
     * Magic Class Call
     *
     * @param $name
     * @param $args
     * @return mixed|null
     */
    public function __call($name,$args) {
        $useArgs = (is_array($args) && count($args)) ? implode(",",$args) : $this->Core;
        $methods = $this->methods;
        $method = (isset($methods[$name]) && is_object($methods[$name])) ? $methods[$name] : null;
        if($method === null) {
            if(class_exists($name) && in_array(strtolower($name), $this->plugins)) {
                $method = new $name($useArgs);
                $methods[$name] = $method;
                $this->methods = $methods;
            } else {
                $method = null;
            }
        }
        return $method;
    }

    /**
     * Register Allowed Plugins
     */
    private function _registerPlugins() {
        /** @var $configClass Config */
        $configClass = $this->Core->Config();

        /** @var $helperClass Helper */
        $helperClass = $this->Core->Helper();

        /** @var $sitesClass Sites */
        $sitesClass = $this->Core->Sites();

        /** Set Core Plugins */
        $corePlugins = array();
        /** Example */
        // $corePlugins[] = 'core';

        /** Get Default Config Plugins */
        $defaultPlugins = (is_array($configClass->get('default_plugins')) ? $configClass->get('default_plugins') : array());

        $curSite = $sitesClass->getSite();
        $curSiteModules = (count($curSite) && isset($curSite['plugins'])) ? $curSite['plugins'] : array();
        $allowedModules = array_merge($corePlugins, $defaultPlugins, $curSiteModules);

        $coreModulesPath = $this->Core->getRootPath() . DS . 'core' . DS . 'plugins';
        $pluginsRootDirsArray = scandir($coreModulesPath);

        foreach($allowedModules as $pluginName) {
            if(in_array(strtolower($pluginName), $pluginsRootDirsArray)) {
                $this->registerPlugin(strtolower($pluginName));
            }
        }
    }

    /**
     * Init Modules
     */
    public function initPlugins() {
        $plugins = $this->plugins;

        foreach($plugins as $pluginName) {
            $ucfPluginName = ucfirst($pluginName);
            $this->$ucfPluginName();
        }
    }


    /**
     * Parameter name [!string]: name of pugin to register
     * Returns true if successfully register, otherwise print warning
     *
     * @param $name
     * @return bool
     */
    public function registerPlugin($name)
    {
        if (!$name) {
            die('no name given!');
        }

        if (in_array($name, $this->plugins, true)) {
            $this->Core->setNote('Plugin `' . $name . '` already registered!', 'warning');

            return (false);
        } else {
            $this->plugins[] = $name;

            return (true);
        }
    }

    /**
     * Parameter name [!string]: name of plugin to unregister
     * Returns true if successfully unregisted, otherwise print warning
     *
     * @param $name
     * @return bool
     */
    public function unregisterPlugin($name)
    {
        if (!$name) {
            die('no name given!');
        }

        $found = array_search($name, $this->plugins, true);
        if (!$found) {
            $this->Core->setNote('Plugin `' . $name . '` CANNOT be unregistered! No such module registered.', 'warning');
        } else {
            unset($this->plugins[$name]);

            return (true);
        }
    }
}