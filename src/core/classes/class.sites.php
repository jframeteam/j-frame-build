<?php
/**
 * Sites Handler
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** Sites Class */
class Sites
{
    /** @var Core */
    private $Core;
    /** @vars */
    public $id_site;
    private $_usedSites = null;
    private $_usedSitesUrlKeys = null;

    public function __construct($Core)
    {
        $this->Core = $Core;

        $site = $this->getSite();
        $id_client = (count($site)) ? $site['id'] : 0;
        $this->id_site = $id_client;

        $this->_setSessionInfo();
    }

    private function _setSessionInfo() {
        /** Set Session Info */
        $_SESSION['site'] = $this->getSite();
    }

    private function _prepareSiteData($data) {
        $preparedData = array();

        if(is_array($data) && count($data)) {
            $id = (array_key_exists('id', $data)) ? $data['id'] : false;

            if($id !== false) {

                $countries = $this->Core->i18n()->getCountries();

                $name            = (array_key_exists('name', $data)) ? $data['name'] : '';
                $key             = (array_key_exists('key', $data)) ? $data['key'] : '';
                $urlKey_fromName = ($name != '') ? $this->Core->Helper()->createCleanString($name) : '';
                $urlKey_fromKey  = ($key != '') ? $this->Core->Helper()->createCleanString($key) : '';
                $urlKey          = ($urlKey_fromKey != '') ? $urlKey_fromKey : $urlKey_fromName;
                $contact         = (array_key_exists('contact', $data)) ? $data['contact'] : '';
                $street          = (array_key_exists('street', $data)) ? $data['street'] : '';
                $streetNo        = (array_key_exists('streetno', $data)) ? $data['streetno'] : '';
                $zip             = (array_key_exists('zip', $data)) ? $data['zip'] : '';
                $city            = (array_key_exists('city', $data)) ? $data['city'] : '';
                $state           = (array_key_exists('state', $data)) ? $data['state'] : '';
                $siteCountryId   = (array_key_exists('id_i18n_countries', $data)) ? $data['id_i18n_countries'] : '';
                $phone           = (array_key_exists('phone', $data)) ? $data['phone'] : '';
                $fax             = (array_key_exists('fax', $data)) ? $data['fax'] : '';
                $mobile          = (array_key_exists('mobile', $data)) ? $data['mobile'] : '';
                $web             = (array_key_exists('web', $data)) ? $data['web'] : '';
                $email           = (array_key_exists('email', $data)) ? $data['email'] : '';
                $models          = (array_key_exists('models', $data)) ? $data['models'] : '';
                $plugins         = (array_key_exists('plugins', $data)) ? $data['plugins'] : '';
                $active          = (array_key_exists('active', $data)) ? $data['active'] : 0;

                $preparedData = array(
                    'id' => $id,
                    'urlKey' => $urlKey,
                    'name' => $name,
                    'contact' => $contact,
                    'address' => array(
                        'street' => $street,
                        'streetno' => $streetNo,
                        'zip' => $zip,
                        'city' => $city,
                        'state' => $state,
                        'country_id' => $siteCountryId,
                        'phone' => $phone,
                        'fax' => $fax,
                        'mobile' => $mobile,
                        'web' => $web,
                        'email' => $email
                    ),
                    'models' => array(),
                    'plugins' => array(),
                    'active' => $active
                );

                if (isset($countries[$siteCountryId])) {

                    $country = $countries[$siteCountryId];

                    $countryId = $country['id'];
                    $countryCode = $country['code'];
                    $countryName = $country['name'];

                    $preparedData['address']['country']['id'] = $countryId;
                    $preparedData['address']['country']['code'] = $countryCode;
                    $preparedData['address']['country']['name'] = $countryName;
                }

                $modelsArray = preg_split('/,/', $models, -1, PREG_SPLIT_NO_EMPTY);
                foreach ($modelsArray as $modelKey) {
                    $preparedData['models'][] = trim($modelKey);
                }

                $pluginsArray = preg_split('/,/', $plugins, -1, PREG_SPLIT_NO_EMPTY);
                foreach ($pluginsArray as $pluginKey) {
                    $preparedData['plugins'][] = trim($pluginKey);
                }
            }
        }

        return $preparedData;
    }

    /**
     * Get all Sites as array
     *
     * @param bool $activeState
     * @return array
     */
    public function getSites($activeState=true) {
        $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'system_sites` ORDER BY `id`';

        $sitesFull = $this->Core->Db()->fromDatabase($sql, '@raw');

        $sites = array();
        if(is_array($sitesFull) && count($sitesFull)) {
            foreach ($sitesFull as $site) {
                $sites[$site['id']] = $this->_prepareSiteData($site);
            }
        }

        if(is_bool($activeState)) {
            foreach ($sites as $siteId => $site) {
                $site_active = (array_key_exists('active', $site)) ? $site['active'] : 0;
                if($site_active != $activeState) {
                    unset($sites[$siteId]);
                }
            }
        }

        return $sites;
    }

    public function getSite($siteId = null) {
        /** @var $Helper Helper */
        $Helper = $this->Core->Helper();

        $return = array();

        $usedSites = $this->_usedSites;

        if ($siteId !== null) {
            $site_id = ($this->siteExists((int)$siteId)) ? (int)$siteId : false;
        } else {
            $getUrlKey = $this->Core->Request()->getUrlKey();
            $getSiteUrlKey = (strpos($getUrlKey,'?site_key=') !== false && array_key_exists('site_key',$_GET)) ? $_GET['site_key'] : $getUrlKey;


            if($Helper->isCli()) {
                $argv = (isset($argv)) ? $argv : $_SERVER['argv'];
                if(!empty($argv[1])) {
                    $getSiteUrlKey = $argv[1];
                }
            }
            $getClientIdByUrlKey = $this->getSiteIdByUrlKey($getSiteUrlKey);
            $site_id = ($getClientIdByUrlKey) ? $getClientIdByUrlKey : false;
        }

        if ($site_id) {
            if (
                is_array($usedSites) &&
                array_key_exists($site_id, $usedSites) &&
                is_array($usedSites[$site_id]) &&
                count($usedSites[$site_id])
            ) {
                $return = $usedSites[$site_id];
            } else {
                if ($site_id) {
                    $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'system_sites` WHERE `id`=:siteId';
                    $params = array(
                        'siteId' => $site_id
                    );

                    $result = $this->Core->Db()->fromDatabase($sql, '@raw', $params);

                    foreach ($result as $row) {
                        $return = $this->_prepareSiteData($row);
                    }
                }
                if ($usedSites === null) {
                    $usedSites = array();
                }
                $usedSites[$site_id] = $return;
                $this->_usedSites = $usedSites;
            }
        }
        return $return;
    }

    public function getSiteBaseUrl($siteId = null) {
        $return = $this->Core->getBaseUrl();

        $site = $this->getSite($siteId);

        if(count($site) && array_key_exists('urlKey',$site)) {
            $return .= $site['urlKey'] . '/';
        }

        return $return;
    }

    public function siteExists($id) {
        $sql = 'SELECT id FROM `' . DB_TABLE_PREFIX . 'system_sites` WHERE `active`=:active AND `id` = :siteId ORDER BY `id`;';
        $params = array(
            'active' => 1,
            'siteId' => $id
        );
        $result = $this->Core->Db()->fromDatabase($sql,'@raw', $params);
        return (count($result)) ? true : false;
    }

    public function getSiteIdByUrlKey($urlKey) {
        $usedSiteUrlKeys = $this->_usedSitesUrlKeys;
        if(!is_array($usedSiteUrlKeys) || !array_key_exists($urlKey, $usedSiteUrlKeys)) {
            if($usedSiteUrlKeys === null) {
                $usedSiteUrlKeys = array();
            }
            $siteId = 0;
            $sites = $this->getSites();
            foreach ($sites as $site) {
                if ($urlKey == $site['urlKey']) {
                    $siteId = $site['id'];
                    break;
                }
            }
            $usedSiteUrlKeys[$urlKey] = $siteId;
            $this->_usedSitesUrlKeys = $usedSiteUrlKeys;
        }
        return $usedSiteUrlKeys[$urlKey];
    }

    public function getSiteChooser($accessibleSites = array()) {
        $chooserSites = array();
        if(count($accessibleSites)) {
            foreach($this->getSites() as $siteId => $site) {
                if(in_array($siteId,$accessibleSites)){
                    $chooserSites[$siteId] = $site;
                }
            }
        } else {
            $chooserSites = $this->getSites();
        }

        $siteChooserHtml = $this->Core->i18n()->translate('Keine Seiten zur Auswahl.');
        if(count($chooserSites)) {
            $currLangUrlKey = $this->Core->i18n()->getCurrLangUrlKey();
            $langUrlSlug = ($currLangUrlKey !== '') ? $currLangUrlKey . '/' : '';

            if(count($chooserSites) >= 2) {
                $siteChooserHtml = '';
                foreach ($chooserSites as $siteId => $site) {
                    $siteChooserHtml .= '<a href="' . $this->Core->getBaseUrl() . $langUrlSlug . $site['urlKey'] . '">' . $site['name'] . '</a><br />';
                }
            } else {
                $site = reset($chooserSites);
                $this->Core->Request()->redirect($this->Core->getBaseUrl() . $site['urlKey']);
            }
        }

        return $siteChooserHtml;
    }

    public function getSiteSwitchMenu() {
        $chooserSites = $this->getSites();
        $curSite = $this->getSite();
        $curSiteId = (is_array($curSite) && array_key_exists('id',$curSite)) ? $curSite['id'] : 0;

        $menuHtml = '';
        if(count($chooserSites)) {
            $currLangUrlKey = $this->Core->i18n()->getCurrLangUrlKey();
            $langUrlSlug = ($currLangUrlKey !== '') ? $currLangUrlKey . '/' : '';

            if(count($chooserSites) >= 2) {
                $menuLvl2Html = '';
                foreach($chooserSites as $siteId => $site) {
                    $li_attr_class = ($curSiteId == $siteId) ? ' class="active"' : '';

                    $linkTitle = $site['name'] ;

                    $siteUrl = $this->Core->getBaseUrl() . $langUrlSlug . $site['urlKey'];
                    $linkUrl = $siteUrl;

                    $menuLvl2Html .= '<li' . $li_attr_class . '>' . PHP_EOL;
                    $menuLvl2Html .= '<a href="' . $linkUrl . '" title="' . $linkTitle . '">' . $linkTitle . '</a>' . PHP_EOL;
                    $menuLvl2Html .= '</li>' . PHP_EOL;
                }

                if($menuLvl2Html !== '') {
                    $liDropdownActiveClass = ($curSiteId > 0) ? ' active' : '';
                    $menuHtml = '';

                    $menuHtml .= '<li class="dropdown' . $liDropdownActiveClass . '">';

                    $menuHtml .= '<a href="" title="' . $this->Core->i18n()->translate('Seiten') . '" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                    $menuHtml .= '<i class="fa fa-sitemap" aria-hidden="true"></i> ';
//                    $menuHtml .= $this->Core->i18n()->translate('Seiten');
                    $menuHtml .= '<span class="caret"></span>';
                    $menuHtml .= '</a>';

                    $menuHtml .= '<ul class="dropdown-menu">';
                    $menuHtml .= $menuLvl2Html;
                    $menuHtml .= '</ul>';

                    $menuHtml .= '</li>';
                }
            }
        }

        return $menuHtml;
    }

    public function addToMvcMenu() {

        $addToMvcMenu = '';
        $getClientSwitchMenu = $this->getSiteSwitchMenu();
        if($getClientSwitchMenu != '') {
            $addToMvcMenu .= '</ul>';
            $addToMvcMenu .= '<ul class="nav navbar-nav navbar-right site-switch-menu">';
            $addToMvcMenu .= $getClientSwitchMenu;
            $addToMvcMenu .= '</ul>';
        }

        return $addToMvcMenu;
    }
}
