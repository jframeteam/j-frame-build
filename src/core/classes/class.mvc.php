<?php
/**
 * MVC Class, Creates Output
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** MVC Class */
class Mvc
{
    /** @var Core */
    private $Core;

    /**
     * @vars
     * $Core
     * $_registeredModels
     * $_models
     * $modelKey
     * $controllerKey
     * $actionKey
     * $modelClasses
     * $metaTitle
     * $mvcMenuArray
     * $pageTitle
     * $pageHeadMeta
     * $pageAfterBodyStart
     * $pageBeforeBodyEnd
     */
    private $_registeredModels = null;
    private $_models           = null;
    private $modelKey          = null;
    private $controllerKey     = null;
    private $actionKey         = null;
    private $modelClasses      = null;
    private $metaTitle;
    private $mvcMenuArray      = null;
    private $mvcPlainAjax      = false;
    private $themeName         = null;
    private $pageTitle;
    private $pageHeadMeta;
    private $pageAfterBodyStart;
    private $pageBeforeBodyEnd;
    private $forceHeaderCache  = false;
    private $forceNoFileCache  = false;

    /**
     * mvc constructor.
     * @param $Core Core
     */
    function __construct($Core)
    {
        $this->Core = $Core;

        $this->_registerModels();
        $this->_setModels();
        $this->_setSessionInfo();
        $this->_initOutputSetter();
    }

    private function _setSessionInfo() {
        /** Set Session Info */
        $_SESSION['model'] = $this->getModel();
        $_SESSION['mvc']   = $this->getMvcInfo();
    }

    private function _registerModels()
    {
        $registeredModels = $this->_registeredModels;
        if($registeredModels === null) {
            $registeredModels = array();

            /** Set Core Models */
            $registeredModels['index']   = array(); /** Model Placeholder - Index */
            $registeredModels['noroute'] = array(); /** Model Placeholder - NoRoute */
            $registeredModels['system']  = array(); /** Model System */

            /** Set Default Models */
            $defaultModels = $this->Core->Config()->get('default_models');
            if(is_array($defaultModels) && count($defaultModels)) {
                foreach($defaultModels as $defaultModel) {
                    $registeredModels[$defaultModel] = array();
                }
            }

            /** Set Client Models */
            $curSite = $this->Core->Sites()->getSite();
            $allowedModels = (isset($curSite['models']) && is_array($curSite['models'])) ? $curSite['models'] : array();
            if(count($allowedModels)) {
                foreach($allowedModels as $allowedModel) {
                    $registeredModels[$allowedModel] = array();
                }
            }

            $coreModelsPath = $this->Core->getRootPath() . DS . 'core' . DS . 'models';
            $modelRootDirsArray = scandir($coreModelsPath);

            foreach ($modelRootDirsArray as $modelKey) {
                if ($modelKey == '.' || $modelKey == '..' || !array_key_exists($modelKey,$registeredModels)) {
                    continue;
                }

                $modelControllers = array();

                $modelControllerDirsArray = scandir($coreModelsPath . DS . $modelKey . DS . 'controller');

                foreach ($modelControllerDirsArray as $modelControllerFile) {

                    if ($modelControllerFile == '.' || $modelControllerFile == '..') {
                        continue;
                    }
                    $path_parts = pathinfo($coreModelsPath . DS . $modelKey . DS . 'controller' . DS . $modelControllerFile);

                    $controllerKey = $path_parts['filename'];
                    $modelControllers[] = $controllerKey;

                    $modelViews = array();

                    $viewsDir = $coreModelsPath . DS . $modelKey . DS . 'views' . DS . $controllerKey;
                    if (is_dir($viewsDir)) {
                        $modelViewDirsArray = scandir($viewsDir);

                        foreach ($modelViewDirsArray as $modelViewFile) {

                            if ($modelViewFile == '.' || $modelViewFile == '..') {
                                continue;
                            }
                            $path_parts = pathinfo($coreModelsPath . DS . $modelKey . DS . 'views' . DS . $modelViewFile);

                            $viewKey = $path_parts['filename'];

                            if (strpos($viewKey, '.') !== false) {
                                $viewKeyParts = preg_split('/\./', $viewKey, -1, PREG_SPLIT_NO_EMPTY);
                                $viewKey = $viewKeyParts[0];
                            }
                            if ($viewKey != '') {
                                $modelViews[] = $viewKey;
                            }
                        }
                    }

                    $modelViewsUnique = array_unique($modelViews);
                    if (count($modelViewsUnique) != count($modelViews)) {
                        $modelViewsSorted = array();
                        foreach ($modelViewsUnique as $modelViewKey) {
                            $modelViewsSorted[] = $modelViewKey;
                        }
                        $modelViews = $modelViewsSorted;
                    }

                    $registeredModels[$modelKey]['mvc'][$controllerKey] = $modelViews;
                }
            }

            $this->_registeredModels = $registeredModels;
        }
    }

    private function _setModels()
    {
        $models = array();
        $registeredModels = $this->_registeredModels;
        foreach($registeredModels as $key => $model) {
            $modelMvcArray = (isset($model['mvc']) && is_array($model['mvc'])) ? $model['mvc'] : array();

            $models[$key] = array(
                'key'      => $key,
                'path'     => DS . 'core' . DS . 'models' . DS . $key,
                'mvc'      => $modelMvcArray
            );
        }

        asort($models);

        $this->_models = $models;
    }

    /** Basic Model Methods */

    public function getModels()
    {
        if($this->_models == null) {
            $this->_setModels();
        }
        return $this->_models;
    }

    public function getModel($modelKey = '')
    {
        $models = $this->getModels();
        $model  = $models['index'];

        $useModelKey = ($modelKey != '') ? $modelKey : $this->getModelKey();

        if(array_key_exists($useModelKey,$models)){
            $model = $models[$useModelKey];
        }
        return $model;
    }

    public function hasModel($modelKey)
    {
        $models = $this->getModels();

        return (array_key_exists($modelKey,$models));
    }

    public function modelClass($modelKey = null,$args = null) {
        $modelKey     = ($modelKey !== null) ? (array_key_exists(lcfirst($modelKey), $this->getModels())) ? lcfirst($modelKey) : 'noroute' : $this->getModelKey();
        $modelClasses = (is_array($this->modelClasses)) ? $this->modelClasses : array();
        $modelClass   = (array_key_exists($modelKey, $modelClasses) && is_object($modelClasses[$modelKey])) ? $modelClasses[$modelKey] : null;

        if($modelClass === null) {
            $className = ucfirst($modelKey);
            if(class_exists($className) && $modelKey != 'noroute') {
                $useArgs = (is_array($args) && count($args)) ? implode(",",$args) : $this->Core;
                $modelClass = new $className($useArgs);
                $modelClasses[$modelKey] = $modelClass;
                $this->modelClasses = $modelClasses;
            }
        }

        return $modelClass;
    }

    /** Model Info Methods */

    public function getModelUrl($modelKey = '')
    {
        /** @var $sitesClass Sites */
        $sitesClass = $this->Core->Sites();
        $model = $this->getModel($modelKey);
        if(!isset($model['full_url'])) {
            $site = $sitesClass->getSite();
            $siteUrlKey = (count($site) && array_key_exists('urlKey',$site)) ? $site['urlKey'] : '';
            $siteUrlKeySlug = ($siteUrlKey !== '') ? $siteUrlKey . '/' : '';

            $getCurrLangUrlKey  = $this->Core->i18n()->getCurrLangUrlKey();
            $currLangUrlKey     = ($getCurrLangUrlKey != '') ? $getCurrLangUrlKey . '/' : '';

            $fullClientUrl =  $this->Core->getBaseUrl() . $siteUrlKeySlug . $currLangUrlKey;

            $fullUrl  = $fullClientUrl . $model['key'];

            $model['full_url'] = $fullUrl;
        }
        return $model['full_url'];
    }

    public function getModelAjaxUrl($modelKey = '')
    {
        if($modelKey === '') {
            $modelKey = $this->getModelKey();
        }
        $modelFullUrl = $this->getModelUrl($modelKey);
        $modelFullAjaxUrl = str_replace($modelKey,  'ajax/' . $modelKey, $modelFullUrl);
        return $modelFullAjaxUrl;
    }

    public function getModelKey()
    {
        $modelKey = $this->modelKey;
        if($modelKey === null) {
            $modelKey = 'index';

            $getPath = $this->Core->Request()->getParams();
            $getPath = ($getPath !== '') ? trim($getPath,'/') . '/' : '';

            if($getPath !== '') {
                /** @var $sitesClass Sites */
                $sitesClass = $this->Core->Sites();
                $site       = $sitesClass->getSite();
                $siteUrlKey = (array_key_exists('urlKey', $site)) ? $site['urlKey'] : '';
                $siteUrlKeySlug = ($siteUrlKey !== '') ? $siteUrlKey . '/' : '';
                if ($siteUrlKeySlug !== '') {
                    $getPath = str_replace($siteUrlKeySlug, '', $getPath);
                }

                $pathParts = preg_split('/\//', $getPath, -1, PREG_SPLIT_NO_EMPTY);

                if (array_key_exists(0, $pathParts)) {
                    if (preg_match('#\b[a-z]{2}\b#', $pathParts[0])) {
                        if (array_key_exists(1, $pathParts)) {
                            $modelKey = $pathParts[1];
                        }
                    } else {
                        $modelKey = $pathParts[0];
                    }
                }

                $models = $this->getModels();

                if (!array_key_exists($modelKey, $models)) {
                    $modelKey = 'noroute';
                }
            }

            $this->modelKey = $modelKey;
        }

        return $modelKey;
    }

    public function getControllerKey()
    {
        $controllerKey = $this->controllerKey;

        if($controllerKey === null) {

            $controllerKey = 'index';

            $modelKey      = $this->getModelKey();

            $getPath = $this->Core->Request()->getParams();
            $getPath = ($getPath !== '') ? trim($getPath,'/') . '/' : '';

            if($getPath !== '' && $modelKey != 'noroute') {
                /** @var $sitesClass Sites */
                $sitesClass = $this->Core->Sites();
                $site       = $sitesClass->getSite();
                $siteUrlKey = (array_key_exists('urlKey', $site)) ? $site['urlKey'] : '';
                $siteUrlKeySlug = ($siteUrlKey !== '') ? $siteUrlKey . '/' : '';
                if ($siteUrlKeySlug !== '') {
                    $getPath = str_replace($siteUrlKeySlug, '', $getPath);
                }

                $replaceString = $modelKey;

                $getPath = str_replace($replaceString, '', $getPath);

                $pathParts = preg_split('/\//', $getPath, -1, PREG_SPLIT_NO_EMPTY);

                if (array_key_exists(0, $pathParts)) {
                    if (preg_match('#\b[a-z]{2}\b#', $pathParts[0])) {
                        if (array_key_exists(1, $pathParts)) {
                            $controllerKey = $pathParts[1];
                        }
                    } else {
                        $controllerKey = $pathParts[0];
                    }
                }
            }

            $this->controllerKey = $controllerKey;
        }

        return $controllerKey;
    }

    public function getActionKey()
    {
        $actionKey = $this->actionKey;

        if($actionKey === null) {
            $actionKey = 'index';

            $modelKey      = $this->getModelKey();
            $controllerKey = ($this->getControllerKey() !== 'index') ? $this->getControllerKey() : '';

            $getPath = $this->Core->Request()->getParams();
            $getPath = ($getPath !== '') ? trim($getPath,'/') . '/' : '';

            if($getPath !== '' && $modelKey != 'noroute') {
                /** @var $sitesClass Sites */
                $sitesClass = $this->Core->Sites();
                $site       = $sitesClass->getSite();
                $siteUrlKey = (array_key_exists('urlKey', $site)) ? $site['urlKey'] : '';
                $siteUrlKeySlug = ($siteUrlKey !== '') ? $siteUrlKey . '/' : '';
                if ($siteUrlKeySlug !== '') {
                    $getPath = str_replace($siteUrlKeySlug, '', $getPath);
                    $getPath = ($getPath !== '') ? trim($getPath,'/') . '/' : '';
                }

                $replaceString = $modelKey . '/' . $controllerKey;

                $getPath = str_replace($replaceString, '', $getPath);

                $pathParts = preg_split('/\//', $getPath, -1, PREG_SPLIT_NO_EMPTY);

                if (array_key_exists(0, $pathParts)) {
                    if (preg_match('#\b[a-z]{2}\b#', $pathParts[0])) {
                        if (array_key_exists(1, $pathParts)) {
                            $actionKey = $pathParts[1];
                        }
                    } else {
                        $actionKey = $pathParts[0];
                    }
                }
            }

            $this->actionKey = $actionKey;
        }

        return $actionKey;
    }

    public function getMvcParams($asArray = true)
    {
        $mvcParams = array();

        $getPath = $this->Core->Request()->getParams();
        $getPath = ($getPath !== '') ? trim($getPath,'/') . '/' : '';

        if($getPath !== '') {
            /** @var $sitesClass Sites */
            $sitesClass = $this->Core->Sites();
            $site       = $sitesClass->getSite();
            $siteUrlKey = (array_key_exists('urlKey', $site)) ? $site['urlKey'] : '';
            $siteUrlKeySlug = ($siteUrlKey !== '') ? $siteUrlKey . '/' : '';
            if ($siteUrlKeySlug !== '') {
                $getPath = str_replace($siteUrlKeySlug, '', $getPath);
            }

            $modelKey      = $this->getModelKey();
            $controllerKey = $this->getControllerKey();
            $actionKey     = $this->getActionKey();

            $controllerKeySlug = ($controllerKey !== 'index' || strpos($getPath, $modelKey . '/index') !== false) ? $controllerKey . '/' : '';
            $actionKeySlug = ($actionKey !== 'index' || strpos($getPath, $modelKey . '/' . $controllerKeySlug . 'index') !== false) ? $actionKey . '/' : '';

            $replaceString = $modelKey . '/' . $controllerKeySlug . $actionKeySlug;

            $mvcParams = str_replace($replaceString, '', $getPath);

            if ($asArray) {
                $pathParts = preg_split('/\//', $mvcParams, -1, PREG_SPLIT_NO_EMPTY);

                $mvcParamsArray = array();
                $paramKeyLast = null;
                foreach ($pathParts as $key => $part) {
                    if ($key > 2) {
                        if ($paramKeyLast == null) {
                            $paramKeyLast = $part;
                            $mvcParamsArray[$part] = null;
                            continue;
                        } else {
                            $mvcParamsArray[$paramKeyLast] = $part;
                            $paramKeyLast = null;
                            continue;
                        }
                    } else {
                        if ($paramKeyLast == null) {
                            $paramKeyLast = $part;
                            $mvcParamsArray[$part] = null;
                            continue;
                        } else {
                            $mvcParamsArray[$paramKeyLast] = $part;
                            $paramKeyLast = null;
                            continue;
                        }
                    }
                }

                $mvcParams = $mvcParamsArray;
            }
        }

        return $mvcParams;
    }

    public function getMvcInfo($path = null)
    {

        $modelKey      = $this->getModelKey();
        $controllerKey = $this->getControllerKey();
        $actionKey     = $this->getActionKey();
        $params        = $this->getMvcParams();

        if($path !== null && strpos($path, '/') !== false) {
            $modelKey      = 'index';
            $controllerKey = 'index';
            $actionKey     = 'index';
            $params        = array();

            $pathParts = preg_split('/\//', trim($path), -1, PREG_SPLIT_NO_EMPTY);
            if (array_key_exists(0, $pathParts)) {
                $modelKey = $pathParts[0];
            }
            if (array_key_exists(1, $pathParts)) {
                $controllerKey = $pathParts[1];
            }
            if (array_key_exists(2, $pathParts)) {
                $actionKey = $pathParts[2];
            }
        }

        $mvcInfo = array(
            'model'      => $modelKey,
            'controller' => $controllerKey,
            'action'     => $actionKey,
            'params'     => $params,
        );

        return $mvcInfo;
    }

    /** Model View Methods */

    public function setMVCPlainAjax($plainAjax) {
        $this->mvcPlainAjax = ($plainAjax !== true) ? false : true;
    }

    public function getMVCPlainAjax() {
        return $this->mvcPlainAjax;
    }

    public function getMVCModelPath()
    {
        $coreModelsPath  = $this->Core->getRootPath() . DS . 'core' . DS . 'models';
        return $coreModelsPath . DS . $this->getModelKey();
    }

    public function getMVCViewPath()
    {
        $ajaxPath = ($this->Core->Request()->isAjax()) ? DS . 'ajax' : '';
        return $this->getMVCModelPath() . DS . 'views' . $ajaxPath;
    }

    public function getMVCViewFilePath()
    {
        $mvcViewPath      = $this->getMVCViewPath();
        $getControllerKey = $this->getControllerKey();
        $getActionKey     = $this->getActionKey();

        $MVCViewFilePath = $mvcViewPath . DS . $getControllerKey . DS . $getActionKey . '.php';

        return (file_exists($MVCViewFilePath)) ? $MVCViewFilePath : false;
    }

    public function getContentByMVC($modelKey, $controllerKey = 'index', $actionKey = 'index')
    {
        $isAjax = $this->Core->Request()->isAjax();

        $getModels = $this->getModels();

        $modelKey = (array_key_exists($modelKey, $getModels)) ? $modelKey : 'noroute';

        $content = '';

        $corePath = $this->Core->getRootPath() . DS . 'core';
        $modelControllerPath = 'models' . DS . $modelKey . DS . 'controller';

        if($isAjax) {
            $modelControllerPath = $modelControllerPath . DS . 'ajax';
        }

        $fileName = $controllerKey . '.php';

        $fullFile = $corePath . DS . $modelControllerPath . DS . $fileName;

        $error = false;
        $errorMsg = '';

        if(file_exists($fullFile)){

            global $Core, $Mvc;
            $Mvc = $this;
            $Core = $this->Core;

            include_once($fullFile);

            if($modelKey == 'noroute') {
                $error = true;
                $errorMsg = 'Model Call Runs into NoRoute';
            } else {
                $actionFunction = $actionKey . 'Action';

                if(function_exists($actionFunction)) {
                    $return = $actionFunction();
                    if($isAjax) {
                        $plainAjax = $this->getMVCPlainAjax();
                        if($plainAjax) {
                            $content .= json_encode($return);
                        } else {
                            $succArr = array('success' => true, 'result' => $return);
                            $content .= json_encode($succArr);
                        }
                    } else {
                        $content .= $return;
                    }
                } else {
                    if($isAjax) {
                        $error = true;
                        $errorMsg = sprintf($this->Core->i18n()->translate('Model Ajax \'%s\' controller Action \'%s\' nicht gefunden...'), $modelKey, $actionKey);

                        $outputMsg = $this->Core->i18n()->translate('An Error occurred, see Log for more info!');

                        $errArr = array('success' => false, 'message' => $outputMsg);
                        $content .= json_encode($errArr);
                    } else {
                        $error = true;
                        $errorMsg = sprintf($this->Core->i18n()->translate('Model \'%s\' controller Action \'%s\' nicht gefunden...'), $modelKey, $actionKey);

                        $outputMsg = $this->Core->i18n()->translate('An Error occurred, see Log for more info!');

                        $content .= $outputMsg;
                    }
                }
            }
        } else {
            $isIndexPage = ($this->getModelKey() == 'index' && $this->getControllerKey() == 'index' && $this->getActionKey() == 'index');
            if($isIndexPage) {
                $outputMsg = $this->Core->i18n()->translate('No Home Path Found...');

                $content .= $outputMsg;
            } else {
                $error = true;
                $errorMsg = sprintf($this->Core->i18n()->translate('Model \'%s\' Controller \'%s\' View File \'%s\' nicht gefunden...'), $modelKey, $controllerKey, $fullFile);

                $outputMsg = $this->Core->i18n()->translate('An Error occurred, see Log for more info!');

                $content .= $outputMsg;
            }
        }

        if($error) {
            $logMessage = sprintf('Get Content By MVC runs into Error: %s', $errorMsg) . PHP_EOL;
            $logMessage .= 'CurrUrl => ' . print_r($this->Core->Request()->getCurrUrl(), true) . PHP_EOL;
            $logMessage .= 'BaseUrl => ' . print_r($this->Core->getBaseUrl(), true) . PHP_EOL;
            $logMessage .= 'Path => ' . print_r($this->Core->Request()->getPath(), true) . PHP_EOL;
            $logMessage .= 'Params => ' . print_r($this->Core->Request()->getParams(), true) . PHP_EOL;
            $logMessage .= 'Mvc => ' . print_r($this->getMvcInfo(), true);
            $this->Core->Log($logMessage, '', true);
        }

        return $content;
    }

    /** MVC Menu Methods */

    public function setMvcMenuArray($menuArray)
    {
        return $this->mvcMenuArray = $menuArray;
    }

    public function getMvcMenuArray()
    {
        $menuArray = $this->mvcMenuArray;
        if($menuArray === null) {
            $menuArray = array();
            $curSite = $this->Core->Sites()->getSite();
            if(count($curSite)) {
                $models = $this->getModels();
                foreach ($models as $model) {
                    if ($model['key'] == 'index' || $model['key'] == 'noroute' || $model['key'] == 'debug') {
                        continue;
                    }
                    $modelFullUrl = $this->getModelUrl($model['key']);
                    $menuArray[$model['key']]['title'] = $this->Core->i18n()->translate($model['key']);
                    $menuArray[$model['key']]['full_url'] = $modelFullUrl;
                    $childrenArray = array();
                    $modelMcvArray = (is_array($model['mvc'])) ? $model['mvc'] : array();
                    foreach ($modelMcvArray as $controllerKey => $viewKeys) {
                        if ($controllerKey == 'index' || $controllerKey == 'ajax') {
                            continue;
                        }

                        $childKey = $model['key'] . '_' . $controllerKey;
                        $childrenArray[$childKey]['title'] = $this->Core->i18n()->translate($childKey);
                        $childrenArray[$childKey]['controller_key'] = $controllerKey;
                        $childrenArray[$childKey]['full_url'] = $modelFullUrl . '/' . $controllerKey;
                    }
                    $menuArray[$model['key']]['children'] = $childrenArray;
                }
            }
            $this->mvcMenuArray = $menuArray;
        }

        return $menuArray;
    }

    public function getMvcMenu()
    {
        /** @var $accClass Acc */
        $accClass = $this->modelClass('Acc');
        $user     = (is_object($accClass) && method_exists($accClass, 'getUser')) ? $accClass->getUser() : array();
        $isSu     = (array_key_exists('su', $user) && $user['su']);

        $menuHtml = '';

        if(count($user)) {
            $menuArray = $this->getMvcMenuArray();

            /** Set Home Path Model in front of Menu */
            if($this->Core->Config()->get('home_path') != '') {
                $homePath        = $this->Core->Config()->get('home_path');
                $homePathMvcInfo = ($homePath != '' && is_array($this->getMvcInfo($homePath))) ? $this->getMvcInfo($homePath) : array();
                $homeModelKey    = (count($homePathMvcInfo) && array_key_exists('model', $homePathMvcInfo) && $homePathMvcInfo['model'] != 'index') ? $homePathMvcInfo['model'] : '';

                if(array_key_exists($homeModelKey, $menuArray)) {
                    $homeModelMenuArray = $menuArray[$homeModelKey];
                    $menuArrayNew = array();

                    $menuArrayNew[$homeModelKey] = $homeModelMenuArray;

                    foreach ($menuArray as $modelKey => $modelUrls) {
                        if ($modelKey == $homeModelKey) {
                            continue;
                        }
                        $menuArrayNew[$modelKey] = $modelUrls;
                    }

                    $menuArray = $menuArrayNew;
                }
            }

            /** @var $cmsClass Cms */
            $cmsClass = $this->modelClass('Cms');
            $currPage = (is_object($cmsClass)) ? $cmsClass->getPage() : array();

            /** @var $mediaClass Media */
            $mediaClass        = $this->modelClass('Media');
            $currGalleryUrlKey = (is_object($mediaClass)) ? $mediaClass->getCurrGalleryUrlKey() : '';

            foreach ($menuArray as $modelKey => $modelUrls) {

//                if($modelKey == 'system' && !$isSu) { continue; }

                $model = $this->getModel($modelKey);

                if (count($user) && !$accClass->hasAccess($modelKey)) {
                    continue;
                }

                $lvl1_li_classes = array();
                $lvl1_li_a_attr = '';
                $lvl1_li_a_caret = '';
                if ($this->getModelKey() == $modelKey) {
                    $addActive = true;

                    /** @TODO: Add Dynamically No Active Forcing */
                    if($this->getModelKey() == 'cms' && count($currPage)) {
                        $addActive = false;
                    }
                    if($this->getModelKey() == 'media' && $currGalleryUrlKey !== '') {
                        $addActive = false;
                    }
                    if($addActive) {
                        $lvl1_li_classes[] = 'active';
                    }
                }

                $hasAccessChildren = array();
                if(is_array($modelUrls) && array_key_exists('children', $modelUrls) && is_array($modelUrls['children'])) {
                    foreach ($modelUrls['children'] as $childKey => $child) {
                        if (count($user) && !$accClass->hasAccess($childKey)) {
                            continue;
                        }
                        $hasAccessChildren[$childKey] = $child;
                    }
                }

                if (count($hasAccessChildren)) {
                    $lvl1_li_classes[] = 'dropdown';
                    $lvl1_li_a_attr = ' class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"';
                    $lvl1_li_a_caret = ' <span class="caret"></span>';
                }
                $lvl1_li_attr_class = (count($lvl1_li_classes)) ? ' class="' . implode(' ', $lvl1_li_classes) . '"' : '';

                $linkTitle = $modelUrls['title'];
                $linkUrl = $modelUrls['full_url'];

                $menuHtml .= '<li' . $lvl1_li_attr_class . '>' . PHP_EOL;
                $menuHtml .= '<a href="' . $linkUrl . '" title="' . $linkTitle . '"' . $lvl1_li_a_attr . '>' . $linkTitle . $lvl1_li_a_caret . '</a>' . PHP_EOL;

                if (count($hasAccessChildren)) {
                    $menuHtml .= '<ul class="dropdown-menu">' . PHP_EOL;

                    $modelMcvArray = (array_key_exists('mvc', $model) && is_array($model['mvc'])) ? $model['mvc'] : array();
                    if (array_key_exists('index', $modelMcvArray) && is_array($modelMcvArray['index']) && count($modelMcvArray['index'])) {
                        $menuHtml .= '<li><a href="' . $linkUrl . '">' . $linkTitle . ' ' . $this->Core->i18n()->translate('Übersicht') . '</a></li>' . PHP_EOL;
                        $menuHtml .= '<li role="separator" class="divider"></li>' . PHP_EOL;
                    }

                    foreach ($hasAccessChildren as $childKey => $child) {

                        $childTitle = $child['title'];
                        $childUrl = $child['full_url'];

                        $lvl2_li_class_attr = '';
                        if ($this->getControllerKey() == $child['controller_key'] && !count($currPage)) {
                            $lvl2_li_class_attr = ' class="active"';
                        }
                        $menuHtml .= '<li' . $lvl2_li_class_attr . '><a href="' . $childUrl . '" title="' . $childTitle . '">' . $childTitle . '</a></li>' . PHP_EOL;
                    }
                    $menuHtml .= '</ul>' . PHP_EOL;
                }
                $menuHtml .= '</li>' . PHP_EOL;
            }
        }

        /** Add To Mvc Menu from Models */
        foreach($this->getModels() as $modelKey => $model) {
            $modelClass = $this->modelClass($modelKey);
            $addToMvcMenu = (is_object($modelClass) && method_exists($modelClass, 'addToMvcMenu')) ? $modelClass->addToMvcMenu() : '';
            $menuHtml .= $addToMvcMenu;
        }

        return $menuHtml;
    }

    public function getBreadcrumb() {

        /** @var $accClass Acc */
        $accClass = $this->modelClass('Acc');

        $mvcArray = $this->getMvcInfo();

        $baseUrl    = $this->Core->getBaseUrl();
        $projectName = $this->Core->Config()->get('project_name');

        $site     = $this->Core->Sites()->getSite();
        $siteUrl  = (count($site)) ? $baseUrl . $site['urlKey'] : $baseUrl;
        $siteName = (count($site)) ? $site['name'] : '';

        $breadcrumb = '<ol class="breadcrumb">';
        if($mvcArray['model'] == 'index' && $mvcArray['controller'] == 'index' && $mvcArray['action'] == 'index') {
            $breadcrumb .= '<a class="active"><a href="' . $baseUrl . '">' . $projectName . '</a></li>';
        } else {
            if(count($site)){
                $breadcrumb .= '<li><a href="' . $siteUrl . '">' . $projectName . ' - ' . $siteName . '</a></li>';

                $modelCode = $mvcArray['model'];
                if($mvcArray['controller'] == 'index' && $mvcArray['action'] == 'index' && (!is_object($accClass) || $accClass->hasAccess($modelCode))) {
                    $breadcrumb .= '<li class="active" id="bc_' . $modelCode . '">' . $this->Core->i18n()->translate($modelCode) . '</li>';
                } else {
                    $breadcrumb .= '<li><a href="' . $siteUrl . '/' . $mvcArray['model'] . '">' . $this->Core->i18n()->translate($modelCode) . '</a></li>';
                }

                if($mvcArray['controller'] != 'index') {
                    $controllerCode = $mvcArray['model'] . '_' . $mvcArray['controller'];
                    if($mvcArray['action'] == 'index' && (!is_object($accClass) || $accClass->hasAccess($controllerCode))) {
                        $breadcrumb .= '<li class="active" id="bc_' . $controllerCode . '">' . $this->Core->i18n()->translate($controllerCode) . '</li>';
                    } else {
                        $breadcrumb .= '<li id="bc_' . $controllerCode . '"><a href="' . $siteUrl . '/' . $mvcArray['model'] . '/' . '">' . $this->Core->i18n()->translate($mvcArray['model'] . '_' . $mvcArray['controller']) . '</a></li>';
                    }
                }

                if($mvcArray['action'] != 'index') {
                    $actionCode = $mvcArray['model'] . '_' . $mvcArray['controller'] . '_' . $mvcArray['action'];
                    $breadcrumb .= '<li class="active" id="bc_' . $actionCode . '">' . $this->getPageTitle() . '</li>';
                }
            }
        }
        $breadcrumb .= '</ol>';

        return $breadcrumb;
    }

    /** Theme Methods */

    public function setThemeName($themeName)
    {
        $this->themeName = $themeName;
    }

    public function getThemeName()
    {
        $thisThemeName    = $this->themeName;
        $optionsThemeName = ($this->Core->Config()->get('theme_name') != '') ? $this->Core->Config()->get('theme_name') : 'pitsPortal';
        // return ($thisThemeName != '') ? $thisThemeName : $optionsThemeName;
        return ($thisThemeName !== null) ? $thisThemeName : $optionsThemeName;
    }

    public function getThemeRelPath()
    {
        return $this->Core->getRootPath().DS.'core'.DS.'themes'.DS.$this->getThemeName();
    }

    public function getThemeUri()
    {
        $theme_base_uri = str_replace("/index.php?path=/", "", $this->Core->getBaseUrl()).'core/themes/'.$this->getThemeName();
        return $theme_base_uri;
    }

    public function getThemeTplRelPath()
    {
        return $this->getThemeRelPath().DS.'tpl';
    }

    public function getThemeTplUri()
    {
        return $this->getThemeUri().'/tpl';
    }

    public function getThemeSkinRelpath()
    {
        return $this->getThemeRelPath().DS.'skin';
    }

    public function getThemeSkinUri()
    {
        return $this->getThemeUri().'/skin';
    }

    public function getMinimalBsThemeHtml() {
        $minimalBsTheme_html_head = '
        <head>
        <meta charset="utf-8">
        <title>{{metaTitle}}</title>
        <meta name="description" content="{{metaDescription}}"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        {{pageHeadMeta}}
        </head>';

        $minimalBsTheme_html_body = '
        <body>
            {{pageAfterBodyStart}}
            <div class="container">
                <div class="content clearfix">
                    <h1 class="page-header">
                        {{pageTitle}}
                    </h1>
                    {{content}}
                </div>
            </div>
            {{pageBeforeBodyEnd}}
            <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        </body>';

        $minimalBsTheme_html = '<!DOCTYPE html><html lang="{{currLang}}">' . $minimalBsTheme_html_head . $minimalBsTheme_html_body . '</html>';

        return $this->minimizeHtml($minimalBsTheme_html);
    }

    public function minimizeHtml($html) {

        $search = array(
            '/\>[^\S ]+/s',     // strip whitespaces after tags, except space
            '/[^\S ]+\</s',     // strip whitespaces before tags, except space
            '/(\s)+/s',         // shorten multiple whitespace sequences
            '/<!--(.|\s)*?-->/' // Remove HTML comments
        );

        $replace = array(
            '>',
            '<',
            '\\1',
            ''
        );

        $html = preg_replace($search, $replace, $html);

        return $html;
    }

    /** Output Methods */

    private function _initOutputSetter() {
        $this->setMetaTitle($this->Core->Config()->get('default_title'));
        $this->setPageTitle($this->Core->Config()->get('project_name'));

        $getPageHeadMeta       = $this->getPageHeadMeta();
        $getPageAfterBodyStart = $this->getPageAfterBodyStart();
        $getPageBeforeBodyEnd  = $this->getPageBeforeBodyEnd();

        $getCorePageHeadMeta       = (method_exists($this->Core,'setMvcPageHeadMeta'))       ? $this->Core->setMvcPageHeadMeta()       : '';
        $getCorePageAfterBodyStart = (method_exists($this->Core,'setMvcPageAfterBodyStart')) ? $this->Core->setMvcPageAfterBodyStart() : '';
        $getCorePageBeforeBodyEnd  = (method_exists($this->Core,'setMvcPageBeforeBodyEnds')) ? $this->Core->setMvcPageBeforeBodyEnds() : '';

        if($getCorePageHeadMeta !== '' && strpos($getPageHeadMeta, $getCorePageHeadMeta) === false) {
            $this->addPageHeadMeta($getCorePageHeadMeta);
        }
        if($getCorePageAfterBodyStart !== '' && strpos($getPageAfterBodyStart, $getCorePageAfterBodyStart) === false) {
            $this->addPageAfterBodyStart($getCorePageAfterBodyStart);
        }
        if($getCorePageBeforeBodyEnd !== '' && strpos($getPageBeforeBodyEnd, $getCorePageBeforeBodyEnd) === false) {
            $this->addPageBeforeBodyEnd($getCorePageBeforeBodyEnd);
        }
    }

    public function setMetaTitle($title)
    {
        return $this->metaTitle = $title;
    }

    public function getMetaTitle()
    {
        return $this->metaTitle;
    }

    public function setDescription($description)    {
        return $this->Core->Config()->set('project_name', $description);
    }

    public function getDescription()
    {
        return $this->Core->Config()->get('project_name');
    }

    public function setPageTitle($title)
    {
        return $this->pageTitle = $title;
    }

    public function getPageTitle()
    {
        return  $this->pageTitle;
    }

    public function addPageHeadMeta($string) {
        $getPageHeadMeta = $this->getPageHeadMeta();
        $pageHeadMeta = $getPageHeadMeta . $string;
        $this->pageHeadMeta = $pageHeadMeta;
        return $pageHeadMeta;
    }

    public function getPageHeadMeta() {
        $pageHeadMeta = $this->pageHeadMeta;
        return $pageHeadMeta;
    }

    public function addPageAfterBodyStart($string) {
        $getPageAfterBodyStart = $this->getPageAfterBodyStart();
        $pageAfterBodyStart = $getPageAfterBodyStart . $string;
        $this->pageAfterBodyStart = $pageAfterBodyStart;
        return $pageAfterBodyStart;
    }

    public function getPageAfterBodyStart() {
        $pageAfterBodyStarts = $this->pageAfterBodyStart;
        return $pageAfterBodyStarts;
    }

    public function addPageBeforeBodyEnd($string) {
        $getPageBeforeBodyEnd = $this->getPageBeforeBodyEnd();
        $pageBeforeBodyEnd = $getPageBeforeBodyEnd . $string;
        $this->pageBeforeBodyEnd = $pageBeforeBodyEnd;
        return $pageBeforeBodyEnd;
    }

    public function getPageBeforeBodyEnd() {
        $pageBeforeBodyEnd = $this->pageBeforeBodyEnd;
        return $pageBeforeBodyEnd;
    }

    public function setContent($model, $content)
    {
        $models = $this->getModels();
        if($model != '' && array_key_exists($model, $models)){
            $models[$model]['content'] = $content;
        }
        $this->_models = $models;
    }

    public function getContent($model = '')
    {
        $thisModel = $this->getModel();
        $content = $thisModel['content'];

        if($model != '') {
            $models = $this->getModels();
            if(array_key_exists($model, $models)){
                $content = $models[$model]['content'];
            } else {
                $content = $models['noroute']['content'];
            }
        }

        $getNotes = $this->Core->getNote();

        if($getNotes != '') {

            $contentWithNote = $getNotes . $content;
            $content = $contentWithNote;
        }

        return $content;
    }

    /** File Cache Methods */

    public function setForceHeaderCache() {
        $this->forceHeaderCache = true;
    }

    public function forceHeaderCache() {
        return $this->forceHeaderCache;
    }

    public function useFileCache() {
        $useFileCache_config = $this->Core->Config()->get('use_output_file_cache');

        $useFileCache = ($useFileCache_config !== '' && is_bool($useFileCache_config)) ? $useFileCache_config : false;

        return $useFileCache;
    }

    public function setForceNoFileCache() {
        $this->forceNoFileCache = true;
    }

    public function forceNoFileCache() {
        return $this->forceNoFileCache;
    }

    public function getFileCacheLifetime() {
        $fileCacheLifetime_config = $this->Core->Config()->get('output_file_cache_lifetime');

        $fileCacheLifetime = ($fileCacheLifetime_config !== '' && is_numeric($fileCacheLifetime_config)) ? $fileCacheLifetime_config : 86400; // 1day in Seconds for default

        return $fileCacheLifetime;
    }

    public function getFileCacheUrlPlainString($url = '') {
        $sitesClass        = $this->Core->Sites();
        $site              = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
        $curSiteKey        = (count($site)) ? $site['urlKey'] : '';
        $curSiteKeyUrlSlug = ($curSiteKey !== '') ? $curSiteKey . '/' : '';

        $curUrl = $this->Core->Request()->getCurrUrl();
        $useUrl = ($url !== '') ? $url : $curUrl;

        $url_trimmed = trim(str_replace(array($this->Core->getBaseUrl(), $curSiteKeyUrlSlug),'',$useUrl),'/');

        $fileCacheUrlPlainString = $this->Core->Helper()->createCleanString($url_trimmed);

        return $fileCacheUrlPlainString;
    }

    public function getFileCachePath() {
        $sitesClass        = $this->Core->Sites();
        $site              = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
        $curSiteKey        = (count($site)) ? $site['urlKey'] : '';

        $coreRelPath  = $this->Core->getRootPath();
        $varRelPath   = $coreRelPath . DS . 'var' . DS;
        $cacheRelPath = $varRelPath . 'cache' . DS;
        $siteCacheRelPath = $cacheRelPath . $curSiteKey . DS;

        if(!is_dir($cacheRelPath)) {
            mkdir($cacheRelPath, 0777);
        }

        if(!is_dir($siteCacheRelPath)) {
            mkdir($siteCacheRelPath, 0777);
        }

        return $siteCacheRelPath;
    }

    public function setFileCache($fileCacheUrlPlainString, $content_html) {
        $cacheRelPath = $this->getFileCachePath();

        $curTimeStamp = time();

        /** If has cache file, delete it first */

        if($this->delFileCache($fileCacheUrlPlainString)) {
            $fullCacheFileName = $cacheRelPath.$fileCacheUrlPlainString . '--' . $curTimeStamp;

            $cacheFile = fopen($fullCacheFileName, 'a') or die('<pre>Cache File Error: Unable to open file ' . $fullCacheFileName . '!');

            $cacheFile_prefix = '<!-- Cached Content Start -- Created: ' . date($this->Core->i18n()->getDateTimeFormat() , $curTimeStamp) . ' -->' . PHP_EOL;
            $cacheFile_suffix = PHP_EOL . '<!-- Cached Content End -->' . PHP_EOL;

            $cacheFile_content = $cacheFile_prefix . $this->minimizeHtml($content_html) . $cacheFile_suffix;

            fwrite($cacheFile, $cacheFile_content);
            fclose($cacheFile);
        }

        return $content_html;
    }

    public function getFileCache($fileCacheUrlPlainString) {
        $fileCache = '';
        $curTimeStamp = time();

        $cacheRelPath = $this->getFileCachePath();

        $it = new FilesystemIterator($cacheRelPath);
        foreach ($it as $fileinfo) {
            if($fileinfo->isDir()) { continue; }

            $fullFilePath = $fileinfo->getPathname();

            $file = $fileinfo->getFilename();
            if (strpos($file, $fileCacheUrlPlainString) !== false) {
                $fileTimeStamp = str_replace($fileCacheUrlPlainString . '--', '', $file);

                if($this->Core->Helper()->isValidTimeStamp($fileTimeStamp) && $fileTimeStamp <= $curTimeStamp) {
                    $fileCache = file_get_contents($fullFilePath);
                } else {
                    $this->delFileCache($fileCacheUrlPlainString);
                }

            }
        }

        return $fileCache;
    }

    public function delFileCache($fileCacheUrlPlainString) {
        $cacheRelPath = $this->getFileCachePath();

        $it = new FilesystemIterator($cacheRelPath);
        foreach ($it as $fileinfo) {
            if($fileinfo->isDir()) { continue; }

            $fullFilePath = $fileinfo->getPathname();

            /** If del all cache, delete all Files */
            if($fileCacheUrlPlainString === 'all') {
                unlink($fullFilePath);
            } else {
                $file = $fileinfo->getFilename();
                if (strpos($file, $fileCacheUrlPlainString) !== false) {
                    unlink($fullFilePath);
                }
            }
        }

        return true;
    }

    /** Get HTML Output */

    public function getOutput()
    {
        $isAjax = $this->Core->Request()->isAjax();

        /** @var $accClass Acc */
        $accClass = $this->modelClass('Acc');
        $isUserLoggedIn = (is_object($accClass) && $accClass->userLoggedIn());

        if($isAjax){
            if (!headers_sent()) {
                header('Content-Type: application/json');
            }
        }

        $forceHeaderCache = $this->forceHeaderCache();

        $noCache = (($isAjax || $isUserLoggedIn) && !$forceHeaderCache);

        if($noCache) {
            if (!headers_sent()) {
                header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
                header("Cache-Control: post-check=0, pre-check=0", false);
                header("Pragma: no-cache");
            }
        }

        $getModelKey      = $this->getModelKey();
        $getControllerKey = $this->getControllerKey();
        $getActionKey     = $this->getActionKey();

        if ($isAjax || $isUserLoggedIn) {
            $this->setForceNoFileCache();
        }

        /** Get Model Content */
        if($this->useFileCache() && !$this->forceNoFileCache()) {
            $getFileCacheUrlPlainString = $this->getFileCacheUrlPlainString();

            $getFileCache = $this->getFileCache($getFileCacheUrlPlainString);

            if($getFileCache !== '') {
                $getContentByController = $getFileCache;
            } else {
                $getContentByController = $this->getContentByMVC($getModelKey, $getControllerKey, $getActionKey);

                $this->setFileCache($getFileCacheUrlPlainString,$getContentByController);
            }
        } else {
            $getContentByController = $this->getContentByMVC($getModelKey, $getControllerKey, $getActionKey);
        }

        /** Set Content */
        $this->setContent($getModelKey, $this->Core->getNote() . $getContentByController);

        global $Core, $Mvc;
        $Mvc = $this;
        $Core = $Mvc->Core;

        $tplFilePath = $this->getThemeTplRelPath().DS.'tpl.php';

        /** If Ajax Output or TPL File does not exists, show content only */
        if($isAjax || !file_exists($tplFilePath)) {

            ob_start('ob_gzhandler');

            $model_html = $this->getContent();

            /** Check if Minimal is trying to use */
            if($this->getThemeName() == 'minimal_bs') {
                $minimalBsTheme_html = $this->getMinimalBsThemeHtml();

                /** Set Placeholder */
                $mtr_search = array(
                    '{{currLang}}',
                    '{{metaTitle}}',
                    '{{metaDescription}}',
                    '{{pageHeadMeta}}',
                    '{{pageAfterBodyStart}}',
                    '{{pageTitle}}',
                    '{{content}}',
                    '{{pageBeforeBodyEnd}}',
                );

                $mtr_replace = array(
                    $this->Core->i18n()->getCurrLang(),
                    $this->getMetaTitle(),
                    $this->getDescription(),
                    $this->getPageHeadMeta(),
                    $this->getPageAfterBodyStart(),
                    $this->getPageTitle(),
                    $this->getContent(),
                    $this->getPageBeforeBodyEnd()
                );

                /** Replace Placeholder */
                $minimalBsTheme_html = str_replace($mtr_search,$mtr_replace,$minimalBsTheme_html);

                /** Minify HTML */
                $minimalBsTheme_html = $this->minimizeHtml($minimalBsTheme_html);

                /** Set Output HTML */
                $model_html = $minimalBsTheme_html;
            }
        }
        else {
            try {
                ob_start();
                require($tplFilePath);
                $model_html = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $model_html = $e->getMessage();
            }
        }

        return $model_html;
    }
}