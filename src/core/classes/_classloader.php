<?php
/**
 * Class Loader
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * Include Core Class
 */
include_once('class.core.php');

/**
 * Include all Main Classes
 */
$classFiles = scandir(JF_ROOT_DIR . DS . 'core' . DS . 'classes' . DS);
foreach($classFiles as $classFile) {
    if($classFile != "." && $classFile != "..") {
        $fileComponents = preg_split('/\./', $classFile, -1, PREG_SPLIT_NO_EMPTY);
        if($fileComponents[0] == 'class' && isset($fileComponents[2]) && $fileComponents[2] == 'php' && $fileComponents[1] != 'core') {
            include_once($classFile);
        }
    }
}

/**
 * Include all Helper Classes
 */
$helperRootDir = JF_ROOT_DIR . DS . 'core' . DS . 'classes' . DS . 'helper';
$helperClasses = scandir($helperRootDir);
foreach($helperClasses as $classFile) {
    if($classFile != "." && $classFile != "..") {
        $fileComponents = preg_split('/\./', $classFile, -1, PREG_SPLIT_NO_EMPTY);
        if($fileComponents[0] == 'class' && isset($fileComponents[2]) && $fileComponents[2] == 'php' && $fileComponents[1] != 'core') {
            include_once($helperRootDir . DS . $classFile);
        }
    }
}

/**
 * Include all Plugin Classes
 */
$pluginsRootDir = JF_ROOT_DIR . DS . 'core' . DS . 'plugins' . DS;
$pluginFolders = scandir($pluginsRootDir);
foreach($pluginFolders as $pluginFolder) {
    if($pluginFolder != "." && $pluginFolder != ".." && is_dir($pluginsRootDir . $pluginFolder . DS . 'classes' . DS)) {
        $pluginClasses = scandir($pluginsRootDir . $pluginFolder . DS . 'classes' . DS);
        foreach($pluginClasses as $classFile) {
            if($classFile != "." && $classFile != "..") {
                $fileComponents = preg_split('/\./', $classFile, -1, PREG_SPLIT_NO_EMPTY);
                if($fileComponents[0] == 'class' && isset($fileComponents[2]) && $fileComponents[2] == 'php' && $fileComponents[1] != 'core') {
                    include_once($pluginsRootDir . $pluginFolder . DS . 'classes' . DS . $classFile);
                }
            }
        }
    }
}

/**
 * Include all Model Classes
 */
$modelsRootDir = JF_ROOT_DIR . DS . 'core' . DS . 'models' . DS;
$modelFolders = scandir($modelsRootDir);
foreach($modelFolders as $modelFolder) {
    if($modelFolder != "." && $modelFolder != ".." && is_dir($modelsRootDir . $modelFolder . DS . 'classes' . DS)) {
        $modelClasses = scandir($modelsRootDir . $modelFolder . DS . 'classes' . DS);
        foreach($modelClasses as $classFile) {
            if($classFile != "." && $classFile != "..") {
                $fileComponents = preg_split('/\./', $classFile, -1, PREG_SPLIT_NO_EMPTY);
                if($fileComponents[0] == 'class' && isset($fileComponents[2]) && $fileComponents[2] == 'php' && $fileComponents[1] != 'core') {
                    include_once($modelsRootDir . $modelFolder . DS . 'classes' . DS . $classFile);
                }
            }
        }
    }
}