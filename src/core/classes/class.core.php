<?php
/**
 * Core Class, contains all necessary Basic Methods and Class Calls
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** Class Core*/
class Core
{
    private $_version = '5.3.0';
    private $rootPath = null;

    /**
     * Set Core Classes
     *
     * @var $helper Helper
     * @var $hooks Hooks
     * @var $db Db
     * @var $config Config
     * @var $request Request
     * @var $i18n i18n
     * @var $sites Sites
     * @var $plugins Plugins
     * @var $mvc Mvc
     * @var $cron Cron
     * @var $shell Shell
     * @var $updater Updater
     */
    private
        $helper  = null,
        $hooks   = null,
        $db      = null,
        $config  = null,
        $request = null,
        $i18n    = null,
        $sites   = null,
        $plugins = null,
        $mvc     = null,
        $cron    = null,
        $shell   = null,
        $updater = null;

    /**
     * Core constructor.
     */
    function __construct()
    {
        /** Init Core Error Handling */
        $this->_initErrorHandling();

        /** Init Core Classes */
        $this->_initCoreClasses();

        /** Init Modules */
        /** @var $plugins Plugins */
        $plugins = $this->Plugins();
        $plugins->initPlugins();

        /** Set Session Information */
        $this->_setSessionInfo();

        /** After Construct */
        $this->_mayRedirect();


    }

    private function _initErrorHandling() {
        $varPath         = $this->getRootPath() . DS . 'var' . DS;
        $logPath         = $varPath . 'log' . DS;
        if(!is_dir($varPath)) {
            $oldmask = umask(0);
            mkdir($varPath, 0777);
        }

        if(!is_dir($logPath)) {
            $oldmask = umask(0);
            mkdir($logPath, 0777);
        }

        error_reporting(E_ALL);

        $displayErrors = (defined('DEV_MODE') && is_bool(DEV_MODE) && DEV_MODE !== false);

        ini_set('display_errors', $displayErrors);

        ini_set('log_errors', true);
        ini_set('error_log', $logPath . 'php-error.log');

        set_error_handler(function($code, $string, $file, $line){
            throw new ErrorException($string, null, $code, $file, $line);
        });

        register_shutdown_function(function(){
            $is_cli = (!isset($_SERVER['SERVER_SOFTWARE']) && (php_sapi_name() == 'cli' || (array_key_exists('argv',$_SERVER) && is_numeric($_SERVER['argc']) && $_SERVER['argc'] > 0)));
            $error = error_get_last();
            if(null !== $error && $error['type'] === E_ERROR)
            {
                $this->Log($error,'core_error',true);
                $errMsg = 'An Error occurred, see Core Error Log for more info!';

                if(!$is_cli) {
                    $output = '<pre>' . $errMsg . '</pre>';

                    $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                    if (strpos(strtolower($actual_link), 'ajax') !== false) {
                        $errArr = array('success' => false, 'message' => $errMsg);
                        $output = json_encode($errArr);
                    }
                } else {
                    $output = $errMsg . PHP_EOL;
                }

                die($output);
            }
        });
    }

    public function reInitErrorHandling() {
        $this->_initErrorHandling();
    }

    /** Init Core Classes */
    private function _initCoreClasses() {
        $coreClasses = array(
            'helper',
            'hooks',
            'db',
            'config',
            'request',
            'i18n',
            'sites',
            'plugins',
            'mvc',
            'cron',
            'shell',
            'updater',
        );

        foreach($coreClasses as $coreClassName) {
            $thisPrivateCoreClassVar = $this->$coreClassName;

            if($thisPrivateCoreClassVar === null) {
                $className = ucfirst($coreClassName);
                if(class_exists($className)) {
                    $this->$coreClassName = new $className($this);
                }
            }
        }
    }

    private function _setSessionInfo() {
        /** @var $requestClass Request */
        $Request = $this->Request();

        /** Set Session Info */
        $_SESSION['baseUrl'] = $this->getBaseUrl();
        $_SESSION['JF_REFERRER'] = $Request->getCurrUrl();
    }

    private function _mayRedirect() {

        /** @var $requestClass Request */
        $requestClass = $this->Request();

        /** @var $helperClass Helper */
        $helperClass = $this->Helper();

        /** @var $mvcClass Mvc */
        $mvcClass = $this->Mvc();

        /** @var $sitesClass Sites */
        $sitesClass = $this->Sites();

        /** @var $accClass Acc */
        $accClass = $mvcClass->modelClass('Acc');


        if(!$helperClass->isCli() && strpos($_SERVER['REQUEST_URI'],'cron.php') === false) {
            $site = (is_object($sitesClass)) ? $sitesClass->getSite() : array();

            if(!count($site)) {
                $allowedModelKeys = array('index','debug','system');
                if(is_object($accClass)) { $allowedModelKeys[] = 'acc'; }
                if(!in_array($mvcClass->getModelKey(),$allowedModelKeys)) {
                    $requestClass->redirect($this->getBaseUrl());
                }
            } else {
                if(is_object($accClass)) {
                    $site = $sitesClass->getSite();
                    $user = $accClass->getUser();

                    if(isset($user['sites']) && is_array($user['sites']) && count($user['sites']) && !in_array($site['id'], $user['sites'])) {
                        $requestClass->redirect($this->getBaseUrl());
                    }
                }
            }
        }
    }

    /** Acc Public Access Methods */

    public function getPublicModelKeys() {
        return array(
            'index',
            'noroute'
        );
    }

    public function getPublicControllerKeys() {
        return array(
            'index'
        );
    }

    public function getPublicActionKeys() {
        return array(
            'index'
        );
    }

    /** Mvc Output Methods */

    public function setMvcPageHeadMeta()
    {
        $coreUrl       = $this->getCoreUrl();
        $componentsUrl = $coreUrl . '/components';

        /** Set Core Head Meta */
        $mvcPageHeadMeta = PHP_EOL . '<!-- J•Frame Core - Head Meta START -->';

        $mvcPageHeadMeta .= PHP_EOL;
        $mvcPageHeadMeta .= '<script type="text/javascript">';

        $mvcPageHeadMeta .= PHP_EOL;
        $mvcPageHeadMeta .= '    var thisBaseUrl = \'' . $this->getBaseUrl() . '\';';

        $mvcPageHeadMeta .= PHP_EOL;
        $mvcPageHeadMeta .= '</script>';

        $mvcPageHeadMeta .= PHP_EOL;
        $mvcPageHeadMeta .= '<link rel="stylesheet" type="text/css" href="' . $componentsUrl . '/bs-datepicker/css/bootstrap-datepicker.min.css" />';

        $mvcPageHeadMeta .= PHP_EOL;
        $mvcPageHeadMeta .= '<link rel="stylesheet" type="text/css" href="' . $componentsUrl . '/dataTables/css/jquery.dataTables.min.css" />';
        $mvcPageHeadMeta .= PHP_EOL;
        $mvcPageHeadMeta .= '<link rel="stylesheet" type="text/css" href="' . $componentsUrl . '/dataTables/css/dataTables.bootstrap.min.css" />';

        $mvcPageHeadMeta .= PHP_EOL;
        $mvcPageHeadMeta .= '<link rel="stylesheet" type="text/css" href="' . $componentsUrl . '/font-awesome-4.7.0/css/font-awesome.min.css" />';

        $mvcPageHeadMeta .= PHP_EOL;
        $mvcPageHeadMeta .= '<link rel="stylesheet" type="text/css" href="' . $componentsUrl . '/core/core.css" />';

        $mvcPageHeadMeta .= PHP_EOL . '<!-- J•Frame Core - Head Meta END -->';
        $mvcPageHeadMeta .= PHP_EOL;

        /** Set i18n Head Meta */
        $mvcPageHeadMeta .= PHP_EOL . '<!-- J•Frame i18n - Head Meta START -->';

        $mvcPageHeadMeta .= PHP_EOL;
        $mvcPageHeadMeta .= '<script type="text/javascript">';

        if($this->i18n()->isMultilang()) {
            $mvcPageHeadMeta .= PHP_EOL;
            $mvcPageHeadMeta .= '    var thisCurrLang = \'' . $this->i18n()->getCurrLang() . '\';';
            $mvcPageHeadMeta .= PHP_EOL;
            $mvcPageHeadMeta .= '    var getDefaultLang = \'' . $this->i18n()->getDefaultLang() . '\';';
        } else {
            $mvcPageHeadMeta .= PHP_EOL;
            $mvcPageHeadMeta .= '    var thisCurrLang = \'' . $this->i18n()->getDefaultLang() . '\';';
        }

        $mvcPageHeadMeta .= PHP_EOL;
        $mvcPageHeadMeta .= '</script>';

        $mvcPageHeadMeta .= PHP_EOL . '<!-- J•Frame i18n - Head Meta END -->';
        $mvcPageHeadMeta .= PHP_EOL;

        return $mvcPageHeadMeta;
    }

    public function setMvcPageBeforeBodyEnds()
    {
        $coreUrl       = $this->getCoreUrl();
        $componentsUrl = $coreUrl . '/components';

        $mvcPageBeforeBodyEnd = PHP_EOL . '<!-- J•Frame Core - Before Body Ends START -->';

        $mvcPageBeforeBodyEnd .= PHP_EOL;
        $mvcPageBeforeBodyEnd .= '<!-- jQuery first! -->';
        $mvcPageBeforeBodyEnd .= PHP_EOL;
        $mvcPageBeforeBodyEnd .= '<script src="' . $this->Config()->get('jquery_cdn') . '"></script>';
        $mvcPageBeforeBodyEnd .= PHP_EOL;
        $mvcPageBeforeBodyEnd .= '<!-- Bootstrap Datepicker Scripts -->';
        $mvcPageBeforeBodyEnd .= PHP_EOL;
        $mvcPageBeforeBodyEnd .= '<script src="' . $componentsUrl . '/bs-datepicker/js/bootstrap-datepicker.min.js"></script>';
        $mvcPageBeforeBodyEnd .= PHP_EOL;
        if ($this->i18n()->getCurrLang() == 'de') {
            $mvcPageBeforeBodyEnd .= '<script src="' . $componentsUrl . '/bs-datepicker/locales/bootstrap-datepicker.de.min.js" charset="UTF-8"></script>';
            $mvcPageBeforeBodyEnd .= PHP_EOL;
        }
        $mvcPageBeforeBodyEnd .= '<!-- Additional Scripts -->';
        $mvcPageBeforeBodyEnd .= PHP_EOL;
        $mvcPageBeforeBodyEnd .= '<script src="' . $componentsUrl . '/pdfmake/pdfmake.min.js"></script>';
        $mvcPageBeforeBodyEnd .= PHP_EOL;
        $mvcPageBeforeBodyEnd .= '<script src="' . $componentsUrl . '/pdfmake/vfs_fonts.js"></script>';
        $mvcPageBeforeBodyEnd .= PHP_EOL;
        $mvcPageBeforeBodyEnd .= '<!-- DataTables Scripts -->';
        $mvcPageBeforeBodyEnd .= PHP_EOL;
        $mvcPageBeforeBodyEnd .= '<script src="' . $componentsUrl . '/dataTables/js/jquery.dataTables.min.js"></script>';
        $mvcPageBeforeBodyEnd .= PHP_EOL;
        $mvcPageBeforeBodyEnd .= '<script src="' . $componentsUrl . '/dataTables/js/dataTables.bootstrap.min.js"></script>';
        $mvcPageBeforeBodyEnd .= PHP_EOL;
        $mvcPageBeforeBodyEnd .= '<script src="' . $componentsUrl . '/dataTables/js/dataTables.buttons.min.js"></script>';
        $mvcPageBeforeBodyEnd .= PHP_EOL;
        $mvcPageBeforeBodyEnd .= '<script src="' . $componentsUrl . '/dataTables/js/buttons.html5.min.js"></script>';
        $mvcPageBeforeBodyEnd .= PHP_EOL;
        $mvcPageBeforeBodyEnd .= '<script src="' . $componentsUrl . '/dataTables/js/buttons.print.min.js"></script>';
        $mvcPageBeforeBodyEnd .= PHP_EOL;
        $mvcPageBeforeBodyEnd .= '<!-- Hyphenator Scripts -->';
        $mvcPageBeforeBodyEnd .= PHP_EOL;
        $mvcPageBeforeBodyEnd .= '<script src="' . $componentsUrl . '/hyphenator/Hyphenator.js"></script>';
        $mvcPageBeforeBodyEnd .= PHP_EOL;
        $mvcPageBeforeBodyEnd .= '<!-- jComponents Scripts -->';
        $mvcPageBeforeBodyEnd .= PHP_EOL;
        $mvcPageBeforeBodyEnd .= '<script src="' . $componentsUrl . '/jBsAlerts/jquery.jBsAlerts.min.js"></script>';
        $mvcPageBeforeBodyEnd .= PHP_EOL;
        $mvcPageBeforeBodyEnd .= '<!-- Main Script -->';
        $mvcPageBeforeBodyEnd .= PHP_EOL;
        $mvcPageBeforeBodyEnd .= '<script src="' . $componentsUrl . '/core/core.js"></script>';

        $mvcPageBeforeBodyEnd .= PHP_EOL;
        $mvcPageBeforeBodyEnd .= PHP_EOL . '<!-- J•Frame Core - Before Body Ends END -->';
        $mvcPageBeforeBodyEnd .= PHP_EOL;

        return $mvcPageBeforeBodyEnd;
    }
    
    /**
     * Core Methods
     */

    public function getVersion() {
        return $this->_version;
    }

    public function getDbVersion() {
        $dbVersion_option = $this->Config()->get('core_version',0);
        $dbVersion = ($dbVersion_option !== '') ? $dbVersion_option : 0;
        return $dbVersion;
    }

    /** Update Methods */
    public function _update100($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '1.0.0';
        $return['changelog'] = 'Implemented Client Meltdown to Core';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update200($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '2.0.0';
        $return['changelog'] = 'Implemented Cron Functionality to Core';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update300($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '3.0.0';
        $return['changelog'] = 'Changed System Options Table Entry for Update equality';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {

            $pipVersionEntry_exits = false;

            $check_for_pipVersionEntry_SQL = 'SELECT  count(*) as "count" FROM `' . DB_TABLE_PREFIX . 'system_config` WHERE `key` = "pip_version";';

            try {
                $result = $this->Db()->fromDatabase($check_for_pipVersionEntry_SQL, '@simple');
                if ($result !== 0) {
                    $pipVersionEntry_exits = true;
                    $return['success'] = false;
                } else {
                    $return['success'] = true;
                }
            } catch (Exception $e) {
                $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
            }

            if ($pipVersionEntry_exits) {
                try {
                   $remove_pipVersionEntry_SQL = 'DELETE FROM `' . DB_CONN_DBNAME . '`.`' . DB_TABLE_PREFIX . 'system_config` WHERE `key` = "pip_version" LIMIT 1';

                    $sqlResult = $this->Db()->toDatabase($remove_pipVersionEntry_SQL);

                    if (!$sqlResult) {
                        // Error
                        $return['sqlResult'] = $sqlResult;
                    } else {
                        $return['success'] = true;
                    }
                } catch (Exception $e) {
                    $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
                }
            }
        }

        if($return['success']) {
            unset($return['error']);
        }

        return $return;
    }

    public function _update314($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '3.1.4';
        $return['changelog'] = 'Changed from PIP System to CFM System. Outsourced almost all PITs Stuff from Core.';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update315($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '3.1.5';
        $return['changelog'] = 'Added Changelog Info to Update Methods (Core and Models) for Future availability in Updater';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update316($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '3.1.6';
        $return['changelog'] = 'Fixed Updater init-check for updates and added Date / DateTime Format to i18n.';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update317($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '3.1.7';
        $return['changelog'] = 'Added Search in JS Files for missing Translations for i18n.';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update318($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '3.1.8';
        $return['changelog'] = 'Component Core JS Init Action - Fixes and Cleanup';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update319($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '3.1.9';
        $return['changelog'] = 'Added "isValidTimestamp" Method to Core Helper';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update320($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '3.2.0';
        $return['changelog'] = 'Added Changelog Methods to Core Updater and enhanced Update Methods in Core and modules for Changelog Output';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update321($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '3.2.1';
        $return['changelog'] = 'Fixes for Update All Models in Core Updater';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update322($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '3.2.2';
        $return['changelog'] = 'Helper Method "isValidTimeStamp" fixed';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update330($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '3.3.0';
        $return['changelog'] = 'Core Init Maintenance Mode Check enhanced, Core Lock File Methods added.';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update335($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '3.3.5';
        $return['changelog'] = 'CSS Animations to Core CSS added. Removed Client Menu from MVC Menu.';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update340($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '3.4.0';
        $return['changelog'] = 'Fixed Model Updates Count Check';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update350($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '3.5.0';
        $return['changelog'] = 'Renaming Options to Config systemwide, Create System Config Table from System Options Table';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {

            $systemOptionsTable_exits = false;

            $check_for_systemOptionsTable_SQL = 'SELECT  count(*) as "count" FROM `' . DB_TABLE_PREFIX . 'system_options`;';

            try {
                $result = $this->Db()->fromDatabase($check_for_systemOptionsTable_SQL, '@simple');
                if ($result >= 0) {
                    $systemOptionsTable_exits = true;
                    $return['success'] = true;
                }
            } catch (Exception $e) {
                $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
            }

            $systemConfigTable_exits = false;

            $check_for_systemConfigTable_SQL = 'SELECT  count(*) as "count" FROM `' . DB_TABLE_PREFIX . 'system_config`;';

            try {
                $result = $this->Db()->fromDatabase($check_for_systemConfigTable_SQL, '@simple');
                if ($result >= 0) {
                    $systemConfigTable_exits = true;
                    $return['success'] = true;
                }
            } catch (Exception $e) {
                $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
            }

            if (!$systemConfigTable_exits) {
                if ($systemOptionsTable_exits) {
                    /** Create Copy from Options Table and create Config Table */
                    try {
                        $crate_systemConfigTable_SQL = 'CREATE TABLE `' . DB_TABLE_PREFIX . 'system_config` LIKE `' . DB_TABLE_PREFIX . 'system_options`;';

                        $sqlResult = $this->Db()->toDatabase($crate_systemConfigTable_SQL);

                        if (!$sqlResult) {
                            // Error
                            $return['success'] = false;
                            $return['sqlResult'] = $sqlResult;
                        } else {
                            $return['success'] = true;
                        }
                    } catch (Exception $e) {
                        $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
                    }
                }
            } else {
                if ($systemOptionsTable_exits) {
                    /** Copy Data from Options Table to Config Table */
                    try {
                        $copyDataTo_systemConfigTable_SQL = 'TRUNCATE `' . DB_TABLE_PREFIX . 'system_config`; INSERT `' . DB_TABLE_PREFIX . 'system_config` SELECT * FROM `' . DB_TABLE_PREFIX . 'system_options`;';

                        $sqlResult = $this->Db()->toDatabase($copyDataTo_systemConfigTable_SQL);

                        if (!$sqlResult) {
                            // Error
                            $return['success'] = false;
                            $return['sqlResult'] = $sqlResult;
                        } else {
                            $return['success'] = true;
                        }
                    } catch (Exception $e) {
                        $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
                    }
                }
            }
        }

        if($return['success']) {
            unset($return['error']);
        }

        return $return;
    }

    public function _update360($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '3.6.0';
        $return['changelog'] = 'Add "web" Col to "system_client" Table';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {

            $table_exits = false;

            $check_for_table_exists_SQL = 'SELECT count(*) as "count" FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = "' . DB_TABLE_PREFIX . 'system_client"';

            try {
                $result = $this->Db()->fromDatabase($check_for_table_exists_SQL, '@simple');

                if ($result !== 0) {
                    $table_exits = true;
                    $return['success'] = false;
                } else {
                    $table_exits = false;
                    $return['success'] = true;
                }
            } catch (Exception $e) {
                $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
            }

            if($table_exits) {
                $web_col_exits = false;

                $check_for_web_col_SQL = 'SELECT IF(EXISTS(SELECT `id` FROM `' . DB_TABLE_PREFIX . 'system_client` WHERE (`web` IS NOT NULL) OR (`web` IS NULL) ), 1, 0) as "exists"';

                try {
                    $result = $this->Db()->fromDatabase($check_for_web_col_SQL, '@simple');

                    if ($result !== 0) {
                        $web_col_exits = true;
                        $return['success'] = true;
                    }
                } catch (Exception $e) {
                    $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
                }

                if (!$web_col_exits) {
                    try {
                        $addCol_SQL = 'ALTER TABLE `' . DB_TABLE_PREFIX . 'system_client` ADD `web` VARCHAR( 150 ) NULL DEFAULT NULL COLLATE "utf8_unicode_ci" AFTER `mobile`';

                        $sqlResult = $this->Db()->toDatabase($addCol_SQL);

                        if (!$sqlResult) {
                            // Error
                            $return['sqlResult'] = $sqlResult;
                            $return['success'] = false;
                        } else {
                            $return['success'] = true;
                        }
                    } catch (Exception $e) {
                        $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
                    }
                }
            }
        }

        if($return['success']) {
            unset($return['error']);
        }

        return $return;
    }

    public function _update362($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '3.6.2';
        $return['changelog'] = 'Changed Col "value" Type to TEXT in "system_config"';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {

            $colType = 'text';

            try {
                $checkColType_SQL = 'SELECT DATA_TYPE AS "type" FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "' . DB_TABLE_PREFIX . 'system_config" AND COLUMN_NAME = "value";';

                $result = $this->Db()->fromDatabase($checkColType_SQL, '@simple');
                if (strtolower($result) !== 'text') {
                    $colType = strtolower($result);
                    $return['success'] = false;
                } else {
                    $return['success'] = true;
                }
            } catch (Exception $e) {
                $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
            }

            if($colType !== 'text') {
                $changeCol_SQL = 'ALTER TABLE `' . DB_TABLE_PREFIX . 'system_config` CHANGE COLUMN `value` `value` TEXT NOT NULL COLLATE "utf8_unicode_ci" AFTER `key`';
                try {
                    $sqlResult = $this->Db()->toDatabase($changeCol_SQL);
                    if (!$sqlResult) {
                        // Error
                        $return['error'] = $sqlResult;
                        $return['success'] = false;
                    } else {
                        $return['success'] = true;
                    }
                } catch (Exception $e) {
                    $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
                }
            }
        }

        if($return['success']) {
            unset($return['error']);
        }

        return $return;
    }

    public function _update370($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '3.7.0';
        $return['changelog'] = 'Implemented Core Error Handling';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update375($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '3.7.5';
        $return['changelog'] = '&bull;&nbsp;Enhanced Core Error Handling.<br />&bull;&nbsp;Optimized Session Info.<br />&bull;&nbsp;Optimized Config Files.';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update380($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '3.8.0';
        $return['changelog'] = 'Create i18n Countries Table from System Countries Table';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {

            $systemCountriesTable_exits = false;

            $check_for_systemCountriesTable_SQL = 'SELECT  count(*) as "count" FROM `' . DB_TABLE_PREFIX . 'system_countries`;';

            try {
                $result = $this->Db()->fromDatabase($check_for_systemCountriesTable_SQL, '@simple');
                if ($result >= 0) {
                    $systemCountriesTable_exits = true;
                    $return['success'] = true;
                }
            } catch (Exception $e) {
                $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
            }

            $i18nCountriesTable_exits = false;

            $check_for_i18nCountriesTable_SQL = 'SELECT  count(*) as "count" FROM `' . DB_TABLE_PREFIX . 'i18n_countries`;';

            try {
                $result = $this->Db()->fromDatabase($check_for_i18nCountriesTable_SQL, '@simple');
                if ($result >= 0) {
                    $i18nCountriesTable_exits = true;
                    $return['success'] = true;
                }
            } catch (Exception $e) {
                $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
            }

            if (!$i18nCountriesTable_exits) {
                if ($systemCountriesTable_exits) {
                    /** Create Copy from Options Table and create Config Table */
                    try {
                        $crate_systemConfigTable_SQL = 'CREATE TABLE `' . DB_TABLE_PREFIX . 'i18n_countries` LIKE `' . DB_TABLE_PREFIX . 'system_countries`;';

                        $sqlResult = $this->Db()->toDatabase($crate_systemConfigTable_SQL);

                        if (!$sqlResult) {
                            // Error
                            $return['success'] = false;
                            $return['sqlResult'] = $sqlResult;
                        } else {
                            $return['success'] = true;
                        }
                    } catch (Exception $e) {
                        $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
                    }
                }
            } else {
                if ($systemCountriesTable_exits) {
                    /** Copy Data from System Countries Table to i18n Countries Table */
                    try {
                        $copyDataTo_systemConfigTable_SQL = 'TRUNCATE `' . DB_TABLE_PREFIX . 'i18n_countries`; INSERT `' . DB_TABLE_PREFIX . 'i18n_countries` SELECT * FROM `' . DB_TABLE_PREFIX . 'system_countries`;';

                        $sqlResult = $this->Db()->toDatabase($copyDataTo_systemConfigTable_SQL);

                        if (!$sqlResult) {
                            // Error
                            $return['success'] = false;
                            $return['sqlResult'] = $sqlResult;
                        } else {
                            $return['success'] = true;
                        }
                    } catch (Exception $e) {
                        $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
                    }
                }
            }
        }

        if($return['success']) {
            unset($return['error']);
        }

        return $return;
    }

    public function _update400($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '4.0.0';
        $return['changelog'] = '&bull;&nbsp;Changed "Client" to "Site" globally!<br />&bull;&nbsp;Request: Optimized URL Path and Params parsing<br />&bull;&nbsp;Mvc: Optimized Model-, Controller-, Action-Key Parsing';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update410($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '4.1.0';
        $return['changelog'] = '&bull;&nbsp;Core JS: Enhanced Manual Modal Open with Ajax Content for Post Data Action<br />&bull;&nbsp;i18n: Edit DB Translations Functionality added, Core / Model / Theme Translation File Handling added';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update420($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '4.2.0';
        $return['changelog'] = 'Renaming Modules functionality to Plugin Functionality globally!';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update450($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '4.5.0';
        $return['changelog'] = 'Added Shell Functionality';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update460($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '4.6.0';
        $return['changelog'] = 'Enhanced dataTables Javascript to support User Options for Ordering Table';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update500($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '5.0.0';
        $return['changelog'] = 'Implementet File Cache functionality in MVC';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update530($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '5.3.0';
        $return['changelog'] = 'Added Referrer Getter to Request Class and implemented setter in Core and Referrer Class';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    /** Base Methods */

    public function getBaseUrl()
    {
        $get_baseurl = $this->Config()->get('baseurl');

        $baseUrl = $get_baseurl;

        if ($this->isUrlRewrite()){
            $baseUrl = rtrim($get_baseurl, '/\\').'/';
        }
        else {
            $baseUrl = rtrim($get_baseurl, '/\\').'/index.php?path=/';
        }

        if($this->Request()->isSecure()) {
            $secureBaseUrl = str_replace('http:', 'https:', $baseUrl);
            $baseUrl = $secureBaseUrl;
        }

        return $baseUrl;
    }

    public function getRootPath()
    {
        if($this->rootPath == null){
            $this->rootPath = JF_ROOT_DIR;
        }

        return $this->rootPath;
    }

    public function getRelPath()
    {
        return str_replace(DS . 'core' . DS . 'classes', '', rtrim(dirname(__FILE__), '/\\'));
    }

    public function isUrlRewrite()
    {
        return ($this->Config()->get('urlrewrite') == '1') ? true : false;
    }

    public function getCoreUrl()
    {
        return str_replace("/index.php?path=", "", $this->getBaseUrl()).'core';
    }

    public function getCoreRelPath()
    {
        return $this->getRelPath().DS.'core';
    }

    /**
     * Logging, Lock and Notes
     */

    public function Log($message, $filename = '', $force = false)
    {
        if($this->Config()->get('enable_logging') || $this->Config()->get('debug_mode') || $force == true) {
            $varPath         = $this->getRootPath() . DS . 'var' . DS;
            $logPath         = $varPath . 'log' . DS;
            $logFileName     = ($filename != '') ? rtrim($filename, '.') : 'system';
            $logFilePathInfo = pathinfo($logFileName);
            $logFilePathName = (array_key_exists('filename', $logFilePathInfo) && $logFilePathInfo['filename'] != '') ? $logFilePathInfo['filename'] : $logFileName;
            $logFileExt      = (array_key_exists('extension', $logFilePathInfo) && $logFilePathInfo['extension'] != '') ? '.' . $logFilePathInfo['extension'] : '.log';
            $logFileFullName = $logFilePathName.$logFileExt;

            $now = new DateTime();
            $dateTime = $now->format(DateTime::ISO8601);
            $logMsg = $dateTime . ': ' . print_r($message, true) . "\n";

            if(!is_dir($varPath)) {
                $oldmask = umask(0);
                mkdir($varPath, 0777);
            }

            if(!is_dir($logPath)) {
                $oldmask = umask(0);
                mkdir($logPath, 0777);
            }

            $logfile = fopen($logPath.$logFileFullName, 'a') or die('<pre>Log File Error: Unable to open file ' . $logPath.$logFileFullName . '!');

            fwrite($logfile, $logMsg);
            fclose($logfile);

            if(((filesize($logPath.$logFileFullName) / 1024) / 1024) > 1.5) {
                @rename($logPath.$logFileFullName, $logPath.$logFileName.'.'.time().$logFileExt);
            }
        }

        return true;
    }

    public function makeLockFile($filename) {
        $return = false;

        if ($filename != '') {
            $varPath          = $this->getRootPath() . DS . 'var' . DS;
            $lockPath         = $varPath . 'lock' . DS;
            $lockFileName     = ($filename != '') ? rtrim($filename, '.') : 'lockfile';
            $lockFilePathInfo = pathinfo($lockFileName);
            $lockFilePathName = (array_key_exists('filename', $lockFilePathInfo) && $lockFilePathInfo['filename'] != '') ? $lockFilePathInfo['filename'] : $lockFileName;
            $lockFileExt      = (array_key_exists('extension', $lockFilePathInfo) && $lockFilePathInfo['extension'] != '') ? '.' . $lockFilePathInfo['extension'] : '.lock';
            $lockFileFullName = $lockFilePathName.$lockFileExt;

            $lockMsg = '';

            if (!is_dir($varPath)) {
                $oldmask = umask(0);
                mkdir($varPath, 0777);
            }

            if (!is_dir($lockPath)) {
                $oldmask = umask(0);
                mkdir($lockPath, 0777);
            }

            $lockFile = fopen($lockPath . $lockFileFullName, 'a') or die('<pre>Lock File Error: Unable to open file ' . $lockPath . $lockFileFullName . '!');

            fwrite($lockFile, $lockMsg);
            fclose($lockFile);

            if (file_exists($lockPath . $lockFileFullName)) {
                $return = true;
            }
        }

        return $return;
    }

    public function deleteLockFile($filename) {
        $return = false;
        if($filename != '') {
            $varPath          = $this->getRootPath() . DS . 'var' . DS;
            $lockPath         = $varPath . 'lock' . DS;
            $lockFileFullName = $filename;

            if(file_exists($lockPath.$lockFileFullName)) {
                if(unlink($lockPath.$lockFileFullName)) {
                    $return = true;
                }
            }
        }

        return $return;
    }

    public function setNote($note, $type = 'info', $kind = 'plain', $title = null)
    {
        $n_types = array(
            'info',
            'success',
            'warning',
            'danger'
        );

        $n_kinds = array(
            'plain',
            'bs-alert',
            'bs-modal'
        );

        $n_typeTitles = array(
            'info'    => $this->i18n()->translate('Info'),
            'success' => $this->i18n()->translate('Erfolg'),
            'warning' => $this->i18n()->translate('Warnung'),
            'danger'  => $this->i18n()->translate('Fehler!!')
        );

        /** set defaults **/
        if($type != '' && !in_array($type, $n_types)) {
            $type = 'info';
        }

        if($kind != '' && !in_array($kind, $n_kinds)) {
            $kind = 'plain';
        }

        if($title === null && $title != '') {
            $title = $n_typeTitles[$type];
        }

        /** return false if note not set **/
        if($note == '') { return false; }

        $noteHash = md5($type . $kind . $title . $note); /** Unique Notes only */

        $_SESSION['note'][$noteHash] = array(
            'type' => $type,
            'kind' => $kind,
            'title' => $title,
            'note' => $note
        );
        return true;
    }

    public function getNote()
    {
        $notes_html = '';

        if(isset($_SESSION['note']) && is_array($_SESSION['note']) && count($_SESSION['note']) && !$this->Request()->isAjax()) {
            foreach($_SESSION['note'] as $noteHash => $note) {

                $type  = $note['type'];
                $kind  = $note['kind'];
                $title = $note['title'];
                $note  = $note['note'];

                if($kind == 'plain') {
                    $notes_html .= $note;
                }

                if($kind == 'bs-alert') {
                    $title = ($title != '') ? '<strong>' . $title . '</strong><br />' : '';
                    $autoclose_class = ($type != 'danger' && $type != 'warning') ? ' autoclose' : '';
                    $notes_html .= '
						<div class="alert alert-' . $type . ' alert-dismissible' . $autoclose_class . '" id="notes_alert_' . $noteHash . '">
							<button type="button" class="close" data-dismiss="alert" aria-label="Schließen"><span aria-hidden="true">&times;</span></button>
							' . $title . $note . '
						</div>
					';
                }

                if($kind == 'bs-modal') {
                    $title = ($title != '') ? $title : '.';
                    $notes_html .= '
						<div class="modal fade autoshow" id="notes_modal_' . $noteHash . '" tabindex="-1" role="dialog" aria-labelledby="actionResult_modalLabel_' . $noteHash . '">
						    <div class="modal-dialog" role="document">
						        <div class="modal-content">
						            <div class="modal-header">
						                <button type="button" class="close" data-dismiss="modal" aria-label="Schließen"><span aria-hidden="true">&times;</span></button>
						                <h4 class="modal-title text-'. $type .'" id="actionResult_modalLabel">' . $title . '</h4>
						            </div>
						            <div class="modal-body">
									' . $note . '
						            </div>
						            <div class="modal-footer">
						                <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
						            </div>
						        </div>
						    </div>
						</div>
            		';
                }

            }
            unset($_SESSION['note']);
        }
        return $notes_html;
    }

    /**
     * Core Classes Methods
     */

    public function Helper()
    {
        return $this->helper;
    }

    public function Hooks()
    {
        return $this->hooks;
    }

    public function Db()
    {
        return $this->db;
    }

    public function Config()
    {
        return $this->config;
    }

    public function Request()
    {
        return $this->request;
    }

    public function i18n()
    {
        return $this->i18n;
    }

    public function Sites()
    {
        return $this->sites;
    }

    public function Plugins()
    {
        return $this->plugins;
    }

    public function Mvc()
    {
        return $this->mvc;
    }

    public function Cron()
    {
        return $this->cron;
    }

    public function Shell()
    {
        return $this->shell;
    }

    public function Updater()
    {
        return $this->updater;
    }
}