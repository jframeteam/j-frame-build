<?php
/**
 * Updater Helper
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** Class Updater */
class Updater
{
    /** @var $Core Core */
    private $Core;

    /**
     * Hooks constructor.
     * @param $Core Core
     */
    public function __construct($Core)
    {
        $this->Core = $Core;

        /** @var $Helper Helper */
        $Helper = $this->Core->Helper();

        if(!$Helper->isCli()) {
            /** Initially Check for Updates */
            $this->_init_updates_check();
        }
    }

    private function _init_updates_check() {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();
        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');
        /** @var $sitesClass Sites */
        $sitesClass = $this->Core->Sites();
        $site = (is_object($sitesClass)) ? $sitesClass->getSite() : array();

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        if($isSu && count($site)) {

            $countUpdates_all = $this->countUpdates('all');
            $updatesAvailable = false;
            if(is_array($countUpdates_all) && count($countUpdates_all)) {
                foreach ($countUpdates_all as $what => $upToDate) {
                    if (!$upToDate) {
                        $updatesAvailable = true;
                    }
                }
            }

            $updaterUrl = $Mvc->getModelUrl('system') . '/updater';

            if (
                $updatesAvailable &&
                strpos($this->Core->Request()->getCurrUrl(), $updaterUrl) === false &&
                !$this->Core->Request()->isAjax()
            ) {
                $note = $this->Core->i18n()->translate('Es sind Updates verfügbar!');
                $note .= ' <a href="' . $updaterUrl . '" class="btn btn-default btn-xs">' . $this->Core->i18n()->translate('zur Versions-Übersicht') . '</a>';

                $type  = 'warning';
                $kind  = 'bs-alert';

                $title = $this->Core->i18n()->translate('Updater Info');

                $this->Core->setNote($note, $type, $kind, $title);
            }
        }
    }

    public function checkForUpdates($what = null, $param = null) {
        $result = array();

        if($what !== null) {
            set_time_limit(0);
            if($what == 'core' || $what == 'all') {
                $coreClass = $this->Core;
                if(is_object($coreClass)) {
                    $result['core'] = $this->_checkAction($coreClass);
                }
            }
            if($what == 'model' || $what == 'all') {
                /** @var $Mvc Mvc */
                $Mvc = $this->Core->Mvc();

                $models = $Mvc->getModels();
                if (is_array($models)) {
                    if ($what != 'all') {
                        if ($param !== null) {
                            if (array_key_exists($param, $models)) {
                                $modelClass = $Mvc->modelClass($param);
                                if(is_object($modelClass)) {
                                    $result['model'][$param] = $this->_checkAction($modelClass);
                                }
                            } else {
                                $result['model'][$param]['success'] = false;
                            }
                        } else {
                            foreach($models as $modelKey => $model) {
                                $modelClass = $Mvc->modelClass($modelKey);
                                if(is_object($modelClass)) {
                                    $result['model'][$modelKey] = $this->_checkAction($modelClass);
                                }
                            }
                        }
                    } else {
                        foreach($models as $modelKey => $model) {
                            $modelClass = $Mvc->modelClass($modelKey);
                            if(is_object($modelClass)) {
                                $result['model'][$modelKey] = $this->_checkAction($modelClass);
                            }
                        }
                    }
                } else {
                    $result['model']['success'] = false;
                }
            }
        }

        return $result;
    }

    public function countUpdates($what = null) {
        $result = array();

        if($what !== null) {
            set_time_limit(0);
            if($what == 'core' || $what == 'all') {
                $core_upToDate = false;
                $coreClass = $this->Core;
                if(is_object($coreClass)) {
                    $result_core = $this->_checkAction($coreClass);
                    $core_upToDate = (array_key_exists('up-to-date',$result_core) && is_bool($result_core['up-to-date']) && $result_core['up-to-date']);
                    $result['core'] = $core_upToDate;
                }
            }
            if($what == 'model' || $what == 'all') {
                /** @var $Mvc Mvc */
                $Mvc = $this->Core->Mvc();

                $models_upToDate = false;

                $modelsNonUpToDateCount = 0;

                $models = $Mvc->getModels();
                if (is_array($models)) {
                    foreach($models as $modelKey => $model) {
                        $modelClass = $Mvc->modelClass($modelKey);
                        if(is_object($modelClass)) {
                            $result_model = $this->_checkAction($modelClass);
                            $model_version   = (array_key_exists('version',$result_model)) ? $result_model['version'] : 0;
                            $model_dbVersion = (array_key_exists('dbVersion',$result_model)) ? $result_model['dbVersion'] : 0;
                            $model_upToDate  = (array_key_exists('up-to-date',$result_model) && is_bool($result_model['up-to-date']) && $result_model['up-to-date']);
                            if($model_version > 0 && !$model_upToDate) {
                                $modelsNonUpToDateCount++;
                            }
                        }
                    }
                }

                $models_upToDate = ($modelsNonUpToDateCount === 0);

                $result['models'] = $models_upToDate;
            }
        }

        return $result;
    }

    public function doUpdate($what = null, $param = null) {
        $result = array();

        if($what !== null) {
            set_time_limit(0);
            if($what == 'core' || $what == 'all') {
                $result['core'] = $this->_updateCore();
            }
            if($what == 'model' || $what == 'all') {
                /** @var $Mvc Mvc */
                $Mvc = $this->Core->Mvc();

                $models = $Mvc->getModels();
                if (is_array($models)) {
                    if ($what != 'all') {
                        if ($param !== null) {
                            if (array_key_exists($param, $models)) {
                                $result['model'][$param] = $this->_updateModel($param);
                            } else {
                                $result['model'][$param]['success'] = false;
                            }
                        } else {
                            $result['model']['success'] = false;
                        }
                    } else {
                        foreach($models as $modelKey => $model) {
                            $modelClass = $Mvc->modelClass($modelKey);
                            if(is_object($modelClass) && method_exists($modelClass, 'getVersion') && method_exists($modelClass, 'getDbVersion')) {
                                $classVersion   = $modelClass->getVersion();
                                $classDbVersion = $modelClass->getDbVersion();

                                if ($classDbVersion < $classVersion) {
                                    $result['model'][$modelKey] = $this->_updateModel($modelKey);
                                }
                            }
                        }
                    }
                } else {
                    $result['model']['success'] = false;
                }
            }
        }

        return $result;
    }

    public function getChangelog($what = null, $param = null) {
        $result = array();

        if($what !== null) {
            set_time_limit(0);
            if($what == 'core' || $what == 'all') {
                $result['core'] = $this->_changelogCore();
            }
            if($what == 'model' || $what == 'all') {
                /** @var $Mvc Mvc */
                $Mvc = $this->Core->Mvc();

                $models = $Mvc->getModels();
                if (is_array($models)) {
                    if ($what != 'all') {
                        if ($param !== null) {
                            if (array_key_exists($param, $models) && is_object($Mvc->modelClass($param))) {
                                $result['model'][$param] = $this->_changelogModel($param);
                            }
                        } else {
                            $result['model'] = array();
                        }
                    } else {
                        foreach($models as $modelKey => $model) {
                            if(!is_object($Mvc->modelClass($modelKey))) { continue; }
                            $result['model'][$modelKey] = $this->_changelogModel($modelKey);
                        }
                    }
                } else {
                    $result['model'] = array();
                }
            }
        }

        return $result;
    }

    private function _updateCore() {
        $result = array();
        $result['success'] = false;

        $coreClass = $this->Core;

        if(is_object($coreClass)) {
            $result = $this->_updateAction($coreClass);
        }

        return $result;
    }

    private function _updateModel($key = '') {
        $result = array();
        $result['success'] = false;

        $modelClass = $this->Core->Mvc()->modelClass($key);

        if(is_object($modelClass)) {
            $result = $this->_updateAction($modelClass);
        }

        return $result;
    }

    private function _updateAction($updateClass) {
        $result = array();
        $result['success'] = false;

        if(is_object($updateClass) && method_exists($updateClass, 'getVersion') && method_exists($updateClass, 'getDbVersion')) {

            $classVersion   = $updateClass->getVersion();
            $classDbVersion = $updateClass->getDbVersion();

            if ($classDbVersion < $classVersion) {

                $updateMethodNames = preg_grep('/^_update/', get_class_methods($updateClass));

                $result['classVersion'] = $classVersion;
                $result['classDbVersion'] = $classDbVersion;
                $result['updateMethodNames'] = $updateMethodNames;

                if(is_array($updateMethodNames)) {

                    sort($updateMethodNames);

                    $updateResult = array();

                    $updateResult_success = false;

                    foreach ($updateMethodNames as $methodName) {
                        $methodNameVersionNumber = str_replace('_update', '', $methodName);
                        $mdvOptionVersionNumber  = str_replace('.', '', $classDbVersion);
                        $modelVersionNumber      = str_replace('.', '', $classVersion);

                        $result['updatingTo'] = $methodNameVersionNumber;

                        if (
                            ($methodNameVersionNumber > $mdvOptionVersionNumber) &&
                            ($methodNameVersionNumber <= $modelVersionNumber)
                        ) {
                            try {
                                $updateResult = $updateClass->$methodName();
                            } catch (Exception $e) {
                                $updateResult['success'] = false;
                                $updateResult['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
                            }

                            $updateResult_success = (is_array($updateResult) && array_key_exists('success',$updateResult) && is_bool($updateResult['success']) && $updateResult['success']);

                            if(!$updateResult_success) {
                                break;
                            }
                        }

                        $result['updateResult'] = $updateResult;
                    }

                    if(is_array($updateResult) && count($updateResult)) {
                        $result = $updateResult;

                        $result_success = (is_array($result) && array_key_exists('success',$result) && is_bool($result['success']) && $result['success']);

                        if($result_success) {
                            $newVersion = (is_array($result) && array_key_exists('version',$result)) ? $result['version'] : 0;

                            if($newVersion > 0) {
                                try {
                                    $updatedObjectName = strtolower(get_class($updateClass));

                                    $this->Core->Config()->set($updatedObjectName . '_version', $newVersion, 0, $updatedObjectName, 'version');
                                } catch (Exception $e) {
                                    $result['success'] = false;
                                    $result['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
                                }
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }

    private function _checkAction($updateClass) {
        $result = array();
        $result['version']    = 0;
        $result['dbVersion']  = 0;
        $result['up-to-date'] = false;

        if(is_object($updateClass) && method_exists($updateClass, 'getVersion') && method_exists($updateClass, 'getDbVersion')) {

            $classVersion   = $updateClass->getVersion();
            $classDbVersion = $updateClass->getDbVersion();

            $result['version']   = $classVersion;
            $result['dbVersion'] = $classDbVersion;

            if($classVersion == $classDbVersion) {
                $result['up-to-date'] = true;
            }
        }

        return $result;
    }

    private function _changelogCore() {
        $result = array();

        $coreClass = $this->Core;

        if(is_object($coreClass)) {
            $result = $this->_getChangeLogAction($coreClass);
        }

        return $result;
    }

    private function _changelogModel($key = '') {
        $result = array();

        $modelClass = $this->Core->Mvc()->modelClass($key);

        if(is_object($modelClass)) {
            $result = $this->_getChangeLogAction($modelClass);
        }

        return $result;
    }

    private function _getChangeLogAction($updateClass) {
        $result = array();

        $changelog = array();

        if(is_object($updateClass) && method_exists($updateClass, 'getVersion') && method_exists($updateClass, 'getDbVersion')) {
            $updatedObjectName = strtolower(get_class($updateClass));

            $updateMethodNames = preg_grep('/^_update/', get_class_methods($updateClass));

            if(is_array($updateMethodNames)) {

                sort($updateMethodNames);

                $updateInfoResult = array();

                foreach ($updateMethodNames as $methodName) {
                    $methodNameVersionNumber = str_replace('_update', '', $methodName);
                    try {
                        $updateInfoResult = $updateClass->$methodName(false);
                    } catch (Exception $e) {
                        $updateInfoResult[$methodNameVersionNumber]['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
                    }

                    $changelogVersion = (array_key_exists('version',$updateInfoResult)) ? $updateInfoResult['version'] : '--';
                    $changelogInfo = (array_key_exists('changelog',$updateInfoResult)) ? $updateInfoResult['changelog'] : '--';

                    $changelog[$changelogVersion] = $changelogInfo;
                }

                if (is_array($updateInfoResult) && count($updateInfoResult)) {
                    $result[$updatedObjectName] = $updateInfoResult;
                }
            }

            $result = $changelog;
        }

        return $result;
    }
}