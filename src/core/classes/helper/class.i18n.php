<?php
/**
 * Handle Internationalisation and Translation
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** Internationalisation Class */
class i18n
{
    /**
     * @var $Core Core
     */
    private $Core;
    private $_languages;
    private $_countries;
    private $_coreTranslations             = null;
    private $_modelTranslations            = null;
    private $_themeTranslations            = null;
    private $_dbTranslations               = null;
    private $_dbTranslationsFull           = null;
    private $_translations                 = null;
    private $_translationStrings           = null;
    private $_defaultLang                  = null;
    private $_searchTranslateStringsResult = null;
    private $_missingTranslations          = null;

    /**
     * i18n constructor.
     * @param $Core Core
     */
    function __construct($Core)
    {
        $this->Core = $Core;
        $this->_setLanguages();

        $sessUser = (array_key_exists('projectParameters', $_SESSION) && is_array($_SESSION['projectParameters']) && array_key_exists('user',$_SESSION['projectParameters'])) ? $_SESSION['projectParameters']['user'] : array();
        $sessUserLang = (is_array($sessUser) && count($sessUser)) ? $sessUser['default_lang'] : $this->getDefaultLang();

        $this->setDefaultLang($sessUserLang);

        $this->_setSessionInfo();
    }

    private function _setSessionInfo() {
        /** Set Session Info */
        $_SESSION['curLang'] = $this->getCurrLang();
    }

    /** Country Methods */

    public function getCountries()
    {
        $countries = $this->_countries;
        if($countries === null) {
            $countries = array();

            try {
                $sql = "SELECT * FROM `" . DB_TABLE_PREFIX . "i18n_countries`;";
                $result = $this->Core->Db()->fromDatabase($sql,'@raw');

                foreach($result as $row) {
                    $countries[$row['id']] = array(
                        'id'   => trim($row['id']),
                        'code' => trim($row['code']),
                        'name' => trim($row['name'])
                    );
                }
            } catch (Exception $e) {
                $error = 'Exception abgefangen: ' . $e->getMessage() . "\n";
                $this->Core->Log($error,'core_error',true);
            }

            $this->_countries = $countries;
        }
        return $countries;
    }

    /** Language Methods  */

    private function _setLanguages()
    {
        $sql = "SELECT * FROM " . DB_TABLE_PREFIX . "i18n";
        $result = $this->Core->Db()->fromDatabase($sql, '@raw');

        foreach($result as $row)
        {
            $id     = trim($row['id']);
            $code   = trim($row['code']);
            $locale = trim($row['locale']);
            $name   = trim($row['name']);
            $active = trim($row['active']);

            $this->addLang($code,$name,$locale,$active);
        }
    }

    public function isMultilang()
    {
        $is_multilang = $this->Core->Config()->get('multilang');
        return ($is_multilang == "1") ? true : false;
    }

    public function setDefaultLang($defaultLangKey)
    {
        $curDefaultLang = $this->getDefaultLang();
        $return = $curDefaultLang;
        if($this->isMultilang()) {
            $this->_defaultLang = $defaultLangKey;
            $return = $defaultLangKey;
        }
        return $return;
    }

    public function getDefaultLang()
    {
        $defaultLang = $this->_defaultLang;
        if($defaultLang == null) {
            $defaultLang = $this->Core->Config()->get('default_lang');
        }
        return $defaultLang;
    }

    public function getLangKeyFromPath() {
        $curr_lang_url_key = '';
        $url_path = $this->Core->Request()->getPath();
        if($url_path != ''){
            /** Get Site from Session */
            $sessSite   = (array_key_exists('site',$_SESSION) && is_array($_SESSION['site'])) ? $_SESSION['site'] : array();
            $siteUrlKey = (count($sessSite) && array_key_exists('urlKey',$sessSite)) ? $sessSite['urlKey'] : '';

            $siteUrlKeySlug = ($siteUrlKey !== '') ? $siteUrlKey . '/' : '';

            $url_path = str_replace($siteUrlKeySlug, '', $url_path);

            $url_parts = preg_split('#/#', $url_path, -1, PREG_SPLIT_NO_EMPTY);
            $first_path_part = (array_key_exists(0,$url_parts)) ? $url_parts[0] : '';
            $lang_code = strip_tags(trim($first_path_part));
            if(preg_match('#\b[a-z]{2}\b#', $lang_code)) {
                $curr_lang_url_key = $lang_code;
            }
        }
        return $curr_lang_url_key;
    }

    public function getCurrLang()
    {
        $curr_lang = $this->getDefaultLang();
        $lang_key_from_path = $this->getLangKeyFromPath();
        if($lang_key_from_path != ''){
            if (array_key_exists($lang_key_from_path, $this->getLanguages()) && $this->isMultilang()){
                $curr_lang = $lang_key_from_path;
            }
        }
        return $curr_lang;
    }

    public function getCurrLangUrlKey() {
        $currLang       = $this->getCurrLang();
        $defaultLang    = $this->getDefaultLang();
        $lang_key_from_path = $this->getLangKeyFromPath();

        $currLangUrlKey = (($lang_key_from_path != '') && ($lang_key_from_path != $defaultLang) && ($lang_key_from_path == $currLang)) ? $lang_key_from_path : '';

        if($currLangUrlKey == '') {

            if($lang_key_from_path != '' && ($lang_key_from_path != $defaultLang) && !$this->Core->Request()->isAjax()) {

                $note = sprintf($this->Core->i18n()->translate('Sprache \'%s\' nicht gefunden. Sprache \'%s\' wird verwendet!'), $lang_key_from_path, $defaultLang);

                if (isset($_SESSION['note']) && is_array($_SESSION['note']) && count($_SESSION['note'])) {
                    foreach ($_SESSION['note'] as $key => $sessNote) {
                        $noteContent = $sessNote['note'];
                        if ($note == $noteContent) {
                            unset($_SESSION['note'][$key]);
                            break;
                        }
                    }
                }

                $type = 'warning';
                $kind = 'bs-alert';
                $title = '';

                $this->Core->setNote($note, $type, $kind, $title);
            }

        }

        return $currLangUrlKey;
    }

    public function addLang($code,$name,$locale,$active = false)
    {
        $this->_languages[$code] = array(
            'name'   => $name,
            'code'   => $code,
            'locale' => $locale,
            'active' => $active
        );
    }

    public function getLanguages()
    {
        return $this->_languages;
    }

    public function getLang($code)
    {
        $_languages = $this->_languages;
        return (array_key_exists($code,$_languages)) ? $_languages[$code] : array();
    }

    public function getLangByLocale($locale)
    {
        $_languages = $this->_languages;
        $lang = array();
        foreach($_languages as $language) {
            if($language['locale'] == $locale) {
                $lang = $language;
                break;
            }
        }
        return $lang;
    }

    public function getDateFormats() {
        $dateFormats = array(
            'en_US' => 'm/d/Y',
            'de_DE' => 'd.m.Y'
        );

        return $dateFormats;
    }

    public function getDateFormat($code = null) {
        $getLang = (count($this->getLang($code))) ? $this->getLang($code) : $this->getLang($this->getDefaultLang());
        $getDateFormats = $this->getDateFormats();
        $dateFormat = 'Y-m-d';

        if(count($getLang)) {
            $locale = (array_key_exists('locale', $getLang)) ? $getLang['locale'] : '';

            $dateFormat = (array_key_exists($locale,$getDateFormats)) ? $getDateFormats[$locale] : $dateFormat;
        }

        return $dateFormat;
    }

    public function getDateTimeFormats() {
        $dateTimeFormats = array(
            'en_US' => 'Y-m-d h:i:s A',
            'de_DE' => 'd.m.Y H:i:s'
        );

        return $dateTimeFormats;
    }

    public function getDateTimeFormat($code = null) {
        $getLang = (count($this->getLang($code))) ? $this->getLang($code) : $this->getLang($this->getDefaultLang());
        $getDateTimeFormats = $this->getDateTimeFormats();
        $dateTimeFormat = 'Y-m-d H:i:s';

        if(count($getLang)) {
            $locale = (array_key_exists('locale', $getLang)) ? $getLang['locale'] : '';

            $dateTimeFormat = (array_key_exists($locale,$getDateTimeFormats)) ? $getDateTimeFormats[$locale] : $dateTimeFormat;
        }

        return $dateTimeFormat;
    }

    public function getPercentTranslated($code = null)
    {
        $translationsFull      = $this->getTranslations();
        $translatonStrings     = $this->getTranslationStrings();
        $fileTranslatonStrings = $this->getFileTranslationStrings();
        $missingTranslations   = $this->getMissingTranslations();

        $percent = array();

        /** Make unique and sort */
        foreach($missingTranslations as $locale => $strings) {
            $strings = array_unique($strings);
            sort($strings);

            $missingTranslations[$locale] = $strings;
        }

        foreach($translationsFull as $langLocale => $translations) {
            $lang = $this->getLangByLocale($langLocale);

            $langCode = $lang['code'];

            $translated = array();
            foreach ($fileTranslatonStrings as $translationString) {

                $translations   = $this->getTranslations($langLocale);

                $fileTranslations = (array_key_exists('fs',$translations) && is_array($translations['fs'])) ? $translations['fs'] : array();
                $dbTranslations   = (array_key_exists('db',$translations) && is_array($translations['db'])) ? $translations['db'] : array();

                $translation    = (array_key_exists($translationString, $fileTranslations)) ? $fileTranslations[$translationString] : '';
                $translation_db = (array_key_exists($translationString, $dbTranslations)) ? $dbTranslations[$translationString] : '';

                if($translation !== '' || $translation_db !== '') {
                    $translated[] = $translationString;
                }

            }

            $translated = array_unique($translated);
            sort($translated);

            $percentTranslated = number_format(count($translated) * (100 / count($fileTranslatonStrings)), 2, ',', '.');

            if($percentTranslated > 100) {
                $percentTranslated = 100;
            }

            if($percentTranslated < 0) {
                $percentTranslated = 0;
            }

            $percent[$langCode] = $percentTranslated;
        }

        return (array_key_exists($code,$percent)) ? $percent[$code] : $percent;
    }

    /** Theme Methods */

    public function getLangChooser() {
        $html = '';

        if ($this->isMultilang()){
            $languages = $this->getLanguages();

            if(count($languages)) {
                $currLang    = $this->getCurrLang();
                $defaultLang = $this->getDefaultLang();

                $siteBaseUrl = $this->Core->Sites()->getSiteBaseUrl();
                $site        = $this->Core->Sites()->getSite();
                $siteUrlKey  = (count($site) && array_key_exists('urlKey',$site)) ? $site['urlKey'] : '';

                $siteUrlKeySlug = ($siteUrlKey !== '') ? $siteUrlKey . '/' : '';

                $currParams  = $this->Core->Request()->getParams();
                $cleanParams = str_replace($siteUrlKeySlug, '', $currParams);

                $html .= '<div class="btn-group btn-group-xs"><a href="#" data-target="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" title="' . $this->translate('Sprache') . '"><span class="flag flag-' . $currLang . '"></span> <span class="caret"></span></a><ul class="dropdown-menu">';

                foreach ($languages as $lang) {

                    $isCurrent   = ($lang['code'] == $currLang) ? true : false;
                    $activeClass = ($isCurrent) ? ' class="active"' : '';

                    $targetLink = $siteBaseUrl;

                    if($lang['code'] != $defaultLang) {
                        $targetLink .= $lang['code'];
                        $targetLink .= '/';
                    }


                    $targetLink .= ($cleanParams !== '') ? $cleanParams : '';

                    $linkName  = $lang['name'];
                    $linkTitle = $this->translate('Sprache wechseln zu') . ' ' . $linkName;
                    if($isCurrent) {
                        $linkTitle = $this->translate('Aktuelle Sprache') . ' ' . $linkName;
                    }

                    $html .= '<li' . $activeClass . '><a href="' . $targetLink . '" title="' . $linkTitle . '"><span class="flag flag-' . $lang['code'] . '"></span> ' . $linkName . '</a></li>';
                }

                $html .= '</ul></div>';
            }
        }

        return $html;
    }

    /** Translation Methods */

    private function _scanTranslationDir($dir) {
        $translations = array();
        if(is_dir($dir)) {

            $i18nFolders = scandir($dir);

            foreach($i18nFolders as $i18nFolder) {
                if($i18nFolder != "." && $i18nFolder != ".." && is_dir($dir . DS . $i18nFolder)) {

                    $langFolders = scandir($dir . DS . $i18nFolder);

                    foreach($langFolders as $langFile) {

                        if($langFile != "." && $langFile != "..") {

                            $fullLangFile = $dir . DS . $i18nFolder . DS . $langFile;
                            if(file_exists($fullLangFile)) {

                                $file_array = file($fullLangFile);
                                foreach($file_array AS $file_row){
                                    $file_row_array     = preg_split('/\";\"/', $file_row, -1, PREG_SPLIT_NO_EMPTY);

                                    $file_row_string    = (isset($file_row_array[0])) ? ltrim($file_row_array[0], "\"") : '';
                                    $file_row_tanslated = (isset($file_row_array[1])) ? rtrim(trim($file_row_array[1]), "\"") : '';

                                    if(is_array($file_row_tanslated)) {
                                        $file_row_tanslated = end($file_row_tanslated);
                                    }

                                    $translations[$i18nFolder]['fs'][$file_row_string] = $file_row_tanslated;
                                }
                            }
                        }
                    }
                }
            }
        }
        return $translations;
    }

    public function saveTranslation($id, $data, $notify = true, $redirect = true) {
        $result = array();
        $result['data'] = $data;
        if(is_array($data) && count($data)) {

            /** @var $accClass Acc */
            $accClass = $this->Core->Mvc()->modelClass('Acc');
            $curUser = (is_object($accClass)) ? $accClass->getUser() : array();
            $curUserId = (count($curUser)) ? $curUser['id'] : 0;

            $language = $this->getLangByLocale($data['locale']);
            $langCode = $language['code'];

            $string = html_entity_decode($data['string']);
            $translation_db = $data['translation_db'];

            $saveType = ($data['id'] === 'new' || $data['id'] < 1) ? 'create' : 'edit';

            if($saveType == 'create' && $translation_db !== '') {

                $translationInsertinto = '
					INSERT INTO
					`' . DB_TABLE_PREFIX . 'i18n_translations`
					(`string`, `' . $langCode .'`, `id_acc_users__changedBy`, `changed`)
					VALUES
					(:string, :translation, :idUser, NOW())
                ';

                $params = array(
                    'string'      => $string,
                    'translation' => $translation_db,
                    'idUser'      => $curUserId
                );

                $sqlResult = $this->Core->Db()->toDatabase($translationInsertinto, $params);

                $this->Core->Hooks()->fire('system_i18n_translation_created');

                $success = false;

                if ($this->Core->Db()->lastInsertId() == ""){
                    $errMsg = $this->Core->i18n()->translate('Konnte nicht erstellt werden!');
                    $note  = sprintf( $this->Core->i18n()->translate('MySQL Error: \'%s\''), $errMsg );
                    if($notify) {
                        $type  = 'danger';
                        $kind  = 'bs-alert';
                        $title = $this->Core->i18n()->translate('Fehler') . '!';
                        $this->Core->setNote($note, $type, $kind, $title);
                    } else {
                        $this->Core->Log()->write($note,'system_i18n_translation_save',true);
                    }
                } else {
                    $note  = $this->Core->i18n()->translate('Translation successful created!');
                    if($notify) {
                        $type  = 'success';
                        $kind  = 'bs-alert';
                        $title = $this->Core->i18n()->translate('Erledigt') . '!';
                        $this->Core->setNote($note, $type, $kind, $title);

                        $success = true;
                    }
                }

                if($redirect) {
                    $redirectUrl = $this->Core->Mvc()->getModelUrl('system') . '/i18n/translations/';
                    $this->Core->Request()->redirect($redirectUrl);
                }
            }

            if($saveType == 'edit') {

                $translationUpdate = '
	                UPDATE
	                    `'.DB_CONN_DBNAME.'`.`' . DB_TABLE_PREFIX . 'i18n_translations`
	                SET
	                    `' . $langCode .'` = :translation,
	                    `id_acc_users__changedBy`= :idUser,
	                    `changed` = NOW()
	                WHERE
	                    `' . DB_TABLE_PREFIX . 'i18n_translations`.`id` = ' . $data['id'] . ';';

                $params = array(
                    'translation' => $translation_db,
                    'idUser'      => $curUserId
                );

                $sqlResult = $this->Core->Db()->toDatabase($translationUpdate, $params);

                $success = false;

                if (!$sqlResult) {
                    $errMsg  = $this->Core->i18n()->translate('Konnte nicht gespeichert werden!');
                    $noteMsg = sprintf( $this->Core->i18n()->translate('MySQL Error: \'%s\''), $errMsg );

                    $note  = $noteMsg . '<pre>' . print_r($sqlResult, true) . '</pre>';
                    $type  = 'danger';
                    $kind  = 'bs-alert';
                    $title = $this->Core->i18n()->translate('Fehler') . '!';
                } else {
                    $note  = $this->Core->i18n()->translate('Translation successful saved!');
                    $type  = 'success';
                    $kind  = 'bs-alert';
                    $title = $this->Core->i18n()->translate('Erledigt') . '!';

                    $success = true;
                }

                if($notify) {
                    $this->Core->setNote($note, $type, $kind, $title);
                } else {
                    if(!$success) {
                        $this->Core->Log()->write($note,'system_i18n_translation_save', true);
                    }
                }

                $this->Core->Hooks()->fire('system_i18n_translation_saved', $data['id']);

                if($redirect) {
                    $redirectUrl = $this->Core->Mvc()->getModelUrl('system') . '/i18n/translations/';
                    $this->Core->Request()->redirect($redirectUrl);
                }

            }
        }
        return $result;
    }

    public function getCoreTranslations() {
        $coreTranslations = $this->_coreTranslations;
        if($coreTranslations === null) {
            // get Core Translations
            $coreTranslationDir = $this->Core->getCoreRelPath() . DS . 'translations';
            $coreTranslations   = $this->_scanTranslationDir($coreTranslationDir);

            $this->_coreTranslations = $coreTranslations;
        }
        return $coreTranslations;
    }

    public function getModelTranslations() {
        $modelTranslations = $this->_modelTranslations;
        if($modelTranslations === null) {
            /** @var $Mvc Mvc */
            $Mvc = $this->Core->Mvc();
            $rootpath = $this->Core->getRootPath();

            // get Model Translations
            $modelTranslations = array();
            if (is_object($Mvc)) {
                $activeModels = $Mvc->getModels();
                $modelsDir = $rootpath . DS . 'core' . DS . 'models';
                if (is_dir($modelsDir)) {
                    $modelsFolders = scandir($modelsDir);
                    foreach ($modelsFolders as $modelsFolder) {
                        if ($modelsFolder != "." && $modelsFolder != ".." && is_dir($modelsDir . DS . $modelsFolder) && array_key_exists($modelsFolder, $activeModels)) {
                            $modelDir = $modelsDir . DS . $modelsFolder . DS . 'translations';
                            $curModelTrans = $this->_scanTranslationDir($modelDir);
                            $modelTranslations = array_merge_recursive($modelTranslations, $curModelTrans);
                        }
                    }
                }
            }
            $this->_modelTranslations = $modelTranslations;
        }

        return $modelTranslations;
    }

    public function getThemeTranslations() {
        $themeTranslations = $this->_themeTranslations;
        if($themeTranslations === null) {

            // get Theme Translations
            $themeTranslations = array();
            if(is_object($this->Core->Mvc())) {
                $themeDir = $this->Core->Mvc()->getThemeRelPath() . DS . 'translations';
                $themeTranslations = $this->_scanTranslationDir($themeDir);
            }

            $this->_themeTranslations = $themeTranslations;
        }
        return $themeTranslations;
    }

    public function getDbTranslations($full = false)
    {
        $dbTranslations     = $this->_dbTranslations;
        $dbTranslationsFull = $this->_dbTranslationsFull;
        if($dbTranslations === null || $dbTranslations === null) {
            $dbTranslations = array();
            $sql = "SELECT * FROM `" . DB_TABLE_PREFIX . "i18n_translations`";
            $result = $this->Core->Db()->fromDatabase($sql, '@raw');
            foreach($result as $row) {
                $languages = array();
                foreach($row as $rowKey => $rowValue) {
                    if(
                        is_numeric($rowKey) ||
                        $rowKey == 'id' ||
                        strlen($rowKey) > 2
                    ) {
                        continue;
                    }
                    $languages[] = $rowKey;
                }
                $string = html_entity_decode($row['string']);

                $dbTranslationsFull[$row['id']]['id']          = $row['id'];
                $dbTranslationsFull[$row['id']]['string']      = $string;
                $dbTranslationsFull[$row['id']]['translations'] = array();
                foreach ($languages as $langCode) {
                    $lang = $this->getLang($langCode);
                    if(count($lang) && isset($lang['active']) && $lang['active']) {
                        $dbTranslations[$lang['locale']]['db'][$string] = html_entity_decode($row[$langCode]);

                        $dbTranslationsFull[$row['id']]['translations'][$lang['locale']] = html_entity_decode($row[$langCode]);
                    }
                }
            }
        }
        return ($full === true) ? $dbTranslationsFull : $dbTranslations;
    }

    public function getDbTranslations_old()
    {
        $dbTranslations = $this->_dbTranslations;
        if($dbTranslations === null) {
            $dbTranslations = array();
            $sql = "SELECT * FROM `" . DB_TABLE_PREFIX . "i18n_translations`";
            $result = $this->Core->Db()->fromDatabase($sql, '@raw');
            foreach($result as $row) {
                $languages = array();
                foreach($row as $rowKey => $rowValue) {
                    if(
                        is_numeric($rowKey) ||
                        $rowKey == 'id' ||
                        strlen($rowKey) > 2
                    ) {
                        continue;
                    }
                    $languages[] = $rowKey;
                }
                $string = html_entity_decode($row['string']);
                foreach ($languages as $langCode) {
                    $lang = $this->getLang($langCode);
                    if(count($lang) && isset($lang['active']) && $lang['active']) {
                        $dbTranslations[$lang['locale']]['db'][$string] = html_entity_decode($row[$langCode]);
                    }
                }
            }
        }
        return $dbTranslations;
    }

    public function getTranslations($locale = null)
    {
        $translations = $this->_translations;
        if($translations === null) {

            $rootpath = $this->Core->getRootPath();

            // Translations Array
            $translations = array();

            // get Main Translations
            $coreTranslations    = $this->getCoreTranslations();

            // get Model Translations
            $modelsTranslations  = $this->getModelTranslations();

            // get Theme Translations
            $themeTranslations   = $this->getThemeTranslations();

            // get DB Translations
            $dbTranslations      = $this->getDbTranslations();

            // merge all Translations and set to Variable
            $translations        = array_merge_recursive($translations, $coreTranslations, $modelsTranslations, $themeTranslations, $dbTranslations);
            $this->_translations = $translations;

        }

        return (array_key_exists($locale, $translations) && is_array($translations[$locale])) ? $translations[$locale] : $translations;
    }

    public function getTranslationStrings()
    {
        $strings = $this->_translationStrings;

        if($strings === null) {

            $strings = array();

            $languages = $this->getLanguages();

            foreach($languages as $lang){
                $translations = $this->getTranslations($lang['locale']);

                $fileTranslations = (array_key_exists('fs',$translations) && is_array($translations['fs'])) ? $translations['fs'] : array();

                foreach($fileTranslations as $string => $translation) {
                    $strings[] = $string;
                }
            }

            $strings = array_unique($strings);
            asort($strings);

            $this->_translationStrings = $strings;

        } else {

            if(!is_array($strings)) {
                $strings = array();
            }

        }

        return $strings;
    }

    public function translate($string, $lang = null)
    {
        $curLangCode = ($this->getCurrLang() != '' && $this->isMultilang()) ? $this->getCurrLang() : $this->getDefaultLang();

        $useLang = $this->getLang($curLangCode);

        if($lang !== null) {
            $getLang = $this->getLang($lang);
            $useLang = (count($getLang)) ? $getLang : $useLang;
        }

        $lang = $useLang;

        $string_translated = $string;

        if(count($lang) && isset($lang['active']) && $lang['active']) {

            $locale = $lang['locale'];
            $translations = $this->getTranslations($locale);

            // translate by Filesystem if set
            $string_translated = (array_key_exists('fs',$translations) && is_array($translations['fs']) && array_key_exists($string, $translations['fs'])) ? $translations['fs'][$string] : '';

            // owerwrite it by DB translations if set
            $string_translated = (array_key_exists('db',$translations) && is_array($translations['db']) && array_key_exists($string, $translations['db']) && is_string($translations['db'][$string]) && $translations['db'][$string] !== '') ? $translations['db'][$string] : $string_translated;
        }

        if ($string_translated == '') {
            return $string;
        }
        else {
            return $string_translated;
        }
    }

    private function _searchTranslateStringsRecursiveInFiles($dir = null) {
        /** Get User from Session */
        $getUserFromSession = (array_key_exists('user', $_SESSION) && is_array($_SESSION['user'])) ? $_SESSION['user'] : array();
        $isSu = (count($getUserFromSession) && array_key_exists('su', $getUserFromSession) && $getUserFromSession['su']);

        if($dir === null) {
            $dir = $this->Core->getCoreRelPath() . DS;
        }

        if($this->_searchTranslateStringsResult === null) {
            $this->_searchTranslateStringsResult = array();
        }

        $handler = opendir($dir);
        while ($file = readdir($handler)) {
            if ($file != "." && $file != "..") {
                $fullFilePath = $dir . $file;
                if(is_dir($fullFilePath)) {
                    $this->_searchTranslateStringsRecursiveInFiles($fullFilePath . DS);
                } else {
                    if (strpos($file, '.php') !== false || strpos($file, '.js') !== false) {
                        $doSearchForMatches = false;

                        $isFileInClassesPath        = (strpos($fullFilePath, 'classes')                  !== false);
                        $isFileInComponentsCorePath = (strpos($fullFilePath, 'components' . DS . 'core') !== false);
                        $isFileInModelsPath         = (strpos($fullFilePath, 'models')                   !== false);
                        $isFileInPluginsPath        = (strpos($fullFilePath, 'plugins')                  !== false);
                        $isFileInModulesPath        = (strpos($fullFilePath, 'plugins')                  !== false);
                        $isFileInThemesPath         = (strpos($fullFilePath, 'themes')                   !== false);

                        if($isFileInClassesPath || $isFileInComponentsCorePath) {
                            $doSearchForMatches = true;
                        }

                        if($isFileInModelsPath && is_object($this->Core->Mvc())) {
                            $activeModels = $this->Core->Mvc()->getModels();

                            $foundModelKeyInPath = false;
                            foreach($activeModels as $modelKey => $model) {
                                $foundModelKeyInPath = (strpos($fullFilePath, 'models' . DS . $modelKey)  !== false);
                                if($foundModelKeyInPath) {
                                    break;
                                }
                            }

                            $doSearchForMatches = $foundModelKeyInPath;
                        }

                        if($isFileInPluginsPath && is_array($this->Core->Config()->get('default_plugins'))) {
                            $defaultPlugins = $this->Core->Config()->get('default_plugins');

                            $foundPluginKeyInPath = false;
                            foreach($defaultPlugins as $pluginKey) {
                                $foundPluginKeyInPath = (strpos($fullFilePath, 'plugins' . DS . $pluginKey)  !== false);
                                if($foundPluginKeyInPath) {
                                    break;
                                }
                            }

                            $doSearchForMatches = $foundPluginKeyInPath;
                        }

                        if($isFileInModulesPath && is_array($this->Core->Config()->get('default_modules'))) {
                            $defaultModules = $this->Core->Config()->get('default_modules');

                            $foundModulesKeyInPath = false;
                            foreach($defaultModules as $pluginKey) {
                                $foundModulesKeyInPath = (strpos($fullFilePath, 'plugins' . DS . $pluginKey)  !== false);
                                if($foundModulesKeyInPath) {
                                    break;
                                }
                            }

                            $doSearchForMatches = $foundModulesKeyInPath;
                        }

                        if($isFileInThemesPath && is_object($this->Core->Mvc())) {
                            $themeName = $this->Core->Mvc()->getThemeName();

                            $foundThemeNameInPath = (strpos($fullFilePath, 'themes' . DS . $themeName)  !== false);

                            $doSearchForMatches = $foundThemeNameInPath;
                        }

                        if($doSearchForMatches || $isSu) {
                            $fileContents = file_get_contents($fullFilePath);
                            if($fileContents !== false) {
                                $regex = '@\->translate\(\'(.+?)(?<![\\\\])\'@s';
                                preg_match_all($regex, $fileContents, $matches);
                                if(is_array($matches) && count($matches) && isset($matches[1]) && count($matches[1])) {
                                    $matchesUnique = array_unique($matches[1]);
                                    $this->_searchTranslateStringsResult[str_replace(JF_ROOT_DIR,'',$fullFilePath)] = $matchesUnique;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function getFileTranslationStrings($cleaned = true) {
        $searchTranslateStringsResult = $this->_searchTranslateStringsResult;
        if($searchTranslateStringsResult === null) {
            $this->_searchTranslateStringsRecursiveInFiles();
            $searchTranslateStringsResult = $this->_searchTranslateStringsResult;
        }

        if($cleaned) {
            $resultsCleaned = array();
            foreach ($searchTranslateStringsResult as $file => $strings) {
                foreach ($strings as $string) {
                    $formattedString = $string;
                    $formattedString = stripslashes($formattedString);
                    // $formattedString = htmlentities($formattedString);
                    $formattedString = trim($formattedString);

                    $resultsCleaned[] = $formattedString;
                }
            }
            $resultsCleanedUnique = array_unique($resultsCleaned);
            sort($resultsCleanedUnique);
            $searchTranslateStringsResult = $resultsCleanedUnique;
        }

        return $searchTranslateStringsResult;
    }

    public function getMissingTranslations() {
        $missingTranslations = $this->_missingTranslations;
        if($missingTranslations === null) {
            $this->_searchTranslateStringsRecursiveInFiles();
            $resultsFinal = $this->getFileTranslationStrings();
            $getTranslations = $this->getTranslations();

            $missingTranslations = array();
            foreach ($getTranslations as $locale => $translationAreas) {
                foreach ($resultsFinal as $translationString) {

                    $translations   = $this->getTranslations($locale);
                    $translation    = (array_key_exists('fs', $translations) && is_array($translations['fs']) && array_key_exists($translationString, $translations['fs'])) ? $translations['fs'][$translationString] : '';
                    $translation_db = (array_key_exists('db', $translations) && is_array($translations['db']) && array_key_exists($translationString, $translations['db'])) ? $translations['db'][$translationString] : '';

                    if($translation === '' && $translation_db === '') {
                        $missingTranslations[$locale][] = $translationString;
                    }

                }
            }

            /** Make unique and sort */
            foreach($missingTranslations as $locale => $strings) {
                $strings = array_unique($strings);
                sort($strings);

                $missingTranslations[$locale] = $strings;
            }

            $this->_missingTranslations = $missingTranslations;
        }
        return $missingTranslations;
    }
}