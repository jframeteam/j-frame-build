<?php
/**
 * DataBase Class
 *
 * Some Methods implemented from Source by Planet ITservices GmbH & Co. KG
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */


/** DB Class */
class Db extends PDO
{

    /**
     * @var $Core Core
     */
    private $Core;
    private $db_config;
    private $db_toTimes = Array(); // global cache array
    private $db_to = Array(); // global cache array

    /**
     * Db constructor.
     * @param $Core
     * @throws Exception
     */
    function __construct($Core)
    {
        $this->Core = $Core;

        $config = array(
            'db_charset'  => DB_CONN_CHARSET,
            'db_type'     => DB_CONN_TYPE,
            'db_host'     => DB_CONN_ADDRESS,
            'db_username' => DB_CONN_USER,
            'db_password' => DB_CONN_PASS,
            'db_name'     => DB_CONN_DBNAME,
        );

        $db_charset = $config['db_charset'];
        $db_dsn = $config['db_type'] . ':host=' . $config['db_host'] . ';dbname=' . $config['db_name'] . ';charset=' . $db_charset;
        $db_user = $config['db_username'];
        $db_pass = $config['db_password'];

        try {
            parent::__construct($db_dsn, $db_user, $db_pass);
            $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        } catch (PDOException $e) {
            $errorMsg = 'Connecting to database failed - ' . $e->getMessage();
            // $errorMsg = '<pre>' . $errorMsg . PHP_EOL . print_r($config, true) . PHP_EOL . print_r($db_dsn, true) . '</pre>';
            throw new Exception($errorMsg);
        }
    }

    /**
     * Param sql statement to check
     * Returns link to prepared statement
     *
     * @param $sql
     * @return mixed
     * @throws Exception
     */
    private function _cachedQuery($sql)
    {
        $md5 = md5($sql);
        if (isset($this->db_to[$md5])) {
            // already in library

            // update time

            $microtime = microtime(true);
            $this->db_to[$md5]['time'] = $microtime;
            unset($this->db_toTimes[array_search($md5, $this->db_toTimes)]);
            $this->db_toTimes[$microtime] = $md5;
        } else {
            // prepare statement and get into local $link variable
            try {
                $link = $this->prepare($sql);
            } catch (PDOException $e) {
                throw new Exception('sql error for: ' . $sql . ' - ' . $e->getMessage());
            }

            // check how many statements already cached and if limit reached (assume 10)
            if (count($this->db_to) > 10) {
                // overwrite oldest entry
                $oldest = min(array_keys($this->db_toTimes));
                unset($this->db_to[$this->db_toTimes[$oldest]]);
                unset($this->db_toTimes[$oldest]);

                $microtime = microtime(true);
                $this->db_toTimes[$microtime] = $md5;
                $this->db_to[$md5]['time'] = $microtime;
                $this->db_to[$md5]['link'] = $link;
            } else {
                $microtime = microtime(true);
                $this->db_toTimes[$microtime] = $md5;
                $this->db_to[$md5]['time'] = $microtime;
                $this->db_to[$md5]['link'] = $link;
            }

        }

        return ($this->db_to[$md5]['link']);

    }

    /**
     * Create External Connection
     *
     * @param $config
     * @return db|null
     * @throws Exception
     */
    static function getExtConnection($config)
    {
        $conn = null;

        $db_charset = 'utf8';
        $db_dsn = $config['db_type'] . ':host=' . $config['db_host'] . ';dbname=' . $config['db_name'] . ';charset=' . $db_charset;
        $db_user = $config['db_username'];
        $db_pass = $config['db_password'];
        try {
            $conn = new db($db_dsn, $db_user, $db_pass);
        } catch (PDOException $exc) {
            throw new Exception('Connecting to external database failed - ' . $exc->getMessage());
        }
        return $conn;
    }

    /**
     * Close External Connection
     *
     * @param $conn
     */
    static function closeExtConnection(&$conn)
    {
        $conn = null;
    }

    /**
     * Get Data from Database
     *
     * @param string $sql : already properly escaped sql string
     * @param bool|string $field : array key by field value
     * @param array $params : Set Array of Query Params
     * @param bool $useLastSql : Use last executed Statement
     * @param bool $html : if values should have encoded html special chars
     * @param bool|string $groupby : field name on which result is grouped by
     * @param bool $withNumRows : saves numRows inside parameters if set
     * @param bool $doNotCheckDataStatus : if data_status must be contained within where clause
     * @param bool $rewriteForViews : if sql should be rewritten to fetch data from personal view
     *
     * @return array|bool (array,array key = id)/false if no value
     * @throws Exception
     * !special value for field="@flat"; if set, an array result like [column1]=column2 is returned, only possible for 2 column queries
     * !special value for field="@simple"; if result is just a single value this value is returned
     * !special value for field="@line"; only one line is returned
     * !special value for field="@raw"; all result lines are returned within a numbered index array
     * !special value for field="@groupby"; all result lines are returned within a group column index array containing numbered index array;
     */
    public function fromDatabase($sql, $field = false, $params = array(), $html = true, $groupby = false, $withNumRows = false, $doNotCheckDataStatus = false, $rewriteForViews = true)
    {
        /** @var $plugins Plugins */
        $plugins = $this->Core->Plugins();
        /** @var $pitsCore PitsCore */
//        $pitsCore = $plugins->PitsCore();

        if (!$sql) {
            throw new Exception('sql statement missing!');
        }

        if (!$field) {
            throw new Exception('field missing.');
        }

        $resultArray = array();

        $result = $this->_cachedQuery($sql);

        try {
            $executed = $result->execute($params);
        } catch (PDOException $e) {
            throw new Exception('sql error for: ' . $sql . ' - ' . $e->getMessage());
        }

        $err = (!$executed) ? true : false;

        //do we have a db error?
        if ($err) {
            //error occured, show the error:
            throw new Exception($result->errorInfo()[2]);
        }

        if (($field != '@flat') && ($field != '@simple') && ($field != '@raw') && ($field != '@line') && ($field != '@groupby')) {
            if ($result->rowCount()) {
                while ($temp = $result->fetch(PDO::FETCH_ASSOC)) {
                    foreach ($temp as &$value) {
                        if ($value != strip_tags($value)) {
                            $value = htmlentities($value, ENT_QUOTES);
                        }
                    }

                    $resultArray[$temp[$field]] = $temp;
                }
                if ($withNumRows) {
//                    $pitsCore->setParameter('numRows', mysqli_num_rows($result));
                }

                return ($resultArray);
            } else {
                if ($withNumRows) {
//                    $pitsCore->setParameter('numRows', 0);
                }

                return (false);
            }
        }

        if ($field == '@flat') {
            if ($result->rowCount()) {
                while ($temp = $result->fetch()) {
                    $temp_0 = ($temp[0] != strip_tags($temp[0])) ? htmlentities($temp[0], ENT_QUOTES) : $temp[0];
                    $temp_1 = ($temp[1] != strip_tags($temp[1])) ? htmlentities($temp[0], ENT_QUOTES) : $temp[1];
                    $resultArray[$temp_0] = $temp_1;
                }

                if ($withNumRows) {
//                    $pitsCore->setParameter('numRows', mysqli_num_rows($result));
                }

                return ($resultArray);
            } else {
                if ($withNumRows) {
//                    $pitsCore->setParameter('numRows', 0);
                }

                return (false);
            }
        }

        if ($field == '@simple') {
            if ($result->rowCount()) {
                $temp = $result->fetch();

                $temp_0 = ($temp[0] != strip_tags($temp[0])) ? htmlentities($temp[0], ENT_QUOTES) : $temp[0];

                $resultArray = $temp_0;

                if ($withNumRows) {
//                    $pitsCore->setParameter('numRows', mysqli_num_rows($result));
                }

                return ($resultArray);
            } else {
                if ($withNumRows) {
//                    $pitsCore->setParameter('numRows', 0);
                }

                return (false);
            }
        }

        if ($field == '@line') {
            if ($result->rowCount()) {
                $resultArray = $result->fetch(PDO::FETCH_ASSOC);
                foreach ($resultArray as $key => $value) {
                    if ($value != strip_tags($value)) {
                        $value = htmlentities($value, ENT_QUOTES);
                    }
                    $resultArray[$key] = $value;
                }
                if ($withNumRows) {
//                    $pitsCore->setParameter('numRows', mysqli_num_rows($result));
                }

                return ($resultArray);
            } else {
                if ($withNumRows) {
//                    $pitsCore->setParameter('numRows', 0);
                }

                return (false);
            }
        }

        if ($field == '@raw') {
            $i = 0;
            if ($result->rowCount()) {
                while ($temp = $result->fetch(PDO::FETCH_ASSOC)) {
                    foreach ($temp as &$value) {
                        if ($value != strip_tags($value)) {
                            $value = htmlentities($value, ENT_QUOTES);
                        }
                    }

                    $resultArray[$i] = $temp;
                    $i++;
                }
                while ($temp = $result->fetch()) {
                    $resultArray[$i] = $temp;
                    $i++;
                }
            }
            if ($withNumRows) {
//                $pitsCore->setParameter('numRows', mysqli_num_rows($result));
            }

            return ($resultArray);
        }

        if ($field == '@groupby') {
            $last_groupby = '';
            $i = 0;
            if ($result->rowCount()) {
                while ($temp = $result->fetch()) {
                    if ($groupby == 'Name') {
                        if ($temp[$groupby][0] != $last_groupby)
                            $i = 0;

                        $resultArray[$temp[$groupby][0]][$i] = $temp;

                        $last_groupby = $temp[$groupby][0];
                    } else {
                        if ($temp[$groupby] != $last_groupby)
                            $i = 0;

                        $resultArray[$temp[$groupby]][$i] = $temp;

                        $last_groupby = $temp[$groupby];
                    }

                    $i++;
                }
            }
            if ($withNumRows) {
//                $pitsCore->setParameter('numRows', mysqli_num_rows($result));
            }

            return ($resultArray);
        } else {
            if ($withNumRows) {
//                $pitsCore->setParameter('numRows', 0);
            }

            return (false);
        }
    }

    /**
     * Set Data To Database
     *
     * withLog [bool]: should this be logged? can only be used if module "logs" is loaded
     * allowForbiddenAction [bool]: allows updates without where and deletes without limit
     *
     * return value of mysql_query
     *
     * @param $sql
     * @param array $params
     * @param bool $withLog
     * @param bool $allowForbiddenAction
     * @param string $logTitle
     * @return bool|mixed
     * @throws Exception
     */
    public function toDatabase($sql, $params = array(), $withLog = false, $allowForbiddenAction = false, $logTitle = "")
    {
        /**
         * @var $plugins Plugins
         * @var $pitsHelper PitsHelper
         */
        $plugins = $this->Core->Plugins();
        $pitsHelper = $plugins->PitsHelper();
        $Logs = null;

        $sql = trim($sql);
        $oneLineSql = preg_replace('/\s+/', ' ', $sql);

        if (!$sql) {
            throw new Exception('sql statement empty!');
        }

        if ((preg_match('/^UPDATE/im', $sql)) && (!preg_match('/ WHERE /im', $oneLineSql)) && (!$allowForbiddenAction)) {
            throw new Exception('<pre>Use of UPDATE statement without "WHERE" clause is forbidden (use allowForbiddenAction?)!' . PHP_EOL . PHP_EOL . $sql . '</pre>');
        }

        if ((preg_match('/^DELETE/im', $sql)) && (!preg_match('/LIMIT 1 |LIMIT 1$/im', $oneLineSql)) && (!$allowForbiddenAction)) {
            throw new Exception('Use of DELETE statement without "LIMIT 1" is forbidden (use allowForbiddenAction?)!');
        }

        if ($withLog) {
            // check for module "logs"
            if (!is_object($Logs)) {
                throw new Exception('module "logs" NOT loaded while withLog set!');
            }

            // insert, update or delete

            if (preg_match('/^INSERT/im', $oneLineSql)) {

                // get table name
                preg_match('/INSERT INTO `(.+?)`/im', $oneLineSql, $matches);
                $tableName = $matches[1];

                $mysql_queryPrepared = $this->prepare($sql);
                if (count($params)) {
                    $mysql_queryReturn = $mysql_queryPrepared->execute($params);
                } else {
                    $mysql_queryReturn = $mysql_queryPrepared->execute();
                }
                $lastInsertId = $this->lastInsertId();
                $sql = 'UPDATE `' . $tableName . '` SET `created`=now() WHERE `id`=' . $lastInsertId;
                $this->toDatabase($sql);
                $Logs->logInsert($tableName, $lastInsertId, $logTitle);

                // Save last inserted id
                $_POST['last_insert_id'] = $lastInsertId;
                return ($mysql_queryReturn);
            }

            if (preg_match('/^UPDATE/im', $oneLineSql)) {
                // extract id
                preg_match('/`id`= ?([0-9]+)/m', $sql, $matches);
                $id = $matches[1];


                // get field list
                $sqlForRegexp = substr($oneLineSql, strpos($oneLineSql, 'SET'));
                //$sqlForRegexp = substr($sql,0,strpos($sql,'WHERE'));
                preg_match_all('/`(.+?)`/', $sqlForRegexp, $matches);

                $fields = $matches[0];

                // get table name
                preg_match('/UPDATE `(.+?)`/im', $oneLineSql, $matches);
                $tableName = $matches[1];

                // fetch all old values
                $sqlOld = 'SELECT ' . $pitsHelper->arrayToString(',', $fields) . ' FROM `' . $tableName . '` WHERE id=' . $id;
                $oldValues = $this->fromDatabase($sqlOld, '@line', false, false, false, false, false);

                $mysql_queryPrepared = $this->prepare($sql);
                if (count($params)) {
                    $mysql_queryReturn = $mysql_queryPrepared->execute($params);
                } else {
                    $mysql_queryReturn = $mysql_queryPrepared->execute();
                }

                $newValues = $this->fromDatabase($sqlOld, '@line', false, false, false, false, false);

                $Logs->logUpdate($tableName, $id, $oldValues, $newValues, $logTitle);

                return ($mysql_queryReturn);
            }

            if (preg_match('/^DELETE/im', $oneLineSql)) {
                // delete statements are logged completely

                // extract id
                preg_match('/`id`=([0-9]+)/m', $oneLineSql, $matches);
                $id = $matches[1];

                $Logs->logSystemAction($id, 'delete statement', $sql);

                $mysql_queryReturn = $this->query($sql);
                return ($mysql_queryReturn);
            }

        } else {
            if (preg_match('/^INSERT/im', $oneLineSql)) {
                // get table name
                preg_match('/INSERT INTO `(.+?)`/im', $oneLineSql, $matches);
                $tableName = $matches[1];

                $mysql_queryPrepared = $this->_cachedQuery($sql);

                try {
                    $mysql_queryReturn = $mysql_queryPrepared->execute($params);
                } catch (PDOException $e) {
                    $paramsTxt = (count($params)) ? PHP_EOL . PHP_EOL . '<pre>$params => ' . print_r($params, true) . '</pre>' : '';
                    throw new Exception('sql error for: ' . $sql . ' - ' . $e->getMessage() . $paramsTxt);
                }

                $err = (!$mysql_queryReturn) ? true : false;

                //do we have a db error?
                if ($err) {
                    //error occured, show the error:
                    throw new Exception($mysql_queryPrepared->errorInfo()[2]);
                }

                $lastInsertId = $this->lastInsertId();

                // Save last inserted id
                $_POST['last_insert_id'] = $lastInsertId;
                return ($mysql_queryReturn);
            } else {


                $mysql_queryPrepared = $this->_cachedQuery($sql);

                try {
                    $mysql_queryReturn = $mysql_queryPrepared->execute($params);
                } catch (PDOException $e) {
                    throw new Exception('sql error for: ' . $sql . ' - ' . $e->getMessage());
                }

                $err = (!$mysql_queryReturn) ? true : false;

                //do we have a db error?
                if ($err) {
                    //error occured, show the error:
                    throw new Exception($mysql_queryPrepared->errorInfo()[2]);
                }

                $return = $mysql_queryPrepared->fetch(PDO::FETCH_ASSOC);
                if (preg_match('/^UPDATE/im', $sql)) {
                    if ($mysql_queryReturn) {
                        $return = $mysql_queryReturn;
                    }
                }
                if (preg_match('/^DELETE/im', $sql)) {
                    $return = true;
                }

                return ($return);

            }
        }


    }

}
