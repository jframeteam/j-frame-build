<?php
/**
 * Shell Helper
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** Class Shell */
class Shell
{
    /** @var $Core Core */
    private $Core;

    private $foreground_colors = array();
    private $background_colors = array();

    /**
     * Hooks constructor.
     * @param $Core Core
     */
    public function __construct($Core)
    {
        $this->Core = $Core;
        $this->_setColors();
    }

    private function _setColors() {
        // Set up shell colors
        $this->foreground_colors['black'] = '0;30';
        $this->foreground_colors['dark_gray'] = '1;30';
        $this->foreground_colors['blue'] = '0;34';
        $this->foreground_colors['light_blue'] = '1;34';
        $this->foreground_colors['green'] = '0;32';
        $this->foreground_colors['light_green'] = '1;32';
        $this->foreground_colors['cyan'] = '0;36';
        $this->foreground_colors['light_cyan'] = '1;36';
        $this->foreground_colors['red'] = '0;31';
        $this->foreground_colors['light_red'] = '1;31';
        $this->foreground_colors['purple'] = '0;35';
        $this->foreground_colors['light_purple'] = '1;35';
        $this->foreground_colors['brown'] = '0;33';
        $this->foreground_colors['yellow'] = '1;33';
        $this->foreground_colors['light_gray'] = '0;37';
        $this->foreground_colors['white'] = '1;37';

        $this->background_colors['black'] = '40';
        $this->background_colors['red'] = '41';
        $this->background_colors['green'] = '42';
        $this->background_colors['yellow'] = '43';
        $this->background_colors['blue'] = '44';
        $this->background_colors['magenta'] = '45';
        $this->background_colors['cyan'] = '46';
        $this->background_colors['light_gray'] = '47';
        $this->background_colors['white'] = '107';
    }

    // Returns colored string
    public function getColoredString($string, $foreground_color = null, $background_color = null) {
        $colored_string = "";

        // Check if given foreground color found
        if (isset($this->foreground_colors[$foreground_color])) {
            $colored_string .= "\033[" . $this->foreground_colors[$foreground_color] . "m";
        }
        // Check if given background color found
        if (isset($this->background_colors[$background_color])) {
            $colored_string .= "\033[" . $this->background_colors[$background_color] . "m";
        }

        // Add string and end coloring
        $colored_string .=  $string . "\033[0m";

        return $colored_string;
    }

    // Returns all foreground color names
    public function getForegroundColors() {
        return array_keys($this->foreground_colors);
    }

    // Returns all background color names
    public function getBackgroundColors() {
        return array_keys($this->background_colors);
    }

    public function clearScreen() {
        /** @var $Core Core */
        $Core = $this->Core;

        /** @var $Helper Helper */
        $Helper = $Core->Helper();

        if($Helper->isWindows()) {
            system('cls');
        } else {
            system('clear');
        }
    }

    public function getShellHeader() {
        $header = '/**                                  * ' . PHP_EOL .
            ' * J•Frame Shell                     * ' . PHP_EOL .
            ' *                                   * ' . PHP_EOL .
            ' * @copyright since 2008 by Jan Doll * ' . PHP_EOL .
            ' * All Rights Reserved               * ' . PHP_EOL .
            ' *                                  **/' . PHP_EOL . PHP_EOL;

        $headerOutput = $this->getColoredString($header, 'purple', 'white');
        return $headerOutput;
    }

    public function getShellSyntax() {
        return '$ php shell.php[ {{SITE-KEY}}] {{AREA-KEY}} --{{AREA-COMMAND}} -{{SUB-COMMAND}}[ -{{SUB-COMMAND}}]';
    }

    public function getAreasArray() {
        $areas = array(
            'shell' => 'Shell Area'
        );
        return $areas;
    }

    public function getShellCommandListArray() {
        $commandList = array(
            '--help'   => 'For Help Output',
            '--list'   => 'This List',
            '--areas'  => 'List Areas',
            '--models' => 'List Models',
            '--sites'  => 'List Sites',
            '--site'   => 'List Current Site',
        );
        return $commandList;
    }

    public function getShellHelp() {
        /** @var $Sites Sites */
        $Sites = $this->Core->Sites();
        $curSite = (is_object($Sites) && is_array($Sites->getSite())) ? $Sites->getSite() : array();
        $curSiteUrlKey = (count($curSite) && array_key_exists('urlKey',$curSite)) ? ' ' . $curSite['urlKey'] : '';

        $helpText = '/** J•Frame Shell - Help */' . PHP_EOL;
        $helpText .= PHP_EOL;
        $helpText .= '=> Shell Basic Syntax' . PHP_EOL;
        $helpText .= '   "' . $this->getShellSyntax() . '"' . PHP_EOL;
        $helpText .= '=> For a List of Commands type' . PHP_EOL;
        $helpText .= '   "$ php shell.php' . $curSiteUrlKey . ' shell --list"' . PHP_EOL;
        $helpText .= PHP_EOL;

        $helpOutput = $this->getColoredString($helpText, 'green');
        return $helpOutput;
    }

    /**
     * List ModelKeys as Array, if Shell Method found
     *
     * @return array
     */
    public function listShellModels() {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        $shellModels = array();

        $models = $Mvc->getModels();

        foreach($models as $modelKey => $model) {
            $modelClass = $Mvc->modelClass($modelKey);

            if(is_object($modelClass)) {
                if(method_exists($modelClass,'shell')) {
                    $shellModels[$modelKey] = 'Model ' . $this->Core->i18n()->translate($modelKey);
                }
            }
        }

        return $shellModels;
    }

    /**
     * Execute Shell Methods and return their Results
     *
     * @param $commandArray
     * @return string
     */
    public function exec($commandArray) {
        /** @var $Core Core */
        $Core = $this->Core;

        /** @var $Helper Helper */
        $Helper = $Core->Helper();

        /** @var $Sites Sites */
        $Sites = $Core->Sites();

        /** @var $Mvc Mvc */
        $Mvc = $Core->Mvc();

        $siteIdFromCommand = null;
        $siteUrlKeys = array();
        foreach($Sites->getSites() as $site) {
            if (array_key_exists('urlKey', $site)) {
                $siteUrlKeys[$site['urlKey']] = $site['name'];

                foreach($commandArray as $cKey => $arg) {
                    if($arg == $site['urlKey']) {
                        unset($commandArray[$cKey]);
                        $commandArray = array_values($commandArray);
                        $siteIdFromCommand = $site['id'];
                    }
                }
            }
        }

        $curSite = (is_object($Sites)) ? $Sites->getSite($siteIdFromCommand) : array();
        $curSiteUrlKey = (count($curSite) && array_key_exists('urlKey',$curSite)) ? ' ' . $curSite['urlKey'] : '';

        /** Get Models with Shell Method */
        $shellModels = $this->listShellModels();
        $models = $Mvc->getModels();
        $modelKeys = array();
        foreach($models as $model) {
            if (array_key_exists('key', $model)) {
                $modelKeys[$model['key']] = $this->Core->i18n()->translate($model['key']);
            }
        }

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');
        $isAccClass = false;
        $isLoggedIn = false;
        if(array_key_exists('acc',$modelKeys)) {
            $accClass = $Mvc->modelClass('Acc');
            if(is_object($accClass)) {
                $isAccClass = true;
                $isLoggedIn = $accClass->userLoggedIn();
            }
        }

        $this->clearScreen();

        $shellResult = $this->getShellHeader();
        $area = (array_key_exists(0,$commandArray)) ? $commandArray[0] : '';
        $action = (array_key_exists(1,$commandArray)) ? $commandArray[1] : '';
        $subCommand = (array_key_exists(2,$commandArray)) ? $commandArray[2] : '';

        if($isAccClass) {
            if (!$isLoggedIn) {
                if(strpos($subCommand, 'login') === false) {
                    $error = 'Acc => Please Login First!' . PHP_EOL . PHP_EOL;
                    $shellResult .= $this->getColoredString($error, 'red');
                    $shellResult .= '============================================' . PHP_EOL . PHP_EOL;
                    $shellResult .= $this->getShellHelp();
                    $shellResult .= '============================================' . PHP_EOL . PHP_EOL;
                    if ($area != 'acc' && $area != 'shell') {
                        return $shellResult;
                    }
                }
            } else {
                if(strpos($subCommand, 'logout') === false) {
                    $user = $accClass->getUser();
                    $userName = (array_key_exists('name', $user)) ? $user['name'] : '';
                    $userSurName = (array_key_exists('surname', $user)) ? $user['surname'] : '';

                    $isLoggedInText = 'Acc => You are logged in as ' . $userName . ' ' . $userSurName . PHP_EOL . PHP_EOL;
                    $shellResult .= $this->getColoredString($isLoggedInText, 'green');
                    $shellResult .= '============================================' . PHP_EOL . PHP_EOL;
                }

                if ($curSiteUrlKey == '') {
                    $error = 'Site => Set Site First!' . PHP_EOL . PHP_EOL;
                    $shellResult .= $this->getColoredString($error, 'red');
                    $shellResult .= '============================================' . PHP_EOL . PHP_EOL;
                    $shellResult .= $this->getShellHelp();
                    $shellResult .= '============================================' . PHP_EOL . PHP_EOL;
                    if($area != 'acc' && $area != 'shell') {
                        return $shellResult;
                    }
                }
            }
        } else {
            if($curSiteUrlKey == '') {
                $error = 'Site => Set Site First!' . PHP_EOL . PHP_EOL;
                $shellResult .= $this->getColoredString($error, 'red');
                $shellResult .= '============================================' . PHP_EOL . PHP_EOL;
                $shellResult .= $this->getShellHelp();
                $shellResult .= '============================================' . PHP_EOL . PHP_EOL;
                if($area != 'shell') {
                    return $shellResult;
                }
            }
        }

        if($area === ''){
            $shellResult .= $this->getShellHelp();
        } else {
            if(!array_key_exists($area,$this->getAreasArray())) {
                /** @var $Mvc Mvc */
                $Mvc = $this->Core->Mvc();
                $models = $Mvc->getModels();
                if(array_key_exists($area,$models)) {
                    $modelKey = $area;
                    $modelClass = $Mvc->modelClass($modelKey);

                    if (is_object($modelClass)) {
                        if (method_exists($modelClass, 'shell')) {
                            $shellResult .= $modelClass->shell($commandArray);
                        }
                    }
                } else {
                    $shellResult .= $this->getColoredString('Area Model Key "' . $area . '" not found...', 'red');
                }
            } else {
                if($area == 'shell') {

                    if($action !== '') {
                        if(array_key_exists($action,$this->getShellCommandListArray())) {

                            switch ($action) {
                                case '--help':
                                    $shellResult .= $this->getShellHelp();
                                    break;
                                case '--list':
                                    $listResult =  'Shell => Command List:' . PHP_EOL;
                                    foreach($this->getShellCommandListArray() as $commandKey => $commandText) {
                                        $listResult .= '  * ' . $commandKey . ' /** ' . $commandText . ' */' . PHP_EOL;
                                    }
                                    $shellResult .= $this->getColoredString($listResult,'yellow');
                                    break;
                                default:
                                    if(!array_key_exists($action,$this->getShellCommandListArray())) {
                                        $shellResult .= $this->getColoredString('Shell => Action "' . $action . '" not found...' . PHP_EOL . PHP_EOL, 'red');
                                        $shellResult .= $this->getShellHelp();
                                    }
                            }

                            if($action == '--areas') {
                                $listResult =  'Shell => Areas List:' . PHP_EOL;
                                foreach($this->getAreasArray() as $commandKey => $commandText) {
                                    $listResult .= '  * ' . $commandKey . ' /** ' . $commandText . ' */' . PHP_EOL;
                                }
                                foreach($shellModels as $modelKey => $modelText) {
                                    $listResult .= '  * ' . $modelKey . ' /** ' . $modelText . ' */' . PHP_EOL;
                                }
                                $shellResult .= $this->getColoredString($listResult,'yellow');
                            }

                            if($action == '--models') {
                                $listResult =  'Shell => Models List:' . PHP_EOL;
                                foreach($modelKeys as $modelKey => $modelText) {
                                    $listResult .= '  * ' . $modelKey . ' /** ' . $modelText . ' */' . PHP_EOL;
                                }
                                $shellResult .= $this->getColoredString($listResult,'yellow');
                            }

                            if($action == '--sites') {
                                $listResult =  'Shell => Sites List:' . PHP_EOL;
                                foreach($siteUrlKeys as $siteKey => $siteName) {
                                    $listResult .= '  * ' . $siteKey . ' /** ' . $siteName . ' */' . PHP_EOL;
                                }
                                $shellResult .= $this->getColoredString($listResult,'yellow');
                            }

                            if($action == '--site') {
                                $listResult =  'Shell => Current Site:' . PHP_EOL;
                                $listResult .= '  * ' . print_r($curSite, true) . PHP_EOL;
                                $shellResult .= $this->getColoredString($listResult,'yellow');
                            }

                        } else {
                            $shellResult .= $this->getColoredString('Shell => Action "' . $action . '" not found...', 'red');
                        }
                    } else {
                        $shellResult .= $this->getColoredString('Shell => Action not given...' . PHP_EOL . PHP_EOL, 'red');
                        $shellResult .= $this->getShellHelp();
                    }
                }
            }
        }
        $shellResult .= PHP_EOL;

        return $shellResult;
    }
}