<?php
/**
 * Cron Helper
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** Class Cron */
class Cron
{
    /** @var $Core Core */
    private $Core;

    /**
     * Hooks constructor.
     * @param $Core Core
     */
    public function __construct($Core)
    {
        $this->Core = $Core;
    }

    /**
     * List ModelKeys as Array, if Cron Method found
     *
     * @return array
     */
    public function listCronModels() {
        $cronModels = array();

        $models = $this->Core->Mvc()->getModels();

        foreach($models as $modelKey => $model) {
            $modelClass = $this->Core->Mvc()->modelClass($modelKey);

            if(is_object($modelClass)) {
                if(method_exists($modelClass,'cron')) {
                    $cronModels[] = $modelKey;
                }
            }
        }

        return $cronModels;
    }

    /**
     * Run All Model Cron Methods and return their Results
     *
     * @return array
     */
    public function run() {
        $cronResults = array();
        $models = $this->Core->Mvc()->getModels();

        foreach($models as $modelKey => $model) {
            $modelClass = $this->Core->Mvc()->modelClass($modelKey);

            if(is_object($modelClass)) {
                if(method_exists($modelClass,'cron')) {
                    $cronResults[$modelKey] = $modelClass->cron();
                }
            }
        }

        return $cronResults;
    }
}