<?php
/**
 * Config Getter and Setter
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** Config Class */
class Config
{
    /**
     * @var $Core Core
     */
    private $Core;
    private $_configDefaults = null;
    private $_usedConfig = null;

    /**
     * Config constructor.
     * @param $Core
     */
    function __construct($Core)
    {
        $this->Core = $Core;
    }

    /**
     * get Default Config from File
     *
     * @return array
     */
    private function _getDefaults()
    {
        $_configDefaults = $this->_configDefaults;

        if($_configDefaults === null) {
            $_configDefaults = array();

            $coreConfigPath = $this->Core->getRootPath() . DS . 'config';
            $defaultConfigFilePath = $coreConfigPath . DS . 'config.default.php';
            if(!file_exists($defaultConfigFilePath)) {
                $errorMsg = 'Could not load default config file!';
                $this->Core->Log($errorMsg . ' Estimated Filepath = "' . $defaultConfigFilePath . '"', '', true);
                die($errorMsg . ' See Log for more Information!');
            }
            include_once($defaultConfigFilePath);

            $this->_configDefaults = $_configDefaults;
        }

        return $_configDefaults;
    }

    private function _prepareConfigData($data) {

        $config = array();

        if(is_array($data) && count($data)) {

            $id = (array_key_exists('id', $data)) ? $data['id'] : false;

            if ($id !== false) {
                $key    = (array_key_exists('key', $data))             ? $data['key']             : '';
                $value  = (array_key_exists('value', $data))           ? $data['value']           : '';
                $owner  = (array_key_exists('owner', $data))           ? $data['owner']           : 'system';
                $type   = (array_key_exists('type', $data))            ? $data['type']            : 'config';
                $siteId = (array_key_exists('id_system_sites', $data)) ? $data['id_system_sites'] : 0;

                if($key != '') {
                    $config['id']        = $id;
                    $config['key']       = $key;
                    $config['value']     = $value;
                    $config['owner']     = $owner;
                    $config['type']      = $type;
                    $config['siteId']    = $siteId;

                    /** If Value is a serialized array */
                    try {
                        if ((strpos($value,'{') !== false) && is_array(@unserialize($value))) {
                            $config['value'] = unserialize($value);
                        }
                    } catch (Exception $e) {
                        $note = 'Unserialize Error on Config Key "' . $key . '": ' . $e->getMessage();

                        $this->Core->Log($note,'core_config',true);
                    }
                }
            }
        }

        return $config;
    }

    public function getKeys($siteId = null)
    {
    	$result = array();
		$cKeysArray = array();
		
    	if($siteId !== null && is_numeric($siteId)) {
	        $sql = 'SELECT `key` FROM `' . DB_TABLE_PREFIX . 'system_config` WHERE `id_system_sites` = :id_system_sites GROUP BY `key`';
	        $params = array(
	            'id_system_sites' => $siteId
	        );
	    } else {
	        $sql = 'SELECT `key` FROM `' . DB_TABLE_PREFIX . 'system_config` GROUP BY `key`';
	        $params = array();
	    }
        $resultDB = $this->Core->Db()->fromDatabase($sql, '@raw', $params);
        
        $defaultConfig = $this->_getDefaults();
        
        $defaultConfig = (array_key_exists(0, $defaultConfig) && is_array($defaultConfig[0])) ? $defaultConfig[0] : array();
		foreach($defaultConfig as $dcKey => $dcArr) {
			$cKeysArray[] = $dcKey;
		}
        
		$configKeys = $resultDB;
		foreach($configKeys as $kId => $kArr) {
			if(is_array($kArr) && array_key_exists('key',$kArr) && is_string($kArr['key'])) {
				$cKeysArray[] = $kArr['key'];
			}
		}
		array_unique($cKeysArray);
		sort($cKeysArray);
		
		$result = $cKeysArray;

        // return Owner Array
        return $result;
    }

    public function getOwners($siteId = 0)
    {
        $sql = 'SELECT `owner` FROM `' . DB_TABLE_PREFIX . 'system_config` WHERE `id_system_sites` = :id_system_sites GROUP BY `owner`';
        $params = array(
            'id_system_sites' => $siteId
        );
        $result = $this->Core->Db()->fromDatabase($sql, '@raw', $params);

        // return Owner Array
        return $result;
    }

    public function getOwnerNames()
    {
        $ownerNames = array(
            'system' => 'System',
            'site'	 => 'Site',
            'mvc'    => 'Mvc',
            'design' => 'Design',
            'i18n'   => 'i18n',
        );

        // return Area Names
        return $ownerNames;
    }

    public function getTypes($siteId = 0)
    {
        $sql = 'SELECT `type` FROM `' . DB_TABLE_PREFIX . 'system_config` WHERE `id_system_sites` = :id_system_sites GROUP BY `type`';
        $params = array(
            'id_system_sites' => $siteId
        );
        $result = $this->Core->Db()->fromDatabase($sql, '@raw', $params);

        // return Type Array
        return $result;
    }

    public function get($config_key, $siteId = null, $onlyValue = true, $fromCache = true)
    {
        /** Get Site from Session */
        $sessSite = (array_key_exists('site',$_SESSION) && is_array($_SESSION['site'])) ? $_SESSION['site'] : array();
        $sessSiteId = (array_key_exists('id', $sessSite)) ? $sessSite['id'] : 0;

        $siteId = ($siteId !== null) ? $siteId : $sessSiteId;

        $usedConfig = $this->_usedConfig;
        if(
            $fromCache &&
            is_array($usedConfig) &&
            array_key_exists($siteId, $usedConfig) &&
            array_key_exists($config_key, $usedConfig[$siteId]) &&
            is_array($usedConfig[$siteId][$config_key]) &&
            count($usedConfig[$siteId][$config_key])
        ) {
            $get_config_array = $usedConfig[$siteId][$config_key];
            $get_config = (array_key_exists('value', $get_config_array)) ? $get_config_array['value'] : '';
        } else {

            $get_default_config_array = $this->_getDefaults();

            $get_config_array = (
                array_key_exists(0, $get_default_config_array) &&
                array_key_exists($config_key, $get_default_config_array[0]) &&
                is_array($get_default_config_array[0][$config_key])
            ) ? $get_default_config_array[0][$config_key] : array('value' => '');

            $get_config = (array_key_exists('value', $get_config_array)) ? $get_config_array['value'] : '';

            $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'system_config` WHERE `key` = :key AND (`id_system_sites` = :id_root_site OR `id_system_sites` = :id_curr_site)';
            $params = array(
                'key' => $config_key,
                'id_root_site' => 0,
                'id_curr_site' => $siteId
            );
            $result = $this->Core->Db()->fromDatabase($sql, '@raw', $params);

            if (is_array($result) && count($result)) {
                $config_db_array = array();
                foreach ($result as $row) {
                    $preparedData = $this->_prepareConfigData($row);
                    if (count($preparedData)) {
                        $config_db_array[$row['id_system_sites']][$row['key']] = $this->_prepareConfigData($row);
                        $config_db_array[$row['id_system_sites']][$row['key']]['fromDB'] = true;
                    }
                }

                $get_db_root_config_array = (
                    array_key_exists(0, $config_db_array) &&
                    array_key_exists($config_key, $config_db_array[0]) &&
                    is_array($config_db_array[0])
                ) ? $config_db_array[0][$config_key] : array();

                $get_db_root_config = (count($get_db_root_config_array) && array_key_exists('value', $get_db_root_config_array) && $get_db_root_config_array['value'] != '') ? $get_db_root_config_array['value'] : $get_config;

                $get_db_curr_config_array = (
                    array_key_exists($siteId, $config_db_array) &&
                    array_key_exists($config_key, $config_db_array[$siteId]) &&
                    is_array($config_db_array[$siteId])
                ) ? $config_db_array[$siteId][$config_key] : array('value' => '');

                $get_db_curr_config = (count($get_db_curr_config_array) && array_key_exists('value', $get_db_curr_config_array) && $get_db_curr_config_array['value'] != '') ? $get_db_curr_config_array['value'] : $get_db_root_config;

                $get_config_array = $get_db_curr_config_array;
                $get_config = $get_db_curr_config;
            }

            if(!is_array($usedConfig)) {
                $usedConfig = array();
            }

            $usedConfig[$siteId][$config_key] = $get_config_array;
            $this->_usedConfig = $usedConfig;
        }
        return ($onlyValue) ? $get_config : $get_config_array;
    }

    public function set($config_key, $config_value = '', $siteId = null, $config_owner = 'custom', $config_type = 'config')
    {
        $curSite   = $this->Core->Sites()->getSite();
        $curSiteId = (count($curSite)) ? $curSite['id'] : 0;

        $siteId = ($siteId !== null) ? $siteId : $curSiteId;

        $accClass = $this->Core->Mvc()->modelClass('Acc');
        $curUser  = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser) && array_key_exists('id',$curUser)) ? $curUser['id'] : 0;

        $db_owner               = $config_owner;
        $db_type                = $config_type;
        $db_key                 = $config_key;
        $db_value               = (is_array($config_value)) ? serialize($config_value) : $config_value;
        $db_id_users__changedBy = $curUserId;
        $db_created             = "NOW()";

        $return = false;

        $configArray = $this->get($config_key, $siteId, false);

        if(
            count($configArray) &&
            array_key_exists('fromDB',$configArray) &&
            $configArray['fromDB'] === true
        ){
            $db_owner = ($db_owner != '' && $db_owner != $configArray['owner']) ? $db_owner : $configArray['owner'];
            $update_query_set_config = ' 
                UPDATE
                    `' . DB_TABLE_PREFIX . 'system_config`
                SET
                    `value` = :value,
                    `owner` = :owner,
                    `id_system_sites` = :id_system_sites,
                    `id_acc_users__changedBy` = :id_acc_users__changedBy
                WHERE
                    `key` = :key';
            $params = array(
                'value'   => $db_value,
                'owner'   => $db_owner,
                'key'     => $db_key,
                'id_system_sites' => $siteId,
                'id_acc_users__changedBy' => $db_id_users__changedBy
            );
            $this->Core->Db()->toDatabase($update_query_set_config, $params);
            $return = true;
        } else {
            $insert_into_query_set_config = 'INSERT INTO `' . DB_TABLE_PREFIX . 'system_config` (`owner`, `type`, `key`, `value`, `id_system_sites`, `id_acc_users__changedBy`) VALUES (:owner, :type, :key, :value, :id_system_sites, :id_acc_users__changedBy);';

            $params = array(
                'owner'               => $db_owner,
                'type'                => $db_type,
                'key'                 => $db_key,
                'value'               => $db_value,
                'id_system_sites'     => $siteId,
                'id_acc_users__changedBy' => $db_id_users__changedBy
            );

            if(count($configArray)) {

                $configArray_owner = (array_key_exists('owner', $configArray)) ? $configArray['type'] : '';
                $configArray_type = (array_key_exists('type', $configArray)) ? $configArray['type'] : '';

                $params['owner'] = ($config_owner != $configArray_owner) ? $config_owner : $configArray_owner;
                $params['type']  = ($config_type  != $configArray_type)  ? $config_type  : $configArray_type;
            }

            $this->Core->Db()->toDatabase($insert_into_query_set_config, $params);
            $return = true;
        }

        return $return;
    }
}