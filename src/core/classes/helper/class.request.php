<?php
/**
 * Requests from URL
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** Request Class */
class Request
{
    private $Core;
    private $path   = null;
    private $params = null;
    private $isAjax = false;

    function __construct($Core)
    {
        $this->Core = $Core;

        $this->_setSessionInfo();
    }

    private function _setSessionInfo() {
        /** Set Session Info */
        $_SESSION['urlPath']   = $this->getPath();
        $_SESSION['urlKey']    = $this->getUrlKey();
        $_SESSION['urlParams'] = $this->getParams();
    }

    /**
     * get Secure State
     *
     * @return bool
     */
    public function isSecure() {
        $secure_connection = false;

        if(isset($_SERVER['HTTPS'])) {
            if ($_SERVER['HTTPS'] == "on") {
                $secure_connection = true;
            }
        }

        return $secure_connection;
    }

    /**
     * get Current Url
     *
     * @return string
     */
    public function getCurrUrl() {
        return (isset($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    }

    /**
     * get HTTP Referrer
     *
     * @return string
     */
    public function getReferrer() {
        $sessReferrer = (array_key_exists('JF_REFERRER', $_SESSION)) ? $_SESSION['JF_REFERRER'] : '';
        $serverReferrer = (isset($_SERVER) && is_array($_SERVER) && array_key_exists('HTTP_REFERER', $_SERVER)) ? $_SERVER['HTTP_REFERER'] : '';
        
        return ($sessReferrer !== '') ? $sessReferrer : $serverReferrer;
    }

    /**
     * get Ajax State
     *
     * @return bool
     */
    public function isAjax() {
        $isAjax = $this->isAjax;
        return $isAjax;
    }

    /**
     * Get Path from URL
     *
     * @return bool|string
     */
    public function getPath() {
        $is_cli = (!isset($_SERVER['SERVER_SOFTWARE']) && (php_sapi_name() == 'cli' || (array_key_exists('argv',$_SERVER) && is_numeric($_SERVER['argc']) && $_SERVER['argc'] > 0)));
        $path = $this->path;
        $getPath = (isset($_GET['path']) && $_GET['path'] != "") ? trim($_GET['path'],'/')  : '';
        $getRequestUri = (!$is_cli) ? $_SERVER['REQUEST_URI'] : '';

        if($this->isAjax() && (strpos($path, 'ajax') || strpos($getPath, 'ajax'))) {
            $path = str_replace('ajax/', '', $path);
            $getPath = str_replace('ajax/', '', $getPath);
        }

        if($path === null || $path != '/' . $getPath) {

            $ajaxCheckPath = ($path !== null) ? $path : $getPath;

            if(strpos($ajaxCheckPath, 'ajax') !== false) {
                $this->isAjax = true;
                $getPath = str_replace('ajax/', '', $ajaxCheckPath);
            }

            if ($getPath != "") {
                $path = '/' . $getPath;
            } else {
                $path = (!$is_cli) ? substr($getRequestUri, strlen($_SERVER['SCRIPT_NAME'])) : '/';
            }

            $this->path = $path;
        }

        return $path;
    }

    /**
     * Get current UrlKey
     *
     * @return string
     */
    public function getUrlKey(){
        $request = $this->getPath();

        $url_key = '';

        if($request != ""){

            $url_parts = preg_split('#/#', $request, -1, PREG_SPLIT_NO_EMPTY);

            $first_path_part  = (array_key_exists(0,$url_parts)) ? strip_tags(trim($url_parts[0])) : '';
            $second_path_part = (array_key_exists(1,$url_parts)) ? strip_tags(trim($url_parts[1])) : '';

            if($first_path_part != '' && !preg_match('#\b[a-z]{1,2}\b#', $first_path_part)){
                $url_key = $first_path_part;
            } else {
                if($second_path_part != ''){
                    $url_key = $second_path_part;
                }
            }

        }
        return $url_key;
    }

    /**
     * Get Params String
     *
     * @return string
     */
    public function getParams()
    {
        /** Get Site from Session */
        $sessSite = (array_key_exists('site',$_SESSION) && is_array($_SESSION['site'])) ? $_SESSION['site'] : array();
        $sessSiteUrlKey = (array_key_exists('urlKey', $sessSite)) ? $sessSite['urlKey'] : '';

        $request = $this->getPath();
        $params = $request;
        if ($request != '') {
            $url_parts = preg_split('#/#', $request, -1, PREG_SPLIT_NO_EMPTY);
            $first_path_part = (array_key_exists(0, $url_parts)) ? strip_tags(trim($url_parts[0])) : '';


            /** Remove Site Url Key */
            if($first_path_part == $sessSiteUrlKey) {
                $request = str_replace('/' . $first_path_part, '', '/' . trim($request,'/'));

                $url_parts = preg_split('#/#', $request, -1, PREG_SPLIT_NO_EMPTY);
                $first_path_part = (array_key_exists(0, $url_parts)) ? strip_tags(trim($url_parts[0])) : '';

                $params = $request;
            }

            /** Remove Lang Code */
            if (preg_match('#\b[a-z]{2}\b#', $first_path_part)) {
                $params = str_replace('/' . $first_path_part, '', '/' . trim($request,'/'));
            }
        }

        return trim($params,'/');
    }

    public function getPost($key = '') {
        $post = (isset($_POST)) ? $_POST : array();

        $files = (isset($_FILES)) ? $_FILES : array();

        if(count($files)) {
            $post['_FILES'] = $files;
        }

        return ($key != '' && isset($post[$key])) ? $post[$key] : $post;
    }

    public function redirect($url, $type = '302', $exit = true) {
        $_SESSION['JF_REFERRER'] = $this->getCurrUrl();
        $_SERVER['HTTP_REFERER'] = $this->getCurrUrl();

        header("Location: " . $url, true, $type);

        if($exit) {
            exit;
        }
    }
}