<?php
/**
 * Media Model - Main Class
 *
 * Some Methods implemented from Source by Planet ITservices GmbH & Co. KG
 * and by Michael Milawski <https://github.com/millsoft/php-mysql-tunnel>
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** Set Path to inc Folder */
$incPath = JF_ROOT_DIR . DS . 'core' . DS . 'models' . DS . 'media' . DS . 'inc';

/** include Imaging Class */
require_once($incPath . DS . 'imaging' . DS . 'class.imaging.php');

/** include Vendor Autoload */
require_once($incPath . DS . 'vendor' . DS . 'autoload.php');

/** Media Class */
class Media {
    public $_version = '9.0.0';

    /**
     * @var Core
     * @var Mvc
     */
    private $Core;
    private $Mvc;
    private $mediaFilesystem;
    private $_dirs = null;
    private $_diskUsage = null;
    private $_imagingClass = null;
    private $_uploadsFolderName = 'uploads';
    private $_galleriesStyleLogoFolderName = '_galleries_styles_logos';
    private $_wysiwygFolderName = '_wysiwyg';
    private $_cronStartedAt;
    private $_cronArchiveFtpSettings;
    public $maxClientDiskSpace = 21474836480; /** Bytes (20GB) */
    public $postMaxSize = 5242880; /** Bytes (5MB) */

    /**
     * Media constructor.
     * @param $Core Core
     */
    function __construct($Core)
    {
        $this->Core = $Core;
        $this->Mvc = $Core->Mvc();

        $this->_mediaInit();
    }

    public function getVersion() {
        return $this->_version;
    }

    public function getDbVersion() {
        $dbVersion_option = $this->Core->Config()->get('media_version',0);
        $dbVersion = ($dbVersion_option !== '') ? $dbVersion_option : 0;
        return $dbVersion;
    }

    /** Update Methods */

    public function _update380($execute=true) {
        /** May Add 'max_lifetime' Column */

        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '3.8.0';
        $return['changelog'] = 'Added Max Lifetime SQL Table Column to Dirs Table';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {

            $new_col_exits = false;

            $check_for_new_col_SQL = 'SELECT IF(EXISTS(SELECT `id` FROM `' . DB_TABLE_PREFIX . 'media_dirs` WHERE `max_lifetime` IS NOT NULL), 1, 0) as "exists"';

            try {
                $result = $this->Core->Db()->fromDatabase($check_for_new_col_SQL, '@simple');
                if ($result !== 0) {
                    $new_col_exits = true;
                    $return['success'] = true;
                }
            } catch (Exception $e) {
                $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
            }

            if (!$new_col_exits) {
                try {
                    $addCol_SQL = 'ALTER TABLE `' . DB_TABLE_PREFIX . 'media_dirs` ADD COLUMN `max_lifetime` TIMESTAMP NOT NULL DEFAULT 0 AFTER `id_media_galleries_styles`;';

                    $sqlResult = $this->Core->Db()->toDatabase($addCol_SQL);

                    if (!$sqlResult) {
                        // Error
                        $return['sqlResult'] = $sqlResult;
                    } else {
                        $return['success'] = true;
                    }
                } catch (Exception $e) {
                    $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
                }
            }
        }

        if($return['success']) {
            unset($return['error']);
        }

        return $return;
    }

    public function _update390($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '3.9.0';
        $return['changelog'] = 'Enhanced auto deletion functionality and fixed a bug in syncing media folders action.';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update400($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '4.0.0';
        $return['changelog'] = 'Media Browser now with Mass Delete Action and List / Grid View toggle Button';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update401($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '4.0.1';
        $return['changelog'] = 'CSS & JS Responsive Fixes';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update402($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '4.0.2';
        $return['changelog'] = 'Implemented correct use for Helper Method "isValidTimeStamp" and added same Auto Deletion Info to Dir Info like in Dir edit Auto Delete';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update410($execute=true) {
        /** May Add 'show_in_parent_gallery' Column */

        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '4.1.0';
        $return['changelog'] = 'Added "show_in_parent_gallery" SQL Table Column to Dirs Table';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {

            $new_col_exits = false;

            $check_for_new_col_SQL = 'SELECT IF(EXISTS(SELECT `id` FROM `' . DB_TABLE_PREFIX . 'media_dirs` WHERE `show_in_parent_gallery` IS NOT NULL), 1, 0) as "exists"';

            try {
                $result = $this->Core->Db()->fromDatabase($check_for_new_col_SQL, '@simple');
                if ($result !== 0) {
                    $new_col_exits = true;
                    $return['success'] = true;
                }
            } catch (Exception $e) {
                $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
            }

            if (!$new_col_exits) {
                try {
                    $addCol_SQL = 'ALTER TABLE `' . DB_TABLE_PREFIX . 'media_dirs` ADD COLUMN `show_in_parent_gallery` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `id_media_galleries_styles`;';

                    $sqlResult = $this->Core->Db()->toDatabase($addCol_SQL);

                    if (!$sqlResult) {
                        // Error
                        $return['sqlResult'] = $sqlResult;
                    } else {
                        $return['success'] = true;
                    }
                } catch (Exception $e) {
                    $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
                }
            }
        }

        if($return['success']) {
            unset($return['error']);
        }

        return $return;
    }

    public function _update411($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '4.1.1';
        $return['changelog'] = 'Added "Show Dir in parent Gallery" Functionality:<ul><li>Edit / Create Actions</li><li>Enhanced Unlock Functionality in Gallery View</li></ul>';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update450($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '4.5.0';
        $return['changelog'] = 'Enhanced Thumbnail Creation Error Handling.';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update500($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '5.0.0';
        $return['changelog'] = 'Performance Enhancements';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update510($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '5.1.0';
        $return['changelog'] = 'Enhanced Error Handling and FileActions for BIGDATA';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update550($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '5.5.0';
        $return['changelog'] = 'Added Shell Method';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update560($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '5.6.0';
        $return['changelog'] = 'Added Send Dir Link via Modal Window functionality';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update600($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '6.0.0';
        $return['changelog'] = 'Removed Event Stuff!';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update650($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '6.5.0';
        $return['changelog'] = 'Added High Security for viewing Files in Media Dirs!';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update655($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '6.5.5';
        $return['changelog'] = 'Fixed Bugs for secure File Url in Gallery Styles and added some public dir roots for secure File Urls';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update700($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '7.0.0';
        $return['changelog'] = '&nbsp;&nbsp;&bull;&nbsp;Outsources inc components for loading via Composer / NPM<br />&nbsp;&nbsp;&bull;&nbsp;Added Video.JS to Play Audio / Video Files directly in Thumbnail';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update800($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '8.0.0';
        $return['changelog'] = '&nbsp;&nbsp;&bull;&nbsp;Setting the Basic Gallery Stuff on Class Init<br />&nbsp;&nbsp;&bull;&nbsp;Implemented Auto Unlock Gallery on List Dirs Action';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update820($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '8.2.0';
        $return['changelog'] = 'Added Thumbnail Max Width and Max Height via Config';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update850($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '8.5.0';
        $return['changelog'] = '&nbsp;&nbsp;&bull;&nbsp;Added Use Content Delivery and Chunked File Upload via Config<br />&nbsp;&nbsp;&bull;&nbsp;ReArranged Dropzone Init in Media JS';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update860($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '8.6.0';
        $return['changelog'] = 'Cron Archive FTP Settings outsourced into Config';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update900($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '9.0.0';
        $return['changelog'] = 'Added FineUploader and new MediaFilesystem Class for better Upload Handling';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    /** Base Media Methods */

    private function _mediaInit() {
    	$this->mediaFilesystem = new MediaFilesystem($this->Core);
        $this->_mayCreateNecessaryMediaFolders();
        $this->_setMaxClientDiscSpace();
        $this->_setPostMaxSize();
        $this->_setBasicGalleryStuff();
        $this->_setBasicMediaContentStuff();
        $this->_setMvcPageHeadMeta();
        $this->_setMvcPageBeforeBodyEnds();
    }

    private function _mediaBaseFolderName()
    {
        $sitesClass  = $this->Core->Sites();
        $site       = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
        $curSiteKey = (count($site)) ? $site['urlKey'] : '';

        return $curSiteKey;
    }

    private function _mediaUploadsFolderName()
    {
        return $this->_uploadsFolderName;
    }

    private function _mediaGalleriesStyleLogoFolderName()
    {
        return $this->_galleriesStyleLogoFolderName;
    }

    private function _mayCreateNecessaryMediaFolders()
    {
        $mediaModelUsable = true;
        if(is_writable($this->Core->getRelPath().DS.'var')) {
            $this->_mayCreateRootMediaFolder();
            if($this->mediaFolderWritable()) {
                $this->_mayCreateBaseFolder();
                $this->_mayCreateUploadsFolder();
                $this->_mayCreateGalleryStyleLogoFolder();
            } else {
                $mediaModelUsable = false;
                $note = $this->Core->i18n()->translate('Media Folder is nicht beschreibbar...');
                $type  = 'danger';
                $kind  = 'bs-alert';
                $title = $this->Core->i18n()->translate('Fehler') . '!';
                $this->Core->setNote($note, $type, $kind, $title);
            }
        } else {
            $mediaModelUsable = false;
            $note = $this->Core->i18n()->translate('Der Ordner \'var\' im System ist nicht beschreibbar...');
            $type  = 'danger';
            $kind  = 'bs-alert';
            $title = $this->Core->i18n()->translate('Fehler') . '!';
            $this->Core->setNote($note, $type, $kind, $title);
        }

        if(!$mediaModelUsable && $this->Mvc->getActionKey() != 'mainerror') {
            $redirectUrl = $this->Mvc->getModelUrl('media') . '/index/mainerror';
            $this->Core->Request()->redirect($redirectUrl);
        }
    }

    private function _setMvcPageHeadMeta()
    {
        $site   = $this->Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $getPageHeadMeta = $this->Mvc->getPageHeadMeta();

        $getModelKey      = $this->Mvc->getModelKey();
        $getControllerKey = $this->Mvc->getControllerKey();
        $getActionKey     = $this->Mvc->getActionKey();

        if($getModelKey == 'media' && strpos($getPageHeadMeta, 'Media Head Meta') === false) {
            $incUrl = $this->Core->getCoreUrl() . '/models/media/inc';

            $mediaModelAjaxUrl = $this->Core->Mvc()->getModelAjaxUrl();

            $mvcPageHeadMeta = PHP_EOL . '<!-- Media Head Meta START -->';

            if($getControllerKey == 'galleries' && $getActionKey == 'view') {
                $mvcPageHeadMeta .= '<link rel="stylesheet" type="text/css"  href="' . $incUrl . '/node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.css" />';
            }

            if($getControllerKey == 'dirs' || $getControllerKey == 'galleries') {
                $mvcPageHeadMeta .= '<link rel="stylesheet" type="text/css"  href="' . $incUrl . '/node_modules/video.js/dist/video-js.min.css" />';
            }

            $mvcPageHeadMeta .= '<link rel="stylesheet" type="text/css"  href="' . $incUrl . '/assets/css/media.css" />';
            $mvcPageHeadMeta .= '<script type="text/javascript">';
            $mvcPageHeadMeta .= '    var mediaModelAjaxUrl = \'' . $mediaModelAjaxUrl . '\';';
            if($getControllerKey == 'dirs' || $getControllerKey == 'galleries') {
                $mvcPageHeadMeta .= PHP_EOL;
                $mvcPageHeadMeta .= '    var maxUsage = \'' . $this->maxClientDiskSpace . '\';';
                $mvcPageHeadMeta .= PHP_EOL;
                $mvcPageHeadMeta .= '    var isUsage = \'' . $this->getDiskUsage() . '\';';
                $mvcPageHeadMeta .= PHP_EOL;
                $mvcPageHeadMeta .= '    var maxFileSize = \'' . $this->postMaxSize . '\';';

                if($this->useChunkedFileUpload()) {
                    $mvcPageHeadMeta .= PHP_EOL;
                    $mvcPageHeadMeta .= '    var chunkedFileUpload = 1;';
                }
            }
            $mvcPageHeadMeta .= PHP_EOL;
            $mvcPageHeadMeta .= '    var mediaGalleriesStyles_logoBaseUrl = \'' . $this->getGalleryStyleLogoFolderUrl() .'\';';
            $mvcPageHeadMeta .= '</script>';

            $mvcPageHeadMeta .= PHP_EOL . '<!-- Media Head Meta END -->';
            $mvcPageHeadMeta .= PHP_EOL;

            $this->Core->Mvc()->addPageHeadMeta($mvcPageHeadMeta);
        }
    }

    private function _setMvcPageBeforeBodyEnds()
    {
        $getPageBeforeBodyEnd = $this->Mvc->getPageBeforeBodyEnd();

        $getModelKey      = $this->Mvc->getModelKey();
        $getControllerKey = $this->Mvc->getControllerKey();
        $getActionKey     = $this->Mvc->getActionKey();

        if ($getModelKey == 'media' && strpos($getPageBeforeBodyEnd, 'Media Before Body Ends') === false) {
            $incUrl = $this->Core->getCoreUrl() . '/models/media/inc';

            $mvcPageBeforeBodyEnd = PHP_EOL . '<!-- Media Before Body Ends START -->';

            $mvcPageBeforeBodyEnd .= '<script src="' . $incUrl . '/node_modules/jquery-lazy/jquery.lazy.js"></script>';

            if ($getControllerKey == 'dirs' || $getControllerKey == 'galleries') {
                $mvcPageBeforeBodyEnd .= '<script src="' . $incUrl . '/node_modules/dropzone/dist/dropzone.js"></script>';

                $mvcPageBeforeBodyEnd .= '<script src="' . $incUrl . '/node_modules/video.js/dist/video.min.js"></script>';
                $mvcPageBeforeBodyEnd .= '<script src="' . $incUrl . '/node_modules/video.js/dist/ie8/videojs-ie8.min.js"></script>';
            }

            if($getControllerKey == 'galleries' && $getActionKey == 'view') {
                $mvcPageBeforeBodyEnd .= '<script src="' . $incUrl . '/node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js"></script>';
            }

            $mvcPageBeforeBodyEnd .= '<script src="' . $incUrl . '/assets/js/media.js"></script>';

            $mvcPageBeforeBodyEnd .= PHP_EOL . '<!-- Media Before Body Ends END -->';
            $mvcPageBeforeBodyEnd .= PHP_EOL;

            $this->Core->Mvc()->addPageBeforeBodyEnd($mvcPageBeforeBodyEnd);

        }
    }

    private function _setMaxClientDiscSpace() {
        $sitesClass  = $this->Core->Sites();
        $site       = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
        $siteId     = (count($site) && array_key_exists('id',$site)) ? $site['id'] : 0;

        $maxClientDiskSpace = $this->Core->Config()->get('maxClientDiskSpace', $siteId);

        $this->maxClientDiskSpace = ($maxClientDiskSpace > 0) ? $maxClientDiskSpace : 21474836480;
    }

    private function _setPostMaxSize() {
        $postMaxSize = ini_get('post_max_size');

        $postMaxSize = trim($postMaxSize);

        switch (substr ($postMaxSize, -1))
        {
            case 'M': case 'm': $postMaxSize = (int)$postMaxSize * 1048576; break;
            case 'K': case 'k': $postMaxSize = (int)$postMaxSize * 1024; break;
            case 'G': case 'g': $postMaxSize = (int)$postMaxSize * 1073741824; break;
            default: $postMaxSize = 52428804;
        }

        $this->postMaxSize = $postMaxSize;
    }
    
    public function getMediaFilesystem() {
    	return $this->mediaFilesystem;
    }

    /** Acc Public Access Methods */

    public function getPublicControllerKeys() {
        return array(
            'galleries',
            'dirs',
            'api'
        );
    }

    public function getPublicActionKeys() {
        return array(
            'view',
            'download',
            'contents'
        );
    }

    /** Media Helper Methods */

    public function getMediaRootRelPath() {
        return $this->Core->getRelPath().DS.'var'.DS.'media';
    }

    public function getMediaRelPath() {
        $getMediaRootRelPath = $this->getMediaRootRelPath();
        $mediaBaseFolderName = $this->_mediaBaseFolderName();
        $mbfnPath = ($mediaBaseFolderName !== '') ? DS . $mediaBaseFolderName : DS;

        return rtrim($getMediaRootRelPath, DS) . $mbfnPath;
    }

    public function getMediaRootUrl() {
        return str_replace("/index.php?path=", "", $this->Core->getBaseUrl()).'var/media';
    }

    public function getMediaUrl() {
        $getMediaRootUrl = $this->getMediaRootUrl();
        $mediaBaseFolderName = $this->_mediaBaseFolderName();
        $mbfnUrlSlug = ($mediaBaseFolderName !== '') ? $mediaBaseFolderName . '/' : '';

        return rtrim($getMediaRootUrl, '/') . '/' . $mbfnUrlSlug;
    }

    public function getMediaUploadsRelPath() {
        $_uploadsFolderName = $this->_mediaUploadsFolderName();
        $mediaPath = $this->getMediaRelPath();

        return rtrim($mediaPath, DS) . DS . $_uploadsFolderName;
    }

    public function mediaFolderExists() {
        $mediaRootPath = $this->getMediaRootRelPath();
        return is_dir($mediaRootPath);
    }

    public function mediaFolderWritable() {
        $mediaRootPath = $this->getMediaRootRelPath();
        return is_writable($mediaRootPath);
    }

    public function getDiskUsage($asPercent = false) {
        $diskUsage = $this->_diskUsage;
        if($diskUsage === null) {
            $diskUsage = 0;
            /** Bytes */
            $checkFullPath = $this->getMediaUploadsRelPath();
            if (is_dir($checkFullPath)) {
                $diskUsage = $this->folderSize($checkFullPath);
            }
            $this->_diskUsage = $diskUsage;
        }

        if($asPercent) {
            $diskUsagePercent = 0;
            if($diskUsage != 0) {
                $maxClientDiskSpace = $this->maxClientDiskSpace;
                $prePercent = number_format($diskUsage * (100 / $maxClientDiskSpace), 2, ',', '.');
                $diskUsagePercent = ($prePercent > 100) ? 100 : ($prePercent < 0) ? 0 : $prePercent;
            }
            $diskUsage = $diskUsagePercent;
        }
        return $diskUsage;
    }

    public function formatBytes($size, $precision = 2, $asString = true)
    {
    	if ($size == 0) { return 0; } // if zero, return zero
    	
        $suffixTranslated = array(
            '' => '',
            'K' => $this->Core->i18n()->translate('kB'), /** Kilobyte */
            'M' => $this->Core->i18n()->translate('MB'), /** Megabyte */
            'G' => $this->Core->i18n()->translate('GB'), /** Gigabyte */
            'T' => $this->Core->i18n()->translate('TB'), /** Terabyte */
        );

        $base = log($size, 1024);
        $suffixes = array('', 'K', 'M', 'G', 'T');

        $calculatedSize = round(pow(1024, $base - floor($base)), $precision);
        $returnSize     = ((int)$calculatedSize >= 0) ? $calculatedSize : 0;

        $returnSuffix   = $suffixes[floor($base)];
        $returnSuffix   = (array_key_exists($returnSuffix,$suffixTranslated)) ? $suffixTranslated[$returnSuffix] : $returnSuffix;

        $returnString = ($returnSize >= 1) ? $returnSize . ' ' . $returnSuffix : 0;

        $return = ($asString) ? $returnString : $returnSize;

        return $return;
    }

    public function publicDirPathRoots() {
        return array(
            $this->_galleriesStyleLogoFolderName,
            $this->_wysiwygFolderName
        );
    }

    /** Database Directory Methods */

    private function _prepareDirData($data) {
        /** @var $ermClass Erm */
        $ermClass = $this->Mvc->modelClass('Erm');

        $preparedData = array();
        if(is_array($data) && count($data)) {
            $id               = (array_key_exists('id',                        $data))                                          ? $data['id']                        : false;
            $name             = (array_key_exists('name',                      $data))                                          ? $data['name']                      : '';
            $dir              = (array_key_exists('dir',                       $data))                                          ? $data['dir']                       : '';
            $password         = (array_key_exists('password',                  $data))                                          ? $data['password']                  : '';
            $downloads        = (array_key_exists('downloads',                 $data))                                          ? $data['downloads']                 : 0;
            $active           = (array_key_exists('active',                    $data) && $data['active']  === 1)                ? true                               : false;
            $public           = (array_key_exists('public',                    $data) && $data['public']  === 1)                ? true                               : false;
            $deleted          = (array_key_exists('deleted',                   $data) && $data['deleted'] === 1)                ? true                               : false;
            $archiveState     = (array_key_exists('archived',                  $data) && is_numeric($data['archived']))         ? (int)$data['archived']             : 0;
            $parentId         = (array_key_exists('id_media_dirs__parent',     $data))                                          ? $data['id_media_dirs__parent']     : 0;
            $galleryStyleId   = (array_key_exists('id_media_galleries_styles', $data))                                          ? $data['id_media_galleries_styles'] : 0;
            $showInParentGal  = (array_key_exists('show_in_parent_gallery',    $data) && $data['show_in_parent_gallery'] === 1) ? true                               : false;
            $maxLifeTime      = (array_key_exists('max_lifetime',              $data))                                          ? $data['max_lifetime']              : 0;
            $created          = (array_key_exists('created',                   $data))                                          ? $data['created']                   : 0;
            $id_acc_users     = (array_key_exists('id_acc_users',              $data))                                          ? $data['id_acc_users']              : 0;
            $id_system_site   = (array_key_exists('id_system_sites',           $data))                                          ? $data['id_system_sites']           : 0;
            $getContent       = (array_key_exists('content',                   $data) && is_bool($data['content']))             ? $data['content']                   : false;

            $archived         = ($archiveState === 1) ? true : false;
            $backup           = ($archiveState === 2) ? true : false;
            $backup_failed    = ($archiveState === 3) ? true : false;

            if($id !== false) {

                /** Set Dir Path recursive */
                $preparedData_dirPath = DS . $dir . DS;
                if($parentId > 0 && $this->dirExists($parentId)) {
                    $parentDir     = $this->getDir($parentId);
                    $parentDirPath = rtrim($parentDir['dirPath'],DS);

                    $preparedData_dirPath = $parentDirPath . $preparedData_dirPath;
                }

                /** Set Dir Max Lifetime recursive */
                $preparedData_maxLifeTime = $maxLifeTime;
                if($maxLifeTime === 0 && $parentId > 0 && $this->dirExists($parentId)) {
                    $parentDir             = $this->getDir($parentId);
                    $parentDir_maxLifeTime = ($maxLifeTime === 0 && array_key_exists('max_lifetime',$parentDir) && $parentDir['max_lifetime'] !== 0) ? $parentDir['max_lifetime'] : $maxLifeTime;

                    $preparedData_maxLifeTime = $parentDir_maxLifeTime;
                }

                /** Set Dir Contents, Downloads*/
                $preparedData_dirContents = ($getContent) ? $this->getDirContents($preparedData_dirPath) : array();
                $preparedData_fileDownloads = 0;
                if(is_array($preparedData_dirContents) && count($preparedData_dirContents)) {
                    $fileDownloads = 0;
                    foreach($preparedData_dirContents as $file => $fileArray){
                        $fileDownloads += $this->getFileDownloads($id,$file);
                    }
                    $preparedData_fileDownloads = $fileDownloads;
                }

                /** Set Prepared Data */
                $preparedData['id']                        = $id;
                $preparedData['name']                      = $name;
                $preparedData['dir']                       = $dir;
                $preparedData['dirPath']                   = $preparedData_dirPath;
                $preparedData['dirSize']                   = 0;
                $preparedData['password']                  = $password;
                $preparedData['dirFolderExists']           = $this->checkIfDirFolderExists($preparedData_dirPath);
                $preparedData['dirContents']               = $preparedData_dirContents;
                $preparedData['dirSize']                   = 0;
                $preparedData['downloads']                 = $downloads;
                $preparedData['fileDownloads']             = $preparedData_fileDownloads;
                $preparedData['active']                    = $active;
                $preparedData['public']                    = $public;
                $preparedData['deleted']                   = $deleted;
                $preparedData['archive_state']             = $archiveState;
                $preparedData['archived']                  = $archived;
                $preparedData['backup']                    = $backup;
                $preparedData['backup_failed']             = $backup_failed;
                $preparedData['id_media_dirs__parent']     = $parentId;
                $preparedData['id_media_galleries_styles'] = $galleryStyleId;
                $preparedData['show_in_parent_gallery']    = $showInParentGal;
                $preparedData['max_lifetime']              = $preparedData_maxLifeTime;
                $preparedData['created']                   = strtotime($created);
                $preparedData['id_acc_users']              = $id_acc_users;
                $preparedData['id_system_sites']           = $id_system_site;

                /** ReSet Dir Size */
                if($getContent) {
                    $preparedData['dirSize'] = $this->getDirSize($preparedData);
                }

                /** Last Event Stuff */
                $eventId = 0;
                if(is_object($ermClass)) {
                    $dirs2events = $ermClass->getDirs2Events();
                    if(is_array($dirs2events)) {
                        foreach($dirs2events as $dir2event) {
                            $dir2event_dirId = (is_array($dir2event) && array_key_exists('id_media_dirs', $dir2event)) ? $dir2event['id_media_dirs'] : 0;
                            $dir2event_eventId = (is_array($dir2event) && array_key_exists('id_events', $dir2event)) ? $dir2event['id_events'] : 0;

                            if($dir2event_dirId == $id) {
                                $eventId = $dir2event_eventId;
                                break;
                            }
                        }
                    }
                }
                $preparedData['id_events'] = $eventId;
            }
        }
        return $preparedData;
    }

    public function getDirs($getConfig = null) {
        $config = array(
            'siteId'                 => null,  /** null, int    => is int, Dirs output only from existing Site by Site ID */
            'parentId'               => 0,     /** int, string  => is int, Dirs output only from existing Parent Dir by Parent Dir ID, if "all" for full Dirs output */
            'start'                  => 0,     /** int          => Beginning from this Dir ID (SQL Statement) */
            'limit'                  => null,  /** null, int    => Limiting Dirs output count (SQL Statement) */
            'orderBy'                => null,  /** null, string => Ordering by this table column name (SQL Statement) */
            'orderDirection'         => 'asc', /** string       => "asc" or "desc" Ordering direction (SQL Statement) */
            'search'                 => null,  /** null, string => Search Value in "name" table column (SQL Statement) */
            'archived'               => false, /** bool, string => if "true" Dirs output only with Archive State > 0, with String 'both' output with all Dirs */
            'onlyFirstArchiveParent' => false, /** bool         => if "true" and 'archived' is also set to "true", limiting Dirs output to only the first Parent Dir */
            'content'                => false, /** bool         => if "true" gives Content Info too */
            'cache'                  => true,  /** bool         => if "true" gives cached output if set */
        );

        $getConfig = (is_array($getConfig)) ? $getConfig : array();

        if(count($getConfig)) {
            foreach($config as $key => $value) {
                if(array_key_exists($key, $getConfig)) {
                    $config[$key] = $getConfig[$key];
                }
            }
        }

        $fromCache = (is_bool($config['cache'])) ? $config['cache'] : true;

        $paramsHash = md5(@serialize($config));

        $_dirs = ($this->_dirs === null || !is_array($this->_dirs)) ? array() : $this->_dirs;

        if(!array_key_exists($paramsHash, $_dirs) || !$fromCache) {
            /** @var $ermClass Erm */
            $ermClass = $this->Mvc->modelClass('Erm');
            /** @var $sitesClass Sites */
            $sitesClass = $this->Core->Sites();
            $site = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
            $curSiteId = (count($site)) ? $site['id'] : 0;

            $searchSql = ($config['search'] !== null && $config['search'] !== '') ? ' WHERE lower(`name`) REGEXP lower("' . $config['search'] . '")' : '';

            if (!is_bool($config['archived']) && $config['archived'] == 'both') {
                $archivedSql = ($searchSql !== '') ? ' AND (`archived` = 0 OR `archived` > 0)' : ' WHERE (`archived` = 0 OR `archived` > 0)';
            } else {
                $archivedFlag = ($config['archived']) ? ' != 0' : ' = 0';
                $archivedSql = ($searchSql !== '') ? ' AND `archived`' . $archivedFlag : ' WHERE `archived`' . $archivedFlag;
            }

            if (!is_numeric($config['parentId']) && $config['parentId'] == 'all') {
                $parentSql = '';
            } else {
                $parentId = ((int)$config['parentId'] > 0 && $this->dirExists((int)$config['parentId'])) ? (int)$config['parentId'] : 0;
                $parentSql = ($searchSql !== '' || $archivedSql !== '') ? ' AND `id_media_dirs__parent` = ' . $config['parentId'] : ' WHERE `id_media_dirs__parent` = ' . $parentId;
            }

            if ($config['siteId'] !== null && $config['siteId'] > 0 && is_object($sitesClass) && $sitesClass->siteExists($config['siteId'])) {
                $siteSql = ' AND `id_system_sites` = ' . $config['siteId'];
            } else {
                $siteSql = ' AND (`id_system_sites` = 0 OR `id_system_sites` = ' . $curSiteId . ')';
            }

            $orderBySql = ($config['orderBy'] !== null) ? ' ORDER BY `' . $config['orderBy'] . '` ' . $config['orderDirection'] : '';
            $limitSql = ($config['limit'] !== null) ? ' LIMIT ' . intval($config['start']) . ',' . intval($config['limit']) : '';

            $getDirs = array();

            $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'media_dirs`' . $searchSql . $archivedSql . $parentSql . $siteSql . $orderBySql . $limitSql . ';';

            $result = $this->Core->Db()->fromDatabase($sql, '@raw');

            $getContent = (is_bool($config['content'])) ? $config['content'] : false;

            foreach ($result as $row) {
                $row['content'] = $getContent;
                $getPrepareData = $this->_prepareDirData($row);
                if (count($getPrepareData)) {
                    $getDirs[$getPrepareData['id_system_sites']][$getPrepareData['id']] = $getPrepareData;
                }
            }

            if (is_bool($config['archived']) && $config['archived']) {
                $archivedDirs = array();
                foreach ($getDirs as $gdClientId => $gDirs) {
                    foreach ($gDirs as $gDirId => $gDir) {
                        if ($config['onlyFirstArchiveParent']) {
                            if ($this->getFirstArchiveParentFolderId($gDirId) == $gDirId) {
                                $archivedDirs[$gdClientId][$gDirId] = $gDir;
                            }
                        } else {
                            $archivedDirs[$gdClientId][$gDirId] = $gDir;
                        }
                    }
                }
                $getDirs = $archivedDirs;
            }

            $dirsResult = ($config['siteId'] !== null && is_int($config['siteId']) && array_key_exists($config['siteId'], $getDirs)) ? $getDirs[$config['siteId']] : $getDirs;

            $_dirs[$paramsHash] = $dirsResult;

            $this->_dirs = $_dirs;
        } else {
            $dirsResult = $_dirs[$paramsHash];
        }

        $dirsResult = (!isset($dirsResult) || !is_array($dirsResult)) ? array() : $dirsResult;

        return $dirsResult;
    }

    public function getDirsCount($getConfig = null) {

        /** See getDirs() Config for more Info */
        $config = array(
            'siteId'               => null,
            'parentId'               => 0,
            'archived'               => false,
            'onlyFirstArchiveParent' => false
        );

        $getConfig = (is_array($getConfig)) ? $getConfig : array();

        if(count($getConfig)) {
            foreach($config as $key => $value) {
                if(array_key_exists($key, $getConfig)) {
                    $config[$key] = $getConfig[$key];
                }
            }
        }

        $getDirsConfig = array(
            'parentId'               => $config['parentId'],
            'archived'               => $config['archived'],
            'onlyFirstArchiveParent' => $config['onlyFirstArchiveParent']
        );

        $getDirs = $this->getDirs($getDirsConfig);

        $getDirsCounts = array();
        foreach($getDirs as $gdClientId => $gDirs) {
            $getDirsCounts[$gdClientId] = (is_array($gDirs)) ? count($gDirs) : 0;
        }

        $return = $getDirsCounts;
        if($config['siteId'] !== null && is_int($config['siteId'])) {
            $return = (array_key_exists($config['siteId'],$getDirsCounts)) ? $getDirsCounts[$config['siteId']] : 0;
        }

        return $return;
    }

    public function getDir($dirId, $withContent=false)
    {
        $dir = array();

        if($this->dirExists($dirId)) {
            $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'media_dirs` WHERE `id` = :id;';
            $params = array(
                'id' => (int)$dirId
            );
            $result = $this->Core->Db()->fromDatabase($sql, '@raw', $params);

            foreach($result as $row) {
                if($withContent) {
                    $row['content'] = true;
                }
                $getPrepareData = $this->_prepareDirData($row);
                if(count($getPrepareData)) {
                    $dir = $getPrepareData;
                }
            }
        }

        return $dir;
    }

    public function getDirByFolderName($dirFolderName) {
        $dir = array();

        $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'media_dirs` WHERE `dir` = :dir;';
        $params = array(
            'dir' => $dirFolderName
        );
        $result = $this->Core->Db()->fromDatabase($sql, 'id', $params);

        if(is_array($result)) {
            foreach ($result as $row) {
                $dir = $this->_prepareDirData($row);
            }
        }

        return $dir;
    }

    public function getDirIdByFolderName($dirFolderName) {
        $dirId = 0;

        $sql = 'SELECT `id`, `name` FROM `' . DB_TABLE_PREFIX . 'media_dirs` WHERE `dir` = :dir LIMIT 1;';
        $params = array(
            'dir' => $dirFolderName
        );
        $result = $this->Core->Db()->fromDatabase($sql, '@raw', $params);

        foreach($result as $row) {
            $dirId = $row['id'];
        }

        return $dirId;
    }

    public function getDirIdByFolderPath($dirFolderPath) {
        $dirIdByFolderPath = 0;
        $dirByFolderPath = $this->getDirByFolderPath($dirFolderPath);

        if(count($dirByFolderPath)) {
            $dirIdByFolderPath = $dirByFolderPath['id'];
        }

        return $dirIdByFolderPath;
    }

    public function getDirByFolderPath($dirFolderPath) {
        $dirByFolderPath = array();

        /** @var $sitesClass Sites */
        $sitesClass = $this->Core->Sites();
        $site = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
        $curSiteId = (count($site)) ? $site['id'] : 0;

        $getAllDirsConfig = array(
            'siteId'   => $curSiteId,
            'parentId' => 'all',
            'archived' => 'both'
        );
        $getAllDirs = $this->getDirs($getAllDirsConfig);
        foreach($getAllDirs as $dir) {
            if (array_key_exists('dirPath', $dir) && $dir['dirPath'] == $dirFolderPath) {
                $dirByFolderPath = $dir;
                break;
            }
        }

        return $dirByFolderPath;
    }

    public function getDirSubFolderByDirId($dirId) {
        $subFolder = array();
        if($this->dirExists($dirId)) {
            $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'media_dirs` WHERE `id_media_dirs__parent` = :dirId;';
            $params = array(
                'dirId' => $dirId
            );
            $result = $this->Core->Db()->fromDatabase($sql, '@raw', $params);

            foreach ($result as $row) {
                $dir = $this->_prepareDirData($row);
                if(count($dir)) {
                    $subFolder[$dir['id']] = $dir;
                }
            }
        }
        return $subFolder;
    }

    public function getDirSubSubFolderCountByDirId($dirId) {
        $subSubFolderCount = 0;
        if($this->dirExists($dirId)) {
            $sql = 'SELECT count(`id`) FROM `' . DB_TABLE_PREFIX . 'media_dirs` WHERE `id_media_dirs__parent` IN (SELECT `id` FROM `' . DB_TABLE_PREFIX . 'media_dirs` WHERE `id_media_dirs__parent` = :dirId /* id Vater */) AND `archived` = 0';
            $params = array(
                'dirId' => $dirId
            );
            $result = $this->Core->Db()->fromDatabase($sql, '@simple', $params);

            if(!is_numeric($result)) {
                $result = 0;
            }

            $subSubFolderCount = $result;
        }
        return $subSubFolderCount;
    }

    public function dirExists($dirId)
    {
        $sql = 'SELECT id FROM `' . DB_TABLE_PREFIX . 'media_dirs` WHERE `id` = :id;';
        $params = array(
            'id' => (int)$dirId
        );
        $result = $this->Core->Db()->fromDatabase($sql,'@raw', $params);
        return (count($result)) ? true : false;
    }

    public function saveDir($saveData) {
        $return = array();
        $return['success'] = false;
        $return['msg'] = $this->Core->i18n()->translate('Keine Daten zum Speichern übergeben');

        if(!is_array($saveData) || !count($saveData)) {
            return $return;
        }

        /** @var $accClass Acc */
        $accClass = $this->Mvc->modelClass('Acc');
        $user     = (is_object($accClass)) ? $accClass->getUser() : array();
        $userId   = (count($user)) ? $user['id'] : 0;

        $site   = $this->Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;
        $entryId  = (array_key_exists('id', $saveData)) ? $saveData['id'] : false;

        $forbiddenFolderNames = $this->forbiddenFolderNames();

        $createNew = ($entryId === false) ? true : false;
        if ($createNew) {

            $entryName           = (array_key_exists('name'                     , $saveData))                                               ? trim(trim(strip_tags($saveData['name']),'.')) : '';
            $entryPassword       = (array_key_exists('password'                 , $saveData))                                               ? $saveData['password']                         : '';
            $entryDownloads      = (array_key_exists('downloads'                , $saveData))                                               ? $saveData['downloads']                        : 0;
            $entryActive         = (array_key_exists('active'                   , $saveData) && $saveData['active'] === 1)                  ? 1                                             : 0;
            $entryPublic         = (array_key_exists('public'                   , $saveData) && $saveData['public'] == '1')                 ? 1                                             : 0;
            $entryParentId       = (array_key_exists('id_media_dirs__parent'    , $saveData))                                               ? $saveData['id_media_dirs__parent']            : 0;
            $maxLifetime         = (array_key_exists('max_lifetime'             , $saveData))                                               ? $saveData['max_lifetime']                     : 0;
            $entryGalleryStyleId = (array_key_exists('id_media_galleries_styles', $saveData))                                               ? $saveData['id_media_galleries_styles']        : 0;
            $showInParentGal     = (array_key_exists('show_in_parent_gallery'   , $saveData) && $saveData['show_in_parent_gallery'] == '1') ? $saveData['show_in_parent_gallery']           : 0;
            $entryUserId         = (array_key_exists('id_acc_users'             , $saveData))                                               ? $saveData['id_acc_users']                     : $userId;
            $entryClientId       = $siteId;

            $maxLifetime = ($maxLifetime !== 0 && (strtotime($maxLifetime) !== false)) ? date('Y-m-d H:i:s',strtotime($maxLifetime)) : 0;

            $entryDir = $this->Core->Helper()->createCleanString($entryName, '@+',false);

            if($entryName == '' || $entryDir == '' || in_array($entryName,$forbiddenFolderNames) || in_array($entryDir,$forbiddenFolderNames)){
                $return['success'] = false;
                $return['msg'] = $this->Core->i18n()->translate('Konnte Ordner nicht erstellen, Ordner Name nicht erlaubt!');
            } else {

                $parentDir = ($this->dirExists($entryParentId)) ? $this->getDir($entryParentId) : array();
                $parentDirPath = (count($parentDir)) ? $parentDir['dirPath'] : '';

                if ($entryPassword === '') {
                    $parentDirPassword = (count($parentDir)) ? $parentDir['password'] : $this->generatePassword();
                    $entryPassword = $parentDirPassword;
                }

                if (!is_numeric($entryGalleryStyleId)) {
                    $parentDirGalleryStyleId = (count($parentDir)) ? $parentDir['id_media_galleries_styles'] : 0;
                    $entryGalleryStyleId = $parentDirGalleryStyleId;
                }

                $dirPath = trim($parentDirPath, DS) . DS . $entryDir;

                $getDirByFolderPath = $this->getDirByFolderPath(DS . $dirPath . DS);
                $isDir = (is_array($getDirByFolderPath) && count($getDirByFolderPath));

                $dirFolderExists = $this->checkIfDirFolderExists($dirPath);

                $createDirFolderResult = array();
                if(!$dirFolderExists) {
                    $createDirFolderResult = $this->createDirFolder($dirPath);
                    $dirFolderExists = $this->checkIfDirFolderExists($dirPath);
                }

                if($dirFolderExists && !$isDir) {
                    //MySQL Query für die Erstellung
                    $entryInsertInto = 'INSERT INTO `' . DB_TABLE_PREFIX . 'media_dirs` (`name`, `dir`, `password`, `downloads`, `active`, `public`, `id_media_dirs__parent`, `id_media_galleries_styles`, `show_in_parent_gallery`, `max_lifetime`, `id_acc_users`, `id_system_sites`, `id_acc_users__changedBy`) VALUES (:name, :dir, :password, :downloads, :active, :public, :id_media_dirs__parent, :id_media_galleries_styles, :show_in_parent_gallery, :max_lifetime, :id_acc_users, :id_system_sites, :id_acc_users__changedBy)';
                    $params = array(
                        'name'                      => $entryName,
                        'dir'                       => $entryDir,
                        'password'                  => $entryPassword,
                        'downloads'                 => $entryDownloads,
                        'active'                    => $entryActive,
                        'public'                    => $entryPublic,
                        'id_media_dirs__parent'     => $entryParentId,
                        'id_media_galleries_styles' => $entryGalleryStyleId,
                        'show_in_parent_gallery'    => $showInParentGal,
                        'max_lifetime'              => $maxLifetime,
                        'id_acc_users'              => $entryUserId,
                        'id_system_sites'          => $entryClientId,
                        'id_acc_users__changedBy'   => $entryUserId
                    );

                    $sqlResult = $this->Core->Db()->toDatabase($entryInsertInto, $params);

                    $newEntryId = $this->Core->Db()->lastInsertId();

                    if ($newEntryId == '') {
                        $return['success'] = false;
                        $return['msg'] = $this->Core->i18n()->translate('Konnte Ordner nicht erstellen!');
                    } else {
                        $return['success'] = true;
                        $return['msg'] = $this->Core->i18n()->translate('Ordner erfolgreich erstellt!');
                    }
                } else {
                    $createDirFolderResult_success = (is_array($createDirFolderResult) && array_key_exists('success', $createDirFolderResult) && $createDirFolderResult['success'] > 0);
                    $createDirFolderResult_message = (is_array($createDirFolderResult) && array_key_exists('message', $createDirFolderResult)) ? $createDirFolderResult['message'] : '';

                    $additionalErrorMessage = ($createDirFolderResult_message != '') ? '<br />&bull;&nbsp;' . $createDirFolderResult_message : '';

                    if($isDir) {
                        $additionalErrorMessage .= '<br />&bull;&nbsp;' . $this->Core->i18n()->translate('Ordner existiert bereits im System!');
                    }

                    $return['success'] = false;
                    $return['msg'] = $this->Core->i18n()->translate('Konnte Ordner nicht erstellen!') . $additionalErrorMessage;
                }
            }
        } else {
            if($this->dirExists($entryId)) {

                $saveDir = $this->getDir($entryId);

                $dirSaveData = $saveDir;

                foreach($saveData as $saveDataKey => $saveDataValue) {
                    if(array_key_exists($saveDataKey, $dirSaveData)) {
                        if($saveDataKey == 'max_lifetime') {
                            $maxLifetime = ($saveDataValue !== 0 && (strtotime($saveDataValue) !== false)) ? date('Y-m-d H:i:s',strtotime($saveDataValue)) : 0;

                            $saveDataValue = $maxLifetime;
                        }
                        $dirSaveData[$saveDataKey] = $saveDataValue;
                    }
                }


                $saveDirName = $saveDir['name'];
                $dirSaveDataName = trim(trim(strip_tags($dirSaveData['name']), '.'));
                $doRenameFolder = ($saveDirName != $dirSaveDataName) ? true : false;

                $curDirFolderName = $saveDir['dir'];
                $newDirFolderName = $this->Core->Helper()->createCleanString($dirSaveDataName, '@+',false);

                $curDirFolderPath = $saveDir['dirPath'];
                $newDirFolderPath = DS . trim(substr(trim($curDirFolderPath,DS), 0, -strlen($curDirFolderName)),DS) . DS . $newDirFolderName . DS;

                if($doRenameFolder && ($newDirFolderName == '' || in_array($newDirFolderName,$forbiddenFolderNames))) {
                    $doRenameFolder = false;
                }

                $renameSuccess = true;
                if($doRenameFolder) {
                    $renameFolderResult = $this->renameDirFolder($curDirFolderPath, $newDirFolderPath);

                    if(count($renameFolderResult)) {
                        if($renameFolderResult['success']) {
                            $note  = $renameFolderResult['message'];
                            $type  = 'success';
                            $kind  = 'bs-alert';
                            $title = '';
                            $this->Core->setNote($note, $type, $kind, $title);

                            $dirSaveData['dir'] = $newDirFolderName;
                        } else {
                            $note  = $this->Core->i18n()->translate('Ordner konnte nicht umbenannt werden!');
                            $note  .= ' (' . $curDirFolderName . ' =&gt; ' . $newDirFolderName . ')';
                            $note  .= '<br/>';
                            $note  .= $renameFolderResult['message'];
                            $type  = 'danger';
                            $kind  = 'bs-alert';
                            $title = $this->Core->i18n()->translate('Fehler') . '!';
                            $this->Core->setNote($note, $type, $kind, $title);
                            $renameSuccess = false;
                        }
                    }
                }

                if(!$renameSuccess) {
                    $dirSaveDataName = $saveDirName;
                }

                //MySQL Query für die Bearbeitung
                $entryUpdate = '
                UPDATE
                    `' . DB_CONN_DBNAME . '`.`' . DB_TABLE_PREFIX . 'media_dirs`
                SET
                    `name` = :name,
                    `dir` = :dir,
                    `password` = :password,
                    `downloads` = :downloads,
                    `active` = :active,
                    `public` = :public,
                    `deleted` = :deleted,
                    `archived` = :archived,
                    `id_media_galleries_styles` = :id_media_galleries_styles,
                    `show_in_parent_gallery` = :show_in_parent_gallery,
                    `max_lifetime` = :max_lifetime,
                    `id_acc_users__changedBy` = :id_acc_users__changedBy
                WHERE
                    `' . DB_TABLE_PREFIX . 'media_dirs`.`id` = :id;';

                $params = array(
                    'id' => $dirSaveData['id'],
                    'name' => $dirSaveDataName,
                    'dir' => $dirSaveData['dir'],
                    'password' => $dirSaveData['password'],
                    'downloads' => $dirSaveData['downloads'],
                    'active' => $dirSaveData['active'],
                    'public' => $dirSaveData['public'],
                    'deleted' => $dirSaveData['deleted'],
                    'archived' => $dirSaveData['archived'],
                    'id_media_galleries_styles' => $dirSaveData['id_media_galleries_styles'],
                    'show_in_parent_gallery' => $dirSaveData['show_in_parent_gallery'],
                    'max_lifetime' => $dirSaveData['max_lifetime'],
                    'id_acc_users__changedBy' => $userId
                );

                $sqlResult = $this->Core->Db()->toDatabase($entryUpdate, $params);

                if (!$sqlResult) {
                    $return['success'] = false;
                    $return['msg'] = $this->Core->i18n()->translate('Ordner konnte nicht gespeichert werden!');
                } else {
                    $return['success'] = true;
                    $return['msg'] = $this->Core->i18n()->translate('Ordner erfolgreich gespeichert!');
                }

                /** Clear Gallery Cache */
                $this->clearGalleryCache($entryId);
            }
        }

        return $return;
    }

    public function deleteDir($dirId,$physically=true,$force=false) {
        $return = array();
        $return['success'] = false;
        $return['msg'] = $this->Core->i18n()->translate('Keine Id zum löschen übergeben');

        if(!is_int($dirId)) {
            return $return;
        }

        /** @var $accClass Acc */
        $accClass = $this->Mvc->modelClass('Acc');
        $canDelete = (!is_object($accClass) || $accClass->hasAccess('media_dirs_delete') || $force) ? true: false;

        $dir = ($this->dirExists($dirId)) ? $this->getDir($dirId) : array();

        if(!count($dir)) {
            $return['success'] = false;
            $return['msg'] = $this->Core->i18n()->translate('Ordner konnte nicht gefunden werden');
            return $return;
        }

        if(!$canDelete) {
            $return['success'] = false;
            $return['msg'] = $this->Core->i18n()->translate('Keine Berechtigung zum löschen dieses Ordners!');
        } else {
            /** delete sub dirs first */
            $subDirs = $this->getDirSubFolderByDirId($dirId);
            foreach($subDirs as $subDir) {
                $this->deleteDir($subDir['id'], $physically);
            }

            if($physically) {
                if ($dir['dirSize'] === 0) {
                    $doDelete = true;
                } else {
                    /** Delete anyway */
                    $doDelete = true;

                    // $return['success'] = false;
                    // $return['msg']     = $this->Core->i18n()->translate('Ordner ist nicht leer!');
                    // $doDelete          = false;
                }

                if($doDelete) {
                    $entryDeleteSql = "DELETE FROM `" . DB_TABLE_PREFIX . "media_dirs` WHERE id = '" . $dirId . "' LIMIT 1";
                    $sqlResult = $this->Core->Db()->toDatabase($entryDeleteSql);
                    if (!$sqlResult) {
                        $return['success'] = false;
                        $return['msg'] = sprintf($this->Core->i18n()->translate('MySQL Error: \'%s\''), $this->Core->Db()->errorInfo()[2]);
                    } else {
                        if($dir['dirFolderExists']) {
                            $dirFolderName = $dir['dirPath'];
                            $deleteFolderResult = $this->deleteDirFolder($dirFolderName);
                            if (is_array($deleteFolderResult)) {
                                if ($deleteFolderResult['success']) {
                                    $return['success'] = true;
                                    $return['msg'] = $this->Core->i18n()->translate('Ordner erfolgreich gelöscht!');
                                } else {
                                    $return['success'] = false;
                                    $return['msg'] = $deleteFolderResult['message'];
                                }
                            }
                        } else {
                            $return['success'] = true;
                            $return['msg'] = $this->Core->i18n()->translate('Ordner erfolgreich gelöscht!');
                        }
                    }
                }
            } else {
                $saveData = array();
                $saveData['id'] = $dir['id'];
                $saveData['deleted'] = 1;

                $saveResult = $this->saveDir($saveData);
                $return .= '<pre>' . print_r($saveResult, true) . '</pre>';
            }

            /** Clear Gallery Cache */
            $this->clearGalleryCache($dirId);
        }
        return $return;
    }

    public function getFirstArchiveParentFolderId($dirId) {
        $return = null;
        $dir = ($this->dirExists($dirId)) ? $this->getDir($dirId) : array();

        if(count($dir)) {
            $archiveState = (array_key_exists('archive_state',$dir)) ? $dir['archive_state'] : 0;
            if($archiveState > 0) {
                $return = $dirId;

                $parentDirId  = (array_key_exists('id_media_dirs__parent',$dir)) ? $dir['id_media_dirs__parent'] : 0;
                $parentDir    = ($this->dirExists($parentDirId)) ? $this->getDir($parentDirId) : array();
                if (count($parentDir)) {
                    $parentDirArchiveState = (array_key_exists('archive_state', $parentDir)) ? $parentDir['archive_state'] : 0;
                    if ($parentDirArchiveState) {
                        $return = $this->getFirstArchiveParentFolderId($parentDirId);
                    }
                }
            }
        }
        return $return;
    }

    public function setArchiveStateToDir($dirId, $archiveState = 1, $withSubDirs = true) {

        if((int)$archiveState > 1) {
            $withSubDirs = false;
        }

        $saveData = array();
        $saveData['id'] = $dirId;
        $saveData['archived'] = (int)$archiveState;
        $saveData['active'] = 0;
        $saveData['withSubDirs'] = (bool)$withSubDirs;

        $return = array();
        $return[$dirId]['archive']['success'] = false;
        $return[$dirId]['archive']['message'] = $this->Core->i18n()->translate('Konnte Ordner Archivierungsstatus nicht setzen');
        $return[$dirId]['archive']['saveData'] = $saveData;

        if($this->dirExists($dirId)) {
            $dir = $this->getDir($dirId);

            $saveData['id'] = $dir['id'];

            $return[$dirId]['archive']['saveData'] = $saveData;

            $saveResult = $this->saveDir($saveData);

            if(is_array($saveResult) && count($saveResult) && array_key_exists('success', $saveResult)) {
                if ($saveResult['success']) {
                    $return[$dirId]['archive']['success'] = true;
                    $return[$dirId]['archive']['message'] = $this->Core->i18n()->translate('Ordner Archivierungsstatus erfolgreich gesetzt');
                    $return[$dirId]['archive']['saveData'] = $saveData;
                }
            }

            if($withSubDirs) {
                $subFolder = $this->getDirSubFolderByDirId($dirId);
                if (is_array($subFolder) && count($subFolder)) {
                    foreach($subFolder as $subDirId => $subDir) {
                        if(!array_key_exists('archive_state', $subDir) || $subDir['archive_state'] < 1) {
                            $return = $return + $this->setArchiveStateToDir($subDirId, (int)$archiveState, (bool)$withSubDirs);
                        }
                    }
                }
            }
        }

        return $return;
    }

    public function moveDirRecursively($dirPath, $dirNewPath) {

        $src = (is_dir($dirPath)) ? $dirPath : $this->getDirPath($dirPath);
        $dest = (is_dir($dirNewPath)) ? $dirNewPath : $this->getDirPath($dirNewPath);

        $return = array();
        $return[$src]['success'] = true;

        // If source is not a directory stop processing
        if(!is_dir($src)) $return[$src]['success'] = false;

        // If the destination directory does not exist create it
        if(!is_dir($dest)) {
            if(!mkdir($dest)) {
                // If the destination directory could not be created stop processing
                $return[$src]['success'] = false;
            }
        }

        if($return[$src]['success']) {
            // Open the source directory to read in files
            $i = new DirectoryIterator($src);
            foreach ($i as $f) {
                if ($f->isFile()) {
                    rename($f->getRealPath(), $dest . DS . $f->getFilename());
                } else if (!$f->isDot() && $f->isDir()) {
                    $return[$src]['children'] = $this->moveDirRecursively($f->getRealPath(), $dest . DS . $f);
                    unlink($f->getRealPath());
                }
            }
            unlink($src);
        }

        return $return;
    }

    public function copyDirRecursively($dirPath, $dirNewPath){

        $src = (is_dir($dirPath)) ? $dirPath : $this->getDirPath($dirPath);
        $dest = (is_dir($dirNewPath)) ? $dirNewPath : $this->getDirPath($dirNewPath);

        $return = array();
        $return[$src]['success'] = true;

        // If source is not a directory stop processing
        if(!is_dir($src)) $return[$src]['success'] = false;

        // If the destination directory does not exist create it
        if(!is_dir($dest)) {
            if(!mkdir($dest)) {
                // If the destination directory could not be created stop processing
                $return[$src]['success'] = false;
            }
        }

        if($return[$src]['success']) {
            // Open the source directory to read in files
            $i = new DirectoryIterator($src);
            foreach($i as $f) {
                if($f->isFile()) {
                    copy($f->getRealPath(), $dest . DS . $f->getFilename());
                } else if(!$f->isDot() && $f->isDir()) {
                    $return[$src]['children'] = $this->copyDirRecursively($f->getRealPath(), $dest . DS . $f);
                }
            }
        }

        return $return;
    }

    public function setDirDownloads($dirFolderName,$downloadCount=1) {
        $success = false;
        $dirId = $this->getDirIdByFolderName($dirFolderName);
        if($dirId) {
            $dirDownloads = $this->getDirDownloads($dirFolderName);
            $params = array(
                'dirId' => $dirId,
                'downloads' => $dirDownloads + $downloadCount
            );
            $dirDownloadsUpdate = 'UPDATE `' . DB_TABLE_PREFIX . 'media_dirs` SET `downloads` = :downloads WHERE `id` = :dirId;';
            $sqlResult = $this->Core->Db()->toDatabase($dirDownloadsUpdate, $params);
            if ($sqlResult){
                $success = true;
            }
        }
        return $success;
    }

    public function getDirDownloads($dirFolderName) {
        $dirDownloads = 0;
        $dirId = $this->getDirIdByFolderPath($dirFolderName);
        if($dirId) {
            $sql = 'SELECT `id`, `name`, `downloads` FROM `' . DB_TABLE_PREFIX . 'media_dirs` WHERE `id` = :dirId';
            $params = array(
                'dirId'    => $dirId
            );
            $result = $this->Core->Db()->fromDatabase($sql,'@raw', $params);
            if(is_array($result) && count($result)) {
                foreach($result as $dlEntry) {
                    $dirDownloads = $dlEntry['downloads'];
                }
            }
        }
        return $dirDownloads;
    }

    public function setFileDownloads($dirFolderName,$fileName,$downloadCount=1) {
        $success = false;
        $dirId = $this->getDirIdByFolderPath($dirFolderName);
        if($dirId) {
            $fileDownloads = $this->getFileDownloads($dirId,$fileName);

            $createNewEntry = ($fileDownloads > 0) ? false : true;

            $params = array(
                'dirId' => $dirId,
                'fileName' => $fileName,
                'downloads' => $fileDownloads + $downloadCount
            );

            if($createNewEntry) {
                $fileDownloadsInsertInto = 'INSERT INTO `' . DB_TABLE_PREFIX . 'media_file_downloads` (`id_media_dirs`, `filename`, `downloads`) VALUES (:dirId, :fileName, :downloads)';
                $sqlResult = $this->Core->Db()->toDatabase($fileDownloadsInsertInto, $params);
                if ($this->Core->Db()->lastInsertId() != ''){
                    $success = true;
                }
            } else {
                $fileDownloadsUpdate = 'UPDATE `' . DB_TABLE_PREFIX . 'media_file_downloads` SET `downloads` = :downloads WHERE `id_media_dirs` = :dirId AND `filename` = :fileName;';
                $sqlResult = $this->Core->Db()->toDatabase($fileDownloadsUpdate, $params);
                if ($sqlResult){
                    $success = true;
                }
            }
        }
        return $success;
    }

    public function getFileDownloads($dirId,$fileName = null) {
        $fileDownloads = ($fileName !== null) ? 0 : array();
        $dirId = (is_numeric($dirId)) ? (int)$dirId : 0;
        $whereSql = ($fileName !== null) ? ' WHERE `id_media_dirs` = :dirId AND `filename` = :filename' : ' WHERE `id_media_dirs` = :dirId';
        if($dirId) {
            $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'media_file_downloads`' . $whereSql . ';';
            $params = array(
                'dirId'    => $dirId
            );

            if($fileName !== null) {
                $params['filename'] = $fileName;
            }

            $result = $this->Core->Db()->fromDatabase($sql,'@raw', $params);

            if(is_array($result) && count($result)) {
                foreach($result as $dlEntry) {
                    if($fileName === null) {
                        $fileDownloads[$dlEntry['filename']] = $dlEntry['downloads'];
                    } else {
                        $fileDownloads = $dlEntry['downloads'];
                    }
                }
            }
        }
        return $fileDownloads;
    }

    public function delFileDownloads($dirId,$fileName) {
        $return = array();
        $dirId = (is_numeric($dirId)) ? (int)$dirId : 0;
        if($dirId) {
            $deleteSql = "DELETE FROM `" . DB_TABLE_PREFIX . "media_file_downloads` WHERE `id_media_dirs` = :dirId AND `filename` = :filename LIMIT 1";
            $params = array(
                'dirId'    => $dirId,
                'filename' => $fileName
            );
            $sqlResult = $this->Core->Db()->toDatabase($deleteSql, $params);

            if (!$sqlResult) {
                $return['success'] = false;
                $return['message'] = sprintf($this->Core->i18n()->translate('MySQL Error: \'%s\''), $this->Core->Db()->errorInfo()[2]);
            } else {
                $return['success'] = true;
                $return['message'] = $this->Core->i18n()->translate('Downloads der Datei erfolgreich gelöscht');
            }
        } else {
            $return['success'] = false;
            $return['message'] = $this->Core->i18n()->translate('Downloads der Datei konnte nicht gelöscht werden');
        }
        return $return;
    }

    public function getAllFileDownloadsInclSubFolder($dirId) {
        $allFileDownloads = 0;
        $dirId = (is_numeric($dirId)) ? (int)$dirId : 0;
        if($dirId) {
            $sql = 'SELECT
                      SUM(`downloads`)
                    FROM
                      `' . DB_TABLE_PREFIX . 'media_file_downloads`
                    WHERE
                      `id_media_dirs` IN
                        (
                          SELECT
                            `id`
                          FROM
                            (
                              (
                                SELECT
                                  `id`
                                FROM
                                  (
                                    SELECT
                                      `id`
                                    FROM
                                      (
                                        SELECT
                                          *
                                        FROM
                                          `' . DB_TABLE_PREFIX . 'media_dirs`
                                        ORDER BY `id_media_dirs__parent`, `id`
                                      ) AS files_sorted,
                                      (
                                        SELECT
                                          @pv := :dirId /* id of parent folder */
                                      ) AS initialisation
                                    WHERE
                                      find_in_set(`id_media_dirs__parent`, @pv) > 0 AND @pv := concat(@pv, ",", `id`)
                                  ) AS kapp
                              )                   
                              UNION
                                (
                                  SELECT
                                    :dirId2 AS id
                                )
                            ) AS nasskapp
                        )
                ';
            $params = array(
                'dirId'  => $dirId,
                'dirId2' => $dirId
            );
            $result = $this->Core->Db()->fromDatabase($sql,'@simple', $params);

            if(!is_numeric($result)) {
                $result = 0;
            }

            $allFileDownloads = $result;
        }
        return $allFileDownloads;
    }

    public function delFileDownloadsFromDir($dirId) {
        $return = array();
        $dirId = (is_numeric($dirId)) ? (int)$dirId : 0;
        if($dirId) {

            $deleteSql = "DELETE FROM `" . DB_TABLE_PREFIX . "media_file_downloads` WHERE `id_media_dirs` = :dirId LIMIT 1";
            $params = array(
                'dirId'    => $dirId
            );
            $sqlResult = $this->Core->Db()->toDatabase($deleteSql, $params);

            if (!$sqlResult) {
                $return['success'] = false;
                $return['message'] = sprintf($this->Core->i18n()->translate('MySQL Error: \'%s\''), $this->Core->Db()->errorInfo()[2]);
            } else {
                $return['success'] = true;
                $return['message'] = $this->Core->i18n()->translate('Datei Downloads zum Ordner erfolgreich gelöscht');
            }
        } else {
            $return['success'] = false;
            $return['message'] = $this->Core->i18n()->translate('Datei Downloads zum Ordner konnten nicht gelöscht werden');
        }
        return $return;
    }

    public function syncFileDownloads() {
        $return = array();
        $return['success'] = false;
        $return['message'] = $this->Core->i18n()->translate('Datei-Downloads konnten nicht synchronisiert werden.');

        $site = $this->Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $getDirsConfig = array(
            'siteId'       => $siteId,
            'parentId'       => 'all'
        );
        $dirs = $this->getDirs($getDirsConfig);

        if(count($dirs)) {
            $return['success'] = true;
            $return['result']['fullDirFilesDeleted'] = 0;
            $return['result']['filesDeleted'] = 0;

            foreach ($dirs as $dir) {
                $dirId = $dir['id'];
                $dirPath = $dir['dirPath'];
                $dirContents = $dir['dirContents'];

                if (!$this->checkIfDirFolderExists($dirPath)) {
                    $this->delFileDownloadsFromDir($dirId);
                    $return['result']['fullDirFilesDeleted'] = $return['result']['fullDirFilesDeleted']++;
                } else {
                    foreach ($this->getFileDownloads($dirId) as $fileName => $downloads) {
                        if (!array_key_exists($fileName, $dirContents)) {
                            $this->delFileDownloads($dirId, $fileName);
                            $return['result']['filesDeleted'] = $return['result']['filesDeleted']++;
                        }
                    }
                }
            }

            $return['message'] = ($return['result']['fullDirFilesDeleted'] > 0 || $return['result']['filesDeleted'] > 0) ? $this->Core->i18n()->translate('Datei-Downloads Sync erfolgreich.') : $this->Core->i18n()->translate('Datei-Downloads Sync up-to-date.');
        }

        return $return;
    }

    public function generatePassword($length = 8) {
        $password = "";
        $possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";
        $maxlength = strlen($possible);

        if ($length > $maxlength) {
            $length = $maxlength;
        }

        $i = 0;
        while ($i < $length) {
            $char = substr($possible, mt_rand(0, $maxlength-1), 1);

            if (!strstr($password, $char)) {
                $password .= $char;
                $i++;
            }
        }
        return $password;
    }

    public function getDirsNested($dirs = null, $parentId = 0, $onlyNames = true, $key = 'dir') {

        if($dirs === null) {
            $sitesClass = $this->Core->Sites();
            $site      = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
            $curSiteId = (count($site)) ? $site['id'] : 0;

            $getDirsConfig = array(
                'siteId'   => $curSiteId,
                'parentId' => 'all',
                'archived' => 'both'
            );
            $dirs = $this->getDirs($getDirsConfig);
        }

        $dirsNested = array();

        foreach ($dirs as $dir) {
            if ($dir['id_media_dirs__parent'] == $parentId) {
                $children = $this->getDirsNested($dirs, $dir['id'], $onlyNames, $key);

                ksort($children);

                if($onlyNames) {
                    $dirsNested[$dir[$key]] = $children;
                } else {
                    $dirsNested[$dir[$key]] = $dir;
                    $dirsNested[$dir[$key]]['children'] = $children;
                }
            }
        }

        ksort($dirsNested);

        return $dirsNested;
    }

    public function buildNestedArrayForGetDirsNestedOneLevel($dirs, $parent = null) {
        $tree = array();
        foreach ($dirs as $d) {
            if ($d['id_media_dirs__parent'] == $parent) {
                $children = $this->buildNestedArrayForGetDirsNestedOneLevel($dirs, $d['id']);
                // set a trivial key
                if (!empty($children)) {
                    $d['children'] = $children;
                }
                $tree[] = $d;
            }
        }
        return $tree;
    }

    public function getDirInfoNestedOneLevel($dirs, $info = 'name', $r = 0, $p = null) {
        $return = array();
        foreach ($dirs as $i => $t) {

            if(!array_key_exists($info,$t)) { continue; }

            $dash = '';
            if($info == 'name') {
                $dash = ($t['id_media_dirs__parent'] == 0) ? '' : str_repeat('&ndash;', $r) . ' ';
            }
            $return[$t['id']] = $dash . $t[$info];

            if ($t['id_media_dirs__parent'] == $p) {
                // reset $r
                $r = 0;
            }
            if (isset($t['children'])) {
                $return = $return + $this->getDirInfoNestedOneLevel($t['children'], $info, ++$r, $t['id_media_dirs__parent']);
            }
        }

        return $return;
    }

    public function getDirTreeAsLinkedList($dirsNested, $level = 0, $curDirPath, $textFormatting = true, $showArchivedDirTree = false) {
        /** @var $accClass Acc */
        $accClass = $this->Mvc->modelClass('Acc');

        $canArchive = (!is_object($accClass) || $accClass->hasAccess('media_dirs_archive'));

        $curMvcModelActionUrl = $this->Mvc->getModelUrl($this->Mvc->getModelKey()) . '/' . $this->Mvc->getControllerKey();

        $return = '<ul>';
        foreach($dirsNested as $dir)
        {
            $dirName = $dir['name'];
            $dirPath = $dir['dirPath'];
            $dirArchiveState = $dir['archive_state'];

            if($dirArchiveState > 0 && !$canArchive) { continue; }

            $showSubTree = (array_key_exists('children', $dir) && count($dir['children']));

            $dirListUrl = $curMvcModelActionUrl .  '/list' . $dirPath;

            $isCurrent = ($curDirPath == $dirPath);
            $isActive = (strpos($curDirPath, $dirPath) !== false);

            $liActiveClass = ($isActive) ? ' class="active"' : '';

            $return .= '<li' . $liActiveClass . '>';

            if($dirArchiveState > 0) {
                if(!$showArchivedDirTree && $showSubTree) {
                    $showSubTree = false;
                }
                if($textFormatting) {
                    $return .= '<span class="text-warning" data-toggle="tooltip" title="' . $this->Core->i18n()->translate('Archiviert') . '">' . $dirName . '</span>';
                } else {
                    $return .= '<span>' . $dirName . '</span>';
                }
            } else {
                if($isCurrent) {
                    $return .= '<span>' . $dirName . '</span>';
                } else {
                    if($textFormatting) {
                        $return .= '<a href="' . $dirListUrl . '" target="_self" data-toggle="tooltip" title="' . $this->Core->i18n()->translate('In das Verzeichnis wechseln') . '">' . $dirName . '</a>';
                    } else {
                        $return .= '<a href="' . $dirListUrl . '" target="_self">' . $dirName . '</a>';
                    }
                }
            }

            if ($showSubTree) {
                $return .= $this->getDirTreeAsLinkedList($dir['children'], $level + 1, $curDirPath, $textFormatting, $showArchivedDirTree);
            }
            $return .=  '</li>';

        }
        $return .= '</ul>';
        return $return;
    }

    public function checkDbAndFsSynchronizeState() {
        $return = array();

        /** Get Filesystem an Database Information */
        $folders_fs   = $this->getDirFoldersArray();
        $folders_db   = $this->getDirsNested();

        /** Get Differences in Filesystem an Database Information */
        $folders_diff = $this->array_diff_assoc_recursive($folders_fs, $folders_db);

//        $debugNote = '<pre>' .
//            'count($folders_fs) => ' . print_r(count($folders_fs), true) . PHP_EOL .
//            'count($folders_db) => ' . print_r(count($folders_db), true) . PHP_EOL .
//            'count($folders_diff) => ' . print_r(count($folders_diff), true) . PHP_EOL .
//            '$folders_diff => ' . print_r($folders_diff, true) . PHP_EOL .
//            '</pre>';

        /** If Differences Exists */
        if(count($folders_diff)) {
            $return['success']      = false;
            $return['message']      = $this->Core->i18n()->translate('Media Folder sollten Synchronisiert werden.');
            $return['folders_diff'] = $folders_diff;
        } else {
            $return['success'] = true;
            $return['message'] = $this->Core->i18n()->translate('Media Folder Sync up-to-date');
        }

        return $return;
    }

    public function synchronizeDbAndFs() {
        $return = array();

        /** Get Differences in Filesystem an Database Information */
        $syncCheck         = $this->checkDbAndFsSynchronizeState();
        $syncCheck_success = (is_array($syncCheck) && array_key_exists('success', $syncCheck)) ? $syncCheck['success'] : false;
        $syncCheck_message = (is_array($syncCheck) && array_key_exists('message', $syncCheck)) ? $syncCheck['message'] : '';

        $return['success'] = $syncCheck_success;
        $return['message'] = $syncCheck_message;

        /** If Differences Exists */
        $folders_diff = (!$syncCheck_success && array_key_exists('folders_diff', $syncCheck) && is_array($syncCheck['folders_diff'])) ? $syncCheck['folders_diff'] : array();
        if(count($folders_diff)) {
            $folders_path = $this->getDirFoldersArray(DS,true);

            /** Set Missing Folder Path Array */
            $folders_missing = array();
            foreach ($folders_path as $path) {
                $trimmedPath = trim($path, DS);
                if ($path == DS) { continue; }
                if ($this->getDirIdByFolderPath($path) > 0) { continue; }
                if (!count(preg_grep('/^'. $trimmedPath . '[\d]*/', array_keys($folders_diff)))) { continue; }
                $folders_missing[] = $path;
            }

            /** Create New Dirs by Missing Folder Path's */
            if (count($folders_missing)) {
                $saveNewDirCount=0;
                foreach($folders_missing as $path) {
                    $parentFolderPath = (dirname($path) == DS) ? dirname($path) : dirname($path) . DS;
                    $folderName = trim(str_replace($parentFolderPath, '', $path),DS);

                    if(count($this->getDirByFolderPath($path))) { continue; }

                    $parentDir_id = ($parentFolderPath != DS) ? $this->getDirIdByFolderPath($parentFolderPath) : 0;

                    $newDirSaveData = array(
                        'name'                  => $folderName,
                        'password'              => $this->generatePassword(),
                        'id_media_dirs__parent' => $parentDir_id,
                        'id_acc_users'          => 0
                    );

                    $saveDirResult = (($parentFolderPath != DS && $parentDir_id == 0) || $this->getDirIdByFolderPath($path) > 0) ? 'not saved' : $this->saveDir($newDirSaveData);

                    if(is_array($saveDirResult) && $saveDirResult['success']) {
                        $saveNewDirCount++;
                    }

                    $return['debug']['saveNewDir'][] = array(
                        'parentFolderPath' => $parentFolderPath,
                        'folderPath'       => $path,
                        'folderName'       => $folderName,
                        'id_media_dirs__parent'        => $parentDir_id,
                        'saveDirResult'    => $saveDirResult
                    );
                }

                if($saveNewDirCount < count($folders_missing)) {
                    $return['success'] = false;
                    $return['message'] = $this->Core->i18n()->translate('Media Folder konnten nicht vollständig synchronisiert werden.');

                    $return['debug']['saveNewDirCount'] = $saveNewDirCount;
                    $return['debug']['folders_missing'] = $folders_missing;
                } else {
                    $return['success'] = true;
                    $return['message'] = $this->Core->i18n()->translate('Media Folder Synchronisiert.');
                }
            } else {
                $return['success'] = false;
                $return['message'] = $this->Core->i18n()->translate('Fehler beim Media Folder synchronisieren.');

                $return['debug']['folders_diff'] = $folders_diff;
            }
        }

        return $return;
    }

    public function array_diff_assoc_recursive($array1, $array2)
    {
        foreach($array1 as $key => $value)
        {
            if(is_array($value))
            {
                if(!isset($array2[$key]))
                {
                    $difference[$key] = $value;
                }
                elseif(!is_array($array2[$key]))
                {
                    $difference[$key] = $value;
                }
                else
                {
                    $new_diff = $this->array_diff_assoc_recursive($value, $array2[$key]);
                    if($new_diff != FALSE)
                    {
                        $difference[$key] = $new_diff;
                    }
                }
            }
            elseif(!isset($array2[$key]) || $array2[$key] != $value)
            {
                $difference[$key] = $value;
            }
        }
        return !isset($difference) ? array() : $difference;
    }

    /** Filesystem Directory Methods */

    private function _mayCreateRootMediaFolder() {
        $mediaRootPath = $this->getMediaRootRelPath();
        if(!file_exists($mediaRootPath)) {
            mkdir($mediaRootPath);
        }
    }

    private function _mayCreateBaseFolder() {
        $_mediaBaseFolderName = $this->_mediaBaseFolderName();
        $mediaRootPath = $this->getMediaRootRelPath();

        $basePath =  rtrim($mediaRootPath, DS) . DS . $_mediaBaseFolderName;
        if(!file_exists($basePath)) {
            mkdir($basePath);
        }
    }

    private function _mayCreateUploadsFolder() {
        $_uploadsFolderName = $this->_mediaUploadsFolderName();
        $mediaPath = $this->getMediaRelPath();

        $uploadsPath =  rtrim($mediaPath, DS) . DS . $_uploadsFolderName;
        if(!file_exists($uploadsPath)) {
            mkdir($uploadsPath);
        }
    }

    private function _mayCreateDir($dirFolderName)
    {
        if(!$this->checkIfDirFolderExists($dirFolderName)) {
            $this->createDirFolder($dirFolderName);
        }
    }

    private function _setBasicMediaContentStuff() {
        /**
         * @var $Core Core
         * @var $Mvc Mvc
         * @var $Config Config
         * @var $Request Request
         */
        $Core    = $this->Core;
        $Mvc     = $Core->Mvc();
        $Config  = $Core->Config();
        $Request = $Core->Request();

        $this->_handleDirectMediaAccess();

        $modelKey        = $Mvc->getModelKey();
        $modelController = $Mvc->getControllerKey();
        $modelAction     = $Mvc->getActionKey();

        if (
            $modelKey        == 'media' &&
            $modelController == 'dirs' &&
            $modelAction     == 'contents'
        ) {
            $curUrl                = trim($Request->getCurrUrl(),'/') . '/';
            $curMvcModelActionUrl  = $Mvc->getModelUrl($modelKey) . '/' . $modelController . '/' . $modelAction . '/';
            $curMvcModelActionSlug = trim(str_replace($curMvcModelActionUrl, '', $curUrl),'/');
            $dirPathFromSlug       = str_replace('dirpath/','',$curMvcModelActionSlug);
            $curDirPath            = ($dirPathFromSlug === '' || $dirPathFromSlug === 'dirpath' || strpos($dirPathFromSlug, 'http') !== false) ? '/' : '/' . $dirPathFromSlug . '/';
            $curDirPath            = str_replace('/', DS, $curDirPath);

            $curDirIdByDirPath     = $this->getDirIdByFolderPath($curDirPath);

            $isDir = ($this->dirExists($curDirIdByDirPath));

            if($isDir) {
                $Mvc->setForceHeaderCache();
            }

            $Mvc->setForceNoFileCache();
        }
    }

    private function _handleDirectMediaAccess() {

        $useCd = $this->useContentDelivery();

        $htaccessFilePath = rtrim($this->getMediaRelPath(), DS) . DS . '.htaccess';

        if(!$useCd) {
            if(!file_exists($htaccessFilePath)) {
                $htaccessContent = 'Satisfy Any' . PHP_EOL . 'Order Deny,Allow' . PHP_EOL . 'Allow from all';

                $htaccessFile = fopen($htaccessFilePath, 'a') or die('<pre>Media Access Handling File Error: Unable to open file ' . $htaccessFilePath . '!');

                fwrite($htaccessFile, $htaccessContent);
                fclose($htaccessFile);
            }
        } else {
            if(file_exists($htaccessFilePath)) {
                unlink($htaccessFilePath);
            }
        }
    }

    private function _rmdir_recursive($fullDirPath)
    {
        foreach(scandir($fullDirPath) as $file) {
            if ('.' === $file || '..' === $file) { continue; }
            if (is_dir($fullDirPath . DS . $file))  { $this->_rmdir_recursive($fullDirPath . DS . $file); }
            else { unlink($fullDirPath . DS . $file); }
        }
        rmdir($fullDirPath);
    }

    private function _imagingClass() {
        $_imagingClass = $this->_imagingClass;
        if($_imagingClass === null) {
            $_imagingClass = new Imaging();
            $this->_imagingClass = $_imagingClass;
        }
        return $_imagingClass;
    }

    public function useContentDelivery() {
        /**
         * @var $Core Core
         * @var $Config Config
         */
        $Core   = $this->Core;
        $Config = $Core->Config();

        $useCd_config = $Config->get('media_use_cd');

        $useCd = ($useCd_config !== '' && is_bool($useCd_config)) ? $useCd_config : true;

        return $useCd;
    }

    public function useChunkedFileUpload() {
        /**
         * @var $Core Core
         * @var $Config Config
         */
        $Core   = $this->Core;
        $Config = $Core->Config();

        $useCFU_config = $Config->get('media_use_cfu');

        $useCFU = ($useCFU_config !== '' && is_bool($useCFU_config)) ? $useCFU_config : false;

        return $useCFU;
    }

    public function forbiddenFolderNames() {
        $forbiddenFolderNames = array(
            '.',
            '..',
            ',',
            'com1',
            'com2',
            'com3',
            'com4',
            'com5',
            'com6',
            'com7',
            'com8',
            'com9',
            'ltp1',
            'ltp2',
            'ltp3',
            'ltp4',
            'ltp5',
            'ltp6',
            'ltp7',
            'ltp8',
            'ltp9',
            'com',
            'nul',
            'prn'
        );

        return $forbiddenFolderNames;
    }

    public function getDirFoldersArray($path = DS, $onlyPath = false) {
        $_uploadsFolderName = $this->_mediaUploadsFolderName();
        $currFullPath = $this->getMediaRelPath() . DS . $_uploadsFolderName . $path;
        $result = array();
        $cdir = scandir($currFullPath);
        foreach ($cdir as $key => $mediaFolder) {
            if (!in_array($mediaFolder,array('.','..','thumbnails')))  {
                if (is_dir($currFullPath . $mediaFolder)){

                    if($onlyPath) {
                        $result[] = $path . $mediaFolder . DS;
                        $result = array_merge($result, $this->getDirFoldersArray($path . $mediaFolder . DS, $onlyPath));
                    } else {
                        $result[$mediaFolder] = $this->getDirFoldersArray($path . $mediaFolder . DS);
                    }
                }
            }
        }
        return $result;
    }

    public function getDirPath($dirFolderName) {
        $_uploadsFolderName = $this->_mediaUploadsFolderName();
        $_galleriesStyleLogoFolderName = $this->_mediaGalleriesStyleLogoFolderName();
        $mediaPath = $this->getMediaRelPath();

        $cleanFolderPath = trim(str_replace('/', DS, $dirFolderName), DS);
        $uploadsPath = ($cleanFolderPath != $_galleriesStyleLogoFolderName) ? DS . $_uploadsFolderName : DS;

        return rtrim($mediaPath, DS) . $uploadsPath . DS . $cleanFolderPath;
    }

    public function getDirUrl($dirFolderName) {
        $_uploadsFolderName = $this->_mediaUploadsFolderName();
        $_galleriesStyleLogoFolderName = $this->_mediaGalleriesStyleLogoFolderName();
        $mediaUrl = $this->getMediaUrl();

        $cleanFolderPath = trim(str_replace('/', DS, $dirFolderName), DS);
        $uploadsUrlSlug = ($cleanFolderPath != $_galleriesStyleLogoFolderName) ? $_uploadsFolderName . '/' : '';

        return rtrim($mediaUrl, '/') . '/' . $uploadsUrlSlug . str_replace(DS, '/', $cleanFolderPath);
    }

    public function getFileUrl($dirFolderPath, $fileName, $thumbnail = false) {
        $thumbnailUrlSlug = ($thumbnail) ? 'thumbnails/' : '';

        $fileUrl = $this->getDirUrl($dirFolderPath) . '/' . $thumbnailUrlSlug . $fileName;

        if($this->useContentDelivery()) {
            /** @var $Mvc Mvc */
            $Mvc = $this->Core->Mvc();

            $mediaModelUrl = $Mvc->getModelUrl('media');

            $dirContentsUrl = $mediaModelUrl . '/dirs/contents/' . trim(str_replace(DS, '/', $dirFolderPath), '/') . '/' . $thumbnailUrlSlug;

            $fileUrl = $dirContentsUrl . $fileName;
        }

        return $fileUrl;
    }

    public function checkIfDirFolderExists($dirFolderName) {
        $dirPath = $this->getDirPath($dirFolderName);
        return is_dir($dirPath);
    }

    public function createDirFolder($dirFolderName) {
        $return = array();
        $return['success'] = false;
        $return['message'] = $this->Core->i18n()->translate('Ordner konnte nicht angelegt werden!');
        if(!$this->checkIfDirFolderExists($dirFolderName)) {
            $dirPath = $this->getDirPath($dirFolderName);

            mkdir($dirPath);

            if($this->checkIfDirFolderExists($dirFolderName)) {
                $return['success'] = true;
                $return['message'] = $this->Core->i18n()->translate('Ordner wurde erfolgreich angelegt!');
            }
        } else {
            $return['success'] = false;
            $return['message'] = $this->Core->i18n()->translate('Ordner existiert bereits!');
        }
        return $return;
    }

    public function renameDirFolder($dirFolderName, $newDirFolderName) {
        $return = array();
        $return['success'] = false;
        $return['message'] = $this->Core->i18n()->translate('Ordner konnte nicht umbenannt werden!');
        if($this->checkIfDirFolderExists($dirFolderName)) {
            $dirPath_old = $this->getDirPath($dirFolderName);
            $dirPath_new = $this->getDirPath($newDirFolderName);

            if($this->checkIfDirFolderExists($newDirFolderName)) {
                $return['success'] = false;
                $return['message'] = $this->Core->i18n()->translate('Neuer Ordnername existiert bereits!');
            } else {
                rename($dirPath_old, $dirPath_new);

                if($this->checkIfDirFolderExists($newDirFolderName)) {
                    $return['success'] = true;
                    $return['message'] = $this->Core->i18n()->translate('Ordner wurde erfolgreich umbenannt!');
                }
            }
        } else {
            $return['success'] = false;
            $return['message'] = $this->Core->i18n()->translate('Ordner existiert nicht!');
        }
        return $return;
    }

    public function deleteDirFolder($dirFolderName)
    {
        $return = array();

        if($this->checkIfDirFolderExists($dirFolderName)) {
            $dirPath = $this->getDirPath($dirFolderName);

            $this->_rmdir_recursive($dirPath);

            if(!$this->checkIfDirFolderExists($dirFolderName)) {
                $return['success'] = true;
                $return['message'] = $this->Core->i18n()->translate('Ordner wurde erfolgreich gelöscht!');
            } else {
                $return['success'] = false;
                $return['message'] = $this->Core->i18n()->translate('Ordner konnte nicht gelöscht werden!');
            }
        } else {
            $return['success'] = false;
            $return['message'] = $this->Core->i18n()->translate('Ordner existiert nicht!');
        }
        return $return;
    }

    public function getDirContents($dirFolderName) {
        set_time_limit ( 0 );
        ini_set('memory_limit', '2048M');
        /**
         * @var $Helper Helper
         * @var $mediaFilesystem MediaFilesystem
         */
        $Helper = $this->Core->Helper();
        $mediaFilesystem = $this->getMediaFilesystem();
        // $this->_mayCreateDir($dirFolderName);
        $dirContents = array();
        if($this->checkIfDirFolderExists($dirFolderName)) {

            $dirPath = $this->getDirPath($dirFolderName);
            $dirUrl  = $this->getDirUrl($dirFolderName);

            $it = new FilesystemIterator($dirPath);
            foreach ($it as $fileinfo) {
                if($fileinfo->isDir()) { continue; }

                $fullFilePath = $fileinfo->getPathname();

                $file     = $fileinfo->getFilename();
                $fileExt  = $fileinfo->getExtension();
                $fileName = $fileinfo->getBasename('.' . $fileExt);
                $mimeType = $Helper->getMimeContentType($fullFilePath);
                try {
                    $fileSize = $fileinfo->getSize();
                } catch (Exception $e) {
                    $fileSize = $mediaFilesystem->getFileSize($fullFilePath);
                }
                try {
                    $fileTime = $fileinfo->getMTime();
                } catch (Exception $e) {
                    $fileTime = $mediaFilesystem->getFileMTime($fullFilePath);
                }
                $fileUrl  = $this->getFileUrl($dirFolderName,$file);

                $dirContents[$file]['name']      = $fileName;
                $dirContents[$file]['ext']       = $fileExt;
                $dirContents[$file]['mime']      = $mimeType;
                $dirContents[$file]['size']      = $fileSize; /** Byte */
                $dirContents[$file]['mtime']     = $fileTime;
                $dirContents[$file]['filePath']  = $fullFilePath;
                $dirContents[$file]['fileUrl']   = $fileUrl;

                $unSecureFileUrl = $dirUrl . '/' . $file;
                $dirContents[$file]['unSecureFileUrl'] = $unSecureFileUrl;
            }

            $thDirPath = $dirPath . DS . 'thumbnails';
            $thDirUrl  = $dirUrl . '/thumbnails';

            if(is_dir($thDirPath)) {

                $thit = new FilesystemIterator($thDirPath);
                foreach ($thit as $fileinfo) {
                    if($fileinfo->isDir() || $fileinfo->getFilename() == '.thinfo') { continue; }

                    $fullFilePath = $fileinfo->getPathname();

                    $file     = $fileinfo->getFilename();
                    $fileExt  = $fileinfo->getExtension();
                    $fileName = $fileinfo->getBasename('.' . $fileExt);
                    $mimeType = $Helper->getMimeContentType($fullFilePath);
                    try {
                        $fileSize = $fileinfo->getSize();
                    } catch (Exception $e) {
                        $fileSize = $mediaFilesystem->getFileSize($fullFilePath);
                    }
                    try {
                        $fileTime = $fileinfo->getMTime();
                    } catch (Exception $e) {
                        $fileTime = $mediaFilesystem->getFileMTime($fullFilePath);
                    }
                    $fileUrl  = $this->getFileUrl($dirFolderName,$file,true);

                    $dirContents[$file]['thumbnail']['name']      = $fileName;
                    $dirContents[$file]['thumbnail']['ext']       = $fileExt;
                    $dirContents[$file]['thumbnail']['mime']      = $mimeType;
                    $dirContents[$file]['thumbnail']['size']      = $fileSize; /** Byte */
                    $dirContents[$file]['thumbnail']['mtime']     = $fileTime;
                    $dirContents[$file]['thumbnail']['filePath']  = $fullFilePath;
                    $dirContents[$file]['thumbnail']['fileUrl']   = $fileUrl;

                    $unSecureFileUrl = $thDirUrl . '/' . $file;
                    $dirContents[$file]['thumbnail']['unSecureFileUrl'] = $unSecureFileUrl;
                }
            }
        }
        return $dirContents;
    }

    public function addFile($fileArray, $dirFolderName) {
        set_time_limit ( 0 );

        /**
         * @var $Core Core
         * @var $i18n i18n
         * @var $Request Request
         */
        $Core = $this->Core;
        $i18n = $Core->i18n();
        $Request = $Core->Request();

        $return = array();
        $return['success'] = false;
        $return['message'] = $this->Core->i18n()->translate('Fehler beim speichern der Datei...');
        $return['error']   = $return['message'];

        $return['saveData']['fileArray'] = $fileArray;
        $return['saveData']['dirFolderName'] = $dirFolderName;
        $return['saveData']['targetPath'] = $this->getDirPath($dirFolderName) . DS;

        if (is_array($fileArray) && (array_key_exists('file',$fileArray) || isset($_GET["qqchunkuploaddone"]))) {
            $post = $Request->getPost();

            $faFile     = (array_key_exists('file', $fileArray) && is_array($fileArray['file'])) ? $fileArray['file'] : array();
            $tempFile   = (array_key_exists('tmp_name', $faFile)) ? $faFile['tmp_name'] : time();
            $faFileName = (array_key_exists('name', $faFile)) ? $faFile['name'] : time();

            $targetPath = $this->getDirPath($dirFolderName) . DS;

        	$qqFileName     = (array_key_exists('qqfilename', $post)) ? $post['qqfilename'] : $faFileName;
            $targetFileName = ($faFileName == 'blob' || is_numeric($faFileName)) ? $qqFileName : $faFileName;

            $varPath = $this->Core->getRelPath().DS.'var'.DS;
            $tmpPath = $varPath . 'temp' . DS;
            if(!is_dir($tmpPath)) {
                mkdir($tmpPath);
            }

            if($this->useChunkedFileUpload()) {
            	$return = $this->mfsFileUpload($targetFileName, $targetPath);
            } else {

                if (file_exists($targetPath . $targetFileName)) {
                    $targetFileName = pathinfo($targetFileName, PATHINFO_FILENAME) . '_' . time() . '.' . pathinfo($targetFileName, PATHINFO_EXTENSION);
                }

                $sourceFile = $tempFile;
                $targetFile = $targetPath . $targetFileName;

                if (move_uploaded_file($sourceFile, $targetFile)) {
                    $return['success'] = true;
                    $return['message'] = $i18n->translate('Datei gespeichert...');

                    $this->createThumbnail($targetFileName, $dirFolderName);
                } else {
                    $message = $i18n->translate('Fehler beim speichern der Datei...');
                    $message_err = '';
                    switch ($fileArray['file']['error']) {
                        case UPLOAD_ERR_OK:
                            $message = false;
                            break;
                        case UPLOAD_ERR_INI_SIZE:
                        case UPLOAD_ERR_FORM_SIZE:
                            $message_err .= sprintf($i18n->translate('File too large (limit of %s).'), ini_get('upload_max_filesize'));
                            break;
                        case UPLOAD_ERR_PARTIAL:
                            $message_err .= $i18n->translate('File upload was not completed.');
                            break;
                        case UPLOAD_ERR_NO_FILE:
                            $message_err .= $i18n->translate('Zero-length file uploaded.');
                            break;
                        default:
                            $message_err .= sprintf($i18n->translate('Internal error #'), $fileArray['file']['error']);
                            break;
                    }
                    if (!$message) {
                        if (!is_uploaded_file($fileArray['file']['tmp_name'])) {
                            $message = $i18n->translate('Fehler beim speichern der Datei...') . ' -> ' . $this->Core->i18n()->translate('Unknown error.');
                        }
                    }
                    $return['success'] = false;
                    $return['message'] = $i18n->translate($message) . ' -> ' . $i18n->translate($message_err);
                }
            }

            /** Clear Gallery Cache */
            $this->clearGalleryCache($this->getDirIdByFolderPath($dirFolderName));
        }

        return $return;

    }
    
    /**
     * Chunked File Upload with DropZone JS
     * 
     * DEPRECATED!
     */
    public function dzChunkFileUpload($fileArray, $dirFolderName) {

        /**
         * @var $Core Core
         * @var $i18n i18n
         * @var $Request Request
         */
        $Core = $this->Core;
        $i18n = $Core->i18n();
        $Request = $Core->Request();

        $return = array();
        $return['success'] = false;
        $return['message'] = $this->Core->i18n()->translate('Fehler beim speichern der Datei...');

        $return['saveData']['fileArray'] = $fileArray;
        $return['saveData']['dirFolderName'] = $dirFolderName;
        $return['saveData']['targetPath'] = $this->getDirPath($dirFolderName) . DS;

        if (is_array($fileArray) && array_key_exists('file',$fileArray)) {

            $tempFile = $fileArray['file']['tmp_name'];

            $targetPath = $this->getDirPath($dirFolderName) . DS;

            $targetFileName = $fileArray['file']['name'];

            $varPath = $this->Core->getRelPath().DS.'var'.DS;
            $tmpPath = $varPath . 'temp' . DS;
            if(!is_dir($tmpPath)) {
                mkdir($tmpPath);
            }
            	
	        $post = $Request->getPost();
	
	        $chunks_uuid           = $post['dzuuid'];
	        $chunks_totalCount     = $post['dztotalchunkcount'];
	        $chunks_index          = ($post['dzchunkindex'] + 1);
	        $chunks_size           = $post['dzchunksize'];
	        $chunks_totalFileSize  = $post['dztotalfilesize'];
	
	        $chunks_folderName     = $Core->Helper()->createCleanString($targetFileName) . '_' . $chunks_totalFileSize;
	
	        $chunks_path           = $tmpPath . 'chunks' . DS;

            $sitesClass  = $this->Core->Sites();
            $site       = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
            $curSiteKey = (count($site)) ? $site['urlKey'] : '';
            if($curSiteKey !== '') {
                $chunks_sitePath = $chunks_path . $curSiteKey . DS;
                $chunks_path = $chunks_sitePath;
            }

	        $chunks_fnPath         = $chunks_path . $chunks_folderName . DS;
	        // $chunks_targetPath     = $chunks_path . $chunks_uuid . DS;
	        $chunks_targetPath     = $chunks_fnPath;
	
	        $partNr = ($chunks_index < 10) ? '0' . $chunks_index : $chunks_index;
	        $chunks_targetFileName = $targetFileName . '.part-' . $partNr;
	
	        if(!is_dir($chunks_path)) {
	            mkdir($chunks_path);
	        }
	        if(!is_dir($chunks_targetPath)) {
	            mkdir($chunks_targetPath);
	        };
	
	        $moveUploadedFile = false;
	        if(file_exists($chunks_targetPath . $chunks_targetFileName)) {
	            if((filesize($chunks_targetPath . $chunks_targetFileName) != $chunks_size) && unlink($chunks_targetPath . $chunks_targetFileName)) {
	                $moveUploadedFile = true;
	            }
	        } else {
	            $moveUploadedFile = true;
	        }
	        if($moveUploadedFile) {
	            move_uploaded_file($tempFile, $chunks_targetPath . $chunks_targetFileName);
	        }
	
	        $chunksUploaded = 0;
	        for ( $i = 1; $i <= $chunks_index; $i++ ) {
	            $partNr = ($i < 10) ? '0' . $i : $i;
	            if ( file_exists( $chunks_targetPath.$targetFileName . '.part-' . $partNr ) ) {
	                ++$chunksUploaded;
	
	                $return['success'] = true;
	                $return['message'] = $i18n->translate('Chunk File saved to temp.');
	            }
	        }
	
	        if ($chunksUploaded == $chunks_totalCount) {
	            $mergingParts = array();
	            $deletedParts = array();
	            $finalFileSize = 0;
	
	            for ($i = 1; $i <= $chunks_totalCount; $i++ ) {
	                $partNr = ($i < 10) ? '0' . $i : $i;
	
	                $fileName = $targetFileName . '.part-' . $partNr;
	                $filePath = $chunks_targetPath.$fileName;
	
	                if ( file_exists( $filePath ) ) {
	                    $mergingParts[] = $fileName;
	
	                    $file = fopen($filePath, 'rb');
	                    $buff = fread($file, filesize($filePath));
	                    fclose($file);
	
	                    $final = fopen($chunks_targetPath . $targetFileName, 'ab');
	                    $write = fwrite($final, $buff);
	                    fclose($final);
	
	                    $finalFileSize = filesize($chunks_targetPath . $targetFileName);
	
	                    if(unlink($filePath)) {
	                        $deletedParts[] = $fileName;
	                    }
	                }
	            }
	
	            if(
	                file_exists($chunks_targetPath . $targetFileName) &&
	                filesize($chunks_targetPath . $targetFileName) == $chunks_totalFileSize
	            ) {
	
	                $sourceFile = $chunks_targetPath . $targetFileName;
	
	                if (file_exists($targetPath . $targetFileName)) {
	                    $targetFileName = pathinfo($targetFileName, PATHINFO_FILENAME) . '_' . time() . '.' . pathinfo($targetFileName, PATHINFO_EXTENSION);
	                }
	                $targetFile = $targetPath . $targetFileName;
	
	                if(rename($sourceFile,$targetFile)) {
	                    $return['success'] = true;
	                    $return['message'] = $i18n->translate('Datei gespeichert...');
	
	                    $this->clearTempChunks($chunks_folderName);
	
	                    $this->createThumbnail($targetFileName, $dirFolderName);
	                } else {
	                    $return['success'] = false;
	                    $return['message'] = $i18n->translate('Fehler beim speichern der Datei...');
	
	                    $this->_rmdir_recursive($chunks_targetPath);
	                }
	            } else {
	                $return['success'] = false;
	                $return['message'] = $i18n->translate('Fehler beim speichern der Datei...');
	            }
	
	            $debugNote = array(
	                '$post => ' => $post,
	                '$chunksUploaded' => $chunksUploaded,
	                '$chunks_totalCount' => $chunks_totalCount,
	                '$mergingParts Count' => count($mergingParts),
	                '$deletedParts Count' => count($deletedParts),
	                '$finalFileSize' => $finalFileSize,
	                '$chunks_totalFileSize' => $chunks_totalFileSize
	            );
	
	            $Core->Log($debugNote,'media_fileAdd');
	        }

	        /** Clear Gallery Cache */
	        $this->clearGalleryCache($this->getDirIdByFolderPath($dirFolderName));
        }
        
        return $return;
    }
    
    /**
     * File Upload with Media Filesystem Class
     */
    public function mfsFileUpload($targetFileName, $targetPath) {
        /**
         * @var $Core Core
         * @var $i18n i18n
         * @var $Request Request
         */
        $Core = $this->Core;
        $i18n = $Core->i18n();
        $Request = $Core->Request();
        
    	$return = array();
        $return['success'] = false;
        $return['message'] = $this->Core->i18n()->translate('Fehler beim speichern der Datei...');

        $post = $Request->getPost();

        $uuid          = (array_key_exists('qquuid',          $post)) ? $post['qquuid']          : 0;
        $totalFileSize = (array_key_exists('qqtotalfilesize', $post)) ? $post['qqtotalfilesize'] : 0;
        $qqFileName    = (array_key_exists('qqfilename',      $post)) ? $post['qqfilename']      : $targetFileName;
        
        $chunks_totalCount = (array_key_exists('qqtotalparts', $post)) ? $post['qqtotalparts'] : 0;
        $chunks_index      = (array_key_exists('qqpartindex',  $post)) ? $post['qqpartindex']  : 0;
        $chunks_size       = (array_key_exists('qqchunksize',  $post)) ? $post['qqchunksize']  : 0;

        $chunks_folderName = $Core->Helper()->createCleanString($qqFileName) . '_' . $totalFileSize;

    	$mediaFilesystem = $this->getMediaFilesystem();
    	
    	$mediaFilesystem->setInputName('file');
    	$mediaFilesystem->setChunksFolder($chunks_folderName);
    	$mediaFilesystem->setUploadFolderPath($targetPath);
    	
    	$uploadResult = $mediaFilesystem->upload();
    	
    	$return['success'] = (array_key_exists('success', $uploadResult) && is_bool($uploadResult['success'])) ? $uploadResult['success'] : false;
        $return['error']   = (array_key_exists('error', $uploadResult)) ? $this->Core->i18n()->translate($uploadResult['error']) : $this->Core->i18n()->translate('Fehler beim speichern der Datei...');
        $return['message'] = ($return['success']) ? $i18n->translate('Datei gespeichert...') : $return['error'];
        
        if($return['success']) {
        	if(array_key_exists('error', $return)) {
        		unset($return['error']);
        	}
        	
        	$dirFolderName = rtrim(str_replace(trim($this->getDirPath(DS),DS), '',$targetPath),DS) . DS;
			
			$uuidFileDir  = $targetPath . $uuid . DS;
			$uuidFileFile = $uuidFileDir . $qqFileName;
			
	    	if($mediaFilesystem->fileExists($uuidFileFile)) {
	    		$targetFileName = $qqFileName;
	            if (file_exists($targetPath . $qqFileName)) {
	                $targetFileName = pathinfo($targetFileName, PATHINFO_FILENAME) . '_' . time() . '.' . pathinfo($targetFileName, PATHINFO_EXTENSION);
	            }
	            
	            $targetFile = $targetPath . $targetFileName;
	            $sourceFile = $uuidFileFile;
	            
	    		$chunks_folderName = $Core->Helper()->createCleanString($targetFileName) . '_' . $totalFileSize;
	            
	            if(rename($sourceFile,$targetFile)) {
	                $return['success'] = true;
	                $return['message'] = $i18n->translate('Datei gespeichert...');
	
	                $this->clearTempChunks($chunks_folderName);
	                $this->_rmdir_recursive($uuidFileDir);
	
	                $this->createThumbnail($targetFileName, $dirFolderName);
	            } else {
	                $return['success'] = false;
	                $return['message'] = $i18n->translate('Fehler beim speichern der Datei...');
	                $return['error']   = $return['message'];
	
	                $this->clearTempChunks($chunks_folderName);
	                $this->_rmdir_recursive($uuidFileDir);
	            }
	            
	    	}
        }

        $debugNote = array(
            '$post'              => $post,
            '$uploadResult'      => $uploadResult,
            '$return'            => $return,
            '$chunks_folderName' => $chunks_folderName,
            '$targetPath'        => $targetPath,
        );

        $Core->Log($debugNote,'media_fileAdd');

        return $return;
    }

    public function deleteFile($fullFileName, $dirFolderName) {
        $return = array();
        $return['success'] = false;
        $return['message'] = $this->Core->i18n()->translate('Fehler beim löschen der Datei...');

        if ($fullFileName != '' && file_exists($this->getDirPath($dirFolderName) . DS . $fullFileName)) {

            $fullFilePath = $this->getDirPath($dirFolderName) . DS . $fullFileName;

            unlink($fullFilePath);

            $fullThumbnailFilePath = $this->getDirPath($dirFolderName) . DS . 'thumbnails' . DS . $fullFileName;
            if(file_exists($fullThumbnailFilePath)) {
                unlink($fullThumbnailFilePath);
            }

            if(!file_exists($fullFilePath)) {
                $return['success'] = true;
                $return['message'] = sprintf($this->Core->i18n()->translate('Datei \'%s\' gelöscht...'), $fullFileName);
            }

            /** Clear Gallery Cache */
            $this->clearGalleryCache($this->getDirIdByFolderPath($dirFolderName));
        }

        return $return;
    }

    public function getThumbnailInfo($dirFolderName) {
        /** @var $Helper Helper */
        $Helper = $this->Core->Helper();
        $isWindows = $Helper->isWindows();
        $isCli = $Helper->isCli();

        $thInfo = array();

        $path = $this->getDirPath($dirFolderName);
        $thPath = $path . DS . 'thumbnails' . DS;
        if(!is_dir($thPath)) { mkdir($thPath, 0777, true); }
        $thInfoFile = $thPath . '.thinfo';
        if(!file_exists($thInfoFile)) {
            if($isWindows && $isCli) {
                if(strpos($thInfoFile, '\\') !== false) {
//                    $thInfoFile = str_replace(array('\\','.thinfo'),array('\\\\','\.thinfo'),$thInfoFile);
                    $thInfoFile = str_replace('\\','/',$thInfoFile);
                }
            }
            $thInfoFile_handle = fopen($thInfoFile, 'a+') or die('<pre>Thumbnail Info File Error: Unable to open file ' . $thInfoFile . '!</pre>');
            fwrite($thInfoFile_handle, serialize($thInfo));
            fclose($thInfoFile_handle);
        } else {
            $thInfoFileContents = file_get_contents($thInfoFile);
            $thInfo = (is_array(@unserialize($thInfoFileContents))) ? unserialize($thInfoFileContents) : $thInfo;
        }

        return $thInfo;
    }

    public function setThumbnailInfo($dirFolderName, $fileName) {
        $path = $this->getDirPath($dirFolderName);
        $thInfoFile = $path . DS . 'thumbnails' . DS . '.thinfo';

        $originalFilePath = $path . DS . 'thumbnails' . DS . $fileName;
        $thInfo = $this->getThumbnailInfo($dirFolderName);

        if(file_exists($originalFilePath)){
            $thInfo[$fileName] = md5_file($originalFilePath);
        }

        if(@unlink($thInfoFile)){
            $thInfoFile_handle = fopen($thInfoFile, 'a+') or die('<pre>Thumbnail Info File Error: Unable to open file ' . $thInfoFile . '!');
            fwrite($thInfoFile_handle, serialize($thInfo));
            fclose($thInfoFile_handle);
        }

        return $thInfo;
    }

    public function geThumbnailMaxWidth() {
        $maxWidth_config = $this->Core->Config()->get('media_thumb_max_width');

        $maxWidth = ($maxWidth_config !== '' && is_numeric($maxWidth_config)) ? $maxWidth_config : 1024;

        return $maxWidth;
    }

    public function geThumbnailMaxHeight() {
        $maxHeight_config = $this->Core->Config()->get('media_thumb_max_height');

        $maxHeight = ($maxHeight_config !== '' && is_numeric($maxHeight_config)) ? $maxHeight_config : 768;

        return $maxHeight;
    }

    public function createThumbnail($fullFileName, $dirFolderName){
        $originalsPath = $this->getDirPath($dirFolderName);
        $targetPath    = $originalsPath . DS .  'thumbnails';

        $thInfo = $this->getThumbnailInfo($dirFolderName);

        $doCreateThumb = true;

        if(!file_exists($originalsPath . DS . $fullFileName)) { return false; }

        /** check if image */
        $fileMimeType = $this->Core->Helper()->getMimeContentType($originalsPath . DS . $fullFileName);
        if(strpos($fileMimeType, 'image/') === false) {
            $doCreateThumb = false;
        }

        if($doCreateThumb) {
            /** Check if Thumbnail is already created */
            $originalFileMd5 = md5_file($originalsPath . DS . $fullFileName);
            if(array_key_exists($fullFileName, $thInfo) && $thInfo[$fullFileName] == $originalFileMd5) {
                $doCreateThumb = false;
            }
        }

        if($doCreateThumb) {
            /** Memory can handle */
            $mem_limit    = ini_get ('memory_limit');
            $imageInfo    = getimagesize($originalsPath . DS . $fullFileName);
            $memoryNeeded = 0;
            $imageInfo_0        = (is_array($imageInfo) && array_key_exists(0,$imageInfo) && is_numeric($imageInfo[0])) ? $imageInfo[0] : 0;
            $imageInfo_1        = (is_array($imageInfo) && array_key_exists(1,$imageInfo) && is_numeric($imageInfo[1])) ? $imageInfo[0] : 0;
            $imageInfo_bits     = (is_array($imageInfo) && array_key_exists('bits',$imageInfo) && is_numeric($imageInfo['bits'])) ? $imageInfo['bits'] : 0;
            $imageInfo_channels = (is_array($imageInfo) && array_key_exists('channels',$imageInfo) && is_numeric($imageInfo['channels'])) ? $imageInfo['channels'] : 0;
            if(
                $imageInfo_0 > 0 &&
                $imageInfo_1 > 0 &&
                $imageInfo_bits > 0 &&
                $imageInfo_channels > 0
            ) {
                $memoryNeeded = round(($imageInfo_0 * $imageInfo_1 * $imageInfo_bits * $imageInfo_channels / 8 + Pow(2, 16)) * 1.65);
            }
            $shouldHaveMemory = (memory_get_usage() + $memoryNeeded);
            $getMemoryLimit_byte = (integer) $mem_limit * pow(1024, 2);

//            $maxMemoryNeeded = 5242880; /** 50M */
//
//            if($memoryNeeded > $maxMemoryNeeded) {
//                $doCreateThumb = false;
//            }
//            if($doCreateThumb && ($shouldHaveMemory > $getMemoryLimit_byte)) {
            if($shouldHaveMemory > $getMemoryLimit_byte) {
                // $doCreateThumb = false;
                ini_set('memory_limit', (integer) $mem_limit + ceil(((memory_get_usage() + $memoryNeeded) - (integer) $mem_limit * pow(1024, 2)) / pow(1024, 2)) . 'M');
            }
        }

        if($doCreateThumb) {
            $isCli = $this->Core->Helper()->isCli();

            $maxWidth = $this->geThumbnailMaxWidth();
            $maxHeight = $this->geThumbnailMaxHeight();

            try {
                set_time_limit ( 0 );
                ini_set('gd.jpeg_ignore_warning',1);
                if($isCli) {
                    ini_set('memory_limit', '2048M');
                }
                if(!is_dir($targetPath)) {
                    mkdir($targetPath);
                }

                /** @var $imagingClass Imaging */
                $imagingClass = $this->_imagingClass();

                $imagingClass->set_img($originalsPath . DS . $fullFileName);
                $imagingClass->set_quality(80);
                $imagingClass->set_size($maxWidth, $maxHeight);

                $thumbnailFileName = $fullFileName;
                $thumbnailFilePath = $targetPath;

                /** @TODO: What to do if Thumbnail exists */
//                if (file_exists($thumbnailFilePath . DS . $thumbnailFileName)) {
//                    $thumbnailFileName = pathinfo($thumbnailFileName, PATHINFO_FILENAME) . '_' . time() . '.' . pathinfo($thumbnailFileName, PATHINFO_EXTENSION);
//                }

                $thumbnail = $thumbnailFilePath . DS . $thumbnailFileName;

                $imagingClass->save_img($thumbnail);
                $imagingClass->clear_cache();

                $this->setThumbnailInfo($dirFolderName, $fullFileName);

                return true;

            } catch (Exception $e) {
                $note = 'Creating Thumbnail Error on File \'' . $originalsPath . DS . $fullFileName . '\': ' . $e->getMessage();
                $this->Core->Log($note, 'media_filesystem', true);

                return false;
            }
        } else {
            return false;
        }
    }

    public function dirFolderCleanUp($dirFolderName) {
        if($this->checkIfDirFolderExists($dirFolderName)) {
            $isCli = $this->Core->Helper()->isCli();
            try {

                set_time_limit ( 0 );
                $dirPath   = $this->getDirPath($dirFolderName);
                $thDirPath = $dirPath . DS . 'thumbnails';

                /** Check for Thumbnails */
                $dirFiles = scandir($dirPath);

                /** Clean Dir Files */
                $dirFilesClean = array();
                foreach($dirFiles as $fileName) {
                    if($fileName != '.' && $fileName != '..' && $fileName != '.thinfo' && !is_dir($fileName)) {
                        $thumbnailFullFilePath = $dirPath . DS . 'thumbnails' . DS . $fileName;
                        /** check if image */
                        $fileMimeType = $this->Core->Helper()->getMimeContentType($dirPath . DS . $fileName);
                        $isImage = (strpos($fileMimeType, 'image/') !== false) ? true : false;
                        if (!file_exists($thumbnailFullFilePath) && $isImage) {
                            $dirFilesClean[] = $fileName;
                        }
                    }
                }

                $fi=1; $gi=0; foreach($dirFilesClean as $fileName) {
                    $output = $fi . '/' . count($dirFilesClean) . ' ' . $fileName . ' => ';
                    if($this->createThumbnail($fileName, $dirFolderName)) {
                        $output .= $this->Core->i18n()->translate('Thumbnail generated.');
                        $gi++;
                    } else {
                        $output .= $this->Core->i18n()->translate('Thumbnail could not be generated.');
                    }
                    if($isCli) {
                        echo $output . PHP_EOL;
                    }
                    $fi++;
                }

                /** Check for Originals */
                if(!is_dir($thDirPath)) {
                    mkdir($thDirPath);
                }
                $thDirFiles = scandir($thDirPath);
                $ui=0; foreach($thDirFiles as $fileName) {
                    if($fileName != '.' && $fileName != '..' && $fileName != '.thinfo' && !is_dir($fileName)) {

                        $thumbnailFullFilePath = $thDirPath . DS . $fileName;
                        $originalFullFilePath  = $dirPath . DS . $fileName;

                        if(!file_exists($originalFullFilePath)) {
                            unlink($thumbnailFullFilePath);

                            if($isCli) {
                                echo $fileName . ' deleted...' . PHP_EOL;
                            }
                            $ui++;
                        }
                    }
                }

                /** Check for Chunk Folder and Delete if exists */
                $this->clearTempChunks();

                if($isCli) {
                    $return = array();

                    $return['success'] = true;
                    $return['thumbnails']['needed'] = count($dirFilesClean);
                    $return['thumbnails']['created'] = $gi;
                    $return['originals']['deleted'] = $ui;

                    $this->Core->Shell()->clearScreen();

                    return $return;
                }

                /** Clear Gallery Cache */
                $this->clearGalleryCache($this->getDirIdByFolderPath($dirFolderName));
            } catch (Exception $e) {
                $note = 'Dir Folder Clean Up Error: ' . $e->getMessage();
                $this->Core->Log($note,'media_filesystem',true);

                if($isCli) {
                    $return = array();

                    $return['success'] = false;
                    $return['message'] = $note;

                    $this->Core->Shell()->clearScreen();

                    return $return;
                }

                return false;
            }
        }
    }

    public function clearTempChunks($fileFolderName = null) {
        /** Check for Chunk Folder and Delete if exists */
        $varPath = $this->Core->getRelPath().DS.'var'.DS;
        $tmpPath = $varPath . 'temp' . DS;
        $chunks_path = $tmpPath . 'chunks' . DS;

        $sitesClass = $this->Core->Sites();
        $site       = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
        $curSiteKey = (count($site)) ? $site['urlKey'] : '';
        if($curSiteKey !== '') {
            $chunks_sitePath = $chunks_path . $curSiteKey . DS;
            $chunks_path = $chunks_sitePath;
        }

        if(is_dir($chunks_path)) {
            if($fileFolderName === null) {
                $this->_rmdir_recursive($chunks_path);
            } else {
                if(is_dir($chunks_path . trim(trim($fileFolderName), DS) . DS)) {
                    $this->_rmdir_recursive($chunks_path . trim(trim($fileFolderName), DS));
                }
            }
        }
    }

    /**
     * Dir Id or Dir Array
     *
     * @param $dir int | array
     * @return int
     */
    public function getDirSize($dir) {
        $size = 0; /** Byte */
        $dir = (is_int($dir)) ? $this->getDir($dir) : $dir;
        $dirFolderExists = (array_key_exists('dirFolderExists', $dir) && is_bool($dir['dirFolderExists'])) ? $dir['dirFolderExists'] : false;
        $dirPath = (array_key_exists('dirPath', $dir)) ? $dir['dirPath'] : '';
        if($dirFolderExists) {
            $dirFullPath = $this->getDirPath($dirPath);
            $size = $this->folderSize($dirFullPath);
        }
        return $size;
    }

    public function folderSize($fullPath)
    {
        $size = 0;
        foreach (glob(rtrim($fullPath, '/').'/*', GLOB_NOSORT) as $each) {
            $size += is_file($each) ? filesize($each) : $this->folderSize($each);
        }
        return $size;
    }

    /** Gallery Methods */

    private function _setBasicGalleryStuff () {

        /**
         * @var $Core Core
         * @var $Mvc Mvc
         * @var $Config Config
         * @var $mediaClass Media
         */
        global $Core, $Mvc;
        $Core = $this->Core;
        $Mvc = $Core->Mvc();
        $Config = $Core->Config();
        $mediaClass = $this;

        $curGalleryPath = $this->getCurrGalleryUrlKey();

        if($curGalleryPath !== '') {

            $dir = $this->getDirByFolderPath($curGalleryPath);

            $galleryTitle = $Core->i18n()->translate('Galerie');
            $oldMetaTitle = $galleryTitle;
            $Mvc->setMetaTitle($dir['name'] . ' - ' . $oldMetaTitle);
            $oldPageTitle = $galleryTitle;
            $Mvc->setPageTitle($oldPageTitle . ' - ' . $dir['name']);

            $dirId = $dir['id'];

            $passwordNeeded = $this->getGalleryIsPasswordNeeded($dirId);

            if (!$passwordNeeded) {
                $this->unlockGallery($dirId, $dir['password']);
            }

//            echo '<pre>' . print_r($passwordNeeded, true) . '</pre>'; exit;

//            $unlockedGalleries = $this->getUnlockedGalleries();
            $unlockedGalleries = array_key_exists('unlocked_galleries', $_SESSION) && is_array($_SESSION['unlocked_galleries']) ? $_SESSION['unlocked_galleries'] : array();


            $galleryStyleId = $dir['id_media_galleries_styles'];

            $galleryStyle = ($this->galleryStyleExists($galleryStyleId)) ? $this->getGalleryStyle($galleryStyleId) : array();
            $useGalleryStyle = (count($galleryStyle) && $galleryStyle['active'] == 1);
            if ($useGalleryStyle) {

                $styles_header_prefix = '<!-- Media Gallery Styles -->';
                $getPageAfterBodyStart = $Mvc->getPageAfterBodyStart();

                if (strpos($getPageAfterBodyStart, $styles_header_prefix) === false) {

                    $Mvc->setThemeName('minimal_bs');

                    $viewHeaderFilePath = JF_ROOT_DIR . DS . 'core' . DS . 'models' . DS . 'media' . DS . 'views' . DS . 'galleries' . DS . 'styles.view.header.php';
                    $ob_return_styles_header = '';
                    try {
                        ob_start();
                        include($viewHeaderFilePath);
                        $ob_return_styles_header = ob_get_contents();
                        ob_end_clean();
                    } catch (Exception $e) {
                        $ob_return_styles_header = $e->getMessage();
                    }

                    $Mvc->addPageAfterBodyStart($styles_header_prefix.$ob_return_styles_header);
                }
            }

            if ($Config->get('gallery_view_force_cache')) {
                $Mvc->setForceHeaderCache();
            }

            $isUnlocked = (count($unlockedGalleries) && in_array($dirId, $unlockedGalleries));

            if (!$isUnlocked || $_SERVER['REQUEST_METHOD'] == 'POST') {
                $Mvc->setForceNoFileCache();
            }
        }

    }

    private function _mayCreateGalleryStyleLogoFolder() {
        $galleryStyleLogoFolderRelPath = $this->getGalleryStyleLogoFolderPath();
        if(!file_exists($galleryStyleLogoFolderRelPath)) {
            mkdir($galleryStyleLogoFolderRelPath);
        }
    }

    private function _prepareGalleryStylesData($data) {
        $preparedData = array();
        if(is_array($data) && count($data)) {
            $id                  = (array_key_exists('id',                  $data)) ? $data['id']                  : false;
            $title               = (array_key_exists('title',               $data)) ? $data['title']               : '';
            $logo                = (array_key_exists('logo',                $data)) ? $data['logo']                : '';
            $color_header_bg     = (array_key_exists('color_header_bg',     $data)) ? $data['color_header_bg']     : '#ffffff';
            $color_header_txt    = (array_key_exists('color_header_txt',    $data)) ? $data['color_header_txt']    : '#000000';
            $color_page_bg       = (array_key_exists('color_page_bg',       $data)) ? $data['color_page_bg']       : '#e9e9e9';
            $color_content_bg    = (array_key_exists('color_content_bg',    $data)) ? $data['color_content_bg']    : '#ffffff';
            $color_content_txt   = (array_key_exists('color_content_txt',   $data)) ? $data['color_content_txt']   : '#000000';
            $color_thumbnail_bg  = (array_key_exists('color_thumbnail_bg',  $data)) ? $data['color_thumbnail_bg']  : '#ffffff';
            $color_thumbnail_txt = (array_key_exists('color_thumbnail_txt', $data)) ? $data['color_thumbnail_txt'] : '#000000';
            $active              = (array_key_exists('active',              $data) && $data['active']) ? true      : false;
            $created             = (array_key_exists('created',             $data)) ? $data['created']             : 0;
            $id_acc_users        = (array_key_exists('id_acc_users',        $data)) ? $data['id_acc_users']        : 0;
            $id_system_site    = (array_key_exists('id_system_sites',    $data)) ? $data['id_system_sites']    : 0;

            if($id !== false) {
                /** Set Prepared Data */
                $preparedData['id']                  = $id;
                $preparedData['title']               = $title;
                $preparedData['logo']                = $logo;
                $preparedData['color_header_bg']     = $color_header_bg;
                $preparedData['color_header_txt']    = $color_header_txt;
                $preparedData['color_page_bg']       = $color_page_bg;
                $preparedData['color_content_bg']    = $color_content_bg;
                $preparedData['color_content_txt']   = $color_content_txt;
                $preparedData['color_thumbnail_bg']  = $color_thumbnail_bg;
                $preparedData['color_thumbnail_txt'] = $color_thumbnail_txt;
                $preparedData['active']              = $active;
                $preparedData['created']             = strtotime($created);
                $preparedData['id_acc_users']        = $id_acc_users;
                $preparedData['id_system_sites']    = $id_system_site;
            }
        }
        return $preparedData;
    }

    public function getGalleryStyles($siteId = null, $start = 0, $limit = null, $orderBy = null, $orderDirection = 'asc', $search = null, $active = true){
        $site      = $this->Core->Sites()->getSite();
        $curSiteId = (count($site)) ? $site['id'] : 0;

        $searchSql = ($search !== null && $search !== '') ? ' WHERE lower(`name`) REGEXP lower("' . $search . '")' : '';

        if(!is_bool($active) && $active == 'both') {
            $activeSql = ($searchSql !== '') ? ' AND (`active` = 0 OR `active` > 0)' : ' WHERE (`active` = 0 OR `active` > 0)';
        } else {
            $activeFlag = ($active) ? ' != 0' : ' = 0';
            $activeSql = ($searchSql !== '') ? ' AND `active`' . $activeFlag : ' WHERE `active`' . $activeFlag;
        }

        $siteSql = ($siteId !== null && $this->Core->Sites()->siteExists($siteId)) ? ' AND `id_system_sites` = ' . $siteId : ' AND `id_system_sites` = ' . $curSiteId;

        $orderBySql = ($orderBy !== null) ? ' ORDER BY `' . $orderBy . '` ' . $orderDirection : '';
        $limitSql = ($limit !== null) ? ' LIMIT ' . intval($start) . ',' . intval($limit) : '';

        $getGalleryStyles = array();

        $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'media_galleries_styles`' . $searchSql . $activeSql . $siteSql . $orderBySql . $limitSql . ';';

        $result = $this->Core->Db()->fromDatabase($sql,'@raw');

        foreach($result as $row) {
            $getPrepareData = $this->_prepareGalleryStylesData($row);
            if(count($getPrepareData)) {
                $getGalleryStyles[$row['id_system_sites']][$row['id']] = $getPrepareData;
            }
        }

        return ($siteId !== null && is_int($siteId) && array_key_exists($siteId,$getGalleryStyles)) ? $getGalleryStyles[$siteId] : $getGalleryStyles;
    }

    public function getGalleryStylesCount($siteId = null, $active = true) {

        $getGalleryStyles = array();

        $activeFlag = ($active) ? '1' : '0';
        $activeSql = ' WHERE `active` = ' . $activeFlag;

        $sql = 'SELECT `id_system_sites`, COUNT(*) as \'count\' FROM `' . DB_TABLE_PREFIX . 'media_galleries_styles`' . $activeSql . ' GROUP BY `id_system_sites`;';
        $result = $this->Core->Db()->fromDatabase($sql,'@raw');

        foreach($result as $row) {
            $getGalleryStyles[$row['id_system_sites']] = $row['count'];
        }

        $return = $getGalleryStyles;

        if(!count($getGalleryStyles)) {
            $return = 0;
        }

        if($siteId !== null && is_int($siteId)) {
            if(array_key_exists($siteId,$getGalleryStyles)) {
                $return = $getGalleryStyles[$siteId];
            } else {
                $return = 0;
            }
        }

        return $return;
    }

    public function getGalleryStyle($galleryStyleId)
    {
        $galleryStyle = array();

        if($this->galleryStyleExists($galleryStyleId)) {
            $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'media_galleries_styles` WHERE `id` = :id;';
            $params = array(
                'id' => (int)$galleryStyleId
            );
            $result = $this->Core->Db()->fromDatabase($sql, '@raw', $params);

            foreach($result as $row) {
                $galleryStyle = $this->_prepareGalleryStylesData($row);
            }
        }

        return $galleryStyle;
    }

    public function galleryStyleExists($galleryStyleId)
    {
        $sql = 'SELECT id FROM `' . DB_TABLE_PREFIX . 'media_galleries_styles` WHERE `id` = :id;';
        $params = array(
            'id' => (int)$galleryStyleId
        );
        $result = $this->Core->Db()->fromDatabase($sql,'@raw', $params);
        return (count($result)) ? true : false;
    }

    public function saveGalleryStyle($saveData) {
        $return = array();
        $return['success'] = false;
        $return['msg'] = $this->Core->i18n()->translate('Keine Daten zum Speichern übergeben');

        if(!is_array($saveData) || !count($saveData)) {
            return $return;
        }

        /** @var $accClass Acc */
        $accClass = $this->Mvc->modelClass('Acc');
        $user     = (is_object($accClass)) ? $accClass->getUser() : array();
        $userId   = (count($user)) ? $user['id'] : 0;

        $site   = $this->Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;
        $entryId  = (array_key_exists('id', $saveData) && $saveData['id'] > 0) ? $saveData['id'] : false;

        $createNew = ($entryId === false) ? true : false;
        if ($createNew) {

            $entryTitle                = (array_key_exists('title', $saveData))                                ? trim(strip_tags($saveData['title']))   : '';
            $entryLogo                 = (array_key_exists('logo', $saveData))                                 ? trim($saveData['logo'])                : '';
            $entry_color_header_bg     = (array_key_exists('color_header_bg', $saveData))                      ? trim($saveData['color_header_bg'])     : '#ffffff';
            $entry_color_header_txt    = (array_key_exists('color_header_txt', $saveData))                     ? trim($saveData['color_header_txt'])    : '#000000';
            $entry_color_page_bg       = (array_key_exists('color_page_bg', $saveData))                        ? trim($saveData['color_page_bg'])       : '#e9e9e9';
            $entry_color_content_bg    = (array_key_exists('color_content_bg', $saveData))                     ? trim($saveData['color_content_bg'])    : '#ffffff';
            $entry_color_content_txt   = (array_key_exists('color_content_txt', $saveData))                    ? trim($saveData['color_content_txt'])   : '#000000';
            $entry_color_thumbnail_bg  = (array_key_exists('color_thumbnail_bg', $saveData))                   ? trim($saveData['color_thumbnail_bg'])  : '#ffffff';
            $entry_color_thumbnail_txt = (array_key_exists('color_thumbnail_txt', $saveData))                  ? trim($saveData['color_thumbnail_txt']) : '#000000';
            $entryActive               = (array_key_exists('active', $saveData) && $saveData['active'] == 1)  ? 1                                      : 0;
            $entryUserId               = (array_key_exists('id_acc_users', $saveData))                         ? $saveData['id_acc_users']              : $userId;
            $entryClientId             = $siteId;

            //MySQL Query für die Erstellung
            $entryInsertInto = 'INSERT INTO `' . DB_TABLE_PREFIX . 'media_galleries_styles` (`title`, `logo`, `color_header_bg`, `color_header_txt`, `color_page_bg`, `color_content_bg`, `color_content_txt`, `color_thumbnail_bg`, `color_thumbnail_txt`, `active`, `id_acc_users`, `id_system_sites`, `id_acc_users__changedBy`) VALUES (:title, :logo, :color_header_bg, :color_header_txt, :color_page_bg, :color_content_bg, :color_content_txt, :color_thumbnail_bg, :color_thumbnail_txt, :active, :id_acc_users, :id_system_sites, :id_acc_users__changedBy)';
            $params = array(
                'title'                   => $entryTitle,
                'logo'                    => $entryLogo,
                'color_header_bg'         => $entry_color_header_bg,
                'color_header_txt'        => $entry_color_header_txt,
                'color_page_bg'           => $entry_color_page_bg,
                'color_content_bg'        => $entry_color_content_bg,
                'color_content_txt'       => $entry_color_content_txt,
                'color_thumbnail_bg'      => $entry_color_thumbnail_bg,
                'color_thumbnail_txt'     => $entry_color_thumbnail_txt,
                'active'                  => $entryActive,
                'id_acc_users'            => $entryUserId,
                'id_system_sites'        => $entryClientId,
                'id_acc_users__changedBy' => $entryUserId
            );

            $sqlResult = $this->Core->Db()->toDatabase($entryInsertInto, $params);

            $newEntryId = $this->Core->Db()->lastInsertId();

            if ($newEntryId == ''){
                $return['success'] = false;
                $return['msg'] = $this->Core->i18n()->translate('Konnte Galerie Stil nicht erstellen!');
            } else {
                $return['success'] = true;
                $return['msg'] = $this->Core->i18n()->translate('Galerie Stil erfolgreich erstellt!');
            }
        } else {

            if($this->galleryStyleExists($entryId)) {

                $saveGalleryStyle = $this->getGalleryStyle($entryId);

                $styleSaveData = $saveGalleryStyle;

                foreach($saveData as $saveDataKey => $saveDataValue) {
                    if(array_key_exists($saveDataKey, $styleSaveData)) {
                        $styleSaveData[$saveDataKey] = $saveDataValue;
                    }
                }

                //MySQL Query für die Bearbeitung
                $entryUpdate = '
                UPDATE
                    `' . DB_CONN_DBNAME . '`.`' . DB_TABLE_PREFIX . 'media_galleries_styles`
                SET
                    `title` = :title,
                    `logo` = :logo,
                    `color_header_bg` = :color_header_bg,
                    `color_header_txt` = :color_header_txt,
                    `color_page_bg` = :color_page_bg,
                    `color_content_bg` = :color_content_bg,
                    `color_content_txt` = :color_content_txt,
                    `color_thumbnail_bg` = :color_thumbnail_bg,
                    `color_thumbnail_txt` = :color_thumbnail_txt,
                    `active` = :active,
                    `id_acc_users__changedBy` = :id_acc_users__changedBy
                WHERE
                    `' . DB_TABLE_PREFIX . 'media_galleries_styles`.`id` = :id;';

                $params = array(
                    'id'                      => $styleSaveData['id'],
                    'title'                   => trim(strip_tags($styleSaveData['title'])),
                    'logo'                    => trim($styleSaveData['logo']),
                    'color_header_bg'         => trim($styleSaveData['color_header_bg']),
                    'color_header_txt'        => trim($styleSaveData['color_header_txt']),
                    'color_page_bg'           => trim($styleSaveData['color_page_bg']),
                    'color_content_bg'        => trim($styleSaveData['color_content_bg']),
                    'color_content_txt'       => trim($styleSaveData['color_content_txt']),
                    'color_thumbnail_bg'      => trim($styleSaveData['color_thumbnail_bg']),
                    'color_thumbnail_txt'     => trim($styleSaveData['color_thumbnail_txt']),
                    'active'                  => $styleSaveData['active'],
                    'id_acc_users__changedBy' => $userId
                );

                $sqlResult = $this->Core->Db()->toDatabase($entryUpdate, $params);

                if (!$sqlResult) {
                    $return['success'] = false;
                    $return['msg'] = $this->Core->i18n()->translate('Galerie Stil konnte nicht gespeichert werden!');
                } else {
                    $return['success'] = true;
                    $return['msg'] = $this->Core->i18n()->translate('Galerie Stil erfolgreich gespeichert!');
                }
            }
        }

        return $return;
    }

    public function deleteGalleryStyle($galleryStyleId) {
        $return = array();
        $return['success'] = false;
        $return['message'] = $this->Core->i18n()->translate('Keine Id zum löschen übergeben');

        if(!is_int($galleryStyleId)) {
            return $return;
        }

        /** @var $accClass Acc */
        $accClass = $this->Mvc->modelClass('Acc');
        $canDelete = (!is_object($accClass) || $accClass->hasAccess('media_galleries_styles_delete')) ? true: false;

        $galleryStyle = ($this->galleryStyleExists($galleryStyleId)) ? $this->getGalleryStyle($galleryStyleId) : array();

        if(!count($galleryStyle)) {
            $return['success'] = false;
            $return['message'] = $this->Core->i18n()->translate('Galerie Stil konnte nicht gefunden werden');
            return $return;
        }

        if(!$canDelete) {
            $return['success'] = false;
            $return['message'] = $this->Core->i18n()->translate('Keine Berechtigung zum löschen dieses Galerie Stiles!');
        } else {
            $entryDeleteSql = "DELETE FROM `" . DB_TABLE_PREFIX . "media_galleries_styles` WHERE id = '" . $galleryStyleId . "' LIMIT 1";
            $sqlResult = $this->Core->Db()->toDatabase($entryDeleteSql);
            if (!$sqlResult) {
                $return['success'] = false;
                $return['message'] = sprintf($this->Core->i18n()->translate('MySQL Error: \'%s\''), $this->Core->Db()->errorInfo()[2]);
            } else {
                $return['success'] = true;
                $return['message'] = $this->Core->i18n()->translate('Galerie Stil erfolgreich gelöscht!');

                /** Do Change Gallery Style IDs in Dirs with Parent Dir Gallery Style ID */
                $nestedDirs = $this->getDirsNested(null, 0, false);
                $dirGalleriesStylesIds = $this->getDirInfoNestedOneLevel($nestedDirs, 'id_media_galleries_styles');

                $dirChangeIds = array();
                $dirChangedIds = array();

                foreach($dirGalleriesStylesIds as $dirId => $dirGalleryStyleId) {
                    if($dirGalleryStyleId == $galleryStyleId) {

                        $dirChangeIds[$dirId] = $dirId;
                        $dir = $this->getDir($dirId);
                        $parentDirId = (array_key_exists('id_media_dirs__parent', $dir)) ? $dir['id_media_dirs__parent'] : 0;

                        $newGalleryStyleId = 0;
                        if($this->dirExists($parentDirId)) {
                            $parentDir = $this->getDir($parentDirId);
                            $parentGalleryStyleId = (array_key_exists('id_media_galleries_styles', $parentDir)) ? $parentDir['id_media_galleries_styles'] : 0;

                            if($this->galleryStyleExists($parentGalleryStyleId) && $parentGalleryStyleId != $galleryStyleId) {
                                $newGalleryStyleId = $parentGalleryStyleId;
                            }
                        }

                        $saveData = array();
                        $saveData['id'] = $dir['id'];
                        $saveData['id_media_galleries_styles'] = $newGalleryStyleId;

                        $saveResult = $this->saveDir($saveData);
                        if (is_array($saveResult) && count($saveResult) && array_key_exists('success', $saveResult)) {
                            if ($saveResult['success']) {
                                $dirChangedIds[$dirId]['id'] = $dirId;
                                $dirChangedIds[$dirId]['saveData'] = $saveData;
                                $dirChangedIds[$dirId]['saveResult'] = $saveResult;
                            }
                        }
                    }
                }

                $dirChangeMessage = '';
                if(count($dirChangeIds) > 0) {
                    if(count($dirChangeIds) == count($dirChangedIds)) {
                        $dirChangeMessage = (count($dirChangedIds) > 1) ? sprintf($this->Core->i18n()->translate('%s Ordner wurden angepasst.'), count($dirChangedIds)) : sprintf($this->Core->i18n()->translate('%s Ordner wurde angepasst.'), count($dirChangedIds));
                    }
                    if(count($dirChangedIds) < count($dirChangeIds)) {
                        $dirChangeMessage = (count($dirChangedIds) > 1) ? sprintf($this->Core->i18n()->translate('Nur %s von %s Ordner konnten angepasst werden.'), count($dirChangedIds), count($dirChangeIds)) : sprintf($this->Core->i18n()->translate('Nur %s von %s Ordner konnte angepasst werden.'), count($dirChangedIds), count($dirChangeIds));
                    }
                }

                if($dirChangeMessage != '') {
                    $return['message'] .= '<br />' . $dirChangeMessage;
                }
            }
        }
        return $return;
    }

    public function getGalleryStyleLogoFolderPath() {
        $_galleriesStyleLogoFolderName = $this->_mediaGalleriesStyleLogoFolderName();
        $galleryStyleLogoFolderRelPath = rtrim($this->getMediaRelPath(),DS) . DS . $_galleriesStyleLogoFolderName;

        return $galleryStyleLogoFolderRelPath;
    }

    public function getGalleryStyleLogoFolderUrl($unSecure = false) {
        $mediaUrl = $this->getMediaUrl();
        $_galleriesStyleLogoFolderName = $this->_mediaGalleriesStyleLogoFolderName();
        $unSecureUrl = rtrim($mediaUrl, '/') . '/' . $_galleriesStyleLogoFolderName;

        $dirFolderPath = DS . $_galleriesStyleLogoFolderName . DS;
        $fileName      = '';

        $secureUrl = rtrim($this->getFileUrl($dirFolderPath, $fileName),'/');

        $galleryStyleLogoFolderUrl = ($unSecure) ? $unSecureUrl : $secureUrl;

        return $galleryStyleLogoFolderUrl;
    }

    public function getGalleryIsPasswordNeeded($dirId) {
        $passwordNeeded = true;
        $dir = ($this->dirExists($dirId)) ? $this->getDir($dirId) : array();

        if(count($dir)) {
            $unlockedGalleries = array_key_exists('unlocked_galleries', $_SESSION) && is_array($_SESSION['unlocked_galleries']) ? $_SESSION['unlocked_galleries'] : array();

            $passwordNeeded = (!$dir['public']);

            $parentId = $dir['id_media_dirs__parent'];
            // $isParentDirUnlocked = ($this->dirExists($parentId) && $this->galleryUnlocked($parentId));

            $isParentDirUnlocked = (count($unlockedGalleries) && in_array($parentId, $unlockedGalleries));


            $isFolderShownInParentGallery = $dir['show_in_parent_gallery'];

            if ($passwordNeeded && ($isParentDirUnlocked && $isFolderShownInParentGallery)) {
                $passwordNeeded = false;
            }

//            echo '<pre>' . print_r($passwordNeeded, true) . '</pre>'; exit;
//
//            /** @var $accClass Acc */
//            $accClass = $this->Core->Mvc()->modelClass('Acc');
//
//            $loggedInWithDirOrGalleryAccess = (is_object($accClass) && ($accClass->hasAccess('media_dirs') || $accClass->hasAccess('media_galleries')));
//
//
//            if ($passwordNeeded && $loggedInWithDirOrGalleryAccess) {
//                $passwordNeeded = false;
//            }
        }

        return $passwordNeeded;
    }

    public function galleryUnlocked($dirId) {
        $unlockedGalleries = array_key_exists('unlocked_galleries', $_SESSION) && is_array($_SESSION['unlocked_galleries']) ? $_SESSION['unlocked_galleries'] : array();
        return (count($unlockedGalleries) && in_array($dirId, $unlockedGalleries)) ? true : false;
    }

    public function unlockGallery($dirId, $password = null) {
        $return = false;
        $dir = ($this->dirExists($dirId)) ? $this->getDir($dirId) : array();

        if(count($dir)) {
            $doUnlock = false;
            if($password !== null && $dir['password'] == $password) {
                $doUnlock = true;
            } else{
                if($dir['public']) {
                    $doUnlock = true;
                }

                $passwordNeeded = $this->getGalleryIsPasswordNeeded($dirId);

                if(!$doUnlock && !$passwordNeeded) {
                    $doUnlock = true;
                }
            }

            if($doUnlock) {
                $unlockedGalleries = array_key_exists('unlocked_galleries', $_SESSION) && is_array($_SESSION['unlocked_galleries']) ? $_SESSION['unlocked_galleries'] : array();
                if(!in_array($dir['id'], $unlockedGalleries)) {
                    $unlockedGalleries[] = $dir['id'];
                    $_SESSION['unlocked_galleries'] = $unlockedGalleries;
                }
                $return = true;
            }
        }

        return $return;
    }

    public function galleryDownload($dirFolderName, $downloadFiles, $saveZipFileOnServer = false) {

        $dir = $this->getDirByFolderPath($dirFolderName);

        if(count($dir) && count($downloadFiles)) {
            $mediaFilesystem = $this->getMediaFilesystem();
            $largeFileSizeByte = $mediaFilesystem->getLargeFileSize();

            $archive_file_name = trim($dirFolderName, DS) . '_download_' . time() . '.zip';

            $zipContents = (count($downloadFiles) > 1);

            if($zipContents) {
            	$incPath = JF_ROOT_DIR . DS . 'core' . DS . 'models' . DS . 'media' . DS . 'inc';
            	$fileNotAddedToZipArchive_txtFile = $incPath . DS . 'fileNotAddedToZipArchive.txt';
            	
                if($saveZipFileOnServer) {
                    $zip = new ZipArchive();
                    if ($zip->open($archive_file_name, ZIPARCHIVE::CREATE) !== true) {
                        exit('cannot open \'' . $archive_file_name . '\'' . PHP_EOL);
                    }
                    $file_path = $this->getDirPath($dirFolderName);
                    foreach ($downloadFiles as $file) {
                        $fileSize = $mediaFilesystem->getFileSize($file_path . DS . $file);

                        if($fileSize < $largeFileSizeByte) {
                            $zip->addFile($file_path . DS . $file, $file);
                            $this->setFileDownloads($dirFolderName, $file);
                        } else {
                            $zip->addFile($fileNotAddedToZipArchive_txtFile, $file . '.txt');
                        }
                    }
                    $zip->close();
                } else {
                    $zip = new ZipStream\ZipStream($archive_file_name);
                    $file_path = $this->getDirPath($dirFolderName);
                    
                    foreach ($downloadFiles as $file) {
                        $fileSize = $mediaFilesystem->getFileSize($file_path . DS . $file);

                        if($fileSize < $largeFileSizeByte) {
                            $zip->addFileFromPath($file, $file_path . DS . $file);
                            $this->setFileDownloads($dirFolderName, $file);
                        } else {
                            $zip->addFileFromPath($file . '.txt', $fileNotAddedToZipArchive_txtFile);
                        	
                        }
                    }
                    $zip->finish();
                }

                if (!headers_sent() && file_exists($archive_file_name)) {
                    $this->setDirDownloads($dirFolderName);

                    //then send the headers to force download the zip file
                    header('Content-type: application/zip');
                    header('Content-Disposition: attachment; filename=' . $archive_file_name);
                    header('Content-length: ' . filesize($archive_file_name));
                    header('Pragma: no-cache');
                    header('Expires: 0');
                    readfile($archive_file_name);
                }
            } else {
                $filename = reset($downloadFiles);
                $file_path = $this->getDirPath($dirFolderName);
                $fullFilePath = $file_path . DS . $filename;

                if(is_file($fullFilePath)) {
                    $fileSize = $mediaFilesystem->getFileSize($fullFilePath);

                    /** Handle Single File */
                    header('Content-Type: application/octet-stream');
                    header("Content-Transfer-Encoding: Binary");
                    header('Content-Disposition: attachment; filename="' . $filename . '"');
                    header('Content-length: ' . $fileSize);
                    header('Pragma: no-cache');
                    header('Expires: 0');
                    readfile($fullFilePath);
                }

            }
            exit;
        }
    }

    public function clearGalleryCache($dirId) {
        /**
         * @var $Core Core
         * @var $Mvc Mvc
         */
        $Core = $this->Core;
        $Mvc  = $Core->Mvc();

        if($Mvc->useFileCache()) {
            $dir = ($this->dirExists($dirId)) ? $this->getDir($dirId) : array();

            $dirPath = (array_key_exists('dirPath', $dir)) ? $dir['dirPath'] : '';

            $galleryUrl = $Mvc->getModelUrl('media') . '/galleries/view' . str_replace(DS,'/',$dirPath);

            $fileCacheUrlPlainString = $Mvc->getFileCacheUrlPlainString($galleryUrl);

            $Mvc->delFileCache($fileCacheUrlPlainString);
        }

        return true;
    }

    public function getCurrGalleryUrlKey() {
        $curGallery = '';

        $modelKey        = $this->Mvc->getModelKey();
        $modelController = $this->Mvc->getControllerKey();
        $modelAction     = $this->Mvc->getActionKey();

        if (
            $modelKey == 'media' &&
            $modelController == 'galleries' &&
            $modelAction == 'view'
        ) {
            $params  = $this->Mvc->getMvcParams();

            $curFolderName = '';
            if(count($params)) {
                reset($params);
                $first_key = key($params);
                if($first_key == 'dirFolderName') {
                    $curFolderName = (array_key_exists('dirFolderName', $params)) ? $params['dirFolderName'] : '';
                } else {
                    $curFolderName = $first_key;
                }
            }

            $curUrl = trim($this->Core->Request()->getCurrUrl(),'/') . '/';
            $curMvcModelActionUrl = $this->Mvc->getModelUrl($modelKey) . '/' . $modelController . '/' . $modelAction . '/';
            $curMvcModelActionSlug = trim(str_replace($curMvcModelActionUrl, '', $curUrl),'/');
            $dirPathFromSlug = str_replace('dirpath/','',$curMvcModelActionSlug);
            $curDirPath = ($dirPathFromSlug === '' || $dirPathFromSlug === 'dirpath' || strpos($dirPathFromSlug, 'http') !== false) ? '/' : '/' . $dirPathFromSlug . '/';
            $curDirPath = str_replace('/', DS, $curDirPath);

            $curDirIdByDirPath = $this->getDirIdByFolderPath($curDirPath);

            if($curDirIdByDirPath > 0) {
                $curGallery = $curDirPath;
            }
        }

        return $curGallery;
    }

    public function getUnlockedGalleries() {
        return array_key_exists('unlocked_galleries', $_SESSION) && is_array($_SESSION['unlocked_galleries']) ? $_SESSION['unlocked_galleries'] : array();
    }

    public function unlockedGalleryMenu($type = 'list') {
        $menuHtml = '';
        $unlockedGalleries = $this->getUnlockedGalleries();
        if(count($unlockedGalleries)) {

            $curFolderName = $this->getCurrGalleryUrlKey();

            $menuLvl2Html = '';
            foreach($unlockedGalleries as $dirId) {

//                $dir = $this->getDirByFolderName($folderName);
                $dir = ($this->dirExists($dirId)) ? $this->getDir($dirId) : array();

                if(count($dir)) {

                    $folderName = $dir['dirPath'];

                    $li_attr_class = ($curFolderName == $folderName) ? ' class="active"' : '';

                    $linkTitle = $dir['name'];

//                    $linkUrl = $this->Mvc->getModelUrl('media') . '/galleries/view/' . $dir['dir'];
                    $galleryUrl = $this->Mvc->getModelUrl('media') . '/galleries/view' . str_replace(DS,'/',$dir['dirPath']);
                    $linkUrl = $galleryUrl;

                    $menuLvl2Html .= '<li' . $li_attr_class . '>' . PHP_EOL;
                    $menuLvl2Html .= '<a href="' . $linkUrl . '" title="' . $linkTitle . '">' . $linkTitle . '</a>' . PHP_EOL;
                    $menuLvl2Html .= '</li>' . PHP_EOL;

                }
            }

            if($menuLvl2Html !== '') {
                $liDropdownActiveClass = ($curFolderName !== '') ? ' active' : '';
                $menuHtml = '';
                if($type == 'list') {
                    $menuHtml .= '<li class="dropdown' . $liDropdownActiveClass . '">';

                    $menuHtml .= '<a href="" title="' . $this->Core->i18n()->translate('Galerien') . '" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
                    $menuHtml .= $this->Core->i18n()->translate('Galerien');
                    $menuHtml .= '<span class="caret"></span>';
                    $menuHtml .= '</a>';
                }
                if(strpos($type,'button') !== false) {
                    if($type == 'button') {
                        $menuHtml .= '<div class="btn-group">';
                    }
                    if($type == 'button-lg') {
                        $menuHtml .= '<div class="btn-group btn-group-lg">';
                    }
                    if($type == 'button-sm') {
                        $menuHtml .= '<div class="btn-group btn-group-sm">';
                    }
                    if($type == 'button-xs') {
                        $menuHtml .= '<div class="btn-group btn-group-xs">';
                    }
                    $menuHtml .= '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                    $menuHtml .= $this->Core->i18n()->translate('Galerien');
                    $menuHtml .= ' <span class="caret"></span>';
                    $menuHtml .= '</button>';
                }

                $menuHtml .= '<ul class="dropdown-menu">';
                $menuHtml .= $menuLvl2Html;
                $menuHtml .= '</ul>';

                if($type == 'list') {
                    $menuHtml .= '</li>';
                }
                if(strpos($type,'button') !== false) {
                    $menuHtml .= '</div>';
                }
            }
        }
        return $menuHtml;
    }

    public function addToMvcMenu() {
        return $this->unlockedGalleryMenu();
    }

    /** User API Methods */

    private function _prepareApiUserData($data) {
        $preparedData = array();
        if(is_array($data) && count($data)) {
            $id               = (array_key_exists('id', $data))               ? $data['id']               : false;
            $id_acc_users     = (array_key_exists('id_acc_users', $data))     ? $data['id_acc_users']     : 0;
            $api_key          = (array_key_exists('api_key', $data))          ? $data['api_key']          : '';
            $active           = (array_key_exists('active', $data) && $data['active'] === 1) ? true       : false;
            $created          = (array_key_exists('created', $data))          ? $data['created']          : 0;
            $id_system_site = (array_key_exists('id_system_sites', $data)) ? $data['id_system_sites'] : 0;

            if($id !== false && $id_acc_users != 0) {
                $preparedData['id']               = $id;
                $preparedData['id_acc_users']     = $id_acc_users;
                $preparedData['api_key']          = $api_key;
                $preparedData['active']           = $active;
                $preparedData['created']          = strtotime($created);
                $preparedData['id_system_sites'] = $id_system_site;
            }
        }
        return $preparedData;
    }

    public function getApiUsers($siteId = null, $start = 0, $limit = null, $orderBy = null, $orderDirection = 'asc', $search = null, $active = true) {
        $site      = $this->Core->Sites()->getSite();
        $curSiteId = (count($site)) ? $site['id'] : 0;

        $searchSql = ($search !== null && $search !== '') ? ' WHERE (lower((SELECT CONCAT_WS(" ", `name`, `surname`) FROM `' . DB_TABLE_PREFIX . 'acc_users` WHERE `id`= `' . DB_TABLE_PREFIX . 'media_apisettings`.`id_acc_users`)) REGEXP lower("' . $search . '") OR lower(`api_key`) REGEXP lower("' . $search . '"))' : '';

        $activeFlag = ($active) ? ' != 0' : ' = 0';
        $activeSql = ($searchSql !== '') ? ' AND `active`' . $activeFlag : ' WHERE `active`' . $activeFlag;

        $siteSql = ($siteId !== null && $this->Core->Sites()->siteExists($siteId)) ? ' AND `id_system_sites` = ' . $siteId : ' AND `id_system_sites` = ' . $curSiteId;

        $orderBySql = ($orderBy !== null) ? ' ORDER BY `' . $orderBy . '` ' . $orderDirection : '';
        $limitSql = ($limit !== null) ? ' LIMIT ' . intval($start) . ',' . intval($limit) : '';

        $getApiUsers = array();

        $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'media_apisettings`' . $searchSql . $activeSql . $siteSql . $orderBySql . $limitSql . ';';

        $result = $this->Core->Db()->fromDatabase($sql,'@raw');

        foreach($result as $row) {
            $getPrepareData = $this->_prepareApiUserData($row);
            if(count($getPrepareData)) {
                $getApiUsers[$row['id_system_sites']][$row['id']] = $getPrepareData;
            }
        }

        return ($siteId !== null && is_int($siteId) && array_key_exists($siteId,$getApiUsers)) ? $getApiUsers[$siteId] : $getApiUsers;
    }

    public function getApiUsersCount($siteId = null, $active = true) {

        $getApiUsers = array();

        $site      = $this->Core->Sites()->getSite();
        $curSiteId = (count($site)) ? $site['id'] : 0;

        $activeFlag = ($active) ? ' != 0' : ' = 0';
        $activeSql = ' WHERE `active`' . $activeFlag;

        $siteSql = ($siteId !== null && $this->Core->Sites()->siteExists($siteId)) ? ' AND `id_system_sites` = ' . $siteId : ' AND `id_system_sites` = ' . $curSiteId;

        $sql = 'SELECT `id_system_sites`, COUNT(*) as \'count\' FROM `' . DB_TABLE_PREFIX . 'media_apisettings`' . $activeSql . $siteSql . ' GROUP BY `id_system_sites`;';
        $result = $this->Core->Db()->fromDatabase($sql,'@raw');

        foreach($result as $row) {
            $getApiUsers[$row['id_system_sites']] = $row['count'];
        }

        $return = $getApiUsers;

        if(!count($getApiUsers)) {
            $return = 0;
        }

        if($siteId !== null && is_int($siteId)) {
            if(array_key_exists($siteId,$getApiUsers)) {
                $return = $getApiUsers[$siteId];
            } else {
                $return = 0;
            }
        }

        return $return;
    }

    public function getApiUser($apiUserEntryId)
    {
        $apiUser = array();

        if($this->apiUserExists($apiUserEntryId)) {
            $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'media_apisettings` WHERE `id` = :id;';
            $params = array(
                'id' => (int)$apiUserEntryId
            );
            $result = $this->Core->Db()->fromDatabase($sql, '@raw', $params);

            foreach($result as $row) {
                $apiUser = $this->_prepareApiUserData($row);
            }
        }

        return $apiUser;
    }

    public function getApiUserByUserId($userId) {
        $apiUser = array();

        /** @var $accClass Acc */
        $accClass = $this->Mvc->modelClass('Acc');
        $user = (is_object($accClass) && $accClass->userExists($userId)) ? $accClass->getUser($userId) : array();

        if(count($user)) {
            $site = $this->Core->Sites()->getSite();
            $curSiteId = (count($site)) ? $site['id'] : 0;
            $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'media_apisettings` WHERE `id_acc_users` = :id_acc_users AND `id_system_sites` = :id_system_sites;';
            $params = array(
                'id_acc_users' => (int)$userId,
                'id_system_sites' => $curSiteId
            );

            $result = $this->Core->Db()->fromDatabase($sql, '@raw', $params);

            foreach ($result as $row) {
                $apiUser = $this->_prepareApiUserData($row);
            }
        }

        return $apiUser;
    }

    public function apiUserExists($apiUserEntryId)
    {
        $sql = 'SELECT id FROM `' . DB_TABLE_PREFIX . 'media_apisettings` WHERE `id` = :id;';
        $params = array(
            'id' => (int)$apiUserEntryId
        );
        $result = $this->Core->Db()->fromDatabase($sql,'@raw', $params);
        return (count($result)) ? true : false;
    }

    public function saveApiUser($saveData) {
        $return = array();
        $return['success'] = false;
        $return['msg'] = $this->Core->i18n()->translate('Keine Daten zum Speichern übergeben');

        if(!is_array($saveData) || !count($saveData)) {
            return $return;
        }

        /** @var $accClass Acc */
        $accClass = $this->Mvc->modelClass('Acc');
        $user     = (is_object($accClass)) ? $accClass->getUser() : array();
        $userId   = (count($user)) ? $user['id'] : 0;

        $site   = $this->Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $entryId  = (isset($saveData['id'])) ? $saveData['id'] : false;

        $createNew = ($entryId === false) ? true : false;
        if ($createNew) {

            $entryUserId    = (isset($saveData['id_acc_users'])) ? trim(strip_tags($saveData['id_acc_users'])) : '';
            $entryApiKey    = (isset($saveData['api_key'])) ? $saveData['api_key'] : '';
            $entryActive    = (isset($saveData['active']) && $saveData['active'] == 1) ? 1 : 0;
            $entryCurUserId = $userId;
            $entryClientId  = $siteId;

            //MySQL Query für die Erstellung
            $entryInsertInto = 'INSERT INTO `' . DB_TABLE_PREFIX . 'media_apisettings` (`id_acc_users`, `api_key`, `active`, `id_system_sites`, `id_acc_users__changedBy`) VALUES (:id_acc_users, :api_key, :active, :id_system_sites, :id_acc_users__changedBy)';
            $params = array(
                'id_acc_users'     => $entryUserId,
                'api_key'          => $entryApiKey,
                'active'           => $entryActive,
                'id_system_sites' => $entryClientId,
                'id_acc_users__changedBy' => $entryCurUserId
            );

            $sqlResult = $this->Core->Db()->toDatabase($entryInsertInto, $params);

            $newEntryId = $this->Core->Db()->lastInsertId();

            if ($newEntryId == ''){
                $return['success'] = false;
                $return['msg'] = $this->Core->i18n()->translate('Konnte API-Zugang nicht erstellen!');
            } else {
                $return['success'] = true;
                $return['msg'] = $this->Core->i18n()->translate('API-Zugang erfolgreich erstellt!');
            }
        } else {

            if($this->apiUserExists($entryId)) {

                $saveApiUser = $this->getApiUser($entryId);

                $apiUserSaveData = $saveApiUser;

                foreach($saveData as $saveDataKey => $saveDataValue) {
                    if(array_key_exists($saveDataKey, $apiUserSaveData)) {
                        $apiUserSaveData[$saveDataKey] = $saveDataValue;
                    }
                }

                //MySQL Query für die Bearbeitung
                $entryUpdate = '
                UPDATE
                    `' . DB_CONN_DBNAME . '`.`' . DB_TABLE_PREFIX . 'media_apisettings`
                SET
                    `id_acc_users` = :id_acc_users,
                    `api_key` = :api_key,
                    `active` = :active,
                    `id_acc_users__changedBy` = :id_acc_users__changedBy
                WHERE
                    `' . DB_TABLE_PREFIX . 'media_apisettings`.`id` = :id;';

                $params = array(
                    'id' => $apiUserSaveData['id'],
                    'id_acc_users' => $apiUserSaveData['id_acc_users'],
                    'api_key' => trim(strip_tags($apiUserSaveData['api_key'])),
                    'active' => $apiUserSaveData['active'],
                    'id_acc_users__changedBy' => $userId
                );

                $sqlResult = $this->Core->Db()->toDatabase($entryUpdate, $params);

                if (!$sqlResult) {
                    $return['success'] = false;
                    $return['msg'] = $this->Core->i18n()->translate('API-Zugang konnte nicht gespeichert werden!');
                } else {
                    $return['success'] = true;
                    $return['msg'] = $this->Core->i18n()->translate('API-Zugang erfolgreich gespeichert!');
                }
            }
        }

        return $return;
    }

    public function deleteApiUser($apiUserEntryId) {
        $return = array();
        $return['success'] = false;
        $return['msg'] = $this->Core->i18n()->translate('Keine Id zum löschen übergeben');

        if(!is_int($apiUserEntryId)) {
            return $return;
        }

        /** @var $accClass Acc */
        $accClass = $this->Mvc->modelClass('Acc');
        $canDelete = (!is_object($accClass) || $accClass->hasAccess('media_api_delete')) ? true: false;

        $apiUserEntry = ($this->apiUserExists($apiUserEntryId)) ? $this->getApiUser($apiUserEntryId) : array();

        $apiUser = (array_key_exists('id_acc_users', $apiUserEntry) && is_object($accClass) && $accClass->userExists($apiUserEntry['id_acc_users'])) ? $accClass->getUser($apiUserEntry['id_acc_users']) : array();
        $apiUserName = (count($apiUser)) ? $apiUser['name'] . ' ' . $apiUser['surname'] : '';

        if(!count($apiUserEntry)) {
            $return['success'] = false;
            $return['msg'] = $this->Core->i18n()->translate('API-Zugang konnte nicht gefunden werden');
            return $return;
        }

        if(!$canDelete) {
            $return['success'] = false;
            $return['msg'] = $this->Core->i18n()->translate('Keine Berechtigung zum löschen dieses API-Zugangs!');
        } else {
            $sql = 'DELETE FROM `' . DB_TABLE_PREFIX . 'media_apisettings` WHERE id = :id LIMIT 1';
            $params = array(
                'id' => $apiUserEntryId
            );

            $sqlResult = $this->Core->Db()->toDatabase($sql, $params);

            if (!$sqlResult){
                $return['success'] = false;
                $return['msg'] = sprintf( $this->Core->i18n()->translate('MySQL Error: \'%s\''), $this->Core->Db()->errorInfo()[2]);
            }
            else {
                $return['success'] = true;
                $return['msg'] = sprintf( $this->Core->i18n()->translate('API-Zugangs für User \'%s\' permanent gelöscht!!'), $apiUserName);
            }
        }
        return $return;
    }

    public function createApiKey($userId = 0) {
        $additionalSecret = time() . session_id() . rand();
        if($userId !== 0) {
            /** @var $accClass Acc */
            $accClass = $this->Mvc->modelClass('Acc');

            $user = (is_object($accClass) && $accClass->userExists($userId)) ? $accClass->getUser($userId,true) : array();
            $userSalt = (count($user) && array_key_exists('salt',$user)) ? $user['salt'] : $additionalSecret;

            $additionalSecret = $userId . $userSalt;
        }
        $api_key = hash('sha256', ($additionalSecret));

        return $api_key;
    }

    public function validateApiKey($apiKey,$userId) {
        $sql = 'SELECT id FROM `' . DB_TABLE_PREFIX . 'media_apisettings` WHERE `id_acc_users` = :id_acc_users AND `api_key` = :api_key AND `active` = 1;';
        $params = array(
            'id_acc_users' => (int)$userId,
            'api_key' => $apiKey
        );
        $result = $this->Core->Db()->fromDatabase($sql,'@raw', $params);
        return (count($result)) ? true : false;
    }

    public function apiAuthenticate() {
        /** @var $accClass Acc */
        $accClass = $this->Core->Mvc()->modelClass('Acc');

        $curUser   = (is_object($accClass)) ? $accClass->getUser() : array();
        $userId = (count($curUser) && array_key_exists('id',$curUser)) ? $curUser['id'] : 0;

        $logData = array();

        if(!$userId) {

            $mail = (array_key_exists('PHP_AUTH_USER', $_SERVER)) ? $_SERVER['PHP_AUTH_USER'] : '';
            $pass = (array_key_exists('PHP_AUTH_PW', $_SERVER)) ? $_SERVER['PHP_AUTH_PW'] : '';

            $data = array(
                'mail' => $mail,
                'pass' => $pass
            );

            $validated = (is_object($accClass)) ? $accClass->userCheckLoginCredentials($data) : array('success' => false);


            if ($validated['success'] === false) {
                header('WWW-Authenticate: Basic realm="My Realm"');
                header('HTTP/1.0 401 Unauthorized');
                header('Content-Type: application/json; charset=utf-8');

                $message = (is_array($validated) && array_key_exists('message',$validated)) ? $validated['message'] : $this->Core->i18n()->translate('Nicht Autorisiert');

                $return = array(
                    'success' => false,
                    'message' => $message
                );

                $logData['success'] = $return['success'];
                $logData['message'] = $return['message'];
                $logData['$_POST'] = $_POST;
                $logData['$_GET'] = $_GET;
                $logData['$_SERVER[\'REQUEST_METHOD\']'] = $_SERVER['REQUEST_METHOD'];
                $this->Core->Log($logData,'media_api', true);

                return $return;
            } else {
                $userId = (is_array($validated) && array_key_exists('userId',$validated)) ? $validated['userId'] : 0;
                if($userId === 0) {
                    $return = array(
                        'success' => false,
                        'message' => $this->Core->i18n()->translate('Kein Benzutzer gefunden...')
                    );

                    $logData['success'] = $return['success'];
                    $logData['message'] = $return['message'];
                    $logData['$_POST'] = $_POST;
                    $logData['$_GET'] = $_GET;
                    $logData['$_SERVER[\'REQUEST_METHOD\']'] = $_SERVER['REQUEST_METHOD'];
                    $this->Core->Log($logData,'media_api', true);

                    return $return;
                }
            }
        }

        $params  = $this->Core->Mvc()->getMvcParams();

        $paramApiKey = '';

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'key') {
                $paramApiKey = (array_key_exists('key', $params)) ? $params['key'] : '';
            } else {
                $paramApiKey = $first_key;
            }
        }

        if($paramApiKey === '') {

            $getApiKey = (array_key_exists('key',$_GET)) ? $_GET['key'] : '';
            if($getApiKey === '') {
                $return = array(
                    'success' => false,
                    'message' => $this->Core->i18n()->translate('Kein Api Key gefunden...')
                );

                $logData['success'] = $return['success'];
                $logData['message'] = $return['message'];
                $logData['api_key'] = $paramApiKey;
                $logData['$_POST'] = $_POST;
                $logData['$_GET'] = $_GET;
                $logData['$_SERVER[\'REQUEST_METHOD\']'] = $_SERVER['REQUEST_METHOD'];
                $this->Core->Log($logData,'media_api', true);

                return $return;
            } else {
                $paramApiKey = $getApiKey;
            }
        }

        $validApiKey = $this->validateApiKey($paramApiKey,$userId);

        if(!$validApiKey) {
            $return = array(
                'success' => false,
                'message' => $this->Core->i18n()->translate('Api Key Fehlerhaft...')
            );

            $logData['success'] = $return['success'];
            $logData['message'] = $return['message'];
            $logData['api_key'] = $paramApiKey;
            $logData['$_POST'] = $_POST;
            $logData['$_GET'] = $_GET;
            $logData['$_SERVER[\'REQUEST_METHOD\']'] = $_SERVER['REQUEST_METHOD'];
            $this->Core->Log($logData,'media_api', true);

            return $return;
        }

        // If arrives here, is a valid user.


        $return = array(
            'success' => true,
            'message' => $this->Core->i18n()->translate('Authentifizierung erfolgreich'),
            'userId'  => $userId
        );

        $logData['success'] = $return['success'];
        $logData['message'] = $return['message'];
        $logData['api_key'] = $paramApiKey;
        $logData['userId'] = $return['userId'];
        $logData['$_POST'] = $_POST;
        $logData['$_GET'] = $_GET;
        $logData['$_SERVER[\'REQUEST_METHOD\']'] = $_SERVER['REQUEST_METHOD'];
        $this->Core->Log($logData,'media_api_authentication', true);

        return $return;
    }

    /** Cron Methods */

    public function cron() {
        $return = array();
        $return['success'] = true;
        $return['message'] = 'Media Cron executed!';

        /** @var $sitesClass Sites */
        $sitesClass = $this->Core->Sites();
        $site      = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
        $curSiteId = (count($site)) ? $site['id'] : 0;

        /** @var $configClass Config */
        $configClass = $this->Core->Config();
        $rightsWhitelistConfig = $configClass->get('rightsWhitelist', $curSiteId);
        $rightsWhitelist = (is_array($rightsWhitelistConfig) ? $rightsWhitelistConfig : array());

        if(in_array('media_dirs_archive',$rightsWhitelist)) {
            /** Run Archive Cron */
            $cronArchive_Result = $this->_cronArchive_run();
            $return['cronArchiveResult'] = $cronArchive_Result;
        }

        if(in_array('media_dirs_delete',$rightsWhitelist)) {
            /** Run Auto Delete Cron */
            $cronAutoDelete_Result = $this->_cronAutoDelete_run();
            $return['cronAutoDeleteResult'] = $cronAutoDelete_Result;
        }

        /** Cron Log */
        $this->Core->Log($return, 'media_cron_result', true);

        return $return;
    }

    /** Cron Archive */
    private function _cronArchive_run() {
        $return = array();
        $return['success'] = false;
        $return['message'] = 'An Error occurred..';

        if(date('H') != '3') {
            $return['success'] = false;
            $return['message'] = 'It is not 3 am...';

            return $return;
        }

        /** @var $sitesClass Sites */
        $sitesClass = $this->Core->Sites();
        $site      = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
        $siteId    = (count($site)) ? $site['id'] : 0;

        $getDirsConfig = array(
            'siteId'               => $siteId,
            'parentId'               => 'all',
            'archived'               => true,
            'onlyFirstArchiveParent' => true
        );
        $archivedDirs = $this->getDirs($getDirsConfig);

        $nonBackupArchivedDirs = array();

        foreach($archivedDirs as $dirId => $dir) {
            if($dir['archive_state'] > 0 && $dir['archive_state'] !== 2) {
                $nonBackupArchivedDirs[$dirId] = $dir;
            }
        }

        if(count($nonBackupArchivedDirs)) {
            /** Upload Directory via FTP recursively */
            $archivedDirsNested = array();
            foreach($nonBackupArchivedDirs as $dirId => $dir) {
                $getDirsFullConfig = $getDirsConfig;
                $getDirsFullConfig['parentId'] = $dirId;
                $getDirsFullConfig['onlyFirstArchiveParent'] = false;

                $archivedDirsNested[$dirId] = $this->getDir($dirId);
                $archivedDirsNested[$dirId]['children'] = $this->getDirsNested($this->getDirs($getDirsFullConfig), $dirId, false, 'id');
            }

            $systemDirArchiveAction_result = $this->_cronArchive_systemDirArchiveAction($archivedDirsNested);

            $return['result']['systemDirArchiveAction'] = $systemDirArchiveAction_result;

            /** Set FTP Copy Success State to Root Archive Dirs */
            foreach($systemDirArchiveAction_result as $dirId => $ftpCopyResult) {
                $getRootArchiveDirId = $this->getFirstArchiveParentFolderId($dirId);
                $getRootArchiveDir   = $this->getDir($getRootArchiveDirId);
                $copySuccess         = (array_key_exists('success', $ftpCopyResult) && is_bool($ftpCopyResult['success'])) ? $ftpCopyResult['success'] : false;

                if(!$copySuccess && $getRootArchiveDir['archive_state'] !== 3) {
                    /** Update Dir Archive Failure Information in Database */
                    $this->setArchiveStateToDir($getRootArchiveDirId, 3);
                }
            }

            /** Execute Local Folder Action on FTP Transferred Folders */
            $archivedDirsNested_localAction = array();
            foreach($archivedDirsNested as $dirId => $dir) {
                $getDirsFullConfig = $getDirsConfig;
                $getDirsFullConfig['parentId'] = $dirId;
                $getDirsFullConfig['onlyFirstArchiveParent'] = false;

                /** Local Action Array */
                $archivedDirsNested_localAction[$dirId] = $this->getDir($dirId);
                $archivedDirsNested_localAction[$dirId]['children'] = $this->getDirsNested($this->getDirs($getDirsFullConfig), $dirId, false, 'id');
            }

            $localDirArchiveAction_result = $this->_cronArchive_localDirArchiveAction($archivedDirsNested_localAction);

            $return['result']['localDirArchiveAction'] = $localDirArchiveAction_result;

            $return['success'] = true;
            $return['message'] = 'Archive Cron done!';
        } else {
            $return['success'] = false;
            $return['message'] = 'nothing to archive...';
        }

        return $return;
    }

    private function _cronArchive_ftpSettings() {
        $_cronArchiveFtpSettings = (is_array($this->_cronArchiveFtpSettings)) ? $this->_cronArchiveFtpSettings : array();
        if(!count($_cronArchiveFtpSettings)) {
            /**
             * @var $Core Core
             * @var $Config Config
             */
            $Core   = $this->Core;
            $Config = $Core->Config();

            $ftpHost_config = $Config->get('media_archive_ftpHost');
            $ftpUser_config = $Config->get('media_archive_ftpUser');
            $ftpPass_config = $Config->get('media_archive_ftpPass');

            if(
                $ftpHost_config !== '' &&
                $ftpUser_config !== '' &&
                $ftpPass_config !== ''
            ) {
                $_cronArchiveFtpSettings['ftp_host'] = $ftpHost_config;
                $_cronArchiveFtpSettings['ftp_user'] = $ftpUser_config;
                $_cronArchiveFtpSettings['ftp_pass'] = $ftpPass_config;
            }
        }
        return $_cronArchiveFtpSettings;
    }

    private function _cronArchive_systemDirArchiveAction($archiveDirsNested) {
        $return = array();

        if(is_array($archiveDirsNested) && count($archiveDirsNested)) {
            /** Upload Directory via FTP */
            foreach ($archiveDirsNested as $dirId => $dir) {
                if ($dir['archived'] === true && $dir['backup'] === false) {
                    $dirFolderName = $dir['dirPath'];

                    $ftpCopyResult = $this->_cronArchive_ftpCopy($dirFolderName);

                    $return[$dirId] = $ftpCopyResult;

                    if(array_key_exists('children',$dir) && is_array($dir['children']) && count($dir['children'])) {
                        $return = $return + $this->_cronArchive_systemDirArchiveAction($dir['children']);
                    }
                }
            }
        }

        return $return;
    }

    private function _cronArchive_localDirArchiveAction($archiveDirsNested) {
        $return = array();

        if(is_array($archiveDirsNested) && count($archiveDirsNested)) {

            foreach($archiveDirsNested as $dirId => $dir) {

                if(($dir['archive_state'] > 0 && $dir['archive_state'] !== 2) && $dir['dirFolderExists']) {

                    /** If children exists, run Local Dir Archive Action on children first */
                    if(array_key_exists('children',$dir) && is_array($dir['children']) && count($dir['children'])) {
                        $return = $return + $this->_cronArchive_localDirArchiveAction($dir['children']);
                    }

                    /** Update Dir Archive Information in Database */
                    $return[$dirId]['setArchiveStateToDir'] = $this->setArchiveStateToDir($dir['id'], 2, false);

                    /** Delete Local Dir Files */
                    $return[$dirId]['deleteDirFolder'] = $this->deleteDirFolder($dir['dirPath']);

                    /** Sync File Download Statistics */
                    $return[$dirId]['syncFileDownloads'] = $this->syncFileDownloads();
                }
            }
        }

        return $return;
    }

    private function _cronArchive_ftpCopy($dirFolderName)
    {
        $return = array();
        $return['success'] = false;
        $return['message'] = 'An Error occurred..';

        if ($this->checkIfDirFolderExists($dirFolderName)) {

            $_ftpSettings = $this->_cronArchive_ftpSettings();

            $ftp_server   = (is_array($_ftpSettings) && array_key_exists('ftp_host', $_ftpSettings)) ? $_ftpSettings['ftp_host'] : '';
            $ftp_user     = (is_array($_ftpSettings) && array_key_exists('ftp_user', $_ftpSettings)) ? $_ftpSettings['ftp_user'] : '';;
            $ftp_password = (is_array($_ftpSettings) && array_key_exists('ftp_pass', $_ftpSettings)) ? $_ftpSettings['ftp_pass'] : '';;

            if(
                $ftp_server   === '' ||
                $ftp_user     === '' ||
                $ftp_password === ''
            ) {
                $resultMsg = "FTP connection has failed!";
                $resultMsg .= " -> FTP-Settings not set...";

                $return['success'] = false;
                $return['message'] = $resultMsg;
            } else {

                $portalFolderName = str_replace(array('http','https'),'', $this->Core->Helper()->createCleanString($this->Core->getBaseUrl()));
                $ftpDirPath = '/' . $portalFolderName . '/archive/' . $this->_mediaBaseFolderName() . '/' . $this->_mediaUploadsFolderName() . '/' . trim(str_replace(DS,'/',$dirFolderName),DS);

                $localDirPath = $this->getDirPath($dirFolderName);

                set_time_limit(0);
                $start_time = explode(" ", microtime());
                $start_time = $start_time[1] + $start_time[0];

                $conn_id = ftp_connect($ftp_server);
                $login_result = ftp_login($conn_id, $ftp_user, $ftp_password);

                // turn passive mode on
                ftp_pasv($conn_id, true);

                // set Transfer Mode
                $ftp_transfer_mode = FTP_BINARY; /* FTP_ASCII | FTP_BINARY */

                // Connection successful?
                if ((!$conn_id) || (!$login_result)) {
                    $resultMsg = "FTP connection has failed!";
                    $resultMsg .= "Attempted to connect to $ftp_server for user $ftp_user";

                    $return['success'] = false;
                    $return['message'] = $resultMsg;
                }

                // Create Directories and Sub-Directories
                $this->_cronArchive_ftp_mksubdirs($conn_id, '', trim($ftpDirPath,'/'));

                // Loop through folder and upload each file
                $counter = 0;
                $filesArray = glob($localDirPath . '/*.*');

                foreach ($filesArray as $filename) {
                    $destination_dir = $ftpDirPath . '/';
                    $destination = $destination_dir . basename($filename);
                    $upload = ftp_put($conn_id, $destination, $filename, $ftp_transfer_mode);

                    if($upload) {
                        $counter++;
                    }
                }

                // Close connection
                ftp_close($conn_id);

                // Success??
                if ($counter != count($filesArray)) {
                    if($counter === 0) {
                        $return['success'] = false;
                        $return['message'] = 'FTP copy has failed!';
                    } else {
                        $return['success'] = false;
                        $return['message'] = 'FTP copy not fully successful. Copying Result: only ' . $counter . '/' . count($filesArray) . 'uploaded...';

                    }
                } else {
                    $time_end = explode(" ", microtime());
                    $time_end = $time_end[1] + $time_end[0];
                    $zeitmessung = $time_end - $start_time;
                    $zeitmessung = substr($zeitmessung, 0, 8);

                    $resultMsg = "Successfully transferred <strong>" . $counter . "</strong> files from the directory <strong>" . $dirFolderName . "</strong> to the archive!<br />";

                    $resultMsg .= "Dauer: ";
                    $resultMsg .= floor($zeitmessung / 60) . " Minuten und " . ($zeitmessung % 60) . " Sekunden";

                    $return['success'] = true;
                    $return['message'] = $resultMsg;
                }
            }
        }

        return $return;
    }

    private function _cronArchive_ftp_mksubdirs($ftpcon,$ftpbasedir,$ftpath){
        @ftp_chdir($ftpcon, $ftpbasedir);
        $parts = explode('/',$ftpath);
        foreach($parts as $part){
            if(!@ftp_chdir($ftpcon, $part)){
                @ftp_mkdir($ftpcon, $part);
                @ftp_chdir($ftpcon, $part);
                //ftp_chmod($ftpcon, 0777, $part);
            }
        }
    }

    /** Cron AutoDelete */
    private function _cronAutoDelete_run() {
        $return = array();
        $return['success'] = false;
        $return['message'] = 'An Error occurred..';

        if(date('H') != '3') {
            $return['success'] = false;
            $return['message'] = 'It is not 3 am...';

            return $return;
        }

        /** @var $sitesClass Sites */
        $sitesClass = $this->Core->Sites();
        $site      = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
        $siteId    = (count($site)) ? $site['id'] : 0;

        $getDirsConfig = array(
            'siteId' => $siteId,
            'parentId' => 'all',
            'archived' => 'both'
        );
        $allDirs = $this->getDirs($getDirsConfig);

        $deleteDirs = array();
        foreach($allDirs as $dirId => $dir) {
            $dirDeleteTimeStamp = ($dir['max_lifetime'] !== 0 && (strtotime($dir['max_lifetime']) !== false)) ? strtotime($dir['max_lifetime']) : 0;

            if($dirDeleteTimeStamp !== 0 && (date('Ymd') == date('Ymd',$dirDeleteTimeStamp))) {
                $deleteDirs[$dirId] = $dir;
            }
        }

        if(count($deleteDirs)) {
            /** Upload Directory via FTP recursively */
            $deleteDirsNested = array();
            foreach($deleteDirs as $dirId => $dir) {
                $getDirsFullConfig = $getDirsConfig;
                $getDirsFullConfig['parentId'] = $dirId;

                $deleteDirsNested[$dirId] = $this->getDir($dirId);
                $deleteDirsNested[$dirId]['children'] = $this->getDirsNested($this->getDirs($getDirsFullConfig), $dirId, false, 'id');
            }

            $autoDeleteAction_result = $this->_cronAutoDelete_autoDeleteAction($deleteDirsNested);

            $return['result']['autoDeleteAction'] = $autoDeleteAction_result;

            $return['success'] = true;
            $return['message'] = 'Auto Delete Cron done!';
        } else {
            $return['success'] = false;
            $return['message'] = 'nothing to delete...';
        }

        return $return;
    }

    private function _cronAutoDelete_autoDeleteAction($deleteDirsNested) {
        $return = array();

        if(is_array($deleteDirsNested) && count($deleteDirsNested)) {

            foreach($deleteDirsNested as $dirId => $dir) {

                $dirDeleteTimeStamp = ($dir['max_lifetime'] !== 0 && (strtotime($dir['max_lifetime']) !== false)) ? strtotime($dir['max_lifetime']) : 0;

                if($dirDeleteTimeStamp !== 0 && (date('Ymd') == date('Ymd',$dirDeleteTimeStamp))) {

                    /** If children exists, run Auto Delete Action on children first */
                    if(array_key_exists('children',$dir) && is_array($dir['children']) && count($dir['children'])) {
                        $return = $return + $this->_cronAutoDelete_autoDeleteAction($dir['children']);
                    }

                    /** Delete Dir */
                    $return[$dirId]['deleteDirFolder'] = $this->deleteDir($dir['id'], true, true);
                }
            }
        }

        return $return;
    }

    /** Shell Methods */

    public function shell($commandArgsArray) {

        /** @var $Shell Shell */
        $Shell = $this->Core->Shell();

        /** @var $Sites Sites */
        $Sites = $this->Core->Sites();
        $curSite = (is_object($Sites) && is_array($Sites->getSite())) ? $Sites->getSite() : array();
        $curSiteUrlKey = (count($curSite) && array_key_exists('urlKey',$curSite)) ? ' ' . $curSite['urlKey'] : '';

        $helpOutput = '/** Acc Help */' . PHP_EOL;
        $helpOutput .= 'For a List of Commands type "$ php shell.php' . $curSiteUrlKey . ' media --list"' . PHP_EOL . PHP_EOL;

        $helpOutput = $Shell->getColoredString($helpOutput, 'green');

        $commandModelKey = (array_key_exists(0, $commandArgsArray)) ? $commandArgsArray[0] : '';

        $command = (array_key_exists(1, $commandArgsArray)) ? $commandArgsArray[1] : '';
        $action  = (array_key_exists(2, $commandArgsArray)) ? $commandArgsArray[2] : '';

        $areas = array(
            '--help' => 'For Acc Basic Help',
            '--list' => 'To see this List',
            '--dir'  => 'Dir Area',
        );

        $shellOutput = '';

        switch ($command) {
            case '--help':
                $shellOutput .= $helpOutput;
                break;
            case '--list':
                $listOutput = '/** Media Command List */' . PHP_EOL;
                foreach($areas as $area => $areaText) {
                    $listOutput .= '  * ' . $area . ' /** ' . $areaText . ' */' . PHP_EOL;
                }
                $shellOutput .= $Shell->getColoredString($listOutput , 'yellow');
                break;
            default:
                if($command === '') {
                    $shellOutput .= $helpOutput;
                }
                if(!array_key_exists($command,$areas)) {
                    $shellOutput .= $Shell->getColoredString('/** Media Command "' . $command . '" not found... */' . PHP_EOL . PHP_EOL , 'red');
                }
        }

        if (array_key_exists($command,$areas)) {

            $area = $command;

            if ($area == '--dir') {
                $actionArray = array(
                    '-list' => 'To see Sub Command List from this Area',
                    '-getDirs' => 'get List of Dirs',
                    '-dirFolderCleanUp' => 'Clean Up Dir Folder by Dir Path, i.e. "-dirFolderCleanUp ' . DS . 'dir' . DS . 'path' . DS . '"',
                );

                $shellOutput .= '/** Media Dirs Area */' . PHP_EOL;

                $actionHelpOutput = '=> Help' . PHP_EOL;
                $actionHelpOutput .= '   For a List of Action Commands for this Area type "$ php shell.php' . $curSiteUrlKey . ' media ' . $area . ' -list"' . PHP_EOL . PHP_EOL;

                $actionHelpOutput = $Shell->getColoredString($actionHelpOutput, 'green');

                if ($action !== '') {

                    switch ($action) {
                        case '-list':
                            $shellOutput .= '=> Media Action Command List:' . PHP_EOL;
                            foreach ($actionArray as $actionKey => $actionText) {
                                $shellOutput .= '  * ' . $actionKey . ' /** ' . $actionText . ' */' . PHP_EOL;
                            }
                            break;
                        default:
                            if ($action === '') {
                                $shellOutput .= $actionHelpOutput;
                            }
                            if (!array_key_exists($action, $actionArray)) {
                                $errorOutput = '=> Media Area Action Command "' . $action . '" not found... */' . PHP_EOL;
                                $shellOutput .= $Shell->getColoredString($errorOutput,'red') . PHP_EOL;
                            }
                    }

                    if (!array_key_exists($action, $actionArray)) {
                        $errorOutput = '=> Action Command "' . $action . '" not found...' . PHP_EOL;
                        $shellOutput .= $Shell->getColoredString($errorOutput,'red') . PHP_EOL;
                    } else {

                        if($action == '-getDirs') {
                            $getDirsOutput = '';
                            $getDirsOutput .= '=> Dir List:' . PHP_EOL;

                            /** @var $sitesClass Sites */
                            $sitesClass = $this->Core->Sites();
                            $site = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
                            $siteId = (count($site)) ? $site['id'] : 0;

                            $getDirsConfig = array(
                                'siteId'       => $siteId,
                                'parentId'       => 'all'
                            );
                            $dirs = $this->getDirs($getDirsConfig);

                            $dirsNested = $this->buildNestedArrayForGetDirsNestedOneLevel($dirs);

                            $dirPathNestedOneLevel = $this->getDirInfoNestedOneLevel($dirsNested, 'dirPath');

                            if(is_array($dirPathNestedOneLevel)) {
                                foreach($dirPathNestedOneLevel as $dirPath) {
                                    $getDirsOutput .= '  * ' . $dirPath . PHP_EOL;
                                }
                            }

                            $shellOutput .= $getDirsOutput;
                        }

                        if($action == '-dirFolderCleanUp') {
                            $getDirsOutput = '';
                            $getDirsOutput .= '=> Dir Folder Clean Up:' . PHP_EOL;

                            $dirPath = (array_key_exists(3, $commandArgsArray)) ? $commandArgsArray[3] : '';

                            $dir = $this->getDirByFolderPath($dirPath);

                            $getDirsOutput .= '  * Dir Path "' . $dirPath . '"' . PHP_EOL;

                            if(is_array($dir) && count($dir)) {
                                $dirName = (array_key_exists('name', $dir)) ? $dir['name'] : '';
                                $dirFolderExists = (array_key_exists('dirFolderExists', $dir) && is_bool($dir['dirFolderExists'])) ? $dir['dirFolderExists'] : false;

                                if($dirFolderExists) {
                                    $getDirsOutput .= '  * Dir Name "' . $dirName . '"' . PHP_EOL;
                                    $cleanUpResult = $this->dirFolderCleanUp($dirPath);

                                    $cleanUpResult_success = (is_array($cleanUpResult) && array_key_exists('success',$cleanUpResult) && is_bool($cleanUpResult['success'])) ? $cleanUpResult['success'] : false;
                                    if($cleanUpResult_success) {
                                        $cleanUpResult_thumbnails = (is_array($cleanUpResult) && array_key_exists('thumbnails',$cleanUpResult) && is_array($cleanUpResult['thumbnails'])) ? $cleanUpResult['thumbnails'] : array();

                                        $thumbnailsNeeded  = (array_key_exists('needed',$cleanUpResult_thumbnails)) ? $cleanUpResult_thumbnails['needed'] : 0;
                                        $thumbnailsCreated = (array_key_exists('created',$cleanUpResult_thumbnails)) ? $cleanUpResult_thumbnails['created'] : 0;

                                        $cleanUpResult_originals = (is_array($cleanUpResult) && array_key_exists('originals',$cleanUpResult) && is_array($cleanUpResult['originals'])) ? $cleanUpResult['originals'] : array();

                                        $originalsDeleted  = (array_key_exists('deleted',$cleanUpResult_originals)) ? $cleanUpResult_originals['deleted'] : 0;

                                        $getDirsOutput .= '  * ' . $thumbnailsCreated . '/' . $thumbnailsNeeded . ' Thumbnails created' . PHP_EOL;
                                        $getDirsOutput .= '  * ' . $originalsDeleted . ' Originals deleted' . PHP_EOL;
                                    } else {
                                        $cleanUpResult_messaage = (is_array($cleanUpResult) && array_key_exists('message',$cleanUpResult)) ? $cleanUpResult['message'] : '';

                                        $getDirsOutput .= '  * Dir Cleanup returns an Error:' . PHP_EOL;
                                        $getDirsOutput .= '    => ' . $cleanUpResult_messaage . PHP_EOL;
                                    }
                                }
                            }

                            $shellOutput .= $getDirsOutput;
                        }

                    }

                } else {
                    $shellOutput .= $actionHelpOutput;
                }
            }
        }

        return $shellOutput . PHP_EOL;
    }
}
