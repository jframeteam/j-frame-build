<?php
/**
 * Media Model - Filesystem Class
 *
 * Some Methods implemented from Source by http://fineuploader.com https://github.com/FineUploader/php-traditional-server
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** Set Path to inc Folder */
$incPath = JF_ROOT_DIR . DS . 'core' . DS . 'models' . DS . 'media' . DS . 'inc';

/** include Vendor Autoload */
require_once($incPath . DS . 'vendor' . DS . 'autoload.php');

/** Include the upload handler class */
require_once($incPath . DS . 'vendor' . DS . 'fineuploader' . DS . 'php-traditional-server' . DS . 'handler.php');

/** Media Class */
class MediaFilesystem {

    /**
     * @var Core
     * @var Mvc
     */
    private $Core;
    private $Mvc;
    private $uploader = null;
    private $chunksFolderName = null;
    private $chunksFolderPath = null;
    private $uploadFolderPath = null;
    private $inputName = "qqfile";

    /**
     * MediaFilesystem constructor.
     * @param $Core Core
     */
    function __construct($Core)
    {
        $this->Core = $Core;
        $this->Mvc = $Core->Mvc();
    }

    private function _rmdir_recursive($fullDirPath)
    {
        foreach(scandir($fullDirPath) as $file) {
            if ('.' === $file || '..' === $file) { continue; }
            if (is_dir($fullDirPath . DS . $file))  { $this->_rmdir_recursive($fullDirPath . DS . $file); }
            else { unlink($fullDirPath . DS . $file); }
        }
        rmdir($fullDirPath);
    }
    
    public function getRequestMethod() {
    	global $HTTP_RAW_POST_DATA;

	    if(isset($HTTP_RAW_POST_DATA)) {
	    	parse_str($HTTP_RAW_POST_DATA, $_POST);
	    }
	
	    if (isset($_POST["_method"]) && $_POST["_method"] != null) {
	        return $_POST["_method"];
	    }
	
	    return $_SERVER["REQUEST_METHOD"];
    }
    
    private function _initUploader() {
		$uploader = new UploadHandler();

		// Specify the list of valid extensions, ex. array("jpeg", "xml", "bmp")
		$uploader->allowedExtensions = array(); // all files types allowed by default
		
		// Specify max file size in bytes.
		$uploader->sizeLimit = null;
		
		// Specify the input name set in the javascript.
		$uploader->inputName = $this->getInputName(); // matches Fine Uploader's default inputName value by default
		
		$this->uploader = $uploader;
    }
    
    public function uploader() {
    	$uploader = $this->uploader;
    	if($uploader === null) {
            $this->_initUploader();
    		$uploader = $this->uploader;
    	}
    	return $uploader;
    }
    
    public function folderExists($folderPath) {
    	return is_dir($folderPath);
    }
    
    public function fileExists($filePath) {
    	return file_exists($filePath);
    }
    
    public function createFolder($folderPath, $chmod=0755, $recursive=true) {
        if(!$this->folderExists($folderPath)) {
            return mkdir($folderPath,$chmod,$recursive);
        }
        return $this->folderExists($folderPath);
    }

    public function getLargeFileSize() {
        $largeFileSize_config = $this->Core->Config()->get('media_large_file_size');

        $largeFileSize = ($largeFileSize_config !== '' && is_numeric($largeFileSize_config)) ? $largeFileSize_config : 2147483648; /** 2GB */

        return $largeFileSize;
    }

    public function getFileSize($fullFilePath) {
        $fileSize = 0;
        try {
            try {
                $fileSize = filesize($fullFilePath);
            } catch (Exception $e) {
                exec("du -b $fullFilePath", $out);
                $line = explode("\t", $out[0]);
                $fileSize = $line[0];
            }
        } catch (Exception $e) {
            $this->Core->Log('Get FileSize failed for ' . $fullFilePath . ': ' . $e->getMessage(), 'media_filesystem');
        }
        return $fileSize;
    }

    public function getFileMTime($fullFilePath) {
        $file_mtime = 0;
        try {
            try {
                $file_mtime = filemtime($fullFilePath);
            } catch (Exception $e) {
                $file_mtime = exec("stat '$fullFilePath'");
                if(strpos('Change:', $file_mtime) !== false) {
                    $file_mtime = trim(str_replace('Change:', '', $file_mtime));
                }

                $file_mtime_timestamp = strtotime($file_mtime);

                if($file_mtime_timestamp) {
                    $file_mtime = $file_mtime_timestamp;
                }
            }
        } catch (Exception $e) {
            $this->Core->Log('Get FileMTime failed for ' . $fullFilePath . ': ' . $e->getMessage(), 'media_filesystem');
        }
        return $file_mtime;
    }
    
    public function setInputName($inputName) {
    	$this->inputName = $inputName;
    }
    
    public function getInputName() {
    	return $this->inputName;
    }
    
    public function setUploadFolderPath($uploadFolderPath) {
    	$this->uploadFolderPath = $uploadFolderPath;
    }
    
    public function getUploadFolderPath() {
    	$uploadFolderPath = $this->uploadFolderPath;
    	if($uploadFolderPath === null) {
            $this->createFolder($uploadFolderPath);
            $this->uploadFolderPath = $uploadFolderPath;
    	}
    	return $uploadFolderPath;
    }
    
    public function getChunkRootFolder() {
        $var_path    = $this->Core->getRelPath().DS.'var'.DS;
        $tmp_path    = $var_path . 'temp' . DS;
        $chunks_path = $tmp_path . 'chunks' . DS;

        $sitesClass  = $this->Core->Sites();
        $site       = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
        $curSiteKey = (count($site)) ? $site['urlKey'] : '';
        if($curSiteKey !== '') {
            $chunks_sitePath = $chunks_path . $curSiteKey . DS;
            $chunks_path = $chunks_sitePath;
        }

        $this->createFolder($chunks_path);
        
        return $chunks_path;
    }
    
    public function setChunksFolder($chunksFolderPath) {
    	$chunkRootFolderPath = $this->getChunkRootFolder();
    	if(strpos($chunkRootFolderPath, $chunksFolderPath) === false) {
    		$this->chunksFolderName = $chunksFolderPath;
    		$chunksFolderPath = $chunkRootFolderPath . rtrim($chunksFolderPath, DS) . DS;
    	}
    	$this->createFolder($chunksFolderPath);
    	
    	$this->chunksFolderPath = $chunksFolderPath;
    }
    
    public function getChunksFolder() {
    	$chunksFolderPath = $this->chunksFolderPath;
    	if($chunksFolderPath === null) {
            $chunks_path = $this->getChunkRootFolder() . time() . DS;
            
            $this->createFolder($chunks_path);
            
            $chunksFolderPath = $chunks_path;
            $this->chunksFolderPath = $chunksFolderPath;
    	}
    	return $chunksFolderPath;
    }
    
    public function upload() {
        /**
         * @var $Core Core
         * @var $Config Config
         * @var $Plugins Plugins
         */
        $Core = $this->Core;
        $Config = $Core->Config();
        $Plugins = $Core->Plugins();

        /** @var $uploader UploadHandler */
    	$uploader = $this->uploader();
    	
    	$uploadDirectory = $this->getUploadFolderPath();
    	$chunkDirectory  = $this->getChunksFolder();
    	$chunkFolderName = $this->chunksFolderName;
    	
		$method = $this->getRequestMethod();
		
        $uploader->chunksFolder = $chunkDirectory;
		
        $debugNote = array(
            '$uploadDirectory' => $uploadDirectory,
            '$method'          => $method,
        );

        $this->Core->Log($debugNote,'media_fileAdd');
		
		if ($method == "POST") {
		
		    // Assumes you have a chunking.success.endpoint set to point here with a query parameter of "done".
		    // For example: /myserver/handlers/endpoint.php?done
		    if (isset($_GET["qqchunkuploaddone"])) {

                /** Check for Chunk Combiner Plugin */
                $chunkCombiner_config = $Config->get('chunkCombiner');
                $chunkCombinerPlugin = (is_string($chunkCombiner_config) && $chunkCombiner_config !== '') ? $Plugins->$chunkCombiner_config() : null;

                if(is_object($chunkCombinerPlugin)){
            		try {
	                    $chunkCombinerPlugin->setUploadFolderPath($uploadDirectory);
	                    if($chunkFolderName !== null) {
	                    	$chunkCombinerPlugin->setChunksFolder($chunkFolderName);
	                    } else {
	                    	$chunkCombinerPlugin->setChunksFolder($chunkDirectory);
	                    }
	                    $result = $chunkCombinerPlugin->combineChunks();
		            } catch (Exception $e) {
		                $result = $uploader->combineChunks($uploadDirectory);
		                $result['chunkCombinerPlugin_error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";

			            $debugNote = array(
			                '$post' => $Core->Request()->getPost(),
			                'chunkCombinerPlugin_error' => $result['chunkCombinerPlugin_error'],
			                '$chunkFolderName' => $chunkFolderName,
			                '$chunkDirectory' => $chunkDirectory
			            );

			            $Core->Log($debugNote,'media_filesystem_upload');
		            }
                } else {
                    $result = $uploader->combineChunks($uploadDirectory);
                }
		    }
		    // Handles upload requests
		    else {
		        // Call handleUpload() with the name of the folder, relative to PHP's getcwd()
		        $result = $uploader->handleUpload($uploadDirectory);
		
		        // To return a name used for uploaded file you can use the following line.
		        $result["uploadName"] = $uploader->getUploadName();
		    }
		}
		// for delete file requests
		else if ($method == "DELETE") {
		    $result = $uploader->handleDelete($uploadDirectory);
		}
		else {
		    $result = array("success" => false, "error" => 'Method Not Allowed');
		}
		
		return $result;
    }
    

    public function clearTempChunks($fileFolderName = null) {
        /** Check for Chunk Folder and Delete if exists */
        $chunks_path = $this->getChunkRootFolder();

        if(is_dir($chunks_path)) {
            if($fileFolderName === null) {
                $this->_rmdir_recursive($chunks_path);
            } else {
                if(is_dir($chunks_path . trim(trim($fileFolderName), DS) . DS)) {
                    $this->_rmdir_recursive($chunks_path . trim(trim($fileFolderName), DS));
                }
            }
        }
    }
}
