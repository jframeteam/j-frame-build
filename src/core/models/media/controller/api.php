<?php
/**
 * Media Model Controller API
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function indexAction()
{
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if (is_object($accClass) && !$accClass->hasAccess('media_api')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('media_api') . ' - ' . $Core->i18n()->translate('media') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('media') . ' - ' . $Core->i18n()->translate('media_api'));

        $site = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $curUser = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $canEdit = (!is_object($accClass) || $accClass->hasAccess('media_api_edit'));

        $return = '';

        if (!is_object($accClass)) {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Model Acc nicht geladen.') . '</div>';
        } else {

            $ob_return = '';
            if ($Mvc->getMVCViewFilePath() !== false) {
                try {
                    ob_start();
                    include($Mvc->getMVCViewFilePath());
                    $ob_return = ob_get_contents();
                    ob_end_clean();
                } catch (Exception $e) {
                    $ob_return = $e->getMessage();
                }
            } else {
                $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
            }

            $return .= $ob_return;
        }

        return $return;
    }
}

function logviewAction()
{
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if (is_object($accClass) && !$accClass->hasAccess('media_api_logview')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('media_api_logview') . ' - ' . $Core->i18n()->translate('media') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('media') . ' - ' . $Core->i18n()->translate('media_api_logview'));

        $site = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $curUser = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $canEdit = (!is_object($accClass) || $accClass->hasAccess('media_api_edit'));

        $return = '';

        if (!is_object($accClass)) {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Model Acc nicht geladen.') . '</div>';
        } else {

            $ob_return = '';
            if ($Mvc->getMVCViewFilePath() !== false) {
                try {
                    ob_start();
                    include($Mvc->getMVCViewFilePath());
                    $ob_return = ob_get_contents();
                    ob_end_clean();
                } catch (Exception $e) {
                    $ob_return = $e->getMessage();
                }
            } else {
                $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
            }

            $return .= $ob_return;
        }

        return $return;
    }
}

function saveAction()
{
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    /**
     * @var $plugins Plugins
     * @var $pitsForms PitsForms
     * @var $pitsCore PitsCore
     */

    /** Prepare Form Plugin */
    $plugins = $Core->Plugins();
    $pitsForms = $plugins->PitsForms();

    $pitsCore = $plugins->PitsCore();

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(!is_object($accClass) || ($accClass->hasAccess('media_api_edit') || $accClass->hasAccess('media_api_create'))) {

        $return = '';

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if(is_object($pitsForms)) {
                $pitsCore->decryptPOST();
            }
            $post = $Core->Request()->getPost();

//            return '<pre>' . print_r($post, true) . '</pre>';
            if(count($post)) {
                if (isset($post['action'])) {
                    if ($post['action'] == 'api_create' || $post['action'] == 'api_edit') {
                        $saveResult = $mediaClass->saveApiUser($post);
                        $return = '<pre>' . print_r($post, true) . '</pre>';
                        $return .= '<pre>' . print_r($saveResult, true) . '</pre>';

                        if(is_array($saveResult) && count($saveResult) && array_key_exists('success', $saveResult) && array_key_exists('msg', $saveResult)) {
                            $note = $saveResult['msg'];
                            if($saveResult['success']) {
                                $type  = 'success';
                                $kind  = 'bs-alert';
                                $title = '';
                                $Core->setNote($note, $type, $kind, $title);
                            } else {
                                $type  = 'danger';
                                $kind  = 'bs-alert';
                                $title = $Core->i18n()->translate('Fehler') . '!';
                                $Core->setNote($note, $type, $kind, $title);
                            }
                            $redirectUrl = $Mvc->getModelUrl() . '/api';
                            $Core->Request()->redirect($redirectUrl);
                        }
                        return $return;
                    }
                }
            }
        }
    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }
}

function activateAction()
{
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(!is_object($accClass) || $accClass->hasAccess('media_api_edit')) {

        $return = '';

        $params  = $Mvc->getMvcParams();

        $paramEditId = 0;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id') {
                $paramEditId = (array_key_exists('id', $params)) ? (int)$params['id'] : 0;
            } else {
                $paramEditId = (int)$first_key;
            }
        }

        $dir = ($mediaClass->apiUserExists($paramEditId)) ? $mediaClass->getApiUser($paramEditId) : array();

        if(count($dir)) {
            $saveData = array();
            $saveData['id'] = $dir['id'];
            $saveData['active'] = 1;

            $saveResult = $mediaClass->saveApiUser($saveData);
            $return .= '<pre>' . print_r($saveResult, true) . '</pre>';

            if(is_array($saveResult) && count($saveResult) && array_key_exists('success', $saveResult) && array_key_exists('msg', $saveResult)) {
                if($saveResult['success']) {
                    $note = $Core->i18n()->translate('API-Zugang erfolgreich entsperrt!');
                    $type  = 'success';
                    $kind  = 'bs-alert';
                    $title = '';
                    $Core->setNote($note, $type, $kind, $title);
                } else {
                    $note = $Core->i18n()->translate('API-Zugang konnte nicht entsperrt werden!');
                    $type  = 'danger';
                    $kind  = 'bs-alert';
                    $title = $Core->i18n()->translate('Fehler') . '!';
                    $Core->setNote($note, $type, $kind, $title);
                }
                $redirectUrl = $Mvc->getModelUrl() . '/api';
                $Core->Request()->redirect($redirectUrl);
            }
            return $return;

        } else {

            return $Core->i18n()->translate('API-Zugang nicht gefunden...');
        }

    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }
}

function deactivateAction()
{
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(!is_object($accClass) || $accClass->hasAccess('media_api_edit')) {

        $return = '';

        $params  = $Mvc->getMvcParams();

        $paramEditId = 0;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id') {
                $paramEditId = (array_key_exists('id', $params)) ? (int)$params['id'] : 0;
            } else {
                $paramEditId = (int)$first_key;
            }
        }

        $dir = ($mediaClass->apiUserExists($paramEditId)) ? $mediaClass->getApiUser($paramEditId) : array();

        if(count($dir)) {
            $saveData = array();
            $saveData['id'] = $dir['id'];
            $saveData['active'] = 0;

            $saveResult = $mediaClass->saveApiUser($saveData);
            $return .= '<pre>' . print_r($saveResult, true) . '</pre>';

            if(is_array($saveResult) && count($saveResult) && array_key_exists('success', $saveResult) && array_key_exists('msg', $saveResult)) {
                if($saveResult['success']) {
                    $note = $Core->i18n()->translate('API-Zugang erfolgreich gesperrt!');
                    $type  = 'success';
                    $kind  = 'bs-alert';
                    $title = '';
                    $Core->setNote($note, $type, $kind, $title);
                } else {
                    $note = $Core->i18n()->translate('API-Zugang konnte nicht gesperrt werden!');
                    $type  = 'danger';
                    $kind  = 'bs-alert';
                    $title = $Core->i18n()->translate('Fehler') . '!';
                    $Core->setNote($note, $type, $kind, $title);
                }
                $redirectUrl = $Mvc->getModelUrl() . '/api';
                $Core->Request()->redirect($redirectUrl);
            }
            return $return;

        } else {

            return $Core->i18n()->translate('API-Zugang nicht gefunden...');
        }

    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }
}

function deleteAction()
{
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(!is_object($accClass) || $accClass->hasAccess('media_api_delete')) {

        $return = '';

        $params  = $Mvc->getMvcParams();

        $paramDeleteId = 0;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id') {
                $paramDeleteId = (array_key_exists('id', $params)) ? (int)$params['id'] : 0;
            } else {
                $paramDeleteId = (int)$first_key;
            }
        }

        $dir = ($mediaClass->apiUserExists($paramDeleteId)) ? $mediaClass->getApiUser($paramDeleteId) : array();

        if(count($dir)) {

            $deleteResult = $mediaClass->deleteApiUser($paramDeleteId);
            $return .= '<pre>' . print_r($deleteResult, true) . '</pre>';

            if(is_array($deleteResult) && count($deleteResult) && array_key_exists('success', $deleteResult) && array_key_exists('msg', $deleteResult)) {
                $note = $deleteResult['msg'];
                if($deleteResult['success']) {
                    $type  = 'success';
                    $kind  = 'bs-alert';
                    $title = '';
                    $Core->setNote($note, $type, $kind, $title);
                } else {
                    $type  = 'danger';
                    $kind  = 'bs-alert';
                    $title = $Core->i18n()->translate('Fehler') . '!';
                    $Core->setNote($note, $type, $kind, $title);
                }
                $redirectUrl = $Mvc->getModelUrl() . '/api';
                $Core->Request()->redirect($redirectUrl);
            }
            return $return;

        } else {

            return $Core->i18n()->translate('API-Zugang nicht gefunden...');
        }

    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }
}