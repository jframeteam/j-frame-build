<?php
/**
 * Media Model Controller Index
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function indexAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     */
    global $Core, $Mvc;

    $redirectUrl = $Mvc->getModelUrl() . '/dirs';

    $Core->Request()->redirect($redirectUrl);
}

function mainerrorAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');

    if($mediaClass->mediaFolderExists() && $mediaClass->mediaFolderWritable()) {
        $Core->Request()->redirect($Mvc->getModelUrl());
    }

    return $Core->i18n()->translate('Medienverwaltung ist nicht nutzbar bevor die Probleme behhoben sind.');
}