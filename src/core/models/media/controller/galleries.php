<?php
/**
 * Media Model Controller Galleries
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function indexAction()
{
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if (is_object($accClass) && !$accClass->hasAccess('media_galleries')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('media_galleries') . ' - ' . $Core->i18n()->translate('media') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('media') . ' - ' . $Core->i18n()->translate('media_galleries'));

        $site = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $getDirsConfig = array(
            'siteId'       => $siteId,
            'parentId'       => 'all'
        );
        $dirs = $mediaClass->getDirs($getDirsConfig);

        $nestedDirs = $mediaClass->getDirsNested(null,0,false);

        $return = '';

        $ob_return = '';
        if ($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    }
}

function viewAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    /**
     * @var $plugins Plugins
     * @var $pitsForms PitsForms
     * @var $pitsCore PitsCore
     */

    /** Prepare Form Plugin */
    $plugins = $Core->Plugins();
    $pitsForms = $plugins->PitsForms();

    $pitsCore = $plugins->PitsCore();

    $accClass = $Mvc->modelClass('Acc');

    $mediaClass = $Mvc->modelClass('Media');

    $oldMetaTitle = $Mvc->getMetaTitle();
    $Mvc->setMetaTitle($Core->i18n()->translate('Galerie') . ' - ' . $oldMetaTitle);
    $oldPageTitle = $Mvc->getPageTitle();
    $Mvc->setPageTitle($Core->i18n()->translate('Galerie'));
    $return = '';

    $params  = $Mvc->getMvcParams();

    $paramDirFolderName = '';

    if(count($params)) {
        reset($params);
        $first_key = key($params);
        if($first_key == 'dirFolderName') {
            $paramDirFolderName = (array_key_exists('dirFolderName', $params)) ? $params['dirFolderName'] : '';
        } else {
            $paramDirFolderName = $first_key;
        }
    }
    if($paramDirFolderName == '') {
        return $Core->i18n()->translate('Ordner nicht gefunden...');
    }
    $dir = $mediaClass->getDirByFolderName($paramDirFolderName);

    $curUrl = trim($Core->Request()->getCurrUrl(),'/') . '/';
    $curMvcModelActionUrl = $Mvc->getModelUrl($Mvc->getModelKey()) . '/' . $Mvc->getControllerKey() . '/' . $Mvc->getActionKey() . '/';
    $curMvcModelActionSlug = trim(str_replace($curMvcModelActionUrl, '', $curUrl),'/');
    $dirPathFromSlug = str_replace('dirpath/','',$curMvcModelActionSlug);
    $curDirPath = ($dirPathFromSlug === '' || $dirPathFromSlug === 'dirpath' || strpos($dirPathFromSlug, 'http') !== false) ? '/' : '/' . $dirPathFromSlug . '/';
    $curDirPath = str_replace('/', DS, $curDirPath);

    $curDirByDirPath = $mediaClass->getDirByFolderPath($curDirPath);

    $dir = $curDirByDirPath;
    $galleryStyleId = 0;

    if(count($dir)) {

        $galleryStyleId = $dir['id_media_galleries_styles'];

        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($dir['name'] . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($oldPageTitle . ' - ' . $dir['name']);

        $passwordNeeded = $mediaClass->getGalleryIsPasswordNeeded($dir['id']);

        if(!$passwordNeeded) {
            $mediaClass->unlockGallery($dir['id']);
        }

        $unlocked = $mediaClass->galleryUnlocked($dir['id']);

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
//            if(is_object($pitsForms)) {
//                $pitsCore->decryptPOST();
//            }
            $post = $Core->Request()->getPost();

            if(count($post)) {

                if (isset($post['action'])) {

                    if ($post['action'] == 'unlock_gallery' && $unlocked === false) {
                        $password = (array_key_exists('password', $post)) ? $post['password'] : '';

                        if($mediaClass->unlockGallery($dir['id'], $password)) {
                            $note  = $Core->i18n()->translate('Zugriff gestattet!');
                            $type  = 'success';
                            $kind  = 'bs-alert';
                            $title = '';
                            $Core->setNote($note, $type, $kind, $title);
                        } else {
                            $note  = $Core->i18n()->translate('Passwort falsch!');
                            $type  = 'danger';
                            $kind  = 'bs-alert';
                            $title = $Core->i18n()->translate('Fehler') . '!';
                            $Core->setNote($note, $type, $kind, $title);
                        }

                        $galleryUrl = $Mvc->getModelUrl() . '/galleries/view' . str_replace(DS,'/',$dir['dirPath']);
                        $redirectUrl = $galleryUrl;
                        $Core->Request()->redirect($redirectUrl);
                    }

                    if ($post['action'] == 'download') {

                        $downloadFilesArray = (array_key_exists('download_files',$post) && is_array($post['download_files'])) ? $post['download_files'] : array();
                        if(count($downloadFilesArray)) {
                            $downloadFilesArrayClean = array();
                            foreach ($downloadFilesArray as $fileName) {
                                if ($fileName !== 0 && $fileName != '' && $fileName !== null) {
                                    $downloadFilesArrayClean[] = $fileName;
                                }
                            }
                            $post['download_files'] = $downloadFilesArrayClean;
                        }
                        $downloadFiles = $post['download_files'];

                        if(count($downloadFiles)) {

                            $mediaClass->galleryDownload($dir['dirPath'], $downloadFiles);

                            $note = '<pre>$post => ' . print_r($post, true) . '</pre>';

                            $Core->setNote($note);


                        } else {

                            $note  = $Core->i18n()->translate('Bitte zuerst eine Auswahl treffen!');
                            $type  = 'danger';
                            $kind  = 'bs-alert';
                            $title = $Core->i18n()->translate('Fehler') . '!';
                            $Core->setNote($note, $type, $kind, $title);
                        }

                        $galleryUrl = $Mvc->getModelUrl() . '/galleries/view' . str_replace(DS,'/',$dir['dirPath']);
                        $redirectUrl = $galleryUrl;
                        $Core->Request()->redirect($redirectUrl);
                    }
                }
            }
        }
    }

//    $galleryStyle = ($mediaClass->galleryStyleExists($galleryStyleId)) ? $mediaClass->getGalleryStyle($galleryStyleId) : array();
//    $useGalleryStyle = (count($galleryStyle) && $galleryStyle['active'] == 1);
//    if($useGalleryStyle) {
//        $Mvc->setThemeName('minimal_bs');
//
//        $viewHeaderFilePath = JF_ROOT_DIR . DS . 'core' . DS . 'models' . DS . 'media' . DS . 'views' . DS . 'galleries' . DS . 'styles.view.header.php';
//        $ob_return_styles_header = '';
//        try {
//            ob_start();
//            include($viewHeaderFilePath);
//            $ob_return_styles_header = ob_get_contents();
//            ob_end_clean();
//        } catch (Exception $e) {
//            $ob_return_styles_header = $e->getMessage();
//        }
//        $Mvc->addPageAfterBodyStart($ob_return_styles_header);
//    }
//
//    if($Core->Config()->get('gallery_view_force_cache')) {
//        $Mvc->forceCache();
//    }

    $ob_return = '';
    try {
        ob_start();
        include($Mvc->getMVCViewFilePath());
        $ob_return = ob_get_contents();
        ob_end_clean();
    } catch (Exception $e) {
        $ob_return = $e->getMessage();
    }
    $return .= $ob_return;

    return $return;
}

function stylesAction()
{
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if (is_object($accClass) && !$accClass->hasAccess('media_galleries_styles')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('media_galleries_styles') . ' - ' . $Core->i18n()->translate('media') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('media') . ' - ' . $Core->i18n()->translate('media_galleries_styles'));

        $site = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $getDirsConfig = array(
            'siteId'       => $siteId,
            'parentId'       => 'all'
        );
        $dirs = $mediaClass->getDirs($getDirsConfig);

        $return = '';

        $ob_return = '';
        if ($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    }
}

function stylesCreateAction()
{
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if (is_object($accClass) && !$accClass->hasAccess('media_galleries_styles_create')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('media_galleries_styles_create') . ' - ' . $Core->i18n()->translate('media') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('media') . ' - ' . $Core->i18n()->translate('media_galleries_styles_create'));

        $site = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $canEdit    = (!is_object($accClass) || $accClass->hasAccess('media_galleries_styles_edit'));
        $canDelete  = (!is_object($accClass) || $accClass->hasAccess('media_galleries_styles_delete'));
        $canCreate  = (!is_object($accClass) || $accClass->hasAccess('media_galleries_styles_create'));

        $params  = $Mvc->getMvcParams();

        $paramGalleriesStyleId = 0;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id') {
                $paramGalleriesStyleId = (isset($params['id'])) ? (int)$params['id'] : 0;
            } else {
                $paramGalleriesStyleId = (int)$first_key;
            }
        }

        $galleryStyleData = ($paramGalleriesStyleId !== 0 && $mediaClass->galleryStyleExists($paramGalleriesStyleId)) ? $mediaClass->getGalleryStyle($paramGalleriesStyleId) : array();

        $return = '';

        $ob_return = '';
        if ($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    }
}

function stylesEditAction()
{
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if (is_object($accClass) && !$accClass->hasAccess('media_galleries_styles_edit')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('media_galleries_styles_edit') . ' - ' . $Core->i18n()->translate('media') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('media') . ' - ' . $Core->i18n()->translate('media_galleries_styles_edit'));

        $site = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $canEdit    = (!is_object($accClass) || $accClass->hasAccess('media_galleries_styles_edit'));
        $canDelete  = (!is_object($accClass) || $accClass->hasAccess('media_galleries_styles_delete'));
        $canCreate  = (!is_object($accClass) || $accClass->hasAccess('media_galleries_styles_create'));

        $params  = $Mvc->getMvcParams();

        $paramGalleriesStyleId = 0;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id') {
                $paramGalleriesStyleId = (isset($params['id'])) ? (int)$params['id'] : 0;
            } else {
                $paramGalleriesStyleId = (int)$first_key;
            }
        }

        $galleryStyleData = ($paramGalleriesStyleId !== 0 && $mediaClass->galleryStyleExists($paramGalleriesStyleId)) ? $mediaClass->getGalleryStyle($paramGalleriesStyleId) : array();

        $return = '';

        $ob_return = '';
        if ($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    }
}

function stylesSaveAction()
{
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    /**
     * @var $plugins Plugins
     * @var $pitsForms PitsForms
     * @var $pitsCore PitsCore
     */

    /** Prepare Form Plugin */
    $plugins = $Core->Plugins();
    $pitsForms = $plugins->PitsForms();

    $pitsCore = $plugins->PitsCore();

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(!is_object($accClass) || ($accClass->hasAccess('media_galleries_styles_edit') || $accClass->hasAccess('media_galleries_styles_create'))) {

        $return = '';

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if(is_object($pitsForms)) {
                $pitsCore->decryptPOST();
            }
            $post = $Core->Request()->getPost();
            if(count($post)) {
                if (isset($post['action'])) {
                    if ($post['action'] == 'galleryStyle_save') {
                        $saveResult = $mediaClass->saveGalleryStyle($post);
                        $return = '<pre>' . print_r($post, true) . '</pre>';
                        $return .= '<pre>' . print_r($saveResult, true) . '</pre>';

                        if(is_array($saveResult) && count($saveResult) && array_key_exists('success', $saveResult) && array_key_exists('msg', $saveResult)) {
                            $note = $saveResult['msg'];
                            if($saveResult['success']) {
                                $type  = 'success';
                                $kind  = 'bs-alert';
                                $title = '';
                                $Core->setNote($note, $type, $kind, $title);
                            } else {
                                $type  = 'danger';
                                $kind  = 'bs-alert';
                                $title = $Core->i18n()->translate('Fehler') . '!';
                                $Core->setNote($note, $type, $kind, $title);
                            }
                            $redirectUrl = $Mvc->getModelUrl() . '/galleries/styles';
                            $Core->Request()->redirect($redirectUrl);
                        }
                        return $return;
                    }
                }
            }
        }
    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }
}

function stylesActivateAction()
{
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(!is_object($accClass) || $accClass->hasAccess('media_galleries_styles_edit')) {

        $return = '';

        $params  = $Mvc->getMvcParams();

        $paramEditId = 0;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id') {
                $paramEditId = (array_key_exists('id', $params)) ? (int)$params['id'] : 0;
            } else {
                $paramEditId = (int)$first_key;
            }
        }

        $dir = ($mediaClass->galleryStyleExists($paramEditId)) ? $mediaClass->getGalleryStyle($paramEditId) : array();

        if(count($dir)) {
            $saveData = array();
            $saveData['id'] = $dir['id'];
            $saveData['active'] = 1;

            $saveResult = $mediaClass->saveGalleryStyle($saveData);
            $return .= '<pre>' . print_r($saveResult, true) . '</pre>';

            if(is_array($saveResult) && count($saveResult) && array_key_exists('success', $saveResult) && array_key_exists('msg', $saveResult)) {
                if($saveResult['success']) {
                    $note = $Core->i18n()->translate('Galerie Stil erfolgreich deaktiviert!');
                    $type  = 'success';
                    $kind  = 'bs-alert';
                    $title = '';
                    $Core->setNote($note, $type, $kind, $title);
                } else {
                    $note = $Core->i18n()->translate('Galerie Stil konnte nicht deaktiviert werden!');
                    $type  = 'danger';
                    $kind  = 'bs-alert';
                    $title = $Core->i18n()->translate('Fehler') . '!';
                    $Core->setNote($note, $type, $kind, $title);
                }
                $redirectUrl = $Mvc->getModelUrl() . '/galleries/styles';
                $Core->Request()->redirect($redirectUrl);
            }
            return $return;

        } else {

            return $Core->i18n()->translate('Galerie Stil nicht gefunden...');
        }

    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }
}

function stylesDeactivateAction()
{
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(!is_object($accClass) || $accClass->hasAccess('media_galleries_styles_edit')) {

        $return = '';

        $params  = $Mvc->getMvcParams();

        $paramEditId = 0;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id') {
                $paramEditId = (array_key_exists('id', $params)) ? (int)$params['id'] : 0;
            } else {
                $paramEditId = (int)$first_key;
            }
        }

        $dir = ($mediaClass->galleryStyleExists($paramEditId)) ? $mediaClass->getGalleryStyle($paramEditId) : array();

        if(count($dir)) {
            $saveData = array();
            $saveData['id'] = $dir['id'];
            $saveData['active'] = 0;

            $saveResult = $mediaClass->saveGalleryStyle($saveData);
            $return .= '<pre>' . print_r($saveResult, true) . '</pre>';

            if(is_array($saveResult) && count($saveResult) && array_key_exists('success', $saveResult) && array_key_exists('msg', $saveResult)) {
                if($saveResult['success']) {
                    $note = $Core->i18n()->translate('Galerie Stil erfolgreich deaktiviert!');
                    $type  = 'success';
                    $kind  = 'bs-alert';
                    $title = '';
                    $Core->setNote($note, $type, $kind, $title);
                } else {
                    $note = $Core->i18n()->translate('Galerie Stil konnte nicht deaktiviert werden!');
                    $type  = 'danger';
                    $kind  = 'bs-alert';
                    $title = $Core->i18n()->translate('Fehler') . '!';
                    $Core->setNote($note, $type, $kind, $title);
                }
                $redirectUrl = $Mvc->getModelUrl() . '/galleries/styles';
                $Core->Request()->redirect($redirectUrl);
            }
            return $return;

        } else {

            return $Core->i18n()->translate('Galerie Stil nicht gefunden...');
        }

    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }
}

function stylesDeleteAction()
{
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(!is_object($accClass) || $accClass->hasAccess('media_galleries_styles_delete')) {

        $return = '';

        $params  = $Mvc->getMvcParams();

        $paramDeleteId = 0;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id') {
                $paramDeleteId = (array_key_exists('id', $params)) ? (int)$params['id'] : 0;
            } else {
                $paramDeleteId = (int)$first_key;
            }
        }

        $dir = ($mediaClass->galleryStyleExists($paramDeleteId)) ? $mediaClass->getGalleryStyle($paramDeleteId) : array();

        if(count($dir)) {

            $deleteResult = $mediaClass->deleteGalleryStyle($paramDeleteId);
            $return .= '<pre>' . print_r($deleteResult, true) . '</pre>';

            if(is_array($deleteResult) && count($deleteResult) && array_key_exists('success', $deleteResult) && array_key_exists('message', $deleteResult)) {
                $note = $deleteResult['message'];
                if($deleteResult['success']) {
                    $type  = 'success';
                    $kind  = 'bs-alert';
                    $title = '';
                    $Core->setNote($note, $type, $kind, $title);
                } else {
                    $type  = 'danger';
                    $kind  = 'bs-alert';
                    $title = $Core->i18n()->translate('Fehler') . '!';
                    $Core->setNote($note, $type, $kind, $title);
                }
                $redirectUrl = $Mvc->getModelUrl() . '/galleries/styles';
                $Core->Request()->redirect($redirectUrl);
            }
            return $return;

        } else {

            return $Core->i18n()->translate('Galerie Stil nicht gefunden...');
        }

    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }
}