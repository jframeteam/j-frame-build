<?php
/**
 * Media Model Ajax Controller API
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function dtsourceAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('media_api')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $Mvc->setMVCPlainAjax(true);

        $params  = $Mvc->getMvcParams();

        $paramActive = true;

        if(count($params)) {
            if(array_key_exists('inactive', $params) && $params['inactive'] != '0') {
                $paramActive = false;
            }
        }

        $return = array();

        $site = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $curUser = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $isMailer = (count($Mvc->getModel('mailer')));

        $canEdit    = (!is_object($accClass) || $accClass->hasAccess('media_api_edit'));
        $canDelete  = (!is_object($accClass) || $accClass->hasAccess('media_api_delete'));

        $cols = array(
            'id',
            'id_acc_users',
            'api_key',
            'created',
            'action'
        );

        $dtDraw          = (array_key_exists('draw',$_GET))   ? $_GET['draw']   : 0;
        $dtStart         = (array_key_exists('start',$_GET))  ? $_GET['start']  : 0;
        $dtLength        = (array_key_exists('length',$_GET)) ? $_GET['length'] : 10;
        $dtSearch        = (array_key_exists('search',$_GET)) ? $_GET['search'] : array();
        $dtSearchValue   = (count($dtSearch) && array_key_exists('value',$dtSearch) && $dtSearch['value'] != '') ? $dtSearch['value'] : null;
        $order           = (array_key_exists('order',$_GET))  ? $_GET['order']  : array();
        $orderColInt     = (count($order) && array_key_exists(0,$order) && array_key_exists('column',$order[0])) ? $order[0]['column'] : null;
        $orderDir        = (count($order) && array_key_exists(0,$order) && array_key_exists('dir',$order[0])) ? $order[0]['dir'] : 'asc';
        $orderBy         = (array_key_exists($orderColInt,$cols))  ? $cols[$orderColInt] : null;
        $recordsTotal    = $mediaClass->getApiUsersCount($siteId, $paramActive);
        $recordsFiltered = $recordsTotal;

        $start = $dtStart;
        $limit = $dtLength;

        if($dtSearchValue !== null) {
            $dtSearchValue = trim(str_replace(array('"','$','[',']'),'',$dtSearchValue));
            $searchArray   = array( '\\', '+', '*', '?', '^', '(', ')', '{', '}', '=', '!', '<', '>', '|', ':', '-');
            $replaceArray  = array( '[\\\\]', '[+]', '[*]', '[?]', '[\\\\^]', '[(]', '[)]', '[{]', '[}]', '[=]', '[!]', '[<]', '[>]', '[|]', '[:]', '[-]');
            $dtSearchValue = trim(str_replace($searchArray,$replaceArray,$dtSearchValue));

            if($dtSearchValue == '') {
                $dtSearchValue = null;
            }
        }

        $data = array();
        $apiUsers = $mediaClass->getApiUsers($siteId, $start, $limit, $orderBy, $orderDir, $dtSearchValue, $paramActive);
        foreach($apiUsers as $apiUserId => $apiUser) {

            if(!array_key_exists('created', $apiUser)) { continue; }

            $created = date('d.m.Y', $apiUser['created']);

            $folderIcon = '<i class="fa fa-key" aria-hidden="true" data-toggle="tooltip" title="ID=' . $apiUser['id'] . '"></i>';

            $user = (is_object($accClass)) ? $accClass->getUser($apiUser['id_acc_users']) : array();
            $userName = (count($user)) ? $user['name'] . ' ' . $user['surname'] : 'System';

            $editBtn = ($canEdit) ? '<a href="' . $Mvc->getModelAjaxUrl() . '/api/edit/' . $apiUser['id'] . '" class="btn btn-default" data-toggle="modal" data-target="#createEditApiModal"><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" title="' . $Core->i18n()->translate('bearbeiten') . '"></i></a>' : '';

            $entryDelBtn = ($canDelete) ? '<a href="' . $Mvc->getModelUrl() . '/api/delete/' . $apiUser['id'] . '" class="btn btn-danger" data-action="api_delete" data-toggle="tooltip" title="' . sprintf( $Core->i18n()->translate('API-Zugang für User \'%s\' löschen') , $userName) . '"><i class="fa fa-trash" aria-hidden="true"></i></a>': '';
            $entryActBtn = ($canEdit && !$apiUser['active']) ? '<a href="' . $Mvc->getModelUrl() . '/api/activate/' . $apiUser['id'] . '" class="btn btn-default change-icon" data-action="api_activate" data-toggle="tooltip" title="' . sprintf( $Core->i18n()->translate('API-Zugang für User \'%s\' freischalten') , $userName) . '"><i class="fa fa-lock text-danger" aria-hidden="true"></i><i class="fa fa-unlock text-success" aria-hidden="true"></i></a>': '';
            $entryDeActBtn = ($canEdit && $entryActBtn == '') ? '<a href="' . $Mvc->getModelUrl() . '/api/deactivate/' . $apiUser['id'] . '" class="btn btn-default change-icon" data-action="api_deactivate" data-toggle="tooltip" title="' . sprintf( $Core->i18n()->translate('API-Zugang für User \'%s\' sperren') , $userName) . '"></i><i class="fa fa-unlock text-success" aria-hidden="true"></i><i class="fa fa-lock text-danger" aria-hidden="true"></a>': '';

            $btnGroup = '<div class="btn-group btn-group-sm">';
            $btnGroup .= ($paramActive) ? $editBtn : '';
            $btnGroup .= $entryActBtn;
            $btnGroup .= $entryDeActBtn;
            $btnGroup .= (!$paramActive) ? $entryDelBtn : '';
            $btnGroup .= '</div>';

            $btnPopover = '<button type="button" class="btn btn-default btn-xs visible-xs-inline visible-sm-inline" data-container="body" data-toggle="popover" data-placement="left" data-content="' . htmlentities($btnGroup) . '"><i class="fa fa-cog" aria-hidden="true" data-toggle="tooltip" title="' . sprintf( $Core->i18n()->translate('API-Zugang für User \'%s\' konfigurieren') , $userName) . '"></i></button>';

            $tableData = array();

            $tableData[] = $folderIcon;
            $tableData[] = $userName;
            if($paramActive) {
                $tableData[] = $apiUser['api_key'];
            }
            $tableData[] = $created;
            $tableData[] = $btnPopover . '<div class="hidden-xs hidden-sm">' . $btnGroup . '</div>';

            $data[] = $tableData;
        }

        if($dtSearchValue !== null) {
            $recordsFiltered = count($apiUsers);
        }

        $return['draw']            = $dtDraw;
        $return['searchValue']     = $dtSearchValue;
        $return['recordsTotal']    = $recordsTotal;
        $return['recordsFiltered'] = $recordsFiltered;
        $return['data']            = $data;

        return $return;
    }
}

function createAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('media_api_create')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $Mvc->setMVCPlainAjax(true);

        $result = '';

        $ob_return = '';
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
        $result .= $ob_return;

        $return = array(
            'success' => true,
            'result' => $result
        );

        return $return;
    }
}

function editAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('media_api_edit')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $Mvc->setMVCPlainAjax(true);

        $result = '';

        $params  = $Mvc->getMvcParams();

        $paramEditId = 0;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id') {
                $paramEditId = (array_key_exists('id', $params)) ? (int)$params['id'] : 0;
            } else {
                $paramEditId = (int)$first_key;
            }
        }

        $apiUserEntry = ($mediaClass->apiUserExists($paramEditId)) ? $mediaClass->getApiUser($paramEditId) : array();

        $ob_return = '';
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
        $result .= $ob_return;

        $return = array(
            'success' => true,
            'result' => $result
        );

        return $return;
    }
}

function getapikeybyuseridAction() {

    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('media_api_edit')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $Mvc->setMVCPlainAjax(true);

        $return = array(
            'success' => false,
            'result' => ''
        );

        $params  = $Mvc->getMvcParams();

        $paramUserId = 0;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id') {
                $paramUserId = (array_key_exists('id', $params)) ? (int)$params['id'] : 0;
            } else {
                $paramUserId = (int)$first_key;
            }
        }

        $apiKey = '';

        $user = (is_object($accClass) && $accClass->userExists($paramUserId)) ? $accClass->getUser($paramUserId) : array();

        if(count($user)) {
            $apiKey = $mediaClass->createApiKey($paramUserId);

            $return = array(
                'success' => true,
                'result' => $apiKey
            );
        }

        return $return;
    }
}

/**
 * API Section
 */

/** API Dirs Action */
function dirsAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');

    $apiAuth = $mediaClass->apiAuthenticate();
    if(is_array($apiAuth) && count($apiAuth)) {
        $apiAuthSuccess = (array_key_exists('success', $apiAuth) && $apiAuth['success']);
        $apiAuthMessage = (array_key_exists('message', $apiAuth)) ? $apiAuth['message'] : $Core->i18n()->translate('Api Authentifizierung fehlerhaft...');
        if ($apiAuthSuccess) {
            $userId = (array_key_exists('userId', $apiAuth)) ? $apiAuth['userId'] : 0;
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                return apiPost($userId);
            } else {
                return apiGet($userId);
            }
        } else {
            $return = array(
                'success' => false,
                'message' => $apiAuthMessage
            );
            return $return;
        }
    } else {
        $return = array(
            'success' => false,
            'message' => $Core->i18n()->translate('Api Authentifizierung nicht möglich...')
        );
        return $return;
    }
}

/** API Post Function */
function apiPost($userId) {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    $user = (is_object($accClass)) ? $accClass->getUser($userId) : array();

    $site   = $Core->Sites()->getSite();
    $siteId = (count($site)) ? $site['id'] : 0;

    $post = $Core->Request()->getPost();

    $apiArea = (array_key_exists('area', $post)) ? $post['area'] : '';
    $apiAction = (array_key_exists('action', $post)) ? $post['action'] : '';

    $return = array();

    if($apiArea === 'dir') {
        $dirId = (array_key_exists('id', $post)) ? $post['id'] : 0;
        if($dirId !== 0 && $mediaClass->dirExists($dirId)) {
            $dir = $mediaClass->getDir($dirId);

            switch ($apiAction) {
                case 'connect2event':
                    $eventId = (array_key_exists('eventId', $post)) ? $post['eventId'] : 0;
                    $return['success'] = true;
                    $return['message'] = $Core->i18n()->translate('Ordner mit Event verknüpfen');
                    if(!$mediaClass->eventExists($eventId)) {
                        $return['success'] = false;
                        $return['result']  = $Core->i18n()->translate('Keine Event Id übergeben');
                    } else {
                        if($mediaClass->setDirs2Events($dirId, $eventId)) {
                            $return['result'] = $Core->i18n()->translate('verknüpfung erfolgreich');
                        }
                    }
                    break;
                default:
                    $return['success'] = false;
                    $return['message'] = $Core->i18n()->translate('Keine API Action übergeben');
            }

        } else {
            $return['success'] = false;
            $return['message'] = sprintf($Core->i18n()->translate('Kein Ordner mit der ID \'%s\' gefunden.'), $dirId);
        }
    }

//    $return['debug'] = array(
//        'post' => $post,
//        'user' => $user
//    );

    return $return;
}

/** API Get Function */
function apiGet($userId) {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    $user = (is_object($accClass)) ? $accClass->getUser($userId) : array();

    $site = $Core->Sites()->getSite();
    $siteId = (count($site)) ? $site['id'] : 0;

    $getDirsConfig = array(
        'siteId' => $siteId
    );
    $dirs = $mediaClass->getDirs($getDirsConfig);

    $apiArea = (array_key_exists('area', $_GET)) ? $_GET['area'] : '';
    $apiAction = (array_key_exists('action', $_GET)) ? $_GET['action'] : '';

    $return = array();

    if($apiArea === 'dirs') {
        switch ($apiAction) {
            case 'list':
                $return['success'] = true;
                $return['message'] = $Core->i18n()->translate('Auflistung aller Ordner');
                $return['result']  = $dirs;
                break;
            default:
                $return['success'] = false;
                $return['message'] = $Core->i18n()->translate('Keine API Action übergeben');
        }
    }

    if($apiArea === 'dir') {
        $dirId = (array_key_exists('id', $_GET)) ? $_GET['id'] : 0;
        if($dirId !== 0 && $mediaClass->dirExists($dirId)) {
            $dir = $mediaClass->getDir($dirId);

            switch ($apiAction) {
                case 'list':
                    $return['success'] = true;
                    $return['message'] = $Core->i18n()->translate('Auflistung des Ordners');
                    $return['result']  = $dir;
                    break;
                case 'contents':
                    $return['success'] = true;
                    $return['message'] = $Core->i18n()->translate('Auflistung vom Inhalt des Ordners');
                    $return['result']  = $mediaClass->getDirContents($dir['dirPath']);
                    break;
                default:
                    $return['success'] = false;
                    $return['message'] = $Core->i18n()->translate('Keine API Action übergeben');
            }
        } else {
            $return['success'] = false;
            $return['message'] = sprintf($Core->i18n()->translate('Kein Ordner mit der ID \'%s\' gefunden.'), $dirId);
        }
    }

//    $return['debug'] = array(
//        '$_GET' => $_GET,
//        'user' => $user
//    );

    return $return;
}