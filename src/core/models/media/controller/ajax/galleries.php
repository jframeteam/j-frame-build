<?php
/**
 * Media Model Ajax Controller Galleries
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */
function stylesDtsourceAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('media_galleries_styles')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $Mvc->setMVCPlainAjax(true);

        $params  = $Mvc->getMvcParams();

        $paramActive = true;

        if(count($params)) {
            if(array_key_exists('inactive', $params) || in_array('inactive',$params)) {
                $paramActive = false;
            }
        }

        $return = array();

        $site = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $curUser = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $canEdit    = (!is_object($accClass) || $accClass->hasAccess('media_galleries_styles_edit'));
        $canDelete  = (!is_object($accClass) || $accClass->hasAccess('media_galleries_styles_delete'));
        $canCreate  = (!is_object($accClass) || $accClass->hasAccess('media_galleries_styles_create'));

        $cols = array(
            'id',
            'title',
            'logo',
            'colors',
            'action'
        );

        $dtDraw          = (array_key_exists('draw',$_GET))   ? $_GET['draw']   : 0;
        $dtStart         = (array_key_exists('start',$_GET))  ? $_GET['start']  : 0;
        $dtLength        = (array_key_exists('length',$_GET)) ? $_GET['length'] : 10;
        $dtSearch        = (array_key_exists('search',$_GET)) ? $_GET['search'] : array();
        $dtSearchValue   = (count($dtSearch) && array_key_exists('value',$dtSearch) && $dtSearch['value'] != '') ? $dtSearch['value'] : null;
        $order           = (array_key_exists('order',$_GET))  ? $_GET['order']  : array();
        $orderColInt     = (count($order) && array_key_exists(0,$order) && array_key_exists('column',$order[0])) ? $order[0]['column'] : null;
        $orderDir        = (count($order) && array_key_exists(0,$order) && array_key_exists('dir',$order[0])) ? $order[0]['dir'] : 'asc';
        $orderBy         = (array_key_exists($orderColInt,$cols))  ? $cols[$orderColInt] : null;
        $recordsTotal    = $mediaClass->getGalleryStylesCount($siteId, $paramActive);
        $recordsFiltered = $recordsTotal;

        $start = $dtStart;
        $limit = $dtLength;

        if($dtSearchValue !== null) {
            $dtSearchValue = trim(str_replace(array('"','$','[',']'),'',$dtSearchValue));
            $searchArray   = array( '\\', '+', '*', '?', '^', '(', ')', '{', '}', '=', '!', '<', '>', '|', ':', '-');
            $replaceArray  = array( '[\\\\]', '[+]', '[*]', '[?]', '[\\\\^]', '[(]', '[)]', '[{]', '[}]', '[=]', '[!]', '[<]', '[>]', '[|]', '[:]', '[-]');
            $dtSearchValue = trim(str_replace($searchArray,$replaceArray,$dtSearchValue));

            if($dtSearchValue == '') {
                $dtSearchValue = null;
            }
        }

        $data = array();

        $curMvcModelActionUrl = $Mvc->getModelUrl($Mvc->getModelKey()) . '/' . $Mvc->getControllerKey() . '/' . $Mvc->getControllerKey();

        $galleryStyles = $mediaClass->getGalleryStyles($siteId, $start, $limit, $orderBy, $orderDir, $dtSearchValue, $paramActive);
        foreach($galleryStyles as $galleryStyleId => $galleryStyle) {

            if(!array_key_exists('created', $galleryStyle)) { continue; }

            $created = date('d.m.Y', $galleryStyle['created']);

            $icon = '<i class="fa fa-paint-brush" aria-hidden="true" data-toggle="tooltip" title="ID=' . $galleryStyleId . '"></i>';

            $title = $galleryStyle['title'];

            $logo = ($galleryStyle['logo'] !== '') ? '<i class="fa fa-file-image-o" aria-hidden="true" data-toggle="tooltip" title="' . $galleryStyle['logo'] . '"></i>' : '--';

            $colorsArray = array();

            $colorsArray['color_header_bg']     = (array_key_exists('color_header_bg',     $galleryStyle)) ? $galleryStyle['color_header_bg']     : '#ffffff';
            $colorsArray['color_header_txt']    = (array_key_exists('color_header_txt',    $galleryStyle)) ? $galleryStyle['color_header_txt']    : '#000000';
            $colorsArray['color_page_bg']       = (array_key_exists('color_page_bg',       $galleryStyle)) ? $galleryStyle['color_page_bg']       : '#e9e9e9';
            $colorsArray['color_content_bg']    = (array_key_exists('color_content_bg',    $galleryStyle)) ? $galleryStyle['color_content_bg']    : '#ffffff';
            $colorsArray['color_content_txt']   = (array_key_exists('color_content_txt',   $galleryStyle)) ? $galleryStyle['color_content_txt']   : '#000000';
            $colorsArray['color_thumbnail_bg']  = (array_key_exists('color_thumbnail_bg',  $galleryStyle)) ? $galleryStyle['color_thumbnail_bg']  : '#ffffff';
            $colorsArray['color_thumbnail_txt'] = (array_key_exists('color_thumbnail_txt', $galleryStyle)) ? $galleryStyle['color_thumbnail_txt'] : '#000000';

            $colors = '';

            foreach($colorsArray as $colorKey => $color) {
                $colorSquare = '<i class="fa fa-square fa-border" data-toggle="tooltip" title="' . $colorKey . ': ' . $color . '" style="color: ' . $color . '"></i>' . PHP_EOL;
                $colors .= $colorSquare;
            }

            $user = (is_object($accClass) && $galleryStyle['id_acc_users'] > 0) ? $accClass->getUser($galleryStyle['id_acc_users']) : array();
            $userName = (count($user)) ? $user['name'] . ' ' . $user['surname'] : 'System';

            $editBtn = ($canEdit) ? '<a href="' . $Mvc->getModelUrl() . '/galleries/stylesEdit/' . $galleryStyleId . '" class="btn btn-default"><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" title="' . $Core->i18n()->translate('bearbeiten') . '"></i></a>' : '';
            $createFromThisBtn = ($canCreate) ? '<a href="' . $Mvc->getModelUrl() . '/galleries/stylesCreate/' . $galleryStyleId . '" class="btn btn-success" data-toggle="tooltip" title="' . sprintf( $Core->i18n()->translate('Anhand von \'%s\' einen neuen Stil erstellen') , $title) . '"><i class="fa fa-plus" aria-hidden="true"></i> <i class="fa fa-paint-brush" aria-hidden="true"></i></a>' : '';

            $entryDelBtn = ($canDelete) ? '<a href="' . $Mvc->getModelUrl() . '/galleries/stylesDelete/' . $galleryStyleId . '" class="btn btn-danger" data-action="style_delete" data-toggle="tooltip" title="' . sprintf( $Core->i18n()->translate('Galerie Stil \'%s\' löschen') , $title) . '"><i class="fa fa-trash" aria-hidden="true"></i></a>': '';
            $entryActBtn = ($canEdit && !$galleryStyle['active']) ? '<a href="' . $Mvc->getModelUrl() . '/galleries/stylesActivate/' . $galleryStyleId . '" class="btn btn-default" data-action="dir_activate" data-toggle="tooltip" title="' . sprintf( $Core->i18n()->translate('Galerie Stil \'%s\' aktivieren') , $title) . '"><i class="fa fa-unlock text-success" aria-hidden="true"></i></a>': '';
            $entryDeActBtn = ($canEdit && $entryActBtn == '') ? '<a href="' . $Mvc->getModelUrl() . '/galleries/stylesDeactivate/' . $galleryStyleId . '" class="btn btn-default" data-action="dir_deactivate" data-toggle="tooltip" title="' . sprintf( $Core->i18n()->translate('Galerie Stil \'%s\' deaktivieren') , $title) . '"><i class="fa fa-lock text-danger" aria-hidden="true"></i></a>': '';

            $btnGroup = '<div class="btn-group btn-group-sm">';
            $btnGroup .= ($paramActive)  ? $editBtn : '';
            $btnGroup .= (!$paramActive) ? $entryActBtn : '';
            $btnGroup .= ($paramActive)  ? $entryDeActBtn : '';
            $btnGroup .= ($paramActive) ? $createFromThisBtn : '';
            $btnGroup .= (!$paramActive) ? $entryDelBtn : '';
            $btnGroup .= '</div>';

            $btnPopover = '<button type="button" class="btn btn-default btn-xs visible-xs-inline visible-sm-inline" data-container="body" data-toggle="popover" data-placement="left" data-content="' . htmlentities($btnGroup) . '"><i class="fa fa-cog" aria-hidden="true" data-toggle="tooltip" title="' . sprintf( $Core->i18n()->translate('Galerie Stil \'%s\' konfigurieren') , $title) . '"></i></button>';

            $tableData = array();

            $tableData[] = $icon;
            $tableData[] = $title;
            $tableData[] = $logo;
            $tableData[] = $colors;
            $tableData[] = $btnPopover . '<div class="hidden-xs hidden-sm">' . $btnGroup . '</div>';

            $data[] = $tableData;
        }

        if($dtSearchValue !== null) {
            $recordsFiltered = count($galleryStyles);
        }

        $return['draw']            = $dtDraw;
        $return['searchValue']     = $dtSearchValue;
        $return['recordsTotal']    = $recordsTotal;
        $return['recordsFiltered'] = $recordsFiltered;
        $return['data']            = $data;

        return $return;
    }
}