<?php
/**
 * Media Model Ajax Controller Dirs
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function dtsourceAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $mailerClass Mailer
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $mailerClass = $Mvc->modelClass('Mailer');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('media_dirs')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $Mvc->setMVCPlainAjax(true);

        $params  = $Mvc->getMvcParams();

        $paramArchived = false;
        $parentId = 0;

        if(count($params)) {
            if(array_key_exists('id_media_dirs__parent', $params) && (int)$params['id_media_dirs__parent'] > 0) {
                $parentId = (int)$params['id_media_dirs__parent'];
            }
            if(array_key_exists('archived', $params) && $params['archived'] != '0') {
                $paramArchived = true;
            }
        }

        $return = array();

        $site = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $curUser = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $countGalleryStyles = $mediaClass->getGalleryStylesCount($siteId);

        $isMailer = (count($Mvc->getModel('mailer')));

        $canEdit       = (!is_object($accClass) || $accClass->hasAccess('media_dirs_edit'));
        $canDelete     = (!is_object($accClass) || $accClass->hasAccess('media_dirs_delete'));
        $canAutoDelete = (!is_object($accClass) || $accClass->hasAccess('media_dirs_autodelete'));
        $canArchive    = (!is_object($accClass) || $accClass->hasAccess('media_dirs_archive'));
        $canSeeGallery = (!is_object($accClass) || $accClass->hasAccess('media_galleries'));

        $cols = array(
            'id',
            'name',
            'password',
            'created',
            'id_acc_users',
            'active',
            'action'
        );

        $dtDraw          = (array_key_exists('draw',$_GET))   ? $_GET['draw']   : 0;
        $dtStart         = (array_key_exists('start',$_GET))  ? $_GET['start']  : 0;
        $dtLength        = (array_key_exists('length',$_GET)) ? $_GET['length'] : 10;
        $dtSearch        = (array_key_exists('search',$_GET)) ? $_GET['search'] : array();
        $dtSearchValue   = (count($dtSearch) && array_key_exists('value',$dtSearch) && $dtSearch['value'] != '') ? $dtSearch['value'] : null;
        $order           = (array_key_exists('order',$_GET))  ? $_GET['order']  : array();
        $orderColInt     = (count($order) && array_key_exists(0,$order) && array_key_exists('column',$order[0])) ? $order[0]['column'] : null;
        $orderDir        = (count($order) && array_key_exists(0,$order) && array_key_exists('dir',$order[0])) ? $order[0]['dir'] : 'asc';
        $orderBy         = (array_key_exists($orderColInt,$cols))  ? $cols[$orderColInt] : null;
        $useParentId     = ($paramArchived) ? 'all' : $parentId;

        $getDirsCountConfig = array(
            'siteId'               => $siteId,
            'parentId'               => $useParentId,
            'archived'               => $paramArchived,
            'onlyFirstArchiveParent' => $paramArchived
        );

        $recordsTotal    = $mediaClass->getDirsCount($getDirsCountConfig);
        $recordsFiltered = $recordsTotal;

        $start = $dtStart;
        $limit = $dtLength;

        if($dtSearchValue !== null) {
            $dtSearchValue = trim(str_replace(array('"','$','[',']'),'',$dtSearchValue));
            $searchArray   = array( '\\', '+', '*', '?', '^', '(', ')', '{', '}', '=', '!', '<', '>', '|', ':', '-');
            $replaceArray  = array( '[\\\\]', '[+]', '[*]', '[?]', '[\\\\^]', '[(]', '[)]', '[{]', '[}]', '[=]', '[!]', '[<]', '[>]', '[|]', '[:]', '[-]');
            $dtSearchValue = trim(str_replace($searchArray,$replaceArray,$dtSearchValue));

            if($dtSearchValue == '') {
                $dtSearchValue = null;
            }
        }

        $data = array();

        $curMvcModelActionUrl = $Mvc->getModelUrl($Mvc->getModelKey()) . '/' . $Mvc->getControllerKey();

        if($parentId > 0) {
            $parentRow = array();

            $parentRow[] = '<i class="fa fa-folder-o" aria-hidden="true"></i>';
            $parentRow[] = '<a href="' . $curMvcModelActionUrl . '" target="_self" data-toggle="tooltip" title="' . $Core->i18n()->translate('zum übergeordneten Verzeichnis wechseln') . '"><i class="fa fa-arrow-up" aria-hidden="true"></i> ..</a>';
            if (!$paramArchived) {
                $parentRow[] = '';
            }
            $parentRow[] = '';
            $parentRow[] = '';
            if($countGalleryStyles && !$paramArchived) {
                $tableData[] = '';
            }
            $parentRow[] = '';
            $parentRow[] = '';

            if ($mediaClass->dirExists($parentId) && !$paramArchived) {

                $parentDir = $mediaClass->getDir($parentId);

                $parentParentId = $parentDir['id_media_dirs__parent'];

                if($parentParentId > 0) {

                    if ($mediaClass->dirExists($parentParentId)) {

                        $parentParentDir = $mediaClass->getDir($parentParentId);

                        $parentParentDirPath = $parentParentDir['dirPath'];

                        $parentRow = array();

                        $parentRow[] = '<i class="fa fa-folder-o" aria-hidden="true"></i>';
                        $parentRow[] = '<a href="' . $curMvcModelActionUrl . '/list' . $parentParentDirPath . '" target="_self" data-toggle="tooltip" title="' . $Core->i18n()->translate('zum übergeordneten Verzeichnis wechseln') . '"><i class="fa fa-arrow-up" aria-hidden="true"></i> ..</a>';
                        if (!$paramArchived) {
                            $parentRow[] = '';
                        }
                        $parentRow[] = '';
                        $parentRow[] = '';
                        $parentRow[] = '';
                        if($countGalleryStyles && !$paramArchived) {
                            $tableData[] = '';
                        }
                        $parentRow[] = '';
                        $parentRow[] = '';
                    }
                }
            }

            $data[] = $parentRow;
        }
        $getDirsConfig = array(
            'siteId'               => $siteId,
            'parentId'               => $useParentId,
            'start'                  => $start,
            'limit'                  => $limit,
            'orderBy'                => $orderBy,
            'orderDirection'         => $orderDir,
            'search'                 => $dtSearchValue,
            'archived'               => $paramArchived,
            'onlyFirstArchiveParent' => $paramArchived
        );
        $dirs = $mediaClass->getDirs($getDirsConfig);
        foreach($dirs as $dirId => $dir) {

            if(!array_key_exists('created', $dir)) { continue; }

            $dirFolderExists = $dir['dirFolderExists'];

            $created = date('d.m.Y', $dir['created']);

            $folderIcon = '<i class="fa fa-folder" aria-hidden="true" data-toggle="tooltip" title="ID=' . $dir['id'] . '"></i>';

            $curDirPath = $dir['dirPath'];

            $subFolder = $mediaClass->getDirSubFolderByDirId($dir['id']);
            $countSubFolder = 0;
            $subFolderNamesArray = array();
            $subFolderList = '';
            $aysChangeStateAttributes = '';
            $nestedSubDirs = $mediaClass->getDirsNested(null, $dir['id'], false);
            if(count($subFolder) > 0 && $dirFolderExists) {
                foreach($subFolder as $subDir) {
                    $subDirFolderExists = $subDir['dirFolderExists'];
                    $subDirFolder_archiveState = $subDir['archive_state'];
                    if($subDirFolderExists && $subDirFolder_archiveState < 1 ) {
                        $subFolderNamesArray[] = $subDir['name'];
                    }

                    sort($subFolderNamesArray);
                }

                if(count($subFolderNamesArray)) {
                    $subFolderList .= '<ul class="text-left">';
                    foreach($subFolderNamesArray as $subFolderName) {
                        $subFolderList .= '<li>' . $subFolderName . '</li>';
                    }
                    $subFolderList .= '</ul>';

                    $countSubFolder = count($subFolderNamesArray);
                }

                $subDirsStates = $mediaClass->getDirInfoNestedOneLevel($nestedSubDirs, 'active');

                $hasSubFolderWithSameState = false;
                foreach($subDirsStates as $subDirsStates_dirId => $dirActive) {
                    $subDirsStates_dir = $mediaClass->getDir($subDirsStates_dirId);
                    $subDirsStates_archiveState = $subDirsStates_dir['archive_state'];
                    if($dirActive == $dir['active'] && $subDirsStates_archiveState < 1 ) {
                        $hasSubFolderWithSameState = true;
                        break;
                    }
                }
                $aysChangeStateAttributesText = ($dir['active']) ? $Core->i18n()->translate('Sollen auch alle Unterordner deaktiviert werden?') : $Core->i18n()->translate('Sollen auch alle Unterordner aktiviert werden?');
                $aysChangeStateAttributesDataConfirmCallback = ($dir['active']) ? ' data-confirmcallback="deActivateDirWithAllChildren_ayscallback"' : ' data-confirmcallback="activateDirWithAllChildren_ayscallback"';
                $aysChangeStateAttributesDataDeclineCallback = ($dir['active']) ? ' data-declinecallback="deActivateDirWithoutAllChildren_ayscallback"' : ' data-declinecallback="activateDirWithoutAllChildren_ayscallback"';
                $aysChangeStateAttributes = ($hasSubFolderWithSameState) ? ' data-ays="state"' . $aysChangeStateAttributesDataConfirmCallback . $aysChangeStateAttributesDataDeclineCallback . ' data-aystext="' . htmlentities('<div class="alert alert-warning" role="alert">' . $aysChangeStateAttributesText . '</div>') . '"' : '';
            }

            $subFolderBadge = ($countSubFolder > 0 && $dirFolderExists) ? ' <span class="badge" data-toggle="tooltip" title="' . $Core->i18n()->translate('Unterverzeichnisse vorhanden') . htmlentities($subFolderList) . '">' . $countSubFolder . '</span>' : '';

            $countSubSubFolder = $mediaClass->getDirSubSubFolderCountByDirId($dir['id']);
            $hasSubSubFolderBadge = ($countSubSubFolder) ? ' <span class="badge" data-toggle="tooltip" title="' . $Core->i18n()->translate('Unterverzeichnisse in den Unterverzeichnissen vorhanden') . '"><strong>+</strong></span>' : '';

            $folderName = $dir['name'];
            $folderNameOutput = ($dirFolderExists) ? '<a href="' . $curMvcModelActionUrl . '/list' . $curDirPath . '" target="_self" data-toggle="tooltip" title="' . $Core->i18n()->translate('In das Verzeichnis wechseln') . '">' . $folderName . '</a>' . $subFolderBadge . $hasSubSubFolderBadge : $folderName;

            if($paramArchived) {
                $subFolderNestedList = $mediaClass->getDirTreeAsLinkedList($nestedSubDirs, 0, $dir['dirPath'], false, true);
                $subFolderBadgeTooltipContent = htmlentities('<div class="folderTree text-left"><ul><li>' . $folderName . $subFolderNestedList . '</li></ul></div>');
                $subFolderBadge = (count($subFolder) > 0) ? ' <span class="badge" data-toggle="tooltip" title="' . $Core->i18n()->translate('Unterverzeichnisse vorhanden') . $subFolderBadgeTooltipContent . '"><i class="fa fa-sitemap" aria-hidden="true"></i></span>' : '';

                $folderNameOutput = $folderName . $subFolderBadge;
            }

            $dirPass = $dir['password'];
            $dirPassOutput = ($dir['public']) ? '<s data-toggle="tooltip" title="' . $Core->i18n()->translate('Ordner ist öffentlich.') . '">' . $dirPass . '</s>' : $dirPass;

            $user = (is_object($accClass) && $dir['id_acc_users'] > 0) ? $accClass->getUser($dir['id_acc_users']) : array();
            $userName = (count($user)) ? $user['name'] . ' ' . $user['surname'] : 'System';

            $activeIcon = ($dir['active']) ? '<i class="fa fa-check-circle text-success" aria-hidden="true" data-toggle="tooltip" title="' . $Core->i18n()->translate('Ja') . '"></i>' : '<i class="fa fa-times-circle text-danger" aria-hidden="true" data-toggle="tooltip" title="' . $Core->i18n()->translate('Nein') . '"></i>';

            $backupIcon = '<span class="fa-stack"><i class="fa fa-square fa-stack-2x text-warning"></i><i class="fa fa-hdd-o fa-stack-1x fa-inverse" aria-hidden="true" data-toggle="tooltip" title="' . $Core->i18n()->translate('Vorgemerkt') . '"></i></span>';
            if($dir['backup']) {
                $backupIcon = '<span class="fa-stack"><i class="fa fa-square fa-stack-2x text-success"></i><i class="fa fa-hdd-o fa-stack-1x fa-inverse" aria-hidden="true" data-toggle="tooltip" title="' . $Core->i18n()->translate('Ja') . '"></i></span>';
            }
            if($dir['backup_failed']) {
                $backupIcon = '<span class="fa-stack"><i class="fa fa-square fa-stack-2x text-danger"></i><i class="fa fa-hdd-o fa-stack-1x fa-inverse" aria-hidden="true" data-toggle="tooltip" title="' . $Core->i18n()->translate('Fehlerhaft') . '"></i></span>';
            }

            $galleryStyleId   = (array_key_exists('id_media_galleries_styles',$dir) && (int)$dir['id_media_galleries_styles'] > 0) ? $dir['id_media_galleries_styles'] : 0;
            $galleryStyle     = ($countGalleryStyles && $mediaClass->galleryStyleExists($galleryStyleId)) ? $mediaClass->getGalleryStyle($galleryStyleId) : array();
            $galleryStyleName = (count($galleryStyle)) ? $galleryStyle['title'] : '';

            $galleryStyleColContent = ($galleryStyleName !== '') ? '<i class="fa fa-paint-brush" aria-hidden="true" title="' . $galleryStyleName . '" data-toggle="tooltip"></i>' : '--';

            $activeBackupIcon = (!$paramArchived) ? $activeIcon : $backupIcon;

            /** Gallery View Action Btn */
            $galleryUrl = $Mvc->getModelUrl() . '/galleries/view' . str_replace(DS,'/',$dir['dirPath']);
            $galleryViewBtn = '<a href="' . $galleryUrl . '" class="btn btn-default" target="_blank" data-toggle="tooltip" title="' . sprintf($Core->i18n()->translate('Galerie \'%s\' öffnen'), $dir['name']) . '"><i class="fa fa-eye" aria-hidden="true"></i></a>';

            /** Media Browser Action Btn */
            $mediaBrowserBtn = ($canEdit) ? '<a href="' . $Mvc->getModelAjaxUrl() . '/dirs/mediabrowser/' . $dir['id'] . '" class="btn btn-default" data-toggle="modal" data-target="#listDirsModal" data-dirfoldername="' . $dir['dirPath'] . '"><i class="fa fa-th" aria-hidden="true" data-toggle="tooltip" title="' .sprintf($Core->i18n()->translate('Inhalt des Ordners \'%s\' bearbeiten'), $dir['name']) . '"></i></a>' : '';

            /** Info Action Btn */
            $infoBtn = '<a href="' . $Mvc->getModelAjaxUrl() . '/dirs/info/' . $dir['id'] . '" class="btn btn-default" data-toggle="modal" data-target="#listDirsModal"><i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="' . sprintf( $Core->i18n()->translate('Informationen zum Ordner \'%s\'') , $dir['name']) . '"></i>';

            /** Edit Action Btn */
            $editBtn = ($canEdit) ? '<a href="' . $Mvc->getModelAjaxUrl() . '/dirs/edit/' . $dir['id'] . '" class="btn btn-default" data-toggle="modal" data-target="#listDirsModal"><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" title="' . $Core->i18n()->translate('bearbeiten') . '"></i></a>' : '';

            /** Mail Action Btn */
//            $mailBtn = ($isMailer) ? '<a href="' . $Mvc->getModelUrl('mailer') . '/mail/write/tpl/dir_link/dirId/'.$dir['id'] . '" class="btn btn-default" data-toggle="tooltip" title="' . sprintf($Core->i18n()->translate('Link zur Galerie \'%s\' senden'), $dir['name']) . '"><i class="fa fa-envelope" aria-hidden="true"></i></a>' : '';
            $mailBtn = ($isMailer) ? '<a href="' . $Mvc->getModelAjaxUrl() . '/dirs/sendDirLink/dirId/'.$dir['id'] . '" class="btn btn-default"  data-toggle="modal" data-target="#listDirsModal"><i class="fa fa-envelope" aria-hidden="true"data-toggle="tooltip" title="' . sprintf($Core->i18n()->translate('Link zur Galerie \'%s\' senden'), $dir['name']) . '"></i></a>' : '';

            /** De-/Activate Action Btn */
            $entryActUrl = $Mvc->getModelUrl() . '/dirs/activate/' . $dir['id'];
            $entryActBtn = ($canEdit && !$dir['active']) ? '<a href="' . $entryActUrl . '" class="btn btn-default" data-action="dir_activate" data-toggle="tooltip" title="' . sprintf( $Core->i18n()->translate('Ordner \'%s\' aktivieren') , $dir['name']) . '"' . $aysChangeStateAttributes . '><i class="fa fa-unlock text-success" aria-hidden="true"></i></a>': '';
            $entryDeActUrl = $Mvc->getModelUrl() . '/dirs/deactivate/' . $dir['id'];
            $entryDeActBtn = ($canEdit && $entryActBtn == '') ? '<a href="' . $entryDeActUrl . '" class="btn btn-default" data-action="dir_deactivate" data-toggle="tooltip" title="' . sprintf( $Core->i18n()->translate('Ordner \'%s\' deaktivieren') , $dir['name']) . '"' . $aysChangeStateAttributes . '><i class="fa fa-lock text-danger" aria-hidden="true"></i></a>': '';

            /** Archive Action Btn */
            $subDirsArchiveStates = $mediaClass->getDirInfoNestedOneLevel($nestedSubDirs, 'archive_state');

            $getCountSubFolderByArchiveStates = (is_array($subDirsArchiveStates)) ? count($subDirsArchiveStates): 0;
            $getCountArchivedSubFolder = 0;
            if($getCountSubFolderByArchiveStates) {
                foreach ($subDirsArchiveStates as $subDirId => $archiveState) {
                    if ($archiveState > 0) {
                        $getCountArchivedSubFolder++;
                    }
                }
            }

            $aysArchAttributesText = ($getCountArchivedSubFolder != $getCountSubFolderByArchiveStates) ? $Core->i18n()->translate('Soll dieser und alle Unterordner archiviert werden?') : $Core->i18n()->translate('Soll dieser Ordner archiviert werden?');
            $aysArchAttributes = ($canArchive) ? ' data-ays="archive" data-aystext="' . htmlentities('<div class="alert alert-warning" role="alert">' . $aysArchAttributesText . '</div>') . '"' : '';

            $entryArchBtn = ($canArchive) ? '<a href="' . $Mvc->getModelUrl() . '/dirs/archive/' . $dir['id'] . '" class="btn btn-warning" data-action="dir_archive"' . $aysArchAttributes . ' data-toggle="tooltip" title="' . sprintf( $Core->i18n()->translate('Ordner \'%s\' archivieren') , $dir['name']) . '"><i class="fa fa-archive" aria-hidden="true"></i></a>' : '';

            /** Auto Delete Action Btn */

            $parentId      = $dir['id_media_dirs__parent'];
            $parentDir     = ($mediaClass->dirExists($parentId)) ? $mediaClass->getDir($parentId) : array();

            $parentDirMaxLifetime          = (count($parentDir)) ? $parentDir['max_lifetime'] : 0;
            $parentDirMaxLifetimeTimeStamp = ($parentDirMaxLifetime !== 0 && (strtotime($parentDirMaxLifetime) !== false) && $Core->Helper()->isValidTimeStamp(strtotime($parentDirMaxLifetime))) ? strtotime($parentDirMaxLifetime) : 0;
            $parentDirAutoDeleteDate       = ($parentDirMaxLifetimeTimeStamp !== 0) ? date($Core->i18n()->getDateFormat(), $parentDirMaxLifetimeTimeStamp) : '';

            $dirMaxLifetime          = (array_key_exists('max_lifetime',$dir)) ? $dir['max_lifetime'] : 0;
            $dirMaxLifetimeTimeStamp = ($dirMaxLifetime !== 0 && (strtotime($dirMaxLifetime) !== false) && $Core->Helper()->isValidTimeStamp(strtotime($dirMaxLifetime))) ? strtotime($dirMaxLifetime) : 0;
            $dirAutoDeleteDate       = ($dirMaxLifetimeTimeStamp !== 0) ? date($Core->i18n()->getDateFormat(), $dirMaxLifetimeTimeStamp) : '';

            $useMaxLifetimeTimeStamp = ($dirMaxLifetimeTimeStamp !== 0) ? $dirMaxLifetimeTimeStamp : $parentDirMaxLifetimeTimeStamp;
            $useAutoDeleteDate       = ($dirAutoDeleteDate !== '') ? $dirAutoDeleteDate : $parentDirAutoDeleteDate;

            $entryAutoDelActive = ($useMaxLifetimeTimeStamp !== 0);

            $entryAutoDelBtnClass = ($entryAutoDelActive) ? ' btn-warning pulse--red' : ' btn-default';
            $entryAutoDelBtnTitlePre = (($dirMaxLifetimeTimeStamp !== 0 && $parentDirMaxLifetimeTimeStamp !== 0) && $parentDirMaxLifetimeTimeStamp < $dirMaxLifetimeTimeStamp) ? sprintf($Core->i18n()->translate('Dieser Ordner wird am %s (mit Übergeordnetem) automatisch gelöscht'), $parentDirAutoDeleteDate) : sprintf($Core->i18n()->translate('Dieser Ordner wird am %s automatisch gelöscht'), $useAutoDeleteDate);
            $entryAutoDelBtnTitle = ($entryAutoDelActive) ? htmlentities($entryAutoDelBtnTitlePre . '<br />' . $Core->i18n()->translate('Automatische Löschung bearbeiten.')) : $Core->i18n()->translate('Automatische Löschung bearbeiten.');
            $entryAutoDelBtnHref  = $Mvc->getModelAjaxUrl() . '/dirs/editAutoDelete/' . $dir['id'];

            $entryAutoDelBtn = ($canAutoDelete) ? '<a href="' . $entryAutoDelBtnHref . '" class="btn' . $entryAutoDelBtnClass . '" data-toggle="modal" data-target="#listDirsModal"><span data-toggle="tooltip" title="' . $entryAutoDelBtnTitle . '"><i class="fa fa-clock-o" aria-hidden="true"></i></span></a>' : '';

            /** Delete Action Btn */
            $aysDelAttributesText = $Core->i18n()->translate('Ganz Sicher? Dadurch werden auch alle Unterordner gelöscht!');
            $aysDelAttributes = ($countSubFolder > 0 && $dirFolderExists) ? ' data-ays="delete" data-aystext="' . htmlentities('<div class="alert alert-warning" role="alert">' . $aysDelAttributesText . '</div>') . '"' : '';
            $entryDelBtn = ($canDelete) ? '<a href="' . $Mvc->getModelUrl() . '/dirs/delete/' . $dir['id'] . '" class="btn btn-danger" data-action="dir_delete"' . $aysDelAttributes . ' data-toggle="tooltip" title="' . sprintf( $Core->i18n()->translate('Ordner \'%s\' löschen') , $dir['name']) . '"><i class="fa fa-trash" aria-hidden="true"></i></a>': '';

            /** Set Button Group */
            $btnGroup = '<div class="btn-group btn-group-sm">';
            $btnGroup .= (!$paramArchived) ? $galleryViewBtn : '';
            $btnGroup .= (!$paramArchived) ? $mediaBrowserBtn : '';
            $btnGroup .= $infoBtn;
            $btnGroup .= (!$paramArchived && is_object($mailerClass)) ? $mailBtn : '';
            $btnGroup .= (!$paramArchived) ? $editBtn : '';
            $btnGroup .= (!$paramArchived) ? $entryActBtn : '';
            $btnGroup .= (!$paramArchived) ? $entryDeActBtn : '';
            $btnGroup .= (!$paramArchived) ? $entryArchBtn : '';
            $btnGroup .= (!$paramArchived) ? $entryAutoDelBtn : '';
            $btnGroup .= $entryDelBtn;
            $btnGroup .= '</div>';

            $btnPopover = '<button type="button" class="btn btn-default btn-xs visible-xs-inline visible-sm-inline" data-container="body" data-toggle="popover" data-placement="left" data-content="' . htmlentities($btnGroup) . '"><i class="fa fa-cog" aria-hidden="true" data-toggle="tooltip" title="' . sprintf( $Core->i18n()->translate('Ordner \'%s\' konfigurieren') , $dir['name']) . '"></i></button>';

            /** Set Id Icon Column */
            $idIconCol = $folderIcon;

            /** Set Name Column */
            $nameCol = $folderNameOutput;

            /** Set Password Column */
            $passwordCol = $dirPassOutput;

            /** Set Date Created Column */
            $createdCol = $created;

            /** Set User Name Column */
            $userNameCol = $userName;

            /** Set Gallery Style Column */
            $galleryStyleCol = '<div class="text-center">' . $galleryStyleColContent . '</div>';

            /** Set Active / Backup Column */
            $activeBackupCol = '<div class="text-center">' . $activeBackupIcon . '</div>';

            /** Set Action Column */
            $notFoundActionCol = (!$paramArchived) ? '<div class="bg-danger text-danger text-center clearfix">' . $Core->i18n()->translate('Ordner nicht gefunden...') . ' <div class="btn-group btn-group-sm pull-right">' . $infoBtn . $entryDelBtn . '</div></div>' : '<div class="btn-group btn-group-sm">' . $infoBtn . $entryDelBtn . '</div>';
            $actionCol = ($dirFolderExists) ? $btnPopover . '<div class="hidden-xs hidden-sm">' . $btnGroup . '</div>' : $notFoundActionCol;

            $tableData = array();

            $tableData[] = $idIconCol;
            $tableData[] = $nameCol;
            if(!$paramArchived) {
                $tableData[] = $passwordCol;
            }
            $tableData[] = $createdCol;
            $tableData[] = $userNameCol;
            if($countGalleryStyles && !$paramArchived) {
                $tableData[] = $galleryStyleCol;
            }
            $tableData[] = $activeBackupCol;
            $tableData[] = $actionCol;

            $data[] = $tableData;

            /** may unlock Gallery */
            if ($canSeeGallery && $dir['active'] && !$mediaClass->galleryUnlocked($dir['id'])) {
                $mediaClass->unlockGallery($dir['id'], $dir['password']);
            }
        }

        if($dtSearchValue !== null) {
            $recordsFiltered = count($dirs);
        }

        $return['draw']            = $dtDraw;
        $return['searchValue']     = $dtSearchValue;
        $return['recordsTotal']    = $recordsTotal;
        $return['recordsFiltered'] = $recordsFiltered;
        $return['data']            = $data;
        $return['debug']['parentId'] = $parentId;

        return $return;
    }
}

function createAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('media_dirs_create')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $return = '';

        $params  = $Mvc->getMvcParams();

        $paramParentId = 0;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id_media_dirs__parent') {
                $paramParentId = (array_key_exists('id_media_dirs__parent', $params)) ? (int)$params['id_media_dirs__parent'] : 0;
            } else {
                $paramParentId = (int)$first_key;
            }
        }

        $parentDir = ($mediaClass->dirExists($paramParentId)) ? $mediaClass->getDir($paramParentId) : array();

        $ob_return = '';
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
        $return .= $ob_return;

        return $return;
    }
}

function editAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('media_dirs_edit')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $return = '';

        $params  = $Mvc->getMvcParams();

        $paramEditId = 0;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id') {
                $paramEditId = (array_key_exists('id', $params)) ? (int)$params['id'] : 0;
            } else {
                $paramEditId = (int)$first_key;
            }
        }

        $dir = ($mediaClass->dirExists($paramEditId)) ? $mediaClass->getDir($paramEditId) : array();

        $ob_return = '';
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
        $return .= $ob_return;

        return $return;
    }
}

function editAutoDeleteAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('media_dirs_autodelete')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $return = '';

        $params  = $Mvc->getMvcParams();

        $paramEditId = 0;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id') {
                $paramEditId = (array_key_exists('id', $params)) ? (int)$params['id'] : 0;
            } else {
                $paramEditId = (int)$first_key;
            }
        }

        $dir = ($mediaClass->dirExists($paramEditId)) ? $mediaClass->getDir($paramEditId) : array();

        $ob_return = '';
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
        $return .= $ob_return;

        return $return;
    }
}

function mediabrowserAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('media_dirs_edit')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $return = '';

        $params  = $Mvc->getMvcParams();

        $paramEditId = 0;
        $paramEditFolderPath = DS;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id') {
                $paramEditId = (array_key_exists('id', $params)) ? (int)$params['id'] : 0;
            } else {
                if(is_numeric($first_key)) {
                    $paramEditId = (int)$first_key;
                } else {
                    $paramEditFolderPath = $first_key;
                }
            }
        }

        $dir = ($mediaClass->dirExists($paramEditId)) ? $mediaClass->getDir($paramEditId) : array();
        if(!count($dir) && $paramEditFolderPath != DS) {
            $dir = $mediaClass->getDirByFolderPath($paramEditFolderPath);
        }

        if(!count($dir) && $paramEditFolderPath == '_galleries_styles_logos') {
            $dir = array(
                'id' => 0,
                'name' => $paramEditFolderPath,
                'dirPath' => $paramEditFolderPath,
            );
        }

        $ob_return = '';
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
        $return .= $ob_return;

        return $return;
    }
}

function mediacontentAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('media_dirs_edit')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $return = '';

        $params  = $Mvc->getMvcParams();

        $paramDirFolderName = '';

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'dirFolderName') {
                $paramDirFolderName = (array_key_exists('dirFolderName', $params)) ? $params['dirFolderName'] : '';
            } else {
                $paramDirFolderName = $first_key;
            }
        }

        $curUrl = trim($Core->Request()->getCurrUrl(),'/') . '/';
        $curMvcModelActionUrl = $Mvc->getModelAjaxUrl($Mvc->getModelKey()) . '/' . $Mvc->getControllerKey() . '/' . $Mvc->getActionKey() . '/';
        $curMvcModelActionSlug = trim(str_replace($curMvcModelActionUrl, '', $curUrl),'/');
        $dirPathFromSlug = str_replace('dirpath/','',$curMvcModelActionSlug);
        $curDirPath = ($dirPathFromSlug === '' || $dirPathFromSlug === 'dirpath' || strpos($dirPathFromSlug, 'http') !== false) ? '/' : '/' . $dirPathFromSlug . '/';
        $curDirPath = str_replace('/', DS, $curDirPath);

        $paramDirFolderName = $curDirPath;

        if($paramDirFolderName == '') {
            return $Core->i18n()->translate('Ordner nicht gefunden...');
        }
        // $dir = $mediaClass->getDirByFolderName($paramDirFolderName);
        // $mediaClass->dirFolderCleanUp($paramDirFolderName);
        $dir = $mediaClass->getDirByFolderPath($paramDirFolderName);

        if(!count($dir) && str_replace(array(DS,'/'),'', $paramDirFolderName) == '_galleries_styles_logos') {
            $dir = array(
                'id' => 0,
                'name' => str_replace(array(DS,'/'),'', $paramDirFolderName),
                'dirPath' => $paramDirFolderName,
                'dirContents' => $mediaClass->getDirContents($paramDirFolderName)
            );
        }

        $ob_return = '';
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
        $return .= $ob_return;

        return $return;
    }
}

function foldercleanupAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('media_dirs_edit')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        set_time_limit ( 0 );
        $return = '';

        $params  = $Mvc->getMvcParams();

        $paramDirFolderName = '';

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'dirFolderName') {
                $paramDirFolderName = (array_key_exists('dirFolderName', $params)) ? $params['dirFolderName'] : '';
            } else {
                $paramDirFolderName = $first_key;
            }
        }

        $curUrl = trim($Core->Request()->getCurrUrl(),'/') . '/';
        $curMvcModelActionUrl = $Mvc->getModelAjaxUrl($Mvc->getModelKey()) . '/' . $Mvc->getControllerKey() . '/' . $Mvc->getActionKey() . '/';
        $curMvcModelActionSlug = trim(str_replace($curMvcModelActionUrl, '', $curUrl),'/');
        $dirPathFromSlug = str_replace('dirpath/','',$curMvcModelActionSlug);
        $curDirPath = ($dirPathFromSlug === '' || $dirPathFromSlug === 'dirpath' || strpos($dirPathFromSlug, 'http') !== false) ? '/' : '/' . $dirPathFromSlug . '/';
        $curDirPath = str_replace('/', DS, $curDirPath);

        $paramDirFolderName = $curDirPath;

        if($paramDirFolderName == '') {
            return $Core->i18n()->translate('Ordner nicht gefunden...');
        }

        $mediaClass->dirFolderCleanUp($paramDirFolderName);
    }
}

function clearcacheAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('media_dirs_edit')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        set_time_limit ( 0 );
        $return = array();

        $params  = $Mvc->getMvcParams();

        $paramDirFolderName = '';

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'dirFolderName') {
                $paramDirFolderName = (array_key_exists('dirFolderName', $params)) ? $params['dirFolderName'] : '';
            } else {
                $paramDirFolderName = $first_key;
            }
        }

        $curUrl = trim($Core->Request()->getCurrUrl(),'/') . '/';
        $curMvcModelActionUrl = $Mvc->getModelAjaxUrl($Mvc->getModelKey()) . '/' . $Mvc->getControllerKey() . '/' . $Mvc->getActionKey() . '/';
        $curMvcModelActionSlug = trim(str_replace($curMvcModelActionUrl, '', $curUrl),'/');
        $dirPathFromSlug = str_replace('dirpath/','',$curMvcModelActionSlug);
        $curDirPath = ($dirPathFromSlug === '' || $dirPathFromSlug === 'dirpath' || strpos($dirPathFromSlug, 'http') !== false) ? '/' : '/' . $dirPathFromSlug . '/';
        $curDirPath = str_replace('/', DS, $curDirPath);

        $paramDirFolderName = $curDirPath;

        if($paramDirFolderName == '') {
            return $Core->i18n()->translate('Ordner nicht gefunden...');
        }

        /** Clear Gallery Cache */
        $return['success'] = $mediaClass->clearGalleryCache($mediaClass->getDirIdByFolderPath($paramDirFolderName));

        if($return['success']) {
            $return['message'] = $Core->i18n()->translate('Gallery Cache successfully cleared.');
        }

        $Mvc->setMVCPlainAjax(true);

        return $return;
    }
}

function fileaddAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     * @var $Request Request
     */
    global $Core, $Mvc;

    $Request = $Core->Request();
    $post = $Request->getPost();

    /**
     * @var $plugins Plugins
     * @var $pitsForms PitsForms
     * @var $pitsCore PitsCore
     */

    /** Prepare Form Plugin */
    $plugins = $Core->Plugins();
    $pitsForms = $plugins->PitsForms();

    $pitsCore = $plugins->PitsCore();

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('media_dirs_edit')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        set_time_limit ( 0 );
        $Mvc->setMVCPlainAjax(true);

        $return = array(
            'success' => false,
            'message' => $Core->i18n()->translate('An Error occurred...'),
            'error'   => $Core->i18n()->translate('An Error occurred...')
        );

        $params  = $Mvc->getMvcParams();

        $paramEditId = 0;
        $paramEditFolderPath = DS;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id') {
                $paramEditId = (array_key_exists('id', $params)) ? (int)$params['id'] : 0;
            } else {
                if(is_numeric($first_key)) {
                    $paramEditId = (int)$first_key;
                } else {
                    if(is_numeric($first_key)) {
                        $paramEditId = (int)$first_key;
                    } else {
                        $paramEditFolderPath = $first_key;
                    }
                }
            }
        }

        $dir = ($mediaClass->dirExists($paramEditId)) ? $mediaClass->getDir($paramEditId) : array();

        if(!count($dir) && $paramEditFolderPath == '_galleries_styles_logos') {
            $dir = array(
                'id' => 0,
                'name' => $paramEditFolderPath,
                'dirPath' => $paramEditFolderPath
            );
        }

        if(!count($dir) || !$mediaClass->checkIfDirFolderExists($dir['dirPath'])) {
            $return = array(
                'success' => false,
                'message' => $Core->i18n()->translate('Ordner nicht gefunden...'),
            	'error'   => $Core->i18n()->translate('Ordner nicht gefunden...')
            );
            return $return;
        }

        $dirFolderName = $dir['dirPath'];

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
//            if(is_object($pitsForms)) {
//                $pitsCore->decryptPOST();
//            }
            if(count($post) && (array_key_exists('_FILES',$post) || isset($_GET["qqchunkuploaddone"]))) {
                $fileArray = (array_key_exists('_FILES',$post)) ? $post['_FILES'] : array();
                $addFileResult = $mediaClass->addFile($fileArray, $dirFolderName);
                
                if(is_array($addFileResult)) {
                	$success = (array_key_exists('success', $addFileResult) && is_bool($addFileResult['success']))   ? $addFileResult['success'] : false;
                	$message = (array_key_exists('message', $addFileResult) && is_string($addFileResult['message'])) ? $addFileResult['message'] : $return['message'];
                	$error   = (array_key_exists('error',   $addFileResult) && is_string($addFileResult['error']))   ? $addFileResult['error']   : $return['error'];
                    
                    $return = array(
                        'success' => $success,
                        'message' => $message,
    					'error'   => $error
                    );
                    
	                $debugNote = array(
	                    '$fileArray' => $fileArray,
	                    '$dirFolderName' => $dirFolderName,
	                    '$addFileResult' => $addFileResult,
	                    '$return' => $return,
	                );
	
	                $Core->Log($debugNote, 'media_fileAdd');
                }
                
            } else {
                $return = array(
                    'success' => false,
                    'message' => $Core->i18n()->translate('Keine Datei zum Speichern übertragen...'),
                    'error'   => $Core->i18n()->translate('Keine Datei zum Speichern übertragen...')
                );
            }
        }

        if($mediaClass->useChunkedFileUpload()) {
            if(is_array($post) && array_key_exists('_FILES',$post)) {

                $fileArray = $post['_FILES'];

                $dz_totalFileSize = (array_key_exists('dztotalfilesize', $post)) ? $post['dztotalfilesize'] : 0;
                $qq_totalFileSize = (array_key_exists('qqtotalfilesize', $post)) ? $post['qqtotalfilesize'] : 0;
                
        		$totalFileSize = ($dz_totalFileSize) ? $dz_totalFileSize : $qq_totalFileSize;
                
	        	$qqFileName     = (array_key_exists('qqfilename', $post)) ? $post['qqfilename'] : $fileArray['file']['name'];
	            $targetFileName = ($fileArray['file']['name'] == 'blob') ? $qqFileName : $fileArray['file']['name'];

                $varPath = $Core->getRelPath() . DS . 'var' . DS;
                $tmpPath = $varPath . 'temp' . DS;
                $chunks_path = $tmpPath . 'chunks' . DS;
                $chunks_fnPath = $chunks_path . $Core->Helper()->createCleanString($targetFileName) . '_' . $totalFileSize . DS;
                $chunks_targetPath = $chunks_fnPath;

                $debugNote = array(
                    '$Request->getPost()' => print_r($post, true),
                    '$chunks_targetPath' => print_r($chunks_targetPath, true),
                    '$_SERVER[\'REQUEST_METHOD\']' => $_SERVER['REQUEST_METHOD']
                );

                $Core->Log($debugNote, 'media_fileAdd');
            }
        }
        
        if($return['success'] && array_key_exists('error', $return)) {
        	unset($return['error']);
        }

        return $return;
    }
}

function filedeleteAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    /**
     * @var $plugins Plugins
     * @var $pitsForms PitsForms
     * @var $pitsCore PitsCore
     */

    /** Prepare Form Plugin */
    $plugins = $Core->Plugins();
    $pitsForms = $plugins->PitsForms();

    $pitsCore = $plugins->PitsCore();

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('media_dirs_edit')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $return = '';

        $params  = $Mvc->getMvcParams();

        $paramDirFolderName = '';

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'dirFolderName') {
                $paramDirFolderName = (array_key_exists('dirFolderName', $params)) ? $params['dirFolderName'] : '';
            } else {
                $paramDirFolderName = $first_key;
            }
        }

        $curUrl = trim($Core->Request()->getCurrUrl(),'/') . '/';
        $curMvcModelActionUrl = $Mvc->getModelAjaxUrl($Mvc->getModelKey()) . '/' . $Mvc->getControllerKey() . '/' . $Mvc->getActionKey() . '/';
        $curMvcModelActionSlug = trim(str_replace($curMvcModelActionUrl, '', $curUrl),'/');
        $dirPathFromSlug = str_replace('dirpath/','',$curMvcModelActionSlug);
        $curDirPath = ($dirPathFromSlug === '' || $dirPathFromSlug === 'dirpath' || strpos($dirPathFromSlug, 'http') !== false) ? '/' : '/' . $dirPathFromSlug . '/';
        $curDirPath = str_replace('/', DS, $curDirPath);

        $paramDirFolderName = $curDirPath;

        $debug = array(
            'curUrl' => $curUrl,
            'curMvcModelActionUrl' => $curMvcModelActionUrl,
            'curMvcModelActionSlug' => $curMvcModelActionSlug,
            'dirPathFromSlug' => $dirPathFromSlug,
            'curDirPath' => $curDirPath
        );

        if($paramDirFolderName == '') {
            $return = array(
                'success' => false,
                'message' => $Core->i18n()->translate('Ordner nicht gefunden...'),
                'debug'   => $debug
            );
            return $return;
        }

//        $dir = $mediaClass->getDirByFolderName($paramDirFolderName);
        $dirId = $mediaClass->getDirIdByFolderPath($paramDirFolderName);
        $dir = ($mediaClass->dirExists($dirId)) ? $mediaClass->getDir($dirId) : array();

        $paramEditFolderPath = trim($paramDirFolderName,DS);

        if(!count($dir) && $paramEditFolderPath == '_galleries_styles_logos') {
            $dir = array(
                'id' => 0,
                'name' => $paramEditFolderPath,
                'dirPath' => $paramEditFolderPath
            );
        }

        $debug['dirId'] = $dirId;
        $debug['dir'] = $dir;

        if(!count($dir) || !$mediaClass->checkIfDirFolderExists($dir['dirPath'])) {

            if($paramDirFolderName == '_galleries_styles_logos')
                $return = array(
                    'success' => false,
                    'message' => $Core->i18n()->translate('Ordner nicht gefunden...'),
                    'debug'   => $debug
                );
            return $return;
        }

//        $dirFolderName = $dir['dirPath'];
        $dirFolderName = $paramDirFolderName;

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
//            if(is_object($pitsForms)) {
//                $pitsCore->decryptPOST();
//            }
            $post = $Core->Request()->getPost();
            $return = array();
            $return['post'] = $post;
            if(count($post)) {
                if(array_key_exists('file',$post)) {
                    $fullFileName = (is_object($post)) ? $post->file : $post['file'];
                    $return['dirFolderName'] = $dirFolderName;
                    $return['fullFileName'] = $fullFileName;

                    if((is_object($fullFileName) || is_array($fullFileName))) {
                        foreach($fullFileName as $fileIndex => $fileEle) {
                            $fullFileName = $fileEle;

                            $addFileResult = $mediaClass->deleteFile($fullFileName, $dirFolderName);
                            if (is_array($addFileResult)) {

                                if ($addFileResult['success']) {
                                    $return['files'][$fullFileName]['success'] = true;
                                    $return['files'][$fullFileName]['message'] = $addFileResult['message'];
                                } else {
                                    $return['files'][$fullFileName]['success'] = false;
                                    $return['files'][$fullFileName]['message'] = $addFileResult['message'];
                                }
                            }
                        }
                    } else {
                        $addFileResult = $mediaClass->deleteFile($fullFileName, $dirFolderName);
                        if (is_array($addFileResult)) {

                            if ($addFileResult['success']) {
                                $return['success'] = true;
                                $return['message'] = $addFileResult['message'];
                            } else {
                                $return['success'] = false;
                                $return['message'] = $addFileResult['message'];
                            }
                        }
                    }
                }
                if(array_key_exists('files',$post) && is_array($post['files']) && count($post['files'])) {
                    foreach($post['files'] as $fileEle) {
                        $fullFileName = (is_object($fileEle)) ? $fileEle->file : $fileEle['file'];
                        $return['fullFileName'] = $fullFileName;
                        $return['dirFolderName'] = $dirFolderName;
                        $addFileResult = $mediaClass->deleteFile($fullFileName, $dirFolderName);
                        if (is_array($addFileResult)) {

                            if ($addFileResult['success']) {
                                $return[$fullFileName]['success'] = true;
                                $return[$fullFileName]['message'] = $addFileResult['message'];
                            } else {
                                $return[$fullFileName]['success'] = false;
                                $return[$fullFileName]['message'] = $addFileResult['message'];
                            }
                        }
                    }
                }
            } else {
                $return = array(
                    'success' => false,
                    'message' => $Core->i18n()->translate('Keine Datei zum Löschen übertragen...')
                );
            }
        }

        return $return;
    }
}

function infoAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('media_dirs_edit')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $return = '';

        $params  = $Mvc->getMvcParams();

        $paramEditId = 0;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id') {
                $paramEditId = (array_key_exists('id', $params)) ? (int)$params['id'] : 0;
            } else {
                $paramEditId = (int)$first_key;
            }
        }

        $dir = ($mediaClass->dirExists($paramEditId)) ? $mediaClass->getDir($paramEditId) : array();

        $ob_return = '';
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
        $return .= $ob_return;

        return $return;
    }
}

function sendDirLinkAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $mailerClass Mailer
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $mailerClass = $Mvc->modelClass('Mailer');
    $accClass = $Mvc->modelClass('Acc');

    if(!is_object($mailerClass)) {
        return $Core->i18n()->translate('Model nicht gefunden...');
    }

    if(is_object($accClass) && !$accClass->hasAccess('mailer_mail_write')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $return = '';

        $site   = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $curUser   = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $addressBookEntry = array();
        $writeTo = '';
        $templateSubject = '';
        $templateContent = '';

        $params  = $Mvc->getMvcParams();
        if(count($params)) {
            $dirId              = (isset($params['dirId'])   && $params['dirId']   != '' && is_numeric($params['dirId'])) ? $params['dirId'] : 0;
        }

        $templateCode = 'dir_link';
        $lang = $Core->i18n()->getCurrLang();

        $template = array();

        $canSeeGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_templates_global'));

        $templateByCode = $mailerClass->getTemplateByCode($templateCode, $lang);
        if(count($templateByCode)) {
            $template = $templateByCode;
        }

        if(count($template) && $template['type'] < 2 && !$canSeeGlobal) {
            $template = array();
        }

        if(count($template)) {
            $templateSubject = (isset($template['subject'])) ? $template['subject'] : '';
            $templateContent = (isset($template['content'])) ? $template['content'] : '';
        }

        if(!count($template)) {
            $note  = $Core->i18n()->translate('Vorlage konnte nicht gefunden werden');
            $type  = 'warning';
            $kind  = 'bs-alert';
            $title = '';
            $Core->setNote($note, $type, $kind, $title);
        }

        $ob_return = '';
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
        $return .= $ob_return;

        return $return;
    }
}

function apiAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    $curUser   = (is_object($accClass)) ? $accClass->getUser() : array();
    $userId = (count($curUser) && array_key_exists('id',$curUser)) ? $curUser['id'] : 0;

    $Mvc->setMVCPlainAjax(true);

    if(!$userId) {

        $mail = (array_key_exists('PHP_AUTH_USER', $_SERVER)) ? $_SERVER['PHP_AUTH_USER'] : '';
        $pass = (array_key_exists('PHP_AUTH_PW', $_SERVER)) ? $_SERVER['PHP_AUTH_PW'] : '';

        $data = array(
            'mail' => $mail,
            'pass' => $pass
        );

        $validated = (is_object($accClass)) ? $accClass->userCheckLoginCredentials($data) : array('success' => false);


        if ($validated['success'] === false) {
            header('WWW-Authenticate: Basic realm="My Realm"');
            header('HTTP/1.0 401 Unauthorized');
            header('Content-Type: application/json; charset=utf-8');

            $message = (is_array($validated) && array_key_exists('message',$validated)) ? $validated['message'] : $Core->i18n()->translate('Nicht Autorisiert');

            $return = array(
                'success' => false,
                'message' => $message
            );

            return $return;
        } else {
            $userId = (is_array($validated) && array_key_exists('userId',$validated)) ? $validated['userId'] : 0;
            if($userId === 0) {
                $return = array(
                    'success' => false,
                    'message' => $Core->i18n()->translate('Kein Benzutzer gefunden...')
                );
                return $return;
            }
        }
    }

    $params  = $Mvc->getMvcParams();

    $paramApiKey = '';

    if(count($params)) {
        reset($params);
        $first_key = key($params);
        if($first_key == 'key') {
            $paramApiKey = (array_key_exists('key', $params)) ? $params['key'] : '';
        } else {
            $paramApiKey = $first_key;
        }
    }

    if($paramApiKey === '') {
        $return = array(
            'success' => false,
            'message' => $Core->i18n()->translate('Kein Api Key gefunden...')
        );
        return $return;
    }

    $validApiKey = $mediaClass->validateApiKey($paramApiKey,$userId);

    if(!$validApiKey) {
        $return = array(
            'success' => false,
            'message' => $Core->i18n()->translate('Api Key Fehlerhaft...')
        );
        return $return;
    }

    // If arrives here, is a valid user.

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        return apiPost($userId);
    } else {
        return apiGet($userId);
    }

}

function apiPost($userId) {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');


}

function apiGet($userId) {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    $user = (is_object($accClass)) ? $accClass->getUser($userId) : array();

    $site = $Core->Sites()->getSite();
    $siteId = (count($site)) ? $site['id'] : 0;

    $getDirsConfig = array(
        'siteId' => $siteId
    );
    $dirs = $mediaClass->getDirs($getDirsConfig);

    $apiArea = (array_key_exists('area', $_GET)) ? $_GET['area'] : '';
    $apiAction = (array_key_exists('action', $_GET)) ? $_GET['action'] : '';

    $return = array();

    if($apiArea === 'dirs') {
        switch ($apiAction) {
            case 'list':
                $return['success'] = true;
                $return['message'] = $Core->i18n()->translate('Auflistung aller Ordner');
                $return['result']  = $dirs;
                break;
            default:
                $return['success'] = false;
                $return['message'] = $Core->i18n()->translate('Keine API Action übergeben');
        }
    }

    if($apiArea === 'dir') {
        $dirId = (array_key_exists('id', $_GET)) ? $_GET['id'] : 0;
        if($dirId !== 0 && $mediaClass->dirExists($dirId)) {
            $dir = $mediaClass->getDir($dirId);

            switch ($apiAction) {
                case 'list':
                    $return['success'] = true;
                    $return['message'] = $Core->i18n()->translate('Auflistung des Ordners');
                    $return['result']  = $dir;
                    break;
                case 'contents':
                    $return['success'] = true;
                    $return['message'] = $Core->i18n()->translate('Auflistung vom Inhalt des Ordners');
                    $return['result']  = $mediaClass->getDirContents($dir['dirPath']);
                    break;
                default:
                    $return['success'] = false;
                    $return['message'] = $Core->i18n()->translate('Keine API Action übergeben');
            }
        } else {
            $return['success'] = false;
            $return['message'] = sprintf($Core->i18n()->translate('Kein Ordner mit der ID \'%s\' gefunden.'), $dirId);
        }
    }

    $return['debug'] = array(
        '$_GET' => $_GET,
        'user' => $user
    );

    return $return;
}