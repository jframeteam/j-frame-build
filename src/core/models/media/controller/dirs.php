<?php
/**
 * Media Model Controller Dirs
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function indexAction()
{
    return listAction();
}

function listAction()
{
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if (is_object($accClass) && !$accClass->hasAccess('media_dirs')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('media_dirs') . ' - ' . $Core->i18n()->translate('media') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('media') . ' - ' . $Core->i18n()->translate('media_dirs'));

        $site = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $curUser = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $canEdit = (!is_object($accClass) || $accClass->hasAccess('media_dirs_edit'));
        $canArchive = (!is_object($accClass) || $accClass->hasAccess('media_dirs_archive'));

        /** Check DB and FS Synchronize State */
        $syncCheck = $mediaClass->checkDbAndFsSynchronizeState();

        $syncCheck_success = (is_array($syncCheck) && array_key_exists('success', $syncCheck)) ? $syncCheck['success'] : false;
        $syncCheck_message = (is_array($syncCheck) && array_key_exists('message', $syncCheck)) ? $syncCheck['message'] : '';

        /** if Sync State is not up-to-date */
        if(!$syncCheck_success) {
            $note = $syncCheck_message;
            $type  = 'warning';
            $kind  = 'bs-alert';
            $title = $Core->i18n()->translate('Hinweis') . '!';
            $Core->setNote($note, $type, $kind, $title);
        }

        /** Get current Dir by Dir Path */
        $curUrl = trim($Core->Request()->getCurrUrl(),'/') . '/';
        $curMvcModelActionUrl = $Mvc->getModelUrl($Mvc->getModelKey()) . '/' . $Mvc->getControllerKey() . '/' . $Mvc->getActionKey() . '/';
        $curMvcModelActionSlug = trim(str_replace($curMvcModelActionUrl, '', $curUrl),'/');
        $dirPathFromSlug = str_replace('dirpath/','',$curMvcModelActionSlug);
        $curDirPath = ($dirPathFromSlug === '' || $dirPathFromSlug === 'dirpath' || strpos($dirPathFromSlug, 'http') !== false) ? '/' : '/' . $dirPathFromSlug . '/';
        $curDirPath = str_replace('/', DS, $curDirPath);

        $curDirByDirPath = $mediaClass->getDirByFolderPath($curDirPath);

        $curParentDirId = (is_array($curDirByDirPath) && count($curDirByDirPath)) ? $curDirByDirPath['id'] : 0;

        $getDirsConfig = array(
            'siteId'       => $siteId
        );
        $getDirs = $mediaClass->getDirs($getDirsConfig);

        $return = '';

        $ob_return = '';
        if ($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    }
}

function saveAction()
{
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    /**
     * @var $plugins Plugins
     * @var $pitsForms PitsForms
     * @var $pitsCore PitsCore
     */

    /** Prepare Form Plugin */
    $plugins = $Core->Plugins();
    $pitsForms = $plugins->PitsForms();

    $pitsCore = $plugins->PitsCore();

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(!is_object($accClass) || ($accClass->hasAccess('media_dirs_edit') || $accClass->hasAccess('media_dirs_create'))) {

        $return = '';

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if(is_object($pitsForms)) {
                $pitsCore->decryptPOST();
            }
            $post = $Core->Request()->getPost();
            if(count($post)) {
                if (isset($post['action'])) {
                    if ($post['action'] == 'dirs_create' || $post['action'] == 'dirs_edit') {
                        $saveResult = $mediaClass->saveDir($post);
                        $return = '<pre>' . print_r($post, true) . '</pre>';
                        $return .= '<pre>' . print_r($saveResult, true) . '</pre>';

                        if(is_array($saveResult) && count($saveResult) && array_key_exists('success', $saveResult) && array_key_exists('msg', $saveResult)) {
                            $note = $saveResult['msg'];
                            if($saveResult['success']) {
                                $type  = 'success';
                                $kind  = 'bs-alert';
                                $title = '';
                                $Core->setNote($note, $type, $kind, $title);
                            } else {
                                $type  = 'danger';
                                $kind  = 'bs-alert';
                                $title = $Core->i18n()->translate('Fehler') . '!';
                                $Core->setNote($note, $type, $kind, $title);
                            }
                            $redirectUrl = $Mvc->getModelUrl() . '/dirs';

                            /** Redirect to Parent Folder List if Set */
                            $post_parentDirId = (array_key_exists('id_media_dirs__parent', $post)) ? $post['id_media_dirs__parent'] : 0;
                            if($mediaClass->dirExists($post_parentDirId)) {
                                $parentDir = $mediaClass->getDir($post_parentDirId);

                                $redirectUrl .= '/list/' . str_replace(DS,'/',trim($parentDir['dirPath'],DS)) . '/';
                            }
                            $Core->Request()->redirect($redirectUrl);
                        }
                        return $return;
                    }
                }
            }
        }
    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }
}

function activateAction()
{
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(!is_object($accClass) || $accClass->hasAccess('media_dirs_edit')) {

        $return = '';

        $params  = $Mvc->getMvcParams();

        $paramEditId = 0;
        $changeAllChildren = false;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id') {
                $paramEditId = (array_key_exists('id', $params)) ? (int)$params['id'] : 0;
                $changeAllChildren = (array_key_exists('allChildren', $params));
            } else {
                $paramEditId = (int)$first_key;
                $changeAllChildren = ($params[$first_key] == 'allChildren');
            }
        }

        $dir = ($mediaClass->dirExists($paramEditId)) ? $mediaClass->getDir($paramEditId) : array();

        if(count($dir)) {
            $saveData = array();
            $saveData['id'] = $dir['id'];
            $saveData['active'] = 1;

            $saveResult = $mediaClass->saveDir($saveData);
            $return .= '<pre>' . print_r($saveResult, true) . '</pre>';

            if(is_array($saveResult) && count($saveResult) && array_key_exists('success', $saveResult) && array_key_exists('msg', $saveResult)) {
                if($saveResult['success']) {
                    $note = $Core->i18n()->translate('Ordner erfolgreich aktiviert!');
                    $type  = 'success';
                    $kind  = 'bs-alert';
                    $title = '';
                    $Core->setNote($note, $type, $kind, $title);
                } else {
                    $note = $Core->i18n()->translate('Ordner konnte nicht aktiviert werden!');
                    $type  = 'danger';
                    $kind  = 'bs-alert';
                    $title = $Core->i18n()->translate('Fehler') . '!';
                    $Core->setNote($note, $type, $kind, $title);
                }

                if($changeAllChildren) {
                    $changeSubDirState_result = changeSubDirState($saveData['id'],$saveData['active']);

                    if(is_array($changeSubDirState_result) && count($changeSubDirState_result)) {
                        $subDirChangeIds = (array_key_exists('subDirChangeIds', $changeSubDirState_result) && is_array($changeSubDirState_result['subDirChangeIds'])) ? count($changeSubDirState_result['subDirChangeIds']) : 0;
                        $subDirChangedIds = (array_key_exists('subDirChangedIds', $changeSubDirState_result) && is_array($changeSubDirState_result['subDirChangedIds'])) ? count($changeSubDirState_result['subDirChangedIds']) : 0;

                        if($subDirChangeIds > 0) {
                            if($subDirChangeIds == $subDirChangedIds) {
                                $note = $Core->i18n()->translate('Unterordner erfolgreich aktiviert!');
                                $type  = 'success';
                                $kind  = 'bs-alert';
                                $title = '';
                                $Core->setNote($note, $type, $kind, $title);
                            }
                            if($subDirChangedIds < $subDirChangeIds) {
                                $note = sprintf($Core->i18n()->translate('Nur %s von %s Unterordnern erfolgreich aktiviert!'), $subDirChangedIds, $subDirChangeIds);
                                $type  = 'warning';
                                $kind  = 'bs-alert';
                                $title = '';
                                $Core->setNote($note, $type, $kind, $title);
                            }
                            if($subDirChangedIds == 0) {
                                $note = sprintf($Core->i18n()->translate('Unterordner konnten nicht aktiviert werden!'), $subDirChangedIds, $subDirChangeIds);
                                $type  = 'warning';
                                $kind  = 'bs-alert';
                                $title = '';
                                $Core->setNote($note, $type, $kind, $title);
                            }
                        }
                    }
                }

                $redirectUrl = $Mvc->getModelUrl() . '/dirs';

                /** Redirect to Parent Folder List if Set */
                $post_parentDirId = (array_key_exists('id_media_dirs__parent', $dir)) ? $dir['id_media_dirs__parent'] : 0;
                if($mediaClass->dirExists($post_parentDirId)) {
                    $parentDir = $mediaClass->getDir($post_parentDirId);

                    $redirectUrl .= '/list/' . str_replace(DS,'/',trim($parentDir['dirPath'],DS)) . '/';
                }
                $Core->Request()->redirect($redirectUrl);
            }
            return $return;

        } else {

            return $Core->i18n()->translate('Ordner nicht gefunden...');
        }

    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }
}

function deactivateAction()
{
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(!is_object($accClass) || $accClass->hasAccess('media_dirs_edit')) {

        $return = '';

        $params  = $Mvc->getMvcParams();

        $paramEditId = 0;
        $changeAllChildren = false;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id') {
                $paramEditId = (array_key_exists('id', $params)) ? (int)$params['id'] : 0;
                $changeAllChildren = (array_key_exists('allChildren', $params));
            } else {
                $paramEditId = (int)$first_key;
                $changeAllChildren = ($params[$first_key] == 'allChildren');
            }
        }

        $dir = ($mediaClass->dirExists($paramEditId)) ? $mediaClass->getDir($paramEditId) : array();

        if(count($dir)) {
            $saveData = array();
            $saveData['id'] = $dir['id'];
            $saveData['active'] = 0;

            $saveResult = $mediaClass->saveDir($saveData);
            $return .= '<pre>' . print_r($saveResult, true) . '</pre>';

            if(is_array($saveResult) && count($saveResult) && array_key_exists('success', $saveResult) && array_key_exists('msg', $saveResult)) {
                if($saveResult['success']) {
                    $note = $Core->i18n()->translate('Ordner erfolgreich deaktiviert!');
                    $type  = 'success';
                    $kind  = 'bs-alert';
                    $title = '';
                    $Core->setNote($note, $type, $kind, $title);
                } else {
                    $note = $Core->i18n()->translate('Ordner konnte nicht deaktiviert werden!') . '<br />&bull; ' . $saveResult['msg'];
                    $type  = 'danger';
                    $kind  = 'bs-alert';
                    $title = $Core->i18n()->translate('Fehler') . '!';
                    $Core->setNote($note, $type, $kind, $title);
                }

                if($changeAllChildren) {
                    $changeSubDirState_result = changeSubDirState($saveData['id'],$saveData['active']);

                    if(is_array($changeSubDirState_result) && count($changeSubDirState_result)) {
                        $subDirChangeIds = (array_key_exists('subDirChangeIds', $changeSubDirState_result) && is_array($changeSubDirState_result['subDirChangeIds'])) ? count($changeSubDirState_result['subDirChangeIds']) : 0;
                        $subDirChangedIds = (array_key_exists('subDirChangedIds', $changeSubDirState_result) && is_array($changeSubDirState_result['subDirChangedIds'])) ? count($changeSubDirState_result['subDirChangedIds']) : 0;

                        if($subDirChangeIds > 0) {
                            if($subDirChangeIds == $subDirChangedIds) {
                                $note = $Core->i18n()->translate('Unterordner erfolgreich deaktiviert!');
                                $type  = 'success';
                                $kind  = 'bs-alert';
                                $title = '';
                                $Core->setNote($note, $type, $kind, $title);
                            }
                            if($subDirChangedIds < $subDirChangeIds) {
                                $note = sprintf($Core->i18n()->translate('Nur %s von %s Unterordnern erfolgreich deaktiviert!'), $subDirChangedIds, $subDirChangeIds);
                                $type  = 'warning';
                                $kind  = 'bs-alert';
                                $title = '';
                                $Core->setNote($note, $type, $kind, $title);
                            }
                            if($subDirChangedIds == 0) {
                                $note = sprintf($Core->i18n()->translate('Unterordner konnten nicht deaktiviert werden!'), $subDirChangedIds, $subDirChangeIds);
                                $type  = 'warning';
                                $kind  = 'bs-alert';
                                $title = '';
                                $Core->setNote($note, $type, $kind, $title);
                            }
                        }
                    }
                }

                $redirectUrl = $Mvc->getModelUrl() . '/dirs';

                /** Redirect to Parent Folder List if Set */
                $post_parentDirId = (array_key_exists('id_media_dirs__parent', $dir)) ? $dir['id_media_dirs__parent'] : 0;
                if($mediaClass->dirExists($post_parentDirId)) {
                    $parentDir = $mediaClass->getDir($post_parentDirId);

                    $redirectUrl .= '/list/' . str_replace(DS,'/',trim($parentDir['dirPath'],DS)) . '/';
                }
                $Core->Request()->redirect($redirectUrl);
            }
            return $return;

        } else {
            $note = $Core->i18n()->translate('Ordner nicht gefunden...');
            $type  = 'danger';
            $kind  = 'bs-alert';
            $title = $Core->i18n()->translate('Fehler') . '!';
            $Core->setNote($note, $type, $kind, $title);

            $redirectUrl = $Mvc->getModelUrl() . '/dirs';
            $Core->Request()->redirect($redirectUrl);
        }

    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }
}

function deleteAction()
{
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(!is_object($accClass) || $accClass->hasAccess('media_dirs_delete')) {

        $return = '';

        $params  = $Mvc->getMvcParams();

        $paramDeleteId = 0;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id') {
                $paramDeleteId = (array_key_exists('id', $params)) ? (int)$params['id'] : 0;
            } else {
                $paramDeleteId = (int)$first_key;
            }
        }

        $dir = ($mediaClass->dirExists($paramDeleteId)) ? $mediaClass->getDir($paramDeleteId) : array();

        if(count($dir)) {

            $deleteResult = $mediaClass->deleteDir($paramDeleteId);
            $return .= '<pre>' . print_r($deleteResult, true) . '</pre>';

            if(is_array($deleteResult) && count($deleteResult) && array_key_exists('success', $deleteResult) && array_key_exists('msg', $deleteResult)) {
                $note = $deleteResult['msg'];
                if($deleteResult['success']) {
                    $type  = 'success';
                    $kind  = 'bs-alert';
                    $title = '';
                    $Core->setNote($note, $type, $kind, $title);

                    $mediaClass->delFileDownloadsFromDir($paramDeleteId);
                } else {
                    $type  = 'danger';
                    $kind  = 'bs-alert';
                    $title = $Core->i18n()->translate('Fehler') . '!';
                    $Core->setNote($note, $type, $kind, $title);
                }
                $redirectUrl = $Mvc->getModelUrl() . '/dirs';

                /** Redirect to Parent Folder List if Set */
                $parentDirId = (array_key_exists('id_media_dirs__parent', $dir)) ? $dir['id_media_dirs__parent'] : 0;
                if($mediaClass->dirExists($parentDirId)) {
                    $parentDir = $mediaClass->getDir($parentDirId);

                    $redirectUrl .= '/list/' . str_replace(DS,'/',trim($parentDir['dirPath'],DS)) . '/';
                }
                $Core->Request()->redirect($redirectUrl);
            }
            return $return;

        } else {

            return $Core->i18n()->translate('Ordner nicht gefunden...');
        }

    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }
}

function archiveAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(!is_object($accClass) || $accClass->hasAccess('media_dirs_archive')) {

        $return = '';

        $params  = $Mvc->getMvcParams();

        $paramEditId = 0;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id') {
                $paramEditId = (array_key_exists('id', $params)) ? (int)$params['id'] : 0;
            } else {
                $paramEditId = (int)$first_key;
            }
        }

        $dir = ($mediaClass->dirExists($paramEditId)) ? $mediaClass->getDir($paramEditId) : array();

        if(count($dir)) {
            $saveData = array();
            $saveData['id'] = $dir['id'];
            $saveData['archived'] = 1;

            $nestedDirs    = $mediaClass->getDirsNested(null, $dir['id'], false);
            $subDirsArchiveStates = $mediaClass->getDirInfoNestedOneLevel($nestedDirs, 'archive_state');

            $subDirsArchiveStates_notarchived = array();

            foreach($subDirsArchiveStates as $dirId => $archiveState) {
                if($archiveState < 1) {
                    $subDirsArchiveStates_notarchived[$dirId] = $archiveState;
                }
            }

            $getCountSubFolder = (is_array($subDirsArchiveStates_notarchived)) ? count($subDirsArchiveStates_notarchived): 0;
            $getSubFolderToArchive = array();

            $setArchiveStateToDir_result = $mediaClass->setArchiveStateToDir($dir['id'],$saveData['archived']);

            $getCountArchivedSubFolder = null;
            $isParentFolderArchived = false;
            if($getCountSubFolder) {
                $getCountArchivedSubFolder = 0;
                foreach ($subDirsArchiveStates as $subDirId => $archiveState) {
                    if ($archiveState < 1) {
                        $getSubFolderToArchive[$subDirId] = $archiveState;
                    }
                }
                foreach ($setArchiveStateToDir_result as $subDirId => $archiveResult) {
                    $archiveResultSuccess = (is_array($archiveResult) && array_key_exists('archive', $archiveResult) && is_array($archiveResult['archive']) && array_key_exists('success', $archiveResult['archive'])) ? $archiveResult['archive']['success'] : false;
                    if ($archiveResultSuccess) {
                        if($subDirId == $dir['id']) {
                            $isParentFolderArchived = true;
                        }
                        if(array_key_exists($subDirId, $getSubFolderToArchive)) {
                            $getCountArchivedSubFolder++;
                        }
                    }
                }
            } else {
                $isParentFolderArchived = (
                    is_array($setArchiveStateToDir_result) &&
                    array_key_exists($dir['id'],$setArchiveStateToDir_result) &&
                    is_array($setArchiveStateToDir_result[$dir['id']]) &&
                    array_key_exists('archive', $setArchiveStateToDir_result[$dir['id']]) &&
                    is_array($setArchiveStateToDir_result[$dir['id']]['archive']) &&
                    array_key_exists('success', $setArchiveStateToDir_result[$dir['id']]['archive']) &&
                    is_bool($setArchiveStateToDir_result[$dir['id']]['archive']['success'])
                ) ? $setArchiveStateToDir_result[$dir['id']]['archive']['success'] : false;
            }

            $archieResultValid = (is_array($setArchiveStateToDir_result) && count($setArchiveStateToDir_result));
            if($archieResultValid ) {
                if($isParentFolderArchived) {
                    $note = $Core->i18n()->translate('Ordner erfolgreich zur Archivierung vorgemerkt!');
                    $type  = 'success';
                    $kind  = 'bs-alert';
                    $title = '';
                    $Core->setNote($note, $type, $kind, $title);

                    if($getCountSubFolder) {
                        if($getCountArchivedSubFolder !== null) {

                            if($getCountArchivedSubFolder === count($getSubFolderToArchive)) {
                                $note = $Core->i18n()->translate('Unterordner erfolgreich zur Archivierung vorgemerkt!');
                                $type  = 'success';
                                $kind  = 'bs-alert';
                                $title = '';
                                $Core->setNote($note, $type, $kind, $title);
                            }

                            if($getCountArchivedSubFolder < count($getSubFolderToArchive)) {
                                $note = sprintf($Core->i18n()->translate('Nur (%s/%s) Unterordner konnten erfolgreich zur Archivierung vorgemerkt werden!'),$getCountArchivedSubFolder,count($getSubFolderToArchive));
                                $type  = 'success';
                                $kind  = 'bs-alert';
                                $title = '';
                                $Core->setNote($note, $type, $kind, $title);
                            }

                            if($getCountArchivedSubFolder === 0) {
                                $note = $Core->i18n()->translate('Unterordner konnte nicht archiviert werden!');
                                $type  = 'danger';
                                $kind  = 'bs-alert';
                                $title = $Core->i18n()->translate('Fehler') . '!';
                                $Core->setNote($note, $type, $kind, $title);
                            }

                        }
                    }
                } else {
                    $note = $Core->i18n()->translate('Ordner konnte nicht archiviert werden!');
                    $type  = 'danger';
                    $kind  = 'bs-alert';
                    $title = $Core->i18n()->translate('Fehler') . '!';
                    $Core->setNote($note, $type, $kind, $title);
                }
                $redirectUrl = $Mvc->getModelUrl() . '/dirs/#archived';
                $Core->Request()->redirect($redirectUrl);
            }

            return $return;

        } else {

            return $Core->i18n()->translate('Ordner nicht gefunden...');
        }

    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }
}

function dearchiveAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(!is_object($accClass) || $accClass->hasAccess('media_dirs_archive')) {

        $return = '';

        /** @var $sitesClass Sites */
        $sitesClass = $Core->Sites();
        $site      = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
        $curSiteId = (count($site)) ? $site['id'] : 0;

        $getDirsConfig = array(
            'siteId' => $curSiteId,
            'parentId' => 'all',
            'archived' => true
        );

        $archivedDirs = $mediaClass->getDirs($getDirsConfig);

        $dirsArchiveStates = $mediaClass->getDirInfoNestedOneLevel($archivedDirs, 'archive_state');

        $markedForArchiveDirsIds = array();

        foreach($dirsArchiveStates as $dirId => $archiveState) {
            if($archiveState == 1 || $archiveState == 3) {
                $markedForArchiveDirsIds[] = $dirId;
            }
        }

        $resettedArchiveDirCount = null;
        if(count($markedForArchiveDirsIds)) {
            $resettedArchiveDirCount = 0;
            foreach($markedForArchiveDirsIds as $dirId) {
                $setArchiveStateToDir_result = $mediaClass->setArchiveStateToDir($dirId, 0, false);
                $setArchiveStateToDir_resultSuccess = (
                    is_array($setArchiveStateToDir_result) &&
                    array_key_exists($dirId, $setArchiveStateToDir_result) &&
                    is_array($setArchiveStateToDir_result[$dirId]) &&
                    array_key_exists('archive',$setArchiveStateToDir_result[$dirId]) &&
                    is_array($setArchiveStateToDir_result[$dirId]['archive']) &&
                    array_key_exists('success',$setArchiveStateToDir_result[$dirId]['archive']) &&
                    is_bool($setArchiveStateToDir_result[$dirId]['archive']['success'])
                ) ? $setArchiveStateToDir_result[$dirId]['archive']['success'] : false;

                if($setArchiveStateToDir_resultSuccess) {
                    $resettedArchiveDirCount++;
                }
            }
        }

        if(count($markedForArchiveDirsIds)) {
            if($resettedArchiveDirCount !== null) {
                if($resettedArchiveDirCount === count($markedForArchiveDirsIds)) {
                    $note = $Core->i18n()->translate('Alle zur Archivierung vorgemerkten Ordner wurden erfolgreich zurückgesetzt!');
                    $type  = 'success';
                    $kind  = 'bs-alert';
                    $title = '';
                    $Core->setNote($note, $type, $kind, $title);
                }

                if($resettedArchiveDirCount < count($markedForArchiveDirsIds)) {
                    $note = sprintf($Core->i18n()->translate('Nur (%s/%s) zur Archivierung vorgemerkten Ordner konnten erfolgreich zurückgesetzt werden!'),$resettedArchiveDirCount,count($markedForArchiveDirsIds));
                    $type  = 'success';
                    $kind  = 'bs-alert';
                    $title = '';
                    $Core->setNote($note, $type, $kind, $title);
                }

                if($resettedArchiveDirCount === 0) {
                    $note = $Core->i18n()->translate('Keiner der zur Archivierung vorgemerkten Ordner konnten zurückgesetzt werden!');
                    $type  = 'danger';
                    $kind  = 'bs-alert';
                    $title = '';
                    $Core->setNote($note, $type, $kind, $title);
                }
            } else {
                $note = $Core->i18n()->translate('Keiner der zur Archivierung vorgemerkten Ordner konnten zurückgesetzt werden!');
                $type  = 'warning';
                $kind  = 'bs-alert';
                $title = $Core->i18n()->translate('Fehler') . '!';
                $Core->setNote($note, $type, $kind, $title);
            }
        } else {
            $note = $Core->i18n()->translate('Keine zur Archivierung vorgemerkten Ordner gefunden!');
            $type  = 'warning';
            $kind  = 'bs-alert';
            $title = $Core->i18n()->translate('Fehler') . '!';
            $Core->setNote($note, $type, $kind, $title);
        }
        $redirectUrl = $Mvc->getModelUrl() . '/dirs/';

        $getDirsCountConfigArchived = array(
            'siteId'               => $curSiteId,
            'parentId'               => 'all',
            'archived'               => true,
            'onlyFirstArchiveParent' => true
        );

        if($mediaClass->getDirsCount($getDirsCountConfigArchived)) {
            $redirectUrl .= '#archived';
        }
        $Core->Request()->redirect($redirectUrl);

        return $return;

    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }
}

function syncAction()
{
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    if(!is_object($accClass) || $accClass->hasAccess('media_dirs_create')) {

        $params  = $Mvc->getMvcParams();

        $syncFolder        = (is_array($params) && array_key_exists('folder',$params) && $params['folder'] == 1);
        $syncFileDownloads = (is_array($params) && array_key_exists('fileDownloads',$params) && $params['fileDownloads'] == 1);

        if($syncFolder || $syncFileDownloads) {
            if ($syncFolder) {

                /** Get Differences in Filesystem an Database Information */
                $syncCheck = $mediaClass->synchronizeDbAndFs();
                $syncCheck_success = (is_array($syncCheck) && array_key_exists('success', $syncCheck)) ? $syncCheck['success'] : false;
                $syncCheck_message = (is_array($syncCheck) && array_key_exists('message', $syncCheck)) ? $syncCheck['message'] : '';

                if ($syncCheck_success) {
                    $note = $syncCheck_message;
                    $type = 'success';
                    $kind = 'bs-alert';
                    $title = '';
                } else {
                    $note = $syncCheck_message;
                    $type = 'danger';
                    $kind = 'bs-alert';
                    $title = $Core->i18n()->translate('Fehler') . '!';
                }

                $Core->setNote($note, $type, $kind, $title);
            }

            if ($syncFileDownloads) {
                /** Sync File-Downloads Statistics **/
                $syncFileDownloadsCheck = $mediaClass->syncFileDownloads();

                $syncFileDownloadsCheck_success = (is_array($syncFileDownloadsCheck) && array_key_exists('success', $syncFileDownloadsCheck)) ? $syncFileDownloadsCheck['success'] : false;
                $syncFileDownloadsCheck_message = (is_array($syncFileDownloadsCheck) && array_key_exists('message', $syncFileDownloadsCheck)) ? $syncFileDownloadsCheck['message'] : '';

                if ($syncFileDownloadsCheck_success) {
                    $note = $syncFileDownloadsCheck_message;
                    $type = 'success';
                    $kind = 'bs-alert';
                    $title = '';
                } else {
                    $note = $syncFileDownloadsCheck_message;
                    $type = 'danger';
                    $kind = 'bs-alert';
                    $title = $Core->i18n()->translate('Fehler') . '!';
                }

                $Core->setNote($note, $type, $kind, $title);
            }
        } else {
            $note = $Core->i18n()->translate('Keine Synchronisations-Art ausgewählt...');
            $type  = 'danger';
            $kind  = 'bs-alert';
            $title = '';

            $Core->setNote($note, $type, $kind, $title);
        }

    } else {
        $note = $Core->i18n()->translate('Keine Berechtigung zum Synchronisieren...');
        $type  = 'danger';
        $kind  = 'bs-alert';
        $title = '';

        $Core->setNote($note, $type, $kind, $title);
    }

    $redirectUrl = $Mvc->getModelUrl() . '/dirs';
    $Core->Request()->redirect($redirectUrl);
}

function changeSubDirState($dirId,$newState) {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    $result = array();

    if(!is_object($accClass) || $accClass->hasAccess('media_dirs_edit')) {
        $saveData = array();
        $saveData['active'] = $newState;

        $nestedDirs = $mediaClass->getDirsNested(null, $dirId, false);
        $subDirsStates = $mediaClass->getDirInfoNestedOneLevel($nestedDirs, 'active');

        $subDirChangeIds = array();
        $subDirChangedIds = array();
        foreach ($subDirsStates as $subDirId => $dirActive) {
            $subDirsStates_dir = $mediaClass->getDir($subDirId);
            $subDirsStates_archiveState = $subDirsStates_dir['archive_state'];
            if ($dirActive != $newState && $subDirsStates_archiveState < 1) {
                $subDirChangeIds[$subDirId] = $subDirId;
                $saveData['id'] = $subDirId;

                $saveResult = $mediaClass->saveDir($saveData);
                if (is_array($saveResult) && count($saveResult) && array_key_exists('success', $saveResult)) {
                    if ($saveResult['success']) {
                        $subDirChangedIds[$subDirId]['id'] = $subDirId;
                        $subDirChangedIds[$subDirId]['saveData'] = $saveData;
                        $subDirChangedIds[$subDirId]['saveResult'] = $saveResult;
                    }
                }
            }
        }

        $result['subDirChangeIds'] = $subDirChangeIds;
        $result['subDirChangedIds'] = $subDirChangedIds;
    }

    return $result;
}

function contentsAction()
{

    // http://www.example.de/{{site-key}}/{{model-key}}/{{controller-key}}/{{action-key}}/{{dir-path}}/{{file-name}}
    // http://www.example.de/{{site-key}}/{{model-key}}/{{controller-key}}/{{action-key}}/{{dir-path}}/thumbnails/{{file-name}}

    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $Helper Helper
     * @var $mediaClass Media
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $Helper = $Core->Helper();
    $mediaClass = $Mvc->modelClass('Media');
    $accClass = $Mvc->modelClass('Acc');

    $canViewFile   = false;
    $hasAccess     = (!is_object($accClass) || $accClass->hasAccess('media_dirs'));
    $isDir         = false;
    $public        = false;
    $fileExists    = false;
    $getThumbnail  = false;
    $fileName      = '';
    $fileNameGiven = false;
    $fullFilePath  = '';
    
    $publicDirPathRoots = $mediaClass->publicDirPathRoots();

    /** Get current Dir by Dir Path */
    $curUrl = trim($Core->Request()->getCurrUrl(),'/') . '/';
    $curMvcModelActionUrl = $Mvc->getModelUrl($Mvc->getModelKey()) . '/' . $Mvc->getControllerKey() . '/' . $Mvc->getActionKey() . '/';
    $curMvcModelActionSlug = trim(str_replace($curMvcModelActionUrl, '', $curUrl),'/');
    $dirPathFromSlug = str_replace('dirpath/','',$curMvcModelActionSlug);
    $curDirPath = ($dirPathFromSlug === '' || $dirPathFromSlug === 'dirpath' || strpos($dirPathFromSlug, 'http') !== false) ? '/' : '/' . $dirPathFromSlug . '/';
    $curDirPath = str_replace('/', DS, $curDirPath);

    $curDirByDirPath = $mediaClass->getDirByFolderPath($curDirPath);

    $dir = $curDirByDirPath;

    if(!count($curDirByDirPath)) {

        $curDirPathArray = (strpos($curDirPath, DS) !== false) ? preg_split('#\/#', str_replace(DS, '/', $curDirPath), -1, PREG_SPLIT_NO_EMPTY) : array();
        
        $curDirPathRoot = reset($curDirPathArray);

        $fileName = end($curDirPathArray);
        $fileNameGiven = ($fileName !== '');

        $curDirPath_new = DS . trim(str_replace($fileName,'',$curDirPath),DS) . DS;

        if($curDirPath_new == DS.DS) {
            $curDirPath_new = DS;
        }

        if(strpos($curDirPath_new, DS.'thumbnails'.DS) !== false) {
            $curDirPath_new = DS . trim(str_replace('thumbnails','',$curDirPath_new),DS) . DS;
            $getThumbnail = true;
        }

        $curDirByDirPath_new = $mediaClass->getDirByFolderPath($curDirPath_new);

	    if(!count($curDirByDirPath_new) && in_array($curDirPathRoot,$publicDirPathRoots)) {
	        $curDirByDirPath_new = array(
	            'id'      => 0,
	            'name'    => $curDirPath,
	            'dirPath' => $curDirPath,
                'public'  => true
	        );
	    }

        if(count($curDirByDirPath_new)) {
            $isDir = true;
            $dir = $curDirByDirPath_new;
        }

        $public = (count($dir) && array_key_exists('public',$dir) && is_bool($dir['public'])) ? $dir['public'] : false;
        
        if(in_array($curDirPathRoot,$publicDirPathRoots)) {
        	$public = true;
        }

        $dirId = (is_array($dir) && count($dir)) ? $dir['id'] : 0;

        $galleryUnlocked = $mediaClass->galleryUnlocked($dirId);

        if(!$public && $galleryUnlocked) {
            $public = true;
        }

        $dirPath = $mediaClass->getDirPath($curDirPath_new);

        $fullFilePath = $dirPath . DS . urldecode($fileName);

        if($getThumbnail) {
            $fullFilePath = $dirPath . DS . 'thumbnails' . DS . urldecode($fileName);
        }

        if(file_exists($fullFilePath)) {
            $fileExists = true;
        }
		
        if (
            $fileNameGiven &&
            $isDir &&
            $fileExists
        ) {
        	if ($public) {
            	$canViewFile = true;
        	} else {
	        	if($hasAccess) {
	            	$canViewFile = true;
	        	}
        	}
        }

    }
	
	$debugLog = '<pre>' .
		'$canViewFile => ' . print_r($canViewFile, true) . PHP_EOL .
		'$fileNameGiven => ' . print_r($fileNameGiven, true) . PHP_EOL .
		'$isDir => ' . print_r($isDir, true) . PHP_EOL .
		'$fileExists => ' . print_r($fileExists, true) . PHP_EOL .
		'$pubic => ' . print_r($public, true) . PHP_EOL .
		'$hasAccess => ' . print_r($hasAccess, true) . PHP_EOL .
		'</pre>';
		
//	echo $debugLog; exit();

    if ($canViewFile) {

        $mimeType = $Helper->getMimeContentType($fullFilePath);

        header("Cache-Control: maxage=1");
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Content-type: ' . $mimeType);
        header('Content-Disposition: inline; filename="' . urldecode($fileName) . '"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($fullFilePath));
        header('Accept-Ranges: bytes');
        @readfile($fullFilePath);
        exit;

    } else {
        if (!$fileNameGiven) {
            return '<pre>' . $Core->i18n()->translate('Keine Datei angegeben...') . '</pre>';
        }

        if (!$isDir) {
            return '<pre>' . $Core->i18n()->translate('Ordner nicht gefunden...') . '</pre>';
        }

        if (!$fileExists) {
            header("Status: 404 Not Found");
            echo '<pre>' . $Core->i18n()->translate('Datei nicht gefunden...') . '</pre>';
            exit;
        }

        if (!$hasAccess || !$public) {
            header("HTTP/1.1 401 Unauthorized");
            return '<pre>' . $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...') . '</pre>';
        }
    }
}
