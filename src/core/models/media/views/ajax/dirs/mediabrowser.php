<?php
/**
 * Media Model Ajax View Dirs MediaBrowser
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $mediaClass Media
 */
$dirSaveUrl = $Mvc->getModelUrl() . '/dirs/save';
$dirFound   = (count($dir)) ? true : false;
$incUrl     = $Core->getCoreUrl() . '/models/media/inc';


?>
<?php if($dirFound) : ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">
            <?php echo sprintf($Core->i18n()->translate('Ordner Inhalt von \'%s\' bearbeiten'), $dir['name']); ?>
        </h4>
    </div>
    <div class="modal-body mediabrowser">

        <div class="action-wrapper row">
            <div class="col-sm-12">
                <div class="btn-set top text-right">
                    <span class="btn btn-default" data-action="select_all" onClick="selectAllThumbnails()" title="<?php echo $Core->i18n()->translate('Alle Elemente auswählen'); ?>" style="display: none;">
                        <i class="fa fa-check-square-o" aria-hidden="true"></i>
                    </span>
                    <span class="btn btn-danger" data-action="del_selected" onClick="deleteSelectedMediaElements()" title="<?php echo $Core->i18n()->translate('Ausgewählte Elemente löschen') ?>" style="display: none;">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </span>
                    <span class="btn btn-default" data-action="toggle_listview" onClick="toggleMediaListView()" title="<?php echo $Core->i18n()->translate('Ansicht wechseln') ?>" style="display: none;">
                        <i class="fa fa-th-large gridviewicon" aria-hidden="true"></i>
                        <i class="fa fa-th-list listviewicon" aria-hidden="true"></i>
                    </span>
                    <span class="btn btn-primary" title="<?php echo $Core->i18n()->translate('Datei(en) hochladen') ?>" onclick="$('#media-file-upload').click();">
                        <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                    </span>
                </div>
            </div>
        </div>

        <div id="uploadArea" class="uploadArea">
            <div class="clearfix" id="media-file-upload" title="<?php echo $Core->i18n()->translate('Um dateien hochzuladen, diese hier hineinziehen oder klicken') ?>" data-dirid="<?php echo $dir['id']; ?>" data-dirfoldername="<?php echo $dir['dirPath']; ?>"></div>
            <div class="uploadActionBtns text-right">
                <div class="btn btn-success btn-sm dz-upload-start"><?php echo $Core->i18n()->translate('Upload starten') ?></div>
                <div class="btn btn-danger btn-sm dz-upload-cancel"><?php echo $Core->i18n()->translate('Abbrechen') ?></div>
            </div>
        </div>

        <div class="media-content" id="mc-<?php echo str_replace(DS,'_', trim($dir['dirPath'], DS)); ?>">
            <div class="media-loader"></div>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Schließen'); ?></button>
    </div>
<?php else: ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">
            <?php echo $Core->i18n()->translate('Ordner Inhalt bearbeiten'); ?>
        </h4>
    </div>
    <div class="modal-body">
        <div class="alert alert-danger" role="alert"><?php echo $Core->i18n()->translate('Ordner nicht gefunden...') ?></div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Schließen'); ?></button>
    </div>
<?php endif; ?>