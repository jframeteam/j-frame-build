<?php
/**
 * Media Model Ajax View Dirs Edit
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $accClass Acc
 * @var $plugins Plugins
 * @var $pitsForms PitsForms
 * @var $pitsCore PitsCore
 */

/** Prepare Form Plugin */
$plugins = $Core->Plugins();
$pitsForms = $plugins->PitsForms();
$isForms = (is_object($pitsForms));
$pitsCore = $plugins->PitsCore();

$btnActionName = ($isForms) ? $pitsCore->xorEnc('action') : 'action';

$dirSaveUrl = $Mvc->getModelUrl() . '/dirs/save';
$dirFound   = (count($dir)) ? true : false;
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">
        <?php echo $Core->i18n()->translate('Ordner bearbeiten') ?>
    </h4>
</div>
<?php if($dirFound) : ?>
    <?php
        $parentId      = $dir['id_media_dirs__parent'];
        $parentDir     = ($mediaClass->dirExists($parentId)) ? $mediaClass->getDir($parentId) : array();

        $parentDirMaxLifetime          = (count($parentDir)) ? $parentDir['max_lifetime'] : 0;
        $parentDirMaxLifetimeTimeStamp = ($parentDirMaxLifetime !== 0 && (strtotime($parentDirMaxLifetime) !== false) && $Core->Helper()->isValidTimeStamp(strtotime($parentDirMaxLifetime))) ? strtotime($parentDirMaxLifetime) : 0;
        $parentDirAutoDeleteDate       = ($parentDirMaxLifetimeTimeStamp !== 0) ? date($Core->i18n()->getDateFormat(), $parentDirMaxLifetimeTimeStamp) : '';

        $dirMaxLifetime          = (array_key_exists('max_lifetime',$dir)) ? $dir['max_lifetime'] : 0;
        $dirMaxLifetimeTimeStamp = ($dirMaxLifetime !== 0 && (strtotime($dirMaxLifetime) !== false) && $Core->Helper()->isValidTimeStamp(strtotime($dirMaxLifetime))) ? strtotime($dirMaxLifetime) : 0;
        $dirAutoDeleteDate       = ($dirMaxLifetimeTimeStamp !== 0) ? date($Core->i18n()->getDateFormat(), $dirMaxLifetimeTimeStamp) : '';
    ?>
    <form id="edit_form" class="form-horizontal" action="<?php echo $dirSaveUrl ?>" method="post">
        <?php
        /** Hidden Id Element */
        $formElementData['eleType']       = 'input';
        $formElementData['name']          = 'id';
        $formElementData['label']         = false;
        $formElementData['value']         = $dir['id'];
        $formElementData['type']         = 'hidden';
        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
        ?>
        <?php
        /** Hidden Id Element */
        $formElementData['eleType']       = 'input';
        $formElementData['id']            = '';
        $formElementData['name']          = 'id_media_dirs__parent';
        $formElementData['label']         = false;
        $formElementData['value']         = (count($parentDir)) ? $parentDir['id'] : 0;
        $formElementData['type']          = 'hidden';
        $formElementData['isRequired']    = false;
        $formElementData['ownAttributes'] = array();
        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
        ?>
        <div class="modal-body">

            <?php if($dirAutoDeleteDate !== '' || $parentDirAutoDeleteDate !== '') : ?>
                <?php if($parentDirAutoDeleteDate !== '') : ?>
                    <?php $showDate = ($dirMaxLifetimeTimeStamp !== 0 && ($dirMaxLifetimeTimeStamp < $parentDirMaxLifetimeTimeStamp)) ? $dirAutoDeleteDate : $parentDirAutoDeleteDate; ?>
                    <div class="alert alert-warning" role="alert"><?php echo $Core->i18n()->translate('Übergeordneter Ordner ist bereits zur automatischen Löschung vorgesehen!') . ' (' . $parentDirAutoDeleteDate . ')<br /><i class="fa fa-clock-o pulse--red" aria-hidden="true"></i> ' . sprintf($Core->i18n()->translate('Dieser Ordner wird am %s automatisch gelöscht'), $showDate); ?></div>
                <?php endif; ?>
                <?php if($dirAutoDeleteDate !== '' && $parentDirAutoDeleteDate === '') : ?>
                    <div class="alert alert-warning" role="alert"><?php echo '<i class="fa fa-clock-o pulse--red" aria-hidden="true"></i> ' . sprintf($Core->i18n()->translate('Dieser Ordner wird am %s automatisch gelöscht'), $dirAutoDeleteDate); ?></div>
                <?php endif; ?>
            <?php endif; ?>

            <div class="form-group">
                <label for="name" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Automatische Löschung') ?></label>

                <div class="col-md-10">
                    <?php
                        $dirMaxLifetime          = (array_key_exists('max_lifetime',$dir)) ? $dir['max_lifetime'] : 0;
                        $dirMaxLifetimeTimeStamp = ($dirMaxLifetime !== 0 && (strtotime($dirMaxLifetime) !== false)) ? strtotime($dirMaxLifetime) : 0;
                        $dirAutoDeleteDate       = ($Core->Helper()->isValidTimeStamp($dirMaxLifetimeTimeStamp)) ? date($Core->i18n()->getDateFormat(), $dirMaxLifetimeTimeStamp) : '';

                        $max_lifetime_value = ($dirAutoDeleteDate !== '') ? $dirAutoDeleteDate : $parentDirAutoDeleteDate;

                        $endDate = ($parentDirAutoDeleteDate !== '') ? $parentDirAutoDeleteDate : '';

                        $endDateAttr = ($endDate !== '') ? ' data-end="' . $endDate . '"' : '';
                    ?>
                    <div class="input-group datepicker date" data-start="tomorrow"<?php echo $endDateAttr; ?>>
                        <?php
                        /** Styles Element */
                        $formElementData['eleType']       = 'input';
                        $formElementData['id']            = 'max_lifetime';
                        $formElementData['name']          = 'max_lifetime';
                        $formElementData['label']         = false;
                        $formElementData['type']          = 'datetime';
                        $formElementData['isRequired']    = false;
                        $formElementData['value']         = $max_lifetime_value;
                        $formElementData['ownAttributes'] = array(
                            'placeholder' => $Core->i18n()->translate('Datum der automatischen Löschung')
                        );
                        $formElementData['cssClasses']    = ' date';
                        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                        ?>
                        <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                    </div>
                    <span class="help-block"><?php echo $Core->i18n()->translate('Falls hier ein Datum gesetzt ist, wird dieser Ordner inklusive aller Unterordner an diesem Datum gelöscht.') ?></span>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Abbrechen'); ?></button>
            <button type="submit" class="btn btn-success" name="<?php echo $btnActionName; ?>" value="dirs_edit" data-action="dirs_edit"><?php echo $Core->i18n()->translate('Speichern'); ?></button>
        </div>
    </form>
<?php else: ?>
    <div class="modal-body">
        <div class="alert alert-danger" role="alert"><?php echo $Core->i18n()->translate('Ordner nicht gefunden...') ?></div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Schließen'); ?></button>
    </div>
<?php endif; ?>