<?php
/**
 * Media Model Ajax View Dirs Info
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $accClass Acc
 * @var $mediaClass Media
 * @var $mailerClass Mailer
 */

$mailerClass = $Mvc->modelClass('Mailer');

$canAutoDelete = (!is_object($accClass) || $accClass->hasAccess('media_dirs_autodelete'));
$dirFound   = (count($dir)) ? true : false;

$ermClass = $Mvc->modelClass('Erm');

$curUser = (is_object($accClass)) ? $accClass->getUser() : array();
$isSu    = (array_key_exists('su', $curUser)) ? $curUser['su'] : false;
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">
        <?php echo $Core->i18n()->translate('Ordner Informationen') ?>
    </h4>
</div>

<div class="modal-body">
    <?php if($dirFound) : ?>

        <?php
            $site = $Core->Sites()->getSite();
            $siteId = (count($site)) ? $site['id'] : 0;

            $countGalleryStyles = $mediaClass->getGalleryStylesCount($siteId);

            $parentId      = $dir['id_media_dirs__parent'];
            $parentDir     = ($mediaClass->dirExists($parentId)) ? $mediaClass->getDir($parentId) : array();

            $parentDirMaxLifetime          = (count($parentDir)) ? $parentDir['max_lifetime'] : 0;
            $parentDirMaxLifetimeTimeStamp = ($parentDirMaxLifetime !== 0 && (strtotime($parentDirMaxLifetime) !== false) && $Core->Helper()->isValidTimeStamp(strtotime($parentDirMaxLifetime))) ? strtotime($parentDirMaxLifetime) : 0;
            $parentDirAutoDeleteDate       = ($parentDirMaxLifetimeTimeStamp !== 0) ? date($Core->i18n()->getDateFormat(), $parentDirMaxLifetimeTimeStamp) : '';

            $dirMaxLifetime          = (array_key_exists('max_lifetime',$dir)) ? $dir['max_lifetime'] : 0;
            $dirMaxLifetimeTimeStamp = ($dirMaxLifetime !== 0 && (strtotime($dirMaxLifetime) !== false) && $Core->Helper()->isValidTimeStamp(strtotime($dirMaxLifetime))) ? strtotime($dirMaxLifetime) : 0;
            $dirAutoDeleteDate       = ($dirMaxLifetimeTimeStamp !== 0) ? date($Core->i18n()->getDateFormat(), $dirMaxLifetimeTimeStamp) : '';

            $mailerMails = (is_object($mailerClass)) ? $mailerClass->getMails($siteId, 0, null, null, 'asc', null, true) : array();
            $dirMails    = array();
            foreach($mailerMails as $mailId => $mail) {
                if(array_key_exists('id_media_dirs', $mail) && $mail['id_media_dirs'] == $dir['id']) {
                    $dirMails[$mailId] = $mail;
                }
            }
        ?>

        <?php if(!$dir['dirFolderExists'] && !$dir['backup']) : ?>
            <div class="alert alert-danger" role="alert"><?php echo $Core->i18n()->translate('Ordner nicht gefunden...') ?></div>
        <?php endif; ?>

        <?php if($dir['backup']) : ?>
            <div class="alert alert-warning" role="alert"><?php echo $Core->i18n()->translate('Archiviert') ?></div>
        <?php endif; ?>

        <?php if($dirAutoDeleteDate !== '' || $parentDirAutoDeleteDate !== '') : ?>
            <?php if($parentDirAutoDeleteDate !== '') : ?>
                <?php $showDate = ($dirMaxLifetimeTimeStamp !== 0 && ($dirMaxLifetimeTimeStamp < $parentDirMaxLifetimeTimeStamp)) ? $dirAutoDeleteDate : $parentDirAutoDeleteDate; ?>
                <div class="alert alert-warning" role="alert"><?php echo $Core->i18n()->translate('Übergeordneter Ordner ist bereits zur automatischen Löschung vorgesehen!') . ' (' . $parentDirAutoDeleteDate . ')<br /><i class="fa fa-clock-o pulse--red" aria-hidden="true"></i> ' . sprintf($Core->i18n()->translate('Dieser Ordner wird am %s automatisch gelöscht'), $showDate); ?></div>
            <?php endif; ?>
            <?php if($dirAutoDeleteDate !== '' && $parentDirAutoDeleteDate === '') : ?>
                <div class="alert alert-warning" role="alert"><?php echo '<i class="fa fa-clock-o pulse--red" aria-hidden="true"></i> ' . sprintf($Core->i18n()->translate('Dieser Ordner wird am %s automatisch gelöscht'), $dirAutoDeleteDate); ?></div>
            <?php endif; ?>
        <?php endif; ?>

        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-folder" aria-hidden="true"></i> <?php echo $Core->i18n()->translate('Name') ?></h3>
                    </div>
                    <div class="panel-body">
                        <?php echo $dir['name']; ?>
                        <?php if($dir['archive_state'] === 0) : ?>
                        <span title="<?php echo $Core->i18n()->translate('Ordner-Pfad') ?>">
                            (<i class="fa fa-home" aria-hidden="true"></i><?php echo $dir['dirPath']; ?>)
                        </span>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php if(is_object($ermClass) && $ermClass->eventExists($dir['id_events']) && $accClass->hasAccess('erm_events')) : ?>
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-calendar" aria-hidden="true"></i> <?php echo $Core->i18n()->translate('Event') ?></h3>
                    </div>
                    <div class="panel-body">
                        <?php
                            $event = $ermClass->getEvent($dir['id_events']);
                            echo $event['name_de'];
                        ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <?php if($dir['dirFolderExists']) : ?>
                <?php if($dir['archive_state'] === 0) : ?>
                    <?php if($countGalleryStyles) : ?>
                    <div class="col-sm-6">
                    <?php else: ?>
                    <div class="col-sm-12">
                    <?php endif; ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <i class="fa fa-th" aria-hidden="true"></i>
                                    <?php echo $Core->i18n()->translate('Galerie Url') ?>

                                    <?php if($isSu || $Mvc->useFileCache()): ?>
                                        <div class="btn-group pull-right">
                                        <span class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-cog" aria-hidden="true"></i> <span class="caret"></span>
                                        </span>
                                            <ul class="dropdown-menu">
                                                <?php if($Mvc->useFileCache()): ?>
                                                    <li>
                                                        <a href="#" class="clear-cache" data-dirfoldername="<?php echo $dir['dirPath']; ?>">
                                                            <?php echo $Core->i18n()->translate('Clear Gallery Cache') ?>
                                                        </a>
                                                    </li>
                                                <?php endif; ?>
                                                <?php if($isSu): ?>
                                                    <?php $foldercleanupUrl = $Mvc->getModelAjaxUrl() . '/dirs/foldercleanup' . str_replace(DS,'/',$dir['dirPath']); ?>
                                                    <li>
                                                        <a href="<?php echo $foldercleanupUrl; ?>" target="_blank">
                                                            <?php echo $Core->i18n()->translate('Folder Cleanup') ?>
                                                        </a>
                                                    </li>
                                                <?php endif; ?>
                                            </ul>
                                        </div>
                                    <?php endif; ?>
                                </h3>
                            </div>
                            <div class="panel-body">
                                <?php $galleryUrl = $Mvc->getModelUrl() . '/galleries/view' . str_replace(DS,'/',$dir['dirPath']); ?>
                                <a href="<?php echo $galleryUrl; ?>" target="_blank" title="<?php echo sprintf($Core->i18n()->translate('Galerie \'%s\' öffnen'), $dir['name']); ?>"><?php echo $galleryUrl; ?></a>
                                <?php echo (count($parentDir) && $parentDir['active'] && $dir['show_in_parent_gallery'] && $dir['active']) ? '<hr />' . $Core->i18n()->translate('Dieser Ordner wird in der Galerieansicht des übergeordneten Ordners angezeigt') : ''; ?>
                            </div>
                        </div>
                    </div>
                    <?php if($countGalleryStyles) : ?>
                        <div class="col-sm-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-paint-brush" aria-hidden="true"></i> <?php echo $Core->i18n()->translate('Galerie Stil') ?></h3>
                                </div>
                                <div class="panel-body">
                                    <?php
                                        $galleryStyleId   = (array_key_exists('id_media_galleries_styles',$dir) && (int)$dir['id_media_galleries_styles'] > 0) ? $dir['id_media_galleries_styles'] : 0;
                                        $galleryStyle     = ($countGalleryStyles && $mediaClass->galleryStyleExists($galleryStyleId)) ? $mediaClass->getGalleryStyle($galleryStyleId) : array();
                                        $galleryStyleName = (count($galleryStyle)) ? $galleryStyle['title'] : '--';

                                        echo $galleryStyleName;
                                    ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="clearfix"></div>
                <?php endif; ?>
                <?php if($dir['archive_state'] === 0) : ?>
                    <?php
                        $dirContents = (array_key_exists('dirContents', $dir) && is_array($dir['dirContents'])) ? $dir['dirContents'] : array();
                        $dirSize     = (array_key_exists('dirSize', $dir) && is_numeric($dir['dirSize'])) ? $dir['dirSize'] : 0;
                        if(!count($dirContents) && $mediaClass->getDirSize($dir['id'])) {
                            $dirContents = $mediaClass->getDirContents($dir['dirPath']);
                            $dirSize     = $mediaClass->getDirSize($dir['id']);
                        }
                    ?>
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-files-o" aria-hidden="true"></i> <?php echo $Core->i18n()->translate('Objekte') ?></h3>
                        </div>
                        <div class="panel-body">
                            <?php echo count($dirContents); ?> (<?php echo $mediaClass->formatBytes($dirSize); ?>)
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                <?php else: ?>
                <div class="col-sm-12">
                <?php endif; ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-line-chart" aria-hidden="true"></i> <?php echo $Core->i18n()->translate('Download-Zähler') ?></h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-6">
                                    <span data-toggle="tooltip" title="<?php echo $Core->i18n()->translate('Summe der Datei-Downloads dieses Ordners + der Unterordner') ?>">
                                        <i class="fa fa-folder" aria-hidden="true"></i>
                                        <?php echo $mediaClass->getAllFileDownloadsInclSubFolder($dir['id']); ?>
                                    </span>
                                </div>
                                <div class="col-xs-6">
                                    <span data-toggle="tooltip" title="<?php echo $Core->i18n()->translate('Summe der Datei-Downloads') ?>">
                                        <?php if($dir['fileDownloads']) : ?>
                                            <a data-toggle="collapse" href="#fdwCollapse" aria-expanded="false" aria-controls="fdwCollapse">
                                                <i class="fa fa-file" aria-hidden="true"></i>
                                                <?php echo $dir['fileDownloads']; ?>
                                            </a>
                                        <?php else : ?>
                                            <i class="fa fa-file" aria-hidden="true"></i>
                                            <?php echo $dir['fileDownloads']; ?>
                                        <?php endif; ?>
                                    </span>
                                </div>
                            </div>
                            <?php if($dir['fileDownloads']) : ?>
                                <?php
                                    $fileDownloadsArray = array();
                                    foreach($dir['dirContents'] as $fileName => $fileInfo) {
                                        $fileDownloadsArray[$fileName] = $mediaClass->getFileDownloads($dir['id'], $fileName);
                                    }
                                    arsort($fileDownloadsArray);
                                ?>
                            <div class="filedownloads-wrapper collapse" id="fdwCollapse">
                                <hr />
                                <table class="table table-condensed table-hover table-striped" data-datatable width="100%">
                                    <thead>
                                        <tr>
                                            <th><?php echo $Core->i18n()->translate('Dateiname') ?></th>
                                            <th><?php echo $Core->i18n()->translate('Downloads') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($fileDownloadsArray as $fileName => $fileDownloads) : ?>
                                        <tr>
                                            <td><?php echo $fileName; ?></td>
                                            <td><?php echo $fileDownloads; ?></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

            <?php if(count($dirMails)) : ?>

            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-envelope-o" aria-hidden="true"></i> <?php echo $Core->i18n()->translate('Sent Mails with Dir Link') ?> <span class="badge"><?php echo count($dirMails); ?></span></h3>
                    </div>
                    <div class="panel-body">
                        <a data-toggle="collapse" href="#dirMailsCollapse" aria-expanded="false" aria-controls="dirMailsCollapse">
                            <?php echo $Core->i18n()->translate('show') ?>
                        </a>
                        <div class="dirMails-wrapper collapse" id="dirMailsCollapse">
                            <hr />
                            <table class="table table-condensed table-hover table-striped" data-datatable width="100%">
                                <thead>
                                <tr>
                                    <th><?php echo $Core->i18n()->translate('Von') ?></th>
                                    <th><?php echo $Core->i18n()->translate('An') ?></th>
                                    <th><?php echo $Core->i18n()->translate('Gesendet') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($dirMails as $mailId => $mail) : ?>
                                    <?php
                                        $sendDateTime    = date("d.m.Y", strtotime($mail['created'])) . ' ' . $Core->i18n()->translate('um') . ' ' . date("H:i:s", strtotime($mail['created'])) . ' ' . $Core->i18n()->translate('Uhr');
                                        $sendDate        = date("d.m.Y", strtotime($mail['created']));

                                        $fromMailAddress = (is_array($mail['from']) && array_key_exists(0,$mail['from']) && $mail['from'][0] != '') ? $mail['from'][0] : '';
                                        $fromMailName    = (is_array($mail['from']) && array_key_exists(1,$mail['from']) && $mail['from'][1] != '') ? $mail['from'][1] : '';

                                        $fromTitle       = ($fromMailName != '') ? '&quot;' . $fromMailName . '&quot; &lt;' . $fromMailAddress . '&gt;' : $fromMailAddress;
                                        $from            = ($fromMailName != '') ? '<span title="' . $fromTitle . '">' . $fromMailName . '</span>' : $fromMailAddress;

                                        $to              = (strlen(trim($mail['to'], ',')) > 45) ? '<span title="' . trim($mail['to'], ',') . '">' . substr(trim($mail['to'], ','), 0, 42) . '...</span>' : trim($mail['to'], ',');
                                    ?>
                                    <tr>
                                        <td><?php echo $from; ?></td>
                                        <td><?php echo $to; ?></td>
                                        <td><span title="<?php echo $sendDateTime ?>"><?php echo  $sendDate ?></span></td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
    <?php else: ?>
        <div class="alert alert-danger" role="alert"><?php echo $Core->i18n()->translate('Ordner nicht gefunden...') ?></div>
    <?php endif; ?>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Schließen'); ?></button>
</div>