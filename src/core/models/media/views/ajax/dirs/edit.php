<?php
/**
 * Media Model Ajax View Dirs Edit
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $accClass Acc
 * @var $plugins Plugins
 * @var $pitsForms PitsForms
 * @var $pitsCore PitsCore
 */

/** Prepare Form Plugin */
$plugins = $Core->Plugins();
$pitsForms = $plugins->PitsForms();
$isForms = (is_object($pitsForms));
$pitsCore = $plugins->PitsCore();

$btnActionName = ($isForms) ? $pitsCore->xorEnc('action') : 'action';

$dirSaveUrl = $Mvc->getModelUrl() . '/dirs/save';
$dirFound   = (count($dir)) ? true : false;
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">
        <?php echo $Core->i18n()->translate('Ordner bearbeiten') ?>
    </h4>
</div>
<?php if($dirFound) : ?>
    <?php
        $parentId      = $dir['id_media_dirs__parent'];
        $parentDir     = ($mediaClass->dirExists($parentId)) ? $mediaClass->getDir($parentId) : array();
        $parentDirPass = (count($parentDir)) ? $parentDir['password'] : $dir['password'];
        $parentDirPath = (count($parentDir)) ? $parentDir['dirPath']  : DS;

        $password      = $parentDirPass;

        $site = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $styles = $mediaClass->getGalleryStyles($siteId);

        $dirs2stylesChooserArray = array();
        $dirs2stylesChooserArray[''] = $Core->i18n()->translate('Galerie Stil auswählen');
        foreach($styles as $style) {
            $dirs2stylesChooserArray[$style['id']] = $style['title'];
        }
    ?>
<form id="edit_form" class="form-horizontal" action="<?php echo $dirSaveUrl ?>" method="post">
    <?php
    /** Hidden Id Element */
    $formElementData['eleType']       = 'input';
    $formElementData['name']          = 'id';
    $formElementData['label']         = false;
    $formElementData['value']         = $dir['id'];
    $formElementData['type']         = 'hidden';
    echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
    ?>
    <?php
    /** Hidden Id Element */
    $formElementData['eleType']       = 'input';
    $formElementData['id']            = '';
    $formElementData['name']          = 'id_media_dirs__parent';
    $formElementData['label']         = false;
    $formElementData['value']         = (count($parentDir)) ? $parentDir['id'] : 0;
    $formElementData['type']          = 'hidden';
    $formElementData['isRequired']    = false;
    $formElementData['ownAttributes'] = array();
    echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
    ?>
    <div class="modal-body">

        <div class="form-group">
            <label for="name" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Name') ?> <em>*</em></label>

            <div class="col-md-10">
                <?php
                /** Name Element */
                $formElementData['eleType']       = 'input';
                $formElementData['id']            = 'name';
                $formElementData['name']          = 'name';
                $formElementData['label']         = false;
                $formElementData['value']         = $dir['name'];
                $formElementData['type']          = 'text';
                $formElementData['isRequired']    = true;
                $formElementData['ownAttributes'] = array('placeholder' => $Core->i18n()->translate('Ordner-Name'));
                echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                ?>
            </div>
        </div>

        <div class="form-group">
            <label for="password" class="col-md-2 control-label whitespace-nowrap">
                <?php echo $Core->i18n()->translate('Passwort') ?>
                <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="<?php echo $Core->i18n()->translate('Falls Passwort leer, wird das des übergeordneten Ordners verwendet oder ein automatisches gesetzt!') ?>"></i>
            </label>

            <div class="col-md-10">
                <div class="input-group">
                    <?php
                    /** Password Element */
                    $formElementData['eleType']       = 'input';
                    $formElementData['id']            = 'password';
                    $formElementData['name']          = 'password';
                    $formElementData['label']         = false;
                    $formElementData['value']         = $dir['password'];
                    $formElementData['type']          = 'text';
                    $formElementData['isRequired']    = false;
                    $formElementData['ownAttributes'] = array('placeholder' => $password);
                    echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                    ?>
                    <div class="input-group-addon">
                        <span data-toggle="tooltip" title="<?php echo $Core->i18n()->translate('Öffentlicher Ordner?'); ?>">
                            <i class="fa fa-globe" aria-hidden="true"></i>
                            <?php
                            /** Public Element */
                            $formElementData['eleType']        = 'checkbox';
                            $formElementData['id']             = 'public';
                            $formElementData['name']           = 'public';
                            $formElementData['label']          = false;
                            $formElementData['value']          = $dir['public'];
                            $formElementData['valueChecked']   = 1;
                            $formElementData['valueUnchecked'] = 0;
                            $formElementData['checkboxOnly']   = true;
                            echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                            ?>
                        </span>
                    </div>
                </div>
                <span class="help-block">
                    <?php echo $Core->i18n()->translate('Eine Passwortänderung wirkt sich nur auf diesen Ordner aus.') ?>
                    <br />
                    <?php echo $Core->i18n()->translate('Falls der Ordner auf öffentlich gesetzt ist, wird kein Passwort abgefragt.') ?>
                </span>
            </div>
        </div>

        <?php if(count($styles)) : ?>
        <div class="form-group">
            <label for="name" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Galerie Stil') ?> <em>*</em></label>

            <div class="col-md-10">
                <?php
                /** Styles Element */
                $formElementData['eleType']       = 'select';
                $formElementData['id']            = 'id_media_galleries_styles';
                $formElementData['name']          = 'id_media_galleries_styles';
                $formElementData['label']         = false;
                $formElementData['isRequired']    = false;
                $formElementData['value']         = $dir['id_media_galleries_styles'];
                $formElementData['allValue']      = $dirs2stylesChooserArray;
                echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                ?>
            </div>
        </div>
        <?php endif; ?>

        <?php if(count($parentDir)) : ?>
            <div class="form-group">
                <label for="show_in_parent_gallery" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('In übergeordneter Galerie sichtbar') ?></label>
                <div class="col-md-10">
                    <div class="togglebutton">
                        <label>
                            <?php
                            /** Active Element */
                            $formElementData['eleType']        = 'checkbox';
                            $formElementData['id']             = 'show_in_parent_gallery';
                            $formElementData['name']           = 'show_in_parent_gallery';
                            $formElementData['label']          = false;
                            $formElementData['value']          = $dir['show_in_parent_gallery'];
                            $formElementData['valueChecked']   = '1';
                            $formElementData['valueUnchecked'] = '0';
                            $formElementData['checkboxOnly']  = true;
                            echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                            ?>
                        </label>
                    </div>
                    <span class="help-block"><?php echo $Core->i18n()->translate('Diesen Ordner in der Galerieansicht des übergeordneten Ordners anzeigen') ?></span>
                </div>
            </div>
        <?php endif; ?>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Abbrechen'); ?></button>
        <button type="submit" class="btn btn-success" name="<?php echo $btnActionName; ?>" value="dirs_edit" data-action="dirs_edit"><?php echo $Core->i18n()->translate('Speichern'); ?></button>
    </div>
</form>
<?php else: ?>
    <div class="modal-body">
        <div class="alert alert-danger" role="alert"><?php echo $Core->i18n()->translate('Ordner nicht gefunden...') ?></div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Schließen'); ?></button>
    </div>
<?php endif; ?>