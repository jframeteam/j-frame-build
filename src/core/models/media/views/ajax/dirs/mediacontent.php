<?php
/**
 * Media Model Ajax View Dirs MediaContent
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $mediaClass Media
 * @var $pitsForms PitsForms
 * @var $pitsCore PitsCore
 */

/** Prepare Form Plugin */
$plugins = $Core->Plugins();
$pitsForms = $plugins->PitsForms();
$isForms = (is_object($pitsForms));
$pitsCore = $plugins->PitsCore();

$dirSaveUrl = $Mvc->getModelUrl() . '/dirs/save';
$incUrl     = $Core->getCoreUrl() . '/models/media/inc';
$dirFound   = (count($dir)) ? true : false;

if($dirFound) : ?>
    <?php
        $dirContents = (array_key_exists('dirContents', $dir) && is_array($dir['dirContents'])) ? $dir['dirContents'] : array();
        if(!count($dirContents) && $mediaClass->getDirSize($dir['id'])) {
            $dirContents = $mediaClass->getDirContents($dir['dirPath']);
        }
    ?>
    <?php // $dirUrl = $mediaClass->getDirUrl($dir['dir']); ?>
    <div class="row">
        <?php $fi=0; ksort($dirContents); foreach($dirContents as $fileFullName => $file) : ?>
            <?php
                $mimeIcon = '<i class="fa fa-file" aria-hidden="true"></i>';
                $thPreview = $mimeIcon;
                
                $fileMime = (array_key_exists('mime',$file)) ? $file['mime'] : '';
                $fileName = (array_key_exists('name',$file)) ? $file['name'] : '';
                $fileSize = (array_key_exists('size',$file)) ? $file['size'] : 0;
                $fileUrl  = (array_key_exists('fileUrl',$file)) ? $file['fileUrl'] : '';

                if($fileFullName == '.thinfo') { continue; }

                $isVideo = false;
                $isAudio = false;

                $isImage = false;
                if(strpos($fileMime, 'image/') !== false) {

                    $mimeIcon = '<i class="fa fa-file-image-o" aria-hidden="true"></i>';
                    $isImage = true;
                    if(
                        array_key_exists('thumbnail', $file) &&
                        is_array($file['thumbnail']) &&
                        count($file['thumbnail']) &&
                        array_key_exists('fileUrl', $file['thumbnail'])
                    ) {
                        $thPreview = '<img class="lazy loader" data-src="' . $file['thumbnail']['fileUrl'] . '" alt="">';
                    } else {
                        $thPreview = '<img class="lazy loader" data-src="' . $file['fileUrl'] . '" alt="">';
                    }
                }

                if(strpos($fileMime, 'video/') !== false) {
                    $mimeIcon = '<i class="fa fa-file-video-o" aria-hidden="true"></i>';
                    $thPreview = $mimeIcon;

                    $isVideo = true;
                }

                if(strpos($fileMime, 'audio/') !== false) {
                    $mimeIcon = '<i class="fa fa-file-audio-o" aria-hidden="true"></i>';
                    $thPreview = $mimeIcon;

                    $isAudio = true;
                }

                if(
                    $fileMime == 'application/msword' ||
                    $fileMime == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                ) {
                    $mimeIcon = '<i class="fa fa-file-word-o" aria-hidden="true"></i>';
                    $thPreview = $mimeIcon;
                }

                if(
                    $fileMime == 'application/msexcel' ||
                    $fileMime == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                ) {
                    $mimeIcon = '<i class="fa fa-file-excel-o" aria-hidden="true"></i>';
                    $thPreview = $mimeIcon;
                }

                if($fileMime == 'application/pdf') {
                    $mimeIcon = '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>';
                    $thPreview = $mimeIcon;
                }

                if($fileMime == 'application/zip') {
                    $mimeIcon = '<i class="fa fa-file-archive-o" aria-hidden="true"></i>';
                    $thPreview = $mimeIcon;
                }

                if(!$isImage) {
                    $preview = '<span class="media-preview">' . $thPreview . '</span>';
                    $thPreview = $preview;
                }

            ?>
            <div class="col-xs-6 col-sm-4 col-md-3 thumbnail-wrapper">
                <div class="thumbnail">
                    <div class="togglebutton">
                        <label>
                            <?php
                            /** Right Element */
                            $formElementData['eleType']        = 'checkbox';
                            $formElementData['id']             = '';
                            $formElementData['name']           = 'download_files[]';
                            $formElementData['label']          = false;
                            $formElementData['value']          = '';
                            $formElementData['valueChecked']   = $fileFullName;
                            $formElementData['valueUnchecked'] = '';
                            $formElementData['checkboxOnly']   = true;
                            echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                            ?>
                        </label>
                    </div>
                    <?php echo $thPreview; ?>
                    <div class="caption">
                        <h4 class="one-line-ellipsis" title="<?php echo $fileFullName; ?>">
                            <?php echo $fileName; ?>
                        </h4>
                        <?php if($isVideo || $isAudio) : ?>
                        <?php endif; ?>
                        <div class="row">
                            <div class="col-sm-5">
                                <strong><?php echo $Core->i18n()->translate('Dateigröße'); ?>:</strong>
                            </div>
                            <div class="col-sm-7">
                                <?php echo $mediaClass->formatBytes($fileSize); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-5">
                                <strong><?php echo $Core->i18n()->translate('Dateityp'); ?>:</strong>
                            </div>
                            <div class="col-sm-7">
                                <span title="<?php echo $fileMime; ?>"><?php echo $mimeIcon; ?></span>
                            </div>
                        </div>
                        <hr />
                        <div class="row">
                            <?php if($dir['name'] != '_galleries_styles_logos' || !$isImage) : ?>
                            <div class="col-xs-6">
                                <a href="<?php echo $fileUrl; ?>" target="_blank" class="btn btn-default btn-sm btn-block show-media" role="button" title="<?php echo sprintf($Core->i18n()->translate('Datei \'%s\' im neuen Fenster öffnen'), $fileFullName); ?>">
                                    <i class="fa fa-external-link" aria-hidden="true"></i>
                                </a>
                            </div>
                            <div class="col-xs-6">
                                <button type="button" class="btn btn-danger btn-sm btn-block delete-media" role="button"
                                        data-dirfoldername="<?php echo trim($dir['dirPath'],DS); ?>"
                                        data-filefullname="<?php echo $fileFullName; ?>"
                                        title="<?php echo sprintf($Core->i18n()->translate('Datei \'%s\' löschen'), $fileFullName); ?>"
                                >
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </button>
                            </div>
                            <?php else: ?>
                                <div class="col-xs-4">
                                    <a href="<?php echo $file['fileUrl']; ?>" target="_blank" class="btn btn-default btn-sm btn-block show-media" role="button" title="<?php echo sprintf($Core->i18n()->translate('Datei \'%s\' im neuen Fenster öffnen'), $fileFullName); ?>">
                                        <i class="fa fa-external-link" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <div class="col-xs-4">
                                    <button type="button" class="btn btn-danger btn-sm btn-block delete-media" role="button"
                                            data-dirfoldername="<?php echo trim($dir['dirPath'],DS); ?>"
                                            data-filefullname="<?php echo $fileFullName; ?>"
                                            title="<?php echo sprintf($Core->i18n()->translate('Datei \'%s\' löschen'), $fileFullName); ?>"
                                    >
                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                    </button>
                                </div>
                                <div class="col-xs-4">
                                    <button type="button" class="btn btn-success btn-sm btn-block insert-media" role="button"
                                            data-dirfoldername="<?php echo trim($dir['dirPath'],DS); ?>"
                                            data-filefullname="<?php echo $fileFullName; ?>"
                                            title="<?php echo sprintf($Core->i18n()->translate('Datei \'%s\' einfügen'), $fileFullName); ?>"
                                    >
                                        <i class="fa fa-arrow-down" aria-hidden="true"></i>
                                    </button>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php if($isVideo || $isAudio) : ?>
                        <div class="media-player--wrapper<?php if($isAudio) : ?> mp-audio<?php endif; ?>">
                            <div class="media-player--element" id="mp_<?php echo $dir['id'] . '-' . $fi; ?>">
                                <?php if($isVideo) : ?>
                                    <video id="mf_<?php echo $dir['id'] . '-' . $fi; ?>" class="video-js" data-setup='{ "controls": true, "autoplay": false, "preload": "auto" }' controls>
                                        <source src="<?php echo $fileUrl; ?>" type="<?php echo $fileMime; ?>">
                                    </video>
                                <?php endif; ?>
                                <?php if($isAudio) : ?>
                                    <audio id="mf_<?php echo $dir['id'] . '-' . $fi; ?>" class="video-js" data-setup='{ "controlBar": { "fullscreenToggle": false } }' controls>
                                        <source src="<?php echo $fileUrl; ?>" type="<?php echo $fileMime; ?>">
                                    </audio>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="clearfix"></div>
                </div>
            </div>
        <?php $fi++; endforeach; ?>
    </div>
<?php else: ?>
    <div class="alert alert-warning" role="alert"><?php echo $Core->i18n()->translate('Noch keine Dateien in diesem Ordner...'); ?></div>
<?php endif; ?>
