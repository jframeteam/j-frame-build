<?php
/**
 * Media Model Ajax View Dirs Create
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $accClass Acc
 * @var $plugins Plugins
 * @var $pitsForms PitsForms
 * @var $pitsCore PitsCore
 */

/** Prepare Form Plugin */
$plugins = $Core->Plugins();
$pitsForms = $plugins->PitsForms();
$isForms = (is_object($pitsForms));
$pitsCore = $plugins->PitsCore();

$canAutoDelete = (!is_object($accClass) || $accClass->hasAccess('media_dirs_autodelete'));

$btnActionName = ($isForms) ? $pitsCore->xorEnc('action') : 'action';

$dirSaveUrl = $Mvc->getModelUrl() . '/dirs/save/';
$password   = $mediaClass->generatePassword();

$parentDir  = (isset($parentDir)) ? $parentDir : array();

$parentDirPass = (count($parentDir)) ? $parentDir['password'] : $mediaClass->generatePassword();
$parentDirPath = (count($parentDir)) ? $parentDir['dirPath']  : DS;

$parentDirMaxLifetime          = (count($parentDir)) ? $parentDir['max_lifetime'] : 0;
$parentDirMaxLifetimeTimeStamp = ($parentDirMaxLifetime !== 0 && (strtotime($parentDirMaxLifetime) !== false) && $Core->Helper()->isValidTimeStamp(strtotime($parentDirMaxLifetime))) ? strtotime($parentDirMaxLifetime) : 0;
$parentDirAutoDeleteDate       = ($parentDirMaxLifetimeTimeStamp !== 0) ? date($Core->i18n()->getDateFormat(), $parentDirMaxLifetimeTimeStamp) : '';

$password   = $parentDirPass;

$parentDirGalleryStyleId = (count($parentDir)) ? $parentDir['id_media_galleries_styles']  : 0;
$galleryStyleId = $parentDirGalleryStyleId;

$site = $Core->Sites()->getSite();
$siteId = (count($site)) ? $site['id'] : 0;

$styles = $mediaClass->getGalleryStyles($siteId);

$dirs2stylesChooserArray = array();
$dirs2stylesChooserArray[''] = $Core->i18n()->translate('Galerie Stil auswählen');
foreach($styles as $style) {
    $dirs2stylesChooserArray[$style['id']] = $style['title'];
}
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">
        <?php echo $Core->i18n()->translate('Ordner erstellen') ?>
    </h4>
</div>
<form id="create_form" class="form-horizontal" action="<?php echo $dirSaveUrl ?>" method="post">
    <?php
    /** Hidden Id Element */
    $formElementData['eleType']       = 'input';
    $formElementData['id']            = '';
    $formElementData['name']          = 'id_media_dirs__parent';
    $formElementData['label']         = false;
    $formElementData['value']         = (count($parentDir)) ? $parentDir['id'] : 0;
    $formElementData['type']          = 'hidden';
    $formElementData['isRequired']    = false;
    $formElementData['ownAttributes'] = array();
    echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
    ?>
    <div class="modal-body">

        <?php if($parentDirAutoDeleteDate !== '') : ?>
            <div class="alert alert-warning" role="alert"><i class="fa fa-clock-o pulse--red" aria-hidden="true"></i> <?php echo $Core->i18n()->translate('Übergeordneter Ordner ist bereits zur automatischen Löschung vorgesehen!') . ' ('.$parentDirAutoDeleteDate.')'; ?></div>
        <?php endif; ?>

        <div class="form-group">
            <label for="name" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Name') ?> <em>*</em></label>

            <div class="col-md-10">
                <div class="input-group">
                    <div class="input-group-addon"><i class="fa fa-home" aria-hidden="true"></i><?php echo $parentDirPath; ?></div>
                    <?php
                    /** Name Element */
                    $formElementData['eleType']       = 'input';
                    $formElementData['id']            = 'name';
                    $formElementData['name']          = 'name';
                    $formElementData['label']         = false;
                    $formElementData['value']         = '';
                    $formElementData['type']          = 'text';
                    $formElementData['isRequired']    = true;
                    $formElementData['ownAttributes'] = array('placeholder' => $Core->i18n()->translate('Ordner-Name'));
                    echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                    ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="password" class="col-md-2 control-label whitespace-nowrap">
                <?php echo $Core->i18n()->translate('Passwort') ?>
                <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" title="<?php echo $Core->i18n()->translate('Falls Passwort leer, wird das des übergeordneten Ordners verwendet oder ein automatisches gesetzt!') ?>"></i>
            </label>

            <div class="col-md-10">
                <div class="input-group">
                    <?php
                    /** Password Element */
                    $formElementData['eleType']       = 'input';
                    $formElementData['id']            = 'password';
                    $formElementData['name']          = 'password';
                    $formElementData['label']         = false;
                    $formElementData['value']         = $password;
                    $formElementData['type']          = 'text';
                    $formElementData['isRequired']    = false;
                    $formElementData['ownAttributes'] = array('placeholder' => $password);
                    echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                    ?>
                    <div class="input-group-addon">
                        <span data-toggle="tooltip" title="<?php echo $Core->i18n()->translate('Öffentlicher Ordner?'); ?>">
                            <i class="fa fa-globe" aria-hidden="true"></i>
                            <?php
                            /** Public Element */
                            $formElementData['eleType']        = 'checkbox';
                            $formElementData['id']             = 'public';
                            $formElementData['name']           = 'public';
                            $formElementData['label']          = false;
                            $formElementData['value']          = 0;
                            $formElementData['valueChecked']   = 1;
                            $formElementData['valueUnchecked'] = 0;
                            $formElementData['checkboxOnly']   = true;
                            echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                            ?>
                        </span>
                    </div>
                </div>
                <span class="help-block"><?php echo $Core->i18n()->translate('Falls der Ordner auf öffentlich gesetzt ist, wird kein Passwort abgefragt.') ?></span>
            </div>
        </div>

        <?php if($canAutoDelete) : ?>
            <div class="form-group">
                <label for="name" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Automatische Löschung') ?></label>

                <div class="col-md-10">
                    <?php
                        $max_lifetime_value = $parentDirAutoDeleteDate;

                        $endDate = ($parentDirAutoDeleteDate !== '') ? $parentDirAutoDeleteDate : '';

                        $endDateAttr = ($endDate !== '') ? ' data-end="' . $endDate . '"' : '';
                    ?>
                    <div class="input-group datepicker date" data-start="tomorrow"<?php echo $endDateAttr; ?>>
                        <?php
                        /** Styles Element */
                        $formElementData['eleType']       = 'input';
                        $formElementData['id']            = 'max_lifetime';
                        $formElementData['name']          = 'max_lifetime';
                        $formElementData['label']         = false;
                        $formElementData['type']          = 'datetime';
                        $formElementData['isRequired']    = false;
                        $formElementData['value']         = $max_lifetime_value;
                        $formElementData['ownAttributes'] = array(
                            'placeholder' => $Core->i18n()->translate('Datum der automatischen Löschung')
                        );
                        $formElementData['cssClasses']    = ' date';
                        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                        ?>
                        <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                    </div>
                    <span class="help-block"><?php echo $Core->i18n()->translate('Falls hier ein Datum gesetzt ist, wird dieser Ordner inklusive aller Unterordner an diesem Datum gelöscht.') ?></span>
                </div>
            </div>
        <?php endif; ?>

        <?php if(count($styles)) : ?>
            <div class="form-group">
                <label for="name" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Galerie Stil') ?> <em>*</em></label>

                <div class="col-md-10">
                    <?php
                    /** Styles Element */
                    $formElementData['eleType']       = 'select';
                    $formElementData['id']            = 'id_media_galleries_styles';
                    $formElementData['name']          = 'id_media_galleries_styles';
                    $formElementData['label']         = false;
                    $formElementData['isRequired']    = false;
                    $formElementData['value']         = $parentDirGalleryStyleId;
                    $formElementData['allValue']      = $dirs2stylesChooserArray;
                    echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                    ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if(count($parentDir)) : ?>
        <div class="form-group">
            <label for="show_in_parent_gallery" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('In übergeordneter Galerie sichtbar') ?></label>
            <div class="col-md-10">
                <div class="togglebutton">
                    <label>
                        <?php
                        /** Active Element */
                        $formElementData['eleType']        = 'checkbox';
                        $formElementData['id']             = 'show_in_parent_gallery';
                        $formElementData['name']           = 'show_in_parent_gallery';
                        $formElementData['label']          = false;
                        $formElementData['value']          = '0';
                        $formElementData['valueChecked']   = '1';
                        $formElementData['valueUnchecked'] = '0';
                        $formElementData['checkboxOnly']  = true;
                        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                        ?>
                    </label>
                </div>
                <span class="help-block"><?php echo $Core->i18n()->translate('Diesen Ordner in der Galerieansicht des übergeordneten Ordners anzeigen') ?></span>
            </div>
        </div>
        <?php endif; ?>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Abbrechen'); ?></button>
        <button type="submit" class="btn btn-success" name="<?php echo $btnActionName; ?>" value="dirs_create" data-action="dirs_create"><?php echo $Core->i18n()->translate('Speichern'); ?></button>
    </div>
</form>
