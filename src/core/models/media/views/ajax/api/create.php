<?php
/**
 * Media Model Ajax View API Create
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $accClass Acc
 * @var $plugins Plugins
 * @var $pitsForms PitsForms
 * @var $pitsCore PitsCore
 */

/** Prepare Form Plugin */
$plugins = $Core->Plugins();
$pitsForms = $plugins->PitsForms();
$isForms = (is_object($pitsForms));
$pitsCore = $plugins->PitsCore();

$hiddenPasswordName = ($isForms) ? $pitsCore->xorEnc('password') : 'password';
$btnActionName = ($isForms) ? $pitsCore->xorEnc('action') : 'action';

$apiUserSaveUrl = $Mvc->getModelUrl() . '/api/save/';
$password   = $mediaClass->generatePassword();

$usersSelectArray = array();

$curUser = (is_object($accClass)) ? $accClass->getUser() : array();
$curUserIsSU = (array_key_exists('su', $curUser) && $curUser['su'] == 1) ? true : false;

$users = (is_object($accClass)) ? $accClass->getUsers() : array();

$usersSelectArray[''] = $Core->i18n()->translate('User für API-Zugang auswählen');
foreach($users as $userEntryId => $userEntry) {
    $ueIsActive = (array_key_exists('active', $userEntry) && $userEntry['active'] == 1) ? true : false;
    $ueIsSU = (array_key_exists('su', $userEntry) && $userEntry['su'] == 1) ? true : false;

    /** Check if user already exists as API User or is inactive */
    $getApiUserByUserId = $mediaClass->getApiUserByUserId($userEntryId);
    if((is_array($getApiUserByUserId) && count($getApiUserByUserId)) || !$ueIsActive) { continue; }
    if($ueIsSU && !$curUserIsSU) { continue; }

    $ueName = (array_key_exists('name', $userEntry)) ? $userEntry['name'] : '-';
    $ueSurname = (array_key_exists('surname', $userEntry)) ? $userEntry['surname'] : '-';
    $ueFullName = $ueName . ' ' . $ueSurname;

    $ueApiKey = $mediaClass->createApiKey($userEntryId);

    $usersSelectArray[$userEntryId] = $ueFullName;
}
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">
        <?php echo $Core->i18n()->translate('API-Zugang erstellen') ?>
    </h4>
</div>
<form id="create_form" class="form-horizontal" action="<?php echo $apiUserSaveUrl ?>" method="post">
    <div class="modal-body">

        <div class="form-group">
            <label for="id_acc_users" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('User') ?> <em>*</em></label>

            <div class="col-md-10" id="apiUserChooserWrapper">
                <div class="row">
                    <div class="col-xs-9">
                        <?php
                        /** User Element */
                        $formElementData['eleType']       = 'select';
                        $formElementData['id']            = 'id_acc_users';
                        $formElementData['name']          = 'id_acc_users';
                        $formElementData['label']         = false;
                        $formElementData['value']         = '';
                        $formElementData['allValue']      = $usersSelectArray;
                        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                        ?>
                    </div>
                    <div class="col-xs-3">
                        <div class="togglebutton">
                            <label>
                                <?php
                                /** Active Element */
                                $formElementData['eleType']        = 'checkbox';
                                $formElementData['id']             = '';
                                $formElementData['name']           = 'active';
                                $formElementData['label']          = false;
                                $formElementData['value']          = '1';
                                $formElementData['valueChecked']   = '1';
                                $formElementData['valueUnchecked'] = '0';
                                $formElementData['checkboxOnly']   = true;
                                echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                                ?>
                                <?php echo $Core->i18n()->translate('Aktiv') ?>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="api_key" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('API Key') ?> <em>*</em></label>

            <div class="col-md-10">
                <?php
                /** Api Key Element */
                $formElementData['eleType']       = 'input';
                $formElementData['id']            = 'api_key';
                $formElementData['name']          = 'api_key';
                $formElementData['label']         = false;
                $formElementData['value']         = '';
                $formElementData['type']          = 'text';
                $formElementData['isRequired']    = true;
                $formElementData['ownAttributes'] = array();
                echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                ?>
            </div>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Abbrechen'); ?></button>
        <button type="submit" class="btn btn-success" name="<?php echo $btnActionName; ?>" value="api_create" data-action="api_create"><?php echo $Core->i18n()->translate('Speichern'); ?></button>
    </div>
</form>
