<?php
/**
 * Media Model Ajax View API Edit
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $accClass Acc
 * @var $plugins Plugins
 * @var $pitsForms PitsForms
 * @var $pitsCore PitsCore
 */

/** Prepare Form Plugin */
$plugins = $Core->Plugins();
$pitsForms = $plugins->PitsForms();
$isForms = (is_object($pitsForms));
$pitsCore = $plugins->PitsCore();

$curUser = (is_object($accClass)) ? $accClass->getUser() : array();
$curUserIsSU = (array_key_exists('su', $curUser) && $curUser['su'] == 1) ? true : false;

$btnActionName = ($isForms) ? $pitsCore->xorEnc('action') : 'action';

$apiUserSaveUrl = $Mvc->getModelUrl() . '/api/save/';

$apiUserEntry = (isset($apiUserEntry)) ? $apiUserEntry : array();
?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">
            <?php echo $Core->i18n()->translate('API-Zugang Bearbeiten') ?>
        </h4>
    </div>

<?php if(count($apiUserEntry)) : ?>
<?php $apiUser = (is_object($accClass) && $accClass->userExists($apiUserEntry['id_acc_users'])) ? $accClass->getUser($apiUserEntry['id_acc_users']) : array(); ?>
<?php if(count($apiUser)) : ?>
    <form id="create_form" class="form-horizontal" action="<?php echo $apiUserSaveUrl ?>" method="post">
        <?php
        /** Hidden Id Element */
        $formElementData['eleType']       = 'input';
        $formElementData['name']          = 'id';
        $formElementData['label']         = false;
        $formElementData['value']         = $apiUserEntry['id'];
        $formElementData['type']         = 'hidden';
        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
        ?>

        <div class="modal-body">

            <div class="form-group">
                <label for="id_acc_users" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('User') ?> <em>*</em></label>

                <div class="col-md-10" id="apiUserChooserWrapper">
                    <div class="row">
                        <div class="col-xs-9">
                            <p class="form-control-static"><?php echo $apiUser['name']; ?> <?php echo $apiUser['surname']; ?></p>
                        </div>
                        <div class="col-xs-3">
                            <div class="togglebutton">
                                <label>
                                    <?php
                                    /** Active Element */
                                    $formElementData['eleType']        = 'checkbox';
                                    $formElementData['id']             = '';
                                    $formElementData['name']           = 'active';
                                    $formElementData['label']          = false;
                                    $formElementData['value']          = $apiUserEntry['active'];
                                    $formElementData['valueChecked']   = '1';
                                    $formElementData['valueUnchecked'] = '0';
                                    $formElementData['checkboxOnly']   = true;
                                    echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                                    ?>
                                    <?php echo $Core->i18n()->translate('Aktiv') ?>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="api_key" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('API Key') ?> <em>*</em></label>

                <div class="col-md-10">
                    <?php
                    /** Api Key Element */
                    $formElementData['eleType']       = 'input';
                    $formElementData['id']            = 'api_key';
                    $formElementData['name']          = 'api_key';
                    $formElementData['label']         = false;
                    $formElementData['value']         = $apiUserEntry['api_key'];
                    $formElementData['type']          = 'text';
                    $formElementData['isRequired']    = true;
                    $formElementData['ownAttributes'] = array();
                    echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                    ?>
                </div>
            </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Abbrechen'); ?></button>
            <button type="submit" class="btn btn-success" name="<?php echo $btnActionName; ?>" value="api_create" data-action="api_create"><?php echo $Core->i18n()->translate('Speichern'); ?></button>
        </div>
    </form>
<?php else : ?>
    <div class="modal-body">
        <div class="alert alert-danger" role="alert"><?php echo $Core->i18n()->translate('User zum API-Zugang nicht gefunden'); ?></div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Abbrechen'); ?></button>
    </div>
<?php endif; ?>
<?php else : ?>
    <div class="modal-body">
        <div class="alert alert-danger" role="alert"><?php echo $Core->i18n()->translate('API-Zugang nicht gefunden'); ?></div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Abbrechen'); ?></button>
    </div>
<?php endif; ?>