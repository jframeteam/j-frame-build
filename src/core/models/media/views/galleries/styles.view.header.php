<?php
/**
 * Media Model View Galleries View Styles Header
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $mediaClass Media
 */

$site = $Core->Sites()->getSite();

$galleryStyleId = (isset($galleryStyleId)) ? $galleryStyleId : 0;

$style = ($mediaClass->galleryStyleExists($galleryStyleId)) ? $mediaClass->getGalleryStyle($galleryStyleId) : array();

if(count($style) && $style['active'] == 1) : ?>
<style>
    body {
        background-color: <?php echo $style['color_page_bg']; ?>;
    }
    .style-header-wrapper {
        padding: 15px 0;
        background-color: <?php echo $style['color_header_bg']; ?>;
        color: <?php echo $style['color_header_txt']; ?>;
    }
    .style-header-wrapper img {
        margin-bottom: 15px;
    }
    .style-header-wrapper p {
        margin-bottom: 0;
    }
    .content {
        margin: 15px 0;
        padding: 0 15px;
        background-color: <?php echo $style['color_content_bg']; ?>;
        color: <?php echo $style['color_content_txt']; ?>;
    }
    .content .thumbnail-wrapper .thumbnail,
    .content .thumbnail-wrapper .thumbnail .media-filename{
        background-color: <?php echo $style['color_thumbnail_bg']; ?>;
        color: <?php echo $style['color_thumbnail_txt']; ?>;
    }
    .content .media-content .thumbnail-wrapper.folder-link .thumbnail .media-filename {
        position: static;
    }
</style>
<div class="style-header-wrapper">
    <div class="container">
        <?php if($style['logo'] !== ''): ?>
        <?php $logoSrc = $mediaClass->getGalleryStyleLogoFolderUrl() . '/' . $style['logo']; ?>
        <img src="<?php echo $logoSrc; ?>" class="img-responsive center-block" />
        <?php endif; ?>

        <div class="row">
            <div class="col-sm-10">
                <p>
                    <strong><?php echo $site['name']; ?></strong>
                    |
                    <?php echo $site['address']['street'] . ' ' . $site['address']['streetno']; ?>
                    |
                    <?php echo $site['address']['zip'] . ' ' . $site['address']['city']; ?>
                    |
                    <?php echo $Core->i18n()->translate('Telefon') . ': ' . $site['address']['phone']; ?>
                    |
                    <?php echo $Core->i18n()->translate('E-Mail') . ': ' . $site['address']['email']; ?>
                </p>
            </div>
            <div class="col-sm-2">
                <span class="pull-right-sm galleries-menu">
                    <?php echo $mediaClass->unlockedGalleryMenu('button-xs'); ?>
                </span>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>