<?php
/**
 * Media Model View Galleries Styles Index
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $mediaClass Media
 */
$activeDirs = array();

foreach($dirs as $dir) {
    if(array_key_exists('active', $dir) && $dir['active']) {
        $activeDirs[$dir['id']] = $dir;
    }
}
?>
<div class="action-wrapper">
    <div class="btn-set top text-right">
        <?php if(!is_object($accClass) || $accClass->hasAccess('media_galleries_styles_create')): ?>
            <a href="<?php echo $Mvc->getModelUrl() . '/galleries/stylesCreate'; ?>" class="btn btn-primary" title="<?php echo $Core->i18n()->translate('Stil erstellen'); ?>">
                <i class="fa fa-plus" aria-hidden="true"></i> <i class="fa fa-paint-brush" aria-hidden="true"></i>
            </a>
        <?php endif; ?>
        <?php if($accClass->hasAccess('media_galleries')) : ?>
            <a href="<?php echo $Mvc->getModelUrl() . '/galleries'; ?>" class="btn btn-default" title="<?php echo $Core->i18n()->translate('zurück zu den Galerien'); ?>">
                <i class="fa fa-arrow-left" aria-hidden="true"></i>
            </a>
        <?php endif; ?>
    </div>
</div>

<?php if($mediaClass->getGalleryStylesCount($siteId,false)) : ?>
    <ul class="nav nav-tabs">
        <li class="active"><a href="#active" data-toggle="tab"><?php echo $Core->i18n()->translate('Aktiv') ?> <span class="badge"><?php echo $mediaClass->getGalleryStylesCount($siteId); ?></span></a></li>
        <li><a href="#inactive" data-toggle="tab"><?php echo $Core->i18n()->translate('Inaktiv') ?> <span class="badge"><?php echo $mediaClass->getGalleryStylesCount($siteId,false); ?></span></a></li>
    </ul>
    <div id="dirsTabContent" class="tab-content">
        <div class="tab-pane fade active in" id="active">
            <?php include('styles.entry_list.php'); ?>
        </div>
        <div class="tab-pane fade" id="inactive">
            <?php $dataArea = 'inactive'; include('styles.entry_list.php'); ?>
        </div>
    </div>
<?php else: ?>
    <?php include('styles.entry_list.php'); ?>
<?php endif; ?>