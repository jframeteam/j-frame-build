<?php
/**
 * Media Model View Galleries View
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $mediaClass Media
 * @var $mediaFilesystem MediaFilesystem
 * @var $plugins Plugins
 * @var $forms Forms
 */

$mediaFilesystem = $mediaClass->getMediaFilesystem();

/** Prepare Forms Plugin */
$plugins = $Core->Plugins();
$forms   = $plugins->Forms();
$isForms = (is_object($forms));

//$btnActionName        = ($isForms) ? $pitsCore->xorEnc('action') : 'action';
//$downloadCheckboxName = ($isForms) ? $pitsCore->xorEnc('download_files') : 'download_files';
$btnActionName        = 'action';
$downloadCheckboxName = 'download_files';

$dirFound = (count($dir)) ? true : false;

if($dirFound): ?>
    <?php
    $galleryUrl = $Mvc->getModelUrl() . '/galleries/view' . str_replace(DS,'/',$dir['dirPath']);
    $formActionUrl = $galleryUrl;
    ?>
    <?php
    $dirContents = (array_key_exists('dirContents', $dir) && is_array($dir['dirContents'])) ? $dir['dirContents'] : array();
    if(!count($dirContents) && $mediaClass->getDirSize($dir['id'])) {
        $dirContents = $mediaClass->getDirContents($dir['dirPath']);
    }

    $has2BGFilesAndBigger = false;
    $largeFileSizeByte = $mediaFilesystem->getLargeFileSize();
    if(count($dirContents)) {
        foreach ($dirContents as $fileFullName => $file) {
            $fullFilePath = $file['filePath'];
            if(file_exists($fullFilePath) && $mediaFilesystem->getFileSize($fullFilePath) >= $largeFileSizeByte) {
                $has2BGFilesAndBigger = true;
                break;
            }
        }
    }
    ?>
    <?php $formTarget = ($unlocked) ? ' target="_blank"' : ''; ?>
    <?php if($dir['active']): ?>
        <form id="gallery_form" class="form-horizontal" action="<?php echo $formActionUrl; ?>" method="post"<?php echo $formTarget ?>>
            <?php if($unlocked): ?>
            <?php $hasContents = (count($dirContents)); ?>
                <?php if($hasContents) : ?>
                    <?php if($has2BGFilesAndBigger) : ?>
                        <div class="alert alert-warning" role="alert">
                            <strong><?php echo $Core->i18n()->translate('Important'); ?>:</strong><br />
                            <?php echo sprintf($Core->i18n()->translate('Files with size %s or more cannot downloaded within a ZIP File and must be downloaded separately!'), $mediaClass->formatBytes($largeFileSizeByte)); ?>
                        </div>
                    <?php endif; ?>
                    <div class="action-wrapper row">
                        <div class="col-sm-12">
                            <div class="btn-set top text-right">
                                <span class="btn btn-default" onClick="selectAllThumbnails()" title="<?php echo $Core->i18n()->translate('Alle Elemente auswählen'); ?>">
                                    <i class="fa fa-check-square-o" aria-hidden="true"></i>
                                </span>
                                <button type="submit" name="<?php echo $btnActionName; ?>" value="download" class="btn btn-primary" data-action="download" title="<?php echo $Core->i18n()->translate('Download') ?>">
                                    <i class="fa fa-download" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="row media-content gallery-view">

                    <?php $subDirs = $mediaClass->getDirSubFolderByDirId($dir['id']); ?>
                    <?php if(count($subDirs)) : ?>
                        <?php foreach($subDirs as $subDirId => $subDir) : ?>
                            <?php if($subDir['active'] && $subDir['dirFolderExists'] && $subDir['show_in_parent_gallery']) : ?>
                                <?php $subGalleryUrl = $Mvc->getModelUrl() . '/galleries/view' . str_replace(DS,'/',$subDir['dirPath']); ?>
                                <div class="col-xs-3 col-sm-2 thumbnail-wrapper folder-link" id="<?php echo $subDir['dir']; ?>">
                                    <a href="<?php echo $subGalleryUrl; ?>" class="thumbnail" target="_self" title="<?php echo $subDir['name']; ?>">
                                        <span class="media-preview">
                                            <i class="fa fa-folder" aria-hidden="true"></i>
                                        </span>
                                        <span class="media-filename one-line-ellipsis"><?php echo $subDir['name']; ?></span>
                                    </a>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                        <div class="clearfix"></div>
                        <div class="col-sm-12"><hr /></div>
                    <?php endif; ?>

                    <?php if($hasContents) : ?>
                        <?php $fi=0; ksort($dirContents); foreach($dirContents as $fileFullName => $file): ?>
                            <?php
                                $thPreview = '<i class="fa fa-file" aria-hidden="true"></i>';

                                $fileMime = (array_key_exists('mime',$file)) ? $file['mime'] : '';
                                $fileName = (array_key_exists('name',$file)) ? $file['name'] : '';
                                $fileUrl = (array_key_exists('fileUrl',$file)) ? $file['fileUrl'] : '';

                                if($fileFullName == '.thinfo') { continue; }

                                $isVideo = false;
                                $isAudio = false;

                                $isImage = false;
                                if(strpos($fileMime, 'image/') !== false) {
                                    $isImage = true;
                                    if(
                                        array_key_exists('thumbnail', $file) &&
                                        is_array($file['thumbnail']) &&
                                        count($file['thumbnail']) &&
                                        array_key_exists('fileUrl', $file['thumbnail'])
                                    ) {
                                        // $thPreview = '<img class="lazy loader" data-src="' . $file['thumbnail']['fileUrl'] . '" alt="">';
                                        $thPreview = '<img class="lazy loader th-image" data-src="' . $file['thumbnail']['fileUrl'] . '" alt="">';
                                    } else {
                                        if(array_key_exists('fileUrl', $file)) {
                                            $thPreview = '<img class="lazy loader" data-src="' . $file['fileUrl'] . '" alt="">';
                                        }
                                    }
                                }

                                if(strpos($fileMime, 'video/') !== false) {
                                    $thPreview = '<i class="fa fa-file-video-o" aria-hidden="true"></i>';

                                    $isVideo = true;
                                }

                                if(strpos($fileMime, 'audio/') !== false) {
                                    $thPreview = '<i class="fa fa-file-audio-o" aria-hidden="true"></i>';

                                    $isAudio = true;
                                }

                                if(
                                    $fileMime == 'application/msword' ||
                                    $fileMime == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
                                ) {
                                    $thPreview = '<i class="fa fa-file-word-o" aria-hidden="true"></i>';
                                }

                                if(
                                    $fileMime == 'application/msexcel' ||
                                    $fileMime == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                                ) {
                                    $thPreview = '<i class="fa fa-file-excel-o" aria-hidden="true"></i>';
                                }

                                if($fileMime == 'application/pdf') {
                                    $thPreview = '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>';
                                }

                                if($fileMime == 'application/zip') {
                                    $thPreview = '<i class="fa fa-file-archive-o" aria-hidden="true"></i>';
                                }

                                if(!$isImage) {
                                    $preview = '<span class="media-preview">' . $thPreview . '</span>';
                                    $thPreview = $preview;
                                }

                                /** Filename Output */
                                $mediaFilenameIsImageClass = ($isImage) ? ' mf-image' : '';
                                $thPreview .= '<span class="media-filename one-line-ellipsis'.$mediaFilenameIsImageClass.'">' . $fileFullName . '</span>';
                            ?>
                            <div class="col-xs-6 col-md-3 thumbnail-wrapper" id="<?php echo $fileName; ?>">
                                <a href="<?php echo $fileUrl ?>" class="thumbnail"<?php if($isImage) : ?> data-fancybox="<?php echo $dir['dir']; ?>" data-caption="<?php echo $fileName; ?>" <?php else: ?> target="_blank"<?php endif; ?> title="<?php echo $fileFullName; ?>">
                                    <?php echo $thPreview; ?>
                                    <?php if($isVideo || $isAudio) : ?>
                                        <div class="media-player--wrapper<?php if($isAudio) : ?> mp-audio<?php endif; ?>">
                                            <div class="media-player--element" id="mp_<?php echo $dir['id'] . '-' . $fi; ?>">
                                                <?php if($isVideo) : ?>
                                                    <video id="mf_<?php echo $dir['id'] . '-' . $fi; ?>" class="video-js" data-setup='{ "controls": true, "autoplay": false, "preload": "auto" }' controls>
                                                        <source src="<?php echo $fileUrl; ?>" type="<?php echo $fileMime; ?>">
                                                    </video>
                                                <?php endif; ?>
                                                <?php if($isAudio) : ?>
                                                    <audio id="mf_<?php echo $dir['id'] . '-' . $fi; ?>" class="video-js" data-setup='{ "controlBar": { "fullscreenToggle": false } }' controls>
                                                        <source src="<?php echo $fileUrl; ?>" type="<?php echo $fileMime; ?>">
                                                    </audio>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </a>

                                <div class="togglebutton">
                                    <label>
                                        <?php
                                        /** File Checkbox Element */

                                        $formElementData['type']            = 'checkbox';
                                        $formElementData['id']              = '';
                                        $formElementData['name']            = 'download_files[]';
                                        $formElementData['label']           = false;
                                        $formElementData['value']           = $fileFullName;
                                        $formElementData['valueChecked']    = '';
                                        $formElementData['valueNotChecked'] = '';
                                        $formElementData['checkboxOnly']    = true;
                                        echo ($isForms) ? $forms->buildElement($formElementData) : '';
                                        ?>
                                    </label>
                                </div>

                            </div>
                        <?php $fi++; endforeach; ?>
                    <?php else: ?>
                        <div class="col-sm-12">
                            <div class="alert alert-warning" role="alert"><?php echo $Core->i18n()->translate('Keine Inhalte in der Galerie gefunden!') ?></div>
                        </div>
                    <?php endif; ?>
                </div>
            <?php else: ?>
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title"><?php echo $Core->i18n()->translate('Für den Zugriff auf diese Galerie wird ein Passwort benötigt!') ?></h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="password" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Passwort') ?> <em>*</em></label>

                                    <div class="col-md-10">
                                        <?php
                                        /** Password Element */
                                        $formElementData['id']            = 'password';
                                        $formElementData['name']          = 'password';
                                        $formElementData['label']         = false;
                                        $formElementData['value']         = '';
                                        $formElementData['type']          = 'text';
                                        $formElementData['required']      = true;
                                        $formElementData['addAttributes'] = array(
                                                'placeholder' => $Core->i18n()->translate('Passwort'),
                                                'autocomplete' => 'off',
                                        );
                                        echo ($isForms) ? $forms->buildElement($formElementData) : '';
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-10 col-md-offset-2">
                                        <button type="submit" class="btn btn-success" name="<?php echo $btnActionName; ?>" value="unlock_gallery" data-action="unlock_gallery"><?php echo $Core->i18n()->translate('Weiter'); ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </form>
    <?php else: ?>
        <div class="alert alert-warning" role="alert"><?php echo $Core->i18n()->translate('Galerie ist nicht freigeschaltet!') ?></div>
    <?php endif; ?>
<?php else: ?>
    <div class="alert alert-warning" role="alert"><?php echo $Core->i18n()->translate('Galerie nicht gefunden!') ?></div>
<?php endif; ?>
