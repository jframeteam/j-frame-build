<?php
/**
 * Media Model View Galleries Index
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $mediaClass Media
 * @var $sitesClass Sites
 */
$sitesClass = $Core->Sites();
$site = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
$siteId = (count($site)) ? $site['id'] : 0;

$nestedDirs = (isset($nestedDirs)) ? $nestedDirs : array();

function showGalleries($dirsArray) {
    global $Core, $Mvc;

    $sitesClass = $Core->Sites();
    $site = (is_object($sitesClass)) ? $sitesClass->getSite() : array();

    $siteId = (count($site)) ? $site['id'] : 0;

    $mediaClass = $Mvc->modelClass('Media');

    $return = '';
    if(is_array($dirsArray) && count($dirsArray)) {
        foreach($dirsArray as $dirName => $dir) {

            $canShow = ((array_key_exists('active', $dir) && $dir['active']) && (!array_key_exists('archive_state', $dir) || $dir['archive_state'] < 1));

            if($canShow) {
                $dirContents = (array_key_exists('dirContents', $dir) && is_array($dir['dirContents'])) ? $dir['dirContents'] : array();
                if(!count($dirContents) && $mediaClass->getDirSize($dir['id'])) {
                    $dirContents = $mediaClass->getDirContents($dir['dirPath']);
                }

                $firstContent = (count($dirContents)) ? reset($dirContents) : array();

                $firstImageSrc = '';
                if(count($firstContent)){
                    $firstImageThSrc = (array_key_exists('thumbnail', $firstContent) && is_array($firstContent['thumbnail']) && array_key_exists('mime', $firstContent['thumbnail']) && strpos($firstContent['thumbnail']['mime'], 'image/') !== false) ? $firstContent['thumbnail']['fileUrl'] : '';
                    $firstImageSrc = ($firstImageThSrc == '' && array_key_exists('mime', $firstContent) && strpos($firstContent['mime'], 'image/') !== false) ? $firstContent['fileUrl'] : $firstImageThSrc;
                }

                $previewImageSrc = ($firstImageSrc == '') ? 'http://via.placeholder.com/350?text=nopic' : $firstImageSrc;

                $dirPassword = ($dir['public']) ? '<s data-toggle="tooltip" title="' . $Core->i18n()->translate('Ordner ist öffentlich.') . '">' . $dir['password'] . '</s>' : $dir['password'];

                $galleryUrl = $Mvc->getModelUrl() . '/galleries/view' . str_replace(DS, '/', $dir['dirPath']);

                $return .= '<div class="col-sm-6 col-md-4 col-lg-3">
                    <div class="thumbnail">
                        <img src="' . $previewImageSrc . '" alt="">
                        <div class="caption">
                            <h3 class="one-line-ellipsis" title="' . $dir['name'] . '">' . $dir['name'] . '</h3>
                            <div class="row">
                                <div class="col-sm-6">' .
                        $Core->i18n()->translate('Inhalt') . ':
                                </div>
                                <div class="col-sm-6">' .
                        count($dirContents) . ' ' . $Core->i18n()->translate('Elemente') . '
                                </div>
    
                                <div class="col-sm-6">' .
                        $Core->i18n()->translate('Downloads') . ':
                                </div>
                                <div class="col-sm-6">' .
                        $dir['fileDownloads'] . '
                                </div>
    
                                <div class="col-sm-6">' .
                        $Core->i18n()->translate('Passwort') . ':
                                </div>
                                <div class="col-sm-6">' .
                        $dirPassword . '
                                </div>';

                    if ($mediaClass->getGalleryStylesCount($siteId)) {
                        $style = ($dir['id_media_galleries_styles'] > 0 && $mediaClass->galleryStyleExists($dir['id_media_galleries_styles'])) ? $mediaClass->getGalleryStyle($dir['id_media_galleries_styles']) : array();
                        $styleTitle = (count($style)) ? $style['title'] : '--';

                        $return .= '<div class="col-sm-6">' .
                            $Core->i18n()->translate('Galerie Stil') . ':
                                    </div>
                                    <div class="col-sm-6">' .
                            $styleTitle . '
                                    </div>';
                    }
                    $return .= '</div>
                            <p>
                                <a href="' . $galleryUrl . '" class="btn btn-primary" role="button" target="_blank" title="' . $Core->i18n()->translate('Galerie öffnen') . '">
                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>';
            }

            if (array_key_exists('children', $dir) && count($dir['children'])) {
                $return .= showGalleries($dir['children']);
            }

            if($canShow) {
                if (!$dir['id_media_dirs__parent']) {
                    $return .= '<div class="col-sm-12"><p class="bg-warning">&nbsp;</p></div>';
                }
            }
        }
    }
    return $return;
}

?>
<div class="action-wrapper">
    <div class="btn-set top text-right">
        <?php if(!is_object($accClass) || $accClass->hasAccess('media_galleries_styles')): ?>
            <a href="<?php echo $Mvc->getModelUrl() . '/galleries/styles'; ?>" class="btn btn-primary" title="<?php echo $Core->i18n()->translate('Stile verwalten'); ?>">
                <i class="fa fa-paint-brush" aria-hidden="true"></i>
            </a>
        <?php endif; ?>
    </div>
</div>
<?php if(count($nestedDirs)) : ?>
    <div class="row gallery-list">
        <?php echo showGalleries($nestedDirs); ?>
    </div>
<?php else: ?>
    <div class="alert alert-warning" role="alert"><?php echo $Core->i18n()->translate('Keine freigeschalteten Galerien vorhanden.'); ?></div>
<?php endif; ?>