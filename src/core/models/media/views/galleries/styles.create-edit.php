<?php
/**
 * Media Model View Galleries Styles Create / Edit
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $accClass Acc
 * @var $mediaClass Media
 * @var $plugins Plugins
 * @var $pitsForms PitsForms
 * @var $pitsCore PitsCore
 */

/** Prepare Form Plugin */
$plugins = $Core->Plugins();
$pitsForms = $plugins->PitsForms();
$isForms = (is_object($pitsForms));
$pitsCore = $plugins->PitsCore();

$actionBtnName = ($isForms) ? $pitsCore->xorEnc('action') : 'action';

$type = (isset($type)) ? $type : 'create';

$galleryStyleData = (isset($galleryStyleData)) ? $galleryStyleData : array();

$id                  = (array_key_exists('id',                  $galleryStyleData)) ? $galleryStyleData['id']                  : 0;
$title               = (array_key_exists('title',               $galleryStyleData)) ? $galleryStyleData['title']               : '';
$logo                = (array_key_exists('logo',                $galleryStyleData)) ? $galleryStyleData['logo']                : '';
$color_header_bg     = (array_key_exists('color_header_bg',     $galleryStyleData)) ? $galleryStyleData['color_header_bg']     : '#ffffff';
$color_header_txt    = (array_key_exists('color_header_txt',    $galleryStyleData)) ? $galleryStyleData['color_header_txt']    : '#000000';
$color_page_bg       = (array_key_exists('color_page_bg',       $galleryStyleData)) ? $galleryStyleData['color_page_bg']       : '#e9e9e9';
$color_content_bg    = (array_key_exists('color_content_bg',    $galleryStyleData)) ? $galleryStyleData['color_content_bg']    : '#ffffff';
$color_content_txt   = (array_key_exists('color_content_txt',   $galleryStyleData)) ? $galleryStyleData['color_content_txt']   : '#000000';
$color_thumbnail_bg  = (array_key_exists('color_thumbnail_bg',  $galleryStyleData)) ? $galleryStyleData['color_thumbnail_bg']  : '#ffffff';
$color_thumbnail_txt = (array_key_exists('color_thumbnail_txt', $galleryStyleData)) ? $galleryStyleData['color_thumbnail_txt'] : '#000000';
$active              = (array_key_exists('active',              $galleryStyleData) && $galleryStyleData['active']) ? true      : false;

$canEdit    = (isset($canEdit)) ? $canEdit : false;
$canDelete  = (isset($canDelete)) ? $canDelete : false;
$canCreate  = (isset($canCreate)) ? $canCreate : false;


$fileElementName = 'file';
if($canEdit) {
    $fileElementName = ($isForms) ? $pitsCore->xorEnc('file') : 'file';
}

$infoText = ($type == 'create') ? $Core->i18n()->translate('Gallerie Stil erstellen') : '"' . $title . '" ' . $Core->i18n()->translate('Gallerie Stil bearbeiten');
$infoText .= ($type == 'create' && $title !== '') ? ' ' . $Core->i18n()->translate('von') . ' "' . $title . '"' : '';

$galleriesStylesSaveUrl = $Mvc->getModelUrl() . '/galleries/stylesSave/' . $id;

if($canEdit || $canCreate): ?>

<p><?php echo $infoText; ?></p>

<form id="edit_create_form" class="form-horizontal" action="<?php echo $galleriesStylesSaveUrl ?>" method="post">
    <?php
    /** Hidden Type Element */
    $formElementData['eleType']       = 'input';
    $formElementData['name']          = 'type';
    $formElementData['label']         = false;
    $formElementData['value']         = $type;
    $formElementData['type']         = 'hidden';
    echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
    ?>
    <?php
    /** Hidden Id Element */
    $formElementData['eleType']       = 'input';
    $formElementData['name']          = 'id';
    $formElementData['label']         = false;
    $formElementData['value']         = $id;
    $formElementData['type']         = 'hidden';
    echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
    ?>

    <div class="action-wrapper row">
        <div class="col-sm-12">
            <div class="btn-set top text-right">
                <button type="submit" name="<?php echo $actionBtnName; ?>" value="galleryStyle_save" class="btn btn-primary" data-action="galleryStyle_save">
                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                </button>
                <?php if($accClass->hasAccess('media_galleries_styles')) : ?>
                    <a href="<?php echo $Mvc->getModelUrl() . '/galleries/styles'; ?>" class="btn btn-default" title="<?php echo $Core->i18n()->translate('zurück zur Übersicht'); ?>">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-sm-6" id="general">

            <fieldset>

                <legend><?php echo $Core->i18n()->translate('Allgemein') ?></legend>

                <div class="form-group">
                    <div class="col-md-7 col-md-offset-5">
                        <div class="togglebutton">
                            <label>
                                <?php
                                /** Active Element */
                                $formElementData['eleType']        = 'checkbox';
                                $formElementData['id']             = '';
                                $formElementData['name']           = 'active';
                                $formElementData['label']          = false;
                                $formElementData['value']          = ($type == 'create' || $active) ? '1' : '0';
                                $formElementData['valueChecked']   = '1';
                                $formElementData['valueUnchecked'] = '0';
                                $formElementData['checkboxOnly']  = true;
                                echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                                ?>
                                <?php echo $Core->i18n()->translate('Aktiv') ?>
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="title" class="col-md-5 control-label"><?php echo $Core->i18n()->translate('Titel') ?> <em>*</em></label>

                    <div class="col-md-7">
                        <?php
                        /** Name Element */
                        $formElementData['eleType']       = 'input';
                        $formElementData['id']            = 'title';
                        $formElementData['name']          = 'title';
                        $formElementData['label']         = false;
                        $formElementData['value']         = $title;
                        $formElementData['type']          = 'text';
                        $formElementData['isRequired']    = true;
                        $formElementData['ownAttributes'] = array('placeholder' => $Core->i18n()->translate('Titel'));
                        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="logo" class="col-md-5 control-label"><?php echo $Core->i18n()->translate('Logo') ?></label>

                    <div class="col-md-7">
                        <div class="input-group">
                            <?php
                            /** Surname Element */
                            $formElementData['eleType']       = 'input';
                            $formElementData['id']            = 'logo';
                            $formElementData['name']          = 'logo';
                            $formElementData['label']         = false;
                            $formElementData['value']         = $logo;
                            $formElementData['type']          = 'text';
                            $formElementData['isRequired']    = false;
                            $formElementData['ownAttributes'] = array(
                                'placeholder' => $Core->i18n()->translate('Logo'),
                                'readonly' => 'readonly'
                            );
                            echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                            ?>
                            <div class="input-group-addon">
                                <div class="btn-group btn-group-xs display-flex">
                                    <a href="<?php echo $Mvc->getModelAjaxUrl()?>/dirs/mediabrowser/_galleries_styles_logos"  data-toggle="modal" data-target="#listDirsModal" data-dirfoldername="\_galleries_styles_logos\" class="btn btn-primary btn-xs" title="<?php echo $Core->i18n()->translate('Logo auswählen / hochladen') ?>" >
                                        <i class="fa fa-cloud-upload" aria-hidden="true"></i>
                                    </a>

                                    <button type="button" class="btn btn-danger btn-xs remove-logo<?php if($logo == '') : ?> hidden<?php endif; ?>" role="button"
                                            title="<?php echo $Core->i18n()->translate('Logo entfernen'); ?>"
                                    >
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="color_header_bg" class="col-md-5 control-label"><?php echo $Core->i18n()->translate('Header - Hintergrundfarbe') ?> <em>*</em></label>

                    <div class="col-md-7">
                        <?php
                        /** Header BG Color Element Element */
                        $formElementData['eleType']       = 'input';
                        $formElementData['id']            = 'color_header_bg';
                        $formElementData['name']          = 'color_header_bg';
                        $formElementData['label']         = false;
                        $formElementData['value']         = $color_header_bg;
                        $formElementData['type']          = 'color';
                        $formElementData['isRequired']    = true;
                        $formElementData['ownAttributes'] = array('placeholder' => $Core->i18n()->translate('Header - Hintergrundfarbe'));
                        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="color_header_txt" class="col-md-5 control-label"><?php echo $Core->i18n()->translate('Header -Textfarbe') ?> <em>*</em></label>

                    <div class="col-md-7">
                        <?php
                        /** Header Text Color Element */
                        $formElementData['eleType']       = 'input';
                        $formElementData['id']            = 'color_header_txt';
                        $formElementData['name']          = 'color_header_txt';
                        $formElementData['label']         = false;
                        $formElementData['value']         = $color_header_txt;
                        $formElementData['type']          = 'color';
                        $formElementData['isRequired']    = true;
                        $formElementData['ownAttributes'] = array('placeholder' => $Core->i18n()->translate('Header -Textfarbe'));
                        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="color_page_bg" class="col-md-5 control-label"><?php echo $Core->i18n()->translate('Seite - Hintergrundfarbe') ?> <em>*</em></label>

                    <div class="col-md-7">
                        <?php
                        /** Page Background Color Element */
                        $formElementData['eleType']       = 'input';
                        $formElementData['id']            = 'color_page_bg';
                        $formElementData['name']          = 'color_page_bg';
                        $formElementData['label']         = false;
                        $formElementData['value']         = $color_page_bg;
                        $formElementData['type']          = 'color';
                        $formElementData['isRequired']    = true;
                        $formElementData['ownAttributes'] = array('placeholder' => $Core->i18n()->translate('Seite - Hintergrundfarbe'));
                        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="color_content_bg" class="col-md-5 control-label"><?php echo $Core->i18n()->translate('Inhalt - Hintergrundfarbe') ?> <em>*</em></label>

                    <div class="col-md-7">
                        <?php
                        /** Content Background Color Element */
                        $formElementData['eleType']       = 'input';
                        $formElementData['id']            = 'color_content_bg';
                        $formElementData['name']          = 'color_content_bg';
                        $formElementData['label']         = false;
                        $formElementData['value']         = $color_content_bg;
                        $formElementData['type']          = 'color';
                        $formElementData['isRequired']    = true;
                        $formElementData['ownAttributes'] = array('placeholder' => $Core->i18n()->translate('Inhalt - Hintergrundfarbe'));
                        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="color_content_txt" class="col-md-5 control-label"><?php echo $Core->i18n()->translate('Inhalt - Textfarbe') ?> <em>*</em></label>

                    <div class="col-md-7">
                        <?php
                        /** Content Text Color Element */
                        $formElementData['eleType']       = 'input';
                        $formElementData['id']            = 'color_content_txt';
                        $formElementData['name']          = 'color_content_txt';
                        $formElementData['label']         = false;
                        $formElementData['value']         = $color_content_txt;
                        $formElementData['type']          = 'color';
                        $formElementData['isRequired']    = true;
                        $formElementData['ownAttributes'] = array('placeholder' => $Core->i18n()->translate('Inhalt - Textfarbe'));
                        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="color_thumbnail_bg" class="col-md-5 control-label"><?php echo $Core->i18n()->translate('Element Vorschau - Hintergrundfarbe') ?> <em>*</em></label>

                    <div class="col-md-7">
                        <?php
                        /** Thumbnail Background Color Element */
                        $formElementData['eleType']       = 'input';
                        $formElementData['id']            = 'color_thumbnail_bg';
                        $formElementData['name']          = 'color_thumbnail_bg';
                        $formElementData['label']         = false;
                        $formElementData['value']         = $color_thumbnail_bg;
                        $formElementData['type']          = 'color';
                        $formElementData['isRequired']    = true;
                        $formElementData['ownAttributes'] = array('placeholder' => $Core->i18n()->translate('Element Vorschau - Hintergrundfarbe'));
                        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="color_thumbnail_txt" class="col-md-5 control-label"><?php echo $Core->i18n()->translate('Element Vorschau - Textfarbe') ?> <em>*</em></label>

                    <div class="col-md-7">
                        <?php
                        /** Thumbnail Text Color Element */
                        $formElementData['eleType']       = 'input';
                        $formElementData['id']            = 'color_thumbnail_txt';
                        $formElementData['name']          = 'color_thumbnail_txt';
                        $formElementData['label']         = false;
                        $formElementData['value']         = $color_thumbnail_txt;
                        $formElementData['type']          = 'color';
                        $formElementData['isRequired']    = true;
                        $formElementData['ownAttributes'] = array('placeholder' => $Core->i18n()->translate('Element Vorschau - Textfarbe'));
                        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                        ?>
                    </div>
                </div>
            </fieldset>

        </div>
        <div class="col-sm-6 hidden-xs" id="preview">

            <fieldset>

                <legend><?php echo $Core->i18n()->translate('Vorschau') ?></legend>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="gsp--title"><?php echo $title ?></span>&nbsp;</h3>
                    </div>
                    <div class="panel-body">

                        <?php
                            $logoUrl = ($logo !== '') ? $mediaClass->getGalleryStyleLogoFolderUrl() . '/' . $logo : 'http://via.placeholder.com/600x65?text=Logo';
                            $logoImg = '<img class="img-responsive center-block" src="' . $logoUrl . '"/>';

                            $thumbnailPreviewImg = '<img class="img-responsive center-block" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAejAAAHowBNXh8qQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAeqSURBVHja7Zv5TxRnHIf9N1rE4lmPRmvb1DamUaMmVhtbW1tjTZtUa7BRo3JpUahQFUG5WblF5b4PQRDlkFu55HLB5VhF5JK/4dt91w5Fi7DHzOw7M58fnl/IBjbzPMzM+513FhDRAqBdcBAQAA4CAgAIACAAgAAAAgAIACAAgAAAAgAIAKg8gNFXU9VAu7AACGgXBIAAcBAQwAyME1NkGANqhLmdN4DOkSmqfQbUCHOLABAAAkAACAABIAAEgAAQgGMDeNT1iHruhdPTQm8azDxMxtSfqT/nOPWW+FNnbQo19BshTm0B1PcPU++dCzQavYGmgt6bk1dXnen5ze/ocWMuBKohgO7KGJqI+Ghe8bMxnLSbmjsaIFKJAdQNvqTBjIM2iX/jjBD8AXXWJEOmkgJg1/GRhG12y5+JvvQShCohgLqhcdM1fI+o8gW6qhMhlfcADPnuksh/fYO4iFraKiGW1wDYDdvUFSfJAmCMJGyHWF4DeJa8V1L5Ao/rMyGXtwCaOxplkW9eHl7/GnJ5C0BfelG2ANhlpqHvKQTzFMBI/Fb5AjDRXRUHwTwFwAY2cgZgyPeAYF4CYIMfOeUzjGm/QDAvATzsbpU9gBeJOyCYlwCa9N2yB8CeGEIwJwHUDY3JHsBg9hEI5ukmcCJ8jawB9BX7QTBPAYjx6Nca2prLIJinADpqU2WTPxG+iuqMExDMUwD1A89pInSFPDOAvFOQy+PDIH1ZkOTy2cCpsVcPuTwGUDc4SmNR66W9+Ss6C7E8bwhpba0wb9yQai8AiwxiOd8TyLZuiS1/PHKt6dTfC6lK2RXcVZ0g2pngZcxGetjdDqFKey+A7d8bi/rYzoc+B0wrjGeQqdQXQ+oGRsxbuidDlll3vY/bRO1NRZColncDG54OmC4L8WRM2W9ayrnMKn0s6lPqz3ejtkclVGuchED1vh38yhSEgVo66qi1pcz8OLl+4IXqZRy8nUan7hUiAC1ysryA3g93M3PodjrVmP4JEIBG8K0un5Yv8FPudaqW4XkGAnAwVxvraWGE+/8CYOxK19H9wTEEoFZiWttoUaTnrPIFNieHUFn/CAJQGzc7emip7syc8gU2JAVQYa8RAaiF7J5+WhnjY5F8gXVxfpTVY0AASqeo7xmti/e3Sr4Ai4adORCAQrlrupZvSLpsk3wBdtlg9w4IQGFUDI7TluRQu+QLLIr0Mq8eEIBCqB6apG8yo0WRL8CWjucf3EcAvMMmevvzb4gqfyZu5bcRAM+4lmRJJl/gcHGGzaNjTQdQbZwk98ZyKht4Kcnv96oolly+wL68JPOlBgFYSPnQKO2+d4uW512mrWUJokdwobaKnMLdZQvAPDrOuGa+2UQA863FDc9pc2mcWb6AmBGEPWwk50gPWeXbOjrWXAAZvQb6vDjyDfliRhDf9phcorwcIl/gi6QA88AJAbxFXHcXrS0ImVW+GBGkdelp2TVvh8qfHh3H+5tHzgjgX4Lam2hlftCc8u2JIE8/QKtjfbmQL7Aq+hwld/YggDNNFRaJtzWCYsMwfZL4N1fyBZbp/qTY1nZtBvDA+IoO1xZaLd+aCMoHRmnjzSAu5c8cHQc3NWgrgIqhcfqhIs1m+ZZEUGX6G9tTw7mW/9/o2IP8aiocH0BsaxvtzU0wHzyp5BcPvKDtdxPslj9XBOzssic7ThHyZ+J+77bjAgisr5neArUzQydJBDl9g/RlsU40+e+K4NfCFMXJnx4dl2ROj45lC4CNRd+ejIkdQVJPD60vChNd/tsRHC/LU6x8ARawLAGwU+VvRanv/CJiRRDW0UKrC65IJl9gS2ksrYg5p/gAGMdK86QNgM2ld2fFzD/DTrcvAt/mB/RhfqDk8gW+Komm5dFnVRHB1fpKaQK483SYNt0KtvxBhg0R1Jg4Wl8im3g1RvB7UYr4AeQ86bdpMGJNBJVDE7S/KtMh8tUUgegBJLV3Wb3l2doI2I3YzvIbDpWvlghEDSC0qYkWR50W5bn2uyIoMBhfH3QO5KshAtEC8Km6a542ibm54e0IkvV99FlROFfylR6B3QGwgcIfd3Ik2+EiRKDrbKc1BVe5lK/kCOwKoMp0I/ZjbqLk25z8W2plXeZpKQKbA2BvuWxLDZP8C7rc8FGEeKVGYFMABfoh8xur0n45d1qcfF5x8pUWgdUBpHQ9oTUS73xxinSnJen+ipWvpAisCkDX3GLxO+02y4/yoKWZFxQvXykRWBwA2+fuHOEp7aaFa560LPuSauQrIQKLAgisvS/5F3GOOU3LcwNUJ5/3CCwKgH1I0j1r8d4m+ZdVK5/nCBwegEsSW+YFqF4+rxE4MADTMu/WX5oRz2sEjgkgwrTMS/XTpHzeIpA9AKdID1qScUHT8nmKQNYAFuo8aWnWRcjnKAKLAnANOU/fH9tnNzvOHaJtfq6S8+2lE+QacZ6ORPgpgpOB3uTl4+YQolKuzx/AEd8TdPTALqBCdBHBCAABIAAEgAAQAAJAAAgAASAABIAAEMAbPwgNC8TB0nIA7EM4WBoOoKm1hYpLS4AKYW7nDQBoCwSAAHAQtB5ANdAuC4gIaBgcBASAg4AAAAIACAAgAIAAAAIACAAgAIAAAAIA6uYfNin7dmDTJ5EAAAAASUVORK5CYII=" />';
                        ?>

                        <div class="galleries-styles-preview">
                            <div class="gsp--page container-fluid" style="background-color: <?php echo $color_page_bg; ?>">
                                <div class="gsp--header row" style="padding-top: 15px; background-color: <?php echo $color_header_bg; ?>; color: <?php echo $color_header_txt; ?>">
                                    <div class="container-fluid">
                                        <div class="gsp--logo">
                                            <?php echo $logoImg ?>
                                        </div>
                                        <p><?php echo $Core->i18n()->translate('Header'); ?></p>
                                    </div>
                                </div>

                                <div class="gsp--content container-fluid" style="margin-top: 15px; margin-bottom: 15px; padding-top: 15px; background-color: <?php echo $color_content_bg; ?>; color: <?php echo $color_content_txt; ?>">
                                    <p><?php echo $Core->i18n()->translate('Inhalt'); ?></p>
                                    <div class="row">
                                        <?php echo str_repeat('<div class="thumbnail-wrapper col-sm-3"><div class="gsp--thumbnail thumbnail" style="background-color: ' . $color_thumbnail_bg . '; color: ' . $color_thumbnail_txt . '">' . $thumbnailPreviewImg . '<span class="center-block text-center one-line-ellipsis">' . $Core->i18n()->translate('Element') . '</span></div></div>' . PHP_EOL,12); ?>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </fieldset>
        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="listDirsModal" tabindex="-1" role="dialog" aria-labelledby="listDirsModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><?php echo $Core->i18n()->translate('Ordner'); ?></h4>
                </div>
                <div class="modal-body"><div class="te"></div></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Schließen') ?></button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

<?php else: ?>
    <?php echo $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...'); ?>
<?php endif; ?>