<?php
/**
 * Media Model View API Index
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $mediaClass Media
 * @var $accClass Acc
 * @var $plugins Plugins
 * @var $pitsForms PitsForms
 * @var $pitsCore PitsCore
 */

$fileElementName = 'file';
if($canEdit) {
    /** Prepare Form Plugin */
    $plugins = $Core->Plugins();
    $pitsForms = $plugins->PitsForms();
    $isForms = (is_object($pitsForms));
    $pitsCore = $plugins->PitsCore();

    $fileElementName = ($isForms) ? $pitsCore->xorEnc('file') : 'file';
}

$diskUsage_formatted          = $mediaClass->formatBytes($mediaClass->getDiskUsage());
$maxClientDiskSpace_formatted = $mediaClass->formatBytes($mediaClass->maxClientDiskSpace);
$diskUsagePercent             = $mediaClass->getDiskUsage(true);
$diskUsagePercent_forProgress = str_replace(array('.',','),array('','.'), $diskUsagePercent);


$curUser = (is_object($accClass)) ? $accClass->getUser() : array();
$curUserId = (count($curUser)) ? $curUser['id'] : 0;
?>
<div class="action-wrapper row">
    <div class="col-sm-12">
        <div class="btn-set top text-right">
            <?php if(!is_object($accClass) || $accClass->hasAccess('media_api_create')): ?>
                <a href="<?php echo $Mvc->getModelAjaxUrl() . '/api/create'; ?>" class="btn btn-primary" title="<?php echo $Core->i18n()->translate('API Zugang erstellen'); ?>" data-toggle="modal" data-target="#createEditApiModal">
                    <i class="fa fa-plus" aria-hidden="true"></i> <i class="fa fa-key" aria-hidden="true"></i>
                </a>
            <?php endif; ?>
            <?php if(!is_object($accClass) || $accClass->hasAccess('media_api_logview')): ?>
                <a href="<?php echo $Mvc->getModelUrl() . '/api/logview'; ?>" class="btn btn-default" title="<?php echo $Core->i18n()->translate('media_api_logview'); ?>">
                    <i class="fa fa-list-alt" aria-hidden="true"></i>
                </a>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php if($mediaClass->getApiUsersCount($siteId,false)) : ?>
    <ul class="nav nav-tabs">
        <li class="active"><a href="#dirs" data-toggle="tab"><?php echo $Core->i18n()->translate('Aktiv') ?> <span class="badge"><?php echo $mediaClass->getApiUsersCount($siteId); ?></span></a></li>
        <li><a href="#inactive" data-toggle="tab"><?php echo $Core->i18n()->translate('Inaktiv') ?> <span class="badge"><?php echo $mediaClass->getApiUsersCount($siteId,false); ?></span></a></li>
    </ul>
    <div id="dirsTabContent" class="tab-content">
        <div class="tab-pane fade active in" id="dirs">
            <?php include('index.entry_list.php'); ?>
        </div>
        <div class="tab-pane fade" id="inactive">
            <?php $dataArea = 'inactive'; include('index.entry_list.php'); ?>
        </div>
    </div>
<?php else: ?>
    <?php include('index.entry_list.php'); ?>
<?php endif; ?>

<?php if($canEdit) : ?>
<!-- Modal -->
<div class="modal fade" id="createEditApiModal" tabindex="-1" role="dialog" aria-labelledby="createEditApiModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?php echo $Core->i18n()->translate('Api User'); ?></h4>
            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Schließen') ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<?php endif; ?>
