<?php
/**
 * Media Model View API Index Entry List
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $mediaClass Media
 * @var $accClass Acc
 * @var $plugins Plugins
 * @var $pitsForms PitsForms
 */

$dataArea = (isset($dataArea)) ? $dataArea . '/' : '';

$dtSourceUrl = $Mvc->getModelAjaxUrl() . '/api/dtsource/' . $dataArea;
?>


<div class="table-responsive">
    <table
        class="dir-list table table-hover table-striped table-condensed"
        data-datatable
        data-dtsource="<?php echo $dtSourceUrl; ?>"
        width="100%"
    >
        <thead>
        <tr>
            <th data-dtsortable="0"></th>
            <th data-dtdefaultsort="asc">
                <?php echo $Core->i18n()->translate('User Name'); ?>
            </th>
            <?php if($dataArea === ''): ?>
            <th data-dtsortable="0">
                <?php echo $Core->i18n()->translate('Api Key'); ?>
            </th>
            <?php endif; ?>
            <th>
                <?php echo $Core->i18n()->translate('Datum'); ?>
            </th>
            <th data-dtsortable="0"></th>
        </tr>
        </thead>
        <tbody>
            <!-- will be replaced by DataTables -->
        </tbody>
    </table>
</div>
