<?php
/**
 * Media Model View Dirs List
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $mediaClass Media
 * @var $accClass Acc
 * @var $plugins Plugins
 * @var $pitsForms PitsForms
 * @var $pitsCore PitsCore
 */

$fileElementName = 'file';
if($canEdit) {
    /** Prepare Form Plugin */
    $plugins = $Core->Plugins();
    $pitsForms = $plugins->PitsForms();
    $isForms = (is_object($pitsForms));
    $pitsCore = $plugins->PitsCore();

    $fileElementName = ($isForms) ? $pitsCore->xorEnc('file') : 'file';
}

$diskUsage_formatted          = $mediaClass->formatBytes($mediaClass->getDiskUsage());
$maxClientDiskSpace_formatted = $mediaClass->formatBytes($mediaClass->maxClientDiskSpace);
$diskUsagePercent             = $mediaClass->getDiskUsage(true);
$diskUsagePercent_forProgress = str_replace(array('.',','),array('','.'), $diskUsagePercent);

$curUser   = (is_object($accClass)) ? $accClass->getUser() : array();
$curUserId = (count($curUser)) ? $curUser['id'] : 0;

$curDirPath = (isset($curDirPath)) ? $curDirPath : DS;
$curDirPath_home  = '<i class="fa fa-home" aria-hidden="true"></i>';
$curDirPath_text  = $curDirPath;
$curDirPath_dirId = $mediaClass->getDirIdByFolderPath($curDirPath_text);

if($curDirPath !== DS) {
    $curDirPath_linked = DS;
    $curDirPath_parts = preg_split('#\/#', str_replace(DS,'/',$curDirPath), -1, PREG_SPLIT_NO_EMPTY);
    foreach($curDirPath_parts as $curDirPath_partsKey => $dirFolderName) {
        $dirFromFolderName = $mediaClass->getDirByFolderName($dirFolderName);
        $dirListUrl = $Mvc->getModelUrl() . '/dirs/list/' . str_replace(DS,'/',trim($dirFromFolderName['dirPath'],DS)) . '/';

        if(($curDirPath_partsKey + 1) == count($curDirPath_parts)) {
            $curDirPath_linked .= $dirFolderName . DS;
        } else {
            $curDirPath_linked .= '<a href="' . $dirListUrl . '">' . $dirFolderName . '</a>' . DS;
        }
    }
    $curDirPath = $curDirPath_linked;
    $curDirPath_home = '<a href="' . $Mvc->getModelUrl() . '/dirs/' . '"><i class="fa fa-home" aria-hidden="true"></i></a>';
}

$dirsNested = $mediaClass->getDirsNested(null,0,false);
$dirTreeAsLinkedList = '<div class="folderTree"><ul><li class="root">' . $curDirPath_home . DS . PHP_EOL . $mediaClass->getDirTreeAsLinkedList($dirsNested,0,$curDirPath_text) . '</li></ul></div>';
$dirTreeAsLinkedList_button = '<span title="' . $Core->i18n()->translate('Verzeichnisbaum anzeigen') . '" data-toggle="tooltip" data-trigger="hover"><button type="button" class="btn btn-default btn-sm" data-toggle="popover" data-placement="right" data-trigger="focus" data-content="' . htmlentities($dirTreeAsLinkedList) . '"><i class="fa fa-sitemap" aria-hidden="true"></i></button></span>';

/** @var $sitesClass Sites */
$sitesClass = $Core->Sites();
$site      = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
$curSiteId = (count($site)) ? $site['id'] : 0;

$getDirsConfig = array(
    'siteId' => $curSiteId,
    'parentId' => 'all',
    'archived' => true
);
$archivedDirs = $mediaClass->getDirs($getDirsConfig);
$dirsArchiveStates = $mediaClass->getDirInfoNestedOneLevel($archivedDirs, 'archive_state');
$markedForArchiveDirsCount = 0;
foreach($dirsArchiveStates as $dirId => $archiveState) {
    if($archiveState == 1 || $archiveState == 3) {
        $markedForArchiveDirsCount++;
    }
}
?>
<div class="action-wrapper row">
    <div class="col-sm-6">
        <div class="dir_path" data-dirpath="<?php echo $curDirPath_text; ?>"><?php echo $dirTreeAsLinkedList_button; ?>&nbsp;&nbsp;<?php echo $Core->i18n()->translate('Aktueller Pfad'); ?>: <span class="well well-sm"><?php echo $curDirPath_home; ?><?php echo $curDirPath; ?></span></div>
    </div>
    <div class="col-sm-6">
        <div class="btn-set top text-right">
            <?php if($markedForArchiveDirsCount > 0) : ?>
                <a
                    href="<?php echo $Mvc->getModelUrl() . '/dirs/dearchive/'; ?>"
                    class="btn btn-warning btn-sm"
                    data-toggle="tooltip"
                    title="<?php echo $Core->i18n()->translate('Alle zur Archivierung vorgemerkten Ordner zurücksetzen'); ?>"
                    data-ays="dearchive"
                    data-action="dir_dearchive"
                    data-aystext="<?php echo htmlentities('<div class="alert alert-warning" role="alert">' . $Core->i18n()->translate('Ganz sicher?') . '<br />' . $Core->i18n()->translate('Alle zur Archivierung vorgemerkten Ordner werden dadurch zurückgesetzt.') . '</div>') ?>"
                >
                    <span class="fa-stack">
                        <i class="fa fa-archive fa-stack-1x"></i>
                        <i class="fa fa-ban fa-stack-2x text-danger"></i>
                    </span>
                </a>
            <?php endif; ?>
            <?php if(!is_object($accClass) || $accClass->hasAccess('media_dirs_create')): ?>
                <div class="btn-group">
                    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="<?php echo $Core->i18n()->translate('Synchronisieren'); ?>">
                        <i class="fa fa-exchange" aria-hidden="true"></i>&nbsp;&nbsp;<span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li>
                            <a href="<?php echo $Mvc->getModelUrl() . '/dirs/sync/folder/1/fileDownloads/1/'; ?>" title="<?php echo $Core->i18n()->translate('Media Folder Sync und Datei-Downloads Sync ausführen'); ?>" data-toggle="tooltip">
                                <i class="fa fa-folder" aria-hidden="true"></i> <i class="fa fa-line-chart" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="<?php echo $Mvc->getModelUrl() . '/dirs/sync/folder/1/'; ?>" title="<?php echo $Core->i18n()->translate('Media Folder Sync ausführen'); ?>" data-toggle="tooltip">
                                <i class="fa fa-folder" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $Mvc->getModelUrl() . '/dirs/sync/fileDownloads/1/'; ?>" title="<?php echo $Core->i18n()->translate('Datei-Downloads Sync ausführen'); ?>" data-toggle="tooltip">
                                <i class="fa fa-line-chart" aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul>
                </div>

                <a href="<?php echo $Mvc->getModelAjaxUrl() . '/dirs/create/'.$curDirPath_dirId; ?>" class="btn btn-primary" title="<?php echo $Core->i18n()->translate('Ordner erstellen'); ?>" data-toggle="modal" data-target="#listDirsModal">
                    <i class="fa fa-plus" aria-hidden="true"></i> <i class="fa fa-folder" aria-hidden="true"></i>
                </a>
            <?php endif; ?>
        </div>
    </div>
</div>

<legend>
    <div class="row">
        <div class="col-sm-10">
            <?php echo $Core->i18n()->translate('Verfügbare Ordner'); ?> (<?php echo $diskUsage_formatted; ?> / <?php echo $maxClientDiskSpace_formatted; ?>) (<?php echo $Core->i18n()->translate('Maximale Dateigröße') . ': ' . $mediaClass->formatBytes($mediaClass->postMaxSize); ?>)
        </div>
        <div class="col-sm-2">
            <div class="progress disk-usage" title="<?php echo $diskUsagePercent; ?>%">
                <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $diskUsagePercent_forProgress; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $diskUsagePercent_forProgress; ?>%;">
                    <span><?php echo $diskUsagePercent; ?>%</span>
                </div>
            </div>
        </div>
    </div>
</legend>

<?php
$getDirsCountConfig = array(
    'siteId'               => $siteId,
    'parentId'               => $curParentDirId
);
$getDirsCountConfigArchived = array(
    'siteId'               => $siteId,
    'parentId'               => 'all',
    'archived'               => true,
    'onlyFirstArchiveParent' => true
);
if($curDirPath === DS && $mediaClass->getDirsCount($getDirsCountConfigArchived) && $canArchive) : ?>
    <ul class="nav nav-tabs">
        <li class="active"><a href="#dirs" data-toggle="tab"><?php echo $Core->i18n()->translate('Lokale Ordner') ?> <span class="badge"><?php echo $mediaClass->getDirsCount($getDirsCountConfig); ?></span></a></li>
        <li><a href="#archived" data-toggle="tab"><?php echo $Core->i18n()->translate('Archiviert') ?> <span class="badge"><?php echo $mediaClass->getDirsCount($getDirsCountConfigArchived); ?></span></a></li>
    </ul>
    <div id="dirsTabContent" class="tab-content">
        <div class="tab-pane fade active in" id="dirs">
            <?php include('list.entry_list.php'); ?>
        </div>
        <div class="tab-pane fade" id="archived">
            <?php $dataArea = 'archived'; include('list.entry_list.php'); ?>
        </div>
    </div>
<?php else: ?>
    <?php include('list.entry_list.php'); ?>
<?php endif; ?>

<!-- Modal -->
<div class="modal fade" id="listDirsModal" tabindex="-1" role="dialog" aria-labelledby="listDirsModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?php echo $Core->i18n()->translate('Ordner'); ?></h4>
            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Schließen') ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php /* if($canEdit) : ?>
    <script type="text/javascript">
        var fileElementName = '<?php echo $fileElementName; ?>';
    </script>
<?php endif; */ ?>
