<?php
/**
 * Media Model View Dirs Index Entry List
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $mediaClass Media
 * @var $accClass Acc
 * @var $plugins Plugins
 * @var $pitsForms PitsForms
 */

$dataArea = (isset($dataArea)) ? $dataArea . '/' : '';
$curParentDirId = (isset($curParentDirId)) ? $curParentDirId : 0;

$parentDirUrlSlug = ($curParentDirId > 0) ? 'id_media_dirs__parent/' . $curParentDirId . '/' : '';

$dtSourceUrl = $Mvc->getModelAjaxUrl() . '/dirs/dtsource/' . $parentDirUrlSlug . $dataArea;

$site = $Core->Sites()->getSite();
$siteId = (count($site)) ? $site['id'] : 0;

$countGalleryStyles = $mediaClass->getGalleryStylesCount($siteId);

?>


<div class="table-responsive">
    <table
        class="dir-list table table-hover table-striped table-condensed"
        data-datatable
        data-dtsource="<?php echo $dtSourceUrl; ?>"
        width="100%"
    >
        <thead>
        <tr>
            <th data-dtsortable="0"></th>
            <th data-dtdefaultsort="asc">
                <?php echo $Core->i18n()->translate('Name'); ?>
            </th>
            <?php if($dataArea === ''): ?>
            <th data-dtsortable="0">
                <?php echo $Core->i18n()->translate('Passwort'); ?>
            </th>
            <?php endif; ?>
            <th>
                <?php echo $Core->i18n()->translate('Datum'); ?>
            </th>
            <th>
                <?php echo $Core->i18n()->translate('erstellt von'); ?>
            </th>
            <?php if($dataArea === ''): ?>
                <?php if($countGalleryStyles): ?>
                    <th>
                        <?php echo $Core->i18n()->translate('Galerie Stil'); ?>
                    </th>
                <?php endif; ?>
            <?php endif; ?>
            <th class="text-center">
                <?php echo ($dataArea === '') ? $Core->i18n()->translate('Aktiv') : $Core->i18n()->translate('Backup OK'); ?>
            </th>
            <th data-dtsortable="0"></th>
        </tr>
        </thead>
        <tbody>
            <!-- will be replaced by DataTables -->
        </tbody>
    </table>
</div>
