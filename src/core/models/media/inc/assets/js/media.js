mediaModelAjaxUrl = typeof mediaModelAjaxUrl !== 'undefined' ? mediaModelAjaxUrl   : '';
fileElementName   = typeof fileElementName   !== 'undefined' ? fileElementName     : 'file';
maxUsage          = typeof maxUsage          !== 'undefined' ? Number(maxUsage)    : 0;
isUsage           = typeof isUsage           !== 'undefined' ? Number(isUsage)     : 0;
maxFileSize       = typeof maxFileSize       !== 'undefined' ? Number(maxFileSize) : 0;
chunkedFileUpload = typeof chunkedFileUpload !== 'undefined' ? chunkedFileUpload   : 0;
mediaGalleriesStyles_logoBaseUrl = typeof mediaGalleriesStyles_logoBaseUrl !== 'undefined' ? mediaGalleriesStyles_logoBaseUrl : '';

var allowedMimeTypes = [
    'text/plain',
    'image/jpeg',
    'image/tiff',
    'image/gif',
    'image/png',
    'video/mp4',
    'video/mpeg',
    'video/x-mpeg',
    'video/quicktime',
    'application/pdf',
    'application/msexcel',
    'application/msword',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'audio/mpeg',
    'audio/mpeg3',
    'audio/x-mpeg-3',
    'audio/mp3',
    'application/zip',
    'application/x-zip-compressed',
    'application/octet-stream'
];

function initUploader() {
    if(!chunkedFileUpload) {
        initDropzone();
    } else {
        initFineUploader();
    }
}

function initDropzone() {
    var fileUploadEle = $('#media-file-upload');

    if(mediaModelAjaxUrl != '' && fileUploadEle.length && !fileUploadEle.hasClass('dz-clickable')) {

        var dirId = fileUploadEle.data('dirid');
        var dirFolderName = fileUploadEle.data('dirfoldername');

        // Drozone Config
        var fileAddFolderParam = (dirId > 0) ? dirId : dirFolderName;
        var maxAddedFileSize = (isUsage < maxUsage) ? (maxUsage - isUsage) : 0;

        var dzConfig = {
            url: mediaModelAjaxUrl + '/dirs/fileadd/' + fileAddFolderParam,
            paramName: fileElementName,
            autoProcessQueue: false,
            maxFilesize: maxFileSize,
            acceptedFiles: allowedMimeTypes.join(),
            dictInvalidFileType: core.i18n.translate('Dieser Dateityp ist nicht erlaubt'),
            timeout: 0,
            previewTemplate :
            '<div class="col-xs-12 col-md-6 dz-preview dz-file-preview">' +
            '<div class="thumbnail clearfix">' +
            '<div class="col-sm-2 col-md-3">' +
            '<img data-dz-thumbnail="" class="img-thumbnail center-block" width="100" alt="secondarytile.png" src="">' +
            '</div>' +
            '<div class="col-sm-10 col-md-9">' +
            '<button class="close" type="buttion" aria-label="' + core.i18n.translate('Schließen') + '" data-dz-remove=""><span aria-hidden="true">×</span></button>' +
            '<p class="dz-filename one-line-ellipsis" data-dz-name></p>' +
            '<div class="dz-progress progress"><div class="dz-upload progress-bar" role="progressbar" data-dz-uploadprogress></div></div>' +
            '</div>' +
            '</div>' +
            '</div>'
        };

        if(chunkedFileUpload) {
            dzConfig.chunking             = 1;
            dzConfig.forceChunking        = 1;
            dzConfig.parallelChunkUploads = 1;
            dzConfig.retryChunks          = 1;
            dzConfig.chunkSize            = 20971520; /* 20 MB*/
            dzConfig.retryChunksLimit     = 3;
            //   dzConfig.chunksUploaded       = function (file, done) {
            //     // All chunks have been uploaded. Perform any other actions
            //     dz.removeFile(file);
            //     console.log('all chunks uploaded');
            //       done();
            // };
            // dzConfig.params               = function (files, xhr, chunk) {
            // console.log('files',files);
            // console.log('xhr',xhr);
            // console.log('chunk',chunk);
            // };
        }

        // init dropzone
        Dropzone.autoDiscover = false;
        var dz = new Dropzone("#media-file-upload", dzConfig);

        $('.dz-upload-start').click(function(){
            dz.processQueue();
            fileAdd_holdSess();
        });
        $('.dz-upload-cancel').click(function(){
            dz.removeAllFiles();
            $('.mediabrowser .uploadArea .uploadActionBtns .btn').hide();
        });

        dz.on("addedfile", function(file) {
            if (file.status === 'added') {
                if(file.size > (maxAddedFileSize)) {
                    core.bs_alert.danger('Dateigröße überschreitet verfügbaren Speicherplatz.', 'autoclose', '#listDirsModal .modal-body');
                    this.removeFile(file);
                    $('.mediabrowser .uploadArea .uploadActionBtns .btn').hide();
                } else {
                    if(file.size > maxFileSize) {
                        core.bs_alert.danger(core.i18n.translate('Dateigröße überschreitet maximalen Wert.') + ' (' + file.name + ')', 'autoclose', '#listDirsModal .modal-body');
                        this.removeFile(file);
                        $('.mediabrowser .uploadArea .uploadActionBtns .btn').hide();
                    } else {
                        $('.mediabrowser .uploadArea .uploadActionBtns .btn').show().css("display", "inline-block");
                        $(file.previewElement).attr('title', file.name);
                    }
                }
            }
        });

        dz.on("removedfile", function(file) {
            var queuedFiles = dz.getQueuedFiles();
            if(queuedFiles.length === 0) {
                $('.mediabrowser .uploadArea .uploadActionBtns .btn').hide();
            }
        });

        dz.on("processing", function() {
            dz.options.autoProcessQueue = true;
        });

        dz.on("sending", function(file, xhr, formData) {
            // nothing for now
        });

        dz.on("success", function(file, response, event) {
            var result = response;
            if(result.success) {
                dz.removeFile(file);
            } else {
                dz.removeFile(file);
                if(typeof response.message !== 'undefined') {
                    core.bs_alert.danger(result.message, 'autoclose', '#listDirsModal .modal-body');
                }
            }
        });

        dz.on("error", function(file, response, event) {
            var errorMsg = (core.helper.isObject(response)) ? response.message : response;
            core.bs_alert.danger(errorMsg, 'autoclose', '#listDirsModal .modal-body');
            dz.removeFile(file);
            $('.mediabrowser .uploadArea .uploadActionBtns .btn').hide();
        });

        dz.on("queuecomplete", function(file, response, event) {
            $('.mediabrowser .uploadArea .uploadActionBtns .btn').hide();
            loadMedia(dirFolderName);

            $('#listDirsModal').attr('data-dtreload',1);
            dz.options.autoProcessQueue = false;
        });

        $("#listDirsModal").on('hide.bs.modal', function (event) {
            if($('.dz-processing').length) {
                event.preventDefault();
                core.ays({
                    areYouSureTxt: core.i18n.translate('There are uploads in progress. Cancel upload?'),
                    confirmCallback: function() {
                        dz.removeAllFiles(true);
                        $("#listDirsModal").modal('hide');
                    },
                });
            }
        });
    }

    if(fileUploadEle.closest('.modal-content').length) {
        var modalContent = fileUploadEle.closest('.modal-content');
        var dropTargetId = fileUploadEle.attr('id');
        $(document).bind({
            dragover: function(e) {
                e = e || event;
                e.preventDefault();
                if (e.target === this) {
                    return;
                }
                modalContent.addClass('drag-hover');
            },
            dragleave: function(e) {
                e = e || event;
                e.preventDefault();
                if ((e.originalEvent.pageX !== 0) && (e.originalEvent.pageY !== 0)) {
                    return false;
                }
                modalContent.removeClass('drag-hover');
            }
            ,
            drop: function(e) {
                e = e || event;
                if (e.target.id !== dropTargetId) {
                    e.preventDefault();
                    return false;
                }
            }
        });
    }
}

function initFineUploader() {

    /** Add FineUploader CSS */
    if(!$('.fucss').length) {
        var href = thisBaseUrl + 'core/models/media/inc/node_modules/fine-uploader/fine-uploader/fine-uploader.min.css';
        var fucss_style=document.createElement('link');
        fucss_style.setAttribute('rel', 'stylesheet');
        fucss_style.setAttribute('class', 'fucss');
        fucss_style.type = 'text/css';
        fucss_style.href = href;
        document.head.appendChild(fucss_style);

        $('head').append(
            '<style>' +
            '.qq-upload-drop-area {' +
            ' background: rgba(0,0,0,0);' +
            '}' +
            '.qq-upload-drop-area.dz-drag-hover {' +
            ' background: rgba(0,128,0,0.25);' +
            '}' +
            '.qq-upload-cancel {' +
            ' font-size: 34px;' +
            ' font-weight: 300;' +
            ' margin: 0;' +
            '}' +
            '.qq-progress-bar-container {' +
            ' margin-bottom: 5px;' +
            '}' +
            '</style>'
        );
    }

    /** Add FineUploader Core JavaScript */
    if(!$('.fuimp').length) {
        var url = thisBaseUrl + 'core/models/media/inc/node_modules/fine-uploader/fine-uploader/fine-uploader.min.js';
        var fuimp_script=document.createElement('script');
        fuimp_script.type='text/javascript';
        fuimp_script.setAttribute('class', 'fuimp');
        fuimp_script.src=url;

        document.body.appendChild(fuimp_script);
    }

    /** Add FineUploader Template */
    if(!$('#qq-template').length) {
        var previewTemplate =
            '<div class="col-xs-12 col-md-6 dz-preview dz-file-preview">' +
            '<div class="thumbnail clearfix">' +
            '<div class="col-sm-2 col-md-3">' +
            '<div class="qq-thumbnail-wrapper">' +
            '<img class="qq-thumbnail-selector img-thumbnail center-block" qq-max-size="120" width="120" height="120">' +
            '</div>' +
            '</div>' +
            '<div class="col-sm-10 col-md-9">' +
            '<button class="qq-upload-cancel-selector qq-upload-cancel close" type="buttion" aria-label="' + core.i18n.translate('Schließen') + '" data-dz-remove=""><span aria-hidden="true">×</span></button>' +
            '<div class="qq-file-info">' +
            '<p class="dz-filename one-line-ellipsis qq-upload-file-selector qq-upload-file"></p>' +
            '</div>' +
            '<div class="qq-progress-bar-container-selector qq-progress-bar-container progress"><div role="progressbar" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar dz-upload progress-bar" role="progressbar" data-dz-uploadprogress></div></div>' +
            '<span class="qq-upload-spinner-selector qq-upload-spinner"></span>' +
            '<span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>' +
            '<span class="qq-upload-size-selector qq-upload-size"></span>' +
            '</div>' +
            '</div>' +
            '</div>';

        var futpl =
            '<div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text="Drop files here">\n' +
            // '    <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">\n' +
            // '        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>\n' +
            // '    </div>\n' +
            '    <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>\n' +
            '        <span class="qq-upload-drop-area-text-selector"></span>\n' +
            '    </div>\n' +
            '    <div class="qq-upload-button-selector qq-upload-button">\n' +
            '        <div>Upload a file</div>\n' +
            '    </div>\n' +
            '    <span class="qq-drop-processing-selector qq-drop-processing">\n' +
            '        <span>Processing dropped files...</span>\n' +
            '        <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>\n' +
            '    </span>\n' +
            '    <div class="qq-upload-list-selector qq-upload-list row" role="region" aria-live="polite" aria-relevant="additions removals" id="media-file-upload">\n' +
            previewTemplate +
            '    </div>\n' +
            '\n' +
            '    <dialog class="qq-alert-dialog-selector">\n' +
            '        <div class="qq-dialog-message-selector"></div>\n' +
            '        <div class="qq-dialog-buttons">\n' +
            '            <button type="button" class="qq-cancel-button-selector">Close</button>\n' +
            '        </div>\n' +
            '    </dialog>\n' +
            '\n' +
            '    <dialog class="qq-confirm-dialog-selector">\n' +
            '        <div class="qq-dialog-message-selector"></div>\n' +
            '        <div class="qq-dialog-buttons">\n' +
            '            <button type="button" class="qq-cancel-button-selector">No</button>\n' +
            '            <button type="button" class="qq-ok-button-selector">Yes</button>\n' +
            '        </div>\n' +
            '    </dialog>\n' +
            '\n' +
            '    <dialog class="qq-prompt-dialog-selector">\n' +
            '        <div class="qq-dialog-message-selector"></div>\n' +
            '        <input type="text">\n' +
            '        <div class="qq-dialog-buttons">\n' +
            '            <button type="button" class="qq-cancel-button-selector">Cancel</button>\n' +
            '            <button type="button" class="qq-ok-button-selector">Ok</button>\n' +
            '        </div>\n' +
            '    </dialog>\n' +
            '</div>';

        var futpl_script=document.createElement('script');
        futpl_script.type='text/template';
        fuimp_script.setAttribute('id', 'qq-template');

        document.body.appendChild(futpl_script);
        $('#qq-template').html(futpl);
    }

    /** Init FineUploader */
    var uploadArea = $('.uploadArea');
    var fileUploadEle = $('#media-file-upload');

    var selectFilesBtn = $('.mediabrowser .action-wrapper .btn').has('.fa-cloud-upload');
    var uploadFilesBtn = $('.mediabrowser .uploadArea .uploadActionBtns .btn-success');
    var cancelFilesBtn = $('.mediabrowser .uploadArea .uploadActionBtns .btn-danger');

    selectFilesBtn.removeAttr('onclick');

    if(!$('.fuArea').length) {
        uploadArea.prepend('<div class="fuArea"></div>');
    }

    var fuArea = $('.fuArea');

    var dirId = fileUploadEle.data('dirid');
    var dirFolderName = fileUploadEle.data('dirfoldername');

    // Drozone Config
    var fileAddFolderParam = (dirId > 0) ? dirId : dirFolderName;
    var maxAddedFileSize = (isUsage < maxUsage) ? (maxUsage - isUsage) : 0;

    setTimeout(function(){
        if(mediaModelAjaxUrl !== '' && typeof qq.FineUploader !== 'undefined') {

            var fu_config = {
                element: fuArea[0],

                request: {
                    endpoint: mediaModelAjaxUrl + '/dirs/fileadd/' + fileAddFolderParam,
                    inputName: 'file'
                },

                forceMultipart: true,

                validation: {
                    acceptFiles: allowedMimeTypes.join()
                },

                autoUpload: false,

                button: selectFilesBtn[0],

                // debug: true,

                chunking: {
                    partSizeXX: 100000,
                    enabled: true,
	                success: {
	                    endpoint: mediaModelAjaxUrl + '/dirs/fileadd/' + fileAddFolderParam + '?qqchunkuploaddone'
	                },
                    concurrent: {
                        enabled: true
                    }
                },

                classes: {
                    dropActive: 'dz-drag-hover'
                },

                callbacks: {
                    onSubmit: function(id, name) {
                        var file = fu.getFile(id);
                        if(file.size > (maxAddedFileSize)) {
                            core.bs_alert.danger('Dateigröße überschreitet verfügbaren Speicherplatz.', 'autoclose', '#listDirsModal .modal-body');
                            fu.cancel(id);
                            $('.mediabrowser .uploadArea .uploadActionBtns .btn').hide();
                        } else {
                            if(file.size > maxFileSize) {
                                core.bs_alert.danger(core.i18n.translate('Dateigröße überschreitet maximalen Wert.') + ' (' + file.name + ')', 'autoclose', '#listDirsModal .modal-body');
                                fu.cancel(id);
                                $('.mediabrowser .uploadArea .uploadActionBtns .btn').hide();
                            } else {
                                $('.mediabrowser .uploadArea .uploadActionBtns .btn').show().css("display", "inline-block");
                            }
                        }
                    },

                    onSubmitted: function(id, name) {
                        var fileItem = $('.qq-file-id-'+id);
                        var thWrapper = fileItem.find('.qq-thumbnail-wrapper');
                        var canvas = document.createElement('canvas');
                        canvas.width = 70;
                        canvas.height = 70;
                        canvas.setAttribute('class', 'img-thumbnail center-block');
                        thWrapper.append(canvas);
                        fu.drawThumbnail(id, canvas, 70, false);
                        thWrapper.find('img.img-thumbnail').hide();
                        fileItem.find('.thumbnail').css('min-height', '90px');
                    },

                    onCancel: function(id, name) {
                        var uploads = fu.getUploads();
                        var cancelledUploads = fu.getUploads({ status: qq.status.CANCELED });

                        if((uploads.length-1) === cancelledUploads.length){
                            $('.mediabrowser .uploadArea .uploadActionBtns .btn').hide();
                        }
                    },

                    onComplete: function(id, name, responseJSON, xhr) {
                        var result = responseJSON;
                        if(result.success) {
                            fu.cancel(id);
                        } else {
                            fu.cancel(id);
                            if(typeof response.message !== 'undefined') {
                                core.bs_alert.danger(result.message, 'autoclose', '#listDirsModal .modal-body');
                            }
                        }
                    },

                    onAllComplete: function(succeeded, failed) {
                        $('.mediabrowser .uploadArea .uploadActionBtns .btn').hide();
                        loadMedia(dirFolderName);

                        $('#listDirsModal').attr('data-dtreload',1);
                    },

                    onError: function(id, name, errorReason, xhr) {
                    	// var errMsg = errorReason;
                    	//
                    	// console.log('errorReason', errorReason);
                    	// console.log('xhr', xhr);

                        if(id !== null) {
                            core.bs_alert.danger(errorReason, 'autoclose', '#listDirsModal .modal-body');
                            fu.cancel(id);
                            $('.mediabrowser .uploadArea .uploadActionBtns .btn').hide();
                        }
                    }
                }
            };

            var fu = new qq.FineUploader(fu_config);

            fileUploadEle = $('#media-file-upload');

            $('#media-file-upload:not(.qq-upload-list)').removeAttr('id');
            $('#media-file-upload').click( function (event, ele) {
                if(event.target !== this) return;
                selectFilesBtn.find('input').click();
            });

            qq(uploadFilesBtn[0]).attach("click", function() {
                fu.uploadStoredFiles();
            });

            qq(cancelFilesBtn[0]).attach("click", function() {
                fu.cancelAll();
            });

            uploadFilesBtn.click(function(event, ele) {
                fileAdd_holdSess();
            });

            $("#listDirsModal").on('hide.bs.modal', function (event) {
                var processingElements = $('.qq-upload-spinner:not(.qq-hide)');
                if(processingElements.length) {
                    event.preventDefault();
                    core.ays({
                        areYouSureTxt: core.i18n.translate('There are uploads in progress. Cancel upload?'),
                        confirmCallback: function() {
                            fu.cancelAll();
                            $("#listDirsModal").modal('hide');
                        },
                    });
                }
            });

            if(fileUploadEle.closest('.modal-content').length) {
                var modalContent = fileUploadEle.closest('.modal-content');
                var dropTargetId = fileUploadEle.attr('id');
                $(document).bind({
                    dragover: function(e) {
                        e = e || event;
                        e.preventDefault();
                        if (e.target === this) {
                            return;
                        }
                        modalContent.addClass('drag-hover');
                    },
                    dragleave: function(e) {
                        e = e || event;
                        e.preventDefault();
                        if ((e.originalEvent.pageX !== 0) && (e.originalEvent.pageY !== 0)) {
                            return false;
                        }
                        modalContent.removeClass('drag-hover');
                    }
                    ,
                    drop: function(e) {
                        e = e || event;
                        if (e.target.id !== dropTargetId) {
                            e.preventDefault();
                            return false;
                        }
                    }
                });
            }
        }
    }, 2000);
}

function fileAdd_holdSess() {
    thisBaseUrl    = typeof thisBaseUrl    !== 'undefined' ? thisBaseUrl    : '';
    thisCurrLang   = typeof thisCurrLang   !== 'undefined' ? thisCurrLang   : '';
    getDefaultLang = typeof getDefaultLang !== 'undefined' ? getDefaultLang : '';

    var langUrlSlug = (getDefaultLang != '' && thisCurrLang != getDefaultLang) ? thisCurrLang + '/' : '';

    var holdSessInterval = setInterval(holdSess, 900000); /* 900000 Miliseconds => 15 Minutes */

    function getLanguages() {
        var AjaxUrl = '';
        if(thisBaseUrl != '') {
            var baseUrl = thisBaseUrl + 'ajax/' + langUrlSlug;
            var i18nUrlPath = 'system/i18n';

            AjaxUrl = baseUrl + i18nUrlPath;
        }

        var i18nUrlGetParam = 'getlanguages';
        var getUrl = AjaxUrl + '/' + i18nUrlGetParam;

        $.get(getUrl, function( data ) {
            if (data.success) {
                var result = data.result;
                if (result.success) {
                    return result.getlanguages;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        });
    }

    function holdSess() {
        var progressElements = $('.dz-preview .dz-progress, .qq-upload-spinner:not(.qq-hide)');
        if (progressElements.length) {
            console.log(getCurrendDateAndTime(),'Upload still in progress.');
            getLanguages();
        } else {
            clearInterval(holdSessInterval);
        }
    }
}

function getCurrendDateAndTime() {
    var currentdate = new Date();

    var dateDay   = currentdate.getDate();
    var dateMonth = (currentdate.getMonth()+1);
    var dateYear  = currentdate.getFullYear();

    var timeHour   = (currentdate.getHours() < 10) ? '0' + currentdate.getHours() : currentdate.getHours();
    var timeMinute = (currentdate.getMinutes() < 10) ? '0' + currentdate.getMinutes() : currentdate.getMinutes();
    var timeSecond = (currentdate.getSeconds() < 10) ? '0' + currentdate.getSeconds() : currentdate.getSeconds();

    var date = dateDay + '/' + dateMonth + '/' + dateYear;
    var time = timeHour + ':' + timeMinute + ':' + timeSecond;

    var datetime = date + ' @ ' + time;

    return datetime;
}

function loadMedia(dirFolderName) {
    dirFolderName = typeof dirFolderName !== 'undefined' ? dirFolderName : '';
    var mcSelector = '#mc-' +core.helper.trim(dirFolderName.replaceAll('\\\\','/'),'/').replaceAll('/','_');
    // console.log(mcSelector);

    if(dirFolderName != '' && mediaModelAjaxUrl && $(mcSelector).length) {
        var getAjaxMediaFolderContentUrl = mediaModelAjaxUrl + '/dirs/mediacontent/' +core.helper.trim(dirFolderName.replaceAll('\\\\','/'),'/') + '/';

        $.get( getAjaxMediaFolderContentUrl, function( data ) {

            if (data.success = true) {

                var mediacontent = '';

                try {
                    mediacontent = data.result;
                } catch (e) {
                    // console.log("Error:",e)
                }

                $(mcSelector).html(data.result);

                loadLazy();

                showPlayer();

                if($('.delete-media').length) {
                    $('.delete-media').each(function(i,ele) {
                        var dmBtn = $(ele);
                        dmBtn.click(function () {
                            confirmDeleteMedia(dmBtn);
                        });
                    });
                }

                if($('.insert-media').length) {
                    $('.insert-media').each(function(i,ele) {
                        var imBtn = $(ele);
                        imBtn.click(function () {
                            insertThemeLogoMedia(imBtn);
                        });
                    });
                }

                if($('#listDirsModal .drag-hover').length){
                    $('#listDirsModal .drag-hover').removeClass('drag-hover');
                }

                if($('#listDirsModal .thumbnail-wrapper .togglebutton').length) {
                    if (
                        $('#listDirsModal [data-action="select_all"]').length &&
                        $('#listDirsModal [data-action="del_selected"]').length
                    ) {
                        $('#listDirsModal [data-action="select_all"]').css('display', 'inline-block');
                        $('#listDirsModal [data-action="del_selected"]').css('display', 'inline-block');
                    }
                } else {
                    if (
                        $('#listDirsModal [data-action="select_all"]').length &&
                        $('#listDirsModal [data-action="del_selected"]').length
                    ) {
                        $('#listDirsModal [data-action="select_all"]').hide();
                        $('#listDirsModal [data-action="del_selected"]').hide();
                    }
                }

                if($('#listDirsModal .thumbnail-wrapper').length) {
                    if ($('#listDirsModal [data-action="toggle_listview"]').length) {
                        $('#listDirsModal [data-action="toggle_listview"]').css('display', 'inline-block');
                    }
                } else {
                    if ($('#listDirsModal [data-action="toggle_listview"]').length) {
                        $('#listDirsModal [data-action="toggle_listview"]').hide();
                    }
                }
            }
        });
    }
}

function deleteMedia(dirFolderName, fullFileName) {
    dirFolderName = typeof dirFolderName !== 'undefined' ? dirFolderName : '';
    fullFileName = typeof fullFileName !== 'undefined' ? fullFileName : '';
    if(dirFolderName != '' && fullFileName != '') {
        var getAjaxMediaFolderContentUrl = mediaModelAjaxUrl + '/dirs/filedelete/' + dirFolderName;

        var data = {};
        data[fileElementName] = fullFileName;

        // $.post( getAjaxMediaFolderContentUrl, { fullFileName: fullFileName } ).done( function( data ) {
        $.post( getAjaxMediaFolderContentUrl, data ).done( function( data ) {
            if (data.success = true) {
                var result = data.result;

                if('files' in result) {
                    var success_files_list = '';
                    var failed_files_list = '';
                    $.each(result.files, function (file_fullFileName, file_result) {
                        if(file_result.success) {
                            success_files_list += '<li>' + file_fullFileName + '</li>';
                        } else {
                            failed_files_list += '<li>' + file_fullFileName + '</li>';
                        }
                    });

                    var success_message = '';
                    if(success_files_list !== '') {
                        success_message = core.i18n.translate('Folgende Dateien wurden erfolgreich gelöscht');
                        success_message += '<ul>';
                        success_message += success_files_list;
                        success_message += '</ul>';
                        core.bs_alert.success(success_message, '', '#listDirsModal .modal-body');
                    }

                    var failed_message = '';
                    if(failed_files_list !== '') {
                        failed_message = core.i18n.translate('Folgende Dateien konnten nicht gelöscht werden');
                        failed_message += '<ul>';
                        failed_message += failed_files_list;
                        failed_message += '</ul>';
                        core.bs_alert.danger(failed_message, '', '#listDirsModal .modal-body');
                    }
                } else {
                    if (result.success) {
                        core.bs_alert.success(result.message, '', '#listDirsModal .modal-body');

                        $('#listDirsModal').attr('data-dtreload', 1);
                    } else {
                        core.bs_alert.danger(result.message, '', '#listDirsModal .modal-body');
                    }
                }

                loadMedia(dirFolderName);
            }
        });
    } else {
        core.bs_alert.success('Keine Daten zum löschen übergeben.', '', '#listDirsModal .modal-body');
    }

}

function confirmDeleteMedia(delBtn) {
    var dirFolderName = delBtn.data('dirfoldername');
    var fullFileName  = delBtn.data('filefullname');

    // console.log('dirFolderName', dirFolderName);
    // console.log('fullFileName', fullFileName);

    core.ays().on('core.ays.confirmed', function() { deleteMedia(dirFolderName, fullFileName); $(this).unbind('core.ays.confirmed'); });
}

function deleteSelectedMediaElements() {
    var checked_toggleButton_eles = $('#listDirsModal .mediabrowser .media-content .togglebutton input[type="checkbox"]:checked');

    if(checked_toggleButton_eles.length) {
        core.ays().on('core.ays.confirmed', function() {

            var dirFolderName = '';
            var delFiles = [];

            checked_toggleButton_eles.each(function (i, ele) {
                var toggleButtonEle = $(this);
                var thumbnailWrapper = toggleButtonEle.closest('.thumbnail-wrapper');
                var delBtn = thumbnailWrapper.find('.delete-media');

                if(delBtn.length) {

                    dirFolderName = delBtn.data('dirfoldername');
                    var fullFileName  = delBtn.data('filefullname');

                    delFiles.push(fullFileName);
                }
            });

            deleteMedia(dirFolderName, delFiles);

            $(this).unbind('core.ays.confirmed');
        });
    } else {
        core.bs_alert.warning('Keine Daten zum löschen übergeben.', 'autoclose', '#listDirsModal .modal-body');
    }
}

function loadLazy() {

    var lazyConfig = {
        afterLoad: function(element) {
            element.removeClass('loader');
        },
        placeholder: "data:image/gif;base64,R0lGODlhQABAAKUAAMx+BOTChPTixNyiROzSpPzy5NyybNSSJPTq1MyKFOzKlNyqVOzatOS6dPz69NSaPPzu3MyGFOzGlPTm1NymVOzWtPz29OSybNSWNNSOJOzOpNyuZPTavOS+hMyCBOTGjPTmzNymTOzWrPz27NSWLPTu3MyOHOzOnNyuXOS+fPz+/NSePOS2bPTevOTCjPTizNyiTOzSrPzy7NSSLPTq3MyKHOzKnNyqXOS6fPz6/Pzu5MyCDNSeROS2dPTexP///yH/C05FVFNDQVBFMi4wAwEAAAAh+QQICQAAACwAAAAAQABAAAAG/sCfcEgsGocFUYryMMGIqqjqSK1arz9BY7YDeL0rYk41zo2n2HR69MHsdh44wAOGkqOOXB6t7hs1b3RdXV9hQ2VkZg6LeX6OQioPb3BwdHSGkHcOKnl5FnyPVSUwNEMClACDhWJ3enqMoVciGTsURCiBcqtDUYidn7wjObGQOBHHERxDJRFycpesY52Lw0MOIxYOsTkbyMckfAGTcbuQZdSLYtgjMhaPOQsJCRHzxxLWJoM1PB0fAkWtGFUTYmEEO4Pu+qjYUK/esQwFhogw0eEFqCOJ0h0yeFBGCW1qUmSIMJIeMg8soDgaE6ygjIPsEsqqkaDkvJLHQBAr4sAl/rsSP2WArAIhQ42RNb0dWzBiJy8LL6OOAFrC40WeLjIklUeS3j2nRK69BCrD49SXWEpsGMmWngiwRhxELUHV41AsLXggTRAD7hEHBcbavUqkhUwhDk7MyPDVb9yqJYBZsUACA4GBQkaccFxl6lUVMkDZMJEhA49/nNN8HqEDKBEeM0wsNnGhRGpHFlrTgGBbCIgZB2LPnuHi7m0qgEvsplECAsgAwqOXZnC8CujmyyHQoBHxB4wZ4MPL5kG4uhDtyxEggIDAtozw0WN/MF9Fhnb1ECaop5GDAQnw/8GHAH3I4affbhNAMIICMwRIggkYzPBAeQTSgECC+ukHggwG/mDwX2UNmmAAgVXooN4EGUIAgg4oYODiDC66OB+JRxSQIgg4TkADDx7GWBkGb9FohAwT4PgCCEWCMKCQTDbp5JNQYhHCA1RWSWWQTxYgwAsvbNnlkRtYaaUCUZbQ5ZYCoIkADlau8MAADzQQ5QRp1lknDTa4Caebb8JAoXkq+CCAoISmWUILer6p6ApLNjlCCz4wMOikPhjEg6KKDsCDDU/SwICgkkY66DA3wAknD3DCsMCftwXawqeRgqrTDwo8wAOquPIAA3VMqjDBq58G60NvCGhqrK4wnGAcjSO8IOynMm2gKQy6dtBblDIw8KqgRMSgKw8XzNokS1B4yoAO/mFRsAADF1nAK6A+tCCDETkggNkPLxz2gwoc3EABAfRNIMLALyyLxQQsUKAuBe+mRoMIMQwsAgcgsIqECyEorO4NN7RwGwIxEACxxBVwcC1yGiyg8AIq37AAxzFYrIYKLxBgc8Q4SzyBFQX4uzIFLnOMwg0uNAWWDBqccDMBEQ/MgQgGE8GAyi1zLPQGG6ALFwMEaGAz0zGELYLWV6QM9MtC37DBZrz4kcME3f3gQNdehyyCyOJeoYILQV+9wQUygdBADCX8mQMELdjwgQh81Oz11zGgNnMAVg+N9bs54GBADz00YIMIPpSSGdcSfPCBAh/Y0GgOXZ9Qdwsyi/HB5g2Wb9AAHxwYwMLmDRiQAg5s/1CAC8SXXroNJwxUgtJe+xA7QASojfXOme3OuQE4ZA/8Mi4EUPrpqfsjddJ5hyKAASg09sMJuhvQwPu+by+EDt57/8HxH5xg9A8yEHAyMcOLGw0McIHNYc8AHche8ErQPdOVDnUnsEEFwnIcFQTAer3LXgMUiIQAdE8CAbhf+Gzwv9sIoIC7yyD2OCgEBgYghMY7HfIaVR0HMCAAPcBe5zQovx+UAIQgFKECNACCe9GnBBwIAPY2yEIfvrB+JxhWlOQ2Aa6ZTgNDkMEJYuADBETND0EAACH5BAgJAAAALAAAAABAAEAAhsx+BOTChNyiRPTixNSSJOzSpNyyZPzy5MyKFPTq1OS6dOzKlNSaNOzatNyqVPz69MyGDOSybPzu3OzGlPTm1NSWNOzWtPz29NSOJOS+hOzOpNSeRPTavNyuZMyCBOTGjNymVPTmzNSWLOzWrPz27MyOHPTu3OS+fOzOnNSePNyuXPz+/OS2bPTevOTCjNyiTPTizNSSLOzSrNyybPzy7MyKHPTq3OS6fOzKnNSaPNyqXPz6/MyGFPzu5MyCDOS2dPTexP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf+gEGCg4SFhoMHDQEdAjEggyEbOhkjB4eXmJmaIRk5NTwIPKIvgwMQPqceIj8wmq6vgxc4KTUIGKA8tzykgqY+v78ePjEfJLDHhwUlGLe2oqIIvEEDwD4eAD4A2jwLyN6CKy+1t82hu6U8qMHZ1wAj364SOiaDMBg1zbqjpafW2O0eNsDTZKEChg6EDGBwBgpBKGkwql3z5yMBoRUDB60IEKNEDAINEC3LZe6WNGqpfmkDMOPiipcDd/z42DHGBoyCJtQY9+xcL2ATAXhAQOPijpc4ke0wEKNpU48oYjEgh0FHABQDEN0AUcNauw8uVxw9imzFTBE1n1YwJqhggBD+SQ+tgHGjBIAKO8KK3XvsA9oYf9PeuKi0ABCXY/eSddWgQgURFZw69Wgxo97ED3Y8iHuoRwrHMR4DLhE5xgy2lsEl3qFZbGZNDybkgAwZsNOoqQu9xKw58wNXB2bkcAy5RI6QuQuN1dy79TEYIBw7Rp5c99EHvjkbGnBBd4ENFXBXN8R8s6sHG16M4ExCxnhMK8zr/k2oQI77Olq9z/jgwoW8g+ggwH0DKtDDft5odsEDJHQnSAI5pJCDABXct8EC9CGYSXwNkuDhBTgtsMGAOYw4YQ6HaYhJfx22SF8HJQ64QYQ5gKCdirF42OKHQZAwookDDigejoawqCMNJCD+uQIQApj444g2EHnJCklWiSQNNOyAQnpcvtDkI1JecqWVJJiw0QtdcjlYmId4aEKSbyJJwgwvoAkCmnUOyeYgD4xJw5tvdgDCBoOCUCh1e8Ziwp+AYklPopBGKumklBpigKGYYtoCpReY0IOnoPZwwFYOlGoqCAVQ+qcEoYL6gQMg6BBrrDq4QOkBoJrAKqs0yKDDr8Dq4IABN4ZpgwTHHmuCriTA4ICwwf76KKQXSJCADQkgi6wNDEargg4qdOBepAdcay22yWL0w7cqfNvBtz8Uq+IKCSRAQb3W4itIATp08K6/EYSr354r9ECBBPceTIENRQVhwr8dRNBvBCP+ZJjoBfjea22GCkTcbwcLWFIpDfZScDAhDfjbwQkSSArgReVSgFoQD0TAQgsvC/LAplLa6yAhD/TAGQUWB7HkDTNwQKQJMIQwALffJBBABDMo8ENWGjI9wABNw3BAzpmQgAPVPyhQNSsIas311jAMQIHIK1rAwgxVz1D2Dzco0IC830gARAsDAMG20yHAMO0hNJhd9gxI533D4xrMnNEFDbQAhOCCsz1ACEUbEoLdZzf++AkZHG6ZDS00MIDqW6/dMEFWM67A47RbQNgxO0iA2gNAqH55722bDh8KeM9O+wkBZJjAAg0cwPcKNITQgAUtJGVC75i3HeUxK+Bg9fH+GWAdxA4ffDDBBwuMAEQIB+oMQwsyjBD/CBxMu8Pqva+eAN9yaTD6DSf4QFIG4ILynW8BH8CB7QRxgAIUYH70o1+GaNCA/NmAf1NqwOgysL0gXOADLjif+RKIggUGgQYOlMED5TeCBlCAENJ7WmpCEIAbpGoQIyjg+c6HAxx8gDooTOEIhsgB+qHmAkAQHjxIgILXmcAFLgjADklYQkQUQAMqZOEQLSA+o6lIAyMcIQJ9CEQHrjB+RSzi61REgQC4cYoL8KEG3iEIFGrggfGDYP2k9IAQoECH6CPhDwcRRBUWYIgyaIEEwEakA7QABYGUowkPcMcUciAEkoPUA2wPID0LFIA6JKjc2zr3jUAAACH5BAgJAAAALAAAAABAAEAAhsx+BOTChNyiRPTixNyyZNSSJOzSpPzy5PTq1OS6dMyKFOzKlNyqVNSaNOzatPz69MyGDOSybPzu3OzGlNymVPTm1NSWNOzWtPz29OS+hOzOpNyuZNSeRPTavMyCBOTGjNymTPTmzNSWLOzWrPz27PTu3OS+fNSOJOzOnNyuXNSePPz+/OS2bPTevOTCjNyiTPTizNyybNSSLOzSrPzy7PTq3OS6fMyKHOzKnNyqXNSaPPz6/MyGFPzu5MyCDOS2dPTexP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf+gEGCg4SFhoMHDhMxFDopgxUvGwEOB4eXmJmaIS4vMjIFNwUnDIMwCic8Cjw6NjCasLGDDyieIqA3uDc5pgo3q6mrFgskssaHIxafy6AnoBSDITepPKrVPBA3C8fcgis5t7cFMrmi0IKnqNbVENUX3bA9BD3Rt8zk5Lzo0+3r7S+EisEr5IBDgwiEbDC7IW7XoAGreFBb1U7CQwUGBnqboKOBBR1ABpXQEe5TrgKl9k2s5oPHj0E7dPjwkWAFvB02VOjYaYGCTUEflJkUkStlkFPr2J0QGMSADw8AfDDYwW1HAp07G3TMKGgHh3AqYnww8EpQiQA5Tqxa5WObIAz+J576iAqCqqwVJjjoELAzqwAagwp+QGD30ooKGWRA0FHYxNOocz34lLWAA9++fS0EIPRT1o4ZAwZJ4OFhJlSoAGLEciBAL9/XWrVa1EiIwsyouHFzxXSAgmUOrjHr+MGUNgy5pwEAQA2hRiZaL37ztbxzN21vGaDOXa68OwFYBzJwiH5ZQIfrh3Ak7w6Ax4fOmzbwjX4efXr2y2M8yBQCQ6EVF/hmnX2F2NAdByVo8gADOTgAXxAPvEOgYRzI4MAhD47wAgUvxBDChMZg8KA3DzJCAQi+uWAJiN2s4OIOnUlAwYw0oqjBfizG8iKMMP6kAQM0AjkjCKHlmMmLSPb+KEgMDDRJ44kxjGgkITzy+MAOVwZBQg45MNikkwNO+Z+VK8CYJQwMUpCDmmwmKKZhWJZ55QNXrjACl3hyycAGbx6J5ZxzrjBBCinkUCieH/SJCZ1//nmlCVxuYCihOYyg6CWO0kknBg/EsMEGKYCaw6chXWrIpg9wqiqOprbq6quwxqqJDQREQEAMtuJaJKypkkACBr4Ci0EAMeB6awS4Ssirr8wyiwEKyEYQbQwsJBorBjRkq22wFyBL7bc/2CBrs77SQAIND4QQww/rrstuAm66usMBNJRwrrnm7oABCzEk8EMC/cZgw4WvnmtvvSTYW4JNAQAMsA0xmPBDAFL+vnmAwvee618QF/wrcAI2hJtABa6ugMHF9dZr78YS+GuDyzYMXFirO9CAcsqFuQByzDYYUBys2JagMCEtxJzAB869OuIKD1zMahA7mJABDA8+UJbFJTztjYiFIKD1CjB8EECpU14sQQkHVJxJCTgE4LYLHxpJQg0I1FDD2SSoXQgJMwSQwdsBfEAyiyQggIAEh9t99saY7NCC324H4EIAE7gwwa72HVBBBQhwnvjd9mZCwuSRT0A55R9MMILWA2EQAucVSFAB3YhLUMPMhyDw9wR/T/5B6h8QQ2AJr78uu+F3/3xJC72bPgHwCxDszTErmAtTBSF0nv3xgMkygun+wD+/AAqslgAaDXqvQIIEAwwwuCAHhPB657HTY8wOI6AOvOAwzYCCAQaYQQtCUIMVQQ0BMGgBEBbYvu4FYQXak8Dr4mWMFXQAeh+YQWdCgAINaACAMxjBCMhGgg44oAUOGAAQ2ke1QdBAfq9LGzxW0ALg4WA2ENLA/z4YwBCS8IRASCEDB0BB2YXAgANBAAo+IL0gtMCD/wOhCHdFghM6IIjtYyGrMFAB5cEDAxdgCg1Q0MH/zSCAIwDNIDAAxCsu0AEwGAACqDSlEeiQh2g0wAio6IAr+nEAcQSkF0FUAzJ+8INnFOEI1+gAE2JRhSqEAQWNtAME2DGKPRThD1En+EYVhmBhrSIBDBwQQD2GcFdsdKQDCMg4XpUAgS3oACrdZy/cwSMQACH5BAgJAAAALAAAAABAAEAAhsx+BOTChNyiRPTixNyyZNSSJOzSpPzy5PTq1OS6dMyKFOzKlNyqVNSaNOzatPz69OSybPzu3MyGFOzGlNymVPTm1NSWNOzWtPz29OS+hNSOJOzOpNyuZNSeRPTavMyCBOTGjNymTPTmzNSWLOzWrPz27PTu3OS+fMyOHOzOnNyuXNSePPz+/OS2bPTevOTCjNyiTPTizNyybNSSLOzSrPzy7PTq3OS6fMyKHOzKnNyqXNSaPPz6/Pzu5MyCDOS2dPTexP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf+gEGCg4SFhoMlLik3HCEygwg6PyAuNYeXmJmaCAsqOx07oTsEgxUWFiMzMx0gIpqvsIMPNBwCHQKfuKEQgyIjFjOpIxqrKRixyIcOMLcdt6I7DbyCIqiq1ygzGhYpyd6CLBAdzLigutNB1cHXM9koGg7frwc3B5C2ts+h0r3A69ntZuggVEKeISA6QmQgFGCcvnO9frXz585GrxkkDILbwIAChRCuBNUI8czZPnTqUgVstxAcDBw4TrCQx2IChY4eZcwUtMGhrl0RVaEAhmLoMUE03inQoIKHNxYBGHTEGeKCLB35QtzIQSJkkBogZOwg2m6DLAs4FEiQoIGC01j+NXVQ0CHX49yCglzoSIHg7SUWFUDsWLUzyAsNSzWsVcChsCYDdHUwkDyVAghCjl+xuOC1BwocEtSKlnAD1gAOKlTQ7VjXowmNhTgoVqxW8VoammrIUM0h8uqOAY7CTpfWtu21bF9jekACggrUqiXTzTh8EIsXo5Ej99FiHggIdHvTHVDd0IbsatmmyIwJAaPUKsiXN78YeQK8lxA8KMTChQwOVs13SADIURCBJg/88IMDmT3ggYB/UWCBC4ZEoAN+QTjQggwtZIAAhLGUkFkJP0jgw2WDZCADBCvKsIAlICLDQgoa+GDjDINEIMMPHPbYwgX7xajJADvYaOMHPsj+5wCPCezYIgReCWkIBjoY6QOSSP4gSAA/NNkkjz/IJOUlD+Bw5JU+AODDCEFg4KWCTMoQz5iX/PABkmpi6YMJFSRwgwx/3sCkPXQqc6aaAADwwQUOCHqDnwn4WVqhh7AgwQeJYpomACcYcMOnfgIqg1mUHtKBoolmCgAFCzz66asnzFlqIRCoimmiDbzw6g0nvBrlrIK8cGuqiaIA7LHIJqvssodMEMCz0D7767EsVGuttSk8+0IA224LBLPXhkvCtgE46+wL1CXLAwvrtssuC0CUq628Cyzr7r0smLAtCM7yCwIIhFLLwwP4rvvABPyW+y/C8h3Lg8EDD8yuICn+LOwvwush+8DGD7wbsSBAXAzCAiPnYJHDBHNM8MRBHOAvyTmAkMMAfgHLAsERb1zYBiPLTIlwyt688cOEiBBzCjQEzKwgA3dMCA8pbIBAZjx8WOgDGLB3syEm1BwECwgYsMG0ED5QAgYlOO3NASRssEEKBpwc4wM1lHD22UHCgoELbsO9gQEGKFd2CXXXfTfBmvAgQgpRGwC3ATREbvV8GBxQOOF2o41B3odgIPbbgEMOOQkkAMG5RjwcYALml2fOXiEm+C326AaQfgGGsBG++uolmIB2CV5fIsLfkNNQO+kkVIBZMixsbp3lrRN+eiZAhE4CDdd7QIJfB7hgg4j+mdxcQwQm9EAIBiaYUMPuGACtmQORX4/8geAA4QAQLgxQgQkH4Je6CRGogA1sQD7hsEB3hEPbN1gwAPmRzgOFiYADXHA/IAzggpN7QAUqgAAERMAGADRBYR5gOfXhLhkVQJ4HzCcIB1XQAQOwYAwyyMEKBDACCCgfhtZXg+l5owceoEHDglCB+93vghYcAA0jIIIOEhCEEfALCw7gw2+UYADCKYEH7kfBGF5whrIQAQcD6EECqm95QhrABGFowSQOQG4aRMAGcfhEAFZRQAcwov3U+MU3hpGDHQzgE3twQhDxoAdqBEIbLzgAMLZQjGS0oQfBN6sHmCAGDoiBDP0b2MIKiKCJHzxA8GzWOxsgQARwtEEP6jZKbwQCACH5BAgJAAAALAAAAABAAEAAhsx+BOTChNyiRPTixNSSJNyyZOzSpPzy5MyKFPTq1OS6dOzKlNyqVNSaNOzatPz69MyGDOSybPzu3OzGlNymVPTm1NSWNOzWtPz29NSOJOS+hOzOpNyuZNSeRPTavMyCBOTGjNymTPTmzNSWLOzWrPz27MyOHPTu3OS+fOzOnNyuXNSePPz+/OS2bPTevOTCjNyiTPTizNSSLNyybOzSrPzy7MyKHPTq3OS6fOzKnNyqXNSaPPz6/MyGFPzu5MyCDOS2dPTexP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf+gEKCg4SFhoMlMQYgOAUagzctASlBJYeXmJmaJwZAOgw6FKI4gwkwHacdKgsVmq6vgw8OOCo6tZ8MFAykgqYdAr8dHSs6Bg+wyIcDHBE6HDrQFKEUvEK+pwI72TvaNMnfgiwatRy3uaHV18Id3Nsu4K41IDWlESrN0LjUpdi/2tw7IhCyBK+QCCAzFhAC8SwfKF3pOoTwxw6ghEEVdjgoGI4EwhYzEgzCYK8ctFz7ep1KtU1AAEIqLFgAwQIeixQIEUbQUFMQCZMOU1pD5e+fAIJCLsgcIQPIsWQscgCZOuPjO0E8gDxTUSAADQciBWFIgWPiCoDeBD2AYYGpBRn+BXggY7FBwQwcM+yCdDpoABAaEuRiYiEhB4UdOnoKmdD2rYzHMxRr8qAACA4Fdu0iNEBIsiseHkQM8tHArQymj1+6EqEAB97Ldz/OOMCx0AyZj3PLMCHjgqYSGlzjQHEXr+YUGGqXWnr6sQmmJixcxMTDBQoUwi9XVrBROaEcqJ2/5W2iGqYSBoS3vtvKeyEDqKPzlmGBhmdMJ0Bgd93efSEa880XQHL4PTUICzEEgEN3/hkCgnMcnKDJAxOAIIJnD8TQ4CUscNDBAIacMANSQgwQwAsB5CDhhq884BkGGpiQgUKD5IDiiQGQQCKLrtA1gg0Z2LDDIAeAAMIEASD+eaILgvGYyQAwAGmDDQhkAKIQQYCAIgg4InmDk+fNECSQVfaQAS8pHFkhkkjSCOYhD8hAZQY9IFDmkA8caSSSRr5w5ZuH4EBlmT3UicABN6y5p560AXpIEITSiYCZJAwAwgKXKuqmo4WwQGedhU7aQwAO5ACCqZde6hunh8Aw6aSf9hABCTmYmoKpporGqiFAFOqrrwJsAMKtw9aaQ1i7EjLBq4VC0AMEMiQr7bTUVmvtITSksMEG2nK7wZfVTvADBOOWC60DGxiQgroGpKsrtTOQ+8O89O7gwrrpGqCvAVdR2wG95AasQwX75kuDASRUywMEH/zQML0/aHDCwRT+G0DDwTuy6oDDHDfcMAkPVHwxDSTQgGyyEQDwg8osryyhAweXTMLMJHhwH6AY9PABAB5zPIIgFcg8M8kz+yBtCToAoPTSKs8gSAlDz+xBzRXc7KgLFuy888p/Tj11yQMQWG1UNixtAiE3SO1BxtWWEO8HLxACmgcnh2O0oyzcfAMDYgtygIFEuuDA3WDmbTg8JQzgQBCLN8ojCzxAfjgyD1TggOBBuMA428pJHrnkefcoweWLMx7EAAMEsaJ/kEf+ueGhZ/KAB4tjfnoQpw9wA+AcQf4ADy7y4Lrk8SxufOqoDxDDALxzBDwLv4MeuysnaH578sqvLoTV1En2+/f+rUeOjOXIKx5DDBeOlMDfnz2AQQmAAx959NO/wkIC14uwPD3hJOD/DRI4QQngd6D3laAGA8QABuL3gO8JDxz3Q93yYpAAxZzgBhWQQAUSIIEbnIB/QuDBARCIwAQCDnry4x71JjgApPAgARv0XwcDCEIWHECAJDSh3BqowlfUQAS6I8QJYKjBBNwAgB+MxQEOyMT3KVAx0OshLB6wu1hscIManGESsfLBHA4Qfs1z0g2IGMMOnsAHjrNhDQRYghMosARPdFQJNigCGR4xgBIA4QPYWMIEDrBJhatBFrHIQQ/WcIQ4dCMcw/imB9ywkCegYSw+2MTfXYsH76sBGlcIxwInusg7gQAAIfkECAkAAAAsAAAAAEAAQACGzH4E5MKE3KJE9OLE1JIk7NKk3LJs/PLkzIoU9OrU7MqU3KpU1Jo07Nq05Lp0/Pr0zIYM/O7c7MaU3KZU9ObU1JY07Na05LJs/Pb01I4k7M6k3K5k1J5E9Nq85L6EzIIE5MaM3KZM9ObM1JYs7Nas/PbszI4c9O7c7M6c3K5c1J485L58/P785LZs9N685MKM3KJM9OLM1JIs7NKs/PLszIoc9Orc7Mqc3Kpc1Jo85Lp8/Pr8zIYU/O7kzIIM5LZ09N7E////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB/6AQYKDhIWGgyUUDTcBOiCDPSsSMwMlh5eYmZoRFgE/BhegBgGDNikpODgLBgUJmq+wgw9ALw6fny0GoKSCEam/CxMTBiQPsceHIg4OBjqfzKEXvEGmqQvBwhMhOA3I3oIsIDo6BrYGuaHTvr8TOMIhIRwx368lKJaCJz86zMyfu6VQqXKHLYQOQhjoGUrwwkMBQgWakcOVbtC6Be7ecZhwolSIbgqDsOgQwMMKDzYQeWDm7BxAQdVUYRN2g5ABGBwUsKDHgsSLAEBX6BzUYFw5l6MsssvIIUVCQQ04SFWxYoc3FjMkAC1ZcsCgHQHMeVBAAogrQRgKvNhQcALIIP4PcHAQoEJADh3GjlnQGkCCB64B8gah4KHBAU0sbBTYMGynIBRS7ebgcNfxKyAgQPDtu6KkBUKWYbFocDYIjbkcVFCWLAFWAgmaA8Te6sHD4ZCEPEyVnKN3jg6aStzQHPsFiJ9ACwjGnWDu5N6SGTDg0CPTjgEgFGSGrVXrPNyFIPPuzSBHBQYeXmFoIAF28QjgD83IwZtBBfozQmc6UIC4hNLxFUICeeblIMFTlxxgFWgUaPddgIcokIN9P8BnHQoaJKDfDgBCWAgLF0zgVSE9/LBcAijcgMIMt3kICwb6PfACAzKgQAgJKOSoYgMIuhgLVgLIIMMIHAxCgwYY5v6YIgoiLOijJiJMMIKQVIogiAhJIolkih09iQkGP0wppJgy8GJBARoUkCUKJHhpXQ5jUilkkQ8UYCeaeGpgpZuYBCCnCTKYMCUN/N15Z5r48HnIAIFSaUIFJpjQAAUzVFrADHfOoCgmO1TQKKCNgjAACZdiaikQm2KywKeRAmqACzOQIGuslXaY6iA6tNpoBibA0MCssgZr4a2F3GACrzLUwGsGORDr7LPQRistLC400EED1mLbQZfRKqAsAt8qO8IA2pbbwLDQ6gBuBjwgwC4CMChybQfzTjotDO3ywK6+PGxwQr3VuuACqtE+YAICPCSMsL4B0BBwA0BUC0QDy/4R2wG/CCOwcDESS9zAAEBw6+wPPECQMMYI0BBEDAN/DATIL0Nr8MkLJ9xsECe4DMTEA/SssrMlbFBywiYnfFAQGMDcc88xnKDfrUDkAIEPQ/Mw4so9NxDDADZU/CwLGmQwNQ8VEHLAyzGIkCjQ+mGgQ8mtDcLCAE0bwsLPfLLAQQXAFeJLj0GU8HQiIuDtpQE+fODDBCl5g0ECIlAggtpugpA4AInz4ADgmDxwwuSSU5BAAmtD+AIAiieuugkoPA3aASLEIHnkCVAQAQWGgwfiBwD0zrsPmPvggwGZPDB75KLbLkICNigY4AC9Ry+84orzIPIhhEteewQi2JBABP5dQziB79JjDsBDr8CePPejg98iT4ScADz51P9wTA/bL28D+E7LckAJTsIEC1iwgwESQgfRSyAOXIeJCNTue6KLAN7udgJC0aAED3iAZQi4gwd0kIChwUAN6McBBmYCf+2zQXUQ0YMDVLAEF6SBYDqYQQ6CkBAaSOACAogMGuzPBuETxA5cSIMTlMCIJSgBgliAAQ868YZyY0DvDGBCWBwxAu8rogVjeEHBMDGDHixgAUMzAAigDzxDdNIDXOjCIyaxi3JrYgc/OMbQ5M5DRDxAETEAwyQuEYxz1OAYU/WAFlbQiDH0YxydKEgxVhFCLHiAFmlQxCSeoImLzGAgDRzoLCYmsY+KBEcT5ajBRyoqkg/AAB+9qElTwiIQACH5BAgJAAAALAAAAABAAEAAhsx+BOTChNyiRPTixNSSJNyyZOzSpPzy5MyKFPTq1OS6dOzKlNyqVNSaNOzatPz69MyGDOSybPzu3OzGlNymVPTm1NSWNOzWtPz29NSOJOS+hOzOpNyuZNSeRPTavMyCBOTGjNymTPTmzNSWLOzWrPz27MyOHPTu3OS+fOzOnNyuXNSePPz+/OS2bPTevOTCjNyiTPTizNSSLNyybOzSrPzy7MyKHPTq3OS6fOzKnNyqXNSaPPz6/MyGFPzu5MyCDOS2dPTexP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf+gEKCg4SFhoMPNwMkKSA0gweOHiIYh5aXmJkHQY0TAS8vARuQODgKpy8kPpmsrYM8IgYgIBMgoZ6igyelODM4QDNAAS4PrsaHCQsgObO1uLmCB6alMwrALcEux9uDNMrME6Gf0EK7pgrV6REzItysGA4lkODNtAHkPgq9+8EzLSCEirkrdMJACg+EHDBT9oycOVPA0K07oCvCgIGDRGzYkGIDRUEPFtqyR07COV/XZjwaFICDCgMs3LEIYpBjChIxBcVY5mzcKEEP00WsJGiASx0cJuQ0NtOA0w01EwxisQGEsgU0gohYBdJBClP9WgRBBEQFBx1oAyxtNYCGAbf+HDkaECjkxoYBNVidcKBhhoalJM6q0DGYAYi1mCqQeOv0KceLU4/BkjCoBoezaHUw0EHhZyYfJGiEXty4Ywp5GAmBcDkYLQMKr8dienCBhG23NGhANbDBAY/Uuo665syZgo6PllhIsM0ct1OpwAmRyKwZNoXrFABmwhCD+W0DJ6IbcuHaOHYVDhBfquHCQ25V4g+5eI2dQQq6h2og5iGhNuX4h2xgXQDhYcKCBw4g98p/ABrCQl/tFHJAAEQJIoEDLjiAV4OuPPBbQDl0AAMJhAzgAIIIioAfh6wcSEEHIXTAwSAYuBCEAze254AEH7KY2AwCiNhBkNDtdWMQGd7+6EBePlryQABBwjBkB0NqF8ONJh7pQAxNXvLAi1JSGaQAOgjBwwBBoImljQV2ecgCYk65Qwcr1FCCCyaiiSaOK7qZUZArzCnADoMGcYKeaSYKmZ+G8DAkoTtEusMKOSgywKWXpgkdo4bMIOmgg+6AQwUDxBCDnpe2yalqkEoaqQoinFrqrDEwuSohBuxgQaQNSArDrcAGK+ywxCIjQgUiHJtsBagJm4IF0EYLrQASLHtsBcjaGmwAI1gwQrffWqDDCRUkUG4C6CbAlbA6yOCuDBbAO8IMNZhrbrUVSLApsDx4K8MI7wKcAwYJVHvvuT3e6sK7/wacXrkSnFtwAtr+roqCCQw3LENe+uZbcL43MLhqvzJgbHLJFAhSbwIh39DyCX26WcIM7poQr80yBACSBCEXfMMJEpSgHqMxdFByzTJEWA7PQN9wQMLBskDDDiZ3QAgGTR8Qs58YIFbCCyXnQIhyJ1Q41dYA8hCjNgTNYLYQD6iHwQEHvM0hDjZkYIMKIndYwwEn/A01gCBkgAACemdAoSss/B34ATWcUMLg0S2AQA8Z9IA4AjZYAJOBcz8eeAkl2Ik2NzwooHkPmmduuN4KGHiAD4DXYGfZdpbQtXgxsN4665dnToCqhzwQeOSlk5777uKpsDrwrvdAIivGK5886RicnskDCyx6Qub+z2uOgAbGYGB78nZiIPTZLAwdkAcF9PCDBUuh4DvirEdA+SXWl016wh7yEA/aVwgfeAoCP0ggAsUmCAwQwHeaY8D+MKE+7JUAPzwIIAsG2L6lBOEDAADAD0SYQBs0ywDA60EE7OY33XnobAEcoAw9CAAQ/uADN/wBBIAwFQGwDgfaywQPSIefB2jQQx1cygBCiEMR4vAHPdjUADJgAPGwgHlCuGIGeTBDDg4iCEx0Igl/8KvKNCmDRmRB3Dg4wEEskYkJjCMCHeAnNcZwgHHr4BdBWEMR1rCEC2AUF5G4xva1URBgtCEOcSgDEDSLUe1DIgcJWJQQWtIEOFjUsDoN6EWdNIACGiAB8dwRCAAh+QQICQAAACwAAAAAQABAAIbMfgTkwoTcokT04sTUkiTs0qTcsmT88uTMihT06tTkunTsypTs2rTcqlTUmjz8+vTMhgzksmz87tzsxpT05tTUljTs1rT89vTUjiTkvoTszqT02rzcrmTMggTkxozcplT05szUlizs1qz89uzMjhz07tzkvnzszpzcrlzUnjz8/vzktmz03rzkwozcokz04szUkizs0qzcsmz88uzMihz06tzkunzsypzcqlz8+vzMhhT87uTMggzUnkTktnT03sT///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH/oBAgoOEhYaDDwcUPwwiG4Mjji8lF4eWl5iZIxQMMQWfGicMgwc3Hh4LHho/B5mur4MqEgwWIiKeoBqjgqUeJ6inEycDObDGhzu1tp4xnhoFu0C9phMeEwEtLR4gx92DLMoxt6CigzO+HtQe2gEBEt6uDyAPkLUb4gW4uua/6tbtBQjRg1foACNug14sE/EJVLRpwdpdmzFoRwCEBIGUYMGA4whEG5Y1c2gOXap/AR4N8pDBBgMV8FQk2MCI0QCYgiSIs5WvgAaVGk+YspatxYSBQEC0tGGjQLFjKhZx/MFxQ4lBOULe2sAChIRWgnIMsKCuHcYHHpjaUODjBE5Y/gkGDPgxgMGPmk+lbaDwMZPBBS3cDtpgYq0NGTZ8aHibaYdcunftdkyQUeYOSEsNK5ChQMSrEXIf163LiEWljIQKMFWw+fBhHxgtPXhBO7RouxTyoj6gVgFTzsAVULyk4gCIAS/mKr97FXUhBibYAn8tI0LATA9qJE9e90Vf54UG/Ka+wsZNWBcSbJ8E3tKL4DJ8iEB66AJjQSUoDGje/hADGfF58J0lMvFlSA4D9leICjcEUIMhByxA3wwJgEBBAvQpiN19D8RgAAfRAFFDAhdSAMIOGWroigo/rMABBxH4gIgEFEgAAokVHnCfipZIkMGLMOKAQnMH4FijhSQm/shjIQ/cAGQEOAiJw3Ul1ECjjQlcuSQmOcjAAQpSosABDgoAIcuFCSRg5YXDbWnJCWOCCWaUOFxwgZVZ0pimBCm6OUgCUkY5ZwMDjFClBDVYqaifl+QAJZ0N4BBpDDOUgCiieILF6CE2RBrpBzh80IAHB1hagqlWKrmpICeE2oCooH7gQ6k7lFCrBLb2uaoIH/Tqq6gcrCrssMQWayw8B5Q6g7Kl6kasCC640MMH0lKLwgiVZlsCtqcZ60G10wowrQzYVjpCueceC4QP0/Yg7gfuBvDAtujSe2wO7uYrrrgaIDjDDOcCDLCzq4LQg749SOvCTQAbGvC5uvrZQgoC/jhwsLgHf4TtuRwD3K2wD7hgMcUWi4uCIA88fMEIK19AsJ8P2JCCAzT34MDMC4R1bsssPxCxnyBw4EDFQztAmSA8r/zAjsPmYIHINOMgUMtLGzvCfRcsUPF1iPh8CNMa5sDBBwMYUoINGb6swgM5gN1fABXE7QN/xqydg89tb3kCDBWEELcDHvxMiAp3F9523iruDYPfjFfQgwhuh8U23msTrkLk3eQQAAycc9533HEHgIndeLNteduYGwMCBp2H4LnfFThw2ehst7006penbowMBJDQOt8hhBAicZOfbnkhL1/ywAkvVNS575y73kLdh+eee2o0rPBD8iiz4AMM/iT08JYH0C9OQgUy6B5L9YcLhAEAAHSggwAyTPAQuxggQAMNGNBwAlYOgN75ZMA94uDucoSwQfx4wIMOMBACLhjEAHRAA/1hQAf9C8F3LAA+vhGQIJdrX0V0AD8eAICBDOxBQhCgAx0g4IX9w4ANYvEBzgVAfcS5Dw7g58Ae8gACKhTEC1zIwgsiIH8YOBoQKBACz/DoB/E7oRQZqIMgJqWFF8QgBWlAAqlBYkkqqAAPG4jCH1pxgi10IRJjODwenSCKHZjiA634Aha+UItHRAAM/uenC3gABickowN5UEUJttCOWnTAAj7GqAHIQIxlBOIgQHDICzrABrEpVgksEWACHKQABh+YpABwkJJ3OCcQACH5BAgJAAAALAAAAABAAEAAhsx+BOTChNyiRPTixNyyZNSSJOzSpPzy5PTq1OS6dMyKFOzKlNyqVNSaNOzatPz69MyGDOSybPzu3OzGlNymVPTm1NSWNOzWtPz29OS+hOzOpNyuZNSeRPTavMyCBOTGjNymTPTmzNSWLOzWrPz27PTu3OS+fNSOJOzOnNyuXNSePPz+/OS2bPTevOTCjNyiTPTizNyybNSSLOzSrPzy7PTq3OS6fMyKHOzKnNyqXNSaPPz6/MyGFPzu5MyCDOS2dPTexP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf+gEGCg4SFhoM7NCUhAy0wgySOCD0Ph5aXmJkYJY0OLQ6gj4IkIzMGpg4wNJmsrYMrBzAtQJ+0HQ4dA5AjpQa+GhojCDuuxYc0A8lAQA7MnxeiQSQzvb8GKCgaCMbcgxVAAw4DzMy3uqOlI6ca2dg4Jd2sDzWVghjKzOG3uYM0vaYG2GFzQIhYvEIYQoSQQAgBuHHNPImDRM1XwHYoSPT7AO+goAMhKoSAoVHQjmTiIraARvFUwGvYgBAa8WGBzIMlRIYMsW0QJ3DkcJ2Tpu6XQA31gtT4gOPDhwsruvVAgECnyAOvYECEEaJEiZJBHnxTpwFmzyA7UDid8GHCCIP+rg5QrVBVYciogmjAKJH0Eo0Q12bgDQKj7doALkYUo4GgRgUJISvoZHhwRY1Vo5waduFiQoAWrTBQlVBVAl2FIvt6DOIAB9sJnQPIDnDW0goJNUzPhSyyQgm4q2mwbTt7dgYXYA+tIFEiNwLdVStgXV1oQFsXH1wYD5BBcaYdB2rUGP04OfVBIbQH8Mx9fYjBmR70gG7+vLcP7TME6KC60AP4o5QgAWb2HQJEewYQaFsPB2BgyAoOFniJAQtQRggNBvSFwQElMAichPF9+IADNthw00clKMIhCR+CyMoKMARgQwIxfIAIhzTgyCAJALpoiQQTJGDDDyUm0NGGitD+0EMJOPbnIyEPGGBDDDNSGUMCFwSYY4oHJPkkJjtkQCSNUybwQwBBrEADCTSsmWKKEX5pyQg/XEllnXU+AJ6bJLDJXI9yDiKBnVPWaWYID/S5Zptf1RfoK4XWyUIMkzqAgZ9rKhrno4Z8ICmloOKgKKZ9OslpEDPEEIGqMbAawKV9xloCBqZy6kAEBKhKAK4RJHDqr8AGK+yw8TzwAK3IHgvoqR2ksIGz0G7AwrHUJtviryjkoO222mZg7LfgXnuqCTmkUG4OG5Q7wQ7UsqunnsSuwO28I6zw7rvs7rBsoCEwkAMF/gacgy745quvuIEuQAEF/zLMMAMa5aunvv/+IfzlAxssDPDGDMQgyAoHr2AvyPt+iUEAGqecAwofV0yyviXLiUAMIKRMwVkk/ycysa84kEPNFHj8CswWy4kBgA+gQAEI3r0CKAkTxEzdCixsEIIhJbhQ6ysT8ACACYFOwIEAHNgwnTEtyADA2gDg8KUBHIw99gs4bE1ICRSwDYAHe7vtogEqqKCD3HEz4EDJGNjAt95re+CDDVK7ssIHOlQuwOBkvyDACzZeUoIPevPNtw+Oc3D2ahU0YHnlHOhANgcvdDQh43s7DoAPPlRQYAKVr9462R20EoPoe9/uuA9Cm2RMlFcL0oPgvbte+QKS57037rcDoABYI9xgwwD+Fj8wgAk6yEDBYBOo7jvkxazAAeiOH995WDLwYP8JINjwwYkHkCvD//+zgAEG8YAXqK9yPyjagxhAOuNZAC4B4AEEeKAACt7gBiAYBAxuIAMOFkAGIpCBCuJ0gQPaQIHKiQHucEcQQZTgBj6wHwV5cAIeUAA9BbhBCAEoAwugaRAbUF3UVmMAHviAA4TYgAwryMQTvAA9J+ggAEMoAgvIrgICCN55SsAB2cFgghOcYQ3xp0EPdnCHVdwAIRx1nhXoQIJLpGANbyiIENwghzz8XxVbKCcNTDCGYrxfBuvIQRnkkIr/08EAA4WBD1ggjnK0IRTvCMAbWIADBtjUo2AmQD4a3k8BdCQMB0fJgQ/URlgHGEEAIiAAGeRgEAgAQY0cwEZjBAIAIfkECAkAAAAsAAAAAEAAQACGzH4E5MKE3KJE9OLE3LJk1JIk7NKk/PLk9OrU5Lp0zIoU7MqU3KpU1Jo07Nq0/Pr05LJs/O7czIYU7MaU3KZU9ObU1JY07Na0/Pb05L6E1I4k7M6k3K5k1J5E9Nq8zIIE5MaM3KZM9ObM1JYs7Nas/Pbs9O7c5L58zI4c7M6c3K5c1J48/P785LZs9N685MKM3KJM9OLM3LJs1JIs7NKs/PLs9Orc5Lp8zIoc7Mqc3Kpc1Jo8/Pr8/O7kzIIM5LZ09N7E////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB/6AQYKDhIWGgywYNSYRCDaDDxU2PSU8h5eYmZoPixEVCBUVIiaDGAOnQEADJiWarq+IJT0mNgi2nyIVEZCnpw4DHg4xESywxocYJowIns0iIo+CDwMxqgNADtguDgfH3oMHNhG1oAi5IruCptfXv9kOHqTfmiw1xdLj4syhovJBGDHatdMWg5CleYUeHKBEqFO5CCLMpfvXa6CDbQ8GlXDQA2GpAyZAHgzCYtw4ULpi+FuXyh22aIKAkCBRwWOJA4tq4CRUwoY+T7lWBrSYbWSECzNpDLh3TNGiEiYWZRTEoocjWz0OlJga5IEJBKjedaPqICkJGkCYvsJQAqqiqP4HDjB9EKGSK6gDhDFF4IHGWRoGDAww9sBtiRqHo5rA4LFeK2kX/PoNHFjEKx5t2yJmlfPASI+CBiQ1AJj0hg3+LrEowRYx4sOJS6i1iXRy6Q0GUhhgnKkwW82xuYIehABwaQMbUiRPMZieb9htPw8fZOJ48tw0EBjDnNmE9OnUkeNWHkO4odmCeGDA8B38IBHLXfDu/aA9eveEXJDoWGjj50QPBNgefpfwMBsPA+QAgnaD8FBfgBg8cB+BmiCQAgg55GAAIg92WB+FsBxAwwIKggDCAmOx4OCDK34IIiYPeIChiTksgKEL6dXn4I4uvqjahTaWOAEIG1BloIQsTv7ooyAuDOmkiSBMIKGBVLKApJJL9gDlkxMEYIOKVoJJ5ZKZsGDjBC+A8MKaASzFwptVGkhmJjSk+UIAXXZJAphv9vnmnJh4gGcAhBa6AZxiqggoJgMU6iiei0Yq6aSUVjoPCgBkqmmmKVgaBBA3hHrCDaOe8IIFm256g6ckhOqqqyAwkOkHANBKawiegnBDAru+asANtGpqKw5Y+shDqDLsmiyvZdX6Aa0++ACADwVRioAMCSSQrLI3VHBAtNP68IG4PqxKKQk/JIvtsgkwNsO40E4LAArFUshDutpqe4MMMrwgSALhjhtttCRM+kAKMqSrcAI/FBxEDOKGG60EJ/7MJ6kJAUCQML8yjBXEDAP7IAEDqVHKwgD7tvBDAISAIPIHFjhQqYQJkcAvjhrhgAIIs2HAM5ksBHADgw2lYJ4DjyGSAgoSsLzkBhxEPUHJrwDRgARYS1Dkix7ooMLXHBBAg3mYmKBD1lgroEGnFJLAgA5wc6ACBCr84AKWD2SAdtpY4xBAvbCwkAIFOrytAtw6RM3B1peYoAHaCkgQuQZqU+AxaDZQwADhiMs9NweXH2IA5BI8PjkORA/3guaaI454c5rcEPnspZeuwA8GHfPABTUJUsLmhQf/9YavsKCC5LWrLcEMSV/QQAYxYMmDCCBQIIAMTG1AwfbB6zAB4P7pMVC76RosAMkOBcxQgAUcBMAcOCBA0IEAOwjQAQwXQELA9sAHMCAmPICA5NSmgR2MZAIFQMEMFjiDEcyAA4OowA5WMEH7waADDJiPC/hHARD8r0wneNzjcBaEA8xAgQycgQXYR5z67aAD87tfB8w3iARs71CgoUEBNKADQshghwtEgQUayEJBiGAHSKRfDGEAA3/YQAcym44JQuCPAWjghAt04AgsMAIICgIBSXyhBe9nLmm8iAUwQGEKVVjEIEgwjEu8H+x8pMMrolCII9iiF4OAgAYg0YVLpED+yISBFHQghSMQ4gr3eET6udB+HCAB2ZYkggB0wIFD5OIewR4YRh3koHeWqoEDQCADFXRABoOIgA5usAAX1GA6gQAAIfkECAkAAAAsAAAAAEAAQACGzH4E5MKE3KJE9OLE1JIk7NKk3LJk/PLkzIoU9OrU5Lp07MqU1Jo07Nq03KpU/Pr0zIYM5LJs/O7c7MaU9ObU1JY07Na0/Pb01I4k5L6E7M6k1J5E9Nq83K5kzIIE5MaM3KZU9ObM1JYs7Nas/PbszI4c9O7c5L587M6c1J483K5c/P785LZs9N685MKM3KJM9OLM1JIs7NKs3LJs/PLszIoc9Orc5Lp87Mqc1Jo83Kpc/Pr8zIYU/O7kzIIM5LZ09N7E////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB/6AQYKDhIWGgys7DxckBySDOwc0JA87h5eYmZori5OOJpOQJqMSNhI0D5qqq4Q7jCQkkzSzJo+COxI9JqY2CQmnK6zCh4okr5MktaGCDya6pQm9vgk0w9aDjMexsbWQErumvxQJ47bXmYnBgivZsJPK5s3P0xIUFBKE6uf5D5WEi8a2cTOHy0SpejbGUUgl6EIIc/sSVXqgj90Dd8kuLAvSzEY4UyEk4BuUAAaMkeckKlLUCpbLgZCeSbOXwJIgEjAGDICRQJ+wdDs4KfIJMJkif+sahauX4ACiEDBC6BzQc1g6oUdtMrtAUdUDGuRQBumRcyoQIAl+rgAqtJLPa/6UBj2AunMAELtATLC6GpRi1rf7gkgoa9dugwENqm1ayzioY5aB/+nMefcwkAaXtWJim44i4MhkCzeAcdmwjb2MgUa+ZEJn5cuHnf4MunZ1JhqYcw+w8XlQA4iINNs+ZOJsg5qaLvBAsED48FUhHhqaqzkAgOsiGjy3KnwFhQYWxMbwcP06CL3br/VowGGEhRaDBvjw4MEHAPI+blxIz4pEC/AjBMiBYgoAYJ995N03An+a7BCCBRYIGKAMFAgiwnwYzudBCb0xuE57I7RXQIAhBnHAgQYaaF8GHmoSwoQTyjCCDA+M4MON9dF3Ywgt3jbjjzIUIKMEGdxopJEIdP7Y4wozBiljAVBSoIIPEBx5Iwg9atLACEJCGWQBA6RApZU+sJglJiF8CSWUGnAQgw88QCDnnBqciUkCGhSgwZ58Lmjnn4AGKuigtsXAw6GIHlonoUFQsAAOOKAQaaQFbIDAcpgigICZhA7wAQqfforDBxZ0cOmpPGDAgw6MBjHCqJCOKmkDAaR6qKqHxqBkiyss8MGooY4aAgeHXrqcqhjwOKgEH0zw67OjmnAAAsheqioCARDKwQfNfuArtzikkgO1tlKLgQjOZdlrt9z6isOiGRybqqo1YGCBoA9Y4EK3zjoLhCAhVIuBuQEwJGgPKATgwr7NohdEDgNjUK8KDv4T6t0CAUwQAAqELFBDvRvAN2hX/7SwMAyEXBCDCCi8RYIGu662wqhi3TSCcC3sl48GDMTwQZYcnJDBCRoAt8oAG8RQQgwxyNBiCzdEfUMGGXBgsCY9zEAA01yL4DR/LShwg9g3nGC2CyEo+YALIiy9NNcxVPBBzHuNMMMNd0s9tdD3YiJBBW93vbIIFcyg2GoS/KDADCfkHfUJNwRgdCEWwN11BZgzkNZwKNz9w9hkR12hKgEoLcLKJWCOOadBpIvJAy2cdtMPMyyOd9QKaLfKCjNUwLTqmKdgTgMvfECBkjsk0HkHGejTAO14nyB2AXSvM8PKFRCOeQFygbDBBv4CvDADDiOgfBMOJ6igQwcRqBCB7q2fAP3dOLj+eu/aVwCCPjiksEEO4NsACARwA5LowAE60IEKOrDAHxgMBrWjHfVS8gHgDWAQJthACgQQwBcIUAEGBEECO6CDCHSgA9wbxARqZ4HqXaIBOSgcITKQghzkQAA58KD3CiiIBIgwgSpYYPtmIJsgmEAB5luNCQzQg0GEIAcbBOAGPPgCEIBQEDZAIBBLuL4I/AwbHlpBB2oYQA7q8IpB8KEDEEjCBZ7QAMrqkQVuCMAbTlGAG+BhGkX4Q/W1LwLvO9MDCqADOlIRfFYchA1+OEL15e5qZ3KUDgLovRfoMQEI1KICChdQM0GRAAgJ+4EKTqDIG7hABgOYnDUCAQAh+QQICQAAACwAAAAAQABAAIbMfgTkwoTcokT04sTcsmTs0qT88uTUkiT06tTkunTMihTsypTcqlTs2rT8+vTUmjzMhgzksmz87tzsxpTcplT05tTs1rT89vTUljTkvoTszqTcrmT02rzMggTkxozcpkz05szs1qz89uzUliz07tzkvnzUjiTszpzcrlz8/vzUnjzktmz03rzkwozcokz04szcsmzs0qz88uzUkiz06tzkunzMihzsypzcqlz8+vzMhhT87uTMggzUnkTktnT03sT///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH/oBAgoOEhYaEKSk5DoyDKReMOSmHlJWWl4k5i4wOkIM5IiIyoiIOOZeoqYWKKQ6tm56Cj6QkIga1F5Oqu4esmqbAsUCgpDLGMiQGBg68zY6srpudzLLFIrUkyMvOqDQqNIjQi4vCoKOj2dkGMojchgUQAC7hkuOMwim0otgGp4IOJKi5S+EDgEEADRBJAkZOoLlQyK6NIrSDhAR23HIwOGjwgD9Z9SI1qoZuHzJdQC5YREDDgLMcLjoYlGnQg8JWvlC2uiAKGbKPKSTQoIFAAoIdvFIwkEnzoA6MshKhWqSO0DWjCCogOLrLB4+vHA8ScPcPKA0JFc5WQIvAJaoC/jxkxuUBoCkAEGQLIVObFQQCEBVEXCKh42sHujRp9hCcV1AOi1qL+q0AGMTHQxdKQJhrmKnNxoRkbEVboXRlEEgHU0AM4CuPAqANiSid9S8IwIxTNXhg+HXsQyL63q7QzxKH3I5v2OjQ4jclEn/XCqQkwoaJE5eBiPjs/BAJGsgdS7jsQQcEHQ9YdFeF8tOOAQPcCsJQeLMOHCTWN5MBAv6AF4O8oMOAA0JwXgYX6IfKbPD98MMAPyQIRAk6KEBggTpYoGAlQfn3IIQN5AfEAwNaeOEM7W1ISA4QPuhgA/ABaIACFtZIYAAqViIBiBAO0MAPDaTQwIAmlEijAhXk/kgdkD4CyaQMLRRZoQ5SmpCikrIwyeSPIW6ggAlflmgCA1hSUgELDXAJJAsVuGBhkWBSmUGZhyCAJpBpMokBmEWGaYIGdBpCAgdpoknoj4EmquiijDZ6yAMm2HCAdZLaEEOjNIQQggWachoCBx9IasKkM4zaHKMgaKqqqixEQKoNIxxgwgwbNMpCDCHgqmsIA3gwwwwHzGDDrwc8cCWWKcSgbAHL5opAA7MO+yusMySpqAEFZBuDtsquE+yvv47wK3eBvqBBttqiy0wPxIL7aw/HqphDARqccO652XIgiK/hziCuuAkFmgMLJ9BbcMEa4AUECP6COwIGGHgwHZ0y/oRgr70aaCBfD/9iMAIMImJqcAEhEHKCxxhQMMCikhTC4gknIEDIBQ88UEB2F1yaYwohFBDyIBcEWcgAEjoSQg8P3JBjAx7c4MFxzrzAQM01a6ggCAt4MIEHC9wwQHaVGAAD1Q+oIMADVncHQgBac+1B0zFf4oAHSJNdswo9nBCvMylwEMAELbTtdNMe/GCJAT1gQLUKZj9wtgA+hOfODgFU3vbbW99wguSFcEC2CjU/3kMPEvwWQgAtVN7C1lsvIDMqE4D+uOMPuNDDBO3swmJq2qX+N9tbF85eAlSLLgAFuf2AQsxgr4hADCXU4AFKLFT+++ppp5JDArMLMHra/g5sQAEFDGyQwQkNWKtdARP44H4CNdSwsmMTZKC6ByHsTYkDNZxtuwsbQEkBxscACuAAByjYAI4EIQEYwGAFMEgADOIXAIFUQHUBCEHzUJGCCfTAey4AkCAMMD4DMuCAOFDgIGgQARj4QII1gF8NSjaIEwQgA+rJSwMo0IMaEMIDJSzgAVGAgwUCoYEQfGENJhg/+eygBaUDjQF8EDIafICAKESgChkIgxa6EH4yPAEhJvYbgpSQfAZEIA5OBQQWRvCNS4xfDV6nIlAFEYVE3OIRH+jCCPoghjUogQhV5IAQwACNODhhHo1Igha2EIYwKBwZVUQDDcAAjxtAARslHECAFvogkhaQz6JEMIAYeCADEeAO5dAHgqKRJRAAO1pTeElrNGsrSnB2WW92RTBzL2ROek4xV1dTUmhHc3dmY21TUk9QK2QyVkVLR0M0QUNxelFEcCtxUkw2R01tcFI="

    };

    if($('#listDirsModal').length){
        lazyConfig.appendScroll = $('#listDirsModal');
        lazyConfig.bind         = 'event';
    }

    $('img.lazy.loader:not(.th-image)').Lazy(lazyConfig);

    if($('img.lazy.loader.th-image').length) {
        lazyConfig.delay = 500;
        $('img.lazy.loader.th-image').Lazy(lazyConfig);
    }
}

function loadFancyBox() {
    var buttons = new Array();

    $.fancybox.defaults.infobar = true;
    $.fancybox.defaults.buttons = [
        'slideShow',
        'fullScreen',
        'close'
    ];

    $("[data-fancybox]").fancybox();

}

function selectAllThumbnails() {
    if($('.thumbnail-wrapper:not(.folder-link)').length) {
        var thumbnails = $('.thumbnail-wrapper:not(.folder-link)');
        var checked = 0;
        thumbnails.each(function(i,ele) {
            var thWrapper = $(ele);
            if(thWrapper.find('.togglebutton').length) {
                var togglebtn = thWrapper.find('.togglebutton');
                var toggleCheckbox = togglebtn.find('input[type="checkbox"]');

                if(!toggleCheckbox.prop('checked')) {
                    toggleCheckbox.click();
                } else {
                    checked++;
                }
            }
        });
        if(checked === thumbnails.length) {
            thumbnails.each(function(i,ele) {
                var thWrapper = $(ele);
                if(thWrapper.find('.togglebutton').length) {
                    var togglebtn = thWrapper.find('.togglebutton');
                    var toggleCheckbox = togglebtn.find('input[type="checkbox"]');

                    toggleCheckbox.click();
                }
            });
        }
    }
}

function toggleMediaListView() {
    if($('.media-content .thumbnail-wrapper').length) {
        $('.media-content').toggleClass('listview');
    }
    if ($('#listDirsModal [data-action="toggle_listview"]').length) {
        $('#listDirsModal [data-action="toggle_listview"]').toggleClass('listview');
    }
}

function updateGalleriesStylesPreview(){

    /** Set Input Elements */
    var title_Element               = $('#title');
    var logo_Element                = $('#logo');
    var color_header_bg_Element     = $('#color_header_bg');
    var color_header_txt_Element    = $('#color_header_txt');
    var color_page_bg_Element       = $('#color_page_bg');
    var color_content_bg_Element    = $('#color_content_bg');
    var color_content_txt_Element   = $('#color_content_txt');
    var color_thumbnail_bg_Element  = $('#color_thumbnail_bg');
    var color_thumbnail_txt_Element = $('#color_thumbnail_txt');

    /** Set Preview Areas */
    var gsp_title     = $('.gsp--title');
    var gsp_page      = $('.gsp--page');
    var gsp_header    = $('.gsp--header');
    var gsp_logo      = $('.gsp--logo');
    var gsp_content   = $('.gsp--content');
    var gsp_thumbnail = $('.gsp--thumbnail');

    if(
        title_Element.length &&
        logo_Element.length &&
        color_header_bg_Element.length &&
        color_header_txt_Element.length &&
        color_page_bg_Element.length &&
        color_content_bg_Element.length &&
        color_content_txt_Element.length &&
        color_thumbnail_bg_Element.length &&
        color_thumbnail_txt_Element.length &&

        gsp_title.length &&
        gsp_page.length &&
        gsp_header.length &&
        gsp_logo.length &&
        gsp_content.length &&
        gsp_thumbnail.length
    ) {
        gsp_title.text(title_Element.val());

        gsp_page.css({
            backgroundColor : color_page_bg_Element.val()
        });

        var logoSrc = (logo_Element.val() !== '' && mediaGalleriesStyles_logoBaseUrl !== '') ? mediaGalleriesStyles_logoBaseUrl + '/' + logo_Element.val() : 'http://via.placeholder.com/600x65?text=Logo';
        gsp_logo.find('img').attr('src',logoSrc);

        gsp_header.css({
            backgroundColor : color_header_bg_Element.val(),
            color: color_header_txt_Element.val()
        });

        gsp_content.css({
            backgroundColor : color_content_bg_Element.val(),
            color: color_content_txt_Element.val()
        });

        gsp_thumbnail.css({
            backgroundColor : color_thumbnail_bg_Element.val(),
            color: color_thumbnail_txt_Element.val()
        });

    }
}

function insertThemeLogoMedia(insertBtn) {
    var dirFolderName = insertBtn.data('dirfoldername');
    var fullFileName  = insertBtn.data('filefullname');
    var listDirsModal = $('#listDirsModal');
    var logoField = $('#edit_create_form').find('#logo');

    if(logoField.length) {
        logoField.val(fullFileName);
        logoField.parent().find('.remove-logo').removeClass('hidden');
        updateGalleriesStylesPreview();
        listDirsModal.modal('hide');
    }
}

function activateDirWithAllChildren_ayscallback(e,triggerElement) {
    e = typeof e !== 'undefined' ? e : '';
    triggerElement = typeof triggerElement !== 'undefined' ? $(triggerElement) : '';

    if(e !== '' && triggerElement !== '') {
        e.preventDefault();
        e.stopPropagation();


        var href = '';
        if(typeof triggerElement.attr('href') !== 'undefined'){
            href = triggerElement.attr('href');
        }

        window.location.href = href + '/allChildren';
    }
}

function activateDirWithoutAllChildren_ayscallback(e,triggerElement) {
    e = typeof e !== 'undefined' ? e : '';
    triggerElement = typeof triggerElement !== 'undefined' ? $(triggerElement) : '';

    if(e !== '' && triggerElement !== '') {
        e.preventDefault();
        e.stopPropagation();


        var href = '';
        if(typeof triggerElement.attr('href') !== 'undefined'){
            href = triggerElement.attr('href');
        }

        window.location.href = href;
    }
}

function deActivateDirWithAllChildren_ayscallback(e,triggerElement) {
    e = typeof e !== 'undefined' ? e : '';
    triggerElement = typeof triggerElement !== 'undefined' ? $(triggerElement) : '';

    if(e !== '' && triggerElement !== '') {
        e.preventDefault();
        e.stopPropagation();


        var href = '';
        if(typeof triggerElement.attr('href') !== 'undefined'){
            href = triggerElement.attr('href');
        }

        window.location.href = href + '/allChildren';
    }
}

function deActivateDirWithoutAllChildren_ayscallback(e,triggerElement) {
    e = typeof e !== 'undefined' ? e : '';
    triggerElement = typeof triggerElement !== 'undefined' ? $(triggerElement) : '';

    if(e !== '' && triggerElement !== '') {
        e.preventDefault();
        e.stopPropagation();


        var href = '';
        if(typeof triggerElement.attr('href') !== 'undefined'){
            href = triggerElement.attr('href');
        }

        window.location.href = href;
    }
}

function showPlayer() {

    initVideoJs();

    setTimeout(function(){
        if($('.video-js').length && $('.media-player--wrapper').length) {
            $('.media-player--wrapper').addClass('showPlayer');

            var thumbnailElement = $('.media-player--wrapper').closest('.thumbnail');
            if(typeof thumbnailElement.attr('href') !== 'undefined' && thumbnailElement.attr('href') !== false) {
                thumbnailElement.on('click touchstart', function(e) {
                    e.preventDefault();
                });
            }
        }
    }, 1000);
}

function initVideoJs() {
    if(typeof videojs !== 'undefined' && $('.video-js').length) {
        $('.video-js').each(function(i,ele) {
            var thisEle = $(ele);
            var teId = (typeof thisEle.attr('id') !== 'undefined' && thisEle.attr('id') !== false) ? thisEle.attr('id') : '';

            if(teId !== '' && !thisEle.find('.vjs-tech').length) {
                videojs(teId);
            }
        });
    }
}

function videojsPauseAll() {
    $('.video-js').each(function (i,ele) {
        var thisEle = $(this);
        if(thisEle.find('.vjs-tech').length) {
            this.player.pause();
        }
    });
}

function clearGalleryCache(triggerEle) {
    var dirFolderName = triggerEle.data('dirfoldername');

    // console.log('dirFolderName', dirFolderName);

    if(dirFolderName != '') {
        var getAjaxMediaClearCacheUrl = mediaModelAjaxUrl + '/dirs/clearcache/' + dirFolderName;
        $.get( getAjaxMediaClearCacheUrl, function( data ) {
            if (data.success = true) {
                core.bs_alert.success(data.message, 'autoclose', '#listDirsModal .modal-body');
            }
        });
    }
}

$( document ).ready(function() {

    if($('#listDirsModal').length) {
        var modal_ele = $('#listDirsModal');
        modal_ele.on('show.bs.modal', function (e) {
            var getHref = (typeof $(e.relatedTarget).attr('href') !== 'undefined') ? $(e.relatedTarget).attr('href') : '';
            var getAjaxMediabrowserUrl = getHref;
            var getAjaxEditUrl = getHref;
            var dirFolderName = (typeof $(e.relatedTarget).data('dirfoldername') !== 'undefined') ? $(e.relatedTarget).data('dirfoldername') : '';
            initUploader();
            loadMedia(dirFolderName);

            if($('.clear-cache').length) {
                $('.clear-cache').each(function(i,ele) {
                    var ccEle = $(ele);
                    ccEle.click(function (e) {
                        e.preventDefault();
                        clearGalleryCache(ccEle);
                    });
                });
            }

            modal_ele.removeAttr('data-dtreload');
        });
    }

    if($('img.lazy').length) {
        loadLazy();
    }

    if($('[data-fancybox]').length) {
        loadFancyBox();
    }

    if($('.thumbnail-wrapper').length) {
        $('.thumbnail-wrapper').each(function(i,ele) {
            var thWrapper = $(ele);

            if(thWrapper.find('.togglebutton').length) {
                var togglebtn = thWrapper.find('.togglebutton');
                var toggleCheckbox = togglebtn.find('input[type="checkbox"]');

                if(toggleCheckbox.prop('checked')) {
                    thWrapper.addClass('checked');
                }

                toggleCheckbox.click(function() {
                    if(toggleCheckbox.prop('checked')) {
                        thWrapper.addClass('checked');
                    } else {
                        thWrapper.removeClass('checked');
                    }
                });
            }
        });
    }

    if($('[data-action="download"]').length) {
        $('[data-action="download"]').click(function(event) {
            if($('.thumbnail-wrapper').length) {
                var selectedItems = 0;
                $('.thumbnail-wrapper').each(function(i,ele) {
                    var thWrapper = $(ele);

                    if (thWrapper.find('.togglebutton').length) {
                        var togglebtn = thWrapper.find('.togglebutton');
                        var toggleCheckbox = togglebtn.find('input[type="checkbox"]');

                        if (toggleCheckbox.prop('checked')) {
                            selectedItems++;
                        }
                    }
                });
                if(selectedItems === 0) {
                    event.preventDefault();
                    var errMsg = core.i18n.translate('Bitte zuerst eine Auswahl treffen!');
                    core.bs_alert.danger(errMsg);
                }
            }
        });
    }

    if($('a[data-toggle="tab"]').length && $('table[data-datatable]').length) {

        $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
            var dataTables = $('.tab-content .active.in table[data-datatable]');
            dataTables.DataTable()
                .columns.adjust();
            // .ajax.reload();
        });

    }

    if($('#createEditApiModal').length) {
        $('#createEditApiModal').on('show.bs.modal', function (e) {
            var apiUserChooser = $('#apiUserChooserWrapper').find('select#id_acc_users');
            if(apiUserChooser.length){
                apiUserChooser.on('change', function(event,ele) {
                    var getAjaxMediaApiGetKeyUrl = mediaModelAjaxUrl + '/api/getapikeybyuserid/' + apiUserChooser.val();
                    $.get( getAjaxMediaApiGetKeyUrl, function( data ) {
                        if (data.success = true) {
                            $('input#api_key').val(data.result);
                        }
                    });
                });
            }
        });
    }

    if($('.galleries-styles-preview').length) {
        var title_Element               = $('#title');
        var logo_Element                = $('#logo');
        var color_header_bg_Element     = $('#color_header_bg');
        var color_header_txt_Element    = $('#color_header_txt');
        var color_page_bg_Element       = $('#color_page_bg');
        var color_content_bg_Element    = $('#color_content_bg');
        var color_content_txt_Element   = $('#color_content_txt');
        var color_thumbnail_bg_Element  = $('#color_thumbnail_bg');
        var color_thumbnail_txt_Element = $('#color_thumbnail_txt');

        if(
            title_Element.length &&
            logo_Element.length &&
            color_header_bg_Element.length &&
            color_header_txt_Element.length &&
            color_page_bg_Element.length &&
            color_content_bg_Element.length &&
            color_content_txt_Element.length &&
            color_thumbnail_bg_Element.length &&
            color_thumbnail_txt_Element.length
        ) {
            title_Element.keyup(function() { updateGalleriesStylesPreview(); });
            logo_Element.change(function() { updateGalleriesStylesPreview(); });
            color_header_bg_Element.change(function() { updateGalleriesStylesPreview(); });
            color_header_txt_Element.change(function() { updateGalleriesStylesPreview(); });
            color_page_bg_Element.change(function() { updateGalleriesStylesPreview(); });
            color_content_bg_Element.change(function() { updateGalleriesStylesPreview(); });
            color_content_txt_Element.change(function() { updateGalleriesStylesPreview(); });
            color_thumbnail_bg_Element.change(function() { updateGalleriesStylesPreview(); });
            color_thumbnail_txt_Element.change(function() { updateGalleriesStylesPreview(); });
        }

        var logoField = $('#edit_create_form').find('#logo');
        var removeLogoBtn = logoField.parent().find('.remove-logo');

        if(removeLogoBtn.length){
            removeLogoBtn.click(function(){
                logoField.val('');
                updateGalleriesStylesPreview();
                removeLogoBtn.addClass('hidden');
            });
        }
    }

    showPlayer();

    $('.modal').on('hide.bs.modal', function (e) {
        videojsPauseAll();
    });
});