<?php
/**
 * CMS Model View Page Index
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

$site = $Core->Sites()->getSite();
$siteUrlKey = (count($site) && isset($site['urlKey'])) ? $site['urlKey'] . '/' : '';
?>
<table class="cms-pages table table-hover table-striped table-condensed">
      <thead>
        <tr>
          <th>#</th>
          <th>UrlKey</th>
          <th>Titel</th>
          <th>Url</th>
          <th>Public</th>
        </tr>
      </thead>
      <tbody>
        <?php $i=001; foreach($pages as $page): ?>
            <?php if(($page['siteId'] !== 0) && ($page['siteId'] != $site['id']) ) { continue; } ?>
        <tr>
          <th scope="row"><?php echo ($i<10) ? '0'.$i : $i; ?></th>
          <td><?php echo $page['urlkey']; ?></td>
          <?php if($Core->i18n()->isMultilang()) : ?>
          <td>
              <?php foreach($pages[$page['urlkey']] as $key => $langContents): ?>
                <?php if($key == 'urlkey' || $key == 'siteId') { continue; } ?>
                <?php echo $key; ?>: <?php echo $langContents['title']; ?><br />
              <?php endforeach; ?>
          </td>
          <td>
              <?php foreach($pages[$page['urlkey']] as $key => $langContents): ?>
                  <?php if($key == 'urlkey' || $key == 'siteId') { continue; } ?>
                  <?php $langUrlKey = ($Core->i18n()->getDefaultLang() == $key) ? '' : $key . '/'; ?>
                  <?php $fullUrl = $Core->getBaseUrl() . $siteUrlKey . $langUrlKey . 'cms/page/view/' . $page['urlkey']; ?>
                  <?php echo $key; ?>: <a href="<?php echo $fullUrl ?>" target="_blank"><?php echo $fullUrl; ?></a><br />
              <?php endforeach; ?>
          </td>
          <td>
              <?php foreach($pages[$page['urlkey']] as $key => $langContents): ?>
                  <?php if($key == 'urlkey' || $key == 'siteId') { continue; } ?>
                  <?php echo $key; ?>: <?php echo ($langContents['public']) ? $Core->i18n()->translate('Ja') : $Core->i18n()->translate('Nein'); ?><br />
              <?php endforeach; ?>
          </td>
          <?php else : ?>
            <td>
              <?php foreach($pages[$page['urlkey']] as $key => $langContents): ?>
                  <?php if($key == 'urlkey' || $key == 'siteId' || $key !== $Core->i18n()->getDefaultLang()) { continue; } ?>
                  <?php echo $langContents['title']; ?>
              <?php endforeach; ?>
            </td>
          <td>
              <?php foreach($pages[$page['urlkey']] as $key => $langContents): ?>
                  <?php if($key == 'urlkey' || $key == 'siteId' || $key !== $Core->i18n()->getDefaultLang()) { continue; } ?>
                  <?php $langUrlKey = ($Core->i18n()->getDefaultLang() == $key) ? '' : $key . '/'; ?>
                  <?php $fullUrl = $Core->getBaseUrl() . $siteUrlKey . $langUrlKey . 'cms/page/view/' . $page['urlkey']; ?>
                  <a href="<?php echo $fullUrl ?>" target="_blank"><?php echo $fullUrl; ?></a>
              <?php endforeach; ?>
          </td>
          <td>
              <?php foreach($pages[$page['urlkey']] as $key => $langContents): ?>
                  <?php if($key == 'urlkey' || $key == 'siteId' || $key !== $Core->i18n()->getDefaultLang()) { continue; } ?>
                  <?php echo ($langContents['public']) ? $Core->i18n()->translate('Ja') : $Core->i18n()->translate('Nein'); ?><br />
              <?php endforeach; ?>
          </td>
          <?php endif; ?>
        </tr>
        <?php $i++; endforeach; ?>
      </tbody>
    </table>