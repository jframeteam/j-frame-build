<?php
/**
 * CMS Model - Content Management System
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** Content Management Class */
class Cms
{
    private $_version = '4.3.1';

    /** @var Core */
    private $Core;
    /** @var Mvc */
    private $Mvc;
    private $pages = null;
    private $pagesMenuArray = null;

    function __construct($Core)
    {
        $this->Core = $Core;
        $this->Mvc = $Core->Mvc();
    }

    public function getVersion() {
        return $this->_version;
    }

    public function getDbVersion() {
        $dbVersion_option = $this->Core->Config()->get('cms_version',0);
        $dbVersion = ($dbVersion_option !== '') ? $dbVersion_option : 0;
        return $dbVersion;
    }

    /** Update Methods */
    public function _update431($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc;

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '4.3.1';
        $return['changelog'] = 'Added Version Control';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    /** Acc Public Access Methods */

    public function getPublicControllerKeys() {
        return array(
            'page'
        );
    }

    public function getPublicActionKeys() {
        return array(
            'view'
        );
    }

    /** Pages Methods */

    public function getPages() {
        $pages = $this->pages;
        if($pages == null)  {
            $pageArray = array();

            $sql = "SELECT * FROM " . DB_TABLE_PREFIX . "cms_pages";
            $result = $this->Core->Db()->fromDatabase($sql, '@raw');
            $result = (is_array($result)) ? $result : array();

            foreach ($result as $row) {
                $languages = array();
                foreach($row as $rowKey => $rowValue) {
                    if(
                        is_numeric($rowKey) ||
                        $rowKey == 'id' ||
                        strlen($rowKey) > 2
                    ) {
                        continue;
                    }
                    $languages[] = $rowKey;
                }
                $pageArray[$row['urlkey']]['urlkey'] = $row['urlkey'];
                $pageArray[$row['urlkey']]['siteId'] = $row['id_system_sites'];
                foreach ($languages as $langCode) {
                    $value = ($row['type'] == 'content') ? html_entity_decode($row[$langCode]) : $row[$langCode];
                    $pageArray[$row['urlkey']][$langCode][$row['type']] = $value;
                }
            }

            $pages = $pageArray;

            $this->pages = $pages;
        }
        return $pages;
    }

    public function getPage($urlKey = null) {
        $getPage = array();

        if($urlKey === null) {
            $mvcInfo = $this->Mvc->getMvcInfo();
            if($mvcInfo['model'] == 'cms' && $mvcInfo['controller'] == 'page' && $mvcInfo['action'] == 'view' && count($mvcInfo['params'])) {

                $paramsPageUrlKey = '';
                $params = $mvcInfo['params'];
                reset($params);
                $first_key = key($params);
                if($first_key == 'urlkey') {
                    $paramsPageUrlKey = (isset($params['urlkey'])) ? $params['urlkey'] : '';
                } else {
                    $paramsPageUrlKey = $first_key;
                }

                $urlKey = ($paramsPageUrlKey != '') ? $paramsPageUrlKey : '';
            }
        }

        if($urlKey != '') {
            $pages = $this->getPages();
            $site = $this->Core->Sites()->getSite();
            if (
                count($pages) &&
                array_key_exists($urlKey, $pages) &&
                is_array($pages[$urlKey]) &&
                count($pages[$urlKey]) &&
                array_key_exists('siteId', $pages[$urlKey]) &&
                $pages[$urlKey]['siteId'] === $site['id']
            ) {
                $getPage = $pages[$urlKey];
            }
        }

        return $getPage;
    }

    public function getPagesMenuArray($siteId = null) {

        /** @var $accClass Acc */
        $accClass = $this->Mvc->modelClass('Acc');
        $isPublic = (is_object($accClass) && $accClass->userLoggedIn()) ? false : true;

        $pagesMenuArray = $this->pagesMenuArray;
        if($pagesMenuArray == null) {
            $pagesMenuArray = array();

            $pages = $this->getPages();

            $site = $this->Core->Sites()->getSite();
            $siteUrlKey = (count($site) && isset($site['urlKey'])) ? $site['urlKey'] . '/' : '';

            foreach($pages as $urlKey => $page){
                foreach($pages[$urlKey] as $key => $langContents) {
                    if($key == 'urlkey' || $key == 'siteId') { continue; }
                    $langUrlKey = ($this->Core->i18n()->getDefaultLang() == $key) ? '' : $key . '/';
                    $fullUrl = $this->Core->getBaseUrl() . $langUrlKey . $siteUrlKey . 'cms/page/view/' . $page['urlkey'];

                    if($langContents['status'] == 1) {
                        $pagesMenuArray[$page['siteId']][$key][$urlKey]['title'] = $langContents['title'];
                        $pagesMenuArray[$page['siteId']][$key][$urlKey]['url'] = $fullUrl;
                    }

                    $pageIsPublic = ($langContents['public'] == 1);

                    if($isPublic && !$pageIsPublic) {
                        unset($pagesMenuArray[$page['siteId']][$key][$urlKey]);
                    }
                }
            }

            $this->pagesMenuArray = $pagesMenuArray;
        }

        return ($siteId !== null && array_key_exists($siteId, $pagesMenuArray) && is_array($pagesMenuArray[$siteId])) ? $pagesMenuArray[$siteId] : $pagesMenuArray;
    }

    public function getPagesMenu()
    {
        $site = $this->Core->Sites()->getSite();
        $siteId = (is_array($site) && array_key_exists('id',$site)) ? $site['id'] : 0;

        $currLang = $this->Core->i18n()->getCurrLang();

        $currPage = $this->getPage();

        $menuArray = $this->getPagesMenuArray($siteId);

        $menuHtml = '';
        if(isset($menuArray[$currLang])) {
            foreach ($menuArray[$currLang] as $urlKey => $pageData) {
                $li_attr_class = (isset($currPage['urlkey']) && ($currPage['urlkey'] == $urlKey)) ? ' class="active"' : '';

                $linkTitle = $pageData['title'];
                $linkUrl = $pageData['url'];

                $menuHtml .= '<li' . $li_attr_class . '>' . PHP_EOL;
                $menuHtml .= '<a href="' . $linkUrl . '" title="' . $linkTitle . '">' . $linkTitle . '</a>' . PHP_EOL;
                $menuHtml .= '</li>' . PHP_EOL;
            }
        }

        return $menuHtml;
    }

    public function addToMvcMenu() {
        return $this->getPagesMenu();
    }
}