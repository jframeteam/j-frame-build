<?php
/**
 * CMS Model Controller Page
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function indexAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('cms_page')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle('CMS Index - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($oldPageTitle . ' <small>CMS Index</small>');

        /** @var $cmsClass Cms */
        $cmsClass = $Mvc->modelClass('cms');
        $pages = $cmsClass->getPages();
        $pagesMenuArray = $cmsClass->getPagesMenuArray();

        $return = '';
        $ob_return = '';
        if ($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    }
}

function viewAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $cmsClass Cms
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');
    $isPublic = (is_object($accClass) && $accClass->userLoggedIn()) ? false : true;

    $params   = $Mvc->getMvcParams();
    $cmsClass = $Mvc->modelClass('cms');
    $pages    = $cmsClass->getPages();

    $paramsPageUrlKey = '';

    if(count($params)) {
        reset($params);
        $first_key = key($params);
        if($first_key == 'urlkey') {
            $paramsPageUrlKey = (isset($params['urlkey'])) ? $params['urlkey'] : '';
        } else {
            $paramsPageUrlKey = $first_key;
        }
    }

    $page = ($paramsPageUrlKey !== '' && isset($pages[$paramsPageUrlKey])) ? $pages[$paramsPageUrlKey] : array();

    $pagePublic = (array_key_exists($Core->i18n()->getCurrLang(), $page) && $page[$Core->i18n()->getCurrLang()]['public']);

    if($isPublic && !$pagePublic) {
        $page = array();
    }

    if(!count($page)) {
        return '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Seite nicht gefunden.') . '</div>';
    }

    $oldMetaTitle = $Mvc->getPageTitle();
    $Mvc->setMetaTitle($page[$Core->i18n()->getCurrLang()]['title'] . ' - ' . $oldMetaTitle);

    $oldPageTitle = $Mvc->getPageTitle();
    $Mvc->setPageTitle($page[$Core->i18n()->getCurrLang()]['title']);

    $return = '';
    $ob_return = '';
    if($Mvc->getMVCViewFilePath() !== false) {
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
    } else {
        $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
    }

    $return .= $ob_return;

    return $return;
}