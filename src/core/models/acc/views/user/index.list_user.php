<?php
/**
 * Acc Model View User List Users
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */
?>
<?php if(count($usersActive)) :?>
    <?php $usergroups = $accClass->getGroups(); ?>
    <div class="table-responsive">
        <table class="table table-hover table-condensed">
            <tbody>
            <?php foreach($usersActive as $id => $user): ?>

                <?php $isInactiveList = false; ?>
                <?php include('index.list_users_loop.php'); ?>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php else: ?>
    <div class="alert alert-warning" role="alert"><?php echo $Core->i18n()->translate('Keine User in dieser Liste.') ?></div>
<?php endif; ?>