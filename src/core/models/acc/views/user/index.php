<?php
/**
 * Acc Model View User Index
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */
?>
<p><?php $Core->i18n()->translate('Hier können die Beutzer verwaltet werden'); ?></p>

<div class="action-wrapper row">
    <div class="col-sm-12">
        <div class="btn-set text-right">
            <a href="<?php echo $Mvc->getModelUrl() . '/user/create/' ?>" class="btn btn-primary" title="<?php echo $Core->i18n()->translate('User erstellen') ?>">
                <i class="fa fa-user-plus" aria-hidden="true"></i>
            </a>
        </div>
    </div>
</div>

<?php
$users = $accClass->getUsers();

$usersActive = array();
$usersInactive = array();

foreach($users as $id => $user) {
    if($user['su'] && !$curUser['su']) { continue; }

    $isInactive  = ($user['active'] == 0) ? true : false;

    if($isInactive) {
        $usersInactive[$id] = $user;
    } else {
        $usersActive[$id] = $user;
    }
}

/** Sort Arrays */
usort($usersActive, function($a, $b) {
    return $a['name'] > $b['name'];
});
usort($usersInactive, function($a, $b) {
    return $a['name'] > $b['name'];
});
?>

<ul class="nav nav-tabs">
    <li class="active"><a href="#list" data-toggle="tab"><?php echo $Core->i18n()->translate('Aktive') ?> <span class="badge"><?php echo count($usersActive) ?></span></a></li>
    <li><a href="#list-inactive" data-toggle="tab"><?php echo $Core->i18n()->translate('Inaktive') ?> <span class="badge"><?php echo count($usersInactive) ?></span></a></li>
</ul>
<div id="userTabContent" class="tab-content">
    <div class="tab-pane fade active in" id="list">
        <?php include('index.list_user.php') ?>
    </div>
    <div class="tab-pane fade" id="list-inactive">
        <?php include('index.list-inactive_user.php') ?>
    </div>
</div>