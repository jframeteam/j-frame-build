<?php
/**
 * Acc Model View User Profile
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

$thisUser = $userToView;

$usergroups = $accClass->getGroups();

$infotext = ($showCurrUser) ? 'Dies ist Ihr Profil' : 'Profil von';

$canEdit = ($showCurrUser || $accClass->hasAccess('acc_user_edit')) ? true : false;

$editLink = $Mvc->getModelUrl() . '/user/edit/' . $thisUser['id'];

$genderMaleTxt = $Core->i18n()->translate('Herr');
$genderFemaleTxt = $Core->i18n()->translate('Frau');

$genderIcon    = ($thisUser['gender'] == 'male') ? '<i class="fa fa-mars" title="' . $genderMaleTxt . '"></i><span class="visible-xs-inline"> ' . $genderMaleTxt . '</span>' : '<i class="fa fa-venus" title="' . $genderFemaleTxt . '"></i><span class="visible-xs-inline"> ' . $genderFemaleTxt . '</span>';
$genderText    = ($thisUser['gender'] == 'male') ? $genderMaleTxt  : $genderFemaleTxt;
$usergroupName = (isset($usergroups[$thisUser['id_acc_groups']]['name'])) ? $usergroups[$thisUser['id_acc_groups']]['name'] : $Core->i18n()->translate('Nicht gesetzt');

$languages = $Core->i18n()->getLanguages();
$defaultLangName = ($thisUser['default_lang'] !== null && array_key_exists($thisUser['default_lang'], $languages)) ? '<span class="flag flag-' . $thisUser['default_lang'] . '"></span> ' . $Core->i18n()->translate($languages[$thisUser['default_lang']]['name']) : $Core->i18n()->translate('Nicht gesetzt');
?>

<p><?php echo $Core->i18n()->translate($infotext) ?> <?php echo $thisUser['name'] ?> <?php echo $thisUser['surname'] ?>.</p>
<div class="row">
    <div class="col-sm-8 col-sm-offset-2">
        <div class="panel panel-primary userprofile view">
            <div class="panel-heading">
                <h2 class="panel-title">
                    <?php echo $thisUser['name'] ?> <?php echo $thisUser['surname'] ?>
                    <?php if($canEdit): ?>
                        <small>
                            (<a href="<?php echo $editLink; ?>">
                                <?php echo $Core->i18n()->translate('bearbeiten') ?>
                            </a>)
                        </small>
                    <?php endif; ?>
                </h2>
            </div>
            <div class="panel-body">
                <?php include('profile.body.php'); ?>
            </div>
        </div>
    </div>
</div>