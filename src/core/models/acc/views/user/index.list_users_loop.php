<?php
/**
 * Acc Model View User List Users Loop
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */
?>
<?php $itsYou          = ($curUser['id'] == $user['id']); ?>
<?php $defaultLang     = $Core->i18n()->getLang($user['default_lang']); ?>
<?php $genderMaleTxt   = $Core->i18n()->translate('Herr'); ?>
<?php $genderFemaleTxt = $Core->i18n()->translate('Frau'); ?>
<?php $genderIcon      = ($user['gender'] == 'male') ? '<i class="fa fa-mars ' . $user['gender'] . ' text-info" title="' . $genderMaleTxt . '"></i><span class="visible-xs-inline"> ' . $genderMaleTxt . '</span>' : '<i class="fa fa-venus ' . $user['gender'] . ' text-warning" title="' . $genderFemaleTxt . '"></i><span class="visible-xs-inline"> ' . $genderFemaleTxt . '</span>'; ?>
<?php $groupName       = (isset($usergroups[$user['id_acc_groups']]['name'])) ? $usergroups[$user['id_acc_groups']]['name'] : $Core->i18n()->translate('Nicht gesetzt'); ?>
<?php $itsYouIcon      = ($itsYou) ? ' <i class="fa fa-hand-spock-o text-warning" title="' . $Core->i18n()->translate('Das sind Sie!') . '" style="font-size: 0.8em;" aria-hidden="true"></i>' : ''; ?>
<?php $userDelBtn      = ($accClass->hasAccess('acc_user_delete')) ? '<a href="' . $Mvc->getModelUrl() . '/user/delete/' . $user['id'] . '" class="btn btn-danger hidden-xs btn-del-user" data-action="user_delete" title="' . sprintf( $Core->i18n()->translate('User \'%s\' löschen') , $user['name'] . ' ' . $user['surname']) . '"><i class="fa fa-trash" aria-hidden="true"></i></a>': ''; ?>
<?php $userActBtn      = '<a href="' . $Mvc->getModelUrl() . '/user/activate/' . $user['id'] . '" class="btn btn-default" data-action="user_activate" title="' .  sprintf( $Core->i18n()->translate('Aktiviere User \'%s\'') , $user['name'] . ' ' . $user['surname']) . '"><i class="fa fa-smile-o" aria-hidden="true"></i></a>'; ?>
<?php $userDeActBtn    = '<a href="' . $Mvc->getModelUrl() . '/user/deactivate/' . $user['id'] . '" class="btn btn-default" data-action="user_deactivate" data-ays data-aystext="' . $Core->i18n()->translate('Sind Sie Sicher? Der Benutzer ist dann nicht mehr in der Lage sich anzumelden!') . '" data-confirmtext="' . $Core->i18n()->translate('Weiter') . '" title="' . sprintf( $Core->i18n()->translate('User \'%s\' deaktivieren') , $user['name'] . ' ' . $user['surname']) . '"><i class="fa fa-times-circle-o" aria-hidden="true"></i></a>'; ?>
<tr>
    <th>
        <div class="">
            <div class="col-xs-2 col-sm-1">
                <i class="fa fa-user" aria-hidden="true" title="ID=<?php echo $user['id'] ?>"></i>
                <?php if($curUser['su']) : ?>
                    <?php if($itsYou) : ?>
                        <i class="fa fa-star text-warning" title="<?php echo $Core->i18n()->translate('Superuser'); ?>"></i>
                    <?php else: ?>
                        <a href="<?php echo $Mvc->getModelUrl() . '/user/setsu/' . $user['id'] ?>" class="action" data-action="user_setsu" title="<?php echo sprintf( $Core->i18n()->translate('\'%s\' zum Superuser machen') , $user['name'] . ' ' . $user['surname']); ?>">
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                        </a>
                    <?php endif; ?>
                <?php else: ?>
                    <?php if($user['su']) : ?>
                        <i class="fa fa-star text-warning" title="<?php echo $Core->i18n()->translate('Superuser'); ?>"></i>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
            <div class="col-xs-4 col-sm-4">
                <?php echo $user['name'] ?> <?php echo $user['surname'] ?><?php echo $itsYouIcon; ?>
                <small title="<?php echo sprintf($Core->i18n()->translate('Benutzergruppe: \'%s\''), $groupName) ?>" class="hidden-xs"><i class="fa fa-users text-warning" aria-hidden="true"></i></small>
                <span class="hidden-xs pull-right" style="white-space: nowrap;">
                    <?php echo $genderIcon ?> |
                    <?php if($Core->i18n()->isMultilang()) : ?>
                    <span title="<?php echo $Core->i18n()->translate('Standard Sprache'); ?>: <?php echo $defaultLang['name'] ?>"><span class="flag flag-<?php echo $defaultLang['code'] ?>"></span></span> |
                    <?php endif; ?>
                    <i class="fa fa-circle text-muted uos_viewer" data-uid="<?php echo $user['id'] ?>"></i> |
                    <a href="mailto:<?php echo $user['email'] ?>" target="_blank" title="<?php echo sprintf($Core->i18n()->translate('E-Mail senden an \'%s\''), $user['email']); ?>" class="hidden-xs"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                </span>
            </div>
            <div class="col-xs-6 col-sm-7">
                <div class="action pull-right">
                    <div class="btn-group btn-group-sm btn-group-raised">
                        <a href="<?php echo $accClass->getAjaxUserProfileUrl($user['id']) ?>" class="btn btn-default hidden-xs" title="<?php echo $Core->i18n()->translate('Profil anzeigen'); ?>" data-profilelink="ajax"><i class="fa fa-user-circle" aria-hidden="true"></i></a>
                        <?php if($accClass->hasAccess('acc_user_edit')) : ?>
                            <a href="<?php echo $Mvc->getModelUrl() . '/user/edit/' . $user['id'] ?>" class="btn btn-default" title="<?php echo $Core->i18n()->translate('bearbeiten'); ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            <?php if($isInactiveList) : ?>
                                <?php echo $userActBtn; ?>
                            <?php else : ?>
                                <?php echo $userDeActBtn; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if($isInactiveList) : ?>
                            <?php echo $userDelBtn ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </th>
</tr>
