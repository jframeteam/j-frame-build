<?php
/**
 * Acc Model View User Login
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $i18n i18n
 * @var $mailerClass Mailer
 * @var $accClass Acc
 * @var $forms Forms
 */

/** Prepare Forms Plugin */
$plugins = $Core->Plugins();
$forms   = $plugins->Forms();
$isForms = (is_object($forms));

$i18n = $Core->i18n();
$mailerClass = $Mvc->modelClass('Mailer');

$formElementData = array();
?>
<?php if($accClass->userLoggedIn()): ?>
    <?php $user = $accClass->getUser(); ?>
    <p>
        <?php echo sprintf($i18n->translate('Sie sind angemeldet als %s.'), $user['name'] . ' ' . $user['surname']); ?>
        <a href="<?php echo $Mvc->getModelUrl(); ?>/user/logout" title="<?php echo $i18n->translate('ausloggen'); ?>" class="btn btn-secondary btn-block"><?php echo $i18n->translate('ausloggen'); ?></a>
    </p>
<?php else : ?>
    <form action="<?php echo $Mvc->getModelUrl(); ?>/user/login" method="post">
        <div class="form-group">
            <label class="control-label" for="mail"><?php echo $i18n->translate('E-Mail Adresse'); ?></label>
            <?php
            /** E-Mail Element */
            $formElementData['type']          = 'email';
            $formElementData['id']            = 'mail';
            $formElementData['name']          = 'mail';
            $formElementData['label']         = false;
            $formElementData['value']         = '';
            $formElementData['required']      = true;
            $formElementData['addAttributes'] = array('placeholder' => $i18n->translate('E-Mail Adresse'));
            echo ($isForms) ? $forms->buildElement($formElementData) : '';
            ?>
        </div>
        <div class="form-group">
            <label class="control-label" for="pass"><?php echo $i18n->translate('Passwort'); ?></label>
            <?php
            /** Password Element */
            $formElementData['type']          = 'password';
            $formElementData['id']            = 'pass';
            $formElementData['name']          = 'pass';
            $formElementData['label']         = false;
            $formElementData['value']         = '';
            $formElementData['required']      = true;
            $formElementData['addAttributes'] = array('placeholder' => $i18n->translate('Passwort'));
            echo ($isForms) ? $forms->buildElement($formElementData) : '';
            ?>
        </div>
        <?php if(is_object($mailerClass)) : ?>
        <div class="form-group">
            <a href="<?php echo $Mvc->getModelUrl() . '/user/passforgot/'; ?>"><?php echo $i18n->translate('Passwort vergessen?'); ?></a>
        </div>
        <?php endif; ?>
        <button type="submit" class="btn btn-primary btn-block" data-action="login" name="action" value="login">
            <?php echo $i18n->translate('Login'); ?>
        </button>
    </form>
<?php endif; ?>