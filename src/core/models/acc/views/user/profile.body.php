<?php
/**
 * Acc Model View User Profile Body
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

$accClass->hasAccess('acc_user');

$uosViewer = ($accClass->hasAccess('acc_user')) ? '  <i class="fa fa-circle text-muted uos_viewer" data-uid="' . $thisUser['id'] . '"></i>' : '';
$suViewer = ($thisUser['su']) ? ' <i class="fa fa-star text-warning" title="' . $Core->i18n()->translate('Superuser') . '"></i>' : '';
?>

<div class="row">
    <div class="col-sm-12">
        <div class="userdetails">
            <div class="table-responsive">
                <table class="table table-hover">
                    <tbody>
                    <tr>
                        <th><?php echo $Core->i18n()->translate('Name') ?></th>
                        <td><?php echo $thisUser['name'] ?> <?php echo $thisUser['surname'] ?><?php echo $uosViewer; ?><?php echo $suViewer; ?></td>
                    </tr>
                    <tr>
                        <td><strong><?php echo $Core->i18n()->translate('Anrede') ?></strong></td>
                        <td><?php echo $genderText ?></td>
                    </tr>
                    <tr>
                        <td><strong><?php echo $Core->i18n()->translate('E-Mail') ?></strong></td>
                        <td><a href="mailto:<?php echo $thisUser['email'] ?>"><?php echo $thisUser['email'] ?></a></td>
                    </tr>
                    <tr>
                        <td><strong><?php echo $Core->i18n()->translate('Benutzergruppe') ?></strong></td>
                        <td><?php echo $usergroupName ?></td>
                    </tr>
                    <tr>
                        <td><strong><?php echo $Core->i18n()->translate('Information') ?></strong></td>
                        <td><?php echo $thisUser['additional'] ?></td>
                    </tr>
                    <?php if($Core->i18n()->isMultilang()) : ?>
                    <tr>
                        <td><strong><?php echo $Core->i18n()->translate('Standard Sprache') ?></strong></td>
                        <td><?php echo $defaultLangName ?></td>
                    </tr>
                    <?php endif; ?>
                    <tr>
                        <td><strong><?php echo $Core->i18n()->translate('Erstellt') ?></strong></td>
                        <td><?php echo date('d.m.Y H:i:s', strtotime($thisUser['created'])) ?></td>
                    </tr>
                    <?php if(isset($thisUser['changed']) && $accClass->hasAccess('acc_user')) : ?>
                        <tr>
                            <td><strong><?php echo $Core->i18n()->translate('Geändert') ?></strong></td>
                            <td><?php echo date('d.m.Y H:i:s', strtotime($thisUser['changed'])) ?></td>
                        </tr>
                    <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>