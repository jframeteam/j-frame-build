<?php
/**
 * Acc Model View User Password Reset
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $accClass Acc
 * @var $plugins Plugins
 * @var $pitsForms PitsForms
 * @var $pitsCore PitsCore
 */

/** Prepare Form Plugin */
$plugins = $Core->Plugins();
$pitsForms = $plugins->PitsForms();
$isForms = (is_object($pitsForms));
$pitsCore = $plugins->PitsCore();

$actionBtnName = ($isForms) ? $pitsCore->xorEnc('action') : 'action';

$usergroups = $accClass->getGroups();

$infotext = 'Hier können Sie Ihr Passwort ändern';

$userSaveUrl = $Mvc->getModelUrl() . '/user/passreset/' . $paramPassResetHash;

$thisUser = $userToView;

if(count($thisUser)) : ?>

<p><?php echo $Core->i18n()->translate($infotext) ?> <?php echo $thisUser['name'] ?> <?php echo $thisUser['surname'] ?>.</p>

<form id="edit_create_form" class="form-horizontal" action="<?php echo $userSaveUrl ?>" method="post">
    <div class="action-wrapper row">
        <div class="col-sm-12">
            <div class="btn-set top text-right">
                <button type="submit" name="<?php echo $actionBtnName; ?>" value="pass_reset" class="btn btn-primary" data-action="pass_reset">
                    <i class="fa fa-floppy-o" aria-hidden="true"></i><span class="hidden-xs"> <?php echo $Core->i18n()->translate('Speichern') ?></span>
                </button>
            </div>
        </div>
    </div>

    <fieldset>

        <legend><?php echo $Core->i18n()->translate('Passwort ändern') ?></legend>

        <div class="form-group">
            <label for="pass" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Passwort ändern') ?></label>

            <div class="col-md-10">
                <div id="changePassword">
                    <?php
                    /** Password Element */
                    $formElementData['eleType']       = 'input';
                    $formElementData['id']            = 'pass';
                    $formElementData['name']          = 'pass';
                    $formElementData['label']         = false;
                    $formElementData['value']         = '';
                    $formElementData['type']          = 'password';
                    $formElementData['isRequired']    = true;
                    $formElementData['ownAttributes'] = array(
                        'placeholder' => $Core->i18n()->translate('Passwort'),
                        'autocomplete' => 'off'
                    );
                    $formElementData['cssClasses']    = 'validatePassword';
                    echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                    $formElementData['cssClasses']    = '';
                    ?>
                </div>
                <div id="repeatPassword">
                    <?php
                    /** Password Element */
                    $formElementData['eleType']       = 'input';
                    $formElementData['id']            = 'repeat_pass';
                    $formElementData['name']          = 'repeat_pass';
                    $formElementData['label']         = false;
                    $formElementData['value']         = '';
                    $formElementData['type']          = 'password';
                    $formElementData['isRequired']    = true;
                    $formElementData['ownAttributes'] = array(
                        'placeholder' => $Core->i18n()->translate('Passwort wiederholen'),
                        'autocomplete' => 'off'
                    );
                    echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                    ?>
                </div>
                <span class="help-block"><?php echo $Core->i18n()->translate('Passwörter müssen mindestens eine länge von 8 Zeichen haben sowie Buchstaben und Zahlen beinhalten!'); ?></span>
            </div>
        </div>
    </fieldset>
</form>
<?php else: ?>
    <div class="alert alert-danger" role="alert"><?php echo $Core->i18n()->translate('Dieser Link ist ungültig!'); ?></div>
<?php endif; ?>
