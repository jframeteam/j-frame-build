<?php
/**
 * Acc Model View User Create
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $accClass Acc
 * @var $plugins Plugins
 * @var $pitsForms PitsForms
 * @var $pitsCore PitsCore
 */

/** Prepare Form Plugin */
$plugins = $Core->Plugins();
$pitsForms = $plugins->PitsForms();
$isForms = (is_object($pitsForms));
$pitsCore = $plugins->PitsCore();

$actionBtnName = ($isForms) ? $pitsCore->xorEnc('action') : 'action';


$usergroups = $accClass->getGroups();
$languages = $Core->i18n()->getLanguages();
$curUser = $accClass->getUser();

$siteList = ($curUser['su']) ? $Core->Sites()->getSites() : $curUser['sites'];

$infotext = $Core->i18n()->translate('Neuen User anlegen');

$userSaveUrl = $Mvc->getModelUrl() . '/user/save/new';

$genderMaleTxt = $Core->i18n()->translate('Herr');
$genderFemaleTxt = $Core->i18n()->translate('Frau');
?>
<p><?php echo $Core->i18n()->translate($infotext) ?>.</p>

<form id="edit_create_form" class="form-horizontal" action="<?php echo $userSaveUrl ?>" method="post">
    <?php
    /** Name Element */
    $formElementData['eleType']       = 'input';
    $formElementData['name']          = 'id';
    $formElementData['label']         = false;
    $formElementData['value']         = 0;
    $formElementData['type']         = 'hidden';
    echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
    ?>
    <div class="action-wrapper row">
        <div class="col-sm-12">
            <div class="btn-set top text-right">
                <div class="btn-group">
                    <button type="submit" name="<?php echo $actionBtnName; ?>" value="user_save" class="btn btn-primary" data-action="user_save">
                        <i class="fa fa-floppy-o" aria-hidden="true"></i> <span class="hidden-xs"> <?php echo $Core->i18n()->translate('Speichern') ?></span>
                    </button>
                    <a href="<?php echo $userSaveUrl ?>" data-target="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <button type="submit" class="btn btn-link" name="<?php echo $actionBtnName; ?>" value="user_save_exit" data-exit="true" data-action="user_save_exit">
                                <i class="fa fa-floppy-o" aria-hidden="true"></i> <?php echo $Core->i18n()->translate('Speichern und schließen') ?>
                            </button>
                        </li>
                    </ul>
                </div>
                <a href="<?php echo $Mvc->getModelUrl() . '/user/'; ?>" class="btn btn-default" title="<?php echo $Core->i18n()->translate('zurück zur Übersicht'); ?>">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>
            </div>
        </div>
    </div>

    <ul class="nav nav-tabs">
        <li class="active"><a href="#general" data-toggle="tab"><?php echo $Core->i18n()->translate('Allgemein') ?></a></li>
        <li><a href="#additional" data-toggle="tab"><?php echo $Core->i18n()->translate('Weiteres') ?></a></li>
        <li><a href="#settings" data-toggle="tab"><?php echo $Core->i18n()->translate('Einstellungen') ?></a></li>
    </ul>

    <div id="cmsPageFormTabContent" class="tab-content">
        <div class="tab-pane fade active in" id="general">

            <fieldset>

                <legend><?php echo $Core->i18n()->translate('Allgemein') ?></legend>

                <div class="form-group">
                    <label for="gender" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Anrede') ?></label>

                    <div class="col-md-10">
                        <?php
                        /** Gender Element */
                        $formElementData['eleType']       = 'select';
                        $formElementData['id']            = 'gender';
                        $formElementData['name']          = 'gender';
                        $formElementData['label']         = false;
                        $formElementData['value']         = '';
                        $formElementData['allValue']      = array(
                            'female' => $genderFemaleTxt,
                            'male'   => $genderMaleTxt
                        );
                        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="name" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Vorname') ?> <em>*</em></label>

                    <div class="col-md-10">
                        <?php
                        /** Name Element */
                        $formElementData['eleType']       = 'input';
                        $formElementData['id']            = 'name';
                        $formElementData['name']          = 'name';
                        $formElementData['label']         = false;
                        $formElementData['value']         = '';
                        $formElementData['type']          = 'text';
                        $formElementData['isRequired']    = true;
                        $formElementData['ownAttributes'] = array('placeholder' => $Core->i18n()->translate('Vorname'));
                        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="surname" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Nachname') ?></label>

                    <div class="col-md-10">
                        <?php
                        /** Surname Element */
                        $formElementData['eleType']       = 'input';
                        $formElementData['id']            = 'surname';
                        $formElementData['name']          = 'surname';
                        $formElementData['label']         = false;
                        $formElementData['value']         = '';
                        $formElementData['type']          = 'text';
                        $formElementData['isRequired']    = false;
                        $formElementData['ownAttributes'] = array('placeholder' => $Core->i18n()->translate('Nachname'));
                        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('E-Mail') ?> <em>*</em></label>

                    <div class="col-md-10">
                        <?php
                        /** E-Mail Element */
                        $formElementData['eleType']       = 'input';
                        $formElementData['id']            = 'email';
                        $formElementData['name']          = 'email';
                        $formElementData['label']         = false;
                        $formElementData['value']         = '';
                        $formElementData['type']          = 'email';
                        $formElementData['isRequired']    = true;
                        $formElementData['ownAttributes'] = array('placeholder' => $Core->i18n()->translate('E-Mail'));
                        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="pass" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Passwort') ?> <em>*</em></label>

                    <div class="col-md-10">
                        <?php
                        /** Password Element */
                        $formElementData['eleType']       = 'input';
                        $formElementData['id']            = 'pass';
                        $formElementData['name']          = 'pass';
                        $formElementData['label']         = false;
                        $formElementData['value']         = '';
                        $formElementData['type']          = 'password';
                        $formElementData['isRequired']    = true;
                        $formElementData['ownAttributes'] = array(
                            'placeholder' => $Core->i18n()->translate('Passwort'),
                            'data-toggle' => 'password',
                            'autocomplete' => 'off'
                        );
                        $formElementData['cssClasses']    = 'validatePassword';
                        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                        $formElementData['cssClasses']    = '';
                        ?>
                        <div id="repeatPassword">
                            <?php
                            /** Password Element */
                            $formElementData['eleType']       = 'input';
                            $formElementData['id']            = 'repeat_pass';
                            $formElementData['name']          = 'repeat_pass';
                            $formElementData['label']         = false;
                            $formElementData['value']         = '';
                            $formElementData['type']          = 'password';
                            $formElementData['isRequired']    = false;
                            $formElementData['ownAttributes'] = array(
                                'placeholder' => $Core->i18n()->translate('Passwort wiederholen'),
                                'autocomplete' => 'off'
                            );
                            echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                            ?>
                        </div>
                        <span class="help-block"><?php echo $Core->i18n()->translate('Passwörter müssen mindestens eine länge von 8 Zeichen haben sowie Buchstaben und Zahlen beinhalten!'); ?></span>
                    </div>
                </div>
            </fieldset>

        </div>
        <div class="tab-pane fade" id="additional">

            <fieldset>

                <legend><?php echo $Core->i18n()->translate('Weiteres') ?></legend>

                <div class="form-group">
                    <label for="id_system_sites" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Information') ?></label>
                    <div class="col-md-10">
                        <?php
                        /** Additional Element */
                        $formElementData['eleType']       = 'textarea';
                        $formElementData['id']            = 'additional';
                        $formElementData['name']          = 'additional';
                        $formElementData['label']         = false;
                        $formElementData['value']         = '';
                        $formElementData['isRequired']    = false;
                        $formElementData['handlers']      = array(
                            'placeholder' => $Core->i18n()->translate('Information'),
                            'rows'        => 3
                        );
                        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                        ?>
                    </div>
                </div>

            </fieldset>

        </div>
        <div class="tab-pane fade" id="settings">

            <fieldset>

                <legend><?php echo $Core->i18n()->translate('Einstellungen') ?></legend>

                <div class="form-group">
                    <label for="id_system_sites" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Benutzerstatus') ?></label>
                    <div class="col-md-10">
                        <div class="togglebutton">
                            <label>
                                <?php
                                /** Active Element */
                                $formElementData['eleType']        = 'checkbox';
                                $formElementData['id']             = '';
                                $formElementData['name']           = 'active';
                                $formElementData['label']          = false;
                                $formElementData['value']          = '1';
                                $formElementData['valueChecked']   = '1';
                                $formElementData['valueUnchecked'] = '0';
                                $formElementData['checkboxOnly']  = true;
                                echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                                ?>

                                <?php echo $Core->i18n()->translate('Aktiv') ?>
                            </label>
                        </div>
                        <span class="help-block"><?php echo $Core->i18n()->translate('Wenn deaktiviert, kann sich der User nicht mehr einloggen.') ?></span>
                    </div>
                </div>

                <?php if(count($siteList)) : ?>
                    <div class="form-group">
                        <label for="id_system_sites" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Seiten') ?></label>

                        <div class="col-md-10">
                            <div class="row">
                                <?php $cli=001; foreach($siteList as $site) : ?>
                                    <?php $site = (!is_array($site) && $Core->Sites()->siteExists($site)) ? $Core->Sites()->getSite($site) : $site ?>
                                    <?php $siteId = $site['id']; ?>
                                    <?php $checked = (in_array($siteId, $curUser['sites'])) ? ' checked="checked"' : ''; ?>
                                    <?php if(count($siteList) === 1) : ?>
                                        <div class="col-sm-12">
                                            <?php
                                            /** System Site Element */
                                            $formElementData['eleType']       = 'input';
                                            $formElementData['name']          = 'id_system_sites[]';
                                            $formElementData['label']         = false;
                                            $formElementData['value']         = $siteId;
                                            $formElementData['type']         = 'hidden';
                                            echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                                            ?>
                                            <p class="form-control-static"><?php echo $site['name']; ?></p>
                                        </div>
                                    <?php else: ?>
                                        <div class="col-sm-3">
                                            <div class="togglebutton">
                                                <label>
                                                    <?php
                                                    /** System Site Element */
                                                    $formElementData['eleType']        = 'checkbox';
                                                    $formElementData['id']             = '';
                                                    $formElementData['name']           = 'id_system_sites[]';
                                                    $formElementData['label']          = false;
                                                    $formElementData['value']          = ($checked) ? $siteId : '';
                                                    $formElementData['valueChecked']   = $siteId;
                                                    $formElementData['valueUnchecked'] = '0';
                                                    $formElementData['checkboxOnly']   = true;
                                                    echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                                                    ?>
                                                    <?php echo $site['name']; ?>
                                                </label>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                    <?php $cli++; endforeach; ?>
                            </div>
                            <span class="help-block"><?php echo $Core->i18n()->translate('Sollte keine Seite zum Benutzer ausgewählt sein, kann dieser nur auf öffentliche Bereiche zugreifen!') ?></span>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if($accClass->hasAccess('acc_group_edit')) : ?>
                    <div class="form-group">
                        <label for="id_acc_groups" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Benutzergruppe') ?></label>

                        <div class="col-md-10">
                            <?php
                            $defaultGroup = $accClass->getDefaultGroup();
                            $defaultGroupId = (count($defaultGroup)) ? $defaultGroup['id'] : '';

                            $dlAllValue = array();
                            $dlAllValue[0] = $Core->i18n()->translate('Nicht gesetzt');
                            foreach($usergroups as $acgId => $acg) {
                                $dlAllValue[$acgId] = $acg['name'];
                            }
                            /** E-Mail Element */
                            $formElementData['eleType']       = 'select';
                            $formElementData['id']            = 'id_acc_groups';
                            $formElementData['name']          = 'id_acc_groups';
                            $formElementData['label']         = false;
                            $formElementData['value']         = $defaultGroupId;
                            $formElementData['allValue']      = $dlAllValue;
                            echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                            ?>
                        </div>
                    </div>
                <?php else: ?>
                    <?php
                    $defaultGroup = $accClass->getDefaultGroup();
                    $defaultGroupId = (count($defaultGroup)) ? $defaultGroup['id'] : '';

                    /** Default Group Hidden Element */
                    $formElementData['eleType']       = 'input';
                    $formElementData['id']            = false;
                    $formElementData['name']          = 'id_acc_groups';
                    $formElementData['label']         = false;
                    $formElementData['value']         = $defaultGroupId;
                    $formElementData['type']          = 'hidden';
                    $formElementData['ownAttributes'] = false;
                    echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                    ?>
                <?php endif; ?>

                <?php if($Core->i18n()->isMultilang()) : ?>
                    <div class="form-group">
                        <label for="default_lang" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Standard Sprache') ?></label>

                        <div class="col-md-10">
                            <?php
                            $dlAllValue = array();
                            foreach($languages as $langCode => $lang) {
                                $dlAllValue[$langCode] = $lang['name'];
                            }
                            /** E-Mail Element */
                            $formElementData['eleType']       = 'select';
                            $formElementData['id']            = 'default_lang';
                            $formElementData['name']          = 'default_lang';
                            $formElementData['label']         = false;
                            $formElementData['value']         = $Core->i18n()->getDefaultLang();
                            $formElementData['allValue']      = $dlAllValue;
                            echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                            ?>
                        </div>
                    </div>
                <?php endif; ?>

            </fieldset>

        </div>
    </div>



    <?php if($Mvc->hasModel('mailer')) : ?>

        <fieldset>

            <legend>&nbsp;</legend>

            <div class="form-group">
                <div class="col-md-10 col-md-offset-2">
                    <div class="togglebutton">
                        <label>
                            <?php
                            /** System Site Element */
                            $formElementData['eleType']        = 'checkbox';
                            $formElementData['id']             = '';
                            $formElementData['name']           = 'sendinfomail';
                            $formElementData['label']          = false;
                            $formElementData['value']          = '1';
                            $formElementData['valueChecked']   = '1';
                            $formElementData['valueUnchecked'] = '0';
                            $formElementData['checkboxOnly']   = true;
                            echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                            ?>
                            <?php echo $Core->i18n()->translate('Send Mail with Login Information') ?>
                        </label>
                    </div>
                    <span class="help-block"><?php echo $Core->i18n()->translate('If set, the new user will recieve an E-Mail with Login information.') ?></span>
                </div>
            </div>

        </fieldset>

    <?php endif; ?>

    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {

            $('.action-wrapper .btn[type=submit]').click( function(btn){
                $('.action-wrapper .btn[type=submit]').removeClass('clicked');
                $(this).addClass('clicked');
            });

            var checkEmailurl = '<?php echo $Mvc->getModelAjaxUrl('acc'); ?>/user/checkifusermailexists/';
            $('#edit_create_form').submit( function(form_event) {
                var emailToValidate = $('#email').val();
                $.get( checkEmailurl + emailToValidate, function( data ) {
                    if(data.success = true) {
                        var result = data.result;
                        if( Object.prototype.toString.call( result ) === '[object Object]' ) {
                            if(result.found) {
                                core.bs_alert.danger(result.msg);
                            } else {
                                $('#edit_create_form').unbind('submit');
                                $('.btn.clicked').click();
                            }
                        } else {
                            core.bs_alert.danger(result);
                        }
                    }
                });
                form_event.preventDefault();
            });
        });
    </script>

</form>