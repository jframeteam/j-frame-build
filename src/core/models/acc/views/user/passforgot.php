<?php
/**
 * Acc Model View User Password Forgot
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $accClass Acc
 * @var $plugins Plugins
 * @var $pitsForms PitsForms
 * @var $pitsCore PitsCore
 */

$isForms = (is_object($pitsForms));
$pitsCore = $plugins->PitsCore();

$actionBtnName = ($isForms) ? $pitsCore->xorEnc('action') : 'action';

$passforgotPostUrl = $Mvc->getModelUrl() . '/user/passforgot/';?>

<p><?php echo $Core->i18n()->translate('Bitte geben Sie hier Ihre E-Mail Adresse ein.<br />Mit dem Klick auf "Senden" wird eine E-Mail mit weiteren Informationen an diese (falls im System vorhanden) gesendet.') ?></p>

<form id="pass_forgot_form" class="form-horizontal" action="<?php echo $passforgotPostUrl ?>" method="post">
    <div class="action-wrapper row">
        <div class="col-sm-12">
            <div class="btn-set top text-right">
                <button type="submit" name="<?php echo $actionBtnName; ?>" value="pass_forgot" class="btn btn-primary" data-action="pass_forgot">
                    <i class="fa fa-paper-plane" aria-hidden="true"></i><span class="hidden-xs"> <?php echo $Core->i18n()->translate('Senden'); ?></span>
                </button>
            </div>
        </div>
    </div>

    <fieldset>

        <legend><?php echo $Core->i18n()->translate('E-Mail Adresse eingeben') ?></legend>

        <div class="form-group">
            <label for="email" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('E-Mail Adresse') ?></label>

            <div class="col-md-10">
                <?php
                /** Password Element */
                $formElementData['eleType']       = 'input';
                $formElementData['id']            = 'email';
                $formElementData['name']          = 'email';
                $formElementData['label']         = false;
                $formElementData['value']         = '';
                $formElementData['type']          = 'email';
                $formElementData['isRequired']    = true;
                $formElementData['ownAttributes'] = array(
                    'placeholder' => $Core->i18n()->translate('E-Mail Adresse'),
                    'autocomplete' => 'off'
                );
                echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                ?>
            </div>
        </div>
    </fieldset>
</form>