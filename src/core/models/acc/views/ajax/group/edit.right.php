<?php
/**
 * Acc Model Ajax View Group Edit Right
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $accClass Acc
 * @var $plugins Plugins
 * @var $pitsForms PitsForms
 */

/** Prepare Form Plugin */
$plugins = $Core->Plugins();
$pitsForms = $plugins->PitsForms();
$isForms = (is_object($pitsForms));

$right = $accClass->getRightById($paramEditId);

$rightSaveUrl = $Mvc->getModelUrl() . '/group/save/right/' . $right['id'];
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">
        <?php echo $Core->i18n()->translate('Berechtigung bearbeiten') ?> | <?php echo $right['name'] ?>
    </h4>
</div>

<form id="edit_create_form" class="form-horizontal" action="<?php echo $rightSaveUrl ?>" method="post">
    <?php
    /** Name Element */
    $formElementData['eleType']       = 'input';
    $formElementData['name']          = 'id';
    $formElementData['label']         = false;
    $formElementData['value']         = $right['id'];
    $formElementData['type']         = 'hidden';
    echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
    ?>

    <div class="modal-body">

        <div class="form-group">
            <label for="code" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Code') ?></label>

            <div class="col-md-10">
                <p class="form-control-static">
                    <?php echo $right['code'] ?>
                </p>
                <span class="help-block"><?php echo $Core->i18n()->translate('Eine interne Bezeichnung, wird für die Zugriffsprüfung benötigt!') ?></span>
            </div>
        </div>

        <div class="form-group">
            <label for="name" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Name') ?> <em>*</em></label>

            <div class="col-md-10">
                <?php
                /** Name Element */
                $formElementData['eleType']       = 'input';
                $formElementData['id']            = 'name';
                $formElementData['name']          = 'name';
                $formElementData['label']         = false;
                $formElementData['value']         = $right['name'];
                $formElementData['type']          = 'text';
                $formElementData['isRequired']    = true;
                $formElementData['ownAttributes'] = array('placeholder' => $Core->i18n()->translate('Name'));
                echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                ?>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <?php if(!in_array($right['code'], $accClass->getProtectedRights())) : ?>
            <a href="<?php echo $Mvc->getModelUrl(); ?>/group/delete/right/<?php echo $right['id']; ?>" class="btn btn-danger" data-action="right_delete"><?php echo $Core->i18n()->translate('Löschen'); ?></a>
        <?php endif; ?>
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Abbrechen'); ?></button>
        <button type="submit" class="btn btn-success"><?php echo $Core->i18n()->translate('Speichern'); ?></button>
    </div>
</form>