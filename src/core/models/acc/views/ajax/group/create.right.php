<?php
/**
 * Acc Model Ajax View Group Create Right
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $accClass Acc
 * @var $plugins Plugins
 * @var $pitsForms PitsForms
 * @var $pitsCore PitsCore
 */

/** Prepare Form Plugin */
$plugins = $Core->Plugins();
$pitsForms = $plugins->PitsForms();
$isForms = (is_object($pitsForms));
$pitsCore = $plugins->PitsCore();

$actionBtnName      = ($isForms) ? $pitsCore->xorEnc('action') : 'action';

$rightSaveUrl = $Mvc->getModelUrl() . '/group/save/right/';
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">
        <?php echo $Core->i18n()->translate('Berechtigung erstellen') ?>
    </h4>
</div>
    <form id="edit_create_form" class="form-horizontal" action="<?php echo $rightSaveUrl ?>" method="post">
    <?php
    /** Name Element */
    $formElementData['eleType']       = 'input';
    $formElementData['name']          = 'id';
    $formElementData['label']         = false;
    $formElementData['value']         = 0;
    $formElementData['type']         = 'hidden';
    echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
    ?>

    <div class="modal-body">

        <div class="form-group">
            <label for="code" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Code') ?> <em>*</em></label>

            <div class="col-md-10">
                <?php
                /** Code Element */
                $formElementData['eleType']       = 'input';
                $formElementData['id']            = 'code';
                $formElementData['name']          = 'code';
                $formElementData['label']         = false;
                $formElementData['value']         = '';
                $formElementData['type']          = 'text';
                $formElementData['isRequired']    = true;
                $formElementData['ownAttributes'] = array(
                    'placeholder' => $Core->i18n()->translate('Code'),
                    'data-checkcodetype' => 'right',
                );
                echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                ?>
                <span class="help-block"><?php echo $Core->i18n()->translate('Eine interne Bezeichnung, wird für die Zugriffsprüfung benötigt!') ?><br /><strong><?php echo $Core->i18n()->translate('Bitte keine Leerzeichen / Sonderzeichen verwenden!') ?></strong></span>
            </div>
        </div>

        <div class="form-group">
            <label for="name" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Name') ?> <em>*</em></label>

            <div class="col-md-10">
                <?php
                /** Name Element */
                $formElementData['eleType']       = 'input';
                $formElementData['id']            = 'name';
                $formElementData['name']          = 'name';
                $formElementData['label']         = false;
                $formElementData['value']         = '';
                $formElementData['type']          = 'text';
                $formElementData['isRequired']    = true;
                $formElementData['ownAttributes'] = array('placeholder' => $Core->i18n()->translate('Name'));
                echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                ?>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Abbrechen'); ?></button>
        <button type="submit" class="btn btn-success"><?php echo $Core->i18n()->translate('Speichern'); ?></button>
    </div>
</form>