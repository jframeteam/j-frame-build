<?php
/**
 * Acc Model Ajax View Group Create Group
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $accClass Acc
 * @var $plugins Plugins
 * @var $pitsForms PitsForms
 * @var $pitsCore PitsCore
 */

/** Prepare Form Plugin */
$plugins = $Core->Plugins();
$pitsForms = $plugins->PitsForms();
$isForms = (is_object($pitsForms));
$pitsCore = $plugins->PitsCore();

$actionBtnName      = ($isForms) ? $pitsCore->xorEnc('action') : 'action';
$RightsCheckboxName = ($isForms) ? $pitsCore->xorEnc('rights') : 'rights';

$groupSaveUrl = $Mvc->getModelUrl() . '/group/save/group/';
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">
        <?php echo $Core->i18n()->translate('Benutzergruppe erstellen') ?>
    </h4>
</div>
<form id="edit_create_form" class="form-horizontal" action="<?php echo $groupSaveUrl ?>" method="post">
    <?php
    /** Name Element */
    $formElementData['eleType']       = 'input';
    $formElementData['name']          = 'id';
    $formElementData['label']         = false;
    $formElementData['value']         = null;
    $formElementData['type']         = 'hidden';
    echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
    ?>

    <div class="modal-body">

        <div class="form-group">
            <label for="code" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Code') ?> <em>*</em></label>

            <div class="col-md-10">
                <?php
                /** Code Element */
                $formElementData['eleType']       = 'input';
                $formElementData['id']            = 'code';
                $formElementData['name']          = 'code';
                $formElementData['label']         = false;
                $formElementData['value']         = '';
                $formElementData['type']          = 'text';
                $formElementData['isRequired']    = true;
                $formElementData['ownAttributes'] = array(
                        'placeholder' => $Core->i18n()->translate('Code'),
                        'data-checkcodetype' => 'group',
                    );
                echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                ?>
                <span class="help-block"><?php echo $Core->i18n()->translate('Eine interne Bezeichnung, wird für die Zuordnung zum Benutzer benötigt!') ?><br /><strong><?php echo $Core->i18n()->translate('Bitte keine Leerzeichen / Sonderzeichen verwenden!') ?></strong></span>
            </div>
        </div>

        <div class="form-group">
            <label for="name" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Name') ?> <em>*</em></label>

            <div class="col-md-10">
                <?php
                /** Name Element */
                $formElementData['eleType']       = 'input';
                $formElementData['id']            = 'name';
                $formElementData['name']          = 'name';
                $formElementData['label']         = false;
                $formElementData['value']         = '';
                $formElementData['type']          = 'text';
                $formElementData['isRequired']    = true;
                $formElementData['ownAttributes'] = array('placeholder' => $Core->i18n()->translate('Name'));
                echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                ?>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-12">
                <button class="btn btn-warning btn-block" type="button" data-toggle="collapse" data-target="#set_rights" aria-expanded="false" aria-controls="set_rights">
                    <?php echo $Core->i18n()->translate('Berechtigungen setzen'); ?> <span class="caret"></span>
                </button>
            </div>
        </div>

        <div class="collapse" id="set_rights">
            <div class="form-group">
                <label for="admin" class="col-md-6 control-label">
                    <?php echo $Core->i18n()->translate('Administrator') . '?'; ?>
                </label>

                <?php $isGroupPatternAdmin = (array_key_exists('admin',$groupPattern) && $groupPattern['admin']); ?>
                <?php $isRightsExpanded = ($isGroupPatternAdmin) ? 'false' : 'true'; ?>

                <div class="col-md-6">
                    <div class="togglebutton">
                        <label data-toggle="collapse" data-target="#collapseRights" aria-expanded="<?php echo $isRightsExpanded; ?>" aria-controls="collapseRights">
                            <?php
                            /** Right Element */
                            $formElementData['eleType']        = 'checkbox';
                            $formElementData['id']             = 'admin';
                            $formElementData['name']           = 'admin';
                            $formElementData['label']          = false;
                            $formElementData['value']          = ($isGroupPatternAdmin) ? '1' : '0';
                            $formElementData['valueChecked']   = '1';
                            $formElementData['valueUnchecked'] = '0';
                            $formElementData['checkboxOnly']   = true;
                            echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                            ?>
                        </label>
                    </div>
                </div>
            </div>

            <div class="collapse<?php echo ($isGroupPatternAdmin) ? '' : ' in'; ?>" id="collapseRights" aria-expanded="<?php echo $isRightsExpanded; ?>">
                <hr style="margin: 0 0 15px;" />
            <?php foreach($rights as $rightCode => $right): ?>
                <?php $checked = false ?>
                <?php $rightId             = $right['id'] ?>
                <?php $rightName           = (array_key_exists('name',$right) && $right['name'] != '') ? $right['name'] : $rightCode; ?>
                <?php $rightType           = (array_key_exists('type',$right) && $right['type'] != '') ? $right['type'] : ''; ?>
                <?php $fromWhitelist       = (array_key_exists('fromWhitelist',$right) && $right['fromWhitelist']); ?>
                <?php if(count($groupPattern) && array_key_exists('rights', $groupPattern) && count($groupPattern['rights'])) : ?>
                    <?php if(array_key_exists($rightId, $groupPattern['rights']) || array_key_exists($rightCode, $groupPattern['rights'])) : ?>
                        <?php $checked = true ?>
                    <?php endif; ?>
                <?php endif; ?>
                <?php $spanAttr = ''; ?>
                <?php $spanTitle = ''; ?>
                <?php $spanClass = ''; ?>
                <?php
                if($rightId < 1 || $rightType == 'file') {
                    $spanTitle = $Core->i18n()->translate('Kein individueller Name im System vorhanden...');
                    $spanClass = ' text-warning';
                }
                if(!$fromWhitelist) {
                    $spanTitle .= ' ' . $Core->i18n()->translate('Nicht in Whitelist...');
                    $spanClass = ' text-danger';
                }
                if($spanTitle != '') {
                    $spanAttr .= ' title="' . $spanTitle . '"';
                    $spanAttr .= ' data-toggle="tooltip"';
                }
                if($spanClass != '') {
                    $spanAttr .= ' class="' . $spanClass . '"';
                }
                ?>
                <div class="form-group">
                    <label for="right_<?php echo $rightId; ?>" class="col-md-6 control-label">
                        <span<?php echo $spanAttr; ?>>
                            <?php echo $rightName ?>
                        </span>
                    </label>

                    <div class="col-md-6">
                        <div class="togglebutton">
                            <label>
                                <?php
                                /** Right Element */
                                $formElementData['eleType']        = 'checkbox';
                                $formElementData['id']             = 'right_' . $rightId;
                                $formElementData['name']           = 'rights[]';
                                $formElementData['label']          = false;
                                $formElementData['value']          = ($checked) ? $rightCode : '';
                                $formElementData['valueChecked']   = $rightCode;
                                $formElementData['valueUnchecked'] = '0';
                                $formElementData['checkboxOnly']   = true;
                                echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                                ?>
                            </label>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Abbrechen'); ?></button>
        <button type="submit" class="btn btn-success"><?php echo $Core->i18n()->translate('Speichern'); ?></button>
    </div>
</form>
