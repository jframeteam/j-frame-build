<?php
/**
 * Acc Model Ajax View User Profile
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

$thisUser = $userToView;

$usergroups = $accClass->getGroups();

$infotext = ($showCurrUser) ? 'Dies ist Ihr Profil' : 'Profil von';

$canEdit = ($showCurrUser || $accClass->hasAccess('acc_user_edit')) ? true : false;

$editLink = $Mvc->getModelUrl() . '/user/edit/' . $thisUser['id'];

$genderMaleTxt = $Core->i18n()->translate('Herr');
$genderFemaleTxt = $Core->i18n()->translate('Frau');

$genderIcon    = ($thisUser['gender'] == 'male') ? '<i class="fa fa-mars" title="' . $genderMaleTxt . '"></i><span class="visible-xs-inline"> ' . $genderMaleTxt . '</span>' : '<i class="fa fa-venus" title="' . $genderFemaleTxt . '"></i><span class="visible-xs-inline"> ' . $genderFemaleTxt . '</span>';
$genderText    = ($thisUser['gender'] == 'male') ? $genderMaleTxt  : $genderFemaleTxt;

$usergroupName = (isset($usergroups[$thisUser['id_acc_groups']]['name'])) ? $usergroups[$thisUser['id_acc_groups']]['name'] : $Core->i18n()->translate('Nicht gesetzt');

$languages = $Core->i18n()->getLanguages();
$defaultLangName = ($thisUser['default_lang'] !== null && array_key_exists($thisUser['default_lang'], $languages)) ? '<span class="flag flag-' . $thisUser['default_lang'] . '"></span> ' . $Core->i18n()->translate($languages[$thisUser['default_lang']]['name']) : $Core->i18n()->translate('Nicht gesetzt');

$nonAjaxMVCViewPath = str_replace(DS . 'ajax', '', $Mvc->getMVCViewPath()) . DS. $Mvc->getControllerKey() . DS;
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">
        Access Control -  <?php echo $Core->i18n()->translate('Userprofil') ?>
        <?php if($canEdit): ?>
            <a href="<?php echo $editLink; ?>" class="btn btn-success btn-xs">
                <i class="fa fa-pencil" aria-hidden="true" title="<?php echo $Core->i18n()->translate('bearbeiten') ?>"></i>
            </a>
        <?php endif; ?>
    </h4>

</div>
<div class="modal-body">
    <p><?php echo $Core->i18n()->translate($infotext); ?> <?php echo $thisUser['name'] ?> <?php echo $thisUser['surname'] ?></p>
    <?php include($nonAjaxMVCViewPath . 'profile.body.php'); ?>
</div>