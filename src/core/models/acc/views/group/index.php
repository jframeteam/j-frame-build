<?php
/**
 * Acc Model View Group Index
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $accClass Acc
 */

$canCreateGroups = (is_object($accClass) && $accClass->hasAccess('acc_group_create'));
$canEditGroups = (is_object($accClass) && $accClass->hasAccess('acc_group_edit'));
$canDeleteGroups = (is_object($accClass) && $accClass->hasAccess('acc_group_delete'));

$canCreateRights = (is_object($accClass) && $accClass->hasAccess('acc_rights_create'));
$canEditRights = (is_object($accClass) && $accClass->hasAccess('acc_rights_edit'));
$canDeleteRights = (is_object($accClass) && $accClass->hasAccess('acc_rights_delete'));
?>
<p><?php $Core->i18n()->translate('Hier können die Benutzergruppen verwaltet werden'); ?></p>

<div class="action-wrapper row">
    <div class="col-sm-12">
        <div class="btn-set text-right">
            <?php if($canCreateGroups) : ?>
            <a href="<?php echo $Mvc->getModelAjaxUrl('acc') . '/group/create/group/'; ?>" class="btn btn-primary" title="<?php echo $Core->i18n()->translate('Benutzergruppe erstellen') ?>" data-toggle="modal" data-target="#editModal">
                <i class="fa fa-plus" aria-hidden="true"></i> <i class="fa fa-users" aria-hidden="true"></i>
            </a>
            <?php endif; ?>
            <?php if($canCreateRights) : ?>
            <a href="<?php echo $Mvc->getModelAjaxUrl('acc') . '/group/create/right/'; ?>" class="btn btn-primary" title="<?php echo $Core->i18n()->translate('Berechtigung erstellen') ?>" data-toggle="modal" data-target="#editModal">
                <i class="fa fa-plus" aria-hidden="true"></i> <i class="fa fa-lock" aria-hidden="true"></i>
            </a>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php
$groups = $accClass->getGroups();
$rights = $accClass->getRights();
$defaultGroup = $accClass->getDefaultGroup();
?>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-condensed">
        <thead>
        <tr>
            <th></th>
            <?php foreach($groups as $group): ?>
                <?php $isDefaultGroup = (array_key_exists('default', $group)) ? $group['default'] : null; ?>
                <?php
                if($isDefaultGroup === null) {
                    $defaultGroupId = (array_key_exists('id', $defaultGroup)) ? $defaultGroup['id'] : 0;

                    $group['default'] = ($defaultGroupId == $group['id']);
                }
                ?>
                <th class="text-center th-nowrap">
                    <?php echo $group['name'] ?>
                    <?php if($group['code'] != 'admin' && (($canEditGroups && $curUser['id_acc_groups'] !== $group['id']) || $curUser['su'])) : ?>
                        <a href="<?php echo $Mvc->getModelAjaxUrl('acc') . '/group/edit/group/' . $group['id'] ?>" class="btn btn-link text-success btn-xs" title="<?php echo $Core->i18n()->translate('Benutzergruppe bearbeiten') ?>" data-toggle="modal" data-target="#editModal">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                    <?php endif; ?>
                    <?php if(($canCreateGroups && $curUser['id_acc_groups'] !== $group['id']) || $curUser['su']) : ?>
                    <a href="<?php echo $Mvc->getModelAjaxUrl('acc') . '/group/create/group/' . $group['id']; ?>" class="btn btn-link text-success btn-xs" title="<?php echo $Core->i18n()->translate('Benutzergruppe aus dieser erstellen') ?>" data-toggle="modal" data-target="#editModal">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </a>
                    <?php endif; ?>
                    <?php if(($canEditGroups && $curUser['id_acc_groups'] !== $group['id']) || $curUser['su']) : ?>
                        <?php if($group['default']) : ?>
                            <span class="btn btn-link btn-xs text-primary" style="cursor: default;" data-toggle="tooltip" data-container="body" title="<?php echo $Core->i18n()->translate('Standart Benutzergruppe') ?>">
                                <i class="fa fa-circle" aria-hidden="true"></i>
                            </span>
                        <?php else: ?>
                            <a href="<?php echo $Mvc->getModelUrl('acc') . '/group/setdefaultgroup/' . $group['id'] ?>" class="btn btn-link text-success btn-xs" title="<?php echo $Core->i18n()->translate('Benutzergruppe bearbeiten') ?>">
                                <span class="change-icon text-primary" data-toggle="tooltip" data-container="body" title="<?php echo $Core->i18n()->translate('Zur Standart Benutzergruppe machen') ?>">
                                    <i class="fa fa-circle-o" aria-hidden="true"></i>
                                    <i class="fa fa-circle" aria-hidden="true"></i>
                                </span>
                            </a>
                        <?php endif; ?>
                    <?php else: ?>
                        <?php if($group['default']) : ?>
                            <span class="btn btn-link btn-xs text-primary" style="cursor: default;" data-toggle="tooltip" data-container="body" title="<?php echo $Core->i18n()->translate('Standart Benutzergruppe') ?>">
                                <i class="fa fa-circle" aria-hidden="true"></i>
                            </span>
                        <?php endif; ?>
                    <?php endif; ?>
                </th>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
        <?php foreach($rights as $rightCode => $right): ?>
            <?php $rightId             = (array_key_exists('id',$right)) ? $right['id'] : 0; ?>
            <?php $rightName           = (array_key_exists('name',$right) && $right['name'] != '') ? $right['name'] : $rightCode; ?>
            <?php $rightType           = (array_key_exists('type',$right) && $right['type'] != '') ? $right['type'] : ''; ?>
            <?php $fromWhitelist       = (array_key_exists('fromWhitelist',$right) && $right['fromWhitelist']); ?>
            <?php $editSaveRightUrl    = ($rightId > 0) ? $Mvc->getModelAjaxUrl('acc') . '/group/edit/right/' . $rightId : $Mvc->getModelAjaxUrl('acc') . '/group/create/right/' . $rightCode ?>
            <tr>
                <th>
                    <?php $spanAttr = ''; ?>
                    <?php $spanTitle = ''; ?>
                    <?php $spanClass = ''; ?>
                    <?php
                        if($rightId < 1 || $rightType == 'file') {
                            $spanTitle = $Core->i18n()->translate('Kein individueller Name im System vorhanden...');
                            $spanClass = 'text-warning';
                        }
                        if(!$fromWhitelist) {
                            $spanTitle .= ' ' . $Core->i18n()->translate('Nicht in Whitelist...');
                            $spanClass = 'text-danger';
                        }
                        if($spanTitle != '') {
                            $spanAttr .= ' title="' . $spanTitle . '"';
                            $spanAttr .= ' data-toggle="tooltip"';
                        }
                        if($spanClass != '') {
                            $spanAttr .= ' class="' . $spanClass . '"';
                        }
                    ?>
                    <span<?php echo $spanAttr; ?>>
                        <?php echo $rightName; ?>
                    </span>

                    <?php if($canEditRights || $curUser['su']) : ?>
                    <a href="<?php echo $editSaveRightUrl ?>" class="btn btn-link text-success btn-xs" title="<?php echo $Core->i18n()->translate('Berechtigung bearbeiten') ?>" data-toggle="modal" data-target="#editModal">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>
                    <?php endif; ?>
                </th>
                <?php foreach($groups as $groupId => $group): ?>
                    <td class="text-center">
                        <?php if(array_key_exists($rightId, $group['rights']) || array_key_exists($rightCode, $group['rights'])) : ?>
                            <i class="fa fa-check-circle text-success" aria-hidden="true"></i>
                        <?php else: ?>
                            <i class="fa fa-times-circle text-danger" aria-hidden="true"></i>
                        <?php endif; ?>
                    </td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?php echo $Core->i18n()->translate('Bearbeiten'); ?></h4>
            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Schließen') ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->