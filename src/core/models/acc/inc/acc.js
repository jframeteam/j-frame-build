accModelAjaxUrl = typeof accModelAjaxUrl !== 'undefined' ? accModelAjaxUrl : '';

var uosCheckTimer;
var curUser   = (typeof thisCurrUserId !== 'undefined' ? thisCurrUserId : 0);
var doUos     = (typeof doUos === 'undefined' || doUos !== 1) ? 0 : 1;
var doAjaxUos = (typeof doAjaxUos === 'undefined' || doAjaxUos !== 1) ? 0 : 1;

var acc = {
    init: function() {
        acc.user.init();
        acc.group.init();
    },

    user: {
        init: function() {
            /** Ajax Profile Modal */
            var profileLinkAjaxEle = $('[data-profilelink="ajax"]');
            if(profileLinkAjaxEle.length) {
                var profileModalHtml = '<div class="modal fade" id="profileModal" tabindex="-1" role="dialog" aria-labelledby="profileModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title">Userprofile</h4></div><div class="modal-body"><div class="te"></div></div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div></div></div></div>';

                var profileModalEle = $('#profileModal');
                if(!profileModalEle.length) {
                    $('body').append(profileModalHtml);
                    profileModalEle = $('#profileModal');
                }

                profileLinkAjaxEle.click(function(event) {
                    event.preventDefault();
                    profileLinkAjaxEle.removeClass('clicked');
                    $(this).addClass('clicked');
                    $('#profileModal').modal('show');
                });

                profileModalEle.on('show.bs.modal', function (e) {
                    var clickedEle = $('[data-profilelink="ajax"].clicked');
                    var getAjaxProfileUrl = $(e.relatedTarget).attr('href');
                    if(typeof getAjaxProfileUrl === 'undefined') {
                        getAjaxProfileUrl = clickedEle.attr('href');
                    }
                    $.get( getAjaxProfileUrl, function( data ) {
                        if(data.success == true) {
                            profileModalEle.find('.modal-content').html(data.result);

                            if(doUos === 1) {
                                acc.user.uos.updateHtml();
                            }
                        }
                    });
                });
            }

            /** User Edit Change Password Handler */
            var changePasswordEle = $('#changePassword');
            if(changePasswordEle.length) {
                changePasswordEle.on('show.bs.collapse', function () {
                    changePasswordEle.find('[type="password"]').addClass('validatePassword');
                });
                changePasswordEle.on('hide.bs.collapse', function () {
                    changePasswordEle.find('[type="password"]').removeClass('validatePassword');
                    changePasswordEle.find('[type="password"]').val('');
                });
            }

            /** Login Focus E-Mail Field */
            var loginMailEle = $('.login-wrapper input#mail');
            if(loginMailEle.length) {
                loginMailEle.focus();
            }

            /** User Online State Action */
            if(doUos === 1) {
                acc.user.uos.get();
            }

            /** User Options Action */
            acc.user.options.init();
        },

        getAjaxUrl:      function() {
            var AjaxUrl = '';

            if(accModelAjaxUrl !== '') {
                AjaxUrl = accModelAjaxUrl + '/';
            }
            if(AjaxUrl === '' && thisBaseUrl !== '') {
                var baseUrl = thisBaseUrl + 'ajax/' + langUrlSlug;
                var accUrlPath = 'acc/';

                AjaxUrl = baseUrl + accUrlPath;
            }
            return AjaxUrl;
        },

        uos: {

            data: {},

            get: function() {
                if(curUser !== 0 && doUos === 1 && accModelAjaxUrl !== '') {
                    var AjaxUrl = acc.user.getAjaxUrl();
                    var uosUrl = AjaxUrl + "user/checkuos";
                    $.get(uosUrl, function (data) {
                        if (data.success) {
                            var result = data.result;
                            var rIsObj = (typeof result === 'object');
                            if (rIsObj) {
                                acc.user.uos.data = result.uos;
                                var uosdIsObj = (typeof acc.user.uos.data === 'object');
                                if (uosdIsObj) {
                                    acc.user.uos.updateHtml();
                                    if(doAjaxUos) {
                                        uosCheckTimer = setTimeout(acc.user.uos.get(), 3000);
                                    }
                                }
                            }
                        }
                    });
                }
            },

            updateHtml: function() {
                var uosViewerEle = $('.uos_viewer');
                if(uosViewerEle.length) {
                    uosViewerEle.each(function(uv_i, uv_ele) {
                        var uos_v_ele = $(uv_ele);
                        var uid = uos_v_ele.data('uid');
                        var uData = (typeof acc.user.uos.data[uid] !== 'undefined') ? acc.user.uos.data[uid] : false;
                        if(uData !== false) {
                            uData.state_int = (typeof uData.state_int !== 'undefined') ? uData.state_int : 0;
                            uData.state_text = (typeof uData.state_text !== 'undefined') ? uData.state_text : '';
                            var onlineState = uData.state_int;
                            var titleTxt = uData.state_text;
                            var textClass = '';
                            if(onlineState == 1) {
                                textClass = 'text-success';
                            }
                            if(onlineState == 2) {
                                textClass = 'text-muted';
                            }
                            if(onlineState == 0) {
                                textClass = 'text-danger';
                            }

                            uos_v_ele.removeAttr('data-toggle').attr('data-toggle','tooltip');

                            var doTitleAttr = uos_v_ele.attr('data-original-title');

                            if(typeof doTitleAttr !== typeof undefined && doTitleAttr !== false) {
                                uos_v_ele.attr('data-original-title',titleTxt);
                            } else {
                                uos_v_ele.removeAttr('title').attr('title',titleTxt);
                            }

                            uos_v_ele.removeClass('text-success text-warning text-danger').addClass(textClass);
                        }
                    });
                }
            }

        },

        options: {

            init: function() {
                if(acc.user.options.data === null) {
                    acc.user.options.setData();
                }
            },

            data: null,

            setData: function() {
                var AjaxUrl = acc.user.getAjaxUrl();
                var getOptionsUrl = AjaxUrl + 'user/getOptions/';

                $.ajax({
                    type:           'get',
                    cache:          false,
                    url:            getOptionsUrl
                })
                    .done(function( data ) {
                        if(typeof data === 'object') {
                            acc.user.options.data = data;
                        }
                    });
            },

            get: function(option, userId) {
                option = typeof option !== 'undefined' ? option : '';
                userId = typeof userId !== 'undefined' ? userId : curUser;

                if(acc.user.options.data === null) {
                    acc.user.options.setData();
                }

                var getData = {};

                if(userId > 0) {
                    if(acc.user.options.data !== null) {
                        if(typeof acc.user.options.data[userId] !== 'undefined') {
                            getData = acc.user.options.data[userId];

                            if(option !== '' && typeof getData[option] !== 'undefined') {
                                getData = getData[option];
                            }
                        }
                        return getData;
                    }
                }
            },

            set: function(option, value, userId) {
                option = typeof option !== 'undefined' ? option : '';
                value  = typeof value  !== 'undefined' ? value  : '';
                userId = typeof userId !== 'undefined' ? userId : curUser;

                if(userId > 0 && option !== '') {
                    var old_option_value = acc.user.options.get(option, userId);

                    if(value != old_option_value) {
                        var AjaxUrl = acc.user.getAjaxUrl();
                        var setOptionUrl = AjaxUrl + 'user/setOption/';
                        var setOptionUrlParams = 'id/' + userId + '/option/' + option + '/value/' + value;
                        var setOptionUrlFull = setOptionUrl + setOptionUrlParams;

                        if(core.helper.isArray(value) || core.helper.isObject(value)) {
                            var postData = {};

                            postData.id     = userId;
                            postData.option = option;
                            postData.value  = value;

                            $.ajax({
                                type:           'post',
                                cache:          false,
                                url:            setOptionUrl,
                                data:           {'data':JSON.stringify(postData)}
                            })
                                .done(function( data ) {
                                    if (data) {
                                        core.bs_alert.success('Benutzer Option erfolgreich gespeichert.', '', '', true);
                                        acc.user.options.setData();
                                    } else {
                                        core.bs_alert.danger('Benutzer Option konnte nicht gespeichert werden.', '', '', true);
                                    }
                                });
                        } else {
                            $.get(setOptionUrlFull, function (data) {
                                if (data) {
                                    core.bs_alert.success('Benutzer Option erfolgreich gespeichert.', '', '', true);
                                    acc.user.options.setData();
                                } else {
                                    core.bs_alert.danger('Benutzer Option konnte nicht gespeichert werden.', '', '', true);
                                }
                            });
                        }
                    }
                }
            }
        }
    },

    group: {
        init: function() {
            var editModalEle = $('#editModal');
            if(editModalEle.length) {
                editModalEle.on('shown.bs.modal', function () {
                    acc.group.checkCodeEntry();
                });
            }
        },

        checkCodeEntry: function() {
            var codeEle = $('#edit_create_form').find('#code');
            if(accModelAjaxUrl != '' && codeEle.length) {
                var checkType =  codeEle.data('checkcodetype');
                if(checkType != '') {
                    codeEle.on('input', function () {
                        var checkCode = codeEle.val();
                        if(checkCode != '') {
                            var getAjaxCheckCodeUrl = accModelAjaxUrl + '/group/check/' + checkType + '/' + checkCode;
                            $.get(getAjaxCheckCodeUrl, function (data) {
                                if (data.success) {
                                    var result = data.result;
                                    var rIsObj = (typeof result === 'object');
                                    if (rIsObj) {
                                        var success = result.success;
                                        var dynamicAlertDangerEle;
                                        var codeEleParent = codeEle.parent();
                                        var codeEleParentParent = codeEleParent.parent();
                                        var submittButtonEle = $('button[type=submit]');
                                        if (!success) {
                                            dynamicAlertDangerEle = $('.alert-danger.dynamicadded');
                                            if(!dynamicAlertDangerEle.length) {
                                                core.bs_alert.danger(result.message, '', '#editModal .modal-body');
                                            }
                                            submittButtonEle.prop('disabled', true);
                                            codeEleParentParent.addClass('has-error');
                                        } else {
                                            dynamicAlertDangerEle = $('.alert-danger.dynamicadded');
                                            if(dynamicAlertDangerEle.length) {
                                                dynamicAlertDangerEle.remove();
                                            }
                                            submittButtonEle.prop('disabled', false);
                                            codeEleParentParent.removeClass('has-error');
                                        }
                                    }
                                }
                            });
                        }
                    });
                }
            }
        }
    }
};

$( document ).ready(function() {
    acc.init();
});