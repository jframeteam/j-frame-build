<?php
/**
 * Acc Model Controller Group
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function indexAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    if(!$accClass->hasAccess('acc_group')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('acc_group') . ' - ' . $Core->i18n()->translate('acc') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('acc') . ' - ' . $Core->i18n()->translate('acc_group'));

        $curUser = $accClass->getUser();

        $return = '';

        $ob_return = '';
        if($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    }
}

function saveAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    /**
     * @var $plugins Plugins
     * @var $pitsForms PitsForms
     * @var $pitsCore PitsCore
     */

    /** Prepare Form Plugin */
    $plugins = $Core->Plugins();
    $pitsForms = $plugins->PitsForms();

    $pitsCore = $plugins->PitsCore();

    $accClass = $Mvc->modelClass('Acc');

    if(!$accClass->hasAccess('acc_group_edit') || !$accClass->hasAccess('acc_rights_edit')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $return = '';

        $rights = $accClass->getRights();
        $groups = $accClass->getGroups();

        $params  = $Mvc->getMvcParams();

        $paramSaveId = 0;

        $saveType = '';

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'group') {
                $saveType = 'group';
                $paramSaveId = (isset($params['group'])) ? (int)$params['group'] : 'noId';
            }
            if($first_key == 'right') {
                $saveType = 'right';
                $paramSaveId = (isset($params['right'])) ? (int)$params['right'] : 0;
            }
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if(is_object($pitsForms)) {
                $pitsCore->decryptPOST();
            }
            $post = $Core->Request()->getPost();

            $rightsArray = (array_key_exists('rights',$post) && is_array($post['rights'])) ? $post['rights'] : array();

            if(count($rightsArray)) {
                $rightsArrayClean = array();
                foreach ($rightsArray as $rightCode) {
                    if (!is_numeric($rightCode)) {
                        $rightsArrayClean[] = $rightCode;
                    }
                }
                $post['rights'] = $rightsArrayClean;
            }

            if($saveType != '') {
                if($saveType == 'group') {
                    $saveResult = $accClass->saveGroup($paramSaveId,$post);
                    // $return .= '<pre>' .  print_r($saveResult, true) . '</pre>';
                    return $return;
                }
                if($saveType == 'right') {
                    $saveResult = $accClass->saveRight($paramSaveId,$post);
                    // $return .= '<pre>' .  print_r($saveResult, true) . '</pre>';
                    return $return;
                }
            }
        }

        return $return;
    }
}

function deleteAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    if(!$accClass->hasAccess('acc_group_delete') || !$accClass->hasAccess('acc_rights_delete')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $return = '';

        $rights = $accClass->getRights();
        $groups = $accClass->getGroups();

        $params  = $Mvc->getMvcParams();

        $paramDeleteId = 0;

        $deleteType = '';

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'group') {
                $deleteType = 'group';
                $paramDeleteId = (isset($params['group'])) ? (int)$params['group'] : 0;
            }
            if($first_key == 'right') {
                $deleteType = 'right';
                $paramDeleteId = (isset($params['right'])) ? (int)$params['right'] : 0;
            }
        }

        $errMsg = '';
        $hasErr = false;
        if($paramDeleteId < 1 || $deleteType == '') {
            $errMsg = $Core->i18n()->translate('Kein Typ übergeben, nichts zu tun...');
            $hasErr = true;
        }

        if(!$hasErr) {
            if($deleteType == 'group') {
                $return .= '<pre>' .  print_r($accClass->deleteGroup($paramDeleteId), true) . '</pre>';
                return $return;
            }
            if($deleteType == 'right') {
                $return .= '<pre>' .  print_r($accClass->deleteRight($paramDeleteId), true) . '</pre>';
                return $return;
            }
        } else {
            $note  = $errMsg;
            $type  = 'danger';
            $kind  = 'bs-alert';
            $title = $this->Core->i18n()->translate('Fehler') . '!';
            $this->Core->setNote($note, $type, $kind, $title);

            $redirectUrl = $this->Mvc->getModelUrl() . '/group/';
            $Core->Request()->redirect($redirectUrl);
        }

        return $return;
    }
}

function setdefaultgroupAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    /**
     * @var $plugins Plugins
     * @var $pitsForms PitsForms
     */

    /** Prepare Form Plugin */
    $plugins = $Core->Plugins();
    $pitsForms = $plugins->PitsForms();

    $accClass = $Mvc->modelClass('Acc');

    if(!$accClass->hasAccess('acc_group_edit') || !$accClass->hasAccess('acc_rights_edit')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $return = '';
        $groups = $accClass->getGroups();

        $params  = $Mvc->getMvcParams();

        $paramGroupId = 0;

        $saveType = '';

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id') {
                $paramGroupId = (isset($params['id'])) ? (int)$params['id'] : 0;
            } else {
                $paramGroupId = (int)$first_key;
            }
        }

        $newDefaultGroup = (array_key_exists($paramGroupId, $groups)) ? $groups[$paramGroupId] : array();

        $note  = $Core->i18n()->translate('Benutzergruppe kann nicht geändert werden!');
        $type  = 'danger';
        $kind  = 'bs-alert';
        $title = $Core->i18n()->translate('Fehler') . '!';

        if(count($newDefaultGroup)) {
            $setDefaultGroupResult = $accClass->setDefaultGroup($paramGroupId);

            if($setDefaultGroupResult) {
                $note  = sprintf( $Core->i18n()->translate('Benutzergruppe \'%s\' erfolgreich gespeichert!!'), $newDefaultGroup['name']);
                $type  = 'success';
                $kind  = 'bs-alert';
                $title = $Core->i18n()->translate('Erledigt') . '!';
            }
        }

        $Core->setNote($note, $type, $kind, $title);

        $redirectUrl = $Mvc->getModelUrl() . '/group/';
        $Core->Request()->redirect($redirectUrl);

        return $return;
    }
}

function rawAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    if(!$accClass->hasAccess('acc_group')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $return = '';

        $return .= '<pre>';
        $return .= '$accClass->getRights() => ' . print_r($accClass->getRights(), true) . PHP_EOL;
        $return .= '$accClass->getGroups() => ' . print_r($accClass->getGroups(), true);
        $return .= '</pre>';

        return $return;
    }
}