<?php
/**
 * Acc Model Ajax Controller Group
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function createAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    if(!$accClass->hasAccess('acc_group_create')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $return = '';

        $params  = $Mvc->getMvcParams();

        $rights = $accClass->getRights();
        $groups = $accClass->getGroups();

        $groupPatternId = 0;

        $createType = '';

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'group') {
                $createType = 'group';
                $groupPatternId = ($params['group'] != '' && array_key_exists($params['group'], $groups)) ? $params['group'] : 'noId';
            }
            if($first_key == 'right') {
                $createType = 'right';
            }
        }

        $groupPattern = (array_key_exists($groupPatternId,$groups)) ? $groups[$groupPatternId] : array();

        $errMsg = '';
        $hasError = false;
        if($createType == '') {
            $errMsg = $Core->i18n()->translate('Kein Typ übergeben, nichts zu tun...');
            $hasError = true;
        }

        $ob_return = '';
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
        $return .= $ob_return;

        return $return;
    }
}

function editAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    if(!$accClass->hasAccess('acc_group_edit')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $return = '';

        $params  = $Mvc->getMvcParams();

        $rights = $accClass->getRights();
        $groups = $accClass->getGroups();

        $paramEditId = 0;

        $editType = '';

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'group') {
                $editType = 'group';
                $paramEditId = (isset($params['group'])) ? (int)$params['group'] : 0;
            }
            if($first_key == 'right') {
                $editType = 'right';
                $paramEditId = (isset($params['right'])) ? (int)$params['right'] : 0;
            }
        }

        $errMsg = '';
        $hasError = false;
        if($paramEditId < 1 || $editType == '') {
            $errMsg = $Core->i18n()->translate('Kein Typ übergeben, nichts zu tun...');
            $hasError = true;
        }

        if($editType == 'group' && !array_key_exists($paramEditId, $groups)) {
            $errMsg = sprintf($Core->i18n()->translate('Benutzergruppe mit der ID \'%s\' konnte nicht gefunden werden...'), $paramEditId);
            $hasError = true;
        }

        if($editType == 'right' && !count($accClass->getRightById($paramEditId))) {
            $errMsg = sprintf($Core->i18n()->translate('Berechtigung mit der ID \'%s\' konnte nicht gefunden werden...'), $paramEditId);
            $hasError = true;
        }

        $ob_return = '';
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
        $return .= $ob_return;

        return $return;
    }
}

function checkAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    if(!$accClass->hasAccess('acc_group_edit') || !$accClass->hasAccess('acc_group_create')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $return = array();

        $params = $Mvc->getMvcParams();

        $paramCheckCode = '';

        $editType = '';

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'group') {
                $editType = 'group';
                $paramCheckCode = (isset($params['group'])) ? $params['group'] : '';
            }
            if($first_key == 'right') {
                $editType = 'right';
                $paramCheckCode = (isset($params['right'])) ? $params['right'] : '';
            }
        }

        $errMsg = '';
        $hasError = false;
        if($paramCheckCode == '' || $editType == '') {
            $errMsg = $Core->i18n()->translate('Kein Typ übergeben, nichts zu tun...');
            $hasError = true;
        }


        $return['editType'] = $editType;
        $return['paramCheckCode'] = $paramCheckCode;
        $return['groupExists'] = $accClass->groupExists($paramCheckCode);

        if($editType == 'group' && $accClass->groupExists($paramCheckCode)) {
            $errMsg = sprintf($Core->i18n()->translate('Benutzergruppe mit dem Code \'%s\' existiert bereits!'), $paramCheckCode);
            $hasError = true;
        }

        if($editType == 'right' && $accClass->rightExists($paramCheckCode)) {
            $errMsg = sprintf($Core->i18n()->translate('Berechtigung mit dem Code \'%s\' existiert bereits!'), $paramCheckCode);
            $hasError = true;
        }

        $return['success'] = (!$hasError) ? true : false;
        $return['message'] = $errMsg;

        return $return;
    }
}