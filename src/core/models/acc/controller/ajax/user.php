<?php
/**
 * Acc Model Ajax Controller User
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function profileAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    $return = '';

    $params  = $Mvc->getMvcParams();
    $curUser = $accClass->getUser();

    $curUserId   = $curUser['id'];
    $paramUserId = $curUserId;

    if(count($params)) {
        reset($params);
        $first_key = key($params);
        if($first_key == 'id') {
            $paramUserId = (isset($params['id'])) ? (int)$params['id'] : 0;
        } else {
            $paramUserId = (int)$first_key;
        }
    }

    $showCurrUser = ($paramUserId == $curUserId) ? true : false;

    if($showCurrUser) {
        $userToView = $curUser;
    } else {
        $userToView = $accClass->getUser($paramUserId);
        if(!$accClass->hasAccess('acc_user')) {
            $return .= $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
            return $return;
        } else {
            if(!$accClass->userExists($paramUserId) || ($userToView['su'] && !$curUser['su'])) {
                $return .= sprintf($Core->i18n()->translate('User mit der ID \'%s\' konnte nicht gefunden werden...'), $paramUserId);
                return $return;
            }
        }
    }

    $ob_return = '';
    try {
        ob_start();
        include($Mvc->getMVCViewFilePath());
        $ob_return = ob_get_contents();
        ob_end_clean();
    } catch (Exception $e) {
        $ob_return = $e->getMessage();
    }
    $return .= $ob_return;

    return $return;
}

function checkifusermailexistsAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    $return = '';

    $params  = $Mvc->getMvcParams();

    $paramUserEmail = '';

    if(count($params)) {
        reset($params);
        $first_key = key($params);
        if($first_key == 'email') {
            $paramUserEmail = (isset($params['email'])) ? trim($params['email']) : '';
        } else {
            $paramUserEmail = trim($first_key);
        }
    }

    if($paramUserEmail != '') {

        if(!$accClass->hasAccess('acc_user')) {
            $return = $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
        } else {
            $result = array();
            $userByEmail = $accClass->getUserByEmail(strtolower($paramUserEmail));
            if(count($userByEmail)) {
                $result['found'] = true;
                $result['msg'] = sprintf($Core->i18n()->translate('E-Mail Adresse \'%s\' bereits vergeben...'), strtolower($paramUserEmail));
            } else {
                $result['found'] = false;
                $result['msg'] = sprintf($Core->i18n()->translate('E-Mail Adresse \'%s\' verfügbar...'), strtolower($paramUserEmail));;
            }
            $return = $result;
        }
    } else {
        $return = $Core->i18n()->translate('Keine E-Mail Adresse zum prüfen...');
    }

    return $return;
}

function checkuosAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    $return = '';

    if(!$accClass->hasAccess('acc_user')) {
        $return .= $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
        return $return;
    } else {
        $userOnlineStates = $accClass->getUserOnlineStates();
        $return = array();
        $return['uos'] = $userOnlineStates;
    }

    return $return;
}

function getOptionsAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    $params  = $Mvc->getMvcParams();
    $curUser = $accClass->getUser();

    $curUserId   = (count($curUser) && array_key_exists('id',$curUser)) ? $curUser['id'] : 0;
    $paramUserId = $curUserId;
    $paramOption = '';

    if(count($params)) {
        reset($params);
        $first_key = key($params);
        if($first_key == 'id') {
            $paramUserId = (array_key_exists('id',$params)) ? (int)$params['id'] : 0;
            $paramOption = (array_key_exists('option',$params)) ? $params['option'] : '';
        } else {
            $paramUserId = (int)$first_key;
            $paramOption = $params[$first_key];
        }
    }

    $itsMe = ($paramUserId == $curUserId) ? true : false;

    if (!$accClass->hasAccess('acc_user_edit') || !$itsMe) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $Mvc->setMVCPlainAjax(true);

        $return = array();

        if($itsMe) {
            $return = $accClass->getUserOptions($paramUserId);

            if($paramOption !== '' && array_key_exists($paramOption, $return)) {
                $return = $return[$paramOption];
            }
        }

        if($accClass->hasAccess('acc_user_edit')) {
            if(!$itsMe && $paramUserId > 0 && $accClass->userExists($paramUserId)) {
                $return = $accClass->getUserOptions($paramUserId);

                if($paramOption !== '' && array_key_exists($paramOption, $return)) {
                    $return = $return[$paramOption];
                }
            } else {
                $return = $accClass->getUserOptions();
            }
        }

        return $return;

    }
}

function setOptionAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    $params  = $Mvc->getMvcParams();
    $curUser = $accClass->getUser();

    $curUserId   = (is_array($curUser) && count($curUser)) ? $curUser['id'] : 0;
    $paramUserId = $curUserId;
    $paramOption = '';
    $paramValue  = '';

    if(count($params)) {
        $paramUserId = (array_key_exists('id',    $params)) ? (int)$params['id'] : $curUserId;
        $paramOption = (array_key_exists('option',$params)) ? $params['option']  : '';
        $paramValue  = (array_key_exists('value', $params)) ? $params['value']   : '';
    }


    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $post = $Core->Request()->getPost();

        $post = (array)json_decode(reset($post));

        $paramUserId = (array_key_exists('id',    $post)) ? (int)$post['id']      : $curUserId;
        $paramOption = (array_key_exists('option',$post)) ? $post['option']       : '';
        $paramValue  = (array_key_exists('value', $post)) ? (array)$post['value'] : '';
    }

    $itsMe = ($paramUserId == $curUserId) ? true : false;

    if (!$accClass->hasAccess('acc_user_edit') || !$itsMe) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $Mvc->setMVCPlainAjax(true);
        $return = false;

        if($itsMe) {
            if($accClass->setUserOption($paramUserId, $paramOption, $paramValue)) {
                $return = $accClass->getUserOptions($paramUserId);
            }
        }

        if($accClass->hasAccess('acc_user_edit')) {
            if(!$itsMe) {
                if($accClass->userExists($paramUserId)) {
                    $return = ($accClass->setUserOption($paramUserId, $paramOption, $paramValue));
                }
            } else {
                $return = ($accClass->setUserOption($paramUserId, $paramOption, $paramValue));
            }
        }

        return $return;
    }
}