<?php
/**
 * Acc Model Controller User
 *
 * Some Methods implemented from Source by Planet ITservices GmbH & Co. KG
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function indexAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    if(!$accClass->hasAccess('acc_user')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('acc_user') . ' - ' . $Core->i18n()->translate('acc') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('acc') . ' - ' . $Core->i18n()->translate('acc_user'));

        $curUser = $accClass->getUser();

        $return = '';

        $ob_return = '';
        if($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    }
}

function profileAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    $oldMetaTitle = $Mvc->getMetaTitle();
    $Mvc->setMetaTitle($Core->i18n()->translate('Profil') . ' - ' . $Core->i18n()->translate('acc') . ' - ' . $oldMetaTitle);
    $oldPageTitle = $Mvc->getPageTitle();
    $Mvc->setPageTitle($Core->i18n()->translate('acc') . ' - ' . $Core->i18n()->translate('Profil'));

    $return = '';

    $params  = $Mvc->getMvcParams();
    $curUser = $accClass->getUser();

    $curUserId   = $curUser['id'];
    $paramUserId = $curUserId;

    if(count($params)) {
        reset($params);
        $first_key = key($params);
        if($first_key == 'id') {
            $paramUserId = (isset($params['id'])) ? (int)$params['id'] : 0;
        } else {
            $paramUserId = (int)$first_key;
        }
    }

    $showCurrUser = ($paramUserId == $curUserId) ? true : false;

    if($showCurrUser) {
        $userToView = $curUser;
    } else {
        $userToView = $accClass->getUser($paramUserId);
        if(!$accClass->hasAccess('acc_user')) {
            $return .= $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
            return $return;
        } else {
            if(!$accClass->userExists($paramUserId) || ($userToView['su'] && !$curUser['su'])) {
                $return .= sprintf($Core->i18n()->translate('User mit der ID \'%s\' konnte nicht gefunden werden...'), $paramUserId);
                return $return;
            }
        }
    }

    $userOnlineState = $accClass->getUserOnlineStates($userToView['id']);

    $ob_return = '';
    if($Mvc->getMVCViewFilePath() !== false) {
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
    } else {
        $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
    }
    $return .= $ob_return;

    return $return;
}

function editAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    $return = '';

    $oldMetaTitle = $Mvc->getMetaTitle();
    $Mvc->setMetaTitle($Core->i18n()->translate('User bearbeiten') . ' - ' . $Core->i18n()->translate('acc') . ' - ' . $oldMetaTitle);
    $oldPageTitle = $Mvc->getPageTitle();
    $Mvc->setPageTitle($Core->i18n()->translate('acc') . ' - ' . $Core->i18n()->translate('User bearbeiten'));

    $params  = $Mvc->getMvcParams();
    $curUser = $accClass->getUser();

    $curUserId   = $curUser['id'];
    $paramUserId = $curUserId;

    if(count($params)) {
        reset($params);
        $first_key = key($params);
        if($first_key == 'id') {
            $paramUserId = (isset($params['id'])) ? (int)$params['id'] : 0;
        } else {
            $paramUserId = (int)$first_key;
        }
    }

    $showCurrUser = ($paramUserId == $curUserId) ? true : false;

    if($showCurrUser) {
        $userToView = $curUser;
    } else {
        if(!$accClass->hasAccess('acc_user_edit')) {
            $return .= $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
            return $return;
        } else {
            if($accClass->userExists($paramUserId)) {
                $userToView = $accClass->getUser($paramUserId);
            } else {
                $return .= sprintf($Core->i18n()->translate('User mit der ID \'%s\' konnte nicht gefunden werden...'), $paramUserId);
                return $return;
            }
        }
    }

    $ob_return = '';
    if($Mvc->getMVCViewFilePath() !== false) {
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
    } else {
        $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
    }
    $return .= $ob_return;

    return $return;
}

function firstLoginAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    $return = '';

    $oldMetaTitle = $Mvc->getMetaTitle();
    $Mvc->setMetaTitle($Core->i18n()->translate('Passwort ändern') . ' - ' . $Core->i18n()->translate('acc') . ' - ' . $oldMetaTitle);
    $oldPageTitle = $Mvc->getPageTitle();
    $Mvc->setPageTitle($Core->i18n()->translate('acc') . ' - ' . $Core->i18n()->translate('Passwort ändern'));

    $userToView = $accClass->getUser();

    $ob_return = '';
    if($Mvc->getMVCViewFilePath() !== false) {
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
    } else {
        $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
    }
    $return .= $ob_return;

    return $return;
}

function createAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    $return = '';

    $oldMetaTitle = $Mvc->getMetaTitle();
    $Mvc->setMetaTitle($Core->i18n()->translate('User erstellen') . ' - ' . $Core->i18n()->translate('acc') . ' - ' . $oldMetaTitle);
    $oldPageTitle = $Mvc->getPageTitle();
    $Mvc->setPageTitle($Core->i18n()->translate('acc') . ' - ' . $Core->i18n()->translate('User erstellen'));

    if(!$accClass->hasAccess('acc_user_create')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $ob_return = '';
        if($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }
        $return .= $ob_return;
    }

    return $return;
}

function saveAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    /**
     * @var $plugins Plugins
     * @var $pitsForms PitsForms
     * @var $pitsCore PitsCore
     */

    /** Prepare Form Plugin */
    $plugins = $Core->Plugins();
    $pitsForms = $plugins->PitsForms();

    $pitsCore = $plugins->PitsCore();

    $accClass = $Mvc->modelClass('Acc');

    $curUser = $accClass->getUser();
    $params  = $Mvc->getMvcParams();

    $paramsUserId = 0;

    if(count($params)) {
        reset($params);
        $first_key = key($params);
        if($first_key == 'id' && $first_key != 'new') {
            $paramsUserId = (isset($params['id'])) ? (int)$params['id'] : 0;
        } else {
            $paramsUserId = (int)$first_key;
        }
    }

    $user = ($paramsUserId !== 0 && $accClass->userExists($paramsUserId)) ? $accClass->getUser($paramsUserId) : array();

    $saveMyself = (count($user) && array_key_exists('id', $user) && $user['id'] === $curUser['id']);

    if($saveMyself || $accClass->hasAccess('acc_user_edit') || $accClass->hasAccess('acc_user_create')) {

        $return = '';

        $new = (!count($user)) ? true : false;

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if(is_object($pitsForms)) {
                $pitsCore->decryptPOST();
            }
            $post = $Core->Request()->getPost();

            $sitesArray = (array_key_exists('id_system_sites',$post) && is_array($post['id_system_sites'])) ? $post['id_system_sites'] : array();

            if(count($sitesArray)) {
                $sitesArrayClean = array();
                foreach ($sitesArray as $siteId) {
                    $siteId = (int)$siteId;
                    if ($siteId !== 0) {
                        $sitesArrayClean[] = $siteId;
                    }
                }
                $post['id_system_sites'] = $sitesArrayClean;
            }

            $saveResult = true;

            if(!$new) {
                if(isset($post['action'])) {
                    if($post['action'] == 'user_save' || $post['action'] == 'user_save_exit') {
                        $saveResult = $accClass->saveUser($paramsUserId, $post);
                        // $return .= '<pre>' . $saveResult . '</pre>';
                        // return $return;
                    }

                    if($post['action'] == 'user_delete' && $accClass->hasAccess('acc_user_delete')) {
                        $permanently = (isset($post['permanently']) && $post['permanently'] == '1') ? true: false;
                        $saveResult = $accClass->saveUser($paramsUserId, $post);
                        // $return .= '<pre>' . $saveResult . '</pre>';
                        // return $return;
                    }
                }
            } else {
                $post['id'] = 0;
                if($post['action'] == 'user_save' || $post['action'] == 'user_save_exit') {
                    $saveResult = $accClass->saveUser($paramsUserId, $post);
                    // $return .= '<pre>' . $saveResult . '</pre>';
                    // return $return;
                }
            }

            if(is_bool($saveResult) && !$saveResult) {
                $redirectUrl = $Mvc->getModelUrl('acc') . '/user/edit/' . $user['id'];
                if($post['action'] == 'user_save_exit') {
                    $redirectUrl = $Mvc->getModelUrl('acc') . '/user/';
                }
                $Core->Request()->redirect($redirectUrl);
            }
        }
    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }
}

function activateAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    if($accClass->hasAccess('acc_user_edit')) {


        $return = '';
        $params  = $Mvc->getMvcParams();


        $paramsUserId = 0;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id' && $first_key != 'new') {
                $paramsUserId = (isset($params['id'])) ? (int)$params['id'] : 0;
            } else {
                $paramsUserId = (int)$first_key;
            }
        }

        $user = ($paramsUserId !== 0 && $accClass->userExists($paramsUserId)) ? $accClass->getUser($paramsUserId) : array();

        $new = (!count($user)) ? true : false;

        if(!$new) {
            $data = array();
            $data['id'] = $paramsUserId;
            $data['active'] = 1;
            $data['action'] = 'user_save_exit';

            $return = '<pre>' . print_r($data, true) . '</pre>';
            $return .= '<pre>' .$accClass->saveUser($paramsUserId, $data) . '</pre>';
            return $return;
        } else {
            $note  = sprintf( $Core->i18n()->translate('Fehler beim speichern der Daten!'), $user['name'] . ' ' . $user['surname']);
            $type  = 'danger';
            $kind  = 'bs-alert';
            $title = $Core->i18n()->translate('Erledigt') . '!';
            $Core->setNote($note, $type, $kind, $title);

            $redirectUrl = $Mvc->getModelUrl() . '/user/';
            $Core->Request()->redirect($redirectUrl);
        }
    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }
}

function deactivateAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    if($accClass->hasAccess('acc_user_edit')) {

        $return = '';
        $params  = $Mvc->getMvcParams();


        $paramsUserId = 0;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id' && $first_key != 'new') {
                $paramsUserId = (isset($params['id'])) ? (int)$params['id'] : 0;
            } else {
                $paramsUserId = (int)$first_key;
            }
        }

        $user = ($paramsUserId !== 0 && $accClass->userExists($paramsUserId)) ? $accClass->getUser($paramsUserId) : array();

        $new = (!count($user)) ? true : false;

        if(!$new) {
            $data = array();
            $data['id'] = $paramsUserId;
            $data['active'] = 0;
            $data['action'] = 'user_save_exit';

            $return = '<pre>' . print_r($data, true) . '</pre>';
            $return .= '<pre>' .$accClass->saveUser($paramsUserId, $data) . '</pre>';
            return $return;
        } else {
            $note  = sprintf( $Core->i18n()->translate('Fehler beim speichern der Daten!'), $user['name'] . ' ' . $user['surname']);
            $type  = 'danger';
            $kind  = 'bs-alert';
            $title = $Core->i18n()->translate('Erledigt') . '!';
            $Core->setNote($note, $type, $kind, $title);

            $redirectUrl = $Mvc->getModelUrl() . '/user/';
            $Core->Request()->redirect($redirectUrl);
        }
    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }
}

function deleteAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    if($accClass->hasAccess('acc_user_delete')) {

        $return = '';

        $curUser = $accClass->getUser();
        $params  = $Mvc->getMvcParams();

        $paramsUserId = 0;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id' && $first_key != 'new') {
                $paramsUserId = (isset($params['id'])) ? (int)$params['id'] : 0;
            } else {
                $paramsUserId = (int)$first_key;
            }
        }

        $user = ($paramsUserId !== 0 && $accClass->userExists($paramsUserId)) ? $accClass->getUser($paramsUserId) : array();

        if(count($user) && $curUser['id'] != $user['id'] && !$user['su']) {
            $return .= '<pre>' .$accClass->deleteUser($paramsUserId) . '</pre>';
            return $return;
        } else {
            $note  = $Core->i18n()->translate('Fehler beim löschen des Users!');
            $type  = 'danger';
            $kind  = 'bs-alert';
            $title = $Core->i18n()->translate('Fehler') . '!';
            $Core->setNote($note, $type, $kind, $title);

            $redirectUrl = $Mvc->getModelUrl() . '/user/';
            $Core->Request()->redirect($redirectUrl);
        }
    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }
}

function setsuAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    if($accClass->hasAccess('acc_user_edit')) {

        $return = '';

        $curUser = $accClass->getUser();
        $params  = $Mvc->getMvcParams();

        $paramsUserId = 0;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id' && $first_key != 'new') {
                $paramsUserId = (isset($params['id'])) ? (int)$params['id'] : 0;
            } else {
                $paramsUserId = (int)$first_key;
            }
        }

        $user = ($paramsUserId !== 0 && $accClass->userExists($paramsUserId)) ? $accClass->getUser($paramsUserId) : array();

        if($curUser['su'] && count($user) && $curUser['id'] != $user['id']) {
            $return .= '<pre>' .$accClass->setUserToSu($paramsUserId) . '</pre>';
            return $return;
        } else {
            $note  = $Core->i18n()->translate('Fehler beim speichern der Daten!');
            $type  = 'danger';
            $kind  = 'bs-alert';
            $title = $Core->i18n()->translate('Fehler') . '!';
            $Core->setNote($note, $type, $kind, $title);

            $redirectUrl = $Mvc->getModelUrl() . '/user/';
            $Core->Request()->redirect($redirectUrl);
        }
    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }
}

function loginAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     * @var $plugins Plugins
     * @var $forms Forms
     */
    global $Core, $Mvc;
    $accClass = $Mvc->modelClass('Acc');

    $oldMetaTitle = $Mvc->getMetaTitle();
    $Mvc->setMetaTitle($Core->i18n()->translate('Login') . ' - ' . $Core->i18n()->translate('acc') . ' - ' . $oldMetaTitle);
    $oldPageTitle = $Mvc->getPageTitle();
    $Mvc->setPageTitle($Core->i18n()->translate('acc') . ' - ' . $Core->i18n()->translate('Login'));

    /** Prepare Forms Plugin */
    $plugins = $Core->Plugins();
    $forms   = $plugins->Forms();
    $isForms = (is_object($forms));

    if(!$isForms) {
        $note  = $Core->i18n()->translate('Forms Module nicht geladen');
        $type  = 'danger';
        $kind  = 'bs-alert';
        $title = $Core->i18n()->translate('Fehler') . '!';

        $Core->setNote($note, $type, $kind, $title);
    }

    $return = '';
    $ob_return = '';
    if($Mvc->getMVCViewFilePath() !== false) {
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
    } else {
        $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
    }

    $return .= $ob_return;

    return $return;
}

function logoutAction() {
    /** @var $Mvc Mvc */
    /** @var $accClass Acc */
    global $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    $accClass->userLogout();
}

function passforgotAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     * @var $plugins Plugins
     * @var $pitsForms PitsForms
     * @var $pitsCore PitsCore
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    /** Prepare Form Plugin */
    $plugins = $Core->Plugins();
    $pitsForms = $plugins->PitsForms();

    $pitsCore = $plugins->PitsCore();

    $return = '';

    $oldMetaTitle = $Mvc->getMetaTitle();
    $Mvc->setMetaTitle($Core->i18n()->translate('Passwort vergessen') . ' - ' . $Core->i18n()->translate('acc') . ' - ' . $oldMetaTitle);
    $oldPageTitle = $Mvc->getPageTitle();
    $Mvc->setPageTitle($Core->i18n()->translate('acc') . ' - ' . $Core->i18n()->translate('Passwort vergessen'));

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if (is_object($pitsForms)) {
            $pitsCore->decryptPOST();
        }
        $post = $Core->Request()->getPost();

        $action = (array_key_exists('action', $post)) ? $post['action'] : '';

        if($action == 'pass_forgot') {
            $userEmail = (array_key_exists('email', $post)) ? $post['email'] : '';
            $user = $accClass->getUserByEmail($userEmail);

            if(count($user)) {

                $accClass->setUserPassResetHash($user['id']);

            } else {
                $note  = $Core->i18n()->translate('E-Mail Adresse existiert im System nicht!');
                $type  = 'danger';
                $kind  = 'bs-alert';
                $title = $Core->i18n()->translate('Fehler') . '!';
                $Core->setNote($note, $type, $kind, $title);

                $redirectUrl = $Mvc->getModelUrl() . '/user/passforgot';
                $Core->Request()->redirect($redirectUrl);
            }
        }
    }

    $ob_return = '';
    if($Mvc->getMVCViewFilePath() !== false) {
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
    } else {
        $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
    }
    $return .= $ob_return;

    return $return;
}

function passresetAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     * @var $plugins Plugins
     * @var $pitsForms PitsForms
     * @var $pitsCore PitsCore
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    /** Prepare Form Plugin */
    $plugins = $Core->Plugins();
    $pitsForms = $plugins->PitsForms();

    $pitsCore = $plugins->PitsCore();

    $return = '';

    $oldMetaTitle = $Mvc->getMetaTitle();
    $Mvc->setMetaTitle($Core->i18n()->translate('Passwort zurücksetzen') . ' - ' . $Core->i18n()->translate('acc') . ' - ' . $oldMetaTitle);
    $oldPageTitle = $Mvc->getPageTitle();
    $Mvc->setPageTitle($Core->i18n()->translate('acc') . ' - ' . $Core->i18n()->translate('Passwort zurücksetzen'));

    $params  = $Mvc->getMvcParams();

    $paramPassResetHash = '';

    if(count($params)) {
        reset($params);
        $first_key = key($params);
        if($first_key == 'passResetHash') {
            $paramPassResetHash = (array_key_exists('passResetHash', $params)) ? $params['passResetHash'] : '';
        } else {
            $paramPassResetHash = $first_key;
        }
    }

    $userToView = $accClass->getUserByPassResetHash($paramPassResetHash);

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if (is_object($pitsForms)) {
            $pitsCore->decryptPOST();
        }
        $post = $Core->Request()->getPost();

        $action = (array_key_exists('action', $post)) ? $post['action'] : '';

        if($action == 'pass_reset') {
            $user = $userToView;
            if(count($user)) {
                $newPass = (array_key_exists('pass', $post)) ? $post['pass'] : '';

                if($newPass !== '') {
                    $saveData = array();
                    $saveData['pass'] = $newPass;

                    $saveResult = $accClass->saveUser($user['id'], $saveData, false, false, true);

                    if($saveResult !== false) {
                        $note = $Core->i18n()->translate('Passwort erfolgreich geändert!');
                        $type = 'success';
                        $kind = 'bs-alert';
                        $title = '';
                        $Core->setNote($note, $type, $kind, $title);

                        $accClass->unsetUserPassResetHash($paramPassResetHash);

                        $Core->Request()->redirect($Core->getBaseUrl());
                    } else {
                        $note  = $Core->i18n()->translate('Passwort konnte nicht zrückgesetzt werden!');
                        $type  = 'danger';
                        $kind  = 'bs-alert';
                        $title = $Core->i18n()->translate('Fehler') . '!';
                        $Core->setNote($note, $type, $kind, $title);

                        $redirectUrl = $Mvc->getModelUrl() . '/user/passreset/' . $paramPassResetHash;
                        $Core->Request()->redirect($redirectUrl);
                    }

                } else {
                    $note = $Core->i18n()->translate('Bitte alle Felder ausfüllen!');
                    $type = 'danger';
                    $kind = 'bs-alert';
                    $title = $Core->i18n()->translate('Fehler') . '!';
                    $Core->setNote($note, $type, $kind, $title);

                    $redirectUrl = $Mvc->getModelUrl() . '/user/passreset/' . $paramPassResetHash;
                    $Core->Request()->redirect($redirectUrl);
                }

            } else {
                $note  = $Core->i18n()->translate('Passwort konnte nicht zrückgesetzt werden!');
                $type  = 'danger';
                $kind  = 'bs-alert';
                $title = $Core->i18n()->translate('Fehler') . '!';
                $Core->setNote($note, $type, $kind, $title);

                $redirectUrl = $Mvc->getModelUrl() . '/user/passreset/' . $paramPassResetHash;
                $Core->Request()->redirect($redirectUrl);
            }
        }
    }

    $ob_return = '';
    if($Mvc->getMVCViewFilePath() !== false) {
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
    } else {
        $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
    }
    $return .= $ob_return;

    return $return;
}