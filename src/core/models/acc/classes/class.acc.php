<?php
/**
 * Access an User Control
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** Access Control Class */
class Acc
{
    private $_version = '9.0.0';
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     */
    private $Core;
    private $Mvc;
    /**
     * @vars
     * $rights = Full Rights Array
     * $groups = Full Groups Array with Rights
     */
    private $rights;
    private $rightsWhitelist;
    private $rightsDB;
    private $groups;
    private $publicModelKeys;
    private $publicControllerKeys;
    private $publicActionKeys;
    private $corePublicAreas = null;
    private $modelsPublicAreas = null;
    private $curUser = null;
    private $curUserFull = null;
    private $_searchRightStringsResult = null;
    private $_missingRights = null;

    /**
     * Acc constructor.
     * @param $Core Core
     */
    function __construct($Core)
    {
        $this->Core = $Core;
        $this->Mvc = $Core->Mvc();

        $this->_addAccHooks();

        $this->_mayLogin();

        $this->_addAccPublicAreas();
        $this->_setCorePublicAreas();
        $this->_setModelsPublicAreas();

        $this->_checkLogin();
        $this->_checkFirstLogin();

        $this->_setMvcPageHeadMeta();
        $this->_setMvcPageBeforeBodyEnds();
    }

    public function getVersion() {
        return $this->_version;
    }

    public function getDbVersion() {
        $dbVersion_option = $this->Core->Config()->get('acc_version',0);
        $dbVersion = ($dbVersion_option !== '') ? $dbVersion_option : 0;
        return $dbVersion;
    }

    /** Update Methods */
    public function _update810($execute=true) {
        /** May Add 'admin' Column */

        $return = array();
        $return['success']   = false;
        $return['version']   = '8.1.0';
        $return['changelog'] = 'Added Admin Table Column to SQL acc_groups Table';

        $user = $this->getUser();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {

            $admin_col_exits = false;

            $check_for_admin_col_SQL = 'SELECT IF(EXISTS(SELECT `id` FROM `' . DB_TABLE_PREFIX . 'acc_groups` WHERE `admin` IS NOT NULL), 1, 0) as "exists"';

            try {
                $result = $this->Core->Db()->fromDatabase($check_for_admin_col_SQL, '@simple');
                if ($result !== 0) {
                    $admin_col_exits = true;
                    $return['success'] = true;
                }
            } catch (Exception $e) {
                $result['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
            }

            if (!$admin_col_exits) {
                try {
                    $addCol_SQL = 'ALTER TABLE `' . DB_TABLE_PREFIX . 'acc_groups` ADD `admin` TINYINT( 1 ) NOT NULL DEFAULT 0 AFTER `default`';

                    $sqlResult = $this->Core->Db()->toDatabase($addCol_SQL);

                    if (!$sqlResult) {
                        // Error
                        $result['sqlResult'] = $sqlResult;
                        $return['success'] = false;
                    } else {
                        $return['success'] = true;
                    }
                } catch (Exception $e) {
                    $result['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
                }
            }
        }

        if($return['success']) {
            unset($return['error']);
        }

        return $return;
    }

    public function _update830($execute=true) {

        $return = array();
        $return['success']   = false;
        $return['version']   = '8.3.0';
        $return['changelog'] = 'Changed SQL acc_rights2groups Table for Whitelist ACC Rights functionality';

        $user = $this->getUser();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {

            $can_update_table = false;

            /** May Add 'acc_rights_code' Column */
            $acc_rights_code_col_exits = false;

            $check_for_acc_rights_code_col_SQL = 'SELECT IF(EXISTS(SELECT `id` FROM `' . DB_TABLE_PREFIX . 'acc_rights2groups` WHERE `acc_rights_code` IS NOT NULL), 1, 0) as "exists"';

            try {
                $result = $this->Core->Db()->fromDatabase($check_for_acc_rights_code_col_SQL, '@simple');
                if($result !== 0) {
                    $acc_rights_code_col_exits = true;
                    $can_update_table = true;

                    $return['success'] = true;
                }
            } catch (Exception $e) {
                $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
                $return['success'] = false;
            }

            if(!$acc_rights_code_col_exits) {
                $addCol_SQL = "ALTER TABLE `' . DB_TABLE_PREFIX . 'acc_rights2groups`  ADD `acc_rights_code` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `id_acc_rights`;";

                $sqlResult = $this->Core->Db()->toDatabase($addCol_SQL);

                if (!$sqlResult){
                    // Error
                    $return['sqlResult'] = $sqlResult;
                    $return['success'] = false;
                } else {
                    $can_update_table = true;
                    $return['success'] = true;
                }
            }

            /** Is there a 'id_acc_rights' Column */
            $id_acc_rights_col_exits = false;

            $check_for_id_acc_rights_col_SQL = 'SELECT IF(EXISTS(SELECT `id` FROM `' . DB_TABLE_PREFIX . 'acc_rights2groups` WHERE `id_acc_rights` IS NOT NULL), 1, 0) as "exists"';

            try {
                $result = $this->Core->Db()->fromDatabase($check_for_id_acc_rights_col_SQL, '@simple');
                if($result !== 0) {
                    $id_acc_rights_col_exits = true;

                    $return['success'] = true;
                }
            } catch (Exception $e) {
                $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
            }

            if ($id_acc_rights_col_exits && $can_update_table) {

                /** Update Table Contents */
                try {
                    $rights = $this->getRights();

                    /** Update Contents */
                    $sql = 'UPDATE `'.DB_CONN_DBNAME.'`.`' . DB_TABLE_PREFIX . 'acc_rights2groups` SET `acc_rights_code`=:acc_rights_code WHERE `id_acc_rights`=:id_acc_rights';
                    $params = array();

                    foreach($rights as $rightCode => $right) {

                        $rightId = $right['id'];

                        if($rightId > 0 && $rightCode !== '') {
                            $params['acc_rights_code'] = $rightCode;
                            $params['id_acc_rights'] = $rightId;

                            $sqlResult = $this->Core->Db()->toDatabase($sql, $params);

                            if (!$sqlResult){
                                // Error
                                $return['sqlResult'] = $sqlResult;
                                $return['success'] = false;
                            } else {
                                $return['success'] = true;
                            }
                        }
                    }

                    /** Remove Column*/
                    $alterTableSql = 'ALTER TABLE `' . DB_TABLE_PREFIX . 'acc_rights2groups` DROP COLUMN `id_acc_rights`';
                    $alterTableSqlResult = $this->Core->Db()->toDatabase($alterTableSql);
                    if (!$alterTableSqlResult){
                        // Error
                        $return['sqlResult'] = $sqlResult;
                        $return['success'] = false;
                    } else {
                        $return['success'] = true;
                    }
                } catch (Exception $e) {
                    $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
                    $return['success'] = false;
                }
            }
        }

        if($return['success']) {
            unset($return['error']);
        }

        return $return;
    }

    public function _update831($execute=true) {

        $return = array();
        $return['success']   = false;
        $return['version']   = '8.3.1';
        $return['changelog'] = 'Saving user info in Session Root, not to projectParameters. Added User Save Hook to refresh session user info.';

        $user = $this->getUser();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update840($execute=true) {

        $return = array();
        $return['success']   = false;
        $return['version']   = '8.4.0';
        $return['changelog'] = 'Added User Options Functionality';

        $user = $this->getUser();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update841($execute=true) {

        $return = array();
        $return['success']   = false;
        $return['version']   = '8.4.1';
        $return['changelog'] = 'Fixed Check Login Method';

        $user = $this->getUser();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update850($execute=true) {

        $return = array();
        $return['success']   = false;
        $return['version']   = '8.5.0';
        $return['changelog'] = 'Added Shell Method';

        $user = $this->getUser();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update870($execute=true) {

        $return = array();
        $return['success']   = false;
        $return['version']   = '8.7.0';
        $return['changelog'] = 'Added Additional Info Col to Acc Users Table';

        $user = $this->getUser();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $new_col_exits = false;

            $check_for_new_col_SQL = 'SELECT IF(EXISTS(SELECT `id` FROM `' . DB_TABLE_PREFIX . 'acc_users` WHERE (`additional` IS NOT NULL) OR (`additional` IS NULL) ), 1, 0) as "exists"';

            try {
                $result = $this->Core->Db()->fromDatabase($check_for_new_col_SQL, '@simple');

                if ($result !== 0) {
                    $new_col_exits = true;
                    $return['success'] = true;
                }
            } catch (Exception $e) {
                $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
            }

            if (!$new_col_exits) {
                try {
                    $addCol_SQL = 'ALTER TABLE `' . DB_TABLE_PREFIX . 'acc_users` ADD `additional` TEXT NULL DEFAULT NULL COLLATE "utf8_unicode_ci" AFTER `email`';

                    $sqlResult = $this->Core->Db()->toDatabase($addCol_SQL);

                    if (!$sqlResult) {
                        // Error
                        $return['sqlResult'] = $sqlResult;
                        $return['success'] = false;
                    } else {
                        $return['success'] = true;
                    }
                } catch (Exception $e) {
                    $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
                }
            }
        }

        if($return['success']) {
            unset($return['error']);
        }

        return $return;
    }

    public function _update875($execute=true) {

        $return = array();
        $return['success']   = false;
        $return['version']   = '8.7.5';
        $return['changelog'] = 'Added Logout possibility on firstLogin';

        $user = $this->getUser();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update880($execute=true) {

        $return = array();
        $return['success']   = false;
        $return['version']   = '8.8.0';
        $return['changelog'] = 'Enhanced Group Handling for MultiSite Functionality';

        $user = $this->getUser();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update900($execute=true) {

        $return = array();
        $return['success']   = false;
        $return['version']   = '9.0.0';
        $return['changelog'] = 'Force NoFileCache on Login / Logout';

        $user = $this->getUser();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    private function _addAccHooks() {
        $this->Core->Hooks()->add('user_may_logout', function($userId, $hookAction) { $this->_userMayLogout($userId, $hookAction); });
        $this->Core->Hooks()->add('user_pass_changed', function($user) { $this->_sendPassChangedMail($user); });
        $this->Core->Hooks()->add('user_saved', function($userId) {
            $thisUserId = $this->getCurrUserId();
            $user = ($this->userLoggedIn() && is_array($this->getUser())) ? $this->getUser() : array();
            if(($thisUserId == $userId)) {
                $_SESSION['user'] = $user;
            }
        });
    }

    /** Mvc Output Methods */
    private function _setMvcPageHeadMeta()
    {
        $getPageHeadMeta = $this->Mvc->getPageHeadMeta();

        if(strpos($getPageHeadMeta, 'Acc Head Meta') === false) {
            $curUser = $this->getUser();
            $curUserId = (count($curUser) && array_key_exists('id', $curUser)) ? $curUser['id'] : 0;

            if ($curUserId != 0) {
                $mvcPageHeadMeta = PHP_EOL . '<!-- Acc Head Meta START -->';

                $mvcPageHeadMeta .= PHP_EOL;
                $mvcPageHeadMeta .= '<script type="text/javascript">';
                $mvcPageHeadMeta .= PHP_EOL;
                $mvcPageHeadMeta .= '    var thisCurrUserId = ' . $curUserId . ';';

                if ($this->hasAccess('acc_user')) {
                    $mvcPageHeadMeta .= PHP_EOL;
                    $mvcPageHeadMeta .= '    var doUos = 1;';
                    $mvcPageHeadMeta .= PHP_EOL;
                    $mvcPageHeadMeta .= '    var doAjaxUos = 0;';
                }

                $mvcPageHeadMeta .= PHP_EOL;
                $mvcPageHeadMeta .= '    var accModelAjaxUrl = \'' . $this->Mvc->getModelAjaxUrl('acc') . '\';';

                $mvcPageHeadMeta .= PHP_EOL;
                $mvcPageHeadMeta .= '</script>';

                $mvcPageHeadMeta .= PHP_EOL . '<!-- Acc Head Meta END -->';
                $mvcPageHeadMeta .= PHP_EOL;

                $this->Core->Mvc()->addPageHeadMeta($mvcPageHeadMeta);
            }
        }
    }

    private function _setMvcPageBeforeBodyEnds()
    {
        $getPageBeforeBodyEnd = $this->Mvc->getPageBeforeBodyEnd();

        if(strpos($getPageBeforeBodyEnd, 'Acc Before Body Ends') === false) {
            $incUrl = $this->Core->getCoreUrl() . '/models/acc/inc';

            $mvcPageBeforeBodyEnd = PHP_EOL . '<!-- Acc Before Body Ends START -->';

            $mvcPageBeforeBodyEnd .= '<script src="' . $incUrl . '/acc.js"></script>';

            $mvcPageBeforeBodyEnd .= PHP_EOL . '<!-- Acc Before Body Ends END -->';
            $mvcPageBeforeBodyEnd .= PHP_EOL;

            $this->Core->Mvc()->addPageBeforeBodyEnd($mvcPageBeforeBodyEnd);
        }
    }

    /** Public Areas Methods */
    private function _addAccPublicAreas() {
        $this->addPublicModelKey('acc');
        $this->addPublicControllerKey('user');
        $this->addPublicActionKey('login');
        $this->addPublicActionKey('logout');
        $this->addPublicActionKey('passforgot');
        $this->addPublicActionKey('passreset');
        $this->addPublicActionKey('getOptions');
    }

    private function _setCorePublicAreas() {
        $corePublicAreas = $this->corePublicAreas;
        if($corePublicAreas === null) {
            foreach ($this->Core->getPublicModelKeys() as $publicModelKey) {
                $corePublicAreas['modelKeys'][] = $publicModelKey;
                $this->addPublicModelKey($publicModelKey);
            }
            foreach ($this->Core->getPublicControllerKeys() as $publicControllerKey) {
                $corePublicAreas['controllerKeys'][] = $publicControllerKey;
                $this->addPublicControllerKey($publicControllerKey);
            }
            foreach ($this->Core->getPublicActionKeys() as $publicActionKey) {
                $corePublicAreas['actionKeys'][] = $publicActionKey;
                $this->addPublicActionKey($publicActionKey);
            }
            $this->corePublicAreas = $corePublicAreas;
        }
    }

    private function _setModelsPublicAreas() {
        $modelsPublicAreas = $this->modelsPublicAreas;
        if($modelsPublicAreas === null) {
            $modelsPublicAreas = array();
            $models = $this->Mvc->getModels();

            foreach($models as $modelKey => $model) {
                if($modelKey == 'acc') { continue; }
                $modelClass = $this->Mvc->modelClass($modelKey);

                if(is_object($modelClass)) {
                    if(method_exists($modelClass,'getPublicControllerKeys')) {
                        $modelsPublicAreas['modelKeys'][] = $modelKey;
                        $this->addPublicModelKey($modelKey);
                        foreach ($modelClass->getPublicControllerKeys() as $publicControllerKey) {
                            $modelsPublicAreas['controllerKeys'][] = $publicControllerKey;
                            $this->addPublicControllerKey($publicControllerKey);
                        }
                        if(method_exists($modelClass,'getPublicActionKeys')) {
                            foreach ($modelClass->getPublicActionKeys() as $publicActionKey) {
                                $modelsPublicAreas['actionKeys'][] = $publicActionKey;
                                $this->addPublicActionKey($publicActionKey);
                            }
                        }
                    }
                }
            }

            $this->modelsPublicAreas = $modelsPublicAreas;
        }
    }

    public function addPublicModelKey($modelKey) {
        $publicModelKeys = $this->publicModelKeys;

        if(!is_array($publicModelKeys)) {
            $publicModelKeys = array();
        }

        $publicModelKeys[] = $modelKey;

        $this->publicModelKeys = $publicModelKeys;
    }

    public function addPublicControllerKey($controllerKey) {
        $publicControllerKeys = $this->publicControllerKeys;

        if(!is_array($publicControllerKeys)) {
            $publicControllerKeys = array();
        }

        $publicControllerKeys[] = $controllerKey;

        $this->publicControllerKeys = $publicControllerKeys;
    }

    public function addPublicActionKey($actionKey) {
        $publicActionKeys = $this->publicActionKeys;

        if(!is_array($publicActionKeys)) {
            $publicActionKeys = array();
        }

        $publicActionKeys[] = $actionKey;

        $this->publicActionKeys = $publicActionKeys;
    }

    /** User Methods **/
    private function _prepareUserData($data) {
        $user = array();
        if(is_array($data) && count($data)) {

            $id           = (array_key_exists('id',$data)) ? $data['id'] : false;

            if($id !== false) {

                $name          = (array_key_exists('name',$data)) ? $data['name'] : '';
                $surname       = (array_key_exists('surname',$data)) ? $data['surname'] : '';
                $gender        = (array_key_exists('gender',$data)) ? $data['gender'] : '';
                $email         = (array_key_exists('email',$data)) ? $data['email'] : '';
                $additional    = (array_key_exists('additional',$data)) ? $data['additional'] : '';
                $su            = (array_key_exists('su',$data)) ? $data['su'] : 0;
                $active        = (array_key_exists('active',$data)) ? $data['active'] : 0;
                $id_acc_groups = (array_key_exists('id_acc_groups',$data)) ? $data['id_acc_groups'] : NULL;
                $sites         = (array_key_exists('sites',$data)) ? array($data['sites']) : array();
                $firstLogin    = (array_key_exists('alreadyLoggedIn',$data) && $data['alreadyLoggedIn'] === 1) ? false : true;
                $online        = (array_key_exists('online',$data)) ? $data['online'] : '';
                $created       = (array_key_exists('created',$data)) ? $data['created'] : '';
                $last_login    = (array_key_exists('last_login',$data)) ? $data['last_login'] : '';
                $default_lang  = (array_key_exists('default_lang',$data)) ? $data['default_lang'] : '';

                $user = array(
                    'id'            => $id,
                    'name'          => $name,
                    'surname'       => $surname,
                    'gender'        => $gender,
                    'email'         => $email,
                    'additional'    => $additional,
                    'su'            => $su,
                    'active'        => $active,
                    'id_acc_groups' => $id_acc_groups,
                    'sites'         => $sites,
                    'firstLogin'    => $firstLogin,
                    'online'        => $online,
                    'created'       => $created,
                    'last_login'    => $last_login,
                    'default_lang'  => $default_lang
                );

                if (array_key_exists('sites',$data) && strpos($data['sites'], ',') !== false) {
                    $user['sites'] = explode(',', $data['sites']);
                }

                if($user['su']) {
                    $user['sites'] = array();
                    $sites = $this->Core->Sites()->getSites();
                    foreach($sites as $siteId => $site) {
                        $user['sites'][] = $siteId;
                    }
                    
                    $vag_grouId = (array_key_exists('system_debug_vag', $_SESSION) && is_numeric($_SESSION['system_debug_vag'])) ? $_SESSION['system_debug_vag'] : null;
                    
                    if($vag_grouId !== null) {
                    	$user['id_acc_groups'] = $vag_grouId;
                    }
                }

                if(is_array($user['sites']) && count($user['sites'])) {
                    sort($user['sites']);
                }

                if (array_key_exists('changed',$data) && $data['changed'] != '0000-00-00 00:00:00') {
                    $user['changed'] = $data['changed'];
                    $user['changedByUserId'] = (array_key_exists('id_acc_users__changedBy',$data)) ? $data['id_acc_users__changedBy'] : 0;
                }
            }
        }

        return $user;
    }

    public function getUsers($start = 0, $limit = null, $fullInfo = false) {
        $users = array();

        $limitSql = ($limit !== null) ? ' LIMIT ' . intval($start) . ',' . intval($limit) : '';

        $sql = 'SELECT users.*, ( SELECT GROUP_CONCAT(id_system_sites) FROM `' . DB_TABLE_PREFIX . 'system_users2sites` WHERE id_acc_users = users.id ) AS sites FROM `' . DB_TABLE_PREFIX . 'acc_users` AS users' . $limitSql . ';';
        $result = $this->Core->Db()->fromDatabase($sql,'@raw');

        foreach($result as $row) {
            $users[$row['id']] = $this->_prepareUserData($row);

            if($fullInfo) {
                $users[$row['id']]['salt'] = $row['salt'];
                $users[$row['id']]['pass_crypted'] = $row['pass'];
            }
        }
        return $users;
    }

    public function userExists($id)
    {
        $sql = 'SELECT id FROM `' . DB_TABLE_PREFIX . 'acc_users` WHERE `id` = :userid;';
        $params = array(
            'userid' => (int)$id
        );
        $result = $this->Core->Db()->fromDatabase($sql,'@raw', $params);
        return (count($result)) ? true : false;
    }

    public function getUser($id = null, $fullInfo = false)
    {
        $user = array();

        $sessUserId = $this->getCurrUserId();

        $userId = ($id != null && $this->userExists($id)) ? $id: $sessUserId;

        if($this->Core->Helper()->isCli()) {

        }

        if($userId !== 0) {

            /** Check if Curr User and return Cached Data if set */
            if($userId == $sessUserId) {
                $curUser     = $this->curUser;
                $curUserFull = $this->curUserFull;

                if($fullInfo && $curUserFull !== null) {
                    return $curUserFull;
                }
                if (!$fullInfo && $curUser !== null) {
                    return $curUser;
                }
            }

            /** Get User from DB */
            $sql = 'SELECT user.*, ( SELECT GROUP_CONCAT(id_system_sites) FROM `' . DB_TABLE_PREFIX . 'system_users2sites` WHERE id_acc_users = user.id ) AS sites FROM `' . DB_TABLE_PREFIX . 'acc_users` AS user WHERE `id` = :userid;';
            $params = array(
                'userid' => $userId
            );
            $result = $this->Core->Db()->fromDatabase($sql,'@raw', $params);
            foreach($result as $row) {
                $user = $this->_prepareUserData($row);

                if($fullInfo) {
                    $user['salt'] = $row['salt'];
                    $user['pass_crypted'] = $row['pass'];
                }
            }
        }

        /** Check if Curr User and set Cache Data if not set */
        if($userId == $sessUserId) {
            $curUser     = $this->curUser;
            $curUserFull = $this->curUserFull;

            if($fullInfo && $curUserFull === null) {
                $this->curUserFull = $user;
            }
            if(!$fullInfo && $curUser === null) {
                $this->curUser = $user;
            }
        }

        return $user;
    }

    public function getUserByEmail($email, $fullInfo = false) {
        $user = array();

        if($this->Core->Helper()->check_email($email)) {
            $sql = 'SELECT user.*, ( SELECT GROUP_CONCAT(id_system_sites) FROM `' . DB_TABLE_PREFIX . 'system_users2sites` WHERE `id_acc_users` = `user`.`id` ) AS sites FROM `' . DB_TABLE_PREFIX . 'acc_users` AS user WHERE `user`.`email`=:email;';
            $params = array(
                'email' => strtolower($email)
            );

            $result = $this->Core->Db()->fromDatabase($sql,'@raw', $params);

            foreach($result as $row) {
                $user = $this->_prepareUserData($row);

                if($fullInfo) {
                    $user['salt'] = $row['salt'];
                    $user['pass_crypted'] = $row['pass'];
                }
            }
        }

        return $user;
    }

    public function getUserByPassResetHash($passResetHash) {
        $user = array();
        $getPassResetHash = $passResetHash;

        $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'acc_passreset2user` WHERE `pass_reset_hash`=:pass_reset_hash;';
        $params = array(
            'pass_reset_hash' => $getPassResetHash
        );
        $result = $this->Core->Db()->fromDatabase($sql,'@raw', $params);

        $userId = 0;
        $passResetHash = '';
        $passResetHashCreated = '';
        foreach($result as $row) {
            $userId               = $row['id_acc_users'];
            $passResetHash        = html_entity_decode($row['pass_reset_hash']);
            $passResetHashCreated = $row['created'];
        }

        if($userId !== 0 && $passResetHash !== '' && $passResetHashCreated !== ''){
            $user = $this->getUser($userId);
            if(count($user)) {
                $hashLifetime = 86400;
                /** 24 h in Seconds */
                $curTime = time();
                $hashCreatedTime = strtotime($passResetHashCreated);

                $hashAgeValid = (($curTime - $hashCreatedTime) < $hashLifetime);

                if(!$hashAgeValid) {
                    $user = array();
                    $this->unsetUserPassResetHash($passResetHash);
                }
            }
        }

        return $user;
    }

    public function setUserPassResetHash($userId) {
        $user = $this->getUser($userId, true);

        $return = false;
        if(count($user)){
            $userSalt = $user['salt'];
            if (!session_id()) {
                die('error: no session! (setUserPassResetHash)');
            }
            $sessId = session_id();

            $passwordHash = str_replace(array('/','.'), '_', $this->getPasswordHash($sessId,$userSalt));

            if($passwordHash != '') {

                $passHashUser = $this->getUserByPassResetHash($passwordHash);

                if(!count($passHashUser)) {

                    $sql = '
                        INSERT INTO
                        `' . DB_TABLE_PREFIX . 'acc_passreset2user`
                        (`id_acc_users`, `pass_reset_hash`)
                        VALUES
                        (:id_acc_users, :pass_reset_hash)
                    ';

                    $params = array(
                        'id_acc_users' => $userId,
                        'pass_reset_hash' => $passwordHash
                    );

                    $sqlResult = $this->Core->Db()->toDatabase($sql, $params);

                    if (!$sqlResult) {
                        $noteMsg = $this->Core->i18n()->translate('Passwort Reset Hash konnte nicht erstellt werden!');

                        $note = $noteMsg;
                        $type = 'danger';
                        $kind = 'bs-alert';
                        $title = $this->Core->i18n()->translate('Fehler') . '!';

                        $this->Core->setNote($note, $type, $kind, $title);

                    } else {
                        $sendResult = $this->_sendResetPassLink($user, $passwordHash);
                        if ($sendResult !== false) {
                            $note = sprintf($this->Core->i18n()->translate('Eine E-Mail mit weiteren Informationen wurde soeben an \'%s\' gesendet.'), $user['email']);
                            $type = 'success';
                            $kind = 'bs-alert';
                            $title = $this->Core->i18n()->translate('Erledigt') . '!';

                            $this->Core->setNote($note, $type, $kind, $title);

                            $return = true;

                            $redirectUrl = $this->Mvc->getModelUrl() . '/user/passforgot';
                            $this->Core->Request()->redirect($redirectUrl);
                        } else {

                            $note = $this->Core->i18n()->translate('E-Mail konnte nicht versendet werden.');
                            $type = 'danger';
                            $kind = 'bs-alert';
                            $title = $this->Core->i18n()->translate('Fehler') . '!';

                            $this->Core->setNote($note, $type, $kind, $title);
                        }
                    }
                } else {
                    $note = sprintf($this->Core->i18n()->translate('Eine E-Mail mit weiteren Informationen wurde bereits an \'%s\' gesendet.'), $user['email']);
                    $type = 'warning';
                    $kind = 'bs-alert';
                    $title = '';

                    $this->Core->setNote($note, $type, $kind, $title);

                    $return = false;

                    $redirectUrl = $this->Mvc->getModelUrl() . '/user/passforgot';
                    $this->Core->Request()->redirect($redirectUrl);
                }
            }
        }
        return $return;
    }

    public function unsetUserPassResetHash($passResetHash) {
        $return = false;
        if($passResetHash !== '') {
            $sql = 'DELETE FROM `' . DB_TABLE_PREFIX . 'acc_passreset2user` WHERE `pass_reset_hash` = :pass_reset_hash';
            $params = array(
                'pass_reset_hash' => $passResetHash
            );

            $sqlResult = $this->Core->Db()->toDatabase($sql, $params, false,true);

            if (!$sqlResult) {
                $return = false;
            } else {
                $return = true;
            }
        }
        return $return;
    }

    public function getCurrUserId() {
        return (isset($_SESSION) && array_key_exists('userid', $_SESSION)) ? $_SESSION['userid'] : 0;
    }

    public function getUserProfileUrl($id = null)
    {
        $user = $this->getUser($id);
        $userId = (count($user) && array_key_exists('id', $user)) ?  $user['id'] : 0;

        return $this->Mvc->getModelUrl('acc') . '/user/profile/' . $userId;
    }

    public function getAjaxUserProfileUrl($id = null)
    {
        $user = $this->getUser($id);
        $userId = (count($user) && array_key_exists('id', $user)) ?  $user['id'] : 0;

        $modelAjaxUrl = $this->Mvc->getModelAjaxUrl('acc');

        return $modelAjaxUrl . '/user/profile/' . $userId;
    }

    public function userLoggedIn($id = null)
    {
        $isLoggedIn = false;

        $sess_userId = $this->getCurrUserId();
        $userId  = ($id !== null && $this->userExists($id)) ? $id : $sess_userId;

        if($userId !== 0) {
            $uos = $this->getUserOnlineStates($userId);
            if($uos['state_int'] >= 1) {
                $isLoggedIn = true;
            } else {
                $isLoggedIn = false;
            }
        }

        return $isLoggedIn;
    }

    public function getUserMenu() {
        $userMenu = '';
        $user = $this->getUser();
        if(count($user)) {
            $userMenu = '<li class="dropdown user-menu">';
            $userMenu .= '<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
            $userMenu .= '<i class="fa fa-user-circle" aria-hidden="true"></i>&nbsp;';
            $userMenu .= $user['name'] . ' ' . $user['surname'];
            $userMenu .= '<span class="caret"></span>';
            $userMenu .= '</a>';

            $userMenu .= '<ul class="dropdown-menu">';

            /** Ajax Profile Link */
            $userMenu .= '<li>';
            $userMenu .= '<a href="' . $this->getAjaxUserProfileUrl($user['id']) . '" data-profilelink="ajax">';
            $userMenu .= '<i class="fa fa-user" aria-hidden="true"></i>&nbsp;';
            $userMenu .= $this->Core->i18n()->translate('Profil');
            $userMenu .= '</a>';
            $userMenu .= '</li>';

            /** Profile Edit Link */
            $userMenu .= '<li>';
            $userMenu .= '<a href="' . $this->Mvc->getModelUrl('acc') . '/user/edit">';
            $userMenu .= '<i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;';
            $userMenu .= $this->Core->i18n()->translate('bearbeiten');
            $userMenu .= '</a>';
            $userMenu .= '</li>';

            /** Logout Link */
            $userMenu .= '<li>';
            $userMenu .= '<a href="' . $this->Mvc->getModelUrl('acc') . '/user/logout">';
            $userMenu .= '<i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;';
            $userMenu .= $this->Core->i18n()->translate('ausloggen');
            $userMenu .= '</a>';
            $userMenu .= '</li>';

            $userMenu .= '</ul>';
            $userMenu .= '</li>';
        } else {
            $mvcInfo = $this->Core->Mvc()->getMvcInfo();
            $activeClass = ($mvcInfo['model'] == 'acc' && $mvcInfo['controller'] == 'user' && $mvcInfo['action'] == 'login') ? ' class="active"' : '';
            $userMenu = '<li' . $activeClass . '>';
            $userMenu .= '<a href="' . $this->Mvc->getModelUrl('acc') . '/user/login'  . '">';
            $userMenu .= '<i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;';
            $userMenu .= $this->Core->i18n()->translate('Login');
            $userMenu .= '</a>';
            $userMenu .= '</li>';
        }

        return $userMenu;
    }

    /** User Action Methods */
    private function _checkLogin()
    {
        /**
         * @var $requestClass Request
         * @var $i18nClass i18n
         * @var $Mvc Mvc
         */
        $requestClass = $this->Core->Request();
        $i18nClass = $this->Core->i18n();
        $Mvc = $this->Mvc;

        $modelKey        = $Mvc->getModelKey();
        $modelController = $Mvc->getControllerKey();
        $modelAction     = $Mvc->getActionKey();

        if(
            $modelKey == 'acc' &&
            $modelController == 'user' &&
            ($modelAction == 'login' || $modelAction == 'logout')
        ) {
            $this->Core->Mvc()->setForceNoFileCache();
        }

        $sess_userId = $this->getCurrUserId();
        if($sess_userId === 0) {
            $path            = $requestClass->getParams();

            $MvcPublicModelKeys      = $this->publicModelKeys;
            $MvcPublicControllerKeys = $this->publicControllerKeys;
            $MvcPublicActionKeys     = $this->publicActionKeys;

            $forceLogin = true;
            $forceLoginWithMessage = true;
            $forceLoginWithRedirect = true;
            if (($path == '' || $path == '/')) {
                $forceLogin = true;
                $forceLoginWithMessage = false;
            }

            if (
                $path != '' &&
                (
                    in_array($modelKey, $MvcPublicModelKeys) &&
                    in_array($modelController, $MvcPublicControllerKeys) &&
                    in_array($modelAction, $MvcPublicActionKeys)
                )
            ) {
                $forceLogin = false;
                $forceLoginWithMessage = false;
                $forceLoginWithRedirect = false;
            }

            if($this->Core->Helper()->isCli() ||  strpos($_SERVER['REQUEST_URI'],'cron.php') !== false) {
                $forceLogin = false;
                $forceLoginWithMessage = false;
                $forceLoginWithRedirect = false;
            }

            if ($forceLogin) {
                $this->_redirectToLogin($forceLoginWithMessage, $forceLoginWithRedirect);
            }
        } else {
            if (!$this->userLoggedIn()) {
                $this->userLogout($sess_userId, true, true, true);
            } else {
                $user = $this->getUser();

                if (count($user) && !$user['su'] && $user['active'] === 0) {
                    $note = $i18nClass->translate('Sie sind noch nicht freigeschalten.');
                    $type = 'warning';
                    $kind = 'bs-alert';
                    $this->Core->setNote($note, $type, $kind);

                    $this->_redirectToLogin(false, true);
                }

                if ($requestClass->getUrlKey() != 'ajax') {
                    $defaultLang = $i18nClass->getDefaultLang();
                    $userDefaultLang = (array_key_exists('default_lang',$user)) ? $user['default_lang'] : $defaultLang;
                    if ($defaultLang != $userDefaultLang) {
                        $i18nClass->setDefaultLang($userDefaultLang);
                    }
                    $this->setOnlineActivity();
                }
            }
        }
    }

    private function _checkFirstLogin()
    {
        $user = $this->getUser();
        if(is_array($user) && count($user) && array_key_exists('firstLogin', $user) && $user['firstLogin']) {
            if(
                $this->Mvc->getActionKey() !== 'firstLogin' &&
                $this->Mvc->getActionKey() !== 'save' &&
                $this->Mvc->getActionKey() !== 'logout' &&
                !$this->Core->Request()->isAjax()
            ) {
                $redirectUrl = $this->Mvc->getModelUrl('acc') . '/user/firstLogin';
                $this->Core->Request()->redirect($redirectUrl);
            }
        }
    }

    private function _mayLogin()
    {
        /**
         * @var $Core Core
         * @var $Helper Helper
         */
        $Core = $this->Core;
        $Helper = $Core->Helper();
        if (
            !$Helper->isCli() &&
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            array_key_exists('action', $_POST) &&
            $_POST['action'] == 'login'
        ) {
            $this->userLogin();
        }
    }

    public function userLogin($data=null)
    {
        /** Run only if not logged in */
        if($this->userLoggedIn()) { return array(); }

        if($data === null) {
            $mail = (isset($_POST['mail']) && trim($_POST['mail']) != '') ? trim($_POST['mail']) : '';
            $pass = (isset($_POST['pass']) && trim($_POST['pass']) != '') ? trim($_POST['pass']) : '';
            $notify = true;
            $redirect = true;
        } else {
            $data = (is_array($data)) ? $data : array();

            $mail = (array_key_exists('mail',$data) && trim($data['mail']) != '') ? trim($data['mail']) : '';
            $pass = (array_key_exists('pass',$data) && trim($data['pass']) != '') ? trim($data['pass']) : '';
            $notify = (array_key_exists('notify',$data) && is_bool($data['notify'])) ? $data['notify'] : false;
            $redirect = (array_key_exists('redirect',$data) && is_bool($data['redirect'])) ? $data['redirect'] : false;;
        }

        unset($_POST['action']);

        $mail = strip_tags($mail);

        $data = array(
            'mail' => $mail,
            'pass' => $pass
        );
        $checkLoginCredentials = $this->userCheckLoginCredentials($data);

        $checkLoginCredentials_success = (is_array($checkLoginCredentials) && array_key_exists('success',$checkLoginCredentials) && $checkLoginCredentials['success'] === true);

        if($checkLoginCredentials_success) {
            $message = (is_array($checkLoginCredentials) && array_key_exists('message',$checkLoginCredentials)) ? $checkLoginCredentials['message'] : $this->Core->i18n()->translate('Login erfolgreich!');
            $userId = (is_array($checkLoginCredentials) && array_key_exists('userId',$checkLoginCredentials)) ? $checkLoginCredentials['userId'] : 0;

            $_SESSION['userid'] = $userId;

            $user = $this->getUser($userId);

            $_SESSION['user'] = $user;

            /** Set Last Login */
            $this->setOnlineActivity($userId, true);

            if($notify) {
                $noteLogin      = $message;
                $noteLoginType  = 'success';
                $noteLoginKind  = 'bs-alert';
                $noteLoginTitle = '';
                $this->Core->setNote($noteLogin, $noteLoginType, $noteLoginKind, $noteLoginTitle);
            }

            $getLangKeyFromPath = $this->Core->i18n()->getLangKeyFromPath();
            $curLangUrlKey = ($getLangKeyFromPath != '') ? $getLangKeyFromPath . '/' : '';
            $redirectUrl = $this->Core->Sites()->getSiteBaseUrl() . $curLangUrlKey;
            if(array_key_exists('firstLogin', $user) && $user['firstLogin']) {
                $redirectUrl = $this->Mvc->getModelUrl() . '/user/firstLogin';
            }

            $this->Core->Hooks()->fire('acc_user_logged_in');

            if($redirect) {
                $this->Core->Request()->redirect($redirectUrl);
            }
        } else {
            $message = (is_array($checkLoginCredentials) && array_key_exists('message',$checkLoginCredentials)) ? $checkLoginCredentials['message'] : $this->Core->i18n()->translate('Login nicht möglich!');

            if($notify) {
                $note = $message;
                $type = 'danger';
                $kind = 'bs-alert';
                $title = 'Fehler';
                $this->Core->setNote($note, $type, $kind, $title);
            }
        }

        return $checkLoginCredentials;
    }

    public function userCheckLoginCredentials($data) {
        $return = array();
        if(is_array($data) && count($data)){
            $mail = $data['mail'];
            $pass = $data['pass'];

            $mail = strip_tags($mail);

            if($mail != '' && $pass != '') {
                if($this->Core->Helper()->check_email($mail) == false) {
                    $return['success'] = false;
                    $return['message'] = $this->Core->i18n()->translate('Bitte eine gültige E-Mail Adresse eingeben!');
                } else {
                    $user = $this->getUserByEmail($mail, true);

                    if(count($user)) {
                        if (
                            // $this->checkMasterKey($pass) ||
                            $this->getPasswordHash($pass, $user['salt']) == $user['pass_crypted']
                        ) {
                            $userId = $user['id'];
                            if($user['active'] == 1 || $user['su']) { /* SU has all powers \o/ !! */
                                $return['success'] = true;
                                $return['message'] = $this->Core->i18n()->translate('Login erfolgreich!');
                                $return['userId']  = $userId;
                            } else {
                                $return['success'] = false;
                                $return['message'] = $this->Core->i18n()->translate('Anmeldung konnte nicht durchgeführt werden, da der User inaktiv oder noch nicht freigeschalten wurde.');
                            }
                        } else {
                            $return['success'] = false;
                            $return['message'] = $this->Core->i18n()->translate('Anmeldung konnte nicht durchgeführt werden, da eine falsche E-Mail-Adresse oder ein falsches Passwort angegeben wurde! Bitte erneut versuchen.');
                        }
                    } else {
                        $return['success'] = false;
                        $return['message'] = $this->Core->i18n()->translate('Anmeldung konnte nicht durchgeführt werden, da mit diesen Zugangsdaten kein Benutzer gefunden wurde.');
                    }
                }
            } else {
                $return['success'] = false;
                $return['message'] = $this->Core->i18n()->translate('Bitte alle Felder ausfüllen!');
            }
        } else {
            $return['success'] = false;
            $return['message'] = $this->Core->i18n()->translate('Login nicht möglich!');
        }

        return $return;
    }

    private function _userMayLogout($userId = null, $hookAction) {
        $curUser = $this->getUser();

        $logoutActions = array(
            'acc_delete_group'         => $this->Core->i18n()->translate('Benutzergruppe wurde gelöscht'),
            'acc_change_group'         => $this->Core->i18n()->translate('Benutzergruppe wurde geändert'),
            'acc_user_deactivate'      => $this->Core->i18n()->translate('Benutzer wurde deaktiviert'),
            'acc_user_change_email'    => $this->Core->i18n()->translate('Benutzer E-Mail wurde geändert'),
            'acc_user_change_password' => $this->Core->i18n()->translate('Benutzerpasswort wurde geändert'),
            'acc_user_change_site'     => $this->Core->i18n()->translate('Benutzer-Seitenzuordnung wurde geändert'),
            'acc_user_change_group'    => $this->Core->i18n()->translate('Benutzer-Gruppenzuordnung wurde geändert'),
        );

        if (($userId !== null && $this->userExists($userId) && $curUser['id'] !== $userId) && array_key_exists($hookAction, $logoutActions)) {
            $isOnline = $this->userLoggedIn($userId);

            if($isOnline){
                $user = $this->getUser($userId);
                /** Reset User Online Datetime */
                $data = array();
                $data['online'] = '0000-00-00 00:00:00';
                $this->saveUser($userId, $data, false, false, true);

                $note = sprintf($this->Core->i18n()->translate('Benutzer \'%s\' wurde ausgeloggt!'), $user['name'] . ' ' . $user['surname']);
                $note .= '<br /><br /><strong>';
                $note .= $this->Core->i18n()->translate('Grund');
                $note .= '</strong>:<br />';
                $note .= $logoutActions[$hookAction];

                $type = 'warning';
                $kind = 'bs-alert';
                $title = $this->Core->i18n()->translate('Zur Information') . ':';

                $this->Core->setNote($note, $type, $kind, $title);
            }
        }
    }

    public function userLogout($id = null, $notify = true, $redirect = true, $logoutBySystem = false)
    {
        if($this->userLoggedIn($id) || $logoutBySystem) {
            /** Reset User Online Datetime */
            $user = $this->getUser($id);
            $data = array();
            $data['online'] = '0000-00-00 00:00:00';
            $this->saveUser($user['id'],$data,false,false, true);

            if($id === null || ($id == $this->getCurrUserId())) {
                /** Curr User Logout */

                /** @var $Helper Helper */
                $Helper = $this->Core->Helper();

                if($Helper->isCli()) {
                    session_destroy();
                    return true;
                } else {
                    session_destroy();
                    session_start();
                }
            }

            $this->Core->Hooks()->fire('acc_user_logged_out', $id);

            if(!$logoutBySystem) {
                $note = $this->Core->i18n()->translate('Logout erfolgreich!');
                $type = 'success';
                $kind = 'bs-alert';
                $title = '';
            } else {
                $note = $this->Core->i18n()->translate('Sie wurden durch das System ausgeloggt!');
                $type = 'warning';
                $kind = 'bs-alert';
                $title = '';
                $redirect = true;
            }
        } else {
            $note  = $this->Core->i18n()->translate('Nicht eingeloggt!');
            $type  = 'danger';
            $kind  = 'bs-alert';
            $title = $this->Core->i18n()->translate('Fehler') . '!';
        }

        if($notify) {
            $this->Core->setNote($note, $type, $kind, $title);
        }

        if($redirect) {
            $currLangUrlKey = $this->Core->i18n()->getCurrLangUrlKey();
            $langUrlSlug = ($currLangUrlKey !== '') ? $currLangUrlKey . '/' : '';

            $currUrl = $this->Core->Request()->getCurrUrl();
            $baseUrl = $this->Core->getBaseUrl() . $langUrlSlug;

            if($currUrl != $baseUrl) {
                $this->Core->Request()->redirect($baseUrl);
            }
        }
    }

    private function _redirectToLogin($withMessage = true, $withRedirect = true) {
        if ($withMessage) {
            $note = $this->Core->i18n()->translate('Sie müssen eingeloggt sein um diesen Bereich zu sehen!');
            $type = 'danger';
            $kind = 'bs-alert';
            $title = $this->Core->i18n()->translate('Fehler') . '!';
            $this->Core->setNote($note, $type, $kind, $title);
        }

        if ($withRedirect) {
            $modelKey        = $this->Mvc->getModelKey();
            $modelController = $this->Mvc->getControllerKey();
            $modelAction     = $this->Mvc->getActionKey();

            if (
                $modelKey != 'acc' &&
                $modelController != 'user' &&
                ($modelAction != 'login' || $modelAction != 'logout')
            ) {
                $redirectUrl = $this->Mvc->getModelUrl('acc') . '/user/login';
                $this->Core->Request()->redirect($redirectUrl);
            }
        }
    }

    public function saveUser($id, $data, $notify = true, $redirect = true, $systemAction = false)
    {
        $success = false;

        if(!array_key_exists('su', $data) && !$systemAction) {
            $userActive = (!array_key_exists('active',$data)) ? 0 : $data['active'];
            $data['active'] = $userActive;
        }

        if($this->userExists($id)) {

            $this->Core->Hooks()->fire('acc_user_save', $id);

            $saveUser = $this->getUser($id, true);

            $currUser = ($id == $this->getCurrUserId()) ? $saveUser : $this->getUser(null, true);

            $saveUserSites = array();
            foreach($saveUser['sites'] as $siteId) {
                if(!is_numeric($siteId) || $siteId === 0) {
                    continue;
                }
                $saveUserSites[] = $siteId;
            }
            $saveUser['sites'] = $saveUserSites;

            $userSaveData = $saveUser;

            $salt = $saveUser['salt'];

            $dataPass = (array_key_exists('pass', $data) && $data['pass'] != '') ? $data['pass'] : '';
            $dataPass_crypted = ($dataPass != '') ? $this->getPasswordHash($dataPass, $salt) : '';
            $saveUserPass_crypted = $saveUser['pass_crypted'];

            $pass_crypted = ($dataPass_crypted != '') ? $dataPass_crypted : $saveUserPass_crypted;

            if($pass_crypted == '') {
                $errMsg  = $this->Core->i18n()->translate('Passwort darf beim Benutzer speichern nicht leer sein!');

                $note  = $errMsg;
                $type  = 'danger';
                $kind  = 'bs-alert';
                $title = $this->Core->i18n()->translate('Fehler') . '!';

                $this->Core->setNote($note, $type, $kind, $title);
                return false;
            }

            if($dataPass_crypted == $saveUserPass_crypted) {
                $errMsg  = $this->Core->i18n()->translate('Passwort darf beim Benutzer speichern nicht identisch mit dem bereits vorhandenen sein!');

                $note  = $errMsg;
                $type  = 'danger';
                $kind  = 'bs-alert';
                $title = $this->Core->i18n()->translate('Fehler') . '!';

                $this->Core->setNote($note, $type, $kind, $title);
                return false;
            }

            $id_system_site = (array_key_exists('id_system_sites', $data) && is_array($data['id_system_sites'])) ? $data['id_system_sites'] : $saveUserSites;

            $htmlFreeFields = array(
                'name',
                'surname'
            );
            foreach($data as $saveDataKey => $saveDataValue) {
                $saveDataValue = (in_array($saveDataKey,$htmlFreeFields) && !is_array($saveDataValue)) ? trim(strip_tags($saveDataValue)) : $saveDataValue;
                if(array_key_exists($saveDataKey,$userSaveData)) {
                    if(in_array($saveDataKey,$htmlFreeFields)) { $saveDataValue = trim(strip_tags($saveDataValue)); }
                    $userSaveData[$saveDataKey] = $saveDataValue;
                }
            }

            if(isset($data['su'])) {
                $userSaveData['su'] = $data['su'];
            }

            if(isset($data['online'])) {
                $userSaveData['online'] = $data['online'];
            }

            if(isset($data['last_login'])) {
                $userSaveData['last_login'] = $data['last_login'];
            }

            $setSu        = (array_key_exists('su', $userSaveData))         ? ', `su` = :su'                 : '';
            $setOnline    = (array_key_exists('online', $userSaveData))     ? ', `online` = :online'         : '';
            $setLastLogin = (array_key_exists('last_login', $userSaveData)) ? ', `last_login` = :last_login' : '';

            $setChanged = ', `changed` = NOW(), `id_acc_users__changedBy` = :id_acc_users__changedBy';

            $setFirstLogin = (
                ($currUser['id'] == $saveUser['id']) &&
                ($saveUser['firstLogin']) &&
                ($pass_crypted != $saveUser['pass_crypted'])
            ) ? ', `alreadyLoggedIn` = :alreadyLoggedIn' : '';

            $userSaveData['active'] = (!$systemAction && ($currUser['id'] == $saveUser['id'])) ? 1 : $userSaveData['active'];

            $userupdate = '
                UPDATE
                    `'.DB_CONN_DBNAME.'`.`' . DB_TABLE_PREFIX . 'acc_users`
                SET
                    `name` = :name,
                    `surname` = :surname,
                    `gender` = :gender,
                    `email` = :email,
                    `additional` = :additional,
                    `active` = :active,
                    `id_acc_groups` = :id_acc_groups,
                    `default_lang` = :default_lang,
                    `pass` = :pass' .
                $setFirstLogin .
                $setSu .
                $setOnline .
                $setLastLogin .
                $setChanged . '
                WHERE
                    `' . DB_TABLE_PREFIX . 'acc_users`.`id` = \'' . $userSaveData['id'] . '\';';

            $params = array(
                'name'          => $userSaveData['name'],
                'surname'       => $userSaveData['surname'],
                'gender'        => $userSaveData['gender'],
                'email'         => $userSaveData['email'],
                'additional'    => $userSaveData['additional'],
                'active'        => $userSaveData['active'],
                'id_acc_groups' => $userSaveData['id_acc_groups'],
                'default_lang'  => $userSaveData['default_lang'],
                'pass'          => $pass_crypted,
            );

            if($setFirstLogin != '') { $params['alreadyLoggedIn']         = 1; }
            if($setSu != '')         { $params['su']                      = $userSaveData['su']; }
            if($setOnline != '')     { $params['online']                  = $userSaveData['online']; }
            if($setLastLogin != '')  { $params['last_login']              = $userSaveData['last_login']; }
            if($setChanged != '')    { $params['id_acc_users__changedBy'] = $currUser['id']; }

            $sqlResult = $this->Core->Db()->toDatabase($userupdate, $params);

            $success = false;

            if (!$sqlResult) {
                $errMsg  = $this->Core->i18n()->translate('Konnte nicht gespeichert werden!');
                $noteMsg = sprintf( $this->Core->i18n()->translate('MySQL Error: \'%s\''), $errMsg );

                $note  = $noteMsg . '<pre>' . print_r($sqlResult, true) . '</pre>';
                $type  = 'danger';
                $kind  = 'bs-alert';
                $title = $this->Core->i18n()->translate('Fehler') . '!';
            } else {
                $note  = sprintf( $this->Core->i18n()->translate('User \'%s\' erfolgreich aktualisiert!!'), $userSaveData['name'] . ' ' . $userSaveData['surname']);
                $type  = 'success';
                $kind  = 'bs-alert';
                $title = $this->Core->i18n()->translate('Erledigt') . '!';

                $success = true;
            }

            if($notify) {
                $this->Core->setNote($note, $type, $kind, $title);
            } else {
                if(!$success) {
                    $this->Core->Log($note, 'acc_user_save', true);
                }
            }

            $changedSites = false;
            if($success && !$systemAction) {

                $sitesToInsert = array();
                $sitesToDelete = array();

                foreach($id_system_site as $siteId) {
                    if(!in_array($siteId, $userSaveData['sites'])) {
                        $sitesToInsert[] = $siteId;
                    }
                }
                foreach($userSaveData['sites'] as $siteId) {
                    if($siteId !== 0 && $siteId != '' && $siteId !== null &&  !in_array($siteId, $id_system_site)) {
                        $sitesToDelete[] = $siteId;
                    }
                }

                if(count($sitesToInsert)) {
                    /** Insert Information into Users2Sites Table */
                    $insertedSitesIntoUsers2SitesTable_Counter = 0;
                    foreach ($sitesToInsert as $siteId) {
                        if($this->Core->Sites()->siteExists($siteId)) {
                            if ($this->insertIntoUsers2Sites($siteId, $userSaveData['id'])) {
                                $insertedSitesIntoUsers2SitesTable_Counter++;
                            }
                        }
                    }
                    if($insertedSitesIntoUsers2SitesTable_Counter >= 1) {
                        $iir2gResult = (count($id_system_site) > $insertedSitesIntoUsers2SitesTable_Counter) ? $insertedSitesIntoUsers2SitesTable_Counter . '/'  . count($id_system_site) : '100%';
                        $note = sprintf($this->Core->i18n()->translate('Seiten-Zuordnungen (%s) gespeichert!!'), $iir2gResult);
                        $type = 'success';
                        $kind = 'bs-alert';
                        $title = $this->Core->i18n()->translate('Erledigt') . '!';

                        $changedSites = true;
                    } else {
                        $note = sprintf($this->Core->i18n()->translate('Seiten-Zuordnungen zum Benutzer \'%s\' konnten nicht gespeichert werden!!'), $userSaveData['name'] . ' ' . $userSaveData['surname']);
                        $type = 'danger';
                        $kind = 'bs-alert';
                        $title = $this->Core->i18n()->translate('Fehler') . '!';
                    }
                    if($notify) {
                        $this->Core->setNote($note, $type, $kind, $title);
                    }
                }


                if(count($sitesToDelete)) {
                    $deletedSitesIntoUsers2SitesTable_Counter = 0;
                    foreach($sitesToDelete as $siteId) {
                        /** Delete Right from Users2Sites Table */
                        if ($this->deleteFromUsers2Sites((int)$siteId, 'site', $userSaveData['id'] )) {
                            $deletedSitesIntoUsers2SitesTable_Counter++;
                        }
                    }
                    if($deletedSitesIntoUsers2SitesTable_Counter >= 1) {
                        $dr2gResult = (count($sitesToDelete) > $deletedSitesIntoUsers2SitesTable_Counter) ? $deletedSitesIntoUsers2SitesTable_Counter . '/'  . count($sitesToDelete) : '100%';
                        $note = sprintf($this->Core->i18n()->translate('Seiten-Zuordnungen (%s) gelöscht!!'), $dr2gResult);
                        $type = 'success';
                        $kind = 'bs-alert';
                        $title = $this->Core->i18n()->translate('Erledigt') . '!';

                        $changedSites = true;
                    } else {
                        $note = sprintf($this->Core->i18n()->translate('Seiten-Zuordnungen zum Benutzer \'%s\' konnten nicht gelöscht werden!!'), $userSaveData['name'] . ' ' . $userSaveData['surname']);
                        $type = 'danger';
                        $kind = 'bs-alert';
                        $title = $this->Core->i18n()->translate('Fehler') . '!';
                    }
                    if($notify) {
                        $this->Core->setNote($note, $type, $kind, $title);
                    }
                }
            }

            /**
             * Fire Hooks if the Events are happening
             *
             * acc_user_saved
             * acc_user_deactivate
             * acc_user_change_email
             * acc_user_change_password
             * acc_user_change_site
             * acc_user_change_group
             */
            $this->Core->Hooks()->fire('acc_user_saved', $saveUser['id']);

            if(!$systemAction) {
                if ($saveUser['active'] === 1 && $userSaveData['active'] === 0) {
                    $this->Core->Hooks()->fire('user_may_logout', array($saveUser['id'], 'acc_user_deactivate'));
                }

                if ($saveUser['email'] != $userSaveData['email']) {
                    $this->Core->Hooks()->fire('user_may_logout', array($saveUser['id'], 'acc_user_change_email'));
                }

                if (($saveUser['pass_crypted'] != $pass_crypted) && $setFirstLogin == '') {
                    $this->Core->Hooks()->fire('user_may_logout', array($saveUser['id'], 'acc_user_change_password'));
                    $this->Core->Hooks()->fire('user_pass_changed', array($saveUser));
                }

                if ($changedSites) {
                    $this->Core->Hooks()->fire('user_may_logout', array($saveUser['id'], 'acc_user_change_site'));
                }

                if ($saveUser['id_acc_groups'] != $userSaveData['id_acc_groups']) {
                    $this->Core->Hooks()->fire('user_may_logout', array($saveUser['id'], 'acc_user_change_group'));
                }
            } else {
                if ($pass_crypted !== $saveUser['pass_crypted']) {
                    $this->Core->Hooks()->fire('user_pass_changed', array($saveUser));
                }
            }

            if($redirect) {
                $redirectUrl = $this->Mvc->getModelUrl() . '/user/edit/' . $userSaveData['id'];
                if($data['action'] == 'user_save_exit') {
                    $redirectUrl = $this->Mvc->getModelUrl() . '/user/';
                }
                $this->Core->Request()->redirect($redirectUrl);
            }

        } else {

            $this->Core->Hooks()->fire('acc_user_create');

            $name             = (isset($data['name']) && trim(strip_tags($data['name'])) != '') ? trim(strip_tags($data['name'])) : '';
            $surname          = (isset($data['surname']) && trim(strip_tags($data['surname'])) != '') ? trim(strip_tags($data['surname'])) : '';
            $gender           = (isset($data['gender']) && $data['gender'] != '') ? $data['gender'] : '';
            $email            = (isset($data['email']) && $data['email'] != '') ? $data['email'] : '';
            $additional       = (isset($data['additional']) && $data['additional'] != '') ? $data['additional'] : '';
            $salt             = $this->createSalt();
            $pass_crypted     = (isset($data['pass']) && $data['pass'] != '') ? $this->getPasswordHash($data['pass'],$salt) : null;
            $active           = $userActive;
            $id_system_site   = (isset($data['id_system_sites']) && is_array($data['id_system_sites'])) ? $data['id_system_sites'] : array();
            $id_acc_groups    = (isset($data['id_acc_groups']) && $data['id_acc_groups'] != '') ? $data['id_acc_groups'] : 0;
            $default_lang     = (isset($data['default_lang']) && $data['default_lang'] != '') ? $data['default_lang'] : $this->Core->i18n()->getDefaultLang();
            $sendInfoMail     = (!array_key_exists('sendinfomail',$data) || $data['sendinfomail'] <= '0' ) ? false : true;
            $changed          = '0000-00-00';

            if($name != '' && $email != '' && $pass_crypted !== null) {

                $userinsertinto = '
					INSERT INTO
					`' . DB_TABLE_PREFIX . 'acc_users`
					(`name`, `surname`, `gender`, `email`, `additional`, `salt`, `pass`, `active`, `id_acc_groups`, `default_lang`, `changed`, `created`)
					VALUES
					(:name, :surname, :gender, :email, :additional, :salt, :pass, :active, :id_acc_groups, :default_lang, :changed, NOW())
                ';

                $params = array(
                    'name'          => $name,
                    'surname'       => $surname,
                    'gender'        => $gender,
                    'email'         => $email,
                    'additional'    => $additional,
                    'salt'          => $salt,
                    'pass'          => $pass_crypted,
                    'active'        => $active,
                    'id_acc_groups' => $id_acc_groups,
                    'default_lang'  => $default_lang,
                    'changed'       => $changed
                );
                $sqlResult = $this->Core->Db()->toDatabase($userinsertinto, $params);

                $this->Core->Hooks()->fire('acc_user_created');

                $success = false;

                if ($this->Core->Db()->lastInsertId() == ""){
                    $errMsg = $this->Core->i18n()->translate('Konnte nicht erstellt werden!');
                    $note  = sprintf( $this->Core->i18n()->translate('MySQL Error: \'%s\''), $errMsg );
                    if($notify) {
                        $type  = 'danger';
                        $kind  = 'bs-alert';
                        $title = $this->Core->i18n()->translate('Fehler') . '!';
                        $this->Core->setNote($note, $type, $kind, $title);
                    } else {
                        $this->Core->Log($note,'acc_user_save',true);
                    }
                } else {
                    $note  = sprintf( $this->Core->i18n()->translate('User \'%s\' erfolgreich hinzugefügt!!'), $name . ' ' . $surname);
                    if($notify) {
                        $type  = 'success';
                        $kind  = 'bs-alert';
                        $title = $this->Core->i18n()->translate('Erledigt') . '!';
                        $this->Core->setNote($note, $type, $kind, $title);

                        $success = true;
                    }

                    $db_id = $this->Core->Db()->lastInsertId();

                    if($sendInfoMail) {
                        $this->_sendCreateMail($this->getUser($db_id), $data['pass']);
                    }

                    if($success) {
                        /** Insert Information into Users2Sites Table */
                        $insertedSitesIntoUsers2SitesTable_Counter = 0;
                        foreach ($id_system_site as $siteId) {
                            if ($this->insertIntoUsers2Sites($siteId, $db_id)) {
                                $insertedSitesIntoUsers2SitesTable_Counter++;
                            }
                        }
                        if($insertedSitesIntoUsers2SitesTable_Counter >= 1) {
                            $iir2gResult = (count($id_system_site) > $insertedSitesIntoUsers2SitesTable_Counter) ? $insertedSitesIntoUsers2SitesTable_Counter . '/'  . count($id_system_site) : '100%';
                            $note = sprintf($this->Core->i18n()->translate('Seiten-Zuordnungen (%s) gespeichert!!'), $iir2gResult);
                            $type = 'success';
                            $kind = 'bs-alert';
                            $title = $this->Core->i18n()->translate('Erledigt') . '!';
                            $this->Core->setNote($note, $type, $kind, $title);
                        } else {
                            $note = sprintf($this->Core->i18n()->translate('Seiten-Zuordnungen zum Benutzer \'%s\' konnten nicht gespeichert werden!!'), $name . ' ' . $surname);
                            $type = 'danger';
                            $kind = 'bs-alert';
                            $title = $this->Core->i18n()->translate('Fehler') . '!';
                            $this->Core->setNote($note, $type, $kind, $title);
                        }
                    }

                    if($redirect) {
                        $redirectUrl = $this->Mvc->getModelUrl() . '/user/edit/' . $db_id;
                        if($data['action'] == 'user_save_exit') {
                            $redirectUrl = $this->Mvc->getModelUrl() . '/user/';
                        }
                        $this->Core->Request()->redirect($redirectUrl);
                    }
                }
            } else {
                $note  = $this->Core->i18n()->translate('Bitte alle Felder ausfüllen!');
                if($notify) {
                    $type  = 'danger';
                    $kind  = 'bs-alert';
                    $title = $this->Core->i18n()->translate('Fehler') . '!';
                    $this->Core->setNote($note, $type, $kind, $title);
                } else {
                    $this->Core->Log($note,'acc_user_save',true);
                }

                if($redirect) {
                    $redirectUrl = $this->Mvc->getModelUrl() . '/user/create/';
                    $this->Core->Request()->redirect($redirectUrl);
                }
            }
        }

        return $success;
    }

    public function insertIntoUsers2Sites($siteId, $userId) {
        $curUser = $this->getUser();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        if(!$this->Core->Sites()->siteExists($siteId) || !$this->userExists($userId)) {
            return false;
        }

        /** Insert into Users2Sites Table */
        $users2sitesInsertSql = 'INSERT INTO `' . DB_TABLE_PREFIX . 'system_users2sites` (`id_acc_users`, `id_system_sites`, `id_acc_users__changedBy`) VALUES (:id_acc_users, :id_system_sites, :id_acc_users__changedBy)';
        $params = array(
            'id_acc_users'            => $userId,
            'id_system_sites'         => $siteId,
            'id_acc_users__changedBy' => $curUserId,
        );

        $sqlResult = $this->Core->Db()->toDatabase($users2sitesInsertSql, $params);

        return ($this->Core->Db()->lastInsertId() == '') ? false : true;
    }

    public function deleteFromUsers2Sites($id, $type, $userId = null) {
        if(!is_int($id) || $id < 1 || ($type != 'user' && $type != 'site') || ($userId !== null && !$this->userExists($userId))) {
            return false;
        }

        if($type == 'site') {
            if(!$this->Core->Sites()->siteExists($id) || $userId === null || !$this->userExists($userId))  {
                return false;
            }
            $whereColumn = 'id_system_sites';
            $whereStatement = " WHERE " . $whereColumn . " = '" . $id . "' AND id_acc_users = '" . $userId . "'";
        } else {
            if(!$this->userExists($id)) {
                return false;
            }
            $whereColumn = 'id_acc_users';
            $whereStatement = " WHERE " . $whereColumn . " = '" . $id . "'";
        }

        /** Delete from Rights2Groups Table */
        $rights2groupsDeleteSql = 'DELETE FROM `' . DB_TABLE_PREFIX . 'system_users2sites`' . $whereStatement;
        $sqlResult = $this->Core->Db()->toDatabase($rights2groupsDeleteSql, array(), false, true);
        if (!$sqlResult) {
            return false;
        } else {
            return true;
        }
    }

    public function deleteUser($id)
    {
        $users = $this->getUsers();
        if(array_key_exists($id, $users)) {
            $delUser = $users[$id];
            $userdeleteSql = 'DELETE FROM `' . DB_TABLE_PREFIX . 'acc_users` WHERE id = :userid LIMIT 1';
            $params = array(
                'userid' => $id
            );
            $sqlResult = $this->Core->Db()->toDatabase($userdeleteSql, $params);

            if (!$sqlResult){
                $note  = sprintf( $this->Core->i18n()->translate('MySQL Error: \'%s\''), $this->Core->Db()->errorInfo()[2]);
                $type  = 'danger';
                $kind  = 'bs-alert';
                $title = $this->Core->i18n()->translate('Fehler') . '!';
                $this->Core->setNote($note, $type, $kind, $title);
            }
            else {
                $note  = sprintf( $this->Core->i18n()->translate('User \'%s\' permanent gelöscht!!'), $delUser['name'] . ' ' . $delUser['surname']);
                $type  = 'success';
                $kind  = 'bs-alert';
                $title = $this->Core->i18n()->translate('Erledigt') . '!';
                $this->Core->setNote($note, $type, $kind, $title);
            }
        } else {
            $note  = sprintf( $this->Core->i18n()->translate('User mit der ID \'%s\' konnte nicht gefunden werden...'), $id);
            $type  = 'danger';
            $kind  = 'bs-alert';
            $title = $this->Core->i18n()->translate('Fehler') . '!';
            $this->Core->setNote($note, $type, $kind, $title);
        }
        $redirectUrl = $this->Mvc->getModelUrl() . '/user/';
        $this->Core->Request()->redirect($redirectUrl);
    }

    public function setUserToSu($id)
    {
        $curUser = $this->getUser();
        $user = $this->getUser($id);

        if($curUser['su'] && $user['id'] != $curUser['id']) {
            $data = array();
            $data['su'] = 1;

            // Set User to SU
            $this->saveUser($user['id'],$data,false,false);

            // Remove SU from Current User
            $data['su'] = 0;
            $data['action'] = 'user_save_exit';
            $this->saveUser($curUser['id'],$data,false,true);
        }
    }

    public function getUserOptions($userId = null, $option = '') {
        /** @var $sitesClass Sites */
        $sitesClass = $this->Core->Sites();
        $site      = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
        $siteId    = (count($site)) ? $site['id'] : 0;

        $getUserOptions_config = $this->Core->Config()->get('user_options', $siteId, true, false);

        $getUserOptions = (is_array($getUserOptions_config)) ? $getUserOptions_config : array();

        $return = $getUserOptions;

        if(array_key_exists($userId, $getUserOptions)) {
            $return = $getUserOptions[$userId];

            if($option !== '' && array_key_exists($option, $getUserOptions[$userId])) {
                $return = $getUserOptions[$userId][$option];
            }
        }


        return $return;
    }

    public function setUserOption($userId,$option,$value = '') {
        $return = false;

        if($this->userExists($userId) && $option !== '' && $value !== '') {
            /** @var $sitesClass Sites */
            $sitesClass = $this->Core->Sites();
            $site      = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
            $siteId    = (count($site)) ? $site['id'] : 0;

            $getUserOptions = $this->getUserOptions();

            $getUserOptions[$userId][$option] = $value;

            $return = $this->Core->Config()->set('user_options', $getUserOptions, $siteId, 'acc', 'option');
        }

        return $return;
    }

    public function delUserOption($userId,$option) {
        $return = false;

        if($this->userExists($userId) && $option !== '') {
            /** @var $sitesClass Sites */
            $sitesClass = $this->Core->Sites();
            $site      = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
            $siteId    = (count($site)) ? $site['id'] : 0;

            $getUserOptions = $this->getUserOptions($userId);

            if(array_key_exists($userId, $getUserOptions)) {
                if($option !== 'all' && array_key_exists($option, $getUserOptions[$userId])) {
                    unset($getUserOptions[$userId][$option]);
                } else {
                    unset($getUserOptions[$userId]);
                }
            }

            $return = $this->Core->Config()->set('user_options', $getUserOptions, $siteId, 'acc', 'option');
        }

        return $return;
    }

    /** Mailer Methods */

    private function _sendCreateMail($user, $pass) {

        if(!count($user) || $pass == '') {
            return false;
        } else {
            /** @var $mailerClass Mailer */
            $mailerClass = $this->Mvc->modelClass('mailer');

            if(is_object($mailerClass)) {

                $currLang = $this->Core->i18n()->getCurrLang();

                $useLang = (array_key_exists('default_lang', $user)) ? $user['default_lang'] : $currLang;

                $userCreate_1 = $mailerClass->getTemplateByCode('user_create_1', $useLang);
                $userCreate_2 = $mailerClass->getTemplateByCode('user_create_2', $useLang);

                if(count($userCreate_1) && count($userCreate_2)) {

                    $sendData = array();

                    /** $sendData['from'] = ''; /** Will be Set by Mailer */

                    $sendData['to']      = $user['email'];
                    $sendData['subject'] = '';
                    $sendData['message'] = '';
                    $sendData['ishtml']  = true;
                    $sendData['userId']  = $user['id'];
                    $sendData['lang']    = $useLang;
                    $sendData['system']  = true;
                    // $sendData['saveMail'] = false;

                    $mailsToSend = array();

                    /** first Mail */
                    $mailsToSend[0] = $sendData;
                    $mailsToSend[0]['subject'] = $userCreate_1['subject'];
                    $mailsToSend[0]['message'] = $userCreate_1['content'];
                    /** second Mail */
                    $mailsToSend[1] = $sendData;
                    $mailsToSend[1]['subject'] = $userCreate_2['subject'];
                    $mailsToSend[1]['message'] = sprintf($userCreate_2['content'], $pass);

                    $sendSuccess = array();
                    foreach ($mailsToSend as $mailSendData) {
                        $sendResult = $mailerClass->sendMail($mailSendData);
                        $sendSuccess[] = $sendResult['success'];
                    }
                }
            }
        }
    }

    private function _sendResetPassLink($user, $resetPassHash) {

        if(!count($user) || $resetPassHash == '') {
            return false;
        } else {
            /** @var $mailerClass Mailer */
            $mailerClass = $this->Mvc->modelClass('mailer');

            if(is_object($mailerClass)) {

                $currLang = $this->Core->i18n()->getCurrLang();
                $useLang = (array_key_exists('default_lang', $user)) ? $user['default_lang'] : $currLang;

                $userResetPassTpl = $mailerClass->getTemplateByCode('user_resetpass', $useLang);

                if(!count($userResetPassTpl)) { return false; }

                $siteId = (array_key_exists('sites', $user) && is_array($user['sites']) && count($user['sites'])) ? reset($user['sites']) : 0;
                $mailerClass->setUseSiteId($siteId);

                $sendData = array();

                /** $sendData['from'] = ''; /** Will be Set by Mailer */

                /** Reset Password E-Mail */
                $sendData['to']      = $user['email'];
                $sendData['subject'] = $userResetPassTpl['subject'];
                $sendData['message'] = sprintf($userResetPassTpl['content'], $resetPassHash);
                $sendData['ishtml']  = true;
                $sendData['userId']  = $user['id'];
                $sendData['lang']    = $useLang;
                $sendData['system']  = true;
                // $sendData['saveMail'] = false;

                $sendResult = $mailerClass->sendMail($sendData);
                $sendSuccess = $sendResult['success'];

                return $sendSuccess;
            } else {

                return false;
            }
        }
    }

    private function _sendPassChangedMail($user) {

        if(!count($user)) {
            return false;
        } else {
            /** @var $mailerClass Mailer */
            $mailerClass = $this->Mvc->modelClass('mailer');

            if(is_object($mailerClass)) {

                $currLang = $this->Core->i18n()->getCurrLang();
                $useLang = (array_key_exists('default_lang', $user)) ? $user['default_lang'] : $currLang;

                $userPassChangedTpl = $mailerClass->getTemplateByCode('user_pass_changed', $useLang);

                if(!count($userPassChangedTpl)) { return false; }

                $siteId = (array_key_exists('sites', $user) && is_array($user['sites']) && count($user['sites'])) ? reset($user['sites']) : 0;
                $mailerClass->setUseSiteId($siteId);

                $sendData = array();

                /** $sendData['from'] = ''; /** Will be Set by Mailer */

                /** Reset Password E-Mail */
                $sendData['to']      = $user['email'];
                $sendData['subject'] = $userPassChangedTpl['subject'];
                $sendData['message'] = $userPassChangedTpl['content'];
                $sendData['ishtml']  = true;
                $sendData['userId']  = $user['id'];
                $sendData['lang']    = $useLang;
                $sendData['system']  = true;
                // $sendData['saveMail'] = false;

                $sendResult = $mailerClass->sendMail($sendData);
                $sendSuccess = $sendResult['success'];

                return $sendSuccess;
            } else {
                return false;
            }
        }
    }

    /** User Security Methods */
    public function createSalt()
    {
        $salt = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789"), 0, 16);
        return $salt;
    }

    public function getPasswordHash($password, $salt)
    {
        $preHash = crypt($password,'$6$rounds=8000$'. $salt .'$'); // use SHA-512, 8000 rounds
        $hash    = substr($preHash,32, 32);

        return $hash;
    }

    public function checkMasterKey($password) {
        $sql = 'SELECT id FROM `' . DB_TABLE_PREFIX . 'system_config` WHERE `value` = sha2(:password,256)';
        $params = array(
            'password' => $password
        );
        $result = $this->Core->Db()->fromDatabase($sql, '@raw', $params);
        return (count($result)) ? true : false;
    }

    /** User OnlineState Methods **/
    private function _prepareUserOnlineState($data) {
        $return = array();

        if(is_array($data) && count($data)) {

            $nowtime = time();

            $user = $data;

            $useInactiveState = false;
            $logoutAfterMinutes = 25;
            $inactiveAfterMinutes = 5;

            $user_id      = (array_key_exists('id', $user))         ? $user['id']         : false;
            $user_name    = (array_key_exists('name', $user))       ? $user['name']       : '';
            $user_surname = (array_key_exists('surname', $user))    ? $user['surname']    : '';
            $online       = (array_key_exists('online', $user))     ? $user['online']     : '';
            $last_login   = (array_key_exists('last_login', $user)) ? $user['last_login'] : '';

            if($user_id !== false && $user_name != '' && $user_surname != '' && $online != '' && $last_login != '') {

                $last_login_strtotime = strtotime($last_login);
                $online_strtotime = strtotime($online);

                $last_login_date_y = date('Y', $last_login_strtotime);
                $last_login_date_m = date('m', $last_login_strtotime);
                $last_login_date_d = date('d', $last_login_strtotime);
                $lastlogin_date_new = $last_login_date_d . '.' . $last_login_date_m . '.' . $last_login_date_y;

                $last_login_time_h = date('H', $last_login_strtotime);
                $last_login_time_m = date('i', $last_login_strtotime);
                $last_login_time_s = date('s', $last_login_strtotime);

                $online_date_y = date('Y', $online_strtotime);
                $online_date_m = date('m', $online_strtotime);
                $online_date_d = date('d', $online_strtotime);

                $online_time_h = date('H', $online_strtotime);
                $online_time_m = date('i', $online_strtotime);
                $online_time_s = date('s', $online_strtotime);

                $online_result = $this->makeDiff($online_strtotime, $nowtime);
                $td = $this->makeDiff($last_login_strtotime, $nowtime);

                $last_login_since = '';
                if ($td['day'][0] == 0) {
                    $last_login_since = $td['std'][0] . ' ' . $td['std'][1] . ', ' . $td['min'][0] . ' ' . $td['min'][1] . ' ' . $this->Core->i18n()->translate('und') . ' ' . $td['sec'][0] . ' ' . $td['sec'][1];
                }
                if ($td['day'][0] == 0 && $td['std'][0] == 0) {
                    $last_login_since = $td['min'][0] . ' ' . $td['min'][1] . ' und ' . $td['sec'][0] . ' ' . $td['sec'][1];
                }
                if ($td['day'][0] == 0 && $td['std'][0] == 0 && $td['min'][0] == 0) {
                    $last_login_since = 'nur ' . $td['sec'][0] . ' ' . $td['sec'][1];
                }
                if ($td['day'][0] > 0 && $td['std'][0] > 0 && $td['min'][0] > 0) {
                    if ($td['day'][0] > 100) {
                        if ($last_login == '0000-00-00 00:00:00') {
                            $last_login_since = $this->Core->i18n()->translate('noch nie');
                        } else {
                            $last_login_since = $this->Core->i18n()->translate('mehr als 100 Tage');
                        }
                    } else {
                        $last_login_since = $td['day'][0] . ' ' . $td['day'][1] . ', ' . $td['std'][0] . ' ' . $td['std'][1] . ', ' . $td['min'][0] . ' ' . $td['min'][1] . ' ' . $this->Core->i18n()->translate('und') . ' ' . $td['sec'][0] . ' ' . $td['sec'][1];
                    }
                }

                $state_int = 0;
                $online_state = $this->Core->i18n()->translate('offline');
                $online_state_text = $user_name . ' ' . $user_surname . ' ' . $this->Core->i18n()->translate('ist') . ' ' . $online_state . '. ' . $this->Core->i18n()->translate('Letzter Login vor') . ': ' . $last_login_since;

                if ($useInactiveState) {
                    if ($online != "0000-00-00 00:00:00" && ($online_result['day'][0] <= 1 && $online_result['std'][0] <= 1 && $online_result['min'][0] <= $inactiveAfterMinutes)) {
                        $state_int = 1;
                        $online_state = $this->Core->i18n()->translate('online');
                        $online_state_text = $user_name . ' ' . $user_surname . ' ' . $this->Core->i18n()->translate('ist') . ' ' . $online_state;
                    }
                    if ($online != "0000-00-00 00:00:00" && ($useInactiveState && ($online_result['day'][0] <= 1 && $online_result['std'][0] <= 1 && $online_result['min'][0] > $inactiveAfterMinutes && $online_result['min'][0] <= $logoutAfterMinutes))) {
                        $state_int = 2;
                        $online_state = $this->Core->i18n()->translate('inaktiv');
                        $online_state_text = $user_name . ' ' . $user_surname . ' ' . $this->Core->i18n()->translate('ist') . ' ' . $online_state . ' ' . $this->Core->i18n()->translate('seit') . ' ' . $online_result['min'][0] . ' ' . $online_result['min'][1];
                    }
                } else {
                    if ($online != "0000-00-00 00:00:00" && ($online_result['day'][0] <= 1 && $online_result['std'][0] <= 1 && $online_result['min'][0] <= $logoutAfterMinutes)) {
                        $state_int = 1;
                        $online_state = $this->Core->i18n()->translate('online');
                        $online_state_text = $user_name . ' ' . $user_surname . ' ' . $this->Core->i18n()->translate('ist') . ' ' . $online_state;
                    }
                }

                $return['id'] = $user_id;
                $return['online'] = $online;
                $return['last_login'] = $last_login;
                $return['state_int'] = $state_int;
                $return['state'] = $online_state;
                $return['state_text'] = $online_state_text;

                $return['last_login_since'] = $last_login_since;
            }
        }

        return $return;
    }

    public function setOnlineActivity($id = null, $setLoginTime = false)
    {
        $userId = ($id !== null && $this->userExists($id)) ? $id : $this->getCurrUserId();
        if($userId !== 0) {
            $nowfull = date("Y-m-d H:i:s",mktime((date("H")),date("i"),date("s")));

            $id = $userId;
            $data = array();
            $data['online'] = $nowfull;
            if($setLoginTime) {
                $data['last_login'] = $nowfull;
            }

            $this->saveUser($id,$data,false,false, true);
        }
    }

    public function getUserOnlineStates($id = null)
    {
        $uos = array();
        $getUsers = $this->getUsers();
        foreach ($getUsers as $user) {
            $uos[$user['id']] = $this->_prepareUserOnlineState($user);
        }

        return ($id !== null && array_key_exists($id, $uos)) ? $uos[$id] : $uos;
    }

    public function makeDiff($first, $second)
    {

        if($first > $second)
            $td['dif'][0] = $first - $second;
        else
            $td['dif'][0] = $second - $first;

        $td['sec'][0] = $td['dif'][0] % 60; // 67 = 7

        $td['min'][0] = (($td['dif'][0] - $td['sec'][0]) / 60) % 60;

        $td['std'][0] = (((($td['dif'][0] - $td['sec'][0]) /60)-
                    $td['min'][0]) / 60) % 24;

        $td['day'][0] = floor( ((((($td['dif'][0] - $td['sec'][0]) /60)-
                    $td['min'][0]) / 60) / 24) );

        $td = $this->makeString($td);

        return $td;

    }

    public function makeString($td)
    {

        if ($td['sec'][0] == 1)
            $td['sec'][1] = $this->Core->i18n()->translate('Sekunde');
        else
            $td['sec'][1] = $this->Core->i18n()->translate('Sekunden');

        if ($td['min'][0] == 1)
            $td['min'][1] = $this->Core->i18n()->translate('Minute');
        else
            $td['min'][1] = $this->Core->i18n()->translate('Minuten');

        if ($td['std'][0] == 1)
            $td['std'][1] = $this->Core->i18n()->translate('Stunde');
        else
            $td['std'][1] = $this->Core->i18n()->translate('Stunden');

        if ($td['day'][0] == 1)
            $td['day'][1] = $this->Core->i18n()->translate('Tag');
        else
            $td['day'][1] = $this->Core->i18n()->translate('Tage');

        return $td;

    }

    /** Access Methods **/
    private function _prepareRightsData($data) {
        $preparedData = array();

        $right_id      = 0;
        $right_name    = '';
        $right_code    = '';

        if(is_array($data) && count($data)) {
            $right_id      = (array_key_exists('id', $data))         ? $data['id']         : 0;
            $right_name    = (array_key_exists('name', $data))       ? $data['name']       : '';
            $right_code    = (array_key_exists('code', $data))       ? $data['code']       : '';
        } else {
            if(is_string($data) && $data != '') {
                $right_code = trim($data);
            }
        }

        if($right_code != '') {
            $preparedData['id']   = $right_id;
            $preparedData['name'] = ($right_name != '') ? $right_name : $this->Core->i18n()->translate($right_code);
            $preparedData['code'] = $right_code;
        }

        return $preparedData;
    }

    public function getProtectedRights($type = 'list') {
        $protectedRights = array();

        $protectedRightsList = array(
            'acc',
            'acc_user',
            'acc_user_create',
            'acc_user_edit',
            'acc_user_delete',
            'acc_group',
            'acc_group_create',
            'acc_group_edit',
            'acc_group_delete',
            'acc_rights',
            'acc_rights_create',
            'acc_rights_edit',
            'acc_rights_delete',
        );

        asort($protectedRightsList);

        if($type == 'full') {
            foreach($protectedRightsList as $rightCode) {
                $preparedData = $this->_prepareRightsData($rightCode);
                if(count($preparedData)) {
                    $protectedRights[$preparedData['code']] = $preparedData;
                    $protectedRights[$preparedData['code']]['type'] = 'protected';
                }
            }
        }

        if($type == 'list') {
            $protectedRights = $protectedRightsList;
        }

        return $protectedRights;
    }

    public function getWhitelistRights() {
        $rightsWhitelist = $this->rightsWhitelist;
        if(!is_array($rightsWhitelist) && !count($rightsWhitelist)) {
            $sitesClass = $this->Core->Sites();
            $site = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
            $siteId = (count($site) && array_key_exists('id', $site)) ? $site['id'] : 0;

            $rightsWhitelist_options = $this->Core->Config()->get('rightsWhitelist', $siteId);
            $rightsWhitelist_optionsArray = (is_array($rightsWhitelist_options)) ? $rightsWhitelist_options : array();

            asort($rightsWhitelist_optionsArray);

            $rightsWhitelist_fullArray = array();
            foreach($rightsWhitelist_optionsArray as $rightCode) {
                $preparedData = $this->_prepareRightsData($rightCode);
                if(count($preparedData)) {
                    $rightsWhitelist_fullArray[$preparedData['code']] = $preparedData;
                    $rightsWhitelist_fullArray[$preparedData['code']]['type'] = 'whitelist';
                }
            }

//            $protectedRights = $this->getProtectedRights('full');
//
//            $rightsWhitelist = array_merge_recursive($protectedRights, $rightsWhitelist_fullArray);
            $rightsWhitelist = (is_array($rightsWhitelist_fullArray)) ? $rightsWhitelist_fullArray : array();

            $this->rightsWhitelist = $rightsWhitelist;
        }

        return $rightsWhitelist;
    }

    public function getFileRights() {
        $rightsFile      = $this->getFileRightStrings();

        $rightsFilet_fullArray = array();
        foreach($rightsFile as $rightCode) {
            $preparedData = $this->_prepareRightsData($rightCode);
            if(count($preparedData)) {
                $rightsFilet_fullArray[$preparedData['code']] = $preparedData;
                $rightsFilet_fullArray[$preparedData['code']]['type'] = 'file';
            }
        }

        return $rightsFilet_fullArray;
    }

    public function getDataBaseRights()
    {
        $rightsDB = $this->rightsDB;
        if(!is_array($rightsDB) && !count($rightsDB)){
            $rightsDB = array();
            $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'acc_rights`';
            $result = $this->Core->Db()->fromDatabase($sql, '@raw');
            foreach($result as $row)
            {
                $preparedData = $this->_prepareRightsData($row);
                if(count($preparedData)) {
                    $rightsDB[$preparedData['code']] = $preparedData;
                    $rightsDB[$preparedData['code']]['type'] = 'db';
                }
            }

            $this->rightsDB = $rightsDB;
        }
        return $rightsDB;
    }

    public function getRights()
    {
        $rights = $this->rights;
        if(!is_array($rights) || !count($rights)){
            $rights = array();

            $rightsFile      = $this->getFileRights();
            $rightsDB        = $this->getDataBaseRights();
            $rightsWhitelist = $this->getWhitelistRights();

            $rights = array_merge ($rightsFile, $rightsDB);

            $user = $this->getUser();
            $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

            foreach($rights as $rightCode => $right) {
                if(!array_key_exists($rightCode, $rightsWhitelist)) { continue; }
                $rights[$rightCode]['fromWhitelist'] = true;
            }
            	
    		$vag_grouId = (array_key_exists('system_debug_vag', $_SESSION) && is_numeric($_SESSION['system_debug_vag'])) ? $_SESSION['system_debug_vag'] : null;

            if(!$isSu || $vag_grouId !== null) {
                $filteredRights = array();

                foreach($rights as $rightCode => $right) {
                    if(!array_key_exists('fromWhitelist', $right) || $right['fromWhitelist'] !== true) { continue; }

                    $filteredRights[$rightCode] = $right;
                }

                $rights = $filteredRights;
            }

            if(!function_exists('sortByCode')) {
                function sortByCode($a, $b)
                {
                    $aCode = (is_array($a) && array_key_exists('code', $a)) ? $a['code'] : '';
                    $bCode = (is_array($b) && array_key_exists('code', $b)) ? $b['code'] : '';

                    return strcmp($aCode, $bCode);
                }
            }
            uasort($rights, 'sortByCode');

            $this->rights = $rights;
        }
        return $rights;
    }

    public function getRightsByType($type)
    {
        $rights = $this->getRights();

        $rightsByType = array();
        foreach($rights as $rightsCode => $right) {
            if(!is_array($right) || !array_key_exists('type' , $right) || $right['type'] != $type) { continue; }

            $rightsByType[$rightsCode] = $right;
        }

        return $rightsByType;
    }

    public function getRightByCode($rightCode) {
        $right = array();

        if($rightCode != '') {
            $rights = $this->getRights();
            if(array_key_exists($rightCode, $rights) && is_array($rights[$rightCode])) {
                $right = $rights[$rightCode];
            }
        }

        return $right;
    }

    public function getRightById($rightId) {
        $right = array();

        if($rightId > 0) {
            foreach ($this->getRights() as $rightCode => $rightArray) {
                if($rightArray['id'] == $rightId) {
                    $right = $rightArray;
                    break;
                }
            }
        }

        return $right;
    }

    public function rightExists($right_code){

        // $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'acc_rights` WHERE `code` = :rightcode;';
        // $params = array(
        //     'rightcode' => $right_code
        // );
        // $result = $this->Core->Db()->fromDatabase($sql, '@raw', $params);

        $result = $this->getRightByCode($right_code);
        return (count($result)) ? true : false;
    }

    public function getGroups()
    {
        $groups = $this->groups;
        if(!is_array($groups) && !count($groups)){
            $site = $this->Core->Sites()->getSite();
            $siteId = (isset($site['id'])) ? $site['id'] : 0;
            $rights = $this->getRights();
            $groups = array();

            $baseAdminGroup = array(
                'id'       => 0,
                'code'     => 'admin',
                'name'     => $this->Core->i18n()->translate('Administrator'),
                'admin'    => true,
                'rights'   => $rights,
                'siteId'   => 0
            );
            $groups[0] = $baseAdminGroup;

            $sql = '
                SELECT
                    groups.id,
                    groups.code,
                    groups.name,
                    groups.default,
                    groups.admin,
                    GROUP_CONCAT(' . DB_TABLE_PREFIX . 'acc_rights2groups.acc_rights_code) AS rights,
                    groups.id_system_sites
                FROM
                    ' . DB_TABLE_PREFIX . 'acc_groups AS groups
                LEFT JOIN
                    ' . DB_TABLE_PREFIX . 'acc_rights2groups ON ' . DB_TABLE_PREFIX . 'acc_rights2groups.id_acc_groups = groups.id
                WHERE
                    groups.`id_system_sites` = 0
                    OR
                    FIND_IN_SET(:siteId, REPLACE(groups.`id_system_sites`, " ", ""))
                GROUP BY
                    groups.code
                ';
            $params = array(
                'siteId' => $siteId
            );
            $result = $this->Core->Db()->fromDatabase($sql, '@raw', $params);

            foreach($result as $row)
            {

                $groupId = $row['id'];

                $gDefaultArray = array();
                if ($row['default'] != '') {
                    if (strpos($row['default'], ',') !== false) {
                        $gDefaultArray = preg_split('/,/', $row['default'], -1, PREG_SPLIT_NO_EMPTY);
                    } else {
                        if (is_numeric($row['default'])) {
                            $gDefaultArray = array($row['default']);
                        } else {
                            $gDefaultArray = array();
                        }
                    }
                }

                $isDefault = (in_array($siteId,$gDefaultArray));

                $groups[$groupId] = array(
                    'id'             => $row['id'],
                    'code'           => $row['code'],
                    'name'           => $row['name'],
                    'default'        => $isDefault,
                    'defaultSiteIds' => $gDefaultArray,
                    'admin'          => ($row['admin']) ? true : false,
                    'rights'         => array(),
                    'siteId'         => $row['id_system_sites']
                );
                if($row['admin']) {
                    $groups[$groupId]['rights'] = $rights;
                } else {
                    $gRightsArray = array();
                    if ($row['rights'] != '') {
                        if (strpos($row['rights'], ',') !== false) {
                            $gRightsArray = preg_split('/,/', $row['rights'], -1, PREG_SPLIT_NO_EMPTY);
                        } else {
                            if (is_numeric($row['rights'])) {
                                $gRightsArray = array($row['rights']);
                            } else {
                                $gRightsArray = array();
                            }
                        }
                    }
                    $gRights = (count($gRightsArray)) ? $gRightsArray : array();
                    foreach ($rights as $rightCode => $right) {
                        $rightId = $right['id'];
                        if (in_array($rightCode, $gRights)) {
                            $groups[$groupId]['rights'][$rightCode] = $right;
                        }
                    }
                }

                if(!function_exists('sortByName')) {
                    function sortByName($a, $b)
                    {
                        return strcmp($a['name'], $b['name']);

                    }
                }
                uasort($groups[$groupId]['rights'], 'sortByName');
            }
        }
        return $groups;
    }

    public function groupExists($group_code) {
        if($group_code == 'admin') { return true; }

        $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'acc_groups` WHERE `code` = :groupcode;';
        $params = array(
            'groupcode' => $group_code
        );
        $result = $this->Core->Db()->fromDatabase($sql, '@raw', $params);
        return (count($result)) ? true : false;
    }

    public function getGroupByGroupCode($group_code) {
        $group = array();

        if($this->groupExists($group_code)) {
            $groups = $this->getGroups();
//            $group = (array_key_exists($group_code,$groups)) ? $groups[$group_code] : array();
            $group = array();

            foreach($groups as $groupId => $curGroup) {
                if($curGroup['code'] == $group_code) {
                    $group = $curGroup;
                    break;
                }
            }
        }
        return $group;
    }

    public function getUsersByGroupCode($group_code) {
        $users = array();
        $group = $this->getGroupByGroupCode($group_code);
        $groupId = (count($group)) ? $group['id'] : 0;

        if($groupId !== 0){
            $sql = '
              SELECT
                users.*,
                ( SELECT GROUP_CONCAT(id_system_sites) FROM `' . DB_TABLE_PREFIX . 'system_users2sites` WHERE id_acc_users = users.id ) AS sites
              FROM
                `' . DB_TABLE_PREFIX . 'acc_users` AS users
              WHERE
                `id_acc_groups` = :id_acc_groups;
            ';

            $params = array(
                'id_acc_groups' => $groupId
            );
            $result = $this->Core->Db()->fromDatabase($sql,'@raw', $params);

            foreach($result as $row) {
                $users[$row['id']] = $this->_prepareUserData($row);
            }
        }

        return $users;
    }

    public function getDefaultGroup() {
        $defaultGroup = array();
        $groups = $this->getGroups();
        foreach ($groups as $groupId => $group) {
            $isDefaultGroup = (array_key_exists('default', $group) && $group['default']);
            if($isDefaultGroup) {
                $defaultGroup = $group;
                break;
            }
        }
        if(!count($defaultGroup)) {
            $defaultGroup = reset($groups);
        }
        return $defaultGroup;
    }

    public function setDefaultGroup($groupId){
        $site = $this->Core->Sites()->getSite();
        $siteId = (isset($site['id'])) ? $site['id'] : 0;

        $return = false;
        $groups = $this->getGroups();
        if(array_key_exists($groupId, $groups)) {
            $group = $groups[$groupId];
            $defaultSiteIdsArray = (is_array($group) && array_key_exists('defaultSiteIds', $group) && is_array($group['defaultSiteIds'])) ? $group['defaultSiteIds'] : array();

            $newDefaultSiteIdsArray = $defaultSiteIdsArray;

            if(!in_array($siteId,$defaultSiteIdsArray)) {
                $newDefaultSiteIdsArray[] = $siteId;
            }

            $newDefaultSiteIdsString = implode(',',$newDefaultSiteIdsArray);

            if (!$this->resetDefaultGroup()){
                $return = false;
            } else {
                if($groupId > 0) {
                    $setDefaultSql = 'UPDATE `' . DB_CONN_DBNAME . '`.`' . DB_TABLE_PREFIX . 'acc_groups` SET `default`="' . $newDefaultSiteIdsString . '" WHERE `id`=' . $groupId . ';';


                    $setDefaultSqlResult = $this->Core->Db()->toDatabase($setDefaultSql);
                    if (!$setDefaultSqlResult) {
                        $return = false;
                    } else {
                        $return = true;
                    }
                } else {
                    $return = true;
                }
            }
        }
        return $return;
    }

    public function resetDefaultGroup() {
        $site = $this->Core->Sites()->getSite();
        $siteId = (isset($site['id'])) ? $site['id'] : 0;

        $resetCount = 0;

        $groups = $this->getGroups();
        foreach($groups as $groupId => $group) {
            if($groupId == '0') { $resetCount++; continue; }

            $defaultSiteIdsArray = (is_array($group) && array_key_exists('defaultSiteIds', $group) && is_array($group['defaultSiteIds'])) ? $group['defaultSiteIds'] : array();

            $newDefaultSiteIdsArray = $defaultSiteIdsArray;

            if(in_array($siteId,$defaultSiteIdsArray)) {
                if(($del_key = array_search($siteId,$defaultSiteIdsArray)) !== false) {
                    unset($newDefaultSiteIdsArray[$del_key]);
                }
            }

            $newDefaultSiteIdsString = implode(',',$newDefaultSiteIdsArray);

            $resetAllSql = 'UPDATE `' . DB_CONN_DBNAME . '`.`' . DB_TABLE_PREFIX . 'acc_groups` SET `default`="' . $newDefaultSiteIdsString . '" WHERE `id`=' . $groupId . ';';
            $resetAllSqlResult = $this->Core->Db()->toDatabase($resetAllSql);



            if (!$resetAllSqlResult){
                // do nothing
            } else {
                $resetCount++;
            }
        }

        return (count($groups) == $resetCount);
    }

    public function hasAccess($right_code)
    {
        $return = false;
        $user = $this->getUser();
        if(count($user)){
        	
            if($user['su']) {
            	
        		$vag_grouId = (array_key_exists('system_debug_vag', $_SESSION) && is_numeric($_SESSION['system_debug_vag'])) ? $_SESSION['system_debug_vag'] : null;
        		
        		if($vag_grouId === null) {
            		return true; /** SU has all powers \o/ !! */
        		}
            	
        	}

            if(!$this->rightExists($right_code)) { return false; } /** If Right Code not exists, return false! */

            $groups = $this->getGroups();

            $userGroupId = (array_key_exists('id_acc_groups',$user)) ? $user ['id_acc_groups'] : 'noId';

            $getUserGroup = (array_key_exists($userGroupId, $groups))? $groups[$userGroupId] : array();

            $getUserGroupRights = (array_key_exists('rights', $getUserGroup) && is_array($getUserGroup['rights'])) ? $getUserGroup['rights'] : array();

            $return = (array_key_exists($right_code,$getUserGroupRights));

            /*
            $userId = $user['id'];

            $sql = 'SELECT id FROM `' . DB_TABLE_PREFIX . 'acc_rights` WHERE id IN (SELECT id_acc_rights FROM `' . DB_TABLE_PREFIX . 'acc_rights2groups` WHERE id_acc_groups=(SELECT id_acc_groups FROM `' . DB_TABLE_PREFIX . 'acc_users` WHERE id=:userid)) AND code=:rightcode;';

            $params = array(
                'userid' => $userId,
                'rightcode' => $right_code
            );

            $result = $this->Core->Db()->fromDatabase($sql, '@raw', $params);

            $return = (count($result)) ? true : false;
             */
        }
        if(!$return) {
            $this->Core->Mvc()->setForceNoFileCache();
        }
        return $return;
    }

    public function saveRight($id, $data)
    {
        $protectedRights = $this->getProtectedRights();
        $rights = $this->getRights();

        $right = (array_key_exists($id, $rights)) ? $rights[$id] : array();

        $saveNew = (!count($right)) ? true : false;

        $htmlFreeFields = array(
            'code',
            'name'
        );

        $rightSaveData = ($saveNew) ? $data : $right;

        foreach($data as $saveDataKey => $saveDataValue) {
            $saveDataValue = (in_array($saveDataKey,$htmlFreeFields) && !is_array($saveDataValue)) ? trim(strip_tags($saveDataValue)) : $saveDataValue;
            if(array_key_exists($saveDataKey,$rightSaveData)) {
                if(in_array($saveDataKey,$htmlFreeFields)) { $saveDataValue = trim(strip_tags($saveDataValue)); }
                $rightSaveData[$saveDataKey] = $saveDataValue;
            }
        }

        $saveName = (isset($rightSaveData['name']) && $rightSaveData['name'] != '') ? $rightSaveData['name'] : '';
        $saveCode = (isset($rightSaveData['code']) && $rightSaveData['code'] != '') ? $rightSaveData['code'] : '';

        $redirectUrl = $this->Mvc->getModelUrl() . '/group/';

        $success = false;

        if($saveName != '') {
            if($saveNew){
                if($saveCode != '') {

                    if($this->rightExists($saveCode)) {
                        $note  = sprintf( $this->Core->i18n()->translate('Berechtigung mit dem Code \'%s\' existiert bereits!'), $saveCode);
                        $type  = 'danger';
                        $kind  = 'bs-alert';
                        $title = $this->Core->i18n()->translate('Fehler') . '!';

                        $this->Core->setNote($note, $type, $kind, $title);
                        $this->Core->Request()->redirect($redirectUrl);
                    }

                    //MySQL Query für die Erstellung
                    $rightinsertinto = '
						INSERT INTO
						`' . DB_TABLE_PREFIX . 'acc_rights`
						(`code`, `name`)
						VALUES
						(:code, :name)';
                    $params = array(
                        'code' => trim($saveCode),
                        'name' => trim($saveName)
                    );
                    $sqlResult = $this->Core->Db()->toDatabase($rightinsertinto, $params);

                    if ($this->Core->Db()->lastInsertId() == ''){
                        $note  = sprintf( $this->Core->i18n()->translate('MySQL Error: \'%s\''), $sqlResult->errorInfo()[2]);
                        $type  = 'danger';
                        $kind  = 'bs-alert';
                        $title = $this->Core->i18n()->translate('Fehler') . '!';

                        $success = false;
                    }
                    else {
                        $note  = sprintf( $this->Core->i18n()->translate('Berechtigung \'%s\' erfolgreich gespeichert!!'), $saveName);
                        $type  = 'success';
                        $kind  = 'bs-alert';
                        $title = $this->Core->i18n()->translate('Erledigt') . '!';

                        $success = true;
                    }
                    $this->Core->setNote($note, $type, $kind, $title);
                    $this->Core->Request()->redirect($redirectUrl);

                } else {
                    $success = false;
                }
            } else {
                $user = $this->getUser();
                if(in_array($right['code'], $protectedRights) && !$user['su']) {
                    $note  = sprintf( $this->Core->i18n()->translate('Berechtigung kann nicht geändert werden!'), $id);
                    $type  = 'warning';
                    $kind  = 'bs-alert';
                    $title = $this->Core->i18n()->translate('Warnung') . '!';
                    $this->Core->setNote($note, $type, $kind, $title);

                    $this->Core->Request()->redirect($redirectUrl);
                }

                //MySQL Query für die Bearbeitung
                $rightupdate = '
                    UPDATE
                        `'.DB_CONN_DBNAME.'`.`' . DB_TABLE_PREFIX . 'acc_rights`
                    SET
                        `name` = :name
                    WHERE
                        `' . DB_TABLE_PREFIX . 'acc_rights`.`id` = :rightsid;';
                $params = array(
                    'name' => trim($saveName),
                    'rightsid' => $id
                );
                $sqlResult = $this->Core->Db()->toDatabase($rightupdate, $params);

                if (!$sqlResult){
                    $note  = sprintf( $this->Core->i18n()->translate('MySQL Error: \'%s\''), $sqlResult->errorInfo()[2]);
                    $type  = 'danger';
                    $kind  = 'bs-alert';
                    $title = $this->Core->i18n()->translate('Fehler') . '!';

                    $success = false;
                }
                else {
                    $note  = sprintf( $this->Core->i18n()->translate('Berechtigung \'%s\' erfolgreich aktualisiert!!'), $saveName);
                    $type  = 'success';
                    $kind  = 'bs-alert';
                    $title = $this->Core->i18n()->translate('Erledigt') . '!';

                    $success = true;
                }
                $this->Core->setNote($note, $type, $kind, $title);
                $this->Core->Request()->redirect($redirectUrl);
            }
        } else {
            $success = false;
        }

        if(!$success) {
            $note  = $this->Core->i18n()->translate('Konnte Berechtigung nicht speichern!');
            $type  = 'danger';
            $kind  = 'bs-alert';
            $title = $this->Core->i18n()->translate('Fehler') . '!';
            $this->Core->setNote($note, $type, $kind, $title);
            $this->Core->Request()->redirect($redirectUrl);

            $return = array(
                'succes' => false,
                'params' => array(
                    'id' => $id,
                    'data' => $data,
                ),
                'right' => $right,
                'saveName' => $saveName,
                'saveNew' => $saveNew,
                'redirectUrl' => $redirectUrl,
            );

            return $return;
        }
    }

    public function saveGroup($id, $data)
    {
        $user = $this->getUser();

        $site = $this->Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $groups = $this->getGroups();
        $rights = $this->getRights();

        $id = (is_numeric($id)) ? $id : 'noId';
        $group = (array_key_exists($id, $groups)) ? $groups[$id] : array();

        $saveNew = (!count($group)) ? true : false;

        $htmlFreeFields = array(
            'code',
            'name'
        );

        $groupSaveData = ($saveNew) ? $data : $group;
        foreach($data as $saveDataKey => $saveDataValue) {
            $saveDataValue = (in_array($saveDataKey,$htmlFreeFields) && !is_array($saveDataValue)) ? trim(strip_tags($saveDataValue)) : $saveDataValue;
            if(array_key_exists($saveDataKey,$groupSaveData)) {
                if(in_array($saveDataKey,$htmlFreeFields)) { $saveDataValue = trim(strip_tags($saveDataValue)); }
                $groupSaveData[$saveDataKey] = $saveDataValue;
            }
        }

        $saveCode   = (isset($groupSaveData['code']) && $groupSaveData['code'] != '') ? $groupSaveData['code'] : '';
        $saveName   = (isset($groupSaveData['name']) && $groupSaveData['name'] != '') ? $groupSaveData['name'] : '';
        $saveAdmin  = (isset($groupSaveData['admin']) && $groupSaveData['admin'] > 0) ? true : false;
        $saveRights = (isset($data['rights']) && is_array($data['rights'])) ? $data['rights'] : array();

        $redirectUrl = $this->Mvc->getModelUrl() . '/group/';

        $success = false;

        if($saveName != '') {
            if($saveNew){
                if($saveCode != '') {

                    if($this->groupExists($saveCode)) {
                        $note  = sprintf( $this->Core->i18n()->translate('Benutzergruppe mit dem Code \'%s\' existiert bereits!'), $saveCode);
                        $type  = 'danger';
                        $kind  = 'bs-alert';
                        $title = $this->Core->i18n()->translate('Fehler') . '!';

                        $this->Core->setNote($note, $type, $kind, $title);
                        $this->Core->Request()->redirect($redirectUrl);
                    }

                    //MySQL Query für die Erstellung
                    $groupinsertinto = '
                        INSERT INTO `' . DB_TABLE_PREFIX . 'acc_groups`
                            (`code`, `name`, `admin`, `id_system_sites`)
                        VALUES
                            (:code, :name, :admin, :id_system_sites)';
                    $params = array(
                        'code'             => trim($saveCode),
                        'name'             => trim($saveName),
                        'admin'             => $saveAdmin,
                        'id_system_sites' => $siteId
                    );
                    $sqlResult = $this->Core->Db()->toDatabase($groupinsertinto, $params);

                    $groupId = $this->Core->Db()->lastInsertId();

                    if ($groupId == ''){
                        $note  = sprintf( $this->Core->i18n()->translate('MySQL Error: \'%s\''), $sqlResult->errorInfo()[2]);
                        $type  = 'danger';
                        $kind  = 'bs-alert';
                        $title = $this->Core->i18n()->translate('Fehler') . '!';

                        $success = false;
                    }
                    else {
                        $note  = sprintf( $this->Core->i18n()->translate('Benutzergruppe \'%s\' erfolgreich gespeichert!!'), $saveName);
                        $type  = 'success';
                        $kind  = 'bs-alert';
                        $title = $this->Core->i18n()->translate('Erledigt') . '!';

                        $success = true;
                    }
                    $this->Core->setNote($note, $type, $kind, $title);

                    if($success) {
                        /** Insert Information into Rights2Groups Table */
                        $insertedRightsIntoRights2GroupsTable_Counter = 0;
                        foreach ($saveRights as $rightId) {
                            $rightId = (int)$rightId;
                            if ($rightId !== 0 && $this->insertIntoRights2Groups((int)$rightId, (int)$groupId)) {
                                $insertedRightsIntoRights2GroupsTable_Counter++;
                            }
                        }
                        if($insertedRightsIntoRights2GroupsTable_Counter >= 1) {
                            $iir2gResult = (count($saveRights) > $insertedRightsIntoRights2GroupsTable_Counter) ? $insertedRightsIntoRights2GroupsTable_Counter . '/'  . count($saveRights) : '100%';
                            $note = sprintf($this->Core->i18n()->translate('Berechtigungs-Zuordnungen (%s) gespeichert!!'), $iir2gResult);
                            $type = 'success';
                            $kind = 'bs-alert';
                            $title = $this->Core->i18n()->translate('Erledigt') . '!';
                            $this->Core->setNote($note, $type, $kind, $title);
                        } else {
                            $note = sprintf($this->Core->i18n()->translate('Berechtigungs-Zuordnungen zur Benutzergruppe \'%s\' konnten nicht gespeichert werden!!'), $saveName);
                            $type = 'danger';
                            $kind = 'bs-alert';
                            $title = $this->Core->i18n()->translate('Fehler') . '!';
                            $this->Core->setNote($note, $type, $kind, $title);
                        }
                    }

                    $this->Core->Request()->redirect($redirectUrl);

                } else {
                    $success = false;
                }
            } else {

                $groupId = $group['id'];

                if(($group['siteId'] === 0 || $user['id_acc_groups'] === $groupId) && !$user['su']) { /** SU has all powers \o/ !! */
                    $note  = sprintf( $this->Core->i18n()->translate('Benutzergruppe kann nicht geändert werden!'), $id);
                    $type  = 'warning';
                    $kind  = 'bs-alert';
                    $title = $this->Core->i18n()->translate('Warnung') . '!';
                    $this->Core->setNote($note, $type, $kind, $title);

                    $this->Core->Request()->redirect($redirectUrl);
                }

                //MySQL Query für die Bearbeitung
                $userupdate = "UPDATE `".DB_CONN_DBNAME."`.`" . DB_TABLE_PREFIX . "acc_groups` SET `name` = '" . trim($saveName) . "', `admin` = '" . $saveAdmin . "' WHERE `" . DB_TABLE_PREFIX . "acc_groups`.`id` = '" . $id . "'";
                $sqlResult = $this->Core->Db()->toDatabase($userupdate);

                if (!$sqlResult){
                    $note  = sprintf( $this->Core->i18n()->translate('MySQL Error: \'%s\''), $sqlResult->errorInfo()[2]);
                    $type  = 'danger';
                    $kind  = 'bs-alert';
                    $title = $this->Core->i18n()->translate('Fehler') . '!';

                    $success = false;
                }
                else {
                    $note  = sprintf( $this->Core->i18n()->translate('Benutzergruppe \'%s\' erfolgreich aktualisiert!!'), $saveName);
                    $type  = 'success';
                    $kind  = 'bs-alert';
                    $title = $this->Core->i18n()->translate('Erledigt') . '!';

                    $success = true;
                }
                $this->Core->setNote($note, $type, $kind, $title);

                if($success) {
                    $rightsToInsert = array();
                    $rightsToDelete = array();

                    foreach($saveRights as $rightCode) {
                        if(!array_key_exists($rightCode, $group['rights'])) {
                            $rightsToInsert[] = $rightCode;
                        }
                    }
                    foreach($group['rights'] as $rightCode => $right) {
                        if(!in_array($rightCode, $saveRights)) {
                            $rightsToDelete[] = $rightCode;
                        }
                    }

                    if(count($rightsToInsert)) {
                        /** Insert Information into Rights2Groups Table */
                        $insertedRightsIntoRights2GroupsTable_Counter = 0;
                        foreach ($rightsToInsert as $rightCode) {
                            if(array_key_exists($rightCode,$rights)) {
                                if ($this->insertIntoRights2Groups($rightCode, $groupId)) {
                                    $insertedRightsIntoRights2GroupsTable_Counter++;
                                }
                            }
                        }
                        if($insertedRightsIntoRights2GroupsTable_Counter >= 1) {
                            $iir2gResult = (count($saveRights) > $insertedRightsIntoRights2GroupsTable_Counter) ? $insertedRightsIntoRights2GroupsTable_Counter . '/'  . count($saveRights) : '100%';
                            $note = sprintf($this->Core->i18n()->translate('Berechtigungs-Zuordnungen (%s) gespeichert!!'), $iir2gResult);
                            $type = 'success';
                            $kind = 'bs-alert';
                            $title = $this->Core->i18n()->translate('Erledigt') . '!';
                            $this->Core->setNote($note, $type, $kind, $title);
                        } else {
                            $note = sprintf($this->Core->i18n()->translate('Berechtigungs-Zuordnungen zur Benutzergruppe \'%s\' konnten nicht gespeichert werden!!'), $saveName);
                            $type = 'danger';
                            $kind = 'bs-alert';
                            $title = $this->Core->i18n()->translate('Fehler') . '!';
                            $this->Core->setNote($note, $type, $kind, $title);
                        }
                    }


                    if(count($rightsToDelete)) {
                        $deletedRightsIntoRights2GroupsTable_Counter = 0;
                        foreach($rightsToDelete as $rightCode) {
                            /** Delete Right from Rights2Groups Table */
                            if ($this->deleteFromRights2Groups($rightCode, 'right', $groupId)) {
                                $deletedRightsIntoRights2GroupsTable_Counter++;
                            }
                        }
                        if($deletedRightsIntoRights2GroupsTable_Counter >= 1) {
                            $dr2gResult = (count($rightsToDelete) > $deletedRightsIntoRights2GroupsTable_Counter) ? $deletedRightsIntoRights2GroupsTable_Counter . '/'  . count($rightsToDelete) : '100%';
                            $note = sprintf($this->Core->i18n()->translate('Berechtigungs-Zuordnungen (%s) gelöscht!!'), $dr2gResult);
                            $type = 'success';
                            $kind = 'bs-alert';
                            $title = $this->Core->i18n()->translate('Erledigt') . '!';
                            $this->Core->setNote($note, $type, $kind, $title);
                        } else {
                            $note = sprintf($this->Core->i18n()->translate('Berechtigungs-Zuordnungen zur Benutzergruppe \'%s\' konnten nicht gelöscht werden!!'), $saveName);
                            $type = 'danger';
                            $kind = 'bs-alert';
                            $title = $this->Core->i18n()->translate('Fehler') . '!';
                            $this->Core->setNote($note, $type, $kind, $title);
                        }
                    }
                }

                $usersByGroupCode = $this->getUsersByGroupCode($group['code']);
                if(count($usersByGroupCode)) {
                    $forceLogoutUsers = array();
                    foreach ($usersByGroupCode as $userId => $user) {
                        $isOnline = $this->userLoggedIn($userId);
                        if($isOnline) {
                            $forceLogoutUsers[$userId] = $user;
                        }
                    }
                    if(count($forceLogoutUsers)) {
                        foreach ($forceLogoutUsers as $user) {
                            $this->Core->Hooks()->fire('user_may_logout', array($user['id'], 'acc_change_group'));
                        }
                    }
                }

                $this->Core->Request()->redirect($redirectUrl);
            }
        } else {
            $success = false;
        }

        if(!$success) {
            $note  = $this->Core->i18n()->translate('Konnte Benutzergruppe nicht speichern!');
            $type  = 'danger';
            $kind  = 'bs-alert';
            $title = $this->Core->i18n()->translate('Fehler') . '!';
            $this->Core->setNote($note, $type, $kind, $title);
            $this->Core->Request()->redirect($redirectUrl);

            $return = array(
                'succes' => false,
                'params' => array(
                    'id' => $id,
                    'data' => $data,
                ),
                'group' => $group,
                'saveName' => $saveName,
                'saveNew' => $saveNew,
                'redirectUrl' => $redirectUrl,
            );

            return $return;
        }
    }

    public function deleteGroup($id)
    {
        $groups = $this->getGroups();
        $group = (array_key_exists($id, $groups)) ? $groups[$id] : array();

        $redirectUrl = $this->Mvc->getModelUrl() . '/group/';

        if(count($group)) {

            if($group['siteId'] === 0) {
                $note  = sprintf( $this->Core->i18n()->translate('Benutzergruppe kann nicht gelöscht werden!'), $id);
                $type  = 'warning';
                $kind  = 'bs-alert';
                $title = $this->Core->i18n()->translate('Warnung') . '!';
                $this->Core->setNote($note, $type, $kind, $title);

                $this->Core->Request()->redirect($redirectUrl);
            }

            $delGroup = $group;
            $groupDeleteSql = "DELETE FROM `" . DB_TABLE_PREFIX . "acc_groups` WHERE id = '".$id."' LIMIT 1";
            $sqlResult = $this->Core->Db()->toDatabase($groupDeleteSql);
            if (!$sqlResult){
                $note  = sprintf( $this->Core->i18n()->translate('MySQL Error: \'%s\''), $this->Core->Db()->errorInfo()[2]);
                $type  = 'danger';
                $kind  = 'bs-alert';
                $title = $this->Core->i18n()->translate('Fehler') . '!';
                $this->Core->setNote($note, $type, $kind, $title);
            }
            else {
                $note  = sprintf( $this->Core->i18n()->translate('Benutzergruppe \'%s\' permanent gelöscht!!'), $delGroup['name']);
                $type  = 'success';
                $kind  = 'bs-alert';
                $title = $this->Core->i18n()->translate('Erledigt') . '!';
                $this->Core->setNote($note, $type, $kind, $title);
            }

            /** Delete Group from Rights2Groups Table */
            $deleteFromRights2groups = $this->deleteFromRights2Groups($id, 'group');
            if ($deleteFromRights2groups) {
                $note = sprintf($this->Core->i18n()->translate('Berechtigungs-Zuornung \'%s\' konnte nicht gelöscht werden!!'), $delGroup['name'] . ' (Group)');
                $type = 'danger';
                $kind = 'bs-alert';
                $title = $this->Core->i18n()->translate('Fehler') . '!';
                $this->Core->setNote($note, $type, $kind, $title);
            } else {
                $note = sprintf($this->Core->i18n()->translate('Berechtigungs-Zuornung \'%s\' permanent gelöscht!!'), $delGroup['name'] . ' (Group)');
                $type = 'success';
                $kind = 'bs-alert';
                $title = $this->Core->i18n()->translate('Erledigt') . '!';
                $this->Core->setNote($note, $type, $kind, $title);
            }

            $usersByGroupCode = $this->getUsersByGroupCode($group['code']);
            if(count($usersByGroupCode)) {
                $forceLogoutUsers = array();
                foreach ($usersByGroupCode as $userId => $user) {
                    $isOnline = $this->userLoggedIn($userId);
                    if($isOnline) {
                        $forceLogoutUsers[$userId] = $user;
                    }
                }
                if(count($forceLogoutUsers)) {
                    foreach ($forceLogoutUsers as $user) {
                        $this->Core->Hooks()->fire('user_may_logout', array($user['id'], 'acc_delete_group'));
                    }
                }
            }
        } else {
            $note  = sprintf( $this->Core->i18n()->translate('Benutzergruppe mit der ID \'%s\' konnte nicht gefunden werden...'), $id);
            $type  = 'danger';
            $kind  = 'bs-alert';
            $title = $this->Core->i18n()->translate('Fehler') . '!';
            $this->Core->setNote($note, $type, $kind, $title);
        }

        $this->Core->Request()->redirect($redirectUrl);
    }

    public function deleteRight($id)
    {
        $protectedRights = $this->getProtectedRights();
        $rights = $this->getRights();
        $right = $this->getRightById($id);

        if(count($right)) {
            if(!in_array($right['code'], $protectedRights)) {
                $delRight = $right;

                /** Delete Right from Rights Table */
                $rightDeleteSql = "DELETE FROM `" . DB_TABLE_PREFIX . "acc_rights` WHERE id = '" . $id . "' LIMIT 1";
                $sqlResult = $this->Core->Db()->toDatabase($rightDeleteSql);
                if (!$sqlResult) {
                    $note = sprintf($this->Core->i18n()->translate('MySQL Error: \'%s\''), $this->Core->Db()->errorInfo()[2]);
                    $type = 'danger';
                    $kind = 'bs-alert';
                    $title = $this->Core->i18n()->translate('Fehler') . '!';
                    $this->Core->setNote($note, $type, $kind, $title);
                } else {
                    $note = sprintf($this->Core->i18n()->translate('Berechtigung \'%s\' permanent gelöscht!!'), $delRight['name']);
                    $type = 'success';
                    $kind = 'bs-alert';
                    $title = $this->Core->i18n()->translate('Erledigt') . '!';
                    $this->Core->setNote($note, $type, $kind, $title);
                }

                /** Delete Right from Rights2Groups Table */
                $deleteFromRights2groups = $this->deleteFromRights2Groups($id, 'right');
                if ($deleteFromRights2groups) {
                    $note = sprintf($this->Core->i18n()->translate('Berechtigungs-Zuornung \'%s\' konnte nicht gelöscht werden!!'), $delRight['name']) . ' (Right)';
                    $type = 'danger';
                    $kind = 'bs-alert';
                    $title = $this->Core->i18n()->translate('Fehler') . '!';
                    $this->Core->setNote($note, $type, $kind, $title);
                } else {
                    $note = sprintf($this->Core->i18n()->translate('Berechtigungs-Zuornung \'%s\' permanent gelöscht!!'), $delRight['name'] . ' (Right)');
                    $type = 'success';
                    $kind = 'bs-alert';
                    $title = $this->Core->i18n()->translate('Erledigt') . '!';
                    $this->Core->setNote($note, $type, $kind, $title);
                }
            } else {
                $note  = sprintf( $this->Core->i18n()->translate('Berechtigung kann nicht gelöscht werden!'), $id);
                $type  = 'warning';
                $kind  = 'bs-alert';
                $title = $this->Core->i18n()->translate('Warnung') . '!';
                $this->Core->setNote($note, $type, $kind, $title);
            }
        } else {
            $note  = sprintf( $this->Core->i18n()->translate('Berechtigung mit der ID \'%s\' konnte nicht gefunden werden...'), $id);
            $type  = 'danger';
            $kind  = 'bs-alert';
            $title = $this->Core->i18n()->translate('Fehler') . '!';
            $this->Core->setNote($note, $type, $kind, $title);
        }

        $redirectUrl = $this->Mvc->getModelUrl() . '/group/';
        $this->Core->Request()->redirect($redirectUrl);
    }

    public function insertIntoRights2Groups($rightIdentifier, $groupId) {
        $site = $this->Core->Sites()->getSite();
        $groups = $this->getGroups();

        $right = (!is_int($rightIdentifier)) ? $this->getRightByCode($rightIdentifier) : $this->getRightById($rightIdentifier);

        if(!is_int($groupId) || $groupId < 1 || !array_key_exists($groupId, $groups) || !count($right)) {
            return false;
        }

        $rightCode = $right['code'];

        /** Insert into Rights2Groups Table */
        $rights2groupsInsertSql = "INSERT INTO `" . DB_TABLE_PREFIX . "acc_rights2groups` (`acc_rights_code`, `id_acc_groups`, `id_acc_users__changedBy`) VALUES ('" . $rightCode . "', '" . (int)$groupId . "', '". (int)$site['id'] ."')";
        $sqlResult = $this->Core->Db()->toDatabase($rights2groupsInsertSql);

        return ($this->Core->Db()->lastInsertId() == '') ? false : true;
    }

    public function deleteFromRights2Groups($identifier, $type, $idGroup = null) {
        $rights = $this->getRights();
        $groups = $this->getGroups();

        if((is_int($identifier) && $identifier < 1) || ($type != 'right' && $type != 'group') || ($idGroup !== null && !is_int($idGroup))) {
            return false;
        }

        $whereStatement = '';

        if($type == 'right') {
            if(!array_key_exists($identifier, $rights) || $idGroup === null || !array_key_exists($idGroup, $groups))  {
                return false;
            }
            $whereColumn = 'acc_rights_code';
            $whereStatement = $whereColumn . " = '" . $identifier . "' AND id_acc_groups = '" . $idGroup . "'";

            $whereStatement = " WHERE " . $whereStatement;
        } else {
            if(!array_key_exists($identifier, $groups)) {
                return false;
            }
            $whereColumn = 'id_acc_groups';
            $whereStatement = " WHERE " . $whereColumn . " = '" . $identifier . "'";
        }

        if($whereStatement === '') {
            return false;
        }

        /** Delete from Rights2Groups Table */
        $rights2groupsDeleteSql = "DELETE FROM `" . DB_TABLE_PREFIX . "acc_rights2groups`" . $whereStatement;
        $sqlResult = $this->Core->Db()->toDatabase($rights2groupsDeleteSql, array(), false, true);
        if (!$sqlResult) {
            return false;
        } else {
            return true;
        }
    }
    private function _searchRightStringsRecursiveInFiles($dir = null) {
        if($dir === null) {
            $dir = $this->Core->getCoreRelPath() . DS;
        }

        if($this->_searchRightStringsResult === null) {
            $this->_searchRightStringsResult = array();
        }

        $handler = opendir($dir);
        while ($file = readdir($handler)) {
            if ($file != "." && $file != "..") {
                $fullFilePath = $dir . $file;
                if(is_dir($fullFilePath)) {
                    $this->_searchRightStringsRecursiveInFiles($fullFilePath . DS);
                } else {
                    if (strpos($file, '.php') !== false) {
                        $fileContents = file_get_contents($fullFilePath);
                        if($fileContents !== false) {
                            $regex = '/->hasAccess\(\'(.+?)\'\)/is';
                            preg_match_all($regex, $fileContents, $matches);
                            if(is_array($matches) && count($matches) && isset($matches[1]) && count($matches[1])) {
                                $matchesUnique = array_unique($matches[1]);
                                $this->_searchRightStringsResult[str_replace(JF_ROOT_DIR,'',$fullFilePath)] = $matchesUnique;
                            }
                        }
                    }
                }
            }
        }
    }

    public function getFileRightStrings($cleaned = true) {
        $searchRightStringsResult = $this->_searchRightStringsResult;

        if($searchRightStringsResult === null) {
            $this->_searchRightStringsRecursiveInFiles();
            $searchRightStringsResult = $this->_searchRightStringsResult;
        }

        if($cleaned) {
            $resultsCleaned = array();
            foreach ($searchRightStringsResult as $file => $strings) {
                foreach ($strings as $string) {
                    $formattedString = $string;
                    $formattedString = stripslashes($formattedString);
                    // $formattedString = htmlentities($formattedString);
                    $formattedString = trim($formattedString);

                    $resultsCleaned[] = $formattedString;
                }
            }
            $resultsCleanedUnique = array_unique($resultsCleaned);
            sort($resultsCleanedUnique);
            $searchRightStringsResult = $resultsCleanedUnique;
        }

        return $searchRightStringsResult;
    }

    public function getMissingRights() {
        $missingRights = $this->_missingRights;
        if($missingRights === null) {
            $this->_searchRightStringsRecursiveInFiles();
            $missingRights = array();
            $resultsFinal = $this->getFileRightStrings();

            foreach($resultsFinal as $rightString) {
                if (!$this->rightExists($rightString)) {
                    $missingRights[] = $rightString;
                }
            }

            $this->_missingRights = $missingRights;
        }
        return $missingRights;
    }

    public function shell($commandArgsArray) {

        /** @var $Shell Shell */
        $Shell = $this->Core->Shell();

        /** @var $Sites Sites */
        $Sites = $this->Core->Sites();
        $curSite = (is_object($Sites) && is_array($Sites->getSite())) ? $Sites->getSite() : array();
        $curSiteUrlKey = (count($curSite) && array_key_exists('urlKey',$curSite)) ? ' ' . $curSite['urlKey'] : '';

        $helpOutput = '/** Acc Help */' . PHP_EOL;
        $helpOutput .= 'For a List of Commands type "$ php shell.php' . $curSiteUrlKey . ' acc --list"' . PHP_EOL . PHP_EOL;

        $helpOutput = $Shell->getColoredString($helpOutput, 'green');

        $commandModelKey = (array_key_exists(0, $commandArgsArray)) ? $commandArgsArray[0] : '';

        $command = (array_key_exists(1, $commandArgsArray)) ? $commandArgsArray[1] : '';
        $action  = (array_key_exists(2, $commandArgsArray)) ? $commandArgsArray[2] : '';

        $areas = array(
            '--help' => 'For Acc Basic Help',
            '--list' => 'To see this List',
            '--user' => 'User Area',
        );

        $shellOutput = '';

        switch ($command) {
            case '--help':
                $shellOutput .= $helpOutput;
                break;
            case '--list':
                $listOutput = '/** Acc Command List */' . PHP_EOL;
                foreach($areas as $area => $areaText) {
                    $listOutput .= '  * ' . $area . ' /** ' . $areaText . ' */' . PHP_EOL;
                }
                $shellOutput .= $Shell->getColoredString($listOutput , 'yellow');
                break;
            default:
                if($command === '') {
                    $shellOutput .= $helpOutput;
                }
                if(!array_key_exists($command,$areas)) {
                    $shellOutput .= $Shell->getColoredString('/** Acc Command "' . $command . '" not found... */' . PHP_EOL . PHP_EOL , 'red');
                }
        }

        if (array_key_exists($command,$areas)) {

            $area = $command;

            if ($area == '--user') {
                $actionArray = array(
                    '-list' => 'To see Sub Command List from this Area',
                    '-login' => 'Login with E-Mail and Password, i.e. "$ php shell.php acc --user -login john.doe@example.com \'********\'"',
                    '-isLoggedIn' => 'Check Login State',
                    '-logout' => 'Logout'
                );

                $shellOutput .= '/** Acc User Area */' . PHP_EOL;

                $actionHelpOutput = '=> Help' . PHP_EOL;
                $actionHelpOutput .= '   For a List of Action Commands for this Area type "$ php shell.php' . $curSiteUrlKey . ' acc ' . $action . ' -list"' . PHP_EOL . PHP_EOL;

                $actionHelpOutput = $Shell->getColoredString($actionHelpOutput, 'green');

                if ($action !== '') {

                    switch ($action) {
                        case '-list':
                            $shellOutput .= '=> Acc Action Command List:' . PHP_EOL;
                            foreach ($actionArray as $actionKey => $actionText) {
                                $shellOutput .= '  * ' . $actionKey . ' /** ' . $actionText . ' */' . PHP_EOL;
                            }
                            break;
                        default:
                            if ($action === '') {
                                $shellOutput .= $actionHelpOutput;
                            }
                            if (!array_key_exists($action, $actionArray)) {
                                $errorOutput = '=> Acc Area Action Command "' . $action . '" not found... */' . PHP_EOL;
                                $shellOutput .= $Shell->getColoredString($errorOutput,'red') . PHP_EOL;
                            }
                    }

                    if (!array_key_exists($action, $actionArray)) {
                        $errorOutput = '=> Action Command "' . $action . '" not found...' . PHP_EOL;
                        $shellOutput .= $Shell->getColoredString($errorOutput,'red') . PHP_EOL;
                    } else {

                        if($action == '-login') {
                            $userEmail = (array_key_exists(3, $commandArgsArray)) ? $commandArgsArray[3] : '';
                            $userPass  = (array_key_exists(4, $commandArgsArray)) ? $commandArgsArray[4] : '';

                            $loginOutput = '=> Login' . PHP_EOL;
                            $loginOutput .= '  * User E-Mail: "' . $userEmail . '"' . PHP_EOL;
                            $loginOutput .= '  * User Pass: "********"' . PHP_EOL;

                            $loginData = array();
                            $loginData['mail'] = $userEmail;
                            $loginData['pass'] = $userPass;
                            $loginData['notify'] = false;
                            $loginData['redirect'] = false;

                            $loginResult = $this->userLogin($loginData);

                            $login_success = (is_array($loginResult) && array_key_exists('success',$loginResult)) ? $loginResult['success'] : false;
                            $login_message = (is_array($loginResult) && array_key_exists('message',$loginResult)) ? $loginResult['message'] : '';

                            if($login_success) {
                                $loginResultText = PHP_EOL . '=> ' . $login_message;
                                $loginOutput .= $Shell->getColoredString($loginResultText,'green');
                            } else {
                                $loginResultText = PHP_EOL . '=> ' . $login_message;
                                $loginOutput .= $Shell->getColoredString($loginResultText,'red');
                            }

                            $shellOutput .= $loginOutput;
                        }

                        if($action == '-isLoggedIn') {

                            $curUser = $this->getUser();

                            $isLoggedIn = '=> Is User Logged In' . PHP_EOL;

                            if(!count($curUser) || !$this->userLoggedIn()) {
                                $isLoggedInText = '=> You are not logged in!' . PHP_EOL;
                                $isLoggedIn .= $Shell->getColoredString($isLoggedInText,'red');
                            } else {
                                $userName = (array_key_exists('name', $curUser)) ? $curUser['name'] : '';
                                $userSurName = (array_key_exists('surname', $curUser)) ? $curUser['surname'] : '';
                                $isLoggedInText = '=> You are logged in as ' . $userName . ' ' . $userSurName;
                                $isLoggedIn .= $Shell->getColoredString($isLoggedInText,'green');
                            }

                            $shellOutput .= $isLoggedIn;
                        }

                        if($action == '-logout') {
                            $logoutOutput = '=> User Logout' . PHP_EOL;

                            $curUser = $this->getUser();
                            if(!count($curUser) || !$this->userLoggedIn()) {
                                $logoutText = '=> You are not logged in!' . PHP_EOL;
                                $logoutOutput .= $Shell->getColoredString($logoutText, 'red');
                            } else {
                                $logoutResult = $this->userLogout(null, false, false);
                                if($logoutResult) {
                                    $logoutText = '=> You are now logged out!' . PHP_EOL;
                                    $logoutOutput .= $Shell->getColoredString($logoutText, 'green');
                                } else {
                                    $logoutText = '=> Logout not successful!' . PHP_EOL;
                                    $logoutOutput .= $Shell->getColoredString($logoutText, 'red');
                                }
                            }

                            $shellOutput .= $logoutOutput;
                        }

                    }

                } else {
                    $shellOutput .= $actionHelpOutput;
                }
            }
        }

        return $shellOutput . PHP_EOL;
    }

}