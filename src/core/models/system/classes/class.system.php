<?php
/**
 * System Model
 *
 * @project J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright 2008-2017 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** System Class */
class System {
    private $_version = '4.0.1';

    /**
     * @var $Core Core
     * @var $Mvc Mvc
     */
    private $Core;
    private $Mvc;

    /**
     * Bayman constructor.
     * @param $Core Core
     */
    function __construct($Core)
    {
        $this->Core = $Core;
        $this->Mvc = $Core->Mvc();

        /** @var $Helper Helper */
        $Helper = $this->Core->Helper();

        if(!$Helper->isCli()) {
            $this->_mayRedirectToSiteChooser();
            $this->_mayRedirectToHomePath();
            $this->_mayRedirectFromSystemAreaIfNotSu();
        }

		$this->_setMvcPageHeadMeta();
		$this->_setMvcPageAfterBodyStart();
		$this->_setMvcPageBeforeBodyEnds();
    }

    public function getVersion() {
        return $this->_version;
    }

    public function getDbVersion() {
        $dbVersion_option = $this->Core->Config()->get('system_version',0);
        $dbVersion = ($dbVersion_option !== '') ? $dbVersion_option : 0;
        return $dbVersion;
    }

    /** Update Methods */
    public function _update100($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '1.0.0';
        $return['changelog'] = 'Implemented System Model';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true; /** Just for Initialisation! */
        }

        if($return['success']) {
            unset($return['error']);
        }

        return $return;
    }

    public function _update130($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '1.3.0';
        $return['changelog'] = 'Added Acc Public Access Methods for i18n Ajax Calls';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        if($return['success']) {
            unset($return['error']);
        }

        return $return;
    }

    public function _update135($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '1.3.5';
        $return['changelog'] = 'Added Changelog Output Modal';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        if($return['success']) {
            unset($return['error']);
        }

        return $return;
    }

    public function _update136($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '1.3.6';
        $return['changelog'] = 'Maintenance Mode Methods added';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        if($return['success']) {
            unset($return['error']);
        }

        return $return;
    }

    public function _update140($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '1.4.0';
        $return['changelog'] = 'Debug Areas added';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update141($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '1.4.1';
        $return['changelog'] = 'Access Control implemented';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update145($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '1.4.5';
        $return['changelog'] = 'SU Admin Bar added';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update147($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '1.4.7';
        $return['changelog'] = 'Fixed Model Updates Count Check, Added AYS Text to SU Admin Bar when click on Maintenance activate.';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update150($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '1.5.0';
        $return['changelog'] = 'Added better Error Handling to Update Controller and enhanced Update Error Note.';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update155($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '1.5.5';
        $return['changelog'] = 'Enhanced Changelog Output.';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update200($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '2.0.0';
        $return['changelog'] = 'Implemented File / Database Translation Admin GUI';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update210($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '2.1.0';
        $return['changelog'] = 'Enhanced Error Message on updater action.';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update220($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '2.2.0';
        $return['changelog'] = 'Fixed redirecting to Site Chooser while Password Saving on firstLogin';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update225($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '2.2.5';
        $return['changelog'] = 'Enhanced Theme Support with SU Admin Bar';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update300($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '3.0.0';
        $return['changelog'] = 'Added LogViewer';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update340($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '3.4.0';
        $return['changelog'] = 'Added System Config -> Beta!';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update400($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '4.0.0';
        $return['changelog'] = 'Added Usergroup View - Debug';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update401($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Core->Mvc();

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '4.0.1';
        $return['changelog'] = 'Some SU Admin Bar Mobile enhancements';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }


    /** Access Control */

    private function _mayRedirectToSiteChooser() {
        /**
         * @var $Core Core
         * @var $Mvc Mvc
         * @var $requestClass Request
         * @var $sitesClass Sites
         */
        $Mvc          = $this->Mvc;
        $requestClass = $this->Core->Request();
        $sitesClass   = $this->Core->Sites();

        /** Get User from Session */
        $getUserFromSession = (array_key_exists('user', $_SESSION) && is_array($_SESSION['user'])) ? $_SESSION['user'] : array();

        /** Get Site from Session */
        $sessSite = (array_key_exists('site',$_SESSION) && is_array($_SESSION['site'])) ? $_SESSION['site'] : array();

        $site = $sitesClass->getSite();
        $siteArray = (is_array($site) && count($site)) ? $site : $sessSite;

        if(
            !count($siteArray) &&
            count($getUserFromSession) &&
            $Mvc->getActionKey() != 'login' &&
            $Mvc->getActionKey() != 'logout' &&
            $Mvc->getActionKey() != 'firstLogin' &&
            $Mvc->getActionKey() != 'save' &&
            !$requestClass->isAjax()
        ) {
            $redirectUrl = $Mvc->getModelUrl('system') . '/sites/chooser';

            if($requestClass->getCurrUrl() != $redirectUrl) {
                $requestClass->redirect($redirectUrl);
            }
        }
    }

    private function _mayRedirectToHomePath() {
        /**
         * @var $Core Core
         * @var $Mvc Mvc
         * @var $requestClass Request
         * @var $configClass Config
         * @var $i18nClass i18n
         * @var $sitesClass Sites
         * @var $accClass Acc
         */
        $Core         = $this->Core;
        $Mvc          = $Core->Mvc();
        $requestClass = $Core->Request();
        $configClass  = $Core->Config();
        $sitesClass   = $Core->Sites();
        $i18nClass    = $Core->i18n();

        /** Get Site from Session */
        $sessSite = (array_key_exists('site',$_SESSION) && is_array($_SESSION['site'])) ? $_SESSION['site'] : array();

        $site = $sitesClass->getSite();
        $siteArray = (is_array($site) && count($site)) ? $site : $sessSite;

        /** Redirect to Home Path if set */
        $homePath = $configClass->get('home_path');

        $isIndexPage = ($Mvc->getModelKey() == 'index' && $Mvc->getControllerKey() == 'index' && $Mvc->getActionKey() == 'index');

        if(
            !$requestClass->isAjax() &&
            $isIndexPage &&
            count($siteArray) &&
            $homePath !== ''
        ) {
            $siteBaseUrl = $sitesClass->getSiteBaseUrl();

            $currLangUrlKey = $i18nClass->getCurrLangUrlKey();
            $langUrlSlug = ($currLangUrlKey !== '') ? $currLangUrlKey . '/' : '';

            $redirectUrl = $siteBaseUrl . $langUrlSlug . $homePath;

            if($requestClass->getCurrUrl() != $redirectUrl) {
                $requestClass->redirect($redirectUrl);
            }
        }
    }

    private function _mayRedirectFromSystemAreaIfNotSu() {
        /**
         * @var $Core Core
         * @var $Mvc Mvc
         * @var $requestClass Request
         */
        $Core         = $this->Core;
        $Mvc          = $this->Mvc;
        $requestClass = $this->Core->Request();

        /** Get User from Session */
        $getUserFromSession = (array_key_exists('user', $_SESSION) && is_array($_SESSION['user'])) ? $_SESSION['user'] : array();
        $isSu = (count($getUserFromSession) && array_key_exists('su', $getUserFromSession) && $getUserFromSession['su']);

        $isDebug   = ($Mvc->getModelKey() == 'system' && $Mvc->getControllerKey() == 'debug');
        $isUpdater = ($Mvc->getModelKey() == 'system' && $Mvc->getControllerKey() == 'updater');

        if(!$requestClass->isAjax() && $isDebug && $isUpdater && !$isSu) {
            $requestClass->redirect($Core->getBaseUrl());
        }
    }

    /** MVC Output Methods */

    private function _setMvcPageHeadMeta()
    {
        $getPageHeadMeta = $this->Mvc->getPageHeadMeta();
        
        if(strpos($getPageHeadMeta, 'System Head Meta') === false) {
            $incUrl = $this->Core->getCoreUrl() . '/models/system/inc';

            $mvcPageHeadMeta = PHP_EOL . '<!-- System Head Meta START -->';

            $mvcPageHeadMeta .= PHP_EOL;
            $mvcPageHeadMeta .= '<link rel="stylesheet" type="text/css" href="' . $incUrl . '/system.css">';

            $mvcPageHeadMeta .= PHP_EOL . '<!-- System Head Meta END -->';
            $mvcPageHeadMeta .= PHP_EOL;

            $this->Core->Mvc()->addPageHeadMeta($mvcPageHeadMeta);
        }
    }

    private function _setMvcPageAfterBodyStart()
    {
        $getPageAfterBodyStart = $this->Mvc->getPageAfterBodyStart();

        /** Get User from Session */
        $getUserFromSession = (array_key_exists('user', $_SESSION) && is_array($_SESSION['user'])) ? $_SESSION['user'] : array();
        $isSu = (count($getUserFromSession) && array_key_exists('su', $getUserFromSession) && $getUserFromSession['su']);

        if($isSu) {
            if(strpos($getPageAfterBodyStart, 'System After Body Start') === false) {
                $incUrl = $this->Core->getCoreUrl() . '/models/system/inc';

                $mvcPageAfterBodyStart = PHP_EOL . '<!-- System After Body Start START -->';

                $mvcPageAfterBodyStart .= PHP_EOL;
                $mvcPageAfterBodyStart .= $this->getSuAdminBar();

                $mvcPageAfterBodyStart .= PHP_EOL . '<!-- System After Body Start END -->';
                $mvcPageAfterBodyStart .= PHP_EOL;

                $this->Core->Mvc()->addPageAfterBodyStart($mvcPageAfterBodyStart);
            }
        }
    }

    private function _setMvcPageBeforeBodyEnds()
    {
        $getPageBeforeBodyEnd = $this->Mvc->getPageBeforeBodyEnd();
		
        if(strpos($getPageBeforeBodyEnd, 'System Before Body Ends') === false) {
            $incUrl = $this->Core->getCoreUrl() . '/models/system/inc';

            $mvcPageBeforeBodyEnd = PHP_EOL . '<!-- System Before Body Ends START -->';

            $mvcPageBeforeBodyEnd .= PHP_EOL;
            $mvcPageBeforeBodyEnd .= '<script src="' . $incUrl . '/system.js"></script>';

            $mvcPageBeforeBodyEnd .= PHP_EOL . '<!-- System Before Body Ends END -->';
            $mvcPageBeforeBodyEnd .= PHP_EOL;

            $this->Core->Mvc()->addPageBeforeBodyEnd($mvcPageBeforeBodyEnd);
        }
    }

    public function getSuAdminBar() {
        global $Core, $Mvc, $systemClass;
        $Core = $this->Core;
        $Mvc = $this->Mvc;
        $systemClass = $this;

        $suAdminBarViewFile = $Core->getCoreRelPath() . DS . 'models' . DS . 'system' . DS . 'views' . DS . 'view.suAdminBar.php';

        $return = '';
        $ob_return = '';
        if(file_exists($suAdminBarViewFile) !== false) {
            try {
                ob_start();
                include($suAdminBarViewFile);
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        }

        $return .= $ob_return;

        return $return;
    }
    
    public function getVagMenu() {
		/**
		* @var $Core Core
		* @var $Mvc Mvc
		* @var $sitesClass Sites
		* @var $accClass Acc
		*/
		
    	$Core = $this->Core;
    	$Mvc = $this->Mvc;
		
		$sitesClass = $Core->Sites();
		$accClass = $Mvc->modelClass('Acc');

		$site   = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
		$siteId = (array_key_exists('id', $site)) ? $site['id'] : 0;
		
		$vagMenu = '';
		$groups  = (is_object($accClass) && is_array($accClass->getGroups())) ? $accClass->getGroups() : array();
		if(count($groups)) {
			$vag_group = array();
			$vag_groupId = (array_key_exists('system_debug_vag', $_SESSION) && is_numeric($_SESSION['system_debug_vag'])) ? $_SESSION['system_debug_vag'] : 'none';
			
			if($vag_groupId !== null) {
				$group     = (array_key_exists($vag_groupId, $groups) && is_array($groups[$vag_groupId])) ? $groups[$vag_groupId] : array();
				$vag_group = $group;

			    $groupSiteIds       = (array_key_exists('siteId', $group)) ? $group['siteId'] : null;
			    $groupSiteIds_array = (strpos($groupSiteIds,',') !== false) ? preg_split('/,/', $groupSiteIds, -1, PREG_SPLIT_NO_EMPTY) : array($groupSiteIds);
			    
			    $groupInCurSite = (in_array($siteId, $groupSiteIds_array) || $vag_groupId == 0);
				
				if(!count($groupInCurSite)) {
					$redirectUrl = $Mvc->getModelUrl('system') . '/debug/vag/group/';
					$Core->Request()->redirect($redirectUrl);
				}
			}
			
			$vagMenu .= '<ul class="nav navbar-nav navbar-right system--vag-menu">';
			
			$ddActiveClass = (count($vag_group)) ? ' active' : '';
			
			$vag_group_name = (array_key_exists('name' , $vag_group)) ? $vag_group['name'] : '';
			
			$vagMenu .= '<li class="dropdown' . $ddActiveClass . '">';
			
			$vagMenu .= '<a href="" title="' . $Core->i18n()->translate('View as group') . '" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">';
			
			$vagMenu .= '<i class="fa fa-eye" aria-hidden="true"></i> ' . $vag_group_name . ' <span class="caret"></span>';
			
			$vagMenu .= '</a>';
			
			$vagMenu .= '<ul class="dropdown-menu">';
			
			if(count($vag_group)) {
				$vagMenu .= '<li class="">';
				
				$vagMenu .= '<a href="' . $Mvc->getModelUrl('system') . '/debug/vag/group/" title="' . $Core->i18n()->translate('reset') . '">' . $Core->i18n()->translate('reset') . '</a>';
				
				$vagMenu .= '</li>';
				
				$vagMenu .= '<li role="separator" class="divider"></li>';
			}
			
			foreach($groups as $gId => $g) {
				
				$gName = (array_key_exists('name', $g)) ? $g['name'] : array();
			        
			    $gSiteIds       = (array_key_exists('siteId', $g)) ? $g['siteId'] : null;
			    $gSiteIds_array = (strpos($gSiteIds,',') !== false) ? preg_split('/,/', $gSiteIds, -1, PREG_SPLIT_NO_EMPTY) : array($gSiteIds);
			    
			    $gInCurSite = (in_array($siteId, $gSiteIds_array) || $gId == 0);
				
				if(!$gInCurSite) { continue; }
				
				$gActiveClass = ($vag_groupId != 'none' && $gId == $vag_groupId) ? 'active' : '';
				
				$setVagUrl = $Mvc->getModelUrl('system') . '/debug/vag/group/' . $gId;
			
				$vagMenu .= '<li class="' . $gActiveClass . '">';
				
				$vagMenu .= '<a href="' . $setVagUrl . '" title="' . $gName . '">' . $gName . '</a>';
				
				$vagMenu .= '</li>';
			
			}
			
			$vagMenu .= '</ul></li></ul>';
		}
		
		return $vagMenu;
    }

    /** Acc Public Access Methods */

    public function getPublicModelKeys() {
        return array(
            'system'
        );
    }

    public function getPublicControllerKeys() {
        return array(
            'i18n',
            'sites'
        );
    }

    public function getPublicActionKeys() {
        return array(
            'getlanguages',
            'gettranslations',
            'translate',
            'chooser'
        );
    }

    /** Maintenance Methods */

    public function isMaintenanceMode() {
        $varPath          = $this->Core->getRootPath() . DS . 'var' . DS;
        $lockPath         = $varPath . 'lock' . DS;
        $lockFileFullName = 'maintenance.lock';

        return (file_exists($lockPath.$lockFileFullName));
    }

    public function setMaintenanceMode($force=false) {
        $return = false;

        /** @var $accClass Acc */
        $accClass = $this->Mvc->modelClass('Acc');

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        if($isSu || $force == true) {
            $lockFileName = 'maintenance.lock';

            if($this->Core->makeLockFile($lockFileName)) {
                $return = true;
            }
        }

        return $return;
    }

    public function unSetMaintenanceMode($force=false) {
        $return = false;

        /** @var $accClass Acc */
        $accClass = $this->Mvc->modelClass('Acc');

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        if($isSu || $force == true) {
            $lockFileName = 'maintenance.lock';

            if($this->Core->deleteLockFile($lockFileName)) {
                $return = true;
            }
        }

        return $return;
    }
    
    /** Config Methods */
    
    public function getAllConfigs($siteId = null) {
	    /**
	     * @var $Core Core
	     * @var $sitesClass Sites
	     * @var $configClass Config
	     */
    	$Core        = $this->Core;
    	$sitesClass  = $Core->Sites();
    	$configClass = $Core->Config();
    	
		$configKeys  = $configClass->getKeys();
		
		$sites = (is_object($sitesClass) && is_array($sitesClass->getSites())) ? $sitesClass->getSites() : array();
		$siteIds = array(0);
		foreach($sites as $sId => $sArr) {
			$siteIds[] = $sId;
		}
		
		$getConfigAll = array();
		foreach($siteIds as $sId) {
			foreach($configKeys as $cKey) {
				$confArr = $configClass->get($cKey,$sId,false,false);
				if(!is_array($confArr) && !array_key_exists('siteId',$confArr)) {
					$confArr = $configClass->get($cKey,0,false,false);
				}
				$getConfigAll[$sId][$cKey] = $confArr;
			}
		}
		
		if($siteId !== null && in_array($siteId, $siteIds)) {
			$getConfigAll = array(
				$siteId => $getConfigAll[$siteId]
			);
		}
		
		return $getConfigAll;
    }
}