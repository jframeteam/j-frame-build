<?php
/**
 * Core Controller i18n
 *
 * @project J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright 2008-2017 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function indexAction() {
    global $Core, $Mvc;

    $redirectUrl = $Mvc->getModelUrl() . '/i18n/languages';

    $Core->Request()->redirect($redirectUrl);
}

function languagesAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('system_i18n_languages')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('Languages') . ' - ' . $Core->i18n()->translate('i18n') . ' - ' . $Core->i18n()->translate('system') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('i18n') . ' - ' . $Core->i18n()->translate('Languages'));

        $return = '';
        $ob_return = '';
        if ($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    }
}

function translationsAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('system_i18n_translations')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('Translations') . ' - ' . $Core->i18n()->translate('i18n') . ' - ' . $Core->i18n()->translate('system') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('i18n') . ' - ' . $Core->i18n()->translate('Translations'));

        $return = '';
        $ob_return = '';
        if ($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    }
}

function saveAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('system_i18n_edit')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $return = '';

        $params = $Mvc->getMvcParams();

        $paramSaveId = 0;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id' && $first_key != 'new') {
                $paramSaveId = (isset($params['id'])) ? (int)$params['id'] : 0;
            } else {
                $paramSaveId = (int)$first_key;
            }
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $post = $Core->Request()->getPost();

            $saveResult = $Core->i18n()->saveTranslation($paramSaveId,$post);
            $return .= '<pre>' .  print_r($saveResult, true) . '</pre>';
        }

        return $return;
    }
}