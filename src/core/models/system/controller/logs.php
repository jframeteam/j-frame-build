<?php
/**
 * System Model Controller Logs
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function indexAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $sitesClass Sites
     * @var $accClass Acc
     * @var $updaterClass Updater
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');
    $sitesClass = $Core->Sites();
    $updaterClass = $Core->Updater();

    $site = (is_object($sitesClass)) ? $sitesClass->getSite() : array();

    $user = (is_object($accClass)) ? $accClass->getUser() : array();
    $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

    if($isSu && count($site)) {
        $return = '';

        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('Logs') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('Logs'));

        $logRelPath = DS . 'var' . DS . 'log' . DS;
        $logFullPath = $Core->getRootPath() . $logRelPath;

        $logFiles = array();

        $it = new FilesystemIterator($logFullPath);
        foreach ($it as $fileinfo) {
            if($fileinfo->isDir()) { continue; }

            $fullFilePath = $fileinfo->getPathname();

            $file     = $fileinfo->getFilename();
            $fileExt  = $fileinfo->getExtension();
            $fileSize = $fileinfo->getSize();
            $fileTime = $fileinfo->getMTime();

            if($fileExt !== 'log') { continue; }

            $logFiles[$file]['size'] = $fileSize;
            $logFiles[$file]['mtime'] = $fileTime;
        }

        $ob_return = '';
        if($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }

}

function viewAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $sitesClass Sites
     * @var $accClass Acc
     * @var $updaterClass Updater
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');
    $sitesClass = $Core->Sites();

    $site = (is_object($sitesClass)) ? $sitesClass->getSite() : array();

    $user = (is_object($accClass)) ? $accClass->getUser() : array();
    $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

    if($isSu && count($site)) {
        $return = '';

        $params  = $Mvc->getMvcParams();

        $paramDirFileName = '';

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'file') {
                $paramDirFileName = (array_key_exists('file', $params)) ? $params['file'] : '';
            } else {
                $paramDirFileName = $first_key;
            }
        }

        $paramDirFileName = urldecode($paramDirFileName);

        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('Log View') . ' \'' . $paramDirFileName . '\'' . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('Log View') . ' \'' . $paramDirFileName . '\'');

        $ob_return = '';
        if($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }

}