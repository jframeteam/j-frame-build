<?php
/**
 * System Model Controller Logs
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function indexAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $sitesClass Sites
     * @var $accClass Acc
     * @var $updaterClass Updater
     * @var $systemClass System
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');
    $sitesClass = $Core->Sites();
    $updaterClass = $Core->Updater();
    $systemClass = $Mvc->modelClass('System');

    $site = (is_object($sitesClass)) ? $sitesClass->getSite() : array();

    $user = (is_object($accClass)) ? $accClass->getUser() : array();
    $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);


    if(is_object($accClass) && !$accClass->hasAccess('system_config')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
} else {
        $return = '';

        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('Config') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('Config') . '&nbsp;<small>BETA!</small>');

        $ob_return = '';
        if($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    }

}