<?php
/**
 * System Model Controller Index
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function indexAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $requestClass Request
     */
    global $Core, $Mvc;
    $requestClass = $Core->Request();

    $redirectUrl = $Mvc->getModelUrl() . '/sites/chooser';

    $requestClass->redirect($redirectUrl);
}

function chooserAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $return = '';

    $accClass = $Mvc->modelClass('Acc');

    $user = (is_object($accClass)) ? $accClass->getUser() : array();
    $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

    if(!count($user)) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('Seite auswählen') . ' - ' . $Core->i18n()->translate('system') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('system') . ' - ' . $Core->i18n()->translate('Seite auswählen'));

        $return = '';

        $ob_return = '';
        if($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    }
}

function activateMaintenanceAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $sitesClass Sites
     * @var $accClass Acc
     * @var $systemClass System
     */
    global $Core, $Mvc;

    $sitesClass = $Core->Sites();
    $accClass = $Mvc->modelClass('Acc');
    $systemClass = $Mvc->modelClass('System');

    $user = (is_object($accClass)) ? $accClass->getUser() : array();
    $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

    $site = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
    $siteUrlKey = (count($site)) ? $site['urlKey'] : '';

    if($isSu) {
        if($systemClass->setMaintenanceMode()) {
            $note = $Core->i18n()->translate('Wartungsmodus aktiviert!');
            $type = 'success';
            $kind = 'bs-alert';
            $title = '';
            $Core->setNote($note, $type, $kind, $title);
        }

        $redirectUrl = $Core->getBaseUrl() . $siteUrlKey;

        $Core->Request()->redirect($redirectUrl);
    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }

}

function deactivateMaintenanceAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $sitesClass Sites
     * @var $accClass Acc
     * @var $systemClass System
     */
    global $Core, $Mvc;

    $sitesClass = $Core->Sites();
    $accClass = $Mvc->modelClass('Acc');
    $systemClass = $Mvc->modelClass('System');

    $user = (is_object($accClass)) ? $accClass->getUser() : array();
    $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

    $site = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
    $siteUrlKey = (count($site)) ? $site['urlKey'] : '';

    if($isSu) {
        if($systemClass->unSetMaintenanceMode()) {
            $note = $Core->i18n()->translate('Wartungsmodus deaktiviert!');
            $type = 'success';
            $kind = 'bs-alert';
            $title = '';
            $Core->setNote($note, $type, $kind, $title);
        }

        $redirectUrl = $Core->getBaseUrl() . $siteUrlKey;

        $Core->Request()->redirect($redirectUrl);
    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }

}