<?php
/**
 * Index Model Controller Index
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function indexAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $sitesClass Sites
     * @var $accClass Acc
     * @var $updaterClass Updater
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');
    $sitesClass = $Core->Sites();
    $updaterClass = $Core->Updater();

    $site = (is_object($sitesClass)) ? $sitesClass->getSite() : array();

    $user = (is_object($accClass)) ? $accClass->getUser() : array();
    $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

    if($isSu && count($site)) {
        $return = '';

        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('Updater') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('Updater'));

        $ob_return = '';
        if($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }

}

function updateAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $sitesClass Sites
     * @var $accClass Acc
     * @var $systemClass System
     * @var $updaterClass Updater
     */
    global $Core, $Mvc;

    $sitesClass = $Core->Sites();
    $accClass = $Mvc->modelClass('Acc');
    $systemClass = $Mvc->modelClass('System');
    $updaterClass = $Core->Updater();

    $site = (is_object($sitesClass)) ? $sitesClass->getSite() : array();

    $user = (is_object($accClass)) ? $accClass->getUser() : array();
    $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

    if($isSu && count($site)) {
        $return = '';

        $params  = $Mvc->getMvcParams();

        $what  = (is_array($params) && array_key_exists('what',$params))  ? $params['what']  : '';
        $param = (is_array($params) && array_key_exists('param',$params)) ? $params['param'] : '';

        $doUpdate_result = $updaterClass->doUpdate($what,$param);

        $updaterUrl = $Mvc->getModelUrl('system') . '/updater/';

        $doUpdate = (is_array($doUpdate_result) && count($doUpdate_result));

        if($doUpdate) {
            if($systemClass->setMaintenanceMode()) {
                $note = $Core->i18n()->translate('Wartungsmodus aktiviert!');
                $type = 'success';
                $kind = 'bs-alert';
                $title = '';
                $Core->setNote($note, $type, $kind, $title);
            }
        }

        $isMaintenanceMode = $systemClass->isMaintenanceMode();

        if($doUpdate && $isMaintenanceMode) {

            $failCount=0;

            foreach ($doUpdate_result as $urWhat => $urData) {
                if(is_array($urData)) {
                    if ($urWhat == 'core') {

                        $coreUpdate_result  = (array_key_exists('success', $urData) && is_bool($urData['success']) && $urData['success']);
                        $coreUpdate_version = (array_key_exists('version', $urData)) ? $urData['version'] : 0;
                        $coreUpdate_error   = (array_key_exists('error', $urData)) ? $urData['error'] : '';

                        if($coreUpdate_result) {
                            $note = sprintf($Core->i18n()->translate('Das Update für das System auf die Version \'%s\' war erfolgreich'),$coreUpdate_version);
                            $type = 'success';
                        } else {
                            $errorTxt = ($coreUpdate_error != '') ? '<br />&nbsp;&nbsp;&bull;&nbsp;' . $coreUpdate_error  : '';

                            $note = $Core->i18n()->translate('Das Update für das System konnte nicht durchgeführt werden.') . ' (v' . $coreUpdate_version . ')' . $errorTxt;
                            $type = 'danger';
                            $failCount++;
                        }
                        $kind = 'bs-alert';
                        $title = '';

                        $Core->setNote($note, $type, $kind, $title);
                    }

                    if ($urWhat == 'model') {
                        if(count($urData)) {
                            foreach($urData as $modelKey => $modelUrData) {
                                $modelUpdate_result  = (array_key_exists('success', $modelUrData) && is_bool($modelUrData['success']) && $modelUrData['success']);
                                $modelUpdate_version = (array_key_exists('version', $modelUrData)) ? $modelUrData['version'] : 0;
                                $modelUpdate_error   = (array_key_exists('error', $modelUrData)) ? $modelUrData['error'] : '';

                                if($modelUpdate_result) {
                                    $note = sprintf($Core->i18n()->translate('Das Update für das Model \'%s\' auf die Version \'%s\' war erfolgreich'),$Core->i18n()->translate($modelKey),$modelUpdate_version);
                                    $type = 'success';
                                } else {
                                    $errorTxt = ($modelUpdate_error != '') ? '<br />&nbsp;&nbsp;&bull;&nbsp;' . $modelUpdate_error : '';

                                    $note = sprintf($Core->i18n()->translate('Das Update für das Model \'%s\' konnte nicht durchgeführt werden.'),$Core->i18n()->translate($modelKey)) . ' (v' . $modelUpdate_version . ')' . $errorTxt;
                                    $type = 'danger';
                                    $failCount++;
                                }
                                $kind = 'bs-alert';
                                $title = '';

                                $Core->setNote($note, $type, $kind, $title);
                            }
                        }
                    }
                }
            }

            if($failCount < 1) {
                if ($systemClass->unSetMaintenanceMode()) {
                    $note = $Core->i18n()->translate('Wartungsmodus deaktiviert!');
                    $type = 'success';
                    $kind = 'bs-alert';
                    $title = '';
                    $Core->setNote($note, $type, $kind, $title);
                }
            }
        } else {
            $note = $Core->i18n()->translate('Updates konnten nicht durchgeführt werden.');
            $type = 'danger';
            $kind = 'bs-alert';
            $title = '';

            $Core->setNote($note, $type, $kind, $title);
        }

        $Core->Request()->redirect($updaterUrl);

        return $return;
    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }

}