<?php
/**
 * Index Model Controller Index
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function indexAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $sitesClass Sites
     * @var $accClass Acc
     * @var $updaterClass Updater
     */
    global $Core, $Mvc;

    $return = '';

    $accClass = $Mvc->modelClass('Acc');
    $sitesClass = $Core->Sites();
    $updaterClass = $Core->Updater();

    $site = (is_object($sitesClass)) ? $sitesClass->getSite() : array();

    $user = (is_object($accClass)) ? $accClass->getUser() : array();
    $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

    if($isSu && count($site)) {
        $return = '';
    } else {
        $return = $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }
    return $return;

}

function changelogAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $sitesClass Sites
     * @var $accClass Acc
     * @var $updaterClass Updater
     */
    global $Core, $Mvc;

    $return = '';

    $accClass = $Mvc->modelClass('Acc');
    $sitesClass = $Core->Sites();
    $updaterClass = $Core->Updater();

    $site = (is_object($sitesClass)) ? $sitesClass->getSite() : array();

    $user = (is_object($accClass)) ? $accClass->getUser() : array();
    $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

    if($isSu && count($site)) {

        $params  = $Mvc->getMvcParams();

        $what  = (is_array($params) && array_key_exists('what',$params))  ? $params['what']  : '';
        $param = (is_array($params) && array_key_exists('param',$params)) ? $params['param'] : '';

        $getChangelog = $updaterClass->getChangelog($what,$param);

        $changelog = array();

        if(is_array($getChangelog) && count($getChangelog)) {
            $changelog = $getChangelog;

            if(array_key_exists($what,$getChangelog)) {
                $changelog = $getChangelog[$what];

                if(is_array($getChangelog[$what]) && array_key_exists($param,$getChangelog[$what])) {
                    $changelog = $getChangelog[$what][$param];
                }
            }
        }

        $ob_return = '';
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
        $return .= $ob_return;

    } else {
        $return = $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }

    return $return;

}