<?php
/**
 * Core Controller Ajax i18n
 *
 * @project J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright 2008-2017 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function indexAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $return = array();

    $params  = $Mvc->getMvcParams();

    $i18nGetAction = '';

    if(count($params)) {
        reset($params);
        $i18nGetAction = key($params);
    }

    $return['success'] = false;

    if($i18nGetAction == 'translate') {
        $post = $Core->Request()->getPost();

        $string = (array_key_exists('string',$post)) ? $post['string'] : '';

        if($string != '') {
            $return['success'] = true;
            $return[$i18nGetAction] = i18n_translate($string);
        }
    }

    if($i18nGetAction == 'getlanguages') {
        $return['success'] = true;
        $return[$i18nGetAction] = i18n_languages();
    }

    if($i18nGetAction == 'gettranslations') {
        $return['success'] = true;
        $return[$i18nGetAction] = i18n_translations();
    }

    return $return;
}

function getlanguagesAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $return = array();
    $return['success'] = true;
    $return['getlanguages'] = i18n_languages();

    return $return;
}

function gettranslationsAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $return = array();

    $return['success'] = true;
    $return['gettranslations'] = i18n_translations();

    return $return;
}

function translateAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $return = array();

    $return['success'] = false;

    $post = $Core->Request()->getPost();

    $string = (array_key_exists('string',$post)) ? $post['string'] : '';
    $lang = (array_key_exists('lang',$post)) ? $post['lang'] : null;

    if($string != '') {
        $return['success'] = true;
        $return['translate'] = i18n_translate($string,$lang);
    }

    return $return;
}

function editAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $accClass Acc
     */
    global $Core, $Mvc;


    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('system_i18n_edit')) {
        return '<div class="alert alert-danger" role="alert" style="margin-bottom: 0;">' . $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...') . '</div>';
    } else {
        $return = '';

        $post = $Core->Request()->getPost();

        $string = (array_key_exists('string',$post)) ? $post['string'] : '';
        $lang   = (array_key_exists('lang',$post)) ? $post['lang'] : null;

        $errMsg = '';
        $hasError = false;
        if($string == '' || $lang == '') {
            $errMsg = $Core->i18n()->translate('Keine Daten übergeben, nicht zu tun...');
            $hasError = true;
        }

        $ob_return = '';
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
        $return .= $ob_return;

        return $return;
    }
}

function i18n_languages() {
    /**
     * @var $Core Core
     */
    global $Core;

    return $Core->i18n()->getLanguages();
}

function i18n_translations() {
    /**
     * @var $Core Core
     */
    global $Core;

    return $Core->i18n()->getTranslations();
}

function i18n_translate($string, $lang = null) {
    /**
     * @var $Core Core
     */
    global $Core;

    $currLangCode = $Core->i18n()->getCurrLang();

    $useLang = ($lang !== null) ? $Core->i18n()->getLang($lang) : $Core->i18n()->getLang($currLangCode);

    $useLangCode = (count($useLang)) ? $useLang['code'] : $currLangCode;

    return $Core->i18n()->translate($string,$useLangCode);
}