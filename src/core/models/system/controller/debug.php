<?php
/**
 * System Model Controller Debug
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function indexAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     */
    global $Core, $Mvc;

    $oldMetaTitle = $Mvc->getMetaTitle();
    $Mvc->setMetaTitle('Debugging Areas - ' . $oldMetaTitle);
    $oldPageTitle = $Mvc->getPageTitle();
    $Mvc->setPageTitle($oldPageTitle . ' <small>v' . $Core->getVersion() . ' - debugging Areas</small>');

    $return = '';
    $ob_return = '';
    if($Mvc->getMVCViewFilePath() !== false) {
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
    } else {
        $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
    }

    $return .= $ob_return;

    return $return;
}

function i18nAction() {
    global $Core, $Mvc;
    $oldMetaTitle = $Mvc->getMetaTitle();
    $Mvc->setMetaTitle('i18n Debugging - ' . $oldMetaTitle);
    $oldPageTitle = $Mvc->getPageTitle();
    $Mvc->setPageTitle($oldPageTitle . ' <small>v' . $Core->getVersion() . ' - i18n debugging</small>');

    $missingTranslationExample = $Core->i18n()->translate('Fehlende Übersetzung, als Beispiel!');

    $return = '';
    $ob_return = '';
    if($Mvc->getMVCViewFilePath() !== false) {
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
    } else {
        $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
    }

    $return .= $ob_return;

    return $return;
}

function accAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     */
    global $Core, $Mvc;

    $oldMetaTitle = $Mvc->getMetaTitle();
    $Mvc->setMetaTitle('Acc Debugging - ' . $oldMetaTitle);
    $oldPageTitle = $Mvc->getPageTitle();
    $Mvc->setPageTitle($oldPageTitle . ' <small>v' . $Core->getVersion() . ' - Acc debugging</small>');

    /** @var $accClass Acc */
    $accClass = $Mvc->modelClass('Acc');
    $missingRightExample = (!is_object($accClass) || $accClass->hasAccess('missing_right_code')) ? true : false;

    $return = '';
    $ob_return = '';
    if($Mvc->getMVCViewFilePath() !== false) {
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
    } else {
        $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
    }

    $return .= $ob_return;

    return $return;
}

function sessionAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     */
    global $Core, $Mvc;

    $oldMetaTitle = $Mvc->getMetaTitle();
    $Mvc->setMetaTitle('Session Debugging - ' . $oldMetaTitle);
    $oldPageTitle = $Mvc->getPageTitle();
    $Mvc->setPageTitle($oldPageTitle . ' <small>v' . $Core->getVersion() . ' - Session debugging</small>');

    $return = '';
    $ob_return = '';
    if($Mvc->getMVCViewFilePath() !== false) {
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
    } else {
        $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
    }

    $return .= $ob_return;

    return $return;
}

function formsAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     */
    global $Core, $Mvc;

    $oldMetaTitle = $Mvc->getMetaTitle();
    $Mvc->setMetaTitle('Forms Debugging - ' . $oldMetaTitle);
    $oldPageTitle = $Mvc->getPageTitle();
    $Mvc->setPageTitle($oldPageTitle . ' <small>v' . $Core->getVersion() . ' - Forms debugging</small>');

    $return = '';
    $ob_return = '';
    if($Mvc->getMVCViewFilePath() !== false) {
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
    } else {
        $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
    }

    $return .= $ob_return;

    return $return;
}

function vagAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     */
    global $Core, $Mvc;

    $accClass = $Mvc->modelClass('Acc');
    $sitesClass = $Core->Sites();
    $updaterClass = $Core->Updater();

    $site   = (is_object($sitesClass)) ? $sitesClass->getSite() : array();
    $siteId = (array_key_exists('id', $site)) ? $site['id'] : 0;

    $user = (is_object($accClass)) ? $accClass->getUser() : array();
    $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

    if($isSu && count($site)) {
        $params = $Mvc->getMvcParams();

        $groupId   = (is_array($params) && array_key_exists('group',$params))  ? $params['group']  : null;
        $groups    = (is_object($accClass) && is_array($accClass->getGroups())) ? $accClass->getGroups() : array();
        $group     = (array_key_exists($groupId, $groups) && is_array($groups[$groupId])) ? $groups[$groupId] : array();
        $groupName = (array_key_exists('name', $group)) ? $group['name'] : '';
        
        $groupSiteIds       = (array_key_exists('siteId', $group)) ? $group['siteId'] : null;
        $groupSiteIds_array = (strpos($groupSiteIds,',') !== false) ? preg_split('/,/', $groupSiteIds, -1, PREG_SPLIT_NO_EMPTY) : array($groupSiteIds);
        
        $groupInCurSite = (is_numeric($groupId) && (in_array($siteId, $groupSiteIds_array) || $groupId == 0));
        
        if($groupInCurSite) {
        	$_SESSION['system_debug_vag'] = $groupId;
        	
            $note  = sprintf( $Core->i18n()->translate('Group "%s" added to "View as group" - Setting'), $groupName);
            $type  = 'success';
            $kind  = 'bs-alert';
            $title = 'System';
        } else {
        	unset($_SESSION['system_debug_vag']);
        	
            $note  = $Core->i18n()->translate('"View as group" - Settings removed.');
            $type  = 'warning';
            $kind  = 'bs-alert';
            $title = 'System';
        }
        
		$redirectUrl = $Mvc->getModelUrl('system') . '/debug/';

        $Core->setNote($note, $type, $kind, $title);
        $Core->Request()->redirect($redirectUrl);
        
	    $return = '';
	    
	    $debugNote = '<pre>' . 
	    	'$siteId => ' . print_r($siteId, true) . PHP_EOL .
	    	'$groupId => ' . print_r($groupId, true) . PHP_EOL .
	    	'$groupSiteIds => ' . print_r($groupSiteIds, true) . PHP_EOL .
	    	'$groupSiteIds_array => ' . print_r($groupSiteIds_array, true) . PHP_EOL .
	    	'$groupInCurSite => ' . print_r($groupInCurSite, true) . PHP_EOL .
	    	'</pre>';
	    	
    	$return .= $debugNote;
	
	    return $return;
    } else {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    }
}