function eleDistanceTop(selector) {
    selector = typeof selector !== 'undefined' ? selector : '';

    var distTop = 0;
    if (selector !== '' && $(selector).length) {
        var bordT = $(selector).css('border-top-width').replace('px', '');
        var paddT = $(selector).css('padding-top').replace('px', '');
        var margT = $(selector).css('margin-top').replace('px', '');

        distTop = parseInt(bordT) + parseInt(paddT) + parseInt(margT);
    }

    return distTop;
}

function addVagMenu() {
    if(!$('.system-su--admin--bar--wrapper #system-su--admin--bar .site-switch-menu li.active').length) {
    	
        var AjaxUrl = '';
        
        if(thisBaseUrl !== '') {
            var baseUrl = thisBaseUrl + 'ajax/' + langUrlSlug;
            var siteKey = core.Request.getCurrUrl().replace(thisBaseUrl, '').split('/')[0];
            var vagMenuUrlPath = siteKey + '/system/debug/vagMenu/';

            AjaxUrl = baseUrl + vagMenuUrlPath;
        }

		if(AjaxUrl !== '') {
	        var getUrl = AjaxUrl;
    	
		    $.get(getUrl, function( data ) {
		        if (data.success) {
		            var result = data.result;
		            if (result.success) {
		                $('#system-su--admin--bar').append(result.vagMenu);
		            }
		        }
		    });
		}
    }
}

$( document ).ready(function($) {

    /** System JS */

    if($('.system-su--admin--bar--wrapper').length) {
        $('body').addClass('system-su--admin--bar');

        var bodyDistTop = eleDistanceTop('body');

        $('body').css('padding-top', bodyDistTop + 37);
        addVagMenu();
    }
    
    if($('.panel.system--site-config .list-group.owner-nav .list-group-item').length) {
		$('.panel.system--site-config .list-group.owner-nav .list-group-item:not(.separator)').on('click',function(e){
			var previous = $(this).closest(".list-group").children(".active");
			previous.removeClass('active'); // previous list-item
			$(e.target).addClass('active'); // activated list-item
		});
    }

});