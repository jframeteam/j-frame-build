<?php
/**
 * System Model View Updater Index
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $sitesClass Sites
 */
$sitesClass = $Core->Sites();
$user = (!isset($user)) ? array() : $user;

$accessibleSites = (count($user) && array_key_exists('sites', $user) && is_array($user['sites']) && count($user['sites'])) ? $accessibleClients = $user['sites'] : $sitesClass->getSites();

$sitesChooser = $sitesClass->getSiteChooser($accessibleSites);

echo $sitesChooser;