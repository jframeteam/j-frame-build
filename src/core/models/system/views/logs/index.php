<?php
/**
 * System Model View Logs Index
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 */

$logFiles = (isset($logFiles)) ? $logFiles : array();

$logViewUrl = $Mvc->getModelUrl() . '/logs/view/';
?>

<div class="action-wrapper row">
    <div class="col-sm-12">
        <div class="btn-set top text-right">
            <a href="<?php echo $Mvc->getModelUrl() . '/logs/'; ?>" class="btn btn-warning" title="<?php echo $Core->i18n()->translate('aktualisieren'); ?>">
                <i class="fa fa-refresh" aria-hidden="true"></i>
            </a>
        </div>
    </div>
</div>

<div class="table-responsive">

    <table class="table table-striped table-hover table-condensed" data-datatable>
        <thead>
        <tr>
            <th class="text-center" data-dtsortable="0"></th>
            <th data-dtdefaultsort="asc"><?php echo $Core->i18n()->translate('Filename') ?></th>
            <th><?php echo $Core->i18n()->translate('Size') ?></th>
            <th><?php echo $Core->i18n()->translate('Last change') ?></th>
            <th class="text-center" data-dtsortable="0"></th>
        </tr>
        </thead>
        <tbody>
        <?php $si=0; foreach($logFiles as $logFileName => $logFile): ?>
            <?php
            $fileSize = $logFile['size'];
            $fileTime = $logFile['mtime'];
            ?>
            <tr>
                <td>
                    <i class="fa fa-file-text" aria-hidden="true"></i>
                </td>
                <td>
                    <?php echo $logFileName; ?>
                </td>
                <td>
                    <?php echo $Core->Helper()->formatBytes($fileSize); ?>
                </td>
                <td>
                    <?php echo date($Core->i18n()->getDateTimeFormat(),$fileTime); ?>
                </td>
                <td>
                    <a href="<?php echo $logViewUrl . urlencode($logFileName); ?>" class="btn btn-default btn-sm" title="<?php echo $Core->i18n()->translate('Log View') ?>" data-toggle="tooltip">
                        <i class="fa fa-eye" aria-hidden="true"></i>
                    </a>
                </td>
            </tr>
            <?php $si++; endforeach; ?>
        </tbody>
    </table>

</div>