<?php
/**
 * System Model View Logs Index
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 */

$logFileRelPath = DS . 'var' . DS . 'log' . DS . $paramDirFileName;
$logFileFullPath = $Core->getRootPath() . $logFileRelPath;
?>

<div class="action-wrapper row">
    <div class="col-sm-12">
        <div class="btn-set top text-right">
            <a href="<?php echo $Mvc->getModelUrl() . '/logs/'; ?>" class="btn btn-default" title="<?php echo $Core->i18n()->translate('zurück zur Übersicht'); ?>">
                <i class="fa fa-arrow-left" aria-hidden="true"></i>
            </a>
            <a href="<?php echo $Mvc->getModelUrl() . '/logs/view/' . $paramDirFileName; ?>" class="btn btn-warning" title="<?php echo $Core->i18n()->translate('aktualisieren'); ?>">
                <i class="fa fa-refresh" aria-hidden="true"></i>
            </a>
        </div>
    </div>
</div>

<?php if(file_exists($logFileFullPath)) : ?>
    <?php
    $data = file_get_contents($logFileFullPath); //read the file

    $regex = '/^\d{4}\-\d{2}\-\d{2}T\d{2}\:\d{2}\:\d{2}[+-]\d{4}\:/m';
    $timesRes = preg_match_all($regex,$data,$times);
    $times = (is_array($times) && is_array(reset($times))) ? array_reverse(reset($times), true) : array();
    $contents = preg_split($regex, $data, -1, PREG_SPLIT_NO_EMPTY);
    ?>
    <h2><?php echo $logFileRelPath; ?> <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $Core->Helper()->formatBytes(filesize($logFileFullPath)); ?> / <?php echo count($times) . ' ' . $Core->i18n()->translate('Einträge'); ?>" style="cursor: help;"></i></h2>
    <div class="panel-group" id="logview_accordion" role="tablist" aria-multiselectable="true">
        <?php foreach($times as $i => $time) :
            $timeFormatted = date('d.m.Y H:i:s', strtotime(substr($time, 0 , -1)));
            $content = (array_key_exists($i, $contents)) ? $contents[$i] : ''; ?>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="logview_collapse_<?php echo $i; ?>_heading">
                    <h3 class="panel-title">
                        <a class="text-primary" role="button" data-toggle="collapse" data-parent="#logview_accordion" href="#logview_collapse_<?php echo $i; ?>" aria-expanded="false" aria-controls="logview_collapse_<?php echo $i; ?>" style="display: block">
                            <?php echo print_r($timeFormatted, true) . ' ' . $Core->i18n()->translate('Uhr'); ?>
                            <span class="pull-right">
                                <span class="caret"></span>
                            </span>
                        </a>
                    </h3>
                </div>
                <div id="logview_collapse_<?php echo $i; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="logview_collapse_<?php echo $i; ?>_heading">
                    <div class="panel-body">
                        <pre><?php echo print_r(trim($content), true); ?></pre>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php else: ?>
    <div class="alert alert-warning" role="alert"><?php echo $Core->i18n()->translate('Keine Logfile gefunden...'); ?></div>
<?php endif; ?>