<?php
/**
* System Model View SU Admin Bar
*
*
* @system J•Frame
* @author Jan Doll <jan_doll@gmx.de>
* @copyright since 2008 by Jan Doll
* All Rights Reserved
*/

/**
* NOTICE OF LICENSE
*
* Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
* its derivatives and/or successors, via any medium, is strictly prohibited.
*
* The Software is deemed proprietary and confidential.
*
* Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
*/

/**
* @var $Core Core
* @var $Mvc Mvc
* @var $sitesClass Sites
* @var $updaterClass Updater
* @var $systemClass System
*/
$sitesClass = $Core->Sites();
$updaterClass = $Core->Updater();
?>
<?php if($systemClass->isMaintenanceMode()) : ?>
<div class="system-su--admin--bar--wrapper pulse--red pulse--bg">
<?php else : ?>
<div class="system-su--admin--bar--wrapper">
<?php endif; ?>
    <div class="container">
        <div class="system-su--admin--bar">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#system-su--admin--bar" aria-expanded="false">
                        <span class="sr-only">Navigation ein-/ausblenden</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <span class="navbar-brand visible-xs">SU AdminBar</span>
                </div>

                <div class="collapse navbar-collapse" id="system-su--admin--bar">
                    <ul class="nav navbar-nav">
                        <?php if($systemClass->isMaintenanceMode()) : ?>
                            <li class="active">
                                <a href="<?php echo $Mvc->getModelUrl('system'); ?>/sites/deactivateMaintenance/" data-toggle="tooltip" data-placement="right" title="<?php echo $Core->i18n()->translate('deaktivieren'); ?>?">
                                    <i class="fa fa-power-off" aria-hidden="true"></i>
                                    <?php echo $Core->i18n()->translate('Wartungsmodus aktiviert!'); ?>
                                    <span class="visible-xs-inline">
                                        <?php echo $Core->i18n()->translate('deaktivieren'); ?>?
                                    </span>
                                </a>
                            </li>
                        <?php else : ?>
                            <li>
                                <a href="<?php echo $Mvc->getModelUrl('system'); ?>/sites/activateMaintenance/" data-ays="enable_maintenance" data-aystext="<?php echo $Core->i18n()->translate('Ganz sicher?'); ?>&lt;br /&gt;<?php echo $Core->i18n()->translate('Ist der Wartungsmodus aktiv, ist ein LogIn über die Benutzeroberfläche ist nicht möglich!'); ?>" data-toggle="tooltip" data-placement="right" title="<?php echo $Core->i18n()->translate('aktivieren'); ?>?">
                                    <i class="fa fa-power-off" aria-hidden="true"></i>
                                    <?php echo $Core->i18n()->translate('Wartungsmodus deaktiviert!'); ?>
                                    <span class="visible-xs-inline">
                                        <?php echo $Core->i18n()->translate('aktivieren'); ?>?
                                    </span>
                                </a>
                            </li>
                        <?php endif; ?>
                        <?php echo $sitesClass->addToMvcMenu(); ?>
                    </ul>
                </div>
            </nav>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="uplink online" title="J•Frame - Uplink">
        <i class="fa fa-podcast" aria-hidden="true"></i>
    </div>
</div>
