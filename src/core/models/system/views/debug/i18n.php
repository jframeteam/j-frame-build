<?php
/**
 * System Model View Debug i18n
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 */

$missingTranslations = $Core->i18n()->getMissingTranslations();
$missingTranslationsSqlInsertIntoArray = array();

if(count($missingTranslations)) : ?>
    <h2>Fehlende Übersetzungen:</h2>
    <div class="row">
        <?php foreach($missingTranslations as $locale => $missingStrings) : ?>
            <?php $lang     = $Core->i18n()->getLangByLocale($locale); ?>
            <?php $langCode = (count($lang)) ? $lang['code'] : 'noLang'; ?>
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?php echo $locale; ?></h3>
                    </div>
                    <div class="panel-body"></div>
                    <ul class="list-group">
                        <?php foreach($missingStrings as $missingString) : ?>
                            <li class="list-group-item"><?php echo htmlentities($missingString); ?></li>
                            <?php $missingTranslationsSqlInsertIntoArray[$missingString][$langCode] = $missingString ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#sqlInserts" aria-expanded="false" aria-controls="sqlInserts">
        Sql Inserts
    </button>
    <div class="collapse" id="sqlInserts">
        <div class="well">
            <code>
                <?php foreach($missingTranslationsSqlInsertIntoArray as $key => $translationInserts) : ?>
                    <?php foreach($translationInserts as $langCode => $translation) : ?>
                        <?php if($langCode != 'de') { continue; } ?>
                        <?php echo 'INSERT INTO `' . DB_TABLE_PREFIX . 'i18n_translations` (`string`, `de`, `en`) VALUES ("'.htmlentities(addslashes($translation)).'", "'.htmlentities(addslashes($translation)).'", "");' ?><br />
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </code>
        </div>
    </div>
<?php endif; ?>