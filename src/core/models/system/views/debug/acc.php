<?php
/**
 * System Model View Debug Acc
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 */

/** @var $accClass Acc */
$accClass = $Mvc->modelClass('Acc');

if(is_object($accClass)) :
    $rights                          = $accClass->getRights();
    $rightsDB                        = $accClass->getDataBaseRights();
    $rightsWhitelist                 = $accClass->getWhitelistRights();
    $rightsFile                      = $accClass->getFileRights();
    $fileRightStringsFull            = $accClass->getFileRightStrings(false);
    $fileRightStrings                = $accClass->getFileRightStrings();
    $missingRights                   = $accClass->getMissingRights();
    $missingRightsSqlInsertIntoArray = array();

    $filesToRightsArray = array();
    foreach($fileRightStringsFull as $path => $rightCodes) {
        foreach($rightCodes as $rightCode) {
            $filesToRightsArray[$rightCode][] = $path;
        }
    }
    $filesToMissingRightsArray = array();
    foreach($filesToRightsArray as $rightCode => $pathArray) {
        if(in_array($rightCode,$missingRights)) {
            $filesToMissingRightsArray[$rightCode] = $pathArray;
            $missingRightsSqlInsertIntoArray[] = $rightCode;
        }
    }
    ?>

    <ul class="nav nav-tabs">
        <li class="active"><a href="#all" data-toggle="tab">All RightCodes <span class="badge"><?php echo count($filesToRightsArray); ?></span></a></li>
        <li><a href="#missing" data-toggle="tab">Missing RightCodes <span class="badge"><?php echo count($filesToMissingRightsArray); ?></span></a></li>
        <li><a href="#raw" data-toggle="tab">Raw</a></li>
    </ul>
    <div id="addressbookTabContent" class="tab-content">
        <div class="tab-pane fade active in" id="all">
            <h2>All RightCodes:</h2>
            <?php if(count($filesToRightsArray)) : ?>
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <?php foreach($filesToRightsArray as $rightCode => $pathArray) : ?>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="right_files_collapse_<?php echo $rightCode; ?>_heading">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#right_files_collapse_<?php echo $rightCode; ?>" aria-expanded="false" aria-controls="right_files_collapse_<?php echo $rightCode; ?>" style="display: block">
                                        <?php echo $rightCode; ?>
                                        <span class="pull-right">
                                            <span class="badge" title="Used in <?php echo count($pathArray); ?> File(s)"><?php echo count($pathArray); ?></span>
                                            <span class="caret"></span>
                                        </span>
                                    </a>
                                </h4>
                            </div>
                            <div id="right_files_collapse_<?php echo $rightCode; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="right_files_collapse_<?php echo $rightCode; ?>_heading">
                                <div class="panel-body">
                                    <p>Used in <?php echo count($pathArray); ?> File(s)</p>
                                </div>
                                <ul class="list-group">
                                    <?php foreach($pathArray as $path) : ?>
                                        <li class="list-group-item"><?php echo $path; ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="tab-pane fade" id="missing">
            <h2>Missing RightCodes:</h2>
            <?php if(count($filesToMissingRightsArray)) : ?>
                <div class="panel-group" id="missing_accordion" role="tablist" aria-multiselectable="true">
                    <?php foreach($filesToMissingRightsArray as $rightCode => $pathArray) : ?>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="missing_right_files_collapse_<?php echo $rightCode; ?>_heading">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#missing_accordion" href="#missing_right_files_collapse_<?php echo $rightCode; ?>" aria-expanded="false" aria-controls="missing_right_files_collapse_<?php echo $rightCode; ?>" style="display: block">
                                        <?php echo $rightCode; ?>
                                        <span class="pull-right">
                                            <span class="badge" title="Used in <?php echo count($pathArray); ?> File(s)"><?php echo count($pathArray); ?></span>
                                            <span class="caret"></span>
                                        </span>
                                    </a>
                                </h4>
                            </div>
                            <div id="missing_right_files_collapse_<?php echo $rightCode; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="missing_right_files_collapse_<?php echo $rightCode; ?>_heading">
                                <div class="panel-body">
                                    <p>Used in <?php echo count($pathArray); ?> File(s)</p>
                                </div>
                                <ul class="list-group">
                                    <?php foreach($pathArray as $path) : ?>
                                        <li class="list-group-item"><?php echo $path; ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#sqlInserts" aria-expanded="false" aria-controls="sqlInserts">
                    Sql Inserts
                </button>
                <div class="collapse" id="sqlInserts">
                    <div class="well">
                        <code>
                            <?php foreach($missingRightsSqlInsertIntoArray as $key => $rightCode) : ?>
                                <?php echo 'INSERT INTO `' . DB_TABLE_PREFIX . 'acc_rights` (`code`, `name`) VALUES ("'.$rightCode.'", "'.$Core->i18n()->translate($rightCode).'");' ?><br />
                            <?php endforeach; ?>
                        </code>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <div class="tab-pane fade" id="raw">
            <h2>Raw:</h2>

            <pre>$rightsWhitelist (<?php echo count($rightsWhitelist); ?>) => <?php echo print_r($rightsWhitelist, true); ?></pre>
            <pre>$rightsFile (<?php echo count($rightsFile); ?>) => <?php echo print_r($rightsFile, true); ?></pre>
            <pre>$rightsDB (<?php echo count($rightsDB); ?>) => <?php echo print_r($rightsDB, true); ?></pre>
            <pre>$rights (<?php echo count($rights); ?>) => <?php echo print_r($rights, true); ?></pre>
            <pre><?php
                foreach($rights as $rightCode => $right) {
                    echo '\'' . $rightCode . '\',' . PHP_EOL;
                }
            ?></pre>
        </div>
    </div>
<?php endif; ?>