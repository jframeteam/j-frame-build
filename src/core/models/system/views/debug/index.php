<?php
/**
 * System Model View Debug Index
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 */

?>

<div class="list-group">
    <a class="list-group-item" href="<?php echo $Mvc->getModelUrl('system'); ?>/debug/i18n">i18n Debug</a>
    <a class="list-group-item" href="<?php echo $Mvc->getModelUrl('system'); ?>/debug/acc">Acc Debug</a>
    <a class="list-group-item" href="<?php echo $Mvc->getModelUrl('system'); ?>/debug/session">Session Debug</a>
    <a class="list-group-item" href="<?php echo $Mvc->getModelUrl('system'); ?>/debug/forms">Forms Debug</a>
</div>
