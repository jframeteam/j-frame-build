<?php
/**
 * Debug Model View Index Index
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 */
$isIonCubeLoaded  = false; foreach(get_loaded_extensions() as $extensionName) { if(strpos(strtolower($extensionName), 'ioncube') !== false) { $isIonCubeLoaded = true; break; } }
echo '<pre>$isIonCubeLoaded => ' . print_r($isIonCubeLoaded, true) . '</pre>';
echo '<pre>$_SESSION => ' . print_r($_SESSION, true) . '</pre>';