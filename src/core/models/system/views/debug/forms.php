<?php
/**
 * System Model View Debug Forms
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 */
?>

    <h2>Forms Plugin</h2>
<?php

/** @var $formsPlugin Forms */
$formsPlugin = $Core->Plugins()->Forms();

if(is_object($formsPlugin)) {
    /** Example */

    $formConfig = array(
        'action'    => '',             /* String | Required! URL Action Target */
        'method'    => 'post',         /* String: post, get | Optional! post by default */
        'multipart' => false,          /* Boolean: | Optional! false by default */
        'title'     => '',             /* String | Optional! */
        'id'        => '',             /* String | Optional! */
        'classes'   => '',             /* String | Optional! Separated by space */
    );

    $formGroups[] = array(
        'code'    => 'group_code',     /* String | Required! Must be unique */
        'title'   => 'Test Group',               /* String | Optional! */
        'id'      => '',               /* String | Optional! */
        'classes' => '',               /* String | Optional! Separated by space */
    );

    $singleFormFieldConfig = array(
        'code'  => 'field_code',       /* String | Required! Must be unique */
        'group' => 'group_code',       /* String | Required! Must exsist in Group Array. Can be Empty and will be ignored, if this Form Field Config Array is used in Single Build */
        'label' => array(              /* Array or Boolean | Required! Is set to false, no Label will be created */
            'title'   => 'Test Label',           /* String | Required! Used for Label Text */
        ),
        'field' => array(              /* Array | Required! Contains all neccessary information for Form Field */
            'type'     => 'text',      /* String: text [html5 types etc], password, textarea, select, checkbox, radio, upload, submit, reset, button | Required! */
            'name'     => 'test',      /* String | Required! */
            'value'    => '',          /* String | Required! Can be empty */
            'id'       => 'test',      /* String | Optional! */
        ),
    );
    $formFields[] = $singleFormFieldConfig;

    $singleFormFieldConfig2 = array(
        'code'  => 'field_code_full',       /* String | Required! Must be unique */
        'group' => 'group_code',       /* String | Required! Must exsist in Group Array. Can be Empty and will be ignored, if this Form Field Config Array is used in Single Build */
        'label' => array(              /* Array or Boolean | Required! Is set to false, no Label will be created */
            'title'   => 'Test 2 Label',           /* String | Required! Used for Label Text */
            'id'      => '',           /* String | Optional! */
            'classes' => 'col-sm-2 control-label',           /* String | Optional! Separated by space */
        ),
        'field' => array(              /* Array | Required! Contains all neccessary information for Form Field */
            'type'     => 'select',      /* String: text [html5 types etc], password, textarea, select, checkbox, radio, upload, submit, reset, button | Required! */
            'name'     => 'test2',      /* String | Required! */
            'value'    => '',          /* String | Required! Can be empty */
            'options'  => array(       /* Array | May be required! I.e. If Type is select */
                'value1' => 'text1',   /* String | Required minimum 1! Value und Text will be set in options tag */
                'value2' => 'text2',
            ),
            'required' => false,       /* Boolean | Optional! false by default */
            'default'  => '',          /* String | Optional! Will be used, if Value is empty */
            'title'    => '',          /* String | Optional! */
            'id'       => 'test2',      /* String | Optional! */
            'classes'  => '',          /* String | Optional! Separated by space */
            'wrapper'  => array(       /* Array | Optional! */
                'tag'     => 'div',       /* String: div, span | Required! */
                'id'      => '',       /* String | Optional! */
                'classes' => 'col-sm-10',       /* String | Optional! Separated by space */
            ),
        ),
        'wrapper'  => array(       /* Array | Optional! */
            'tag'     => 'div',       /* String: div, span | Required! */
            'id'      => '',       /* String | Optional! */
            'classes' => 'row',       /* String | Optional! Separated by space */
        ),
    );
    $formFields[] = $singleFormFieldConfig2;

    $formBuildConfig = array (
        'config' => $formConfig,
        'groups' => $formGroups,
        'fields' => $formFields
    );

    $formsPlugin->build($formBuildConfig);            /* Build new Form with config Array. Will return ->getForm() */

    $buildField = $formsPlugin->buildField($singleFormFieldConfig); /* Single Build for new Form Field with config Array. Will return ->getFields() with specified Field Code */

    $getForm = $formsPlugin->getForm();               /* Returns Full Form as HTML */

    $getGroups = $formsPlugin->getGroups();           /* Returns all Groups with all Fields as HTML. Parameters: Group Code as String (optional) Will return HTML from specific Group if exists */

    $getFields = $formsPlugin->getFields();           /* Returns all Fields as HTML. Parameters: Field Code as String (optional) Will return HTML from specific Field if exists */

    $singleFormFieldSoloConfig = array(
        'type'          => 'text',  /* String: text [html5 types etc], password, textarea, select, checkbox, radio, upload, submit, reset, button | Required! */
        'name'          => 'test3', /* String | Required! */
        'value'         => '',      /* String | Required! Can be empty */
        'id'            => 'test3', /* String | Optional! */
        'placeholder'   => 'Hello', /* String | Optional! */
        'addAttributes' => array(   /* Array | Optional! */
            'data-toggle' => 'test3',
            'autocomplete' => 'off'
        ),
    );
    echo '<pre>' .
        '$formBuildConfig => ' . print_r($formBuildConfig, true) . PHP_EOL .
        '$singleFormFieldSoloConfig => ' . print_r($singleFormFieldSoloConfig, true) . PHP_EOL .
        '$formsPlugin->buildElement($singleFormFieldSoloConfig) => ' . print_r($formsPlugin->buildElement($singleFormFieldSoloConfig), true) . PHP_EOL .
        '</pre>';

    echo $getForm;
}