<?php
/**
 * System View i18n Translations
 *
 * @project J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright 2008-2017 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 */

$languages = $Core->i18n()->getLanguages();

$fileTranslationStringsFull = $Core->i18n()->getFileTranslationStrings(false);
$fileTranslationStrings     = $Core->i18n()->getFileTranslationStrings();
$missingTranslations        = $Core->i18n()->getMissingTranslations();

$filesToTranslationsArray = array();
foreach($fileTranslationStringsFull as $path => $translationStrings) {
    foreach($translationStrings as $translationString) {
        $filesToTranslationsArray[$translationString][] = $path;
    }
}

/** Sort */
ksort($filesToTranslationsArray);

$filesToMissingTranslationsArray = array();
foreach($missingTranslations as $locale => $missingStrings) {
    foreach($filesToTranslationsArray as $translationString => $pathArray) {
        if(in_array($translationString,$missingStrings)) {
            $filesToMissingTranslationsArray[$locale][$translationString] = $pathArray;
        }
    }
}

?>
<div class="alert alert-warning">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><?php echo $Core->i18n()->translate('Please Note') ?></h4>
    <p><?php echo $Core->i18n()->translate('Translations from DB overwrites the File based translations') ?></p>
</div>

<div class="action-wrapper row">
    <div class="col-sm-12">
        <div class="btn-set text-right">
            <a href="<?php echo $Mvc->getModelUrl('system') . '/i18n/'; ?>" class="btn btn-default" title="<?php echo $Core->i18n()->translate('Back to Overview') ?>">
                <i class="fa fa-arrow-left" aria-hidden="true"></i>
                <?php echo $Core->i18n()->translate('Back to Overview') ?>
            </a>
        </div>
    </div>
</div>

<p>
    <?php echo $Core->i18n()->translate('Here listed are all parsed translations. Database and File based from system, models, plugins modules and themes.') ?><br />
    <?php echo $Core->i18n()->translate('You can administrate the Database Translations') ?>
</p>


<ul class="nav nav-tabs">
    <?php $li=1; foreach($languages as $lang): ?>
        <?php
        $translations = $Core->i18n()->getTranslations($lang['locale']);
        $percentTranslated = $Core->i18n()->getPercentTranslated($lang['code'])  . '%';
        $translated = array();
        foreach ($fileTranslationStrings as $translationString) {

            $translation    = (array_key_exists('fs',$translations) && is_array($translations['fs']) && array_key_exists($translationString, $translations['fs'])) ? $translations['fs'][$translationString] : '';
            $translation_db = (array_key_exists('db',$translations) && is_array($translations['db']) && array_key_exists($translationString, $translations['db'])) ? $translations['db'][$translationString] : '';

            if($translation !== '' || $translation_db !== '') {
                $translated[] = $translationString;
            }

        }

        $translated = array_unique($translated);
        sort($translated);
        $classAttr = ($li == 1) ? ' class="active"': '';
        $labelClass = 'label-danger';
        if($percentTranslated > 50) {
            $labelClass = 'label-warning';
        }
        if($percentTranslated > 90) {
            $labelClass = 'label-success';
        }
        ?>
        <li<?php echo $classAttr; ?>>
            <a href="#<?php echo $lang['locale'] ?>" data-toggle="tab"><?php echo $lang['name'] ?> <span class="label <?php echo $labelClass; ?>" title="<?php echo count($translated) ?>/<?php echo count($fileTranslationStrings) ?>"><?php echo $percentTranslated ?></span></a>
        </li>
        <?php $li++; endforeach; ?>
    <li>
        <a href="#missing" data-toggle="tab"><?php echo $Core->i18n()->translate('Missing Translations') ?></a>
    </li>
</ul>
<div id="cmsPageTabContent" class="tab-content">

    <?php $tti=1; foreach($languages as $lang): ?>

        <?php $activeClasses = ($tti == 1) ? ' active in' : ''; ?>
        <div class="tab-pane fade<?php echo $activeClasses; ?>" id="<?php echo $lang['locale'] ?>">

            <div class="table-responsive">

                <table class="table table-striped table-hover table-condensed" data-datatable>
                    <thead>
                    <tr>
                        <th class="text-center"></th>
                        <th data-dtdefaultsort="asc"><?php echo $Core->i18n()->translate('String') ?></th>
                        <th><?php echo $Core->i18n()->translate('Translation') ?></th>
                        <th><?php echo $Core->i18n()->translate('Translation Database') ?></th>
                        <th data-dtsortable="0"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $si=0; foreach($fileTranslationStrings as $tbl_string): ?>
                        <?php $translations   = $Core->i18n()->getTranslations($lang['locale']); ?>
                        <?php $translation_fs = (array_key_exists('fs',$translations) && is_array($translations['fs']) && array_key_exists($tbl_string, $translations['fs'])) ? $translations['fs'][$tbl_string] : ''; ?>
                        <?php $translation_db = (array_key_exists('db',$translations) && is_array($translations['db']) && array_key_exists($tbl_string, $translations['db'])) ? $translations['db'][$tbl_string] : ''; ?>
                        <tr>
                            <td<?php if($translation_fs == '' && $translation_db == '') : ?> class="text-danger"<?php endif; ?> id="<?php echo $lang['locale']; ?>_string_<?php echo $si; ?>">
									<span title="<?php echo $lang['locale']; ?>_string_<?php echo $si; ?>">
										<?php if($translation_fs == '' && $translation_db == '') : ?>
                                            <i class="fa fa-exclamation-circle" aria-hidden="true" title="<?php echo $Core->i18n()->translate('No translation found!'); ?>" data-toggle="tooltip" data-container="body"></i>
                                        <?php endif; ?>
                                        <?php if($translation_fs !== '') : ?>
                                            <i class="fa fa-file-text" aria-hidden="true" title="<?php echo $Core->i18n()->translate('File based translation'); ?>" data-toggle="tooltip" data-container="body"></i>
                                        <?php endif; ?>
                                        <?php if($translation_db !== '') : ?>
                                            <i class="fa fa-database" aria-hidden="true" title="<?php echo $Core->i18n()->translate('Database based translation'); ?>" data-toggle="tooltip" data-container="body"></i>
                                        <?php endif; ?>
									</span>
                            </td>
                            <td<?php if($translation_fs == '' && $translation_db == '') : ?> class="text-danger"<?php endif; ?>><?php echo htmlentities($tbl_string, ENT_QUOTES, "UTF-8"); ?></td>
                            <td><?php echo htmlentities($translation_fs, ENT_QUOTES, "UTF-8"); ?></td>
                            <td><?php echo htmlentities($translation_db, ENT_QUOTES, "UTF-8"); ?></td>
                            <td>
									<span title="<?php echo $Core->i18n()->translate('Edit / create Database translation for this String'); ?>" data-toggle="tooltip" data-container="body">
										<a
                                            href="<?php echo $Mvc->getModelAjaxUrl('system') . '/i18n/edit/'; ?>"
                                            data-toggle="modal"
                                            data-target="#i18nEditModal"

                                            data-ajaxtype="post"
                                            data-post-string="<?php echo htmlentities($tbl_string, ENT_QUOTES, "UTF-8"); ?>"
                                            data-post-lang="<?php echo $lang['code']; ?>"
                                        >
											<i class="fa fa-pencil" aria-hidden="true"></i>
										</a>
									</span>
                            </td>
                        </tr>
                        <?php $si++; endforeach; ?>
                    </tbody>
                </table>

            </div>

        </div>

        <?php $tti++; endforeach; ?>

    <div class="tab-pane fade" id="missing">
        <?php
        $missingTranslationsSqlInsertIntoArray = array();

        if(count($missingTranslations)) :
            ?>
            <h2><?php echo $Core->i18n()->translate('Missing Translations') ?>:</h2>
            <div class="row">

                <div class="panel-group" id="missing_accordion" role="tablist" aria-multiselectable="true">
                    <?php foreach($filesToMissingTranslationsArray as $locale => $missingStrings) : ?>
                        <div class="col-sm-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><?php echo $locale; ?> <span class="badge"><?php echo count($missingStrings); ?></span></h3>
                                </div>
                                <div class="panel-body">
                                    <?php $i=0; foreach($missingStrings as $translationString => $pathArray) : ?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="missing_translations_files_collapse_<?php echo $locale . '_' . $i; ?>_heading">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#missing_accordion" href="#missing_translations_files_collapse_<?php echo $locale . '_' . $i; ?>" aria-expanded="false" aria-controls="missing_right_files_collapse_<?php echo $locale . '_' . $i; ?>" style="display: block">
                                                        <?php echo $translationString; ?>
                                                        <span class="pull-right">
					                                        <span class="badge" title="<?php echo sprintf($Core->i18n()->translate('Used in %s File(s)'), count($pathArray)); ?>"><?php echo count($pathArray); ?></span>
					                                        <span class="caret"></span>
					                                    </span>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="missing_translations_files_collapse_<?php echo $locale . '_' . $i; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="missing_translations_files_collapse_<?php echo $locale . '_' . $i; ?>_heading">
                                                <div class="panel-body">
                                                    <p><?php echo sprintf($Core->i18n()->translate('Used in %s File(s)'), count($pathArray)); ?></p>
                                                </div>
                                                <ul class="list-group">
                                                    <?php foreach($pathArray as $path) : ?>
                                                        <li class="list-group-item"><?php echo $path; ?></li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </div>
                                        </div>
                                        <?php $i++; endforeach; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>

            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#sqlInserts" aria-expanded="false" aria-controls="sqlInserts">
                Sql Inserts
            </button>
            <div class="collapse" id="sqlInserts">
                <div class="well">
                    <code>
                        <?php foreach($missingTranslationsSqlInsertIntoArray as $key => $translationInserts) : ?>
                            <?php foreach($translationInserts as $langCode => $translation) : ?>
                                <?php if($langCode != 'de') { continue; } ?>
                                <?php echo 'INSERT INTO `' . DB_TABLE_PREFIX . 'i18n_translations` (`string`, `de`, `en`) VALUES ("'.htmlentities(addslashes($translation)).'", "'.htmlentities(addslashes($translation)).'", "");' ?><br />
                            <?php endforeach; ?>
                        <?php endforeach; ?>
                    </code>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="i18nEditModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?php echo $Core->i18n()->translate('Edit'); ?></h4>
            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Close') ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
