<?php
/**
 * System View i18n Languages
 *
 * @project J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright 2008-2017 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 */

$languages = $Core->i18n()->getLanguages();
?>

<div class="action-wrapper row">
    <div class="col-sm-12">
        <div class="btn-set text-right">
            <a href="<?php echo $Mvc->getModelUrl('system') . '/i18n/translations/'; ?>" class="btn btn-primary" title="<?php echo $Core->i18n()->translate('Translations') ?>">
                <i class="fa fa-language" aria-hidden="true"></i>
                <?php echo $Core->i18n()->translate('Translations') ?>
            </a>
        </div>
    </div>
</div>

<div class="row">

    <?php foreach ($languages as $code => $lang) : ?>
        <?php $pClass = ($lang['active']) ? 'primary' : 'default'; ?>
        <div class="col-sm-3">
            <div class="panel panel-<?php echo $pClass ?>">
                <div class="panel-heading">
                    <h3 class="panel-title"><span class="flag flag-<?php echo $code; ?>"></span> <?php echo $lang['name'] ?></h3>
                </div>
                <table class="table table-condensed table-striped table-hover">
                    <tbody>
                    <tr>
                        <th scope="row"><?php echo $Core->i18n()->translate('Name') ?></th>
                        <td><?php echo $lang['name'] ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?php echo $Core->i18n()->translate('Code') ?></th>
                        <td><?php echo $lang['code'] ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?php echo $Core->i18n()->translate('Locale') ?></th>
                        <td><?php echo $lang['locale'] ?></td>
                    </tr>
                    <tr>
                        <th scope="row"><?php echo $Core->i18n()->translate('Active') ?></th>
                        <td><?php echo ($lang['active']) ? $Core->i18n()->translate('Yes') : $Core->i18n()->translate('No'); ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    <?php endforeach; ?>

</div>