<?php
/**
 * System View Ajax i18n Edit Translation
 *
 * @project J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright 2008-2017 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $accClass Acc
 */

$language = $Core->i18n()->getLang($lang);

$allTranslations = $Core->i18n()->getTranslations($language['locale']);

$fsTranslations = $allTranslations['fs'];
$fsTranslation  = (array_key_exists(html_entity_decode($string), $fsTranslations)) ? $fsTranslations[html_entity_decode($string)] : '';

$dbTranslationsFull = $Core->i18n()->getDbTranslations(true);

$dbTranslation = array();

foreach($dbTranslationsFull as $transId => $translation) {
    $transString = $translation['string'];
    if($transString == html_entity_decode($string)) {
        $dbTranslation = $translation;
    }
}

$editId          = (count($dbTranslation) && array_key_exists('id',$dbTranslation)) ? $dbTranslation['id'] : 'new';
$editTranslation = (count($dbTranslation) && array_key_exists('tranlations', $dbTranslation) && count($dbTranslation['tranlations']) && array_key_exists($language['locale'], $dbTranslation['tranlations'])) ? $dbTranslation['tranlations'][$language['locale']] : '';

$translationSaveUrl = $Mvc->getModelUrl() . '/i18n/save/' . $editId;?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">
        <?php echo $Core->i18n()->translate('Edit Translation') ?>: <?php echo $language['name']; ?>
    </h4>
</div>

<form id="edit_create_form" class="form-horizontal" action="<?php echo $translationSaveUrl ?>" method="post">
    <input name="id" type="hidden" value="<?php echo $editId; ?>">
    <input name="locale" type="hidden" value="<?php echo $language['locale']; ?>">
    <input name="string" type="hidden" value="<?php echo $string ?>">

    <div class="modal-body">

        <div class="form-group">
            <label for="string" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('String') ?></label>

            <div class="col-md-10">
                <pre class="form-control-static"><?php echo $string; ?></pre>
            </div>
        </div>

        <div class="form-group">
            <label for="translation_fs" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Translation'); ?></label>

            <div class="col-md-10">
                <pre class="form-control-static"><?php echo $fsTranslation; ?></pre>
            </div>
        </div>

        <div class="form-group">
            <label for="translation_db" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Translation Database'); ?><?php if($editId == 'new') : ?> <em>*</em><?php endif; ?></label>

            <div class="col-md-10">
                <textarea id="translation_db" name="translation_db" rows="5" class="form-control"<?php if($editId == 'new') : ?> required="required"<?php endif; ?> placeholder="<?php echo $editTranslation; ?>"><?php echo $editTranslation; ?></textarea>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Cancel'); ?></button>
        <button type="submit" class="btn btn-success"><?php echo $Core->i18n()->translate('Save'); ?></button>
    </div>
</form>