<?php
/**
 * System View Ajax i18n Edit
 *
 * @project J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright 2008-2017 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */
?>
<?php if(!$hasError) : ?>
    <?php include('edit.translation.php'); ?>
<?php else: ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">
            <?php echo $Core->i18n()->translate('Error') ?>
        </h4>
    </div>
    <div class="modal-body">
        <div class="alert alert-danger" role="alert"><?php echo $errMsg; ?></div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Schließen'); ?></button>
    </div>
<?php endif; ?>