<?php
/**
 * System Model View Updater Index
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $updaterClass Updater
 */

$changelog = (isset($changelog)) ? $changelog : array();
$what = (isset($what)) ? $what : '';
$param = (isset($param)) ? $param : '';
$version   = 0;
$dbVersion = 0;

$titleWhat = '';
if($what == 'core') {
    $titleWhat = $Core->i18n()->translate('System');
    $version   = $Core->getVersion();
    $dbVersion = $Core->getDbVersion();
}
if($what == 'model') {
    $titleWhat = $Core->i18n()->translate('Model');

    if($param !== '') {
        $titleWhat .= ' \'' . $Core->i18n()->translate($param) . '\'';

        $modelClass = $Mvc->modelClass($param);
        $version    = (is_object($modelClass) && method_exists($modelClass, 'getVersion')) ? $modelClass->getVersion() : 0;
        $dbVersion  = (is_object($modelClass) && method_exists($modelClass, 'getDbVersion')) ? $modelClass->getDbVersion() : 0;
    }
}
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">
        <?php echo $Core->i18n()->translate('Changelog') ?> <?php echo $titleWhat; ?>
    </h4>
</div>

<div class="modal-body">
    <dl>
        <?php krsort($changelog); $mi=1; foreach($changelog as $clVersion => $clMessage) : ?>
            <?php
            $updateVersionNumber = str_replace('.', '', $clVersion);
            $versionNumber       = str_replace('.', '', $version);
            $dbVersionNumber     = str_replace('.', '', $dbVersion);

            $wrapperClass = (($clVersion !== 0 && $version !== 0) && ($dbVersionNumber < $updateVersionNumber)) ? ' bg-warning' : ' bg-success';
            ?>
            <div class="dl-wrapper<?php echo $wrapperClass;?>">
                <dt>
                    v<?php echo $clVersion; ?>
                </dt>
                <dd>
                    <?php echo $clMessage; ?>
                </dd>
                <?php if($mi < count($changelog)) : ?>
                    <hr />
                <?php endif; ?>
            </div>
        <?php $mi++; endforeach; ?>
    </dl>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Schließen'); ?></button>
</div>

