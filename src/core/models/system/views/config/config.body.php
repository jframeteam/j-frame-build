<?php
/**
 * System Model View Config Index
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $i18nClass i18n
 * @var $configClass Config
 */

$i18nClass   = $Core->i18n();
$configClass = $Core->Config();

$sName = (isset($sName)) ? $sName : $i18nClass->translate('config_defaults');
$sUrlKey = (isset($sUrlKey)) ? $sUrlKey : 'config_defaults';
$sConfigsByOwner = (isset($sConfigsByOwner)) ? $sConfigsByOwner : array();
if(count($sConfigsByOwner)) : ?>
<div class="system--site-config panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title"><?php echo sprintf($i18nClass->translate('%s System Configurations'), '<strong>' . $sName . '</strong>'); ?></h3>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-sm-3">
				<div class="owner-nav list-group">
					<?php $oci=0; foreach($sConfigsByOwner as $oKey => $oConfigs): ?>
						<?php
							$oName = $i18nClass->translate($oKey);
							$activeClass = ($oci < 1) ? ' active' : '';
						?>
						<?php if($oKey == '___') : ?>
							<span class="list-group-item separator"></span>
						<?php else: ?>
							<a href="#<?php echo $sUrlKey . '_' . $oKey; ?>" class="list-group-item<?php echo $activeClass; ?>" data-toggle="tab">
								<?php echo $oName; ?>
							</a>
						<?php endif; ?>
					<?php $oci++; endforeach; ?>
				</div>
			</div>
			<div class="col-sm-9">
				<div id="sOwnerConfTabContent" class="tab-content">
					<?php $oci=0; foreach($sConfigsByOwner as $oKey => $oConfigs): ?>
						<?php
							$oName = $i18nClass->translate($oKey);
							$activeClass = ($oci < 1) ? ' active in' : '';
						?>
						<div class="tab-pane fade<?php echo $activeClass; ?>" id="<?php echo $sUrlKey . '_' . $oKey; ?>">
							<pre><?php echo print_r($oConfigs, true); ?></pre>
						</div>
					<?php $oci++; endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>