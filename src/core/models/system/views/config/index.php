<?php
/**
 * System Model View Config Index
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $i18nClass i18n
 * @var $systemClass System
 * @var $configClass Config
 */

$i18nClass   = $Core->i18n();
$configClass = $Core->Config();

$isSu = (isset($isSu)) ? $isSu : false;

$sites = (is_object($sitesClass) && is_array($sitesClass->getSites())) ? $sitesClass->getSites() : array();
$siteIds = array(0);
foreach($sites as $sId => $sArr) {
	$siteIds[] = $sId;
}

$configKeys       = $configClass->getKeys();

$getConfigDefault = $systemClass->getAllConfigs(0);
$getConfigAll     = $systemClass->getAllConfigs();

$getOwnerNames    = $configClass->getOwnerNames();
?>
<p>...to Rule them all!</p>

<?php if($isSu) : ?>
    <ul class="nav nav-tabs">
		<?php foreach($siteIds as $sId): ?>
			<?php
				$sArr    = array();
				$sUrlKey = 'config_defaults';
				$sName   = $i18nClass->translate('config_defaults');
				$activeClass = 'active';
				
				if($sId !== 0) {
					$sArr    = (array_key_exists($sId, $sites) && is_array($sites[$sId])) ? $sites[$sId] : array();
					$sUrlKey = (array_key_exists('urlKey', $sArr)) ? $sArr['urlKey'] : '';
					$sName   = (array_key_exists('name', $sArr)) ? $sArr['name'] : '';
					$activeClass = '';
				}
			?>
			<li class="<?php echo $activeClass; ?>">
				<a href="#<?php echo $sUrlKey; ?>" data-toggle="tab">
					<?php echo $sName; ?>
				</a>
			</li>
		<?php endforeach; ?>
    </ul>
    <div id="sConfTabContent" class="tab-content">
		<?php foreach($siteIds as $sId): ?>
			<?php
				$sArr    = array();
				$sUrlKey = 'config_defaults';
				$sName   = $i18nClass->translate('config_defaults');
				$activeClass = ' active in';
				
				if($sId !== 0) {
					$sArr    = (array_key_exists($sId, $sites) && is_array($sites[$sId])) ? $sites[$sId] : array();
					$sUrlKey = (array_key_exists('urlKey', $sArr)) ? $sArr['urlKey'] : '';
					$sName   = (array_key_exists('name', $sArr)) ? $sArr['name'] : '';
					$activeClass = '';
				}
				
				$sConfigs = (array_key_exists($sId, $getConfigAll) && is_array($getConfigAll[$sId])) ? $getConfigAll[$sId] : array();
				foreach($getConfigDefault as $dcKey => $dcArr) {
					if(!array_key_exists($dcKey, $sConfigs)) {
						$sConfigs[$dcKey] = $dcArr;
					}
				}
				
				$countConfigs = count($sConfigs);
				
				$sConfigsByOwner = array();
				foreach($sConfigs as $scKey => $scArr) {
					$owner = (array_key_exists('owner',$scArr) && is_string($scArr['owner'])) ? $scArr['owner'] : '';
					$type  = (array_key_exists('type',$scArr) && is_string($scArr['type'])) ? $scArr['type'] : '';
					if($owner !== '' && $type == 'config') {
						$sConfigsByOwner[$owner][$scKey] = $scArr;
						ksort($sConfigsByOwner[$owner]);
					}
				}
				ksort($sConfigsByOwner);
				
				$systemOwnersConfigs = array();
				$otherOwnersConfigs = array();
				foreach($sConfigsByOwner as $oKey => $ocArr) {
					if(array_key_exists($oKey,$getOwnerNames)) {
						$systemOwnersConfigs[$oKey] = $ocArr;
						unset($sConfigsByOwner[$oKey]);
					} else {
						$otherOwnersConfigs[$oKey] = $ocArr;
						unset($sConfigsByOwner[$oKey]);
					}
				}
				
				$sConfigsByOwnerNew = array();
				foreach($systemOwnersConfigs as $socKey => $socArr) {
					$sConfigsByOwnerNew[$socKey] = $socArr;
				}
				$sConfigsByOwnerNew['___'] = array();
				foreach($otherOwnersConfigs as $oocKey => $oocArr) {
					$sConfigsByOwnerNew[$oocKey] = $oocArr;
				}
				
				$sConfigsByOwner = $sConfigsByOwnerNew;
			?>
			
	        <div class="tab-pane fade<?php echo $activeClass; ?>" id="<?php echo $sUrlKey; ?>">
	        	<?php include('config.body.php'); ?>
	        </div>
		<?php endforeach; ?>
    </div>
<?php else: ?>
	<?php
		$currSite = (is_object($sitesClass) && is_array($sitesClass->getSite())) ? $sitesClass->getSite() : array();
		$currSiteId = (array_key_exists('id',$currSite)) ? $currSite['id'] : 0;

		$sId    = $currSiteId;
		
		$sArr    = array();
		$sUrlKey = 'config_defaults';
		$sName   = $i18nClass->translate('config_defaults');
		$activeClass = ' active in';
		
		if($sId !== 0) {
			$sArr    = (array_key_exists($sId, $sites) && is_array($sites[$sId])) ? $sites[$sId] : array();
			$sUrlKey = (array_key_exists('urlKey', $sArr)) ? $sArr['urlKey'] : '';
			$sName   = (array_key_exists('name', $sArr)) ? $sArr['name'] : '';
			$activeClass = '';
		}
		
		$sConfigs = (array_key_exists($sId, $getConfigAll) && is_array($getConfigAll[$sId])) ? $getConfigAll[$sId] : array();
		foreach($getConfigDefault as $dcKey => $dcArr) {
			if(!array_key_exists($dcKey, $sConfigs)) {
				$sConfigs[$dcKey] = $dcArr;
			}
		}
		
		$countConfigs = count($sConfigs);
		
		$sConfigsByOwner = array();
		foreach($sConfigs as $scKey => $scArr) {
			$owner = (array_key_exists('owner',$scArr) && is_string($scArr['owner'])) ? $scArr['owner'] : '';
			$type  = (array_key_exists('type',$scArr) && is_string($scArr['type'])) ? $scArr['type'] : '';
			if($owner !== '' && $type == 'config') {
				$sConfigsByOwner[$owner][$scKey] = $scArr;
				ksort($sConfigsByOwner[$owner]);
			}
		}
		ksort($sConfigsByOwner);
		
		$systemOwnersConfigs = array();
		$otherOwnersConfigs = array();
		foreach($sConfigsByOwner as $oKey => $ocArr) {
			if(array_key_exists($oKey,$getOwnerNames)) {
				$systemOwnersConfigs[$oKey] = $ocArr;
				unset($sConfigsByOwner[$oKey]);
			} else {
				$otherOwnersConfigs[$oKey] = $ocArr;
				unset($sConfigsByOwner[$oKey]);
			}
		}
		
		$sConfigsByOwnerNew = array();
		foreach($systemOwnersConfigs as $socKey => $socArr) {
			$sConfigsByOwnerNew[$socKey] = $socArr;
		}
		$sConfigsByOwnerNew['___'] = array();
		foreach($otherOwnersConfigs as $oocKey => $oocArr) {
			$sConfigsByOwnerNew[$oocKey] = $oocArr;
		}
		
		$sConfigsByOwner = $sConfigsByOwnerNew;
	?>
	<?php include('config.body.php'); ?>
<?php endif; ?>
