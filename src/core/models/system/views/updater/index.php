<?php
/**
 * System Model View Updater Index
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $updaterClass Updater
 */
?>

<?php $countUpdates_all = $updaterClass->countUpdates('all'); ?>
<?php $updateCheck_all  = $updaterClass->checkForUpdates('all'); ?>
<?php $getChangeLog_all = $updaterClass->getChangelog('all'); ?>

<?php
    $canUpdateAll = false;
    $canUpdateModels = false;
    if(is_array($countUpdates_all) && count($countUpdates_all)) {
        foreach ($countUpdates_all as $what => $upToDate) {
            if (!$upToDate) {
                $canUpdateAll = true;
            }
            if ($what == 'models' && !$upToDate) {
                $canUpdateAll = true;
                $canUpdateModels = true;
            }
        }
    }
?>
<?php

$debugNote = '<pre>' .
    '$countUpdates_all => ' . print_r($countUpdates_all, true) . PHP_EOL .
    '$updateCheck_all => ' . print_r($updateCheck_all, true) . PHP_EOL .
    '$getChangeLog_all => ' . print_r($getChangeLog_all, true) . PHP_EOL .
    '$canUpdateAll => ' . print_r($canUpdateAll, true) . PHP_EOL .
    '$canUpdateModels => ' . print_r($canUpdateModels, true) . PHP_EOL .
'</pre>';

//echo $debugNote;
?>
<div class="action-wrapper">
    <div class="btn-set top text-right">
        <?php if($canUpdateAll): ?>
            <a href="<?php echo $Mvc->getModelUrl('system') . '/updater/update/what/all/'; ?>" class="btn btn-success" title="<?php echo $Core->i18n()->translate('Alles updaten'); ?>">
                <i class="fa fa-angle-double-up" aria-hidden="true"></i>
            </a>
        <?php endif; ?>
    </div>
</div>

<?php if(is_array($updateCheck_all) && count($updateCheck_all)) : ?>

    <?php foreach($updateCheck_all as $what => $data) : ?>

        <?php if($what == 'core') : ?>
        <?php $version       = (array_key_exists('version',$data))   ? $data['version']   : 0; ?>
        <?php $dbVersion     = (array_key_exists('dbVersion',$data)) ? $data['dbVersion'] : 0; ?>
        <?php $upToDate      = (array_key_exists('up-to-date',$data) && is_bool($data['up-to-date']) && $data['up-to-date']); ?>
        <?php $coreUpdateUrl = $Mvc->getModelUrl('system') . '/updater/update/what/' . $what . '/'; ?>
        <?php $coreChangelogUrl = $Mvc->getModelAjaxUrl('system') . '/updater/changelog/what/' . $what . '/'; ?>
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $Core->i18n()->translate('System'); ?></h3>
            </div>
            <div class="panel-body">
                <dl class="dl-horizontal">
                    <dt>
                        <?php echo $Core->i18n()->translate('Core'); ?>
                        <br />v<?php echo $dbVersion; ?>
                        <?php if($upToDate) : ?>
                            <i class="fa fa-check text-success" aria-hidden="true"></i>
                        <?php else : ?>
                            <i class="fa fa-exclamation text-danger" aria-hidden="true"></i>
                        <?php endif; ?>
                    </dt>
                    <dd>
                        <?php if(!$upToDate) : ?>
                            <?php echo $Core->i18n()->translate('Das System ist nicht mehr Aktuell!'); ?><br />
                            <?php echo $Core->i18n()->translate('Neueste Version') . ' =&gt; ' . $version; ?>
                            <a href="<?php echo $coreUpdateUrl; ?>" class="btn btn-success btn-xs"><?php echo $Core->i18n()->translate('Jetzt updaten'); ?></a>
                        <?php else : ?>
                            <span class="text-success">
                                <strong><?php echo $Core->i18n()->translate('Das System ist auf dem neuesten Stand!'); ?></strong>
                            </span>
                        <?php endif; ?>
                        <br /><a href="<?php echo $coreChangelogUrl; ?>" class="btn btn-default btn-xs" data-toggle="modal" data-target="#systemUpdaterChangelogModal"><i class="fa fa-list-alt" aria-hidden="true" data-toggle="tooltip" title="<?php echo $Core->i18n()->translate('Changelog'); ?>"></i></a>
                    </dd>
                </dl>
            </div>
        </div>
        <?php endif; ?>

        <?php if($what == 'model') : ?>

            <?php if(is_array($data) && count($data)): ?>

                <?php $modelsUpdateUrl = $Mvc->getModelUrl('system') . '/updater/update/what/' . $what . '/'; ?>
                <?php $modelsChangelogUrl = $Mvc->getModelAjaxUrl('system') . '/updater/changelog/what/' . $what . '/'; ?>

                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <?php echo $Core->i18n()->translate('Models'); ?>
                            <?php if($canUpdateModels): ?>
                                <a href="<?php echo $modelsUpdateUrl; ?>" class="btn btn-success btn-xs pull-right" title="<?php echo $Core->i18n()->translate('Alle Models updaten'); ?>">
                                    <i class="fa fa-angle-double-up" aria-hidden="true"></i>
                                </a>
                            <?php endif; ?>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <dl class="dl-horizontal">
                        <?php $mi=1; foreach($data as $modelKey => $modelData) : ?>
                            <?php $modelVersion        = (array_key_exists('version',$modelData))   ? $modelData['version']   : 0; ?>
                            <?php $modelDbVersion      = (array_key_exists('dbVersion',$modelData)) ? $modelData['dbVersion'] : 0; ?>
                            <?php $modelUpToDate       = (array_key_exists('up-to-date',$modelData) && is_bool($modelData['up-to-date']) && $modelData['up-to-date']); ?>
                            <?php $modelHasVersionInfo = ($modelVersion != 0); ?>
                            <?php $modelChangelogUrl   = $modelsChangelogUrl . 'param/' . $modelKey; ?>
                            <dt<?php if(!$modelHasVersionInfo): ?> class="text-muted"<?php endif; ?>>
                                <?php echo $Core->i18n()->translate($modelKey); ?>
                                <?php if($modelHasVersionInfo): ?>
                                    <br />v<?php echo $modelDbVersion; ?>
                                    <?php if($modelUpToDate) : ?>
                                    <i class="fa fa-check text-success" aria-hidden="true"></i>
                                    <?php else : ?>
                                    <i class="fa fa-exclamation text-danger" aria-hidden="true"></i>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </dt>
                            <dd<?php if(!$modelHasVersionInfo): ?> class="text-muted"<?php endif; ?>>

                            <?php if($modelHasVersionInfo) : ?>
                                <?php $modelUpdateUrl = $modelsUpdateUrl . 'param/' . $modelKey; ?>

                                <?php if(!$modelUpToDate) : ?>
                                    <?php echo sprintf($Core->i18n()->translate('Das Model \'%s\' ist nicht mehr Aktuell!'),'<i>' . $Core->i18n()->translate($modelKey) . '</i>'); ?><br />
                                    <?php echo $Core->i18n()->translate('Neueste Version') . ' =&gt; ' . $modelVersion; ?>
                                    <a href="<?php echo $modelUpdateUrl; ?>" class="btn btn-success btn-xs"><?php echo $Core->i18n()->translate('Jetzt updaten'); ?></a>
                                <?php else : ?>
                                    <span class="text-success"><strong><?php echo $Core->i18n()->translate('Das Model ist auf dem neuesten Stand!'); ?></strong></span>
                                <?php endif; ?>
                                <br /><a href="<?php echo $modelChangelogUrl; ?>" class="btn btn-default btn-xs" data-toggle="modal" data-target="#systemUpdaterChangelogModal"><i class="fa fa-list-alt" aria-hidden="true" data-toggle="tooltip" title="<?php echo $Core->i18n()->translate('Changelog'); ?>"></i></a>
                            <?php else : ?>
                                <?php echo sprintf($Core->i18n()->translate('Keine Versions-Informationen für das Model \'%s\' gefunden.'),'<i>' . $Core->i18n()->translate($modelKey) . '</i>'); ?>
                            <?php endif; ?>
                            </dd>
                            <?php if($mi < count($data)) : ?>
                            <hr />
                            <?php endif; ?>
                        <?php $mi++; endforeach; ?>
                        </dl>
                    </div>
                </div>

            <?php endif; ?>

        <?php endif; ?>

    <?php endforeach; ?>

    <!-- Modal -->
    <div class="modal fade" id="systemUpdaterChangelogModal" tabindex="-1" role="dialog" aria-labelledby="systemUpdaterChangelogModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><?php echo $Core->i18n()->translate('Changelog'); ?></h4>
                </div>
                <div class="modal-body"><div class="te"></div></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Schließen') ?></button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
<?php endif; ?>

