<?php
/**
 * Mailer Model - PITs Portal Mailer via PITS Mailer Service
 *
 * Some Methods implemented from Source by Planet ITservices GmbH & Co. KG
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

$incPath = JF_ROOT_DIR . DS . 'core' . DS . 'models' . DS . 'mailer' . DS . 'inc';

require $incPath . DS . 'PHPMailer' . DS . 'PHPMailerAutoload.php';

/** Mailer Class */
class Mailer
{
    private $_version = '2.5.0';

    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $_mailer PHPMailer
     */
    private $Core;
    private $Mvc;
    private $_mailer;
    private $siteId;
    private $service_url = 'http://sendmail.planet-itservices.com';
    private $lib_remote_file = '/inc/MailerService.php.jpg';

    /**
     * Mailer constructor.
     * @param $Core Core
     */
    function __construct($Core)
    {
        $this->Core = $Core;
        $this->Mvc = $Core->Mvc();

        $this->_mailer = new PHPMailer;

        $site = $this->Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;
        $this->setUseSiteId($siteId);

        $this->_setMvcPageHeadMeta();
        $this->_setMvcPageBeforeBodyEnds();
    }

    public function getVersion() {
        return $this->_version;
    }

    public function getDbVersion() {
        $dbVersion_option = $this->Core->Config()->get('mailer_version',0);
        $dbVersion = ($dbVersion_option !== '') ? $dbVersion_option : 0;
        return $dbVersion;
    }

    /** Update Methods */
    public function _update240($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc;

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '2.4.0';
        $return['changelog'] = 'Added Version Control';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function _update243($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc;

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '2.4.3';
        $return['changelog'] = 'Added Comment Col to mailer_addressbooks Table';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {

            $new_col_exits = false;

            $check_for_new_col_SQL = 'SELECT IF(EXISTS(SELECT `id` FROM `' . DB_TABLE_PREFIX . 'mailer_addressbooks` WHERE (`comment` IS NOT NULL) OR (`comment` IS NULL) ), 1, 0) as "exists"';

            try {
                $result = $this->Core->Db()->fromDatabase($check_for_new_col_SQL, '@simple');

                if ($result !== 0) {
                    $new_col_exits = true;
                    $return['success'] = true;
                }
            } catch (Exception $e) {
                $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
            }

            if (!$new_col_exits) {
                try {
                    $addCol_SQL = 'ALTER TABLE `' . DB_TABLE_PREFIX . 'mailer_addressbooks` ADD `comment` TEXT NULL DEFAULT NULL COLLATE "utf8_unicode_ci" AFTER `email`';

                    $sqlResult = $this->Core->Db()->toDatabase($addCol_SQL);

                    if (!$sqlResult) {
                        // Error
                        $return['sqlResult'] = $sqlResult;
                        $return['success'] = false;
                    } else {
                        $return['success'] = true;
                    }
                } catch (Exception $e) {
                    $return['error'] = 'Exception abgefangen: ' . $e->getMessage() . "\n";
                }
            }
        }

        if($return['success']) {
            unset($return['error']);
        }

        return $return;
    }

    public function _update250($execute=true) {
        /** @var $Mvc Mvc */
        $Mvc = $this->Mvc;

        /** @var $accClass Acc */
        $accClass = $Mvc->modelClass('Acc');

        $return = array();
        $return['success']   = false;
        $return['version']   = '2.5.0';
        $return['changelog'] = '&nbsp;&nbsp;&bull; implemented _prepareAddressBookData Method<br />&nbsp;&nbsp;&bull; Added Comments for AddressBook Entry to Forms and Output';

        $user = (is_object($accClass)) ? $accClass->getUser() : array();
        $isSu = (is_array($user) && count($user) && array_key_exists('su', $user) && $user['su']);

        $version   = $this->getVersion();
        $dbVersion = $this->getDbVersion();

        $updateVersionNumber = str_replace('.', '', $return['version']);
        $versionNumber       = str_replace('.', '', $version);
        $dbVersionNumber     = str_replace('.', '', $dbVersion);

        $execute = ($execute && ($dbVersionNumber < $updateVersionNumber && $updateVersionNumber <= $versionNumber));

        if($isSu && $execute) {
            $return['success'] = true;
        }

        return $return;
    }

    public function setUseSiteId($siteId) {
        $this->siteId = $siteId;
    }

    public function getUseSiteId() {
        return $this->siteId;
    }

    private function _setMvcPageHeadMeta()
    {
        $getPageHeadMeta = $this->Mvc->getPageHeadMeta();

        if(strpos($getPageHeadMeta, 'Mailer Head Meta') === false) {
            $incUrl = $this->Core->getCoreUrl() . '/models/mailer/inc';

            $mvcPageHeadMeta = PHP_EOL . '<!-- Mailer Head Meta START -->';

            $mvcPageHeadMeta .= PHP_EOL;
            $mvcPageHeadMeta .= '<link rel="stylesheet" type="text/css" href="' . $incUrl . '/mailer.css">';

            $mvcPageHeadMeta .= PHP_EOL . '<!-- Mailer Head Meta END -->';
            $mvcPageHeadMeta .= PHP_EOL;

            $this->Core->Mvc()->addPageHeadMeta($mvcPageHeadMeta);
        }
    }

    private function _setMvcPageBeforeBodyEnds()
    {
        $getPageBeforeBodyEnd = $this->Mvc->getPageBeforeBodyEnd();

        if(strpos($getPageBeforeBodyEnd, 'Mailer Before Body Ends') === false) {
            $incUrl = $this->Core->getCoreUrl() . '/models/mailer/inc';

            $mvcPageBeforeBodyEnd = PHP_EOL . '<!-- Mailer Before Body Ends START -->';

            $mvcPageBeforeBodyEnd .= '<script src="' . $incUrl . '/mailer.js"></script>';

            $mvcPageBeforeBodyEnd .= PHP_EOL . '<!-- Mailer Before Body Ends END -->';
            $mvcPageBeforeBodyEnd .= PHP_EOL;

            $this->Core->Mvc()->addPageBeforeBodyEnd($mvcPageBeforeBodyEnd);
        }
    }

    /** Settings Methods */

    public function getSettings() {
        $siteId = $this->getUseSiteId();

        $settings = array();
        $settings['app_username']    = $this->Core->Config()->get('mailer_app_username', $siteId);
        $settings['app_password']    = $this->Core->Config()->get('mailer_app_password', $siteId);
        $settings['envelope_sender'] = $this->Core->Config()->get('mailer_envelope_sender', $siteId);
        $settings['mailer_reply_to'] = $this->Core->Config()->get('mailer_reply_to', $siteId);
        $settings['mailer_cc']       = $this->Core->Config()->get('mailer_cc', $siteId);
        $settings['mailer_bcc']      = $this->Core->Config()->get('mailer_bcc', $siteId);

        /**
         * Use Envelope Sender for personal From E-Mail Addresses.
         * Currently set to False, because there are many E-Mail Server / Client Problems to handle.
         */
        $settings['use_envelope_sender'] = false;

        return $settings;
    }

    public function setSettings($settings) {
        $return = false;
        if(is_array($settings)) {
            $siteId = $this->getUseSiteId();

            $getSettings = $this->getSettings();
            $getSettings_envelope_sender = (array_key_exists('envelope_sender', $getSettings)) ? $getSettings['envelope_sender'] : '';
            $getSettings_app_username    = (array_key_exists('app_username', $getSettings) && trim(strip_tags($getSettings['app_username'])) != '') ? trim(strip_tags($getSettings['app_username'])) : '';
            $getSettings_app_password    = (array_key_exists('app_password', $getSettings))    ? $getSettings['app_password']    : '';
            $getSettings_mailer_reply_to = (array_key_exists('mailer_reply_to', $getSettings)) ? $getSettings['mailer_reply_to'] : '';
            $getSettings_mailer_cc       = (array_key_exists('mailer_cc', $getSettings))       ? $getSettings['mailer_cc']       : '';
            $getSettings_mailer_bcc      = (array_key_exists('mailer_bcc', $getSettings))      ? $getSettings['mailer_bcc']      : '';

            $option_owner = 'mailer';

            $envelope_sender = (array_key_exists('envelope_sender', $settings) && $settings['envelope_sender'] != '') ? $settings['envelope_sender'] : $getSettings_envelope_sender;
            $app_username    = (array_key_exists('app_username', $settings)    && trim(strip_tags($settings['app_username'])) != '') ? trim(strip_tags($settings['app_username'])) : $getSettings_app_username;
            $app_password    = (array_key_exists('app_password', $settings)    && $settings['app_password'] != '')    ? $settings['app_password']    : $getSettings_app_password;
            $mailer_reply_to = (array_key_exists('mailer_reply_to', $settings) && $settings['mailer_reply_to'] != '') ? $settings['mailer_reply_to'] : '';
            $mailer_cc       = (array_key_exists('mailer_cc', $settings)       && $settings['mailer_cc'] != '')       ? $settings['mailer_cc']       : '';
            $mailer_bcc      = (array_key_exists('mailer_bcc', $settings)      && $settings['mailer_bcc'] != '')      ? $settings['mailer_bcc']      : '';

            $set_envelope_sender = $this->Core->Config()->set('mailer_envelope_sender', $envelope_sender, $siteId, $option_owner);
            $set_app_username    = $this->Core->Config()->set('mailer_app_username', $app_username, $siteId, $option_owner);
            $set_app_password    = ($this->hasSettings() && $app_password == '') ? true : $this->Core->Config()->set('mailer_app_password', $app_password, $siteId, $option_owner);
            $set_mailer_reply_to = $this->Core->Config()->set('mailer_reply_to', $mailer_reply_to, $siteId, $option_owner);
            $set_mailer_cc       = $this->Core->Config()->set('mailer_cc', $mailer_cc, $siteId, $option_owner);
            $set_mailer_bcc      = $this->Core->Config()->set('mailer_bcc', $mailer_bcc, $siteId, $option_owner);

            if($set_app_username && $set_app_password && $set_envelope_sender) {
                $return = true;
            }
        }
        return $return;
    }

    public function hasSettings() {
        $return = false;
        $settings = $this->getSettings();
        if($settings['app_username'] != '' && $settings['app_password'] != '' && $settings['envelope_sender'] != '') {
            $return = true;
        }
        return $return;
    }

    public function getSettings_new() {
        $settings = array();
        $settings['mailer_from']          = $this->Core->Config()->get('mailer_from');
        $settings['mailer_reply_to']      = $this->Core->Config()->get('mailer_reply_to');
        $settings['mailer_cc']            = $this->Core->Config()->get('mailer_cc');
        $settings['mailer_bcc']           = $this->Core->Config()->get('mailer_bcc');
        $settings['mailer_use_smtp']      = $this->Core->Config()->get('mailer_use_smtp');
        $settings['mailer_smtp_host']     = $this->Core->Config()->get('mailer_smtp_host');
        $settings['mailer_smtp_port']     = $this->Core->Config()->get('mailer_smtp_port');
        $settings['mailer_smtp_secure']   = $this->Core->Config()->get('mailer_smtp_secure');
        $settings['mailer_smtp_username'] = $this->Core->Config()->get('mailer_smtp_username');
        $settings['mailer_smtp_password'] = $this->Core->Config()->get('mailer_smtp_password');

        return $settings;
    }

    public function setSettings_new($settings) {
        $return = false;
        if(is_array($settings)) {

            $getSettings = $this->getSettings();
            $getSettings_mailer_from      = (array_key_exists('mailer_from', $getSettings))     ? $getSettings['mailer_from']     : '';
            $getSettings_mailer_reply_to  = (array_key_exists('mailer_reply_to', $getSettings)) ? $getSettings['mailer_reply_to'] : '';
            $getSettings_mailer_cc        = (array_key_exists('mailer_cc', $getSettings))       ? $getSettings['mailer_cc']       : '';
            $getSettings_mailer_bcc       = (array_key_exists('mailer_bcc', $getSettings))      ? $getSettings['mailer_bcc']      : '';
            $getSettings_use_smtp         = (array_key_exists('mailer_use_smtp', $getSettings) && $getSettings['mailer_use_smtp']) ? 1 : 0;
            $getSettings_smtp_host        = (array_key_exists('mailer_smtp_host', $getSettings)) ? $getSettings['mailer_smtp_host'] : '';
            $getSettings_smtp_port        = (array_key_exists('mailer_smtp_port', $getSettings)) ? $getSettings['mailer_smtp_port'] : '';
            $getSettings_smtp_secure      = (array_key_exists('mailer_smtp_secure', $getSettings)) ? $getSettings['mailer_smtp_secure'] : '';
            $getSettings_smtp_username    = (array_key_exists('mailer_smtp_username', $getSettings) && trim(strip_tags($getSettings['mailer_smtp_username'])) != '') ? trim(strip_tags($getSettings['mailer_smtp_username'])) : '';
            $getSettings_smtp_password    = (array_key_exists('mailer_smtp_password', $getSettings)) ? $getSettings['mailer_smtp_password'] : '';

            $option_owner = 'mailer';

            $mailer_from          = (array_key_exists('mailer_from', $settings)          && $settings['mailer_from'] != '')          ? $settings['mailer_from']        : '';
            $mailer_reply_to      = (array_key_exists('mailer_reply_to', $settings)      && $settings['mailer_reply_to'] != '')      ? $settings['mailer_reply_to']    : '';
            $mailer_cc            = (array_key_exists('mailer_cc', $settings)            && $settings['mailer_cc'] != '')            ? $settings['mailer_cc']          : '';
            $mailer_bcc           = (array_key_exists('mailer_bcc', $settings)           && $settings['mailer_bcc'] != '')           ? $settings['mailer_bcc']         : '';
            $mailer_use_smtp      = (array_key_exists('mailer_use_smtp', $settings)      && $settings['mailer_use_smtp'] != '')      ? $settings['mailer_use_smtp']    : '';
            $mailer_smtp_host     = (array_key_exists('mailer_smtp_host', $settings)     && $settings['mailer_smtp_host'] != '')     ? $settings['mailer_smtp_host']   : '';
            $mailer_smtp_port     = (array_key_exists('mailer_smtp_port', $settings)     && $settings['mailer_smtp_port'] != '')     ? $settings['mailer_smtp_port']   : '';
            $mailer_smtp_secure   = (array_key_exists('mailer_smtp_secure', $settings)   && $settings['mailer_smtp_secure'] != '')   ? $settings['mailer_smtp_secure'] : '';
            $mailer_smtp_username = (array_key_exists('mailer_smtp_username', $settings) && trim(strip_tags($settings['mailer_smtp_username'])) != '') ? trim(strip_tags($settings['mailer_smtp_username'])) : '';
            $mailer_smtp_password = (array_key_exists('mailer_smtp_password', $settings) && $settings['mailer_smtp_password'] != '') ? $settings['mailer_smtp_password'] : '';

            $set_mailer_from     = $this->Core->Config()->set('mailer_from', $mailer_from, $option_owner);
            $set_mailer_reply_to = $this->Core->Config()->set('mailer_reply_to', $mailer_reply_to, $option_owner);
            $set_mailer_cc       = $this->Core->Config()->set('mailer_cc', $mailer_cc, $option_owner);
            $set_mailer_bcc      = $this->Core->Config()->set('mailer_bcc', $mailer_bcc, $option_owner);
            $set_use_smtp        = $this->Core->Config()->set('mailer_use_smtp', $mailer_use_smtp, $option_owner);
            $set_smtp_host       = $this->Core->Config()->set('mailer_smtp_host', $mailer_smtp_host, $option_owner);
            $set_smtp_port       = $this->Core->Config()->set('mailer_smtp_port', $mailer_smtp_port, $option_owner);
            $set_smtp_secure     = $this->Core->Config()->set('mailer_smtp_secure', $mailer_smtp_secure, $option_owner);
            $set_smtp_username   = $this->Core->Config()->set('mailer_smtp_username', $mailer_smtp_username, $option_owner);
            $set_smtp_password   = ($this->hasSettings() && $mailer_smtp_password == '') ? true : $this->Core->Config()->set('mailer_smtp_password', $mailer_smtp_password, $option_owner);

            if($set_smtp_host && $set_smtp_port && $set_smtp_secure && $set_smtp_username && $set_smtp_password && $set_use_smtp) {
                $return = true;
            }
        }
        return $return;
    }

    public function hasSettings_new() {
        $return = true;
        $settings = $this->getSettings();

        if($settings['mailer_use_smtp']) {
            if(
                ($settings['mailer_smtp_host']     == '') ||
                ($settings['mailer_smtp_port']     == '') ||
                ($settings['mailer_smtp_secure']   == '') ||
                ($settings['mailer_smtp_username'] == '') ||
                ($settings['mailer_smtp_password'] == '')
            ) {
                $return = false;
            }
        }
        return $return;
    }

    /** Mail Methods */

    /** Send Mail Methods */
    private function _mailer() {
        return $this->_mailer;
    }

    private function _prepareSendData($sendData)
    {
        /** Set Variables to Send */
        $settings             = $this->getSettings();
        $mailer_from          = $settings['mailer_from'];
        $mailer_reply_to      = $settings['mailer_reply_to'];
        $mailer_cc            = $settings['mailer_cc'];
        $mailer_bcc           = $settings['mailer_bcc'];
        $mailer_use_smtp      = $settings['mailer_use_smtp'];
        $mailer_smtp_host     = $settings['mailer_smtp_host'];
        $mailer_smtp_port     = $settings['mailer_smtp_port'];
        $mailer_smtp_secure   = $settings['mailer_smtp_secure'];
        $mailer_smtp_username = $settings['mailer_smtp_username'];
        $mailer_smtp_password = $settings['mailer_smtp_password'];

        /** @var $accClass Acc */
        $accClass  = $this->Mvc->modelClass('Acc');
        $curUser   = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        /**
         * Set SendData Array To Variables
         */
        $abeIds          = (array_key_exists('abeIds',          $sendData)) ? $sendData['abeIds']          : '';
        $to              = (array_key_exists('to',              $sendData)) ? $sendData['to']              : '';
        $from            = (array_key_exists('from',            $sendData)) ? $sendData['from']            : $mailer_from;
        $subject         = (array_key_exists('subject',         $sendData)) ? trim(strip_tags($sendData['subject'])) : '';
        $message         = (array_key_exists('message',         $sendData)) ? $sendData['message']         : '';
        $attachments     = (array_key_exists('attachments',     $sendData) && is_array($sendData['attachments'])) ? $sendData['attachments'] : array();
        $cc              = (array_key_exists('cc',              $sendData)) ? $sendData['cc']              : '';
        $bcc             = (array_key_exists('bcc',             $sendData)) ? $sendData['bcc']             : '';
        $reply_to        = (array_key_exists('reply_to',        $sendData)) ? $sendData['reply_to']        : '';
        $ishtml          = (array_key_exists('ishtml',          $sendData)) ? $sendData['ishtml']          : true;
        $userId          = (array_key_exists('userId',          $sendData)) ? $sendData['userId']          : $curUserId;
        $lang            = (array_key_exists('lang',            $sendData) && $this->Core->i18n()->isMultilang()) ? $sendData['lang'] : $this->Core->i18n()->getCurrLang();
        $dirId           = (array_key_exists('dirId',           $sendData)) ? $sendData['dirId']           : 0;

        $cc = ($mailer_cc !== '') ? ','.$mailer_cc : $cc;
        $cc = trim($cc , ',');

        $bcc = ($mailer_bcc !== '') ? ','.$mailer_bcc : $bcc;
        $bcc = trim($bcc , ',');

        /** Set From Array */
        $from = (is_array($from)) ? $from : array($from);
        $fromMailAddress = ($from[0] == $mailer_from) ? $from[0] : $mailer_from;
        $fromMailName = (array_key_exists(1, $from)) ? $from[1] : $this->Core->Config()->get('default_title');

        $fromArray = ($fromMailName != '') ? array($fromMailAddress, $fromMailName) : array($fromMailAddress);

        $reply_to = ($from[0] != $mailer_from) ? $from[0] : $reply_to;

        if(count($curUser)) {
            $reply_to = $curUser['email'];
        }

        if($mailer_reply_to != '') {
            $reply_to = $mailer_reply_to;
        }

        $to = trim($to,',');

        /** Check for overwrite E-Mail Address */
        if (defined('EMAIL_ADDRESS_OVERRIDE') && EMAIL_ADDRESS_OVERRIDE != '') {
            $to = EMAIL_ADDRESS_OVERRIDE;
        }

        if ($ishtml && $message == strip_tags($message)) {
            $message = nl2br($message);
        }

        /** Has Placeholder */
        if((strpos($subject,'{{') !== false && strpos($subject,'}}')) || (strpos($message,'{{') !== false && strpos($message,'}}'))) {

            /**
             * Set ReplaceData Array From Variables
             */
            $replaceData = array();
            $replaceData['abeIds']   = $abeIds;
            $replaceData['to']       = $to;
            $replaceData['from']     = $fromArray;
            $replaceData['cc']       = $cc;
            $replaceData['bcc']      = $bcc;
            $replaceData['reply_to'] = $reply_to;
            $replaceData['lang']     = $lang;
            $replaceData['userId']   = $userId;
            $replaceData['dirId']    = $dirId;

            $replaceArray = $this->_prepareReplacements($replaceData);

            $subjectBeforeReplace = $subject;
            $messageBeforeReplace = $message;
            foreach($replaceArray as $search => $replace) {
                $subject = $this->replaceTemplatePlaceholder($search, $replace, $subject);
                $message = $this->replaceTemplatePlaceholder($search, $replace, $message);
            }
        }

        $messageSystemFooter = ($ishtml) ? '<hr /><p>' : PHP_EOL . '----------------------------------------------' . PHP_EOL;
        $messageSystemFooter .= $this->Core->i18n()->translate('Dies ist eine vom System generierte Nachricht. Bitte nicht direkt auf diese antworten!', $lang);
        $messageSystemFooter .= ($ishtml) ? '</p>' : PHP_EOL;


        /**
         * Set PreparedData Array From Variables
         */
        $preparedSendData = array();
        $preparedSendData['abeIds']          = $abeIds;
        $preparedSendData['to']              = $to;
        $preparedSendData['from']            = $fromArray;
        $preparedSendData['subject']         = $subject;
        $preparedSendData['message']         = $message.$messageSystemFooter;
        $preparedSendData['attachments']     = $attachments;
        $preparedSendData['cc']              = $cc;
        $preparedSendData['bcc']             = $bcc;
        $preparedSendData['reply_to']        = $reply_to;
        $preparedSendData['ishtml']          = $ishtml;
        $preparedSendData['lang']            = $lang;
        $preparedSendData['userId']          = $userId;
        $preparedSendData['dirId']           = $dirId;

        return $preparedSendData;
    }

    private function _prepareReplacements($replaceData)
    {
        /** Set Variables to Send */
        $settings             = $this->getSettings();
        $mailer_from          = $settings['mailer_from'];
        $mailer_reply_to      = $settings['mailer_reply_to'];
        $mailer_cc            = $settings['mailer_cc'];
        $mailer_bcc           = $settings['mailer_bcc'];
        $mailer_use_smtp      = $settings['mailer_use_smtp'];
        $mailer_smtp_host     = $settings['mailer_smtp_host'];
        $mailer_smtp_port     = $settings['mailer_smtp_port'];
        $mailer_smtp_secure   = $settings['mailer_smtp_secure'];
        $mailer_smtp_username = $settings['mailer_smtp_username'];
        $mailer_smtp_password = $settings['mailer_smtp_password'];

        /** @var $accClass Acc */
        $accClass  = $this->Mvc->modelClass('Acc');
        $curUser   = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        /** @var $mediaClass Media */
        $mediaClass = $this->Mvc->modelClass('Media');

        /** @var $clientClass Client */
        $clientClass = $this->Mvc->modelClass('Client');
        $curClient = (is_object($clientClass)) ? $clientClass->getClient() : array();


        /**
         * Set SendData Array To Variables
         */
        $abeIds          = (array_key_exists('abeIds',          $replaceData)) ? $replaceData['abeIds']          : '';
        $to              = (array_key_exists('to',              $replaceData)) ? $replaceData['to']              : '';
        $from            = (array_key_exists('from',            $replaceData)) ? $replaceData['from']            : $mailer_from;
        $cc              = (array_key_exists('cc',              $replaceData)) ? $replaceData['cc']              : '';
        $bcc             = (array_key_exists('bcc',             $replaceData)) ? $replaceData['bcc']             : '';
        $reply_to        = (array_key_exists('reply_to',        $replaceData)) ? $replaceData['reply_to']        : '';
        $userId          = (array_key_exists('userId',          $replaceData)) ? $replaceData['userId']          : $curUserId;
        $dirId           = (array_key_exists('dirId',           $replaceData)) ? $replaceData['dirId']           : 0;
        $lang            = (array_key_exists('lang',            $replaceData) && $this->Core->i18n()->isMultilang()) ? $replaceData['lang'] : $this->Core->i18n()->getCurrLang();


        /** User Data */
        $user = (is_object($accClass) && $userId !== 0 && $userId !== $curUserId && $accClass->userExists($userId)) ? $accClass->getUser($userId) : $curUser;

        /** Dir Data */
        $dir  = (is_object($mediaClass) && $dirId !== 0) ? $mediaClass->getDir($dirId) : array();

        /** Set From Array */
        $from = (is_array($from)) ? $from : array($from);
        $fromMailAddress = $from[0];
        $fromMailName = (array_key_exists(1, $from)) ? $from[1] : $this->Core->Config()->get('default_title');

        /** Address Book Data */
        $abToName = '';
        $abToSurname = '';
        $abToEmail = '';
        if($abeIds !== '') {
            $abeIdsArray = preg_split('/,/', $abeIds, -1, PREG_SPLIT_NO_EMPTY);

            foreach($abeIdsArray as $abeId) {
                $abEntry = $this->getAddressBookEntry($abeId);
                $abEntryEmail = (count($abEntry) && array_key_exists('email',$abEntry) && is_array($abEntry['email'])) ? implode(',',$abEntry['email']) : '';

                $to = $to . ',' . $abEntryEmail;

                $abToName    = (count($abEntry) && array_key_exists('name',$abEntry)) ? $abEntry['name'] : '';
                $abToSurname = (count($abEntry) && array_key_exists('surname',$abEntry)) ? $abEntry['surname'] : '';
                $abToEmail   = $abEntryEmail;
            }
        }

        $getAddressBookEntryByEmail = $this->getAddressBookEntryByEmail($to);
        if(count($getAddressBookEntryByEmail)) {
            $abToName    = (array_key_exists('name',$getAddressBookEntryByEmail)) ? $getAddressBookEntryByEmail['name'] : '';
            $abToSurname = (array_key_exists('surname',$getAddressBookEntryByEmail)) ? $getAddressBookEntryByEmail['surname'] : '';
            $abToEmail   = (array_key_exists('email',$getAddressBookEntryByEmail) && is_array($getAddressBookEntryByEmail['email'])) ? implode(',',$getAddressBookEntryByEmail['email']) : '';
        }

        $return = array();

        $getTemplatePlaceholder = $this->getTemplatePlaceholder();
        if(is_array($getTemplatePlaceholder)) {
            $replaceArray = array();
            foreach ($getTemplatePlaceholder as $placeholder => $description) {

                /** Basic Placeholder */
                switch ($placeholder) {
                    case '{{baseUrl}}':
                        $replaceArray[$placeholder] = $this->Core->getBaseUrl();
                        break;
                    case '{{projectName}}':
                        $replaceArray[$placeholder] = $this->Core->Config()->get('project_name');
                        break;
                    case '{{coreVersion}}':
                        $replaceArray[$placeholder] = $this->Core->getVersion();
                        break;
                    case '{{defaultTitle}}':
                        $replaceArray[$placeholder] = $this->Core->Config()->get('default_title');
                        break;
                }

                /** E-Mail Placeholder */
                switch ($placeholder) {
                    case '{{fromName}}':
                        if ($fromMailName != '') {
                            $replaceArray[$placeholder] = $fromMailName;
                        }
                        break;
                    case '{{fromEmail}}':
                        $replaceArray[$placeholder] = $fromMailAddress;
                        break;
                    case '{{abToName}}':
                        if ($fromMailName != '') {
                            $replaceArray[$placeholder] = $abToName;
                        }
                        break;
                    case '{{abToSurname}}':
                        if ($fromMailName != '') {
                            $replaceArray[$placeholder] = $abToSurname;
                        }
                        break;
                    case '{{abToEmail}}':
                        if ($fromMailName != '') {
                            $replaceArray[$placeholder] = $abToEmail;
                        }
                        break;
                    case '{{toEmail}}':
                        if ($fromMailName != '') {
                            $replaceArray[$placeholder] = $to;
                        }
                        break;
                }

                /** Replace User Placeholder */
                if (count($user)) {
                    switch ($placeholder) {

                        /** User Placeholder */
                        case '{{userGender}}':
                            if (array_key_exists('gender', $user)) {
                                if ($user['gender'] == 'female') {
                                    $replaceArray[$placeholder] = $this->Core->i18n()->translate('Frau', $lang);
                                }
                                if ($user['gender'] == 'male') {
                                    $replaceArray[$placeholder] = $this->Core->i18n()->translate('Herr', $lang);
                                }
                            } else {
                                $replaceArray[$placeholder] = '';
                            }
                            break;
                        case '{{salutation}}':
                            if (array_key_exists('gender', $user)) {
                                if ($user['gender'] == 'female') {
                                    $replaceArray[$placeholder] = $this->Core->i18n()->translate('Sehr geehrte Frau', $lang);
                                }
                                if ($user['gender'] == 'male') {
                                    $replaceArray[$placeholder] = $this->Core->i18n()->translate('Sehr geehrter Herr', $lang);
                                }
                            } else {
                                $replaceArray[$placeholder] = $this->Core->i18n()->translate('Hallo', $lang);
                            }
                            break;
                        case '{{userName}}':
                            $replaceArray[$placeholder] = (array_key_exists('name', $user)) ? $user['name'] : '';
                            break;
                        case '{{userSurname}}':
                            $replaceArray[$placeholder] = (array_key_exists('surname', $user)) ? $user['surname'] : '';
                            break;
                        case '{{userEmail}}':
                            $replaceArray[$placeholder] = (array_key_exists('email', $user)) ? $user['email'] : '';
                            break;
                    }
                }

                /** Replace Directory Placeholder */
                if (count($dir)) {
                    switch ($placeholder) {
                        /** User Placeholder */
                        case '{{dirUrl}}':
                            $replaceArray[$placeholder] = (array_key_exists('dir', $dir)) ? $this->Mvc->getModelUrl('media') . '/galleries/view/' . $dir['dir'] : '';
                            break;
                        case '{{dirPassword}}':
                            $replaceArray[$placeholder] = (array_key_exists('password', $dir)) ? $dir['password'] : '';
                            break;
                    }
                }

                /** Client Placeholder */
                if (count($curClient)) {
                    $client = $curClient;

                    switch ($placeholder) {

                        case '{{clientName}}':
                            if (count($client) && isset($client['name'])) {
                                $replaceArray[$placeholder] = $client['name'];
                            }
                            break;
                        case '{{clientContact}}':
                            if (count($client) && isset($client['contact'])) {
                                $replaceArray[$placeholder] = $client['contact'];
                            }
                            break;
                        case '{{clientAddressStreet}}':
                            if (count($client) && isset($client['address']) && array_key_exists('street', $client['address'])) {
                                $replaceArray[$placeholder] = $client['address']['street'];
                            }
                            break;
                        case '{{clientAddressStreetNo}}':
                            if (count($client) && isset($client['address']) && array_key_exists('streetno', $client['address'])) {
                                $replaceArray[$placeholder] = $client['address']['streetno'];
                            }
                            break;
                        case '{{clientAddressCity}}':
                            if (count($client) && isset($client['address']) && array_key_exists('city', $client['address'])) {
                                $replaceArray[$placeholder] = $client['address']['city'];
                            }
                            break;
                        case '{{clientAddressState}}':
                            if (count($client) && isset($client['address']) && array_key_exists('state', $client['address'])) {
                                $replaceArray[$placeholder] = $client['address']['state'];
                            }
                            break;
                        case '{{clientAddressCountry}}':
                            if (count($client) && isset($client['address']['country']['name'])) {
                                $replaceArray[$placeholder] = $this->Core->i18n()->translate($client['address']['country']['name'], $lang);
                            }
                            break;
                        case '{{clientAddressPhone}}':
                            if (count($client) && isset($client['address']) && array_key_exists('phone', $client['address'])) {
                                $replaceArray[$placeholder] = $client['address']['phone'];
                            }
                            break;
                        case '{{clientAddressFax}}':
                            if (count($client) && isset($client['address']) && array_key_exists('fax', $client['address'])) {
                                $replaceArray[$placeholder] = $client['address']['fax'];
                            }
                            break;
                        case '{{clientAddressMobile}}':
                            if (count($client) && isset($client['address']) && array_key_exists('mobile', $client['address'])) {
                                $replaceArray[$placeholder] = $client['address']['mobile'];
                            }
                            break;
                    }
                }
            }

            $return = $replaceArray;
        }

        return $return;
    }

    public function sendMail_new($sendData)
    {
        if(!$this->hasSettings()) {
            $return['success'] = false;
            $return['msg'] = $this->Core->i18n()->translate('Mailer - SMTP misconfiguration!');
        } else {

            if(!is_array($sendData) || !count($sendData)) {
                $return['success'] = false;
                $return['msg'] = $this->Core->i18n()->translate('Keine Daten zum Senden übergeben!');
                return $return;
            }

            /** Set Variables to Send */
            $settings             = $this->getSettings();
            $mailer_from          = $settings['mailer_from'];
            $mailer_reply_to      = $settings['mailer_reply_to'];
            $mailer_cc            = $settings['mailer_cc'];
            $mailer_bcc           = $settings['mailer_bcc'];
            $mailer_use_smtp      = $settings['mailer_use_smtp'];
            $mailer_smtp_host     = $settings['mailer_smtp_host'];
            $mailer_smtp_port     = $settings['mailer_smtp_port'];
            $mailer_smtp_secure   = $settings['mailer_smtp_secure'];
            $mailer_smtp_username = $settings['mailer_smtp_username'];
            $mailer_smtp_password = $settings['mailer_smtp_password'];

            /** @var $accClass Acc */
            $accClass  = $this->Mvc->modelClass('Acc');
            $curUser   = (is_object($accClass)) ? $accClass->getUser() : array();
            $curUserId = (count($curUser)) ? $curUser['id'] : 0;

            $preparedSendData = $this->_prepareSendData($sendData);

            /**
             * Set SendData Array To Variables
             */
            $abeIds          = (array_key_exists('abeIds',          $preparedSendData)) ? $preparedSendData['abeIds']          : '';
            $to              = (array_key_exists('to',              $preparedSendData)) ? $preparedSendData['to']              : '';
            $fromArray       = (array_key_exists('from',            $preparedSendData)) ? $preparedSendData['from']            : array($mailer_from);
            $subject         = (array_key_exists('subject',         $preparedSendData)) ? trim(strip_tags($preparedSendData['subject'])) : '';
            $message         = (array_key_exists('message',         $preparedSendData)) ? $preparedSendData['message']         : '';
            $attachments     = (array_key_exists('attachments',     $preparedSendData) && is_array($preparedSendData['attachments'])) ? $preparedSendData['attachments'] : array();
            $cc              = (array_key_exists('cc',              $preparedSendData)) ? $preparedSendData['cc']              : '';
            $bcc             = (array_key_exists('bcc',             $preparedSendData)) ? $preparedSendData['bcc']             : '';
            $reply_to        = (array_key_exists('reply_to',        $preparedSendData)) ? $preparedSendData['reply_to']        : '';
            $read_receipt_to = (array_key_exists('read_receipt_to', $preparedSendData)) ? $preparedSendData['read_receipt_to'] : '';
            $ishtml          = (array_key_exists('ishtml',          $preparedSendData)) ? $preparedSendData['ishtml']          : true;
            $saveMail        = (array_key_exists('saveMail',        $preparedSendData)) ? $preparedSendData['saveMail']        : true;
            $userId          = (array_key_exists('userId',          $preparedSendData)) ? $preparedSendData['userId']          : $curUserId;
            $lang            = (array_key_exists('lang',            $preparedSendData) && $this->Core->i18n()->isMultilang()) ? $preparedSendData['lang'] : $this->Core->i18n()->getCurrLang();
            $dirId           = (array_key_exists('dirId',           $preparedSendData)) ? $preparedSendData['dirId']           : 0;
            $system          = (array_key_exists('system',          $preparedSendData) && $preparedSendData['system'] === true) ? true : false;

            $cc = ($mailer_cc != '') ? $cc . ',' . $mailer_cc : $cc;
            $bcc = ($mailer_bcc != '') ? $bcc . ',' . $mailer_bcc : $bcc;

            $reply_to = ($fromArray[0] != $mailer_from) ? $fromArray[0] : $reply_to;

            if(count($curUser)) {
                $reply_to = $curUser['email'];
            }

            if($mailer_reply_to != '') {
                $reply_to = $mailer_reply_to;
            }

            /** Set To */
            if($abeIds !== '') {
                $abeIdsArray = preg_split('/,/', $abeIds, -1, PREG_SPLIT_NO_EMPTY);

                foreach($abeIdsArray as $abeId) {
                    $abEntry = $this->getAddressBookEntry($abeId);
                    $abEntryEmail = (count($abEntry) && array_key_exists('email',$abEntry) && is_array($abEntry['email'])) ? implode(',',$abEntry['email']) : '';

                    $to = $to . ',' . $abEntryEmail;
                }
            }
            $to = trim($to,',');

            if ($ishtml && $message == strip_tags($message)) {
                $message = nl2br($message);
            }

            //E-Mail verschicken:

            /** @var $mail PHPMailer */
            $mail = $this->_mailer();
            try {
                $mail->CharSet = 'UTF-8';

                //Server settings
                if($mailer_use_smtp) {
                    $mail->SMTPDebug = 2;                                 // Enable verbose debug output
                    $mail->isSMTP();                                      // Set mailer to use SMTP
                    $mail->Host = $mailer_smtp_host;                      // Specify main and backup SMTP servers
                    $mail->SMTPAuth = true;                               // Enable SMTP authentication
                    $mail->Username = $mailer_smtp_username;              // SMTP username
                    $mail->Password = $mailer_smtp_password;              // SMTP password
                    $mail->SMTPSecure = $mailer_smtp_secure;              // Enable TLS encryption, `ssl` also accepted
                    $mail->Port = $mailer_smtp_port;                      // TCP port to connect to
                }

                // Set Language
                $mail->setLanguage($lang);

                // Set From
                $mail->setFrom($fromArray[0], $fromArray[1]);

                //Recipients
                if(strpos($to,',') !== false) {
                    $toArray = preg_split('/,/', $to, -1, PREG_SPLIT_NO_EMPTY);
                    foreach($toArray as $toItem) {
                        if($this->Core->Helper()->check_email($toItem)) {
                            $mail->addAddress($toItem);     // Add a recipient
                        }
                    }
                } else {
                    $mail->addAddress($to);     // Add a recipient
                }

                $mail->addReplyTo($reply_to);

                if(strpos($cc,',') !== false) {
                    $ccArray = preg_split('/,/', $cc, -1, PREG_SPLIT_NO_EMPTY);
                    foreach($ccArray as $ccItem) {
                        if($this->Core->Helper()->check_email($ccItem)) {
                            $mail->addCC($ccItem);     // Add cc
                        }
                    }
                } else {
                    $mail->addCC($cc);     // Add cc
                }

                if(strpos($bcc,',') !== false) {
                    $bccArray = preg_split('/,/', $bcc, -1, PREG_SPLIT_NO_EMPTY);
                    foreach($bccArray as $bccItem) {
                        if($this->Core->Helper()->check_email($bccItem)) {
                            $mail->addBCC($bccItem);     // Add cc
                        }
                    }
                } else {
                    $mail->addBCC($bcc);     // Add cc
                }

                /** @TODO: Implement Attachment Functionality */
                //Attachments
                // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
                // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

                //Content
                $mail->isHTML($ishtml);                                  // Set email format to HTML
                $mail->Subject = $subject;
                $mail->Body    = $message;
                $mail->AltBody = strip_tags($message);

                $mailSent = $mail->send();
                $sent = 'OK';
                if(!$mailSent) {
                    $sent = 'Mailer Error: ' . $mail->ErrorInfo;
                }
            } catch (Exception $e) {
                $sent = 'Mailer Error: ' . $mail->ErrorInfo;
            }

            $return = array();

            if ($sent == 'OK') {
                $return['success'] = true;
                $return['msg'] = $this->Core->i18n()->translate('E-Mail wurde verschickt');
            } else {
                $return['success'] = false;
                $return['msg'] = sprintf($this->Core->i18n()->translate('E-Mail wurde nicht verschickt! Fehler: %s'), $sent);
            }

            $this->Core->Hooks()->fire('mailer_mail_send');

            if($saveMail) {
                /**
                 * Set SaveData Array From Variables
                 */
                $saveData = array();
                $saveData['abeIds']          = $abeIds;
                $saveData['to']              = $to;
                $saveData['from']            = $fromArray;
                $saveData['subject']         = $subject;
                $saveData['message']         = $message;
                $saveData['attachments']     = $attachments;
                $saveData['cc']              = $cc;
                $saveData['bcc']             = $bcc;
                $saveData['reply_to']        = $reply_to;
                $saveData['read_receipt_to'] = $read_receipt_to;
                $saveData['ishtml']          = $ishtml;
                $saveData['lang']            = $lang;
                $saveData['userId']          = $userId;
                $saveData['dirId']           = $dirId;
                $saveData['system']          = $system;
                $saveData['sentStatus']      = $return;

                $saveMailResult = $this->saveMail($saveData);
            }
        }

        return $return;
    }

    public function sendMail($sendData)
    {
        if(!$this->hasSettings()) {
            $return['success'] = false;
            $return['msg'] = $this->Core->i18n()->translate('Keine Zugangsdaten zu PITS Mailer Service hinterlegt!');
        } else {

            if(!is_array($sendData) || !count($sendData)) {
                $return['success'] = false;
                $return['msg'] = $this->Core->i18n()->translate('Keine Daten zum Senden übergeben!');
                return $return;
            }

            /** Check if Mailer Service exists an is up to date! */
            $service_url = $this->service_url;
            $incPath = $this->Core->getCoreRelPath() . DS . 'models' . DS . 'mailer' . DS . 'inc';
            $lib_local_file = $incPath . DS . 'MailerService.php';
//            $lib_remote_file = $service_url . $this->lib_remote_file;
//            if ((file_exists($lib_local_file)) && (hash_file('md5', $lib_local_file) == hash_file('md5', $lib_remote_file))) {
//                // nothing to do
//            } else {
//                $fileString = file_get_contents($lib_remote_file);
//                file_put_contents($lib_local_file, $fileString);
//            }

            /** Include Mailer Service */
            require_once($lib_local_file);

            /** Set Variables to Send */
            $settings          = $this->getSettings();
            $app_username      = $settings['app_username'];
            $app_password      = $settings['app_password'];
            $envelope_sender   = $settings['envelope_sender'];
            $useEnvelopeSender = $settings['use_envelope_sender'];
            $mailer_reply_to   = $settings['mailer_reply_to'];
            $mailer_cc         = $settings['mailer_cc'];
            $mailer_bcc        = $settings['mailer_bcc'];


            /** @var $accClass Acc */
            $accClass  = $this->Mvc->modelClass('Acc');
            $curUser   = (is_object($accClass)) ? $accClass->getUser() : array();
            $curUserId = (count($curUser)) ? $curUser['id'] : 0;

            /** @var $mediaClass Media */
            $mediaClass = $this->Mvc->modelClass('Media');

            /**
             * Set SendData Array To Variables
             */
            $abeIds          = (array_key_exists('abeIds',          $sendData)) ? $sendData['abeIds']          : '';
            $to              = (array_key_exists('to',              $sendData)) ? $sendData['to']              : '';
            $from            = (array_key_exists('from',            $sendData)) ? $sendData['from']            : $envelope_sender;
            $subject         = (array_key_exists('subject',         $sendData)) ? trim(strip_tags($sendData['subject'])) : '';
            $message         = (array_key_exists('message',         $sendData)) ? $sendData['message']         : '';
            $attachments     = (array_key_exists('attachments',     $sendData) && is_array($sendData['attachments'])) ? $sendData['attachments'] : array();
            $cc              = (array_key_exists('cc',              $sendData)) ? $sendData['cc']              : '';
            $bcc             = (array_key_exists('bcc',             $sendData)) ? $sendData['bcc']             : '';
            $reply_to        = (array_key_exists('reply_to',        $sendData)) ? $sendData['reply_to']        : '';
            $read_receipt_to = (array_key_exists('read_receipt_to', $sendData)) ? $sendData['read_receipt_to'] : '';
            $ishtml          = (array_key_exists('ishtml',          $sendData)) ? $sendData['ishtml']          : true;
            $nolog           = (array_key_exists('nolog',           $sendData)) ? $sendData['nolog']           : false;
            $extra           = (array_key_exists('extra',           $sendData) && is_array($sendData['extra'])) ? $sendData['extra'] : array();
            $saveMail        = (array_key_exists('saveMail',        $sendData)) ? $sendData['saveMail']        : true;
            $userId          = (array_key_exists('userId',          $sendData)) ? $sendData['userId']          : $curUserId;
            $lang            = (array_key_exists('lang',            $sendData) && $this->Core->i18n()->isMultilang()) ? $sendData['lang'] : $this->Core->i18n()->getCurrLang();
            $dirId           = (array_key_exists('dirId',           $sendData)) ? $sendData['dirId']           : 0;
            $system          = (array_key_exists('system',          $sendData) && $sendData['system'] === true) ? true : false;
            $dryrun          = (array_key_exists('dryrun',          $sendData) && $sendData['dryrun'] === true) ? true : false;

            $cc = ($mailer_cc !== '') ? ','.$mailer_cc : $cc;
            $cc = trim($cc , ',');

            $bcc = ($mailer_bcc !== '') ? ','.$mailer_bcc : $bcc;
            $bcc = trim($bcc , ',');

            $user = (is_object($accClass) && $userId !== 0 && $userId !== $curUserId && $accClass->userExists($userId)) ? $accClass->getUser($userId) : $curUser;

            $dir  = (is_object($mediaClass) && $dirId !== 0) ? $mediaClass->getDir($dirId) : array();

            /** Set From Array */
            $from = (is_array($from)) ? $from : array($from);
            $fromMailAddress = ($from[0] == $envelope_sender) ? $from[0] : $envelope_sender;
            $fromMailName = (array_key_exists(1, $from)) ? $from[1] : $this->Core->Config()->get('default_title');

            if ($useEnvelopeSender) {
                $extra['envelope_sender'] = $envelope_sender;
                $fromMailAddress = $from[0];
            }

            $fromArray = ($fromMailName != '') ? array($fromMailAddress, $fromMailName) : array($fromMailAddress);

            $reply_to = ($from[0] != $envelope_sender) ? $from[0] : $reply_to;

            if(count($curUser)) {
                $reply_to = $curUser['email'];
            }

            if($mailer_reply_to != '') {
                $reply_to = $mailer_reply_to;
            }

            $abToName = '';
            $abToSurname = '';
            $abToEmail = '';
            if($abeIds !== '') {
                $abeIdsArray = preg_split('/,/', $abeIds, -1, PREG_SPLIT_NO_EMPTY);

                foreach($abeIdsArray as $abeId) {
                    $abEntry = $this->getAddressBookEntry($abeId);
                    $abEntryEmail = (count($abEntry) && array_key_exists('email',$abEntry) && is_array($abEntry['email'])) ? implode(',',$abEntry['email']) : '';

                    $to = $to . ',' . $abEntryEmail;

                    $abToName    = (count($abEntry) && array_key_exists('name',$abEntry)) ? $abEntry['name'] : '';
                    $abToSurname = (count($abEntry) && array_key_exists('surname',$abEntry)) ? $abEntry['surname'] : '';
                    $abToEmail   = $abEntryEmail;
                }
            }

            $getAddressBookEntryByEmail = $this->getAddressBookEntryByEmail($to);
            if(count($getAddressBookEntryByEmail)) {
                $abToName    = (array_key_exists('name',$getAddressBookEntryByEmail)) ? $getAddressBookEntryByEmail['name'] : '';
                $abToSurname = (array_key_exists('surname',$getAddressBookEntryByEmail)) ? $getAddressBookEntryByEmail['surname'] : '';
                $abToEmail   = (array_key_exists('email',$getAddressBookEntryByEmail) && is_array($getAddressBookEntryByEmail['email'])) ? implode(',',$getAddressBookEntryByEmail['email']) : '';
            }

            $to = trim($to,',');

            /** Check for overwrite E-Mail Address */
            if (defined('EMAIL_ADDRESS_OVERRIDE') && EMAIL_ADDRESS_OVERRIDE != '') {
                $to = EMAIL_ADDRESS_OVERRIDE;
            }

            if ($ishtml && $message == strip_tags($message)) {
                $message = nl2br($message);
            }

            $site = $this->Core->Sites()->getSite();

            /** Has Placeholder */
            if((strpos($subject,'{{') !== false && strpos($subject,'}}')) || (strpos($message,'{{') !== false && strpos($message,'}}'))) {
                $getTemplatePlaceholder = $this->getTemplatePlaceholder();

                $replaceArray = array();
                foreach($getTemplatePlaceholder as $placeholder => $description) {
                    switch ($placeholder) {

                        /** Basic Placeholder */
                        case '{{baseUrl}}':
                            $replaceArray[$placeholder] = $this->Core->getBaseUrl();
                            break;
                        case '{{pipName}}':
                            $replaceArray[$placeholder] = $this->Core->Config()->get('project_name');
                            break;
                        case '{{pipVersion}}':
                            $replaceArray[$placeholder] = $this->Core->getVersion();
                            break;
                        case '{{defaultTitle}}':
                            $replaceArray[$placeholder] = $this->Core->Config()->get('default_title');
                            break;

                        /** Site Placeholder */
                        case '{{siteName}}':
                            if(count($site) && isset($site['name'])) {
                                $replaceArray[$placeholder] = $site['name'];
                            }
                            break;
                        case '{{siteContact}}':
                            if(count($site) && isset($site['contact'])) {
                                $replaceArray[$placeholder] = $site['contact'];
                            }
                            break;
                        case '{{siteAddressStreet}}':
                            if(count($site) && isset($site['address']) && array_key_exists('street', $site['address'])) {
                                $replaceArray[$placeholder] = $site['address']['street'];
                            }
                            break;
                        case '{{siteAddressStreetNo}}':
                            if(count($site) && isset($site['address']) && array_key_exists('streetno', $site['address'])) {
                                $replaceArray[$placeholder] = $site['address']['streetno'];
                            }
                            break;
                        case '{{siteAddressCity}}':
                            if(count($site) && isset($site['address']) && array_key_exists('city', $site['address'])) {
                                $replaceArray[$placeholder] = $site['address']['city'];
                            }
                            break;
                        case '{{siteAddressState}}':
                            if(count($site) && isset($site['address']) && array_key_exists('state', $site['address'])) {
                                $replaceArray[$placeholder] = $site['address']['state'];
                            }
                            break;
                        case '{{siteAddressCountry}}':
                            if(count($site) && isset($site['address']['country']['name'])) {
                                $replaceArray[$placeholder] = $this->Core->i18n()->translate($site['address']['country']['name'], $lang);
                            }
                            break;
                        case '{{siteAddressPhone}}':
                            if(count($site) && isset($site['address']) && array_key_exists('phone', $site['address'])) {
                                $replaceArray[$placeholder] = $site['address']['phone'];
                            }
                            break;
                        case '{{siteAddressFax}}':
                            if(count($site) && isset($site['address']) && array_key_exists('fax', $site['address'])) {
                                $replaceArray[$placeholder] = $site['address']['fax'];
                            }
                            break;
                        case '{{siteAddressMobile}}':
                            if(count($site) && isset($site['address']) && array_key_exists('mobile', $site['address'])) {
                                $replaceArray[$placeholder] = $site['address']['mobile'];
                            }
                            break;

                        /** E-Mail Placeholder */
                        case '{{fromName}}':
                            if($fromMailName != '') {
                                $replaceArray[$placeholder] = $fromMailName;
                            }
                            break;
                        case '{{fromEmail}}':
                            $replaceArray[$placeholder] = $fromMailAddress;
                            break;
                        case '{{abToName}}':
                            if($fromMailName != '') {
                                $replaceArray[$placeholder] = $abToName;
                            }
                            break;
                        case '{{abToSurname}}':
                            if($fromMailName != '') {
                                $replaceArray[$placeholder] = $abToSurname;
                            }
                            break;
                        case '{{abToEmail}}':
                            if($fromMailName != '') {
                                $replaceArray[$placeholder] = $abToEmail;
                            }
                            break;
                        case '{{toEmail}}':
                            if($fromMailName != '') {
                                $replaceArray[$placeholder] = $to;
                            }
                            break;
                    }

                    /** Replace User Placeholder */
                    if(count($user)) {
                        switch ($placeholder) {

                            /** User Placeholder */
                            case '{{userGender}}':
                                if(array_key_exists('gender', $user)) {
                                    if($user['gender'] == 'female') {
                                        $replaceArray[$placeholder] = $this->Core->i18n()->translate('Frau', $lang);
                                    }
                                    if($user['gender'] == 'male') {
                                        $replaceArray[$placeholder] = $this->Core->i18n()->translate('Herr', $lang);
                                    }
                                } else {
                                    $replaceArray[$placeholder] = '';
                                }
                                break;
                            case '{{salutation}}':
                                if(array_key_exists('gender', $user)) {
                                    if($user['gender'] == 'female') {
                                        $replaceArray[$placeholder] = $this->Core->i18n()->translate('Sehr geehrte Frau', $lang);
                                    }
                                    if($user['gender'] == 'male') {
                                        $replaceArray[$placeholder] = $this->Core->i18n()->translate('Sehr geehrter Herr', $lang);
                                    }
                                } else {
                                    $replaceArray[$placeholder] = $this->Core->i18n()->translate('Hallo', $lang);
                                }
                                break;
                            case '{{userName}}':
                                $replaceArray[$placeholder] = (array_key_exists('name', $user)) ? $user['name'] : '';
                                break;
                            case '{{userSurname}}':
                                $replaceArray[$placeholder] = (array_key_exists('surname', $user)) ? $user['surname'] : '';
                                break;
                            case '{{userEmail}}':
                                $replaceArray[$placeholder] = (array_key_exists('email', $user)) ? $user['email'] : '';
                                break;
                        }
                    }

                    /** Replace Directory Placeholder */
                    switch ($placeholder) {
                        /** User Placeholder */
                        case '{{dirUrl}}':
                            $dirPath = (array_key_exists('dirPath',$dir)) ? $dir['dirPath'] : '';
                            $galleryUrl = $this->Mvc->getModelUrl('media') . '/galleries/view' . str_replace(DS,'/',$dirPath);
                            $replaceArray[$placeholder] = (count($dir) && array_key_exists('dirPath', $dir)) ? $galleryUrl : '';
                            break;
                        case '{{dirPassword}}':
                            $replaceArray[$placeholder] = (count($dir) && array_key_exists('password', $dir)) ? $dir['password'] : '';
                            break;
                    }
                }

                $subjectBeforeReplace = $subject;
                $messageBeforeReplace = $message;
                foreach($replaceArray as $search => $replace) {
                    $subject = $this->replaceTemplatePlaceholder($search, $replace, $subject);
                    $message = $this->replaceTemplatePlaceholder($search, $replace, $message);
                }
            }

            $messageSystemFooter = ($ishtml) ? '<hr /><p>' : PHP_EOL . '----------------------------------------------' . PHP_EOL;
            $messageSystemFooter .= $this->Core->i18n()->translate('Dies ist eine vom System generierte Nachricht. Bitte nicht direkt auf diese antworten!', $lang);
            $messageSystemFooter .= ($ishtml) ? '</p>' : PHP_EOL;

            if($dryrun) {

                $return['success']   = true;
                $return['msg']       = $this->Core->i18n()->translate('E-Mail Dry Run');
                $return['send_data'] = array(
                    'service_url'     => $service_url,
                    'app_username'    => $app_username,
                    'app_password'    => $app_password,
                    'to'              => $to,
                    'from_array'      => $fromArray,
                    'subject'         => $subject,
                    'message'         => $message.$messageSystemFooter,
                    'attachments'     => $attachments,
                    'cc'              => $cc,
                    'bcc'             => $bcc,
                    'reply_to'        => $reply_to,
                    'read_receipt_to' => $read_receipt_to,
                    'ishtml'          => $ishtml,
                    'nolog'           => $nolog,
                    'extra'           => $extra
                );

                return $return;
            }

            //E-Mail verschicken:
            $sent = sendMailOverService(
                $service_url,
                $app_username,
                $app_password,
                $to,
                $fromArray,
                $subject,
                $message.$messageSystemFooter,
                $attachments,
                $cc,
                $bcc,
                $reply_to,
                $read_receipt_to,
                $ishtml,
                $nolog,
                $extra
            );

            $return = array();

            if ($sent == 'OK') {
                $return['success'] = true;
                $return['msg'] = $this->Core->i18n()->translate('E-Mail wurde verschickt');
            } else {
                $return['success'] = false;
                $return['msg'] = sprintf($this->Core->i18n()->translate('E-Mail wurde nicht verschickt! Fehler: %s'), $sent);
            }

            $this->Core->Hooks()->fire('mailer_mail_send');

            if($saveMail) {
                /**
                 * Set SaveData Array From Variables
                 */
                $saveData = array();
                $saveData['abeIds']          = $abeIds;
                $saveData['to']              = $to;
                $saveData['from']            = $fromArray;
                $saveData['subject']         = $subject;
                $saveData['message']         = $message.$messageSystemFooter;
                $saveData['attachments']     = $attachments;
                $saveData['cc']              = $cc;
                $saveData['bcc']             = $bcc;
                $saveData['reply_to']        = $reply_to;
                $saveData['read_receipt_to'] = $read_receipt_to;
                $saveData['ishtml']          = $ishtml;
                $saveData['lang']            = $lang;
                $saveData['userId']          = $userId;
                $saveData['dirId']           = $dirId;
                $saveData['system']          = $system;
                $saveData['sentStatus']      = $return;

                $saveMailResult = $this->saveMail($saveData);
            }
        }

        return $return;
    }

    /** Stored Mail Methods */

    private function _prepareMailData($data) {
        $preparedData = array();
        if(is_array($data) && count($data)) {
            $id               = (array_key_exists('id', $data))               ? $data['id']               : false;
            $to               = (array_key_exists('to', $data))               ? $data['to']               : '';
            $from             = (array_key_exists('from', $data) && is_array(unserialize($data['from']))) ? unserialize($data['from']) : array();
            $subject          = (array_key_exists('subject', $data))          ? $data['subject']          : '';
            $message          = (array_key_exists('message', $data))          ? $data['message']          : '';
            $attachments      = (array_key_exists('attachments', $data) && is_array(unserialize($data['attachments']))) ? unserialize($data['attachments']) : array();
            $cc               = (array_key_exists('cc', $data))               ? $data['cc']               : '';
            $bcc              = (array_key_exists('bcc', $data))              ? $data['bcc']              : '';
            $reply_to         = (array_key_exists('reply_to', $data))         ? $data['reply_to']         : '';
            $read_receipt_to  = (array_key_exists('read_receipt_to', $data))  ? $data['read_receipt_to']  : '';
            $ishtml           = (array_key_exists('ishtml', $data) && $data['ishtml'] === 1) ? true : false;
            $lang             = (array_key_exists('lang', $data))             ? $data['lang']             : '';
            $system           = (array_key_exists('system', $data) && $data['system'] === 1) ? true : false;
            $send_success     = (array_key_exists('send_success', $data) && $data['send_success'] === 1) ? true : false;
            $send_message     = (array_key_exists('send_message', $data))     ? $data['send_message']     : '';
            $created          = (array_key_exists('created', $data))          ? $data['created']          : 0;
            $id_acc_users     = (array_key_exists('id_acc_users', $data))     ? $data['id_acc_users']     : 0;
            $id_system_sites  = (array_key_exists('id_system_sites', $data))  ? $data['id_system_sites']  : 0;
            $id_media_dirs    = (array_key_exists('id_media_dirs', $data))    ? $data['id_media_dirs']    : 0;

            if($id !== false) {

                $preparedData['id']               = $id;
                $preparedData['to']               = $to;
                $preparedData['from']             = $from;
                $preparedData['subject']          = $subject;
                $preparedData['message']          = $message;
                $preparedData['attachments']      = $attachments;
                $preparedData['cc']               = $cc;
                $preparedData['bcc']              = $bcc;
                $preparedData['reply_to']         = $reply_to;
                $preparedData['read_receipt_to']  = $read_receipt_to;
                $preparedData['lang']             = $lang;
                $preparedData['ishtml']           = $ishtml;
                $preparedData['system']           = $system;
                $preparedData['send_success']     = $send_success;
                $preparedData['send_message']     = $send_message;
                $preparedData['created']          = $created;
                $preparedData['id_acc_users']     = $id_acc_users;
                $preparedData['id_system_sites']  = $id_system_sites;
                $preparedData['id_media_dirs']    = $id_media_dirs;
            }

        }
        return $preparedData;
    }

    public function getMails($siteId = null, $start = 0, $limit = null, $orderBy = null, $orderDirection = 'asc', $search = null, $forceAll = false) {

        $isSearch = ($search !== null && $search !== '');

        /** @var $accClass Acc */
        $accClass = $this->Mvc->modelClass('Acc');
        $curUser = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $isCurUserSu = (!is_object($accClass) || ($curUserId !== 0 && $curUser['su']));

        $whereUserSql = (!$isCurUserSu) ? ' WHERE `id_acc_users` = ' . $curUserId : '';
        $whereUserSql = ($forceAll) ? '' : $whereUserSql;
        $whereUserSql = ($isSearch && $whereUserSql == '') ? ' WHERE' : $whereUserSql;

        $searchSql = ($isSearch) ? $whereUserSql . ' lower(`from`) REGEXP lower("' . $search . '") OR lower(`to`) REGEXP lower("' . $search . '") OR lower(`subject`) REGEXP lower("' . $search . '")' : $whereUserSql;
        $orderBySql = ($orderBy !== null) ? ' ORDER BY `' . $orderBy . '` ' . $orderDirection : '';
        $limitSql = ($limit !== null) ? ' LIMIT ' . intval($start) . ',' . intval($limit) : '';

        $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'mailer_mails`' . $searchSql . $orderBySql . $limitSql . ';';
        $result = $this->Core->Db()->fromDatabase($sql,'@raw');

        $getMails = array();
        foreach($result as $row) {
            $preparedData = $this->_prepareMailData($row);
            if(count($preparedData)) {
                $getMails[$preparedData['id_system_sites']][$preparedData['id']] = $preparedData;
            }
        }

        return ($siteId !== null && is_int($siteId) && array_key_exists($siteId,$getMails)) ? $getMails[$siteId] : $getMails;
    }

    public function getMailsCount($siteId = null) {
        /** @var $accClass Acc */
        $accClass = $this->Mvc->modelClass('Acc');
        $curUser = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $isCurUserSu = (!is_object($accClass) || ($curUserId !== 0 && $curUser['su']));

        $whereUserSql = (!$isCurUserSu) ? ' WHERE `id_acc_users` = ' . $curUserId : '';

        $getMails = array();

        $sql = 'SELECT `id_system_sites`, COUNT(*) as \'count\' FROM `' . DB_TABLE_PREFIX . 'mailer_mails`' . $whereUserSql . ' GROUP BY `id_system_sites`;';
        $result = $this->Core->Db()->fromDatabase($sql,'@raw');

        foreach($result as $row) {
            $getMails[$row['id_system_sites']] = $row['count'];
        }

        $return = $getMails;

        if(!count($getMails)) {
            $return = 0;
        }

        if($siteId !== null && is_int($siteId)) {
            if(array_key_exists($siteId,$getMails)) {
                $return = $getMails[$siteId];
            } else {
                $return = 0;
            }
        }

        return $return;
    }

    public function getMail($mailId)
    {
        $mail = array();

        if($this->mailExists($mailId)) {
            $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'mailer_mails` WHERE `id` = :id;';
            $params = array(
                'id' => (int)$mailId
            );
            $result = $this->Core->Db()->fromDatabase($sql, '@raw', $params);

            foreach($result as $row) {
                $mail = $this->_prepareMailData($row);
            }
        }

        return $mail;
    }

    public function mailExists($mailId)
    {
        $sql = 'SELECT id FROM `' . DB_TABLE_PREFIX . 'mailer_mails` WHERE `id` = :id;';
        $params = array(
            'id' => (int)$mailId
        );
        $result = $this->Core->Db()->fromDatabase($sql,'@raw', $params);
        return (count($result)) ? true : false;
    }

    public function saveMail($saveData) {
        /** @var $accClass Acc */
        $accClass  = $this->Mvc->modelClass('Acc');
        $curUser   = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $siteId = $this->getUseSiteId();

        /**
         * Set SaveData Array To Variables
         */
        $abeIds          = (array_key_exists('abeIds',          $saveData)) ? $saveData['abeIds']          : '';
        $to              = (array_key_exists('to',              $saveData)) ? $saveData['to']              : '';
        $from            = (array_key_exists('from',            $saveData)) ? $saveData['from']            : '';
        $subject         = (array_key_exists('subject',         $saveData)) ? $saveData['subject']         : '';
        $message         = (array_key_exists('message',         $saveData)) ? $saveData['message']         : '';
        $attachments     = (array_key_exists('attachments',     $saveData) && is_array($saveData['attachments'])) ? $saveData['attachments'] : array();
        $cc              = (array_key_exists('cc',              $saveData)) ? $saveData['cc']              : '';
        $bcc             = (array_key_exists('bcc',             $saveData)) ? $saveData['bcc']             : '';
        $reply_to        = (array_key_exists('reply_to',        $saveData)) ? $saveData['reply_to']        : '';
        $read_receipt_to = (array_key_exists('read_receipt_to', $saveData)) ? $saveData['read_receipt_to'] : '';
        $ishtml          = (array_key_exists('ishtml',          $saveData)) ? $saveData['ishtml']          : true;
        $lang            = (array_key_exists('lang',            $saveData)) ? $saveData['lang']            : $this->Core->i18n()->getCurrLang();
        $userId          = (array_key_exists('userId',          $saveData) && $saveData['userId'] == $curUserId) ? $saveData['userId'] : 0;
        $dirId           = (array_key_exists('dirId',           $saveData)) ? $saveData['dirId']           : 0;
        $system          = (array_key_exists('system',          $saveData) && $saveData['system'] === true) ? true : false;
        $sentResult      = (array_key_exists('sentStatus',      $saveData) && is_array($saveData['sentStatus'])) ? $saveData['sentStatus'] : array();

        //MySQL Query für die Erstellung
        $mailinsertinto = "INSERT INTO `" . DB_TABLE_PREFIX . "mailer_mails` (`to`, `from`, `subject`, `message`, `attachments`, `cc`, `bcc`, `reply_to`, `read_receipt_to`, `ishtml`, `lang`, `system`, `send_success`, `send_message`, `id_acc_users`, `id_media_dirs`, `id_system_sites`, `id_acc_users__changedBy`) VALUES (:to, :from, :subject, :message, :attachments, :cc, :bcc, :reply_to, :read_receipt_to, :ishtml, :lang, :system, :send_success, :send_message, :id_acc_users, :id_media_dirs, :id_system_sites, :id_acc_users__changedBy)";
        $params = array(
            'to'               => $to,
            'from'             => (!is_array($from)) ? serialize(array($from)) : serialize($from),
            'subject'          => $subject,
            'message'          => $message,
            'attachments'      => (!is_array($attachments)) ? serialize(array($attachments)) : serialize($attachments),
            'cc'               => $cc,
            'bcc'              => $bcc,
            'reply_to'         => $reply_to,
            'read_receipt_to'  => $read_receipt_to,
            'ishtml'           => ($ishtml) ? 1 : 0,
            'lang'             => $lang,
            'system'           => ($system) ? 1 : 0,
            'send_success'     => ($sentResult['success']) ? 1 : 0,
            'send_message'     => $sentResult['msg'],
            'id_acc_users'     => $userId,
            'id_media_dirs'    => $dirId,
            'id_system_sites' => $siteId,
            'id_acc_users__changedBy' => $userId
        );

        $sqlResult = $this->Core->Db()->toDatabase($mailinsertinto, $params);

        $this->Core->Hooks()->fire('mailer_mail_saved');

        $mailId = $this->Core->Db()->lastInsertId();

        $success = false;
        if ($mailId == ''){
            $success = false;
        } else {
            $success = true;
        }
        return $success;
    }

    /** Address Book Methods */
    private function _prepareAddressBookData($data) {
        $preparedData = array();
        if(is_array($data) && count($data)) {
            $id               = (array_key_exists('id', $data))               ? $data['id']               : false;
            $name             = (array_key_exists('name', $data))             ? $data['name']             : '';
            $surname          = (array_key_exists('surname', $data))          ? $data['surname']          : '';
            $email            = (array_key_exists('email', $data))            ? $data['email']            : '';
            $comment          = (array_key_exists('comment', $data))          ? $data['comment']          : '';
            $id_acc_users     = (array_key_exists('id_acc_users', $data))     ? $data['id_acc_users']     : 0;
            $id_system_sites  = (array_key_exists('id_system_sites', $data))  ? $data['id_system_sites']  : 0;

            if($id !== false) {
                $emailsArray = array($email);

	            if(strpos($email,',') !== false) {
	                $emailsArray = array();
	                $emailsParts = preg_split('/,/', $email, -1, PREG_SPLIT_NO_EMPTY);
	                foreach($emailsParts as $emailEntry) {
	                    $emailsArray[] = trim($emailEntry);
	                }
	            }

                $preparedData['id']               = $id;
                $preparedData['name']             = $name;
                $preparedData['surname']          = $surname;
                $preparedData['email']            = $emailsArray;
                $preparedData['comment']          = $comment;
                $preparedData['id_acc_users']     = $id_acc_users;
                $preparedData['id_system_sites']  = $id_system_sites;
            }

        }
        return $preparedData;
    }

    public function getAddressBooks($siteId = null, $start = 0, $limit = null) {
        $limitSql = ($limit !== null) ? ' LIMIT ' . intval($start) . ',' . intval($limit) : '';

        $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'mailer_addressbooks`' . $limitSql . ';';
        $result = $this->Core->Db()->fromDatabase($sql,'@raw');

        $getAddressBooks = array();
        foreach($result as $row) {
            $preparedData = $this->_prepareAddressBookData($row);
            if(count($preparedData)) {
                $getAddressBooks[$preparedData['id_system_sites']][$preparedData['id_acc_users']][$preparedData['id']] = $preparedData;
            }
        }

        $return = $getAddressBooks;

        if($siteId !== null && is_int($siteId)) {
            if (array_key_exists($siteId, $getAddressBooks)) {
                $return = $getAddressBooks[$siteId];
            } else {
                $return = array();
            }
        }

        return $return;
    }

    public function getAddressBooksByCurrentUser() {
        $getAddressBooksByCurrentUser = array();

        /** @var $accClass Acc */
        $accClass = $this->Mvc->modelClass('Acc');

        $siteId = $this->getUseSiteId();

        $curUser   = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $canSeeGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_addressbook_global'));

        $getAddressBooksFull = $this->getAddressBooks($siteId);
        $getAddressBook = array();
        $getAddressBookGlobal = array();
        foreach($getAddressBooksFull as $userId => $addressBook) {
            if($userId != 0 && $userId != $curUserId) { continue; }
            if($userId == 0 && $canSeeGlobal) {
                $getAddressBookGlobal = $addressBook;
            }
            if($userId != 0 && $userId == $curUserId) {
                $getAddressBook = $addressBook;
            }
        }

        $getAddressBooksByCurrentUser['addressbook'] = $getAddressBook;
        $getAddressBooksByCurrentUser['addressbookGlobal'] = $getAddressBookGlobal;

        return $getAddressBooksByCurrentUser;
    }

    public function getAddressBookEntry($entryId) {
        $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'mailer_addressbooks` WHERE `id`=:entryId;';
        $params = array(
            'entryId' => $entryId
        );
        $result = $this->Core->Db()->fromDatabase($sql,'@raw', $params);
        $getAddressBookEntry = array();
        foreach($result as $row) {
            $preparedData = $this->_prepareAddressBookData($row);
            if(count($preparedData)) {
                $getAddressBookEntry = $preparedData;
            }
        }
        return $getAddressBookEntry;
    }

    public function saveAddressBookEntry($saveData) {
        $return = array();
        $return['success'] = false;
        $return['msg'] = $this->Core->i18n()->translate('Keine Daten zum Speichern übergeben');

        if(!is_array($saveData) || !count($saveData)) {
            return $return;
        }

        /** @var $accClass Acc */
        $accClass = $this->Mvc->modelClass('Acc');
        $user     = (is_object($accClass)) ? $accClass->getUser() : array();
        $userId   = (count($user)) ? $user['id'] : 0;

        $sites  = $this->Core->Sites()->getSites();
        $siteId = $this->getUseSiteId();

        $entryId       = (isset($saveData['id'])) ? $saveData['id'] : false;
        $entryName     = (isset($saveData['name'])) ? trim(strip_tags($saveData['name'])) : '';
        $entrySurname  = (isset($saveData['surname'])) ? trim(strip_tags($saveData['surname'])) : '';
        $entryEmail    = (isset($saveData['email'])) ? $saveData['email'] : '';
        $entryComment  = (isset($saveData['comment'])) ? $saveData['comment'] : '';
        $entryUserId   = (isset($saveData['id_acc_users']) && (is_object($accClass) && $accClass->userExists($saveData['id_acc_users']))) ? $saveData['id_acc_users'] : 0;
        $entryClientId = (isset($saveData['id_system_sites']) && array_key_exists($saveData['id_system_sites'], $sites)) ? $saveData['id_system_sites'] : $siteId;


        if($entryName != '' && $entrySurname != '' && $entryEmail != '') {

            if(strpos($entryEmail,',') !== false) {
                $entryEmailArray = array();
                $entryEmailParts = preg_split('/,/', $entryEmail, -1, PREG_SPLIT_NO_EMPTY);
                foreach($entryEmailParts as $emailAddress) {
                    $entryEmailArray[] = trim($emailAddress);
                }
                sort($entryEmailArray);
                $entryEmail = implode(", ", $entryEmailArray);
            }


            $createNew = ($entryId === false) ? true : false;
            if ($createNew) {
                //MySQL Query für die Erstellung
                $entryInsertInto = "INSERT INTO `" . DB_TABLE_PREFIX . "mailer_addressbooks` (`name`, `surname`, `email`, `comment`, `id_acc_users`, `id_system_sites`, `id_acc_users__changedBy`) VALUES (:name, :surname, :email, :comment, :id_acc_users, :id_system_sites, :id_acc_users__changedBy)";
                $params = array(
                    'name'             => $entryName,
                    'surname'          => $entrySurname,
                    'email'            => $entryEmail,
                    'comment'          => $entryComment,
                    'id_acc_users'     => $userId,
                    'id_system_sites'  => $entryClientId,
                    'id_acc_users__changedBy' => $entryUserId
                );

                $sqlResult = $this->Core->Db()->toDatabase($entryInsertInto, $params);

                $newEntryId = $this->Core->Db()->lastInsertId();

                if ($newEntryId == ''){
                    $return['success'] = false;
                    $return['msg'] = $this->Core->i18n()->translate('Konnte Eintrag nicht erstellen!');
                } else {
                    $return['success'] = true;
                    $return['msg'] = $this->Core->i18n()->translate('Eintrag erfolgreich erstellt!');
                }
            } else {

                $addressBooks = $this->getAddressBooks();
                $saveAddressBook = (isset($addressBooks[$entryClientId][$userId])) ? $addressBooks[$entryClientId][$userId] : array();
                $saveAddressBookGlobal = (isset($addressBooks[$entryClientId][0])) ? $addressBooks[$entryClientId][0] : array();

                if(array_key_exists($entryId, $saveAddressBook) || array_key_exists($entryId, $saveAddressBookGlobal)) {

                    //MySQL Query für die Bearbeitung
                    $entryUpdate = "
                    UPDATE
                        `" . DB_CONN_DBNAME . "`.`" . DB_TABLE_PREFIX . "mailer_addressbooks`
                    SET
                        `name` = '" . $entryName . "',
                        `surname` = '" . $entrySurname . "',
                        `email` = '" . $entryEmail . "',
                        `comment` = '" . $entryComment . "',
                        `id_acc_users` = '" . $entryUserId . "',
                        `id_system_sites` = '" . $entryClientId . "',
                        `id_acc_users__changedBy` = '" . $entryUserId . "'
                    WHERE
                        `" . DB_TABLE_PREFIX . "mailer_addressbooks`.`id` = '" . $entryId . "';";

                    $sqlResult = $this->Core->Db()->toDatabase($entryUpdate);

                    if (!$sqlResult) {
                        $return['success'] = false;
                        $return['msg'] = $this->Core->i18n()->translate('Eintrag konnte nicht gespeichert werden!');
                    } else {
                        $return['success'] = true;
                        $return['msg'] = $this->Core->i18n()->translate('Eintrag erfolgreich gespeichert!');
                    }
                }
            }
        }

        return $return;
    }

    public function deleteAddressBookEntry($entryId) {
        $return = array();
        $return['success'] = false;
        $return['msg'] = $this->Core->i18n()->translate('Keine Id zum löschen übergeben');

        if(!is_int($entryId)) {
            return $return;
        }

        /** @var $accClass Acc */
        $accClass = $this->Mvc->modelClass('Acc');
        $user     = (is_object($accClass)) ? $accClass->getUser() : array();
        $userId   = (count($user)) ? $user['id'] : 0;

        $siteId = $this->getUseSiteId();

        $canSeeGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_addressbook_global'));
        $canEditGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_addressbook_global_edit'));

        $addressBookEntry = array();
        if($entryId !== 0) {
            $getAddressBooksFull = $this->getAddressBooks($siteId);
            $getAddressBook = array();
            $getAddressBookGlobal = array();
            foreach($getAddressBooksFull as $abUserId => $addressBook) {
                if($abUserId != 0 && $abUserId != $userId) { continue; }
                if($abUserId == 0 && $canSeeGlobal) {
                    $getAddressBookGlobal = $addressBook;
                }
                if($abUserId != 0 && $abUserId == $userId) {
                    $getAddressBook = $addressBook;
                }
            }

            if(array_key_exists($entryId, $getAddressBook) || array_key_exists($entryId, $getAddressBookGlobal)) {
                $addressBookEntry = (isset($getAddressBook[$entryId])) ? $getAddressBook[$entryId] : $getAddressBookGlobal[$entryId];
            }
        }

        if(!count($addressBookEntry)) {
            $return['success'] = false;
            $return['msg'] = $this->Core->i18n()->translate('Adressbuch Eintrag konnte nicht gefunden werden');
            return $return;
        }

        $canDelete = (($addressBookEntry['id_system_sites'] == 0 && $canEditGlobal) || $addressBookEntry['id_system_sites'] == $siteId);
        if(!$canDelete) {
            $return['success'] = false;
            $return['msg'] = $this->Core->i18n()->translate('Keine Berechtigung zum löschen dieses Eintrags!');
        } else {
            $entryDeleteSql = "DELETE FROM `" . DB_TABLE_PREFIX . "mailer_addressbooks` WHERE id = '".$entryId."' LIMIT 1";
            $sqlResult = $this->Core->Db()->toDatabase($entryDeleteSql);
            if (!$sqlResult){
                $return['success'] = false;
                $return['msg'] = sprintf( $this->Core->i18n()->translate('MySQL Error: \'%s\''), $this->Core->Db()->errorInfo()[2]);
            } else {
                $return['success'] = true;
                $return['msg'] = $this->Core->i18n()->translate('Eintrag erfolgreich gelöscht!');
            }
        }
        return $return;
    }

    public function getAddressBookEntryByEmail($email) {

        $getAddressBooksByCurrentUser = $this->getAddressBooksByCurrentUser();
        $getAddressBook               = $getAddressBooksByCurrentUser['addressbook'];
        $getAddressBookGlobal         = $getAddressBooksByCurrentUser['addressbookGlobal'];

        $getAddressBookEntryByEmail = array();

        foreach($getAddressBook as $entryId => $entry) {
            if(is_array($entry) && array_key_exists('email', $entry) && !is_array($entry['email']) && strpos($entry['email'], ',') !== false) {
                if (implode(', ', $entry['email']) == $email) {
                    $getAddressBookEntryByEmail = $entry;
                    break;
                }
            }
        }

        if(!count($getAddressBookEntryByEmail)) {
            foreach($getAddressBookGlobal as $entryId => $entry) {
                if(implode(', ', $entry['email']) == $email) {
                    $getAddressBookEntryByEmail = $entry;
                    break;
                }
            }
        }

        return $getAddressBookEntryByEmail;
    }

    /** Template Methods */

    private function _prepareTemplateData($data) {
        $preparedData = array();
        if(is_array($data) && count($data)) {
            $id               = (array_key_exists('id', $data))               ? $data['id']               : false;
            $name             = (array_key_exists('name', $data))             ? $data['name']             : '';
            $subject          = (array_key_exists('subject', $data))          ? $data['subject']          : '';
            $content          = (array_key_exists('content', $data))          ? html_entity_decode($data['content']) : '';
            $ishtml           = (array_key_exists('ishtml', $data) && $data['ishtml'] === 1) ? true       : false;
            $code             = (array_key_exists('code', $data))             ? $data['code']             : $this->Core->Helper()->createCleanString($data['name']);
            $lang             = (array_key_exists('lang', $data))             ? $data['lang']             : $this->Core->i18n()->getCurrLang();
            $type             = (array_key_exists('type', $data))             ? $data['type']             : 0;
            $type_max         = (array_key_exists('type_max', $data))         ? $data['type_max']         : 0;
            $id_acc_users     = (array_key_exists('id_acc_users', $data))     ? $data['id_acc_users']     : 0;
            $id_system_sites  = (array_key_exists('id_system_sites', $data))  ? $data['id_system_sites']  : 0;

            if($type < 1) {
                $id_acc_users = 0;
            }

            if($id !== false) {
                $preparedData['id']               = $id;
                $preparedData['name']             = $name;
                $preparedData['subject']          = $subject;
                $preparedData['content']          = $content;
                $preparedData['ishtml']           = $ishtml;
                $preparedData['code']             = $code;
                $preparedData['lang']             = $lang;
                $preparedData['type']             = $type;
                $preparedData['type_max']         = $type_max;
                $preparedData['id_acc_users']     = $id_acc_users;
                $preparedData['id_system_sites']  = $id_system_sites;
            }
        }
        return $preparedData;
    }

    public function getTemplates($siteId = null, $start = 0, $limit = null, $nested = true)
    {
        $limitSql = ($limit !== null) ? ' LIMIT ' . intval($start) . ',' . intval($limit) : '';

        $langWhere = (!$this->Core->i18n()->isMultilang()) ? ' `lang`="'.$this->Core->i18n()->getCurrLang() . '"' : '';

        $whereSql = ($langWhere !== '') ? ' WHERE ' . $langWhere : '';

        $getTemplates = array();

        /** Get Database Templates */
        $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'mailer_templates`' . $whereSql . $limitSql . ';';
        $result = $this->Core->Db()->fromDatabase($sql, '@raw');

        foreach ($result as $row) {
            $preparedTemplateData = $this->_prepareTemplateData($row);
            if (count($preparedTemplateData)) {
                $getTemplates[$preparedTemplateData['id']] = $preparedTemplateData;
            }
        }

        /** Get Templates by Client Id */
        $byClientId = ($siteId !== null && is_int($siteId) && $siteId !== 0);

        if($byClientId) {
            $filteredTemplates = array();
            foreach($getTemplates as $template) {
                $tplClientId = $template['id_system_sites'];
                if($tplClientId === 0 || $tplClientId == $siteId) {
                    $filteredTemplates[$template['id']] = $template;
                }
            }

            $getTemplates = $filteredTemplates;
            $byClientId = true;
        }

        /** Nesting Templates */
        if($nested) {
            $nestedTemplates = array();
            foreach($getTemplates as $template) {
                if($byClientId) {
                    $nestedTemplates[$template['id_acc_users']][] = $template;
                } else {
                    $nestedTemplates[$template['id_system_sites']][$template['id_acc_users']][] = $template;
                }
            }

            $getTemplates = $nestedTemplates;
        }

        return $getTemplates;
    }

    public function getSystemTemplates(){

        $langWhere = (!$this->Core->i18n()->isMultilang()) ? ' `lang`="'.$this->Core->i18n()->getCurrLang() . '"' : '';
        $whereSql = ($langWhere !== '') ? $langWhere.' AND ' : '';

        $getTemplates = array();

        /** Get Database Templates */
        $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'mailer_templates` WHERE ' . $whereSql . '`type`=0;';
        $result = $this->Core->Db()->fromDatabase($sql, '@raw');

        foreach ($result as $row) {
            $preparedTemplateData = $this->_prepareTemplateData($row);
            if (count($preparedTemplateData)) {
                $getTemplates[$preparedTemplateData['code']][$preparedTemplateData['lang']] = $preparedTemplateData;
            }
        }

        return $getTemplates;
    }

    public function getTemplate($entryId) {
        $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'mailer_templates` WHERE `id`=:entryId;';
        $params = array(
            'entryId' => $entryId
        );
        $result = $this->Core->Db()->fromDatabase($sql,'@raw', $params);
        $getTemplateEntry = array();
        foreach($result as $row) {
            $preparedTemplateData = $this->_prepareTemplateData($row);
            if (count($preparedTemplateData)) {
                $getTemplateEntry = $preparedTemplateData;
            }
        }
        return $getTemplateEntry;
    }

    public function getTemplateByCode($code, $lang = null, $singleMode = true) {

        $return = array();

        if($code !== '') {
            $siteId = $this->getUseSiteId();

            /** @var $accClass Acc */
            $accClass = $this->Mvc->modelClass('Acc');

            $curUser   = (is_object($accClass)) ? $accClass->getUser() : array();
            $curUserId = (count($curUser)) ? $curUser['id'] : 0;

            if ($lang === null) {
                $lang = ($this->Core->i18n()->isMultilang()) ? $this->Core->i18n()->getCurrLang() : $this->Core->i18n()->getDefaultLang();
            } else {
                $langArray = ($this->Core->i18n()->isMultilang() && is_array($this->Core->i18n()->getLang($lang))) ? $this->Core->i18n()->getLang($lang) : $this->Core->i18n()->getLang($this->Core->i18n()->getDefaultLang());
                $lang = (count($langArray)) ? $langArray['code'] : $this->Core->i18n()->getDefaultLang();
            }

            $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'mailer_templates` WHERE (`id_system_sites`=0 OR `id_system_sites`=:siteId) AND (`id_acc_users`=0 OR `id_acc_users`=:userId) AND (`code`=:code AND `lang`=:lang);';
            $params = array(
                'siteId' => $siteId,
                'userId' => $curUserId,
                'code' => $code,
                'lang' => $lang
            );
            $result = $this->Core->Db()->fromDatabase($sql, '@raw', $params);

            if (count($result)) {
                $getTemplateEntry = array();
                $getTemplateEntryArray = array();
                foreach ($result as $row) {
                    $preparedTemplateData = $this->_prepareTemplateData($row);
                    if (count($preparedTemplateData)) {
                        $getTemplateEntryArray[$preparedTemplateData['type']] = $preparedTemplateData;
                    }
                }


                if ($singleMode) {
                    $systemTemplate   = (array_key_exists(0, $getTemplateEntryArray)) ? $getTemplateEntryArray[0] : array();
                    $globalTemplate   = (array_key_exists(1, $getTemplateEntryArray)) ? $getTemplateEntryArray[1] : array();
                    $personalTemplate = (array_key_exists(2, $getTemplateEntryArray)) ? $getTemplateEntryArray[2] : array();

                    if (count($systemTemplate)) {
                        $getTemplateEntry = $systemTemplate;
                    }

                    if (count($globalTemplate)) {
                        $getTemplateEntry = $globalTemplate;
                    }

                    if (count($personalTemplate)) {
                        $getTemplateEntry = $personalTemplate;
                    }

                    $return = $getTemplateEntry;
                } else {
                    $return = $getTemplateEntryArray;
                }
            }
        }

        return $return;
    }

    public function getTemplatesByCurrentUser() {
        $getTemplatesByCurrentUser = array();

        /** @var $accClass Acc */
        $accClass = $this->Mvc->modelClass('Acc');

        $siteId = $this->getUseSiteId();

        $curUser   = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $langWhere = (!$this->Core->i18n()->isMultilang()) ? ' `lang`="'.$this->Core->i18n()->getCurrLang() . '"' : '';
        $whereSql = ($langWhere !== '') ? $langWhere.' AND ' : '';

        $sql = 'SELECT * FROM `' . DB_TABLE_PREFIX . 'mailer_templates` WHERE ' . $whereSql . '(`id_system_sites`=0 OR `id_system_sites`=:siteId) AND (`id_acc_users`=0 OR `id_acc_users`=:userId);';
        $params = array(
            'siteId' => $siteId,
            'userId' => $curUserId
        );
        $result = $this->Core->Db()->fromDatabase($sql,'@raw', $params);
        $getTemplatesFull = array();
        foreach($result as $row) {
            $preparedTemplateData = $this->_prepareTemplateData($row);
            if (count($preparedTemplateData)) {
                if(!$this->Core->i18n()->isMultilang() && ($preparedTemplateData['lang'] !== $this->Core->i18n()->getDefaultLang())) { continue; }
                $getTemplatesFull[$preparedTemplateData['id']] = $preparedTemplateData;
            }
        }

        /** Nesting Templates */
        $nestedTemplates = array();
        foreach($getTemplatesFull as $template) {
            $nestedTemplates[$template['type']][$template['id']] = $template;
        }

        $getTemplatesFullBackup = $getTemplatesFull;
        $getTemplatesFull = $nestedTemplates;

        $canSeeGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_templates_global'));

        $getTemplates = array();
        $getTemplatesGlobal = array();
        $getTemplatesSystem = array();
        foreach($getTemplatesFull as $templateType => $templateEntries) {
            if($templateType == 2) {
                foreach($templateEntries as $entryId => $entry) {
                    $getTemplates[$entryId] = $entry;
                }
            }
            if($templateType === 1) {
                foreach($templateEntries as $entryId => $entry) {
                    $getTemplatesGlobal[$entryId] = $entry;
                }
            }
            if($templateType === 0) {
                foreach($templateEntries as $entryId => $entry) {
                    $getTemplatesSystem[$entryId] = $entry;
                }
            }
        }

        $getTemplatesByCurrentUser['templates']       = $getTemplates;
        $getTemplatesByCurrentUser['templatesGlobal'] = ($canSeeGlobal) ? $getTemplatesGlobal : array();
        $getTemplatesByCurrentUser['templatesSystem'] = ($canSeeGlobal) ? $getTemplatesSystem : array();

        return $getTemplatesByCurrentUser;
    }

    public function saveTemplate($saveData) {
        $return = array();
        $return['success'] = false;
        $return['msg'] = $this->Core->i18n()->translate('Keine Daten zum Speichern übergeben');

        if(!is_array($saveData) || !count($saveData)) {
            return $return;
        }

        /** @var $accClass Acc */
        $accClass = $this->Mvc->modelClass('Acc');
        $user     = (is_object($accClass)) ? $accClass->getUser() : array();
        $userId   = (count($user)) ? $user['id'] : 0;

        $sites  = $this->Core->Sites()->getSites();
        $siteId = $this->getUseSiteId();

        $entryId      = (array_key_exists('id'     , $saveData)) ? $saveData['id']                        : false;
        $entryName    = (array_key_exists('name'   , $saveData)) ? trim(strip_tags($saveData['name']))    : '';
        $entryCode    = (array_key_exists('code'   , $saveData)) ? trim(strip_tags($saveData['code']))    : $this->Core->Helper()->createCleanString($entryName);
        $entryLang    = (array_key_exists('lang'   , $saveData)) ? $saveData['lang']                      : $this->Core->i18n()->getCurrLang();
        $entrySubject = (array_key_exists('subject', $saveData)) ? trim(strip_tags($saveData['subject'])) : '';
        $entryContent = (array_key_exists('content', $saveData)) ? $saveData['content']                   : '';
        $entryIshtml  = (array_key_exists('ishtml' , $saveData) && $saveData['ishtml'] === 1) ? 1         : 0;
        $entryUserId  = (array_key_exists('id_acc_users', $saveData) && (is_object($accClass) && $accClass->userExists($saveData['id_acc_users']))) ? $saveData['id_acc_users'] : 0;
        $entrySiteId  = (array_key_exists('id_system_sites', $saveData) && array_key_exists($saveData['id_system_sites'], $sites)) ? $saveData['id_system_sites'] : $siteId;
        $entryType    = (array_key_exists('type'   , $saveData)) ? $saveData['type']                      : 2;
        $entryTypeMax = 2;

        if($entryType < 2) {
            $entryUserId = 0;
        }

        if($entryName != '' && $entryContent != '') {
            $createNew = ($entryId === false) ? true : false;
            if ($createNew) {

                $saveEntry = $this->getTemplateByCode($entryCode,$entryLang);

                if(count($saveEntry)) {
                    if($saveEntry['type'] === 0) {
                        $entryTypeMax = $saveEntry['type_max'];
                        $entryType    = ($entryType < 1) ? 1 : $entryType;
                        $entryType    = (($entryType != $entryTypeMax) && ($entryType > $entryTypeMax)) ? $entryTypeMax : $entryType;
                    } else {
                        if (
                            $saveEntry['id_system_sites'] == $siteId &&
                            $saveEntry['id_acc_users'] == $userId &&
                            $saveEntry['type'] == $entryType
                        ) {
                            $entryName = $entryName . ' - 2';
                            $entryCode = $entryCode . '_2';
                        }
                    }
                }

                //MySQL Query für die Erstellung
                $entryInsertInto = "INSERT INTO `" . DB_TABLE_PREFIX . "mailer_templates` (`name`, `subject`, `content`, `ishtml`, `code`, `lang`, `type`, `type_max`, `id_acc_users`, `id_system_sites`, `id_acc_users__changedBy`) VALUES (:name, :subject, :content, :ishtml, :code, :lang, :type, :type_max, :id_acc_users, :id_system_sites, :id_acc_users__changedBy)";
                $params = array(
                    'name'             => $entryName,
                    'subject'          => $entrySubject,
                    'content'          => $entryContent,
                    'ishtml'           => $entryIshtml,
                    'code'             => $entryCode,
                    'lang'             => $entryLang,
                    'type'             => $entryType,
                    'type_max'         => $entryTypeMax,
                    'id_acc_users'     => $entryUserId,
                    'id_system_sites'  => $entrySiteId,
                    'id_acc_users__changedBy' => $userId
                );

                $sqlResult = $this->Core->Db()->toDatabase($entryInsertInto, $params);

                $newEntryId = $this->Core->Db()->lastInsertId();

                if ($newEntryId == ''){
                    $return['success'] = false;
                    $return['msg'] = $this->Core->i18n()->translate('Konnte Vorlage nicht erstellen!');
                } else {
                    $return['success'] = true;
                    $return['msg'] = $this->Core->i18n()->translate('Vorlage erfolgreich erstellt!');
                }
            } else {

                $getEntry = ($entryId !== false) ? $this->getTemplate($entryId) : array();

                $saveTemplate       = (count($getEntry) && array_key_exists('id_acc_users',$getEntry) && $getEntry['id_acc_users'] == $userId) ? $getEntry : array();
                $saveTemplateGlobal = (count($getEntry) && array_key_exists('id_acc_users',$getEntry) && $getEntry['id_acc_users'] === 0) ? $getEntry : array();

                if(count($saveTemplate) || count($saveTemplateGlobal)) {

                    //MySQL Query für die Bearbeitung
                    $entryUpdate = "
                    UPDATE
                        `" . DB_CONN_DBNAME . "`.`" . DB_TABLE_PREFIX . "mailer_templates`
                    SET
                        `name`             = :name,
                        `subject`          = :subject,
                        `content`          = :content,
                        `ishtml`           = :ishtml,
                        `code`             = :code,
                        `lang`             = :lang,
                        `type`             = :type,
                        `id_acc_users`     = :id_acc_users,
                        `id_system_sites`  = :id_system_sites,
                        `id_acc_users__changedBy` = :id_acc_users__changedBy
                    WHERE
                        `" . DB_TABLE_PREFIX . "mailer_templates`.`id` = :entryId;";

                    $params = array(
                        'entryId'          => $entryId,
                        'name'             => $entryName,
                        'subject'          => $entrySubject,
                        'content'          => $entryContent,
                        'ishtml'           => $entryIshtml,
                        'code'             => $entryCode,
                        'lang'             => $entryLang,
                        'type'             => $entryType,
                        'id_acc_users'     => $entryUserId,
                        'id_system_sites'  => $entrySiteId,
                        'id_acc_users__changedBy' => $userId
                    );

                    $sqlResult = $this->Core->Db()->toDatabase($entryUpdate, $params);

                    if (!$sqlResult) {
                        $return['success'] = false;
                        $return['msg'] = $this->Core->i18n()->translate('Vorlage konnte nicht gespeichert werden!');
                    } else {
                        $return['success'] = true;
                        $return['msg'] = $this->Core->i18n()->translate('Vorlage erfolgreich gespeichert!');
                    }
                }
            }
        }

        return $return;
    }

    public function deleteTemplate($entryId) {
        $return = array();
        $return['success'] = false;
        $return['msg'] = $this->Core->i18n()->translate('Keine Id zum löschen übergeben');

        if(!is_int($entryId)) {
            return $return;
        }

        /** @var $accClass Acc */
        $accClass = $this->Mvc->modelClass('Acc');
        $user     = (is_object($accClass)) ? $accClass->getUser() : array();
        $userId   = (count($user)) ? $user['id'] : 0;

        $siteId = $this->getUseSiteId();

        $canSeeGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_templates_global'));
        $canEditGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_templates_global_edit'));

        $template = array();
        if($entryId !== 0) {
            $userTemplates = $this->getTemplatesByCurrentUser();

            $templates       = $userTemplates['templates'];
            $templatesGlobal = $userTemplates['templatesGlobal'];

            if(array_key_exists($entryId, $templates) || array_key_exists($entryId, $templatesGlobal)) {
                $template = (isset($templates[$entryId])) ? $templates[$entryId] : $templatesGlobal[$entryId];
            }
        }

        if(!count($template)) {
            $return['success'] = false;
            $return['msg'] = $this->Core->i18n()->translate('Vorlage konnte nicht gefunden werden');
            return $return;
        }

        $canDelete = ($template['type'] > 0 && (($template['type'] == 2 && $template['id_acc_users'] == $userId) || ($template['type'] === 1 && $canEditGlobal)));
        if(!$canDelete) {
            $return['success'] = false;
            $return['msg'] = $this->Core->i18n()->translate('Keine Berechtigung zum löschen dieser Vorlage!');
        } else {
            $entryDeleteSql = "DELETE FROM `" . DB_TABLE_PREFIX . "mailer_templates` WHERE id = '".$entryId."' LIMIT 1";
            $sqlResult = $this->Core->Db()->toDatabase($entryDeleteSql);
            if (!$sqlResult){
                $return['success'] = false;
                $return['msg'] = sprintf( $this->Core->i18n()->translate('MySQL Error: \'%s\''), $this->Core->Db()->errorInfo()[2]);
            } else {
                $return['success'] = true;
                $return['msg'] = $this->Core->i18n()->translate('Vorlage erfolgreich gelöscht!');
            }
        }
        return $return;
    }

    public function getTemplatePlaceholder() {
        /** @todo Get Template Placeholder List from DB? */
        $getTemplatePlaceholder = array(
            /** Basic Placeholder */
            '{{baseUrl}}'      => $this->Core->i18n()->translate('Wird ersetzt mit der PITs Portal Url'),
            '{{pipName}}'      => $this->Core->i18n()->translate('Wird ersetzt mit dem PITs Portal Name'),
            '{{pipVersion}}'   => $this->Core->i18n()->translate('Wird ersetzt mit der PITs Portal Version'),
            '{{defaultTitle}}' => $this->Core->i18n()->translate('Wird ersetzt mit dem Standard Titel'),

            /** Acc User Placeholder */
            '{{userGender}}'   => $this->Core->i18n()->translate('Wird ersetzt mit der Benutzer Andrede "Frau" oder "Herr"'),
            '{{salutation}}'   => $this->Core->i18n()->translate('Wird ersetzt mit der personalisierten Begrüßungsformel "Sehr geehrte Frau", "Sehr geehrter Herr" oder "Hallo"'),
            '{{userName}}'     => $this->Core->i18n()->translate('Wird ersetzt mit dem Benutzer Vornamen'),
            '{{userSurname}}'  => $this->Core->i18n()->translate('Wird ersetzt mit dem Benutzer Nachnamen'),
            '{{userEmail}}'    => $this->Core->i18n()->translate('Wird ersetzt mit der Benutzer E-Mail Adresse'),

            /** Media Directory Placeholder */
            '{{dirUrl}}'       => $this->Core->i18n()->translate('Wird ersetzt mit der Galerie Url des Media Ordners'),
            '{{dirPassword}}'  => $this->Core->i18n()->translate('Wird ersetzt mit dem Galerie Passwort des Media Ordners'),

            /** Site Placeholder */
            '{{siteName}}'            => $this->Core->i18n()->translate('Wird ersetzt mit dem aktuellen Seiten Namen'),
            '{{siteContact}}'         => $this->Core->i18n()->translate('Wird ersetzt mit dem aktuellen Seiten Kontakt'),
            '{{siteAddressStreet}}'   => $this->Core->i18n()->translate('Wird ersetzt mit der Straße aus der Adresse der aktuellen Seite'),
            '{{siteAddressStreetNo}}' => $this->Core->i18n()->translate('Wird ersetzt mit der Hausnummer aus der Adresse der aktuellen Seite'),
            '{{siteAddressCity}}'     => $this->Core->i18n()->translate('Wird ersetzt mit der Stadt aus der Adresse der aktuellen Seite'),
            '{{siteAddressState}}'    => $this->Core->i18n()->translate('Wird ersetzt mit dem Bundesland / Staat aus der Adresse der aktuellen Seite'),
            '{{siteAddressCountry}}'  => $this->Core->i18n()->translate('Wird ersetzt mit dem Namen des Landes aus der Adresse der aktuellen Seiten'),
            '{{siteAddressPhone}}'    => $this->Core->i18n()->translate('Wird ersetzt mit der Telefonnummer aus der Adresse der aktuellen Seite'),
            '{{siteAddressFax}}'      => $this->Core->i18n()->translate('Wird ersetzt mit der Faxnummer aus der Adresse der aktuellen Seite'),
            '{{siteAddressMobile}}'   => $this->Core->i18n()->translate('Wird ersetzt mit der Handynummer aus der Adresse der aktuellen Seite'),

            /** E-Mail Placeholder */
            '{{fromName}}'     => $this->Core->i18n()->translate('Wird ersetzt mit dem Absender Vornamen. Falls nicht vorhanden, Wird der Standard Titel verwendet'),
            '{{fromEmail}}'    => $this->Core->i18n()->translate('Wird ersetzt mit der Absender E-Mail Adresse'),
            '{{abToName}}'     => $this->Core->i18n()->translate('Wird ersetzt mit dem Empfänger Vornamen des verwendeten Adressbuch Eintrags.'),
            '{{abToSurname}}'  => $this->Core->i18n()->translate('Wird ersetzt mit dem Empfänger Nachnamen des verwendeten Adressbuch Eintrags.'),
            '{{abToEmail}}'    => $this->Core->i18n()->translate('Wird ersetzt mit der E-Mail des verwendeten Adressbuch Eintrags.'),
            '{{toEmail}}'      => $this->Core->i18n()->translate('Wird ersetzt mit der Empfänger E-Mail Adresse'),
        );
        return $getTemplatePlaceholder;
    }

    public function replaceTemplatePlaceholder($search, $replace, $string) {
        $getTemplatePlaceholder = $this->getTemplatePlaceholder();
        $stringReplaced = $string;
        if(array_key_exists($search,$getTemplatePlaceholder)) {
            $stringReplaced = str_replace($search, $replace, $string);
        }
        return $stringReplaced;
    }

}