<?php
/**
 * Mailer Model View Mail Write
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $mailerClass Mailer
 * @var $accClass Acc
 * @var $mediaClass Media
 * @var $plugins Plugins
 * @var $forms Forms
 */

/** Prepare Forms Plugin */
$plugins = $Core->Plugins();
$forms   = $plugins->Forms();
$isForms = (is_object($forms));

$mediaClass = $Mvc->modelClass('Media');

$mailSendUrl    = $Mvc->getModelUrl() . '/mail/send';
$accClass       = $Mvc->modelClass('Acc');
$thisUser       = (is_object($accClass)) ? $accClass->getUser() : array();
$mailerSettings = $mailerClass->getSettings();

$lang           = (isset($lang)) ? $lang : $Core->i18n()->getCurrLang();

$dirId          = (isset($dirId)) ? $dirId : 0;
$dir            = (is_object($mediaClass) && $dirId !== 0) ? $mediaClass->getDir($dirId) : array();
?>
<form id="mail_write_form" class="form-horizontal" action="<?php echo $mailSendUrl ?>" method="post">

    <div class="action-wrapper row">
        <div class="col-sm-12">
            <div class="btn-set top text-right">
                <button type="submit" name="action" value="mail_send" class="btn btn-primary" data-action="mail_send">
                    <i class="fa fa-paper-plane" aria-hidden="true"></i><span class="hidden-xs"> <?php echo $Core->i18n()->translate('Senden'); ?></span>
                </button>
                <?php if($accClass->hasAccess('mailer_mail')) : ?>
                    <a href="<?php echo $Mvc->getModelUrl(); ?>" class="btn btn-default" title="<?php echo $Core->i18n()->translate('zurück zur Übersicht'); ?>">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <fieldset>

        <legend><?php echo $Core->i18n()->translate('E-Mail schreiben') ?></legend>

        <?php if(count($dir)) : ?>
            <div class="form-group">
                <label for="from" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Ordner') ?></label>

                <div class="col-md-10">
                    <?php
                    /** Hidden Dir Id Element */
                    $formElementData['name']          = 'dirId';
                    $formElementData['label']         = false;
                    $formElementData['value']         = $dir['id'];
                    $formElementData['type']         = 'hidden';
                    echo ($isForms) ? $forms->buildElement($formElementData) : '';
                    ?>
                    <p class="form-control-static"><i class="fa fa-folder" aria-hidden="true"></i> <?php echo $dir['name'] ?></p>
                </div>
            </div>
        <?php endif; ?>

        <?php if($Core->i18n()->isMultilang()) : ?>
            <div class="form-group">
                <label for="lang" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Sprache') ?></label>

                <div class="col-md-10">
                    <?php
                    $dlAllValue = array();
                    foreach ($Core->i18n()->getLanguages() as $langCode => $language) {
                        $dlAllValue[$langCode] = $language['name'];
                    }
                    /** Lang Element */
                    $formElementData['type']    = 'select';
                    $formElementData['id']      = 'lang';
                    $formElementData['name']    = 'lang';
                    $formElementData['label']   = false;
                    $formElementData['value']   = $lang;
                    $formElementData['options'] = $dlAllValue;
                    echo ($isForms) ? $forms->buildElement($formElementData) : '';
                    ?>
                    <p class="help-block"><?php echo $Core->i18n()->translate('Die Platzhalter werden anhand dieser Sprache übersetzt'); ?></p>
                </div>
            </div>
        <?php endif; ?>

        <?php if(count($thisUser)): ?>
            <?php $fromMail = ($mailerSettings['use_envelope_sender']) ? $thisUser['email'] : $mailerSettings['envelope_sender']; ?>
            <?php $fromMailName = $thisUser['name'] . ' ' . $thisUser['surname'] . ' (' . $Core->Config()->get('default_title') . ')'; ?>
        <?php else: ?>
            <?php $fromMail = $mailerSettings['envelope_sender']; ?>
            <?php $fromMailName = $Core->Config()->get('default_title'); ?>
        <?php endif; ?>
        <?php
        /** Hidden From Mail Element */
        $formElementData['name']  = 'from[]';
        $formElementData['label'] = false;
        $formElementData['value'] = $fromMail;
        $formElementData['type']  = 'hidden';
        echo ($isForms) ? $forms->buildElement($formElementData) : '';
        ?>
        <?php
        /** Hidden From Mail Name Element */
        $formElementData['name']  = 'from[]';
        $formElementData['label'] = false;
        $formElementData['value'] = $fromMailName;
        $formElementData['type']  = 'hidden';
        echo ($isForms) ? $forms->buildElement($formElementData) : '';
        ?>
        <div class="form-group">
            <label for="from" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Absender') ?></label>

            <div class="col-md-10">
                <p class="form-control-static">&quot;<?php echo $fromMailName ?>&quot; &lt;<?php echo $fromMail ?>&gt;</p>
            </div>
        </div>

        <div class="form-group">
            <label for="to" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Empfänger') ?> <em>*</em></label>

            <?php $abeId = ''; ?>
            <div class="col-md-10" id="write-to-wrapper">
                <div id="ab-entries">
                    <?php if(count($addressBookEntry)) : ?>
                        <?php $abeId = $addressBookEntry['id']; ?>
                        <div class="well well-sm" id="abe-<?php echo $abeId; ?>" title="<?php echo $writeTo ?>">
                            <i class="fa fa-id-card-o" aria-hidden="true"></i> <?php echo $addressBookEntry['name'] . ' ' . $addressBookEntry['surname'] ?>
                            <span data-id="<?php echo $abeId; ?>" class="btn btn-link btn-xs btn-remove" title="<?php echo sprintf($Core->i18n()->translate('Empfänger \'%s\' entfernen'), $addressBookEntry['name'] . ' ' . $addressBookEntry['surname']); ?>">
                                &times;
                            </span>
                        </div>
                    <?php endif; ?>
                </div>
                <?php
                $isRequired = ($abeId == '') ? true : false;
                /** Hidden abeIds Element */
                $formElementData['id']    = 'abe-input';
                $formElementData['name']  = 'abeIds';
                $formElementData['label'] = false;
                $formElementData['value'] = $abeId;
                $formElementData['type']  = 'hidden';
                echo ($isForms) ? $forms->buildElement($formElementData) : '';
                ?>
                <a
                    href="<?php echo $Mvc->getModelAjaxUrl('mailer') . '/addressbook'; ?>"
                    data-toggle="modal"
                    data-target="#chooseModal"
                    title="<?php echo $Core->i18n()->translate('Empfänger aus Adressbuch'); ?>"
                    id="toChooserLink"
                >
                    <i class="fa fa-id-card-o" aria-hidden="true"></i>
                </a>
                <?php
                $isRequired = ($abeId == '') ? true : false;
                /** To Element */
                $formElementData['id']            = 'to';
                $formElementData['name']          = 'to';
                $formElementData['label']         = false;
                $formElementData['value']         = '';
                $formElementData['type']          = 'text';
                $formElementData['required']      = $isRequired;
                $formElementData['classes']       = 'multiple-email-input';
                $formElementData['addAttributes'] = array('placeholder' => $Core->i18n()->translate('Empfänger'));
                echo ($isForms) ? $forms->buildElement($formElementData) : '';
                $formElementData['cssClasses']    = '';
                ?>
                <p class="help-block"><?php echo $Core->i18n()->translate('Mehrere E-Mail Adressen immer mit Komma getrennt eintragen.'); ?></p>
            </div>
        </div>

        <div class="form-group">
            <label for="subject" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Betreff') ?> <em>*</em></label>

            <div class="col-md-10">
                <?php
                /** Subject Element */
                $formElementData['id']            = 'subject';
                $formElementData['name']          = 'subject';
                $formElementData['label']         = false;
                $formElementData['value']         = $templateSubject;
                $formElementData['type']          = 'text';
                $formElementData['required']      = true;
                $formElementData['classes']       = '';
                $formElementData['addAttributes'] = array('placeholder' => $Core->i18n()->translate('Betreff'));
                echo ($isForms) ? $forms->buildElement($formElementData) : '';
                ?>
            </div>
        </div>

        <div class="form-group">
            <label for="message" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Nachricht') ?> <em>*</em></label>

            <div class="col-md-10">
                <div class="collapse tpl-placeholder" id="collapseTplPlaceholder">
                    <div class="well well-sm">
                        <button type="button" class="close" aria-label="Schließen" data-toggle="collapse" data-target="#collapseTplPlaceholder" aria-expanded="false" aria-controls="collapseTplPlaceholder">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p><?php echo $Core->i18n()->translate('Verwendbare Platzhalter'); ?></p>
                        <?php $i=1; foreach($mailerClass->getTemplatePlaceholder() as $placeholder => $description) : ?><?php echo ($i > 1) ? ', ' : ''; ?><?php echo $placeholder; ?> <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $description; ?>"></i><?php $i++; endforeach; ?>
                    </div>
                </div>
                <?php
                /** Message Element */
                $formElementData['type']          = 'textarea';
                $formElementData['id']            = 'message';
                $formElementData['name']          = 'message';
                $formElementData['label']         = false;
                $formElementData['value']         = $templateContent;
                $formElementData['required']      = true;
                $formElementData['addAttributes'] = array(
                    'placeholder' => $Core->i18n()->translate('Nachricht'),
                    'rows'        => 5
                );
                echo ($isForms) ? $forms->buildElement($formElementData) : '';
                ?>
                <p class="help-block">
                    <a href="#collapseTplPlaceholder" data-toggle="collapse" aria-expanded="false" aria-controls="collapseTplPlaceholder">
                        <?php echo $Core->i18n()->translate('Verwendbare Platzhalter'); ?>
                    </a>
                    <a href="<?php echo $Mvc->getModelAjaxUrl('mailer') . '/templates'; ?>" id="tplChooserLink" data-toggle="modal" data-target="#chooseModal" class="pull-right" data-toggle="tooltip" data-placement="left" title="<?php echo $Core->i18n()->translate('E-Mail Vorlage verwenden'); ?>">
                        <i class="fa fa-file-text" aria-hidden="true"></i>
                    </a>
                </p>
            </div>
        </div>
    </fieldset>
</form>

<!-- Modal -->
<div class="modal fade" id="chooseModal" tabindex="-1" role="dialog" aria-labelledby="chooseModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?php echo $Core->i18n()->translate('Bitte wählen'); ?></h4>
            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Schließen') ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->