<?php
/**
 * Mailer Model View Mail Index
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $mailerClass Mailer
 * @var $accClass Acc
 */
?>
<div class="action-wrapper row">
    <div class="col-sm-12">
        <div class="btn-set top text-right">
            <?php if(!is_object($accClass) || $accClass->hasAccess('mailer_mail_write')): ?>
                <a href="<?php echo $Mvc->getModelUrl() . '/mail/write'; ?>" class="btn btn-primary" title="<?php echo $Core->i18n()->translate('E-Mail schreiben'); ?>">
                    <i class="fa fa-paper-plane" aria-hidden="true"></i>
                </a>
            <?php endif; ?>
            <?php if(!is_object($accClass) || $accClass->hasAccess('mailer')): ?>
                <a href="<?php echo $Mvc->getModelUrl(); ?>" class="btn btn-default" title="<?php echo $Core->i18n()->translate('zurück zur Übersicht'); ?>">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="table-responsive">
    <table
        class="mail-list table table-hover table-striped table-condensed"
        data-datatable
        data-dtsource="<?php echo $Mvc->getModelAjaxUrl() . '/mail/dtsource/'; ?>"
    >
        <thead>
        <tr>
            <th data-dtsortable="0"></th>
            <th data-dtdefaultsort="desc"><?php echo $Core->i18n()->translate('Gesendet'); ?></th>
            <th><?php echo $Core->i18n()->translate('Von'); ?></th>
            <th><?php echo $Core->i18n()->translate('An'); ?></th>
            <th><?php echo $Core->i18n()->translate('Betreff'); ?></th>
            <?php if($Core->i18n()->isMultilang()) : ?>
                <th class="text-center"><?php echo $Core->i18n()->translate('Sprache'); ?></th>
            <?php endif; ?>
            <th class="text-center"><?php echo $Core->i18n()->translate('Status'); ?></th>
            <th data-dtsortable="0"></th>
        </tr>
        </thead>
        <tbody>
        <?php /* foreach($getMails as $mail): ?>
            <?php
                $sendDateTime    = date("d.m.Y", strtotime($mail['created'])) . ' ' . $Core->i18n()->translate('um') . ' ' . date("H:i:s", strtotime($mail['created'])) . ' ' . $Core->i18n()->translate('Uhr');
                $sendDate        = date("d.m.Y", strtotime($mail['created']));

                $fromMailAddress = $mail['from'][0];
                $fromMailName    = (isset($mail['from'][1]) && $mail['from'][1] != '') ? $mail['from'][1] : '';

                $from            = ($fromMailName != '') ? '&quot;' . $fromMailName . '&quot; &lt;' . $fromMailAddress . '&gt;' : $fromMailAddress;
                $to              = $mail['to'];
                $subject         = $mail['subject'];
                $status          = ($mail['send_success']) ? '<i class="fa fa-check-circle text-success" aria-hidden="true" title="' . $mail['send_message'] . '"></i>' : '<i class="fa fa-times-circle text-danger" aria-hidden="true" title="' . $mail['send_message'] . '"></i>';
            ?>
            <tr>
                <td scope="row" title="<?php echo $sendDateTime; ?>"><?php echo $sendDate; ?></td>
                <td><?php echo $from; ?></td>
                <td><?php echo $to; ?></td>
                <td><?php echo $subject; ?></td>
                <td class="text-right"><?php echo $status; ?></td>
            </tr>
            <?php endforeach; */ ?>
        </tbody>
    </table>
</div>

<!-- Modal -->
<div class="modal fade" id="showMailModal" tabindex="-1" role="dialog" aria-labelledby="showMailModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><?php echo $Core->i18n()->translate('E-Mail Ansicht'); ?></h4>
            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Schließen') ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->