<?php
/**
 * Mailer Model Ajax View Mail View
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */
?>
<?php if(!$hasError) : ?>
    <?php
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mailerClass Mailer
     * @var $accClass Acc
     */

    $sendUser = ($mail['id_acc_users'] !== 0 && is_object($accClass) && $accClass->userExists($mail['id_acc_users'])) ? $accClass->getUser($mail['id_acc_users']) : array();

    $sendBy = (count($sendUser)) ? $sendUser ['name'] . ' ' . $sendUser['surname'] : $Core->i18n()->translate('System');

    $isHtml          = $mail['ishtml'];

    $sendDateTime    = date("d.m.Y", strtotime($mail['created'])) . ' ' . $Core->i18n()->translate('um') . ' ' . date("H:i:s", strtotime($mail['created'])) . ' ' . $Core->i18n()->translate('Uhr');
    $sendDate        = date("d.m.Y", strtotime($mail['created']));

    $fromMailAddress = $mail['from'][0];
    $fromMailName    = (isset($mail['from'][1]) && $mail['from'][1] != '') ? $mail['from'][1] : '';

    $from            = ($fromMailName != '') ? '&quot;' . $fromMailName . '&quot; &lt;' . $fromMailAddress . '&gt;' : $fromMailAddress;
    $to              = $mail['to'];
    $cc              = $mail['cc'];
    $bcc             = $mail['bcc'];
    $subject         = $mail['subject'];
    $status          = ($mail['send_success']) ? '<i class="fa fa-check-circle text-success" aria-hidden="true" title="' . $mail['send_message'] . '"></i>' : '<i class="fa fa-times-circle text-danger" aria-hidden="true" title="' . $mail['send_message'] . '"></i>';

    $message         = ($isHtml && ($mail['message'] != strip_tags(html_entity_decode($mail['message'])))) ? html_entity_decode($mail['message']) : nl2br($mail['message']);
    ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">
            <?php echo $Core->i18n()->translate('E-Mail Ansicht') ?>
        </h4>
    </div>
    <div class="modal-body">

        <table class="mail-view table table-hover table-striped table-condensed">
            <tbody>
                <tr>
                    <th><?php echo $Core->i18n()->translate('Gesendet durch'); ?>:</th>
                    <td><?php echo $sendBy ?></td>
                </tr>
                <tr>
                    <th><?php echo $Core->i18n()->translate('Gesendet am'); ?>:</th>
                    <td><?php echo $sendDateTime ?></td>
                </tr>
                <tr>
                    <th><?php echo $Core->i18n()->translate('Von'); ?>:</th>
                    <td><?php echo $from ?></td>
                </tr>
                <tr>
                    <th><?php echo $Core->i18n()->translate('An'); ?>:</th>
                    <td><?php echo $to ?></td>
                </tr>
                <?php if($cc !== '') : ?>
                    <tr>
                        <th><?php echo $Core->i18n()->translate('Cc'); ?>:</th>
                        <td><?php echo $cc ?></td>
                    </tr>
                <?php endif; ?>
                <?php if($bcc !== '') : ?>
                    <tr>
                        <th><?php echo $Core->i18n()->translate('Bcc'); ?>:</th>
                        <td><?php echo $bcc ?></td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <th><?php echo $Core->i18n()->translate('Status'); ?>:</th>
                    <td><?php echo $status ?></td>
                </tr>
                <tr>
                    <th><?php echo $Core->i18n()->translate('Betreff'); ?>:</th>
                    <td><?php echo $subject ?></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <strong><?php echo $Core->i18n()->translate('Inhalt'); ?>:</strong>
                        <div class="mail-message">
                            <?php echo $message; ?>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Schließen'); ?></button>
    </div>
<?php else: ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">
            <?php echo $Core->i18n()->translate('Fehler') ?>
        </h4>
    </div>
    <div class="modal-body">
        <div class="alert alert-danger" role="alert"><?php echo $errMsg; ?></div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $Core->i18n()->translate('Schließen'); ?></button>
    </div>
<?php endif; ?>