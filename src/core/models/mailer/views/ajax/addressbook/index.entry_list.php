<?php
/**
 * Mailer Model Ajax View AddressBook Index Entry List
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $mailerClass Mailer
 * @var $accClass Acc
 */
$useAddressBook = ($isGlobal) ? $getAddressBookGlobal : $getAddressBook;
$legendTitle    = ($isGlobal) ? 'Globales Adressbuch' : 'Persönliches Adressbuch';
?>
<table class="personal-entries table table-hover table-striped table-condensed">
    <thead>
    <tr>
        <th colspan="2"><?php echo $Core->i18n()->translate($legendTitle); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($useAddressBook as $entry): ?>
        <tr>
            <td title="<?php echo implode(", ", $entry['email']); ?>"><?php echo $entry['name']; ?> <?php echo $entry['surname']; ?></td>
            <td class="text-right">
                <div class="btn-group btn-group-sm btn-group-raised">
                    <a href="#" onClick="writeTo('<?php echo trim(str_replace(array('\'',),array('\\\''),json_encode($entry['name'] . ' ' . $entry['surname'])), '"\'\\'); ?>', '<?php echo trim(json_encode(implode(", ", $entry['email'])), '"'); ?>', <?php echo $entry['id']; ?>)" class="btn btn-default" title="<?php echo sprintf($Core->i18n()->translate('Empfänger \'%s\' verwenden'), $entry['name'] . ' ' . $entry['surname']); ?>" data-to="<?php echo $entry['id']; ?>">
                        <i class="fa fa-arrow-down" aria-hidden="true"></i>
                    </a>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
