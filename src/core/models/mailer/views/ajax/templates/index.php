<?php
/**
 * Mailer Model Ajax View Templates Index
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */
?><div class="modal-header">
    <button type="button" class="close" onclick="$('#chooseModal').modal('hide');" aria-hidden="true">&times;</button>
    <h4 class="modal-title">
        <?php echo $Core->i18n()->translate('Vorlage wählen') ?>
    </h4>
</div>

<div class="modal-body">
    <?php if($canSeeGlobal) : ?>
        <?php if(is_object($accClass) && count($curUser)) : ?>
            <ul class="nav nav-tabs">
                <li class="active"><a href="#personal" data-toggle="tab"><?php echo $Core->i18n()->translate('Persönliche Vorlagen') ?> <span class="badge"><?php echo count($getTemplates); ?></span></a></li>
                <li><a href="#global" data-toggle="tab"><?php echo $Core->i18n()->translate('Globale Vorlagen') ?> <span class="badge"><?php echo count($getTemplatesGlobal); ?></span></a></li>
                <li><a href="#system" data-toggle="tab"><?php echo $Core->i18n()->translate('System Vorlagen') ?> <span class="badge"><?php echo count($getTemplatesSystem); ?></span></a></li>
            </ul>
            <div id="addressbookTabContent" class="tab-content">
                <div class="tab-pane fade active in" id="personal">
                    <?php $isPersonal = true; include('index.entry_list.php'); ?>
                </div>
                <div class="tab-pane fade" id="global">
                    <?php $isGlobal = true; include('index.entry_list.php'); ?>
                </div>
                <div class="tab-pane fade" id="system">
                    <?php $isSystem = true; include('index.entry_list.php'); ?>
                </div>
            </div>
        <?php else: ?>
            <ul class="nav nav-tabs">
                <li class="active"><a href="#global" data-toggle="tab"><?php echo $Core->i18n()->translate('Globale Vorlagen') ?> <span class="badge"><?php echo count($getTemplatesGlobal); ?></span></a></li>
                <li><a href="#system" data-toggle="tab"><?php echo $Core->i18n()->translate('System Vorlagen') ?> <span class="badge"><?php echo count($getTemplatesSystem); ?></span></a></li>
            </ul>
            <div id="addressbookTabContent" class="tab-content">
                <div class="tab-pane fade" id="global">
                    <?php $isGlobal = true; include('index.entry_list.php'); ?>
                </div>
                <div class="tab-pane fade" id="system">
                    <?php $isSystem = true; include('index.entry_list.php'); ?>
                </div>
            </div>
        <?php endif; ?>
    <?php else: ?>
        <?php $isPersonal = true; include('index.entry_list.php'); ?>
    <?php endif; ?>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" onclick="$('#chooseModal').modal('hide');"><?php echo $Core->i18n()->translate('Abbrechen'); ?></button>
</div>