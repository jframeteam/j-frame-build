<?php
/**
 * Mailer Model View Index Index
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $mailerClass Mailer
 * @var $accClass Acc
 */
?>
<div class="action-wrapper row">
    <div class="col-sm-12">
    <div class="btn-set top text-right">
        <?php if(!is_object($accClass) || $accClass->hasAccess('mailer_mail_write')): ?>
            <a href="<?php echo $Mvc->getModelUrl() . '/mail/write'; ?>" class="btn btn-primary" title="<?php echo $Core->i18n()->translate('E-Mail schreiben'); ?>">
                <i class="fa fa-paper-plane" aria-hidden="true"></i>
            </a>
        <?php endif; ?>
        <?php if(!is_object($accClass) || $accClass->hasAccess('mailer_settings')): ?>
            <a href="<?php echo $Mvc->getModelUrl() . '/settings/'; ?>" class="btn btn-default" title="<?php echo $Core->i18n()->translate('Einstellungen'); ?>">
                <i class="fa fa-cogs" aria-hidden="true"></i>
            </a>
        <?php endif; ?>
    </div>
    </div>
</div>

<?php if(!$mailerClass->hasSettings()): ?>
    <div class="alert alert-danger" role="alert">
        <?php echo $Core->i18n()->translate('Keine Zugangsdaten zu PITS Mailer Service hinterlegt!'); ?>
    </div>
<?php else: ?>
    <div class="row">
        <?php if(!is_object($accClass) || $accClass->hasAccess('mailer_mail')): ?>
        <div class="col-xs-6 col-md-4">
            <a href="<?php echo $Mvc->getModelUrl() . '/mail'; ?>" class="thumbnail text-center">
                <i class="fa fa-archive" aria-hidden="true"></i>
                <?php echo $Core->i18n()->translate('Gesendet'); ?>
                <span class="badge"><?php echo $getMailCount ?></span>
            </a>
        </div>
        <?php endif; ?>
        <?php if(!is_object($accClass) || $accClass->hasAccess('mailer_addressbook')): ?>
        <div class="col-xs-6 col-md-4">
            <a href="<?php echo $Mvc->getModelUrl() . '/addressbook'; ?>" class="thumbnail text-center">
                <i class="fa fa-address-book" aria-hidden="true"></i>
                <?php echo $Core->i18n()->translate('Adressbuch'); ?>
                <span class="badge"><?php echo $addressBookEntriesCount; ?></span>
            </a>
        </div>
        <?php endif; ?>
        <?php if(!is_object($accClass) || $accClass->hasAccess('mailer_templates')): ?>
        <div class="col-xs-6 col-md-4">
            <a href="<?php echo $Mvc->getModelUrl() . '/templates'; ?>" class="thumbnail text-center">
                <i class="fa fa-file-text" aria-hidden="true"></i>
                <?php echo $Core->i18n()->translate('E-Mail Vorlagen'); ?>
                <span class="badge"><?php echo $templatesCount; ?></span>
            </a>
        </div>
        <?php endif; ?>
    </div>
<?php endif; ?>