<?php
/**
 * Mailer Model View Settings Index
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $mailerClass Mailer
 * @var $accClass Acc
 * @var $plugins Plugins
 * @var $pitsForms PitsForms
 * @var $pitsCore PitsCore
 */

/** Prepare Form Plugin */
$plugins = $Core->Plugins();
$pitsForms = $plugins->PitsForms();
$isForms = (is_object($pitsForms));
$pitsCore = $plugins->PitsCore();

$btnActionName = ($isForms) ? $pitsCore->xorEnc('action') : 'action';

$settingsSaveUrl = $Mvc->getModelUrl() . '/settings/save';

$canSeeFull = (is_object($accClass) && !$accClass->hasAccess('mailer_settings_full')) ? false : true;
?>

<form id="settings_form" class="form-horizontal" action="<?php echo $settingsSaveUrl ?>" method="post">

    <div class="action-wrapper row">
        <div class="col-sm-12">
            <div class="btn-set top text-right">
                <button type="submit" name="<?php echo $btnActionName; ?>" value="settings_save" class="btn btn-primary" data-action="settings_save">
                    <i class="fa fa-floppy-o" aria-hidden="true"></i><span class="hidden-xs">  <?php echo $Core->i18n()->translate('Speichern'); ?></span>
                </button>
                <?php if($accClass->hasAccess('mailer_mail')) : ?>
                    <a href="<?php echo $Mvc->getModelUrl(); ?>" class="btn btn-default" title="<?php echo $Core->i18n()->translate('zurück zur Übersicht'); ?>">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>

<?php if($canSeeFull): ?>
    <ul class="nav nav-tabs">
        <li class="active"><a href="#appsettings" data-toggle="tab"><?php echo $Core->i18n()->translate('PITS Mailer - APP Einstellungen') ?></a></li>
        <li><a href="#mailsettings" data-toggle="tab"><?php echo $Core->i18n()->translate('E-Mail Adressen') ?></a></li>
    </ul>
    <div id="mailersettingsTabContent" class="tab-content">
        <div class="tab-pane fade active in" id="appsettings">

            <fieldset>

                <legend><?php echo $Core->i18n()->translate('PITS Mailer - APP Einstellungen') ?></legend>

                <div class="form-group">
                    <label for="envelope_sender" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('SMTP E-Mail Adresse') ?> <em>*</em></label>

                    <div class="col-md-10">
                        <?php
                        /** Envelope Sender Element */
                        $formElementData['eleType']       = 'input';
                        $formElementData['id']            = 'envelope_sender';
                        $formElementData['name']          = 'envelope_sender';
                        $formElementData['label']         = false;
                        $formElementData['value']         = $settings['envelope_sender'];
                        $formElementData['type']          = 'email';
                        $formElementData['isRequired']    = true;
                        $formElementData['ownAttributes'] = array('placeholder' => $Core->i18n()->translate('SMTP E-Mail Adresse'));
                        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="app_username" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('App Benutzername') ?> <em>*</em></label>
                    <div class="col-md-10">
                        <?php
                        /** App Username Element */
                        $formElementData['eleType']       = 'input';
                        $formElementData['id']            = 'app_username';
                        $formElementData['name']          = 'app_username';
                        $formElementData['label']         = false;
                        $formElementData['value']         = $settings['app_username'];
                        $formElementData['type']          = 'text';
                        $formElementData['isRequired']    = true;
                        $formElementData['ownAttributes'] = array('placeholder' => $Core->i18n()->translate('App Benutzername'));
                        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                        ?>
                    </div>
                </div>

                <?php if($mailerClass->hasSettings()) : ?>
                    <div class="form-group">
                        <label for="app_password" class="col-md-2 control-label" data-toggle="collapse" href="#changePassword" aria-expanded="false" aria-controls="changePassword" style="cursor: pointer; white-space: nowrap;"><?php echo $Core->i18n()->translate('Passwort ändern') ?> <i class="fa fa-chevron-right" aria-hidden="true"></i></label>
                        <div class="col-md-10">
                            <div class="collapse" id="changePassword">
                                <?php
                                /** App Password Element */
                                $formElementData['eleType']       = 'input';
                                $formElementData['id']            = 'app_password';
                                $formElementData['name']          = 'app_password';
                                $formElementData['label']         = false;
                                $formElementData['value']         = '';
                                $formElementData['type']          = 'password';
                                $formElementData['isRequired']    = false;
                                $formElementData['ownAttributes'] = array('placeholder' => $Core->i18n()->translate('App Passwort'));
                                echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                                ?>
                            </div>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="form-group">
                        <label for="app_password" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('App Passwort') ?> <em>*</em></label>

                        <div class="col-md-10">
                            <?php
                            /** App Password Element */
                            $formElementData['eleType']       = 'input';
                            $formElementData['id']            = 'app_password';
                            $formElementData['name']          = 'app_password';
                            $formElementData['label']         = false;
                            $formElementData['value']         = '';
                            $formElementData['type']          = 'password';
                            $formElementData['isRequired']    = true;
                            $formElementData['ownAttributes'] = array('placeholder' => $Core->i18n()->translate('App Passwort'));
                            echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                            ?>
                        </div>
                    </div>
                <?php endif; ?>
            </fieldset>
        </div>
        <div class="tab-pane fade" id="mailsettings">
<?php endif; ?>

            <fieldset>

                <legend><?php echo $Core->i18n()->translate('E-Mail Adressen') ?></legend>

                <div class="form-group">
                    <label for="mailer_cc" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Antwort an') ?></label>

                    <div class="col-md-10">
                        <?php
                        /** Cc Element */
                        $formElementData['eleType']       = 'input';
                        $formElementData['id']            = 'mailer_reply_to';
                        $formElementData['name']          = 'mailer_reply_to';
                        $formElementData['label']         = false;
                        $formElementData['value']         = $settings['mailer_reply_to'];
                        $formElementData['type']          = 'email';
                        $formElementData['isRequired']    = false;
                        $formElementData['ownAttributes'] = array('placeholder' => $Core->i18n()->translate('Antwort an'));
                        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                        ?>
                        <p class="help-block"><?php echo $Core->i18n()->translate('Wenn die Antwort an E-Mail Adresse gesetzt ist, wird diese auch bei Benutzer E-Mails verwendet.'); ?></p>
                    </div>
                </div>

                <div class="form-group">
                    <label for="mailer_cc" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Cc') ?></label>

                    <div class="col-md-10">
                        <?php
                        /** Cc Element */
                        $formElementData['eleType']       = 'input';
                        $formElementData['id']            = 'mailer_cc';
                        $formElementData['name']          = 'mailer_cc';
                        $formElementData['label']         = false;
                        $formElementData['value']         = $settings['mailer_cc'];
                        $formElementData['type']          = 'text';
                        $formElementData['isRequired']    = false;
                        $formElementData['cssClasses']    = 'multiple-email-input';
                        $formElementData['ownAttributes'] = array('placeholder' => $Core->i18n()->translate('Cc'));
                        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                        $formElementData['cssClasses']    = '';
                        ?>
                        <p class="help-block"><?php echo $Core->i18n()->translate('Mehrere E-Mail Adressen immer mit Komma getrennt eintragen.'); ?></p>
                    </div>
                </div>

                <div class="form-group">
                    <label for="mailer_bcc" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Bcc') ?></label>
                    <div class="col-md-10">
                        <?php
                        /** Bcc Element */
                        $formElementData['eleType']       = 'input';
                        $formElementData['id']            = 'mailer_bcc';
                        $formElementData['name']          = 'mailer_bcc';
                        $formElementData['label']         = false;
                        $formElementData['value']         = $settings['mailer_bcc'];
                        $formElementData['type']          = 'text';
                        $formElementData['isRequired']    = false;
                        $formElementData['cssClasses']    = 'multiple-email-input';
                        $formElementData['ownAttributes'] = array('placeholder' => $Core->i18n()->translate('Bcc'));
                        echo ($isForms) ? $pitsForms->createFormElement($formElementData) : '';
                        $formElementData['cssClasses']    = '';
                        ?>
                        <p class="help-block"><?php echo $Core->i18n()->translate('Mehrere E-Mail Adressen immer mit Komma getrennt eintragen.'); ?></p>
                    </div>
                </div>
            </fieldset>
        </div>

<?php if($canSeeFull): ?>
    </div>
<?php endif; ?>
</form>
