<?php
/**
 * Mailer Model View AddressBook Index Entry List
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $mailerClass Mailer
 * @var $accClass Acc
 */
$useAddressBook = ($isGlobal) ? $getAddressBookGlobal : $getAddressBook;
$legendTitle    = ($isGlobal) ? 'Globales Adressbuch' : 'Persönliches Adressbuch';

/** Sort Array */
usort($useAddressBook, function($a, $b) {
    return $a['surname'] > $b['surname'];
});
?>
<legend><?php echo $Core->i18n()->translate($legendTitle) ?></legend>
<table class="personal-entries table table-hover table-striped table-condensed">
    <thead>
    <tr>
        <th></th>
        <th><?php echo $Core->i18n()->translate('Vorname'); ?></th>
        <th><?php echo $Core->i18n()->translate('Nachname'); ?></th>
        <th><?php echo $Core->i18n()->translate('E-Mail'); ?></th>
        <th><?php echo $Core->i18n()->translate('Comment'); ?></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($useAddressBook as $entry): ?>
        <?php if(!array_key_exists('id', $entry)) { continue; } ?>
        <?php $entryDelBtn = ($accClass->hasAccess('mailer_addressbook_entry_delete')) ? '<a href="' . $Mvc->getModelUrl() . '/addressbook/delete/' . $entry['id'] . '" class="btn btn-danger hidden-xs btn-del-entry" data-action="entry_delete" title="' . sprintf( $Core->i18n()->translate('Eintrag \'%s\' löschen') , $entry['name'] . ' ' . $entry['surname']) . '"><i class="fa fa-trash" aria-hidden="true"></i></a>': ''; ?>
        <tr>
            <td scope="row"><i class="fa fa-id-card-o" aria-hidden="true" title="ID=<?php echo $entry['id']; ?>"></i></td>
            <td><?php echo $entry['name']; ?></td>
            <td><?php echo $entry['surname']; ?></td>
            <td><?php echo implode(", ", $entry['email']); ?></td>
            <td><?php echo nl2br($entry['comment']); ?></td>
            <td class="text-right">
                <div class="btn-group btn-group-sm btn-group-raised">
                    <a href="<?php echo $Mvc->getModelUrl() . '/mail/write/to/' . $entry['id'] ?>" class="btn btn-default" title="<?php echo sprintf($Core->i18n()->translate('E-Mail schreiben an \'%s\''), $entry['name'] . ' ' . $entry['surname']); ?>">
                        <i class="fa fa-paper-plane" aria-hidden="true"></i>
                    </a>
                    <?php if(!$isGlobal || $canEditGlobal) : ?>
                        <?php echo $entryDelBtn; ?>
                        <a href="<?php echo $Mvc->getModelUrl() . '/addressbook/edit/' . $entry['id'] ?>" class="btn btn-default" title="<?php echo $Core->i18n()->translate('bearbeiten'); ?>">
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                        </a>
                    <?php endif; ?>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
