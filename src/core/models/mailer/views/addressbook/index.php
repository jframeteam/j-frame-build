<?php
/**
 * Mailer Model View AddressBook Index
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $mailerClass Mailer
 * @var $accClass Acc
 */
?>
<div class="action-wrapper row">
    <div class="col-sm-12">
        <div class="btn-set top text-right">
            <?php if(!is_object($accClass) || $accClass->hasAccess('mailer_addressbook_create')): ?>
                <a href="<?php echo $Mvc->getModelUrl() . '/addressbook/create'; ?>" class="btn btn-primary" title="<?php echo $Core->i18n()->translate('Eintrag hinzufügen'); ?>">
                    <i class="fa fa-plus" aria-hidden="true"></i> <i class="fa fa-id-card-o" aria-hidden="true"></i>
                </a>
            <?php endif; ?>
            <?php if(!is_object($accClass) || $accClass->hasAccess('mailer')): ?>
                <a href="<?php echo $Mvc->getModelUrl(); ?>" class="btn btn-default" title="<?php echo $Core->i18n()->translate('zurück zur Übersicht'); ?>">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                </a>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php if($canSeeGlobal) : ?>
    <?php if(is_object($accClass) && count($curUser)) : ?>
        <ul class="nav nav-tabs">
            <li class="active"><a href="#personal" data-toggle="tab"><?php echo $Core->i18n()->translate('Persönliches Adressbuch') ?> <span class="badge"><?php echo count($getAddressBook); ?></span></a></li>
            <li><a href="#global" data-toggle="tab"><?php echo $Core->i18n()->translate('Globales Adressbuch') ?> <span class="badge"><?php echo count($getAddressBookGlobal); ?></span></a></li>
        </ul>
        <div id="addressbookTabContent" class="tab-content">
            <div class="tab-pane fade active in" id="personal">
                <?php $isGlobal = false; include('index.entry_list.php'); ?>
            </div>
            <div class="tab-pane fade" id="global">
                <?php $isGlobal = true; include('index.entry_list.php'); ?>
            </div>
        </div>
    <?php else: ?>
        <?php $isGlobal = true; include('index.entry_list.php'); ?>
    <?php endif; ?>
<?php else: ?>
    <?php $isGlobal = false; include('index.entry_list.php'); ?>
<?php endif; ?>