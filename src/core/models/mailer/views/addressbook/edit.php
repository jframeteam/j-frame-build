<?php
/**
 * Mailer Model View AddressBook Edit
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $mailerClass Mailer
 * @var $accClass Acc
 * @var $plugins Plugins
 * @var $forms Forms
 */

/** Prepare Forms Plugin */
$plugins = $Core->Plugins();
$forms = $plugins->Forms();
$isForms = (is_object($forms));

$entrySaveUrl = $Mvc->getModelUrl() . '/addressbook/save';
$addressBookEntryId = (is_array($addressBookEntry) && count($addressBookEntry)) ? $addressBookEntry['id'] : 0;
$addressBookEntryEmails = implode(', ', $addressBookEntry['email'])
?>
<form id="addressbook_entry_edit_form" class="form-horizontal" action="<?php echo $entrySaveUrl ?>" method="post">

    <div class="action-wrapper row">
        <div class="col-sm-12">
            <div class="btn-set top text-right">
                <button type="submit" name="action" value="addressbook_entry_save" class="btn btn-primary" data-action="addressbook_entry_save">
                    <i class="fa fa-floppy-o" aria-hidden="true"></i><span class="hidden-xs">  <?php echo $Core->i18n()->translate('Speichern'); ?></span>
                </button>
                <?php if(!is_object($accClass) || $accClass->hasAccess('mailer_addressbook')): ?>
                    <a href="<?php echo $Mvc->getModelUrl() . '/addressbook'; ?>" class="btn btn-default text-danger" title="<?php echo $Core->i18n()->translate('Abbrechen'); ?>">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <fieldset>

        <legend><?php echo $Core->i18n()->translate('Eintrag bearbeiten') ?></legend>

        <?php
        /** Hidden Id Element */
        $formElementData['name']  = 'id';
        $formElementData['label'] = false;
        $formElementData['value'] = $addressBookEntryId;
        $formElementData['type']  = 'hidden';
        echo ($isForms) ? $forms->buildElement($formElementData) : '';
        ?>

        <?php if($canEditGlobal) : ?>
            <div class="form-group">
                <label for="addressbooktype" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Adressbuch') ?></label>

                <div class="col-md-10">
                    <?php
                    $dlAllValue = array();
                    $dlAllValue[1] = $Core->i18n()->translate('persönlich');
                    $dlAllValue[0] = $Core->i18n()->translate('global');

                    /** E-Mail Element */
                    $formElementData['type']     = 'select';
                    $formElementData['id']       = 'addressbooktype';
                    $formElementData['name']     = 'addressbooktype';
                    $formElementData['label']    = false;
                    $formElementData['value']    = $addressBookEntry['id_acc_users'];
                    $formElementData['options']  = $dlAllValue;
                    echo ($isForms) ? $forms->buildElement($formElementData) : '';
                    ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="form-group">
            <label for="name" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Vorname') ?></label>

            <div class="col-md-10">
                <?php
                /** Name Element */
                $formElementData['id']            = 'name';
                $formElementData['name']          = 'name';
                $formElementData['label']         = false;
                $formElementData['value']         = $addressBookEntry['name'];
                $formElementData['type']          = 'text';
                $formElementData['required']      = true;
                $formElementData['addAttributes'] = array('placeholder' => $Core->i18n()->translate('Vorname'));
                echo ($isForms) ? $forms->buildElement($formElementData) : '';
                ?>
            </div>
        </div>

        <div class="form-group">
            <label for="surname" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Nachname') ?></label>

            <div class="col-md-10">
                <?php
                /** Surname Element */
                $formElementData['id']            = 'surname';
                $formElementData['name']          = 'surname';
                $formElementData['label']         = false;
                $formElementData['value']         = $addressBookEntry['surname'];
                $formElementData['type']          = 'text';
                $formElementData['required']    = true;
                $formElementData['addAttributes'] = array('placeholder' => $Core->i18n()->translate('Nachname'));
                echo ($isForms) ? $forms->buildElement($formElementData) : '';
                ?>
            </div>
        </div>

        <div class="form-group">
            <label for="email" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('E-Mail') ?> <em>*</em></label>

            <div class="col-md-10">
                <?php
                /** Message Element */
                $formElementData['id']            = 'email';
                $formElementData['name']          = 'email';
                $formElementData['label']         = false;
                $formElementData['value']         = $addressBookEntryEmails;
                $formElementData['type']          = 'textarea';
                $formElementData['required']    = true;
                $formElementData['classes']    = 'multiple-email-input';
                $formElementData['addAttributes']      = array(
                    'placeholder' => $Core->i18n()->translate('E-Mail'),
                    'rows'        => 3
                );
                echo ($isForms) ? $forms->buildElement($formElementData) : '';
                $formElementData['classes']    = '';
                ?>
                <p class="help-block"><?php echo $Core->i18n()->translate('Mehrere E-Mail Adressen immer mit Komma getrennt eintragen.'); ?></p>
            </div>
        </div>

        <div class="form-group">
            <label for="comment" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Comment') ?></label>

            <div class="col-md-10">
                <?php
                /** E-Mail Element */
                $formElementData['type']          = 'textarea';
                $formElementData['id']            = 'comment';
                $formElementData['name']          = 'comment';
                $formElementData['label']         = false;
                $formElementData['value']         = $addressBookEntry['comment'];
                $formElementData['required']    = false;
                $formElementData['addAttributes']      = array(
                    'placeholder' => $Core->i18n()->translate('Comment'),
                    'rows'        => 3
                );
                echo ($isForms) ? $forms->buildElement($formElementData) : '';
                ?>
            </div>
        </div>
    </fieldset>
</form>