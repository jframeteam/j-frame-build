<?php
/**
 * Mailer Model View Templates Edit
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $mailerClass Mailer
 * @var $accClass Acc
 * @var $plugins Plugins
 * @var $forms Forms
 */

/** Prepare Forms Plugin */
$plugins = $Core->Plugins();
$forms = $plugins->Forms();
$isForms = (is_object($forms));

$typeChooserArray = (isset($typeChooserArray)) ? $typeChooserArray : array();
$systemTemplate = (isset($systemTemplate)) ? $systemTemplate : array();

$entrySaveUrl = $Mvc->getModelUrl() . '/templates/save';
$templateId = (is_array($template) && count($template)) ? $template['id'] : 0;
?>
<form id="addressbook_entry_edit_form" class="form-horizontal" action="<?php echo $entrySaveUrl ?>" method="post">

    <div class="action-wrapper row">
        <div class="col-sm-12">
            <div class="btn-set top text-right">
                <button type="submit" name="action" value="template_save" class="btn btn-primary" data-action="mail_send">
                    <i class="fa fa-floppy-o" aria-hidden="true"></i><span class="hidden-xs">  <?php echo $Core->i18n()->translate('Speichern'); ?></span>
                </button>
                <?php if(!is_object($accClass) || $accClass->hasAccess('mailer_templates')): ?>
                    <a href="<?php echo $Mvc->getModelUrl() . '/templates'; ?>" class="btn btn-default text-danger" title="<?php echo $Core->i18n()->translate('Abbrechen'); ?>">
                        <i class="fa fa-times" aria-hidden="true"></i>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <fieldset>

        <legend><?php echo $Core->i18n()->translate('Vorlage bearbeiten') ?></legend>

        <?php
        /** Hidden Id Element */
        $formElementData['name']  = 'id';
        $formElementData['label'] = false;
        $formElementData['value'] = $templateId;
        $formElementData['type']  = 'hidden';
        echo ($isForms) ? $forms->buildElement($formElementData) : '';
        ?>

        <?php if($canEditGlobal) : ?>
            <div class="form-group">
                <label for="type" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Vorlagen-Typ') ?></label>

                <div class="col-md-10">
                    <?php
                    $dlAllValue = array();
                    $dlAllValue[1] = $Core->i18n()->translate('global');
                    $dlAllValue[2] = $Core->i18n()->translate('persönlich');

                    if(!count($systemTemplate)) {
                        /** Template Type Element */
                        $formElementData['type']    = 'select';
                        $formElementData['id']      = 'type';
                        $formElementData['name']    = 'type';
                        $formElementData['label']   = false;
                        $formElementData['value']   = $template['type'];
                        $formElementData['options'] = $dlAllValue;
                        echo ($isForms) ? $forms->buildElement($formElementData) : '';
                    } else {
                        $limitedValues = array();
                        foreach($typeChooserArray as $dlAllValueKey) {
                            $limitedValues[$dlAllValueKey] = $dlAllValue[$dlAllValueKey];
                        }
                        if(count($limitedValues)) {
                            /** Template Type Element */
                            $formElementData['type']    = 'select';
                            $formElementData['id']      = 'type';
                            $formElementData['name']    = 'type';
                            $formElementData['label']   = false;
                            $formElementData['value']   = $template['type'];
                            $formElementData['options'] = $dlAllValue;
                            echo ($isForms) ? $forms->buildElement($formElementData) : '';
                        } else {
                            /** Is HTML Element */
                            $formElementData['id']            = '';
                            $formElementData['name']          = 'type';
                            $formElementData['label']         = false;
                            $formElementData['value']         = $template['type'];
                            $formElementData['type']          = 'hidden';
                            $formElementData['required']      = false;
                            $formElementData['addAttributes'] = array();
                            echo '<p class="form-control-static">' . $dlAllValue[$template['type']] . '</p>';
                        }
                    }
                    ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if($Core->i18n()->isMultilang()) : ?>
            <div class="form-group">
                <label for="lang" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Sprache') ?></label>

                <div class="col-md-10">
                    <?php
                    $dlAllValue = array();
                    foreach($Core->i18n()->getLanguages() as $langCode => $lang) {
                        $dlAllValue[$langCode] = $lang['name'];
                    }

                    if(!count($systemTemplate)) {
                        /** Lang Element */
                        $formElementData['type']    = 'select';
                        $formElementData['id']      = 'lang';
                        $formElementData['name']    = 'lang';
                        $formElementData['label']   = false;
                        $formElementData['value']   = $template['lang'];
                        $formElementData['options'] = $dlAllValue;
                        echo ($isForms) ? $forms->buildElement($formElementData) : '';
                    } else {
                        /** Lang Element */
                        $formElementData['id']            = '';
                        $formElementData['name']          = 'lang';
                        $formElementData['label']         = false;
                        $formElementData['value']         = $template['lang'];
                        $formElementData['type']          = 'hidden';
                        $formElementData['required']      = false;
                        $formElementData['addAttributes'] = array();
                        echo '<p class="form-control-static"><span class="flag flag-' . $template['lang'] . '"></span></p>';
                    }
                    ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="form-group">
            <label for="ishtml" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('HTML Format') ?></label>
            <div class="col-md-10">
                <div class="togglebutton">
                    <label>
                        <?php
                        if(!count($systemTemplate)) {
                            /** Is HTML Element */
                            $formElementData['type']            = 'checkbox';
                            $formElementData['id']              = '';
                            $formElementData['name']            = 'ishtml';
                            $formElementData['label']           = false;
                            $formElementData['value']           = $template['ishtml'];
                            $formElementData['valueChecked']    = '1';
                            $formElementData['valueNotChecked'] = '0';
                            $formElementData['checkboxOnly']    = true;
                            echo ($isForms) ? $forms->buildElement($formElementData) : '';
                        } else {
                            /** Is HTML Element */
                            $formElementData['id']            = '';
                            $formElementData['name']          = 'ishtml';
                            $formElementData['label']         = false;
                            $formElementData['value']         = $template['ishtml'];
                            $formElementData['type']          = 'hidden';
                            $formElementData['required']      = false;
                            $formElementData['addAttributes'] = array();
                            echo ($isForms) ? $forms->buildElement($formElementData) : '';
                            echo '<p class="form-control-static">';
                            echo ($template['ishtml']) ? '<i class="fa fa-check-circle text-success" aria-hidden="true" title="' . $Core->i18n()->translate('Ja') . '"></i>' : '<i class="fa fa-times-circle text-danger" aria-hidden="true" title="' . $Core->i18n()->translate('Nein') . '"></i>';
                            echo '</p>';
                        }
                        ?>
                    </label>
                </div>
            </div>
        </div>

        <?php if(count($systemTemplate)) : ?>
        <div class="form-group">
            <label for="code" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Vorlagen-Code') ?></label>

            <div class="col-md-10">
                <p class="form-control-static"><?php echo $template['code']; ?></p>
                <span class="help-block"><?php echo $Core->i18n()->translate('Eine interne Bezeichnung, wird für den Versand durch das System benötigt!') ?></span>
            </div>
        </div>
        <?php endif; ?>

        <div class="form-group">
            <label for="name" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Vorlagen-Name') ?> <em>*</em></label>

            <div class="col-md-10">
                <?php
                /** Name Element */
                $formElementData['id']            = 'name';
                $formElementData['name']          = 'name';
                $formElementData['label']         = false;
                $formElementData['value']         = $template['name'];
                $formElementData['type']          = 'text';
                $formElementData['required']      = true;
                $formElementData['addAttributes'] = array('placeholder' => $Core->i18n()->translate('Vorlagen-Name'));
                echo ($isForms) ? $forms->buildElement($formElementData) : '';
                ?>
            </div>
        </div>

        <div class="form-group">
            <label for="subject" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Betreff') ?></label>

            <div class="col-md-10">
                <?php
                /** Subject Element */
                $formElementData['id']            = 'subject';
                $formElementData['name']          = 'subject';
                $formElementData['label']         = false;
                $formElementData['value']         = $template['subject'];
                $formElementData['type']          = 'text';
                $formElementData['required']      = false;
                $formElementData['addAttributes'] = array('placeholder' => $Core->i18n()->translate('Betreff'));
                echo ($isForms) ? $forms->buildElement($formElementData) : '';
                ?>
            </div>
        </div>

        <div class="form-group">
            <label for="content" class="col-md-2 control-label"><?php echo $Core->i18n()->translate('Inhalt') ?> <em>*</em></label>

            <div class="col-md-10">
                <div class="collapse tpl-placeholder" id="collapseTplPlaceholder">
                    <div class="well well-sm">
                        <button type="button" class="close" aria-label="Schließen" data-toggle="collapse" data-target="#collapseTplPlaceholder" aria-expanded="false" aria-controls="collapseTplPlaceholder">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <p><?php echo $Core->i18n()->translate('Verwendbare Platzhalter'); ?></p>
                        <?php $i=1; foreach($mailerClass->getTemplatePlaceholder() as $placeholder => $description) : ?><?php echo ($i > 1) ? ', ' : ''; ?><?php echo $placeholder; ?> <i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $description; ?>"></i><?php $i++; endforeach; ?>
                    </div>
                </div>
                <?php
                /** Content Element */
                $formElementData['type']          = 'textarea';
                $formElementData['id']            = 'content';
                $formElementData['name']          = 'content';
                $formElementData['label']         = false;
                $formElementData['value']         = $template['content'];
                $formElementData['required']      = true;
                $formElementData['addAttributes'] = array(
                    'placeholder' => $Core->i18n()->translate('Inhalt'),
                    'rows'        => 5
                );
                echo ($isForms) ? $forms->buildElement($formElementData) : '';
                ?>
                <p class="help-block"><a data-toggle="collapse" href="#collapseTplPlaceholder" aria-expanded="false" aria-controls="collapseTplPlaceholder"><?php echo $Core->i18n()->translate('Verwendbare Platzhalter'); ?></a></p>
            </div>
        </div>
    </fieldset>
</form>