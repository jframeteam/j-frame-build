<?php
/**
 * Mailer Model View Templates Index Entry List
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * @var $Core Core
 * @var $Mvc Mvc
 * @var $mailerClass Mailer
 * @var $accClass Acc
 */

$isPersonal = (isset($isPersonal)) ? $isPersonal : false;

if($isPersonal) {
    $useTemplates = $getTemplates;
    $legendTitle  = 'Persönliche Vorlagen';
}

$isGlobal = (isset($isGlobal)) ? $isGlobal : false;

if($isGlobal) {
    $useTemplates = $getTemplatesGlobal;
    $legendTitle  = 'Globale Vorlagen';
}

$isSystem = (isset($isSystem)) ? $isSystem : false;

if($isSystem) {
    $useTemplates = $getTemplatesSystem;
    $legendTitle  = 'System Vorlagen';
}

$systemTemplates = $mailerClass->getSystemTemplates();

/** Sort Array */
usort($useTemplates, function($a, $b) {
    return $a['name'] > $b['name'];
});
?>
<legend><?php echo $Core->i18n()->translate($legendTitle) ?></legend>
<table class="personal-entries table table-hover table-striped table-condensed">
    <thead>
    <tr>
        <th></th>
        <th><?php echo $Core->i18n()->translate('Vorlagen-Name'); ?></th>
        <th><?php echo $Core->i18n()->translate('Betreff'); ?></th>
        <th class="text-center"><?php echo $Core->i18n()->translate('HTML Format'); ?></th>
        <?php if($Core->i18n()->isMultilang()) : ?>
        <th class="text-center"><?php echo $Core->i18n()->translate('Sprache'); ?></th>
        <?php endif; ?>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($useTemplates as $entry): ?>
        <?php $entryDelBtn = ($accClass->hasAccess('mailer_template_entry_delete')) ? '<a href="' . $Mvc->getModelUrl() . '/templates/delete/' . $entry['id'] . '" class="btn btn-danger hidden-xs btn-del-user" data-action="user_delete" title="' . sprintf( $Core->i18n()->translate('Vorlage \'%s\' löschen') , $entry['name']) . '"><i class="fa fa-trash" aria-hidden="true"></i></a>': ''; ?>
        <?php $langUrlSlug = ($Core->i18n()->isMultilang()) ? '/lang/' . $entry['lang'] : ''; ?>
        <?php
            $canCreateFromSystem = true;
            $fromSystem = false;
            $templateByCode = $mailerClass->getTemplateByCode($entry['code'], $entry['lang'], false);
            if (count($templateByCode)) {
                $systemTemplate = (array_key_exists(0, $templateByCode)) ? $templateByCode[0] : array();
                $globalTemplate = (array_key_exists(1, $templateByCode)) ? $templateByCode[1] : array();
                $personalTemplate = (array_key_exists(2, $templateByCode)) ? $templateByCode[2] : array();

                if(count($systemTemplate)) {
                    $fromSystem = true;

                    if ($systemTemplate['type_max'] === 0) {
                        $canCreateFromSystem = false;
                    }

                    if ($systemTemplate['type_max'] === 1 && count($globalTemplate)) {
                        $canCreateFromSystem = false;
                    }

                    if ($systemTemplate['type_max'] == 2 && count($globalTemplate) && count($personalTemplate)) {
                        $canCreateFromSystem = false;
                    }
                }
            }
            $tplIconColor = ($isSystem || $fromSystem) ? ' text-warning' : '';
            $tplIconTitle = 'ID=' . $entry['id'];
            $tplIconTitle = ($isSystem) ? $tplIconTitle . ' (' . $Core->i18n()->translate('System Vorlage') . ')' : $tplIconTitle;
            $tplIconTitle = (!$isSystem && $fromSystem) ? $tplIconTitle . ' (' . $Core->i18n()->translate('Wird genutzt als System Vorlage') . ')' : $tplIconTitle;
        ?>
        <tr>
            <td scope="row"><i class="fa fa-file-text<?php echo $tplIconColor; ?>" aria-hidden="true" title="<?php echo $tplIconTitle; ?>"></i></td>
            <td><?php echo $entry['name']; ?></td>
            <td><?php echo $entry['subject']; ?></td>
            <td class="text-center"><?php echo ($entry['ishtml']) ? '<i class="fa fa-check-circle text-success" aria-hidden="true" title="' . $Core->i18n()->translate('Ja') . '"></i>' : '<i class="fa fa-times-circle text-danger" aria-hidden="true" title="' . $Core->i18n()->translate('Nein') . '"></i>'; ?></td>
            <?php if($Core->i18n()->isMultilang()) : ?>
            <td class="text-center"><span class="flag flag-<?php echo $entry['lang']; ?>"></span></td>
            <?php endif; ?>
            <td class="text-right">
                <div class="btn-group btn-group-sm btn-group-raised">
                    <?php if(!$isSystem && !$fromSystem) : ?>
                        <a href="<?php echo $Mvc->getModelUrl() . '/mail/write/tpl/' . $entry['code'] . $langUrlSlug; ?>" class="btn btn-default" title="<?php echo sprintf($Core->i18n()->translate('E-Mail schreiben mit der Vorlage \'%s\''), $entry['name']); ?>">
                            <i class="fa fa-paper-plane" aria-hidden="true"></i>
                        </a>
                    <?php endif; ?>
                    <?php if(!$isGlobal || $canEditGlobal) : ?>
                        <?php if($entry['type'] > 0) : ?>
                            <a href="<?php echo $Mvc->getModelUrl() . '/templates/edit/' . $entry['id'] ?>" class="btn btn-default" title="<?php echo $Core->i18n()->translate('bearbeiten'); ?>">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                            </a>
                            <?php echo $entryDelBtn; ?>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php if($isSystem && $canEditGlobal) : ?>
                        <?php if($canCreateFromSystem) : ?>
                            <a href="<?php echo $Mvc->getModelUrl() . '/templates/create/tpl/' . $entry['code'] . $langUrlSlug; ?>" class="btn btn-default" title="<?php echo sprintf($Core->i18n()->translate('Vorlage aus \'%s\' erstellen'), $entry['name']); ?>">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                <i class="fa fa-file-text" aria-hidden="true"></i>
                            </a>
                        <?php else: ?>
                            <span class="btn btn-default disabled" title="<?php echo sprintf($Core->i18n()->translate('Vorlage aus \'%s\' erstellen'), $entry['name']); ?>">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                                <i class="fa fa-file-text" aria-hidden="true"></i>
                            <span>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
