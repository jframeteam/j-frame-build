function writeTo(name, email, id) {
    name = typeof name !== 'undefined' ? name : '';
    email = typeof email !== 'undefined' ? email : '';
    id = typeof id !== 'undefined' ? id : 0;
    if($('#write-to-wrapper #ab-entries').length && name != '' && email != '' && id !== 0) {
        if(typeof event !== 'undefined'){
            event.preventDefault();
        }

        if($('.alert.entryaddederror').length) {
            $('.alert.entryaddederror').remove();
        }

        var addtoBtn = $('a[data-to="'+ id + '"');

        var writeToWrapper    = $('#write-to-wrapper');
        var abeEntriesWrapper = writeToWrapper.find('#ab-entries');
        var abeInput          = writeToWrapper.find('#abe-input');
        var html              = '<div class="well well-sm" title="' + email + '" id="abe-' + id + '"><i class="fa fa-id-card-o" aria-hidden="true"></i> ' + name + '<span class="btn btn-link btn-xs btn-remove" title="" data-id="' + id + '">&times;</span></div>';

        var addEntry = true;
        $('#write-to-wrapper #ab-entries .well').each( function(i,ele) {
            var wellId = $(ele).attr('id');
            var wellEntryId = wellId.replace('abe-', '');

            if(typeof wellEntryId !== 'undefined' && wellEntryId !== '') {
                if(wellEntryId == id) {
                    addEntry = false;
                }
            }
        });

        if(addEntry) {
            abeEntriesWrapper.append(html);

            writeToAddClickEventToRemove();

            abeInput.val(abeInput.val() + ',' + id);

            $('#to').removeAttr('required');
        } else {
            core.bs_alert.danger('Adressbuch Eintrag bereits hinzugefügt!', 'entryaddederror', '#chooseModal .modal-body');
        }

        if((typeof addtoBtn !== 'undefined') && addtoBtn.length){
            var addtoBtnTr = addtoBtn.closest('tr');
            addtoBtn.remove();
            addtoBtnTr.css('opacity', '0.6');
        }
    }
}

function writeToRemove(id) {
    id = typeof id !== 'undefined' ? id : 0;
    if(id !== 0) {
        if($('#write-to-wrapper #ab-entries #abe-'+id).length) {
            event.preventDefault();

            var writeToWrapper = $('#write-to-wrapper');
            var abeElement = writeToWrapper.find('#abe-'+id);
            var abeInput = writeToWrapper.find('#abe-input');
            var newAbeInputVal = abeInput.val().replace(id, '').trim().replace(/,{1,}$/, '');

            abeInput.val(newAbeInputVal);
            abeInput.attr('value', newAbeInputVal);
            abeElement.remove();

            if(abeInput.val() === ''){
                $('#to').attr('required','required');
            }
        }
    }
}

function writeToAddClickEventToRemove() {
    if($('#write-to-wrapper #ab-entries .well .btn-remove').length) {
        $('#write-to-wrapper #ab-entries .well .btn-remove').each( function(i, ele) {
            var dataId = $(ele).data('id');
            if(typeof dataId !== 'undefined' && dataId !== '') {
                $(ele).click( function() {
                    writeToRemove(dataId);
                });
            }
        });
    }
}

function useTpl(subject, message) {
    subject = typeof subject !== 'undefined' ? subject : '';
    message = typeof message !== 'undefined' ? message : '';
    if($('#message').length && message != '') {
        event.preventDefault();

        if($('#subject').val() != '' || $('#message').val() != '') {
            var aystext = core.i18n.translate('Sind Sie Sicher? Felder für Betreff und Nachricht werden dadurch überschrieben!');
            var deltext = core.i18n.translate('Okay, weiter...');

            var aysData = {
                areYouSureTxt: aystext,
                deleteTxt:     deltext
            };

            core.ays(aysData).on('core.ays.confirmed', function () {
                $('#message').html(message).val(message);

                if (subject != '' && $('#subject').length) {
                    $('#subject').val(subject);
                }
                $('#chooseModal').modal('hide');

                $(this).unbind('core.ays.confirmed');
            });
        } else {
            $('#message').html(message).val(message);
            if (subject != '' && $('#subject').length) {
                $('#subject').val(subject);
            }
            $('#chooseModal').modal('hide');
        }
    }
}

$( document ).ready(function() {
    if($('#chooseModal').length) {
        writeToAddClickEventToRemove();
    }
});