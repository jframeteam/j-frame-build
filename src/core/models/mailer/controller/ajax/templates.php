<?php
/**
 * Mailer Model Ajax Controller Templates
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function indexAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mailerClass Mailer
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mailerClass = $Mvc->modelClass('Mailer');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('mailer_templates')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('mailer_templates') . ' - ' . $Core->i18n()->translate('mailer') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('mailer') . ' - ' . $Core->i18n()->translate('mailer_templates'));

        $site   = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $curUser   = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $canSeeGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_templates_global'));
        $canEditGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_templates_global_edit'));

        $getTemplatesByCurrentUser = $mailerClass->getTemplatesByCurrentUser();
        $getTemplates       = $getTemplatesByCurrentUser['templates'];
        $getTemplatesGlobal = $getTemplatesByCurrentUser['templatesGlobal'];
        $getTemplatesSystem = $getTemplatesByCurrentUser['templatesSystem'];

        $return = '';

        $ob_return = '';
        if($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    }
}