<?php
/**
 * Mailer Model Ajax Controller Mail
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function dtsourceAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mailerClass Mailer
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mailerClass = $Mvc->modelClass('Mailer');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('mailer_mail_send')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $Mvc->setMVCPlainAjax(true);

        $return = array();

        $site = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $curUser = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $cols = array(
            'id',
            'created',
            'from',
            'to',
            'subject',
            'lang',
            'send_success',
            'action'
        );
        if(!$Core->i18n()->isMultilang()) {
            unset($cols['lang']);
        }

        $dtDraw          = (array_key_exists('draw',$_GET)) ? $_GET['draw'] : 0;
        $dtStart         = (array_key_exists('start',$_GET)) ? $_GET['start'] : 0;
        $dtLength        = (array_key_exists('length',$_GET)) ? $_GET['length'] : 10;
        $dtSearch        = (array_key_exists('search',$_GET)) ? $_GET['search'] : array();
        $dtSearchValue   = (count($dtSearch) && array_key_exists('value',$dtSearch) && $dtSearch['value'] != '') ? $dtSearch['value'] : null;
        $order           = (array_key_exists('order',$_GET)) ? $_GET['order'] : array();
        $orderColInt     = (count($order) && array_key_exists(0,$order) && array_key_exists('column',$order[0])) ? $order[0]['column'] : null;
        $orderDir        = (count($order) && array_key_exists(0,$order) && array_key_exists('dir',$order[0])) ? $order[0]['dir'] : 'asc';
        $orderBy         = (array_key_exists($orderColInt,$cols)) ? $cols[$orderColInt] : null;
        $recordsTotal    = $mailerClass->getMailsCount($siteId);
        $recordsFiltered = $recordsTotal;

        $start = $dtStart;
        $limit = $dtLength;

        if($dtSearchValue !== null) {
            $dtSearchValue = trim(str_replace(array('"','$','[',']'),'',$dtSearchValue));
            $searchArray   = array( '\\', '+', '*', '?', '^', '(', ')', '{', '}', '=', '!', '<', '>', '|', ':', '-');
            $replaceArray  = array( '[\\\\]', '[+]', '[*]', '[?]', '[\\\\^]', '[(]', '[)]', '[{]', '[}]', '[=]', '[!]', '[<]', '[>]', '[|]', '[:]', '[-]');
            $dtSearchValue = trim(str_replace($searchArray,$replaceArray,$dtSearchValue));

            if($dtSearchValue == '') {
                $dtSearchValue = null;
            }
        }

        $data = array();
        $mails = $mailerClass->getMails($siteId, $start, $limit, $orderBy, $orderDir, $dtSearchValue);

        foreach($mails as $mail) {
            $sendUserId      = $mail['id_acc_users'];

            $mailIcon        = ($mail['system']) ? '<span class="text-warning" title="ID=' . $mail['id'] . ' (' . $Core->i18n()->translate('System E-Mail') . ')">' : '<span title="ID=' . $mail['id'] . '">';
            $mailIcon       .= '<i class="fa fa-envelope" aria-hidden="true"></i>';
            $mailIcon       .= '</span>';

            $sendDateTime    = date("d.m.Y", strtotime($mail['created'])) . ' ' . $Core->i18n()->translate('um') . ' ' . date("H:i:s", strtotime($mail['created'])) . ' ' . $Core->i18n()->translate('Uhr');
            $sendDate        = date("d.m.Y", strtotime($mail['created']));

            $fromMailAddress = (is_array($mail['from']) && array_key_exists(0,$mail['from']) && $mail['from'][0] != '') ? $mail['from'][0] : '';
            $fromMailName    = (is_array($mail['from']) && array_key_exists(1,$mail['from']) && $mail['from'][1] != '') ? $mail['from'][1] : '';

            $fromTitle       = ($fromMailName != '') ? '&quot;' . $fromMailName . '&quot; &lt;' . $fromMailAddress . '&gt;' : $fromMailAddress;
            $from            = ($fromMailName != '') ? '<span title="' . $fromTitle . '">' . $fromMailName . '</span>' : $fromMailAddress;

            $to              = (strlen(trim($mail['to'], ',')) > 45) ? '<span title="' . trim($mail['to'], ',') . '">' . substr(trim($mail['to'], ','), 0, 42) . '...</span>' : trim($mail['to'], ',');
            $subject         = $mail['subject'];

            $langFlag        = ($mail['lang'] != '') ? '<span class="flag flag-' . $mail['lang'] . '">' : '';

            $status          = ($mail['send_success']) ? '<i class="fa fa-check-circle text-success" aria-hidden="true" title="' . $mail['send_message'] . '"></i>' : '<i class="fa fa-times-circle text-danger" aria-hidden="true" title="' . $mail['send_message'] . '"></i>';

            $galleryViewBtn = '<a href="' . $Mvc->getModelAjaxUrl() . '/mail/view/' . $mail['id'] . '" class="btn btn-default btn-xs" title="' . $Core->i18n()->translate('E-Mail öffnen') . '" data-toggle="modal" data-target="#showMailModal"><i class="fa fa-eye" aria-hidden="true"></i></a>';

            if($Core->i18n()->isMultilang()) {
                $data[] = array(
                    $mailIcon,
                    '<span title="' . $sendDateTime . '">' . $sendDate . '</span>',
                    $from,
                    $to,
                    $subject,
                    '<div class="text-center">' . $langFlag . '</div>',
                    '<div class="text-center">' . $status . '</div>',
                    $galleryViewBtn
                );
            } else {
                $data[] = array(
                    $mailIcon,
                    '<span title="' . $sendDateTime . '">' . $sendDate . '</span>',
                    $from,
                    $to,
                    $subject,
                    '<div class="text-center">' . $status . '</div>',
                    $galleryViewBtn
                );
            }
        }

        if($dtSearchValue !== null) {
            $recordsFiltered = count($mails);
        }

        $return['draw']            = $dtDraw;
        $return['searchValue']     = $dtSearchValue;
        $return['recordsTotal']    = $recordsTotal;
        $return['recordsFiltered'] = $recordsFiltered;
        $return['data']            = $data;

        return $return;
    }
}

function viewAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mailerClass Mailer
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mailerClass = $Mvc->modelClass('Mailer');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('mailer_mail_send')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $return = '';

        $curUser = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $params  = $Mvc->getMvcParams();

        $paramViewId = 0;

        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id') {
                $paramViewId = (array_key_exists('id', $params)) ? (int)$params['id'] : 0;
            } else {
                $paramViewId = (int)$first_key;
            }
        }

        $mail = ($mailerClass->mailExists($paramViewId)) ? $mailerClass->getMail($paramViewId) : array();


        if(count($mail) && $mail['id_acc_users'] != $curUserId && !$curUser['su']) {
            $mail = array();
        }

        $errMsg = '';
        $hasError = false;
        if($paramViewId < 1) {
            $errMsg = $Core->i18n()->translate('Keine Id übergeben, nichts zu tun...');
            $hasError = true;
        }

        if(!count($mail)) {
            $errMsg = sprintf($Core->i18n()->translate('E-Mail mit der ID \'%s\' konnte nicht gefunden werden...'), $paramViewId);
            $hasError = true;
        }

        $ob_return = '';
        try {
            ob_start();
            include($Mvc->getMVCViewFilePath());
            $ob_return = ob_get_contents();
            ob_end_clean();
        } catch (Exception $e) {
            $ob_return = $e->getMessage();
        }
        $return .= $ob_return;

        return $return;
    }
}