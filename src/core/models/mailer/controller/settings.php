<?php
/**
 * Mailer Model Controller Settings
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function indexAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mailerClass Mailer
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mailerClass = $Mvc->modelClass('Mailer');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('mailer_settings')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $settings = $mailerClass->getSettings();

        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('mailer_settings') . ' - ' . $Core->i18n()->translate('mailer') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('mailer') . ' - ' . $Core->i18n()->translate('mailer_settings'));

        $return = '';

        $ob_return = '';
        if($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    }
}

function saveAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mailerClass Mailer
     * @var $accClass Acc
     * @var $plugins Plugins
     * @var $pitsForms PitsForms
     * @var $pitsCore PitsCore
     */
    global $Core, $Mvc;

    /** Prepare Form Plugin */
    $plugins = $Core->Plugins();
    $pitsForms = $plugins->PitsForms();

    $pitsCore = $plugins->PitsCore();

    $mailerClass = $Mvc->modelClass('Mailer');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('mailer_settings_save')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {
        $return = '';
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if(is_object($pitsForms)) {
                $pitsCore->decryptPOST();
            }
            $post = $Core->Request()->getPost();

            if(isset($post['action']) && $post['action'] == 'settings_save') {

                $save_result = $mailerClass->setSettings($post);

                if($save_result) {
                    $note  = $Core->i18n()->translate('Mailer Einstellungen erfolgreich gespeichert!');
                    $type  = 'success';
                    $kind  = 'bs-alert';
                    $title = $Core->i18n()->translate('Erledigt') . '!';
                } else {
                    $note  = $Core->i18n()->translate('Mailer Einstellungen konnten nicht gespeichert werden!');
                    $type  = 'danger';
                    $kind  = 'bs-alert';
                    $title = $Core->i18n()->translate('Fehler') . '!';
                }
                $Core->setNote($note, $type, $kind, $title);

                $redirectUrl = $Mvc->getModelUrl() . '/settings';
                $Core->Request()->redirect($redirectUrl);
            }
        }

        return $return;
    }
}