<?php
/**
 * Mailer Model Controller Mail
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function indexAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mailerClass Mailer
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mailerClass = $Mvc->modelClass('Mailer');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('mailer_mail_list')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('mailer_mail_list') . ' - ' . $Core->i18n()->translate('mailer') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('mailer') . ' - ' . $Core->i18n()->translate('mailer_mail_list'));

        $site   = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $curUser   = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $getMailsFull = $mailerClass->getMails($siteId);
        $getMails = array();
        foreach($getMailsFull as $mailId => $mail) {
            $mailUserId = (is_array($mail) && array_key_exists('id_acc_users',$mail)) ? $mail['id_acc_users'] : 0;
            if($mailUserId == $curUserId) {
                $getMails[$mailId] = $mail;
            }
        }

        $return = '';

        $ob_return = '';
        if($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    }
}

function writeAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mailerClass Mailer
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mailerClass = $Mvc->modelClass('Mailer');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('mailer_mail_write')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('mailer_mail_write') . ' - ' . $Core->i18n()->translate('mailer') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('mailer') . ' - ' . $Core->i18n()->translate('mailer_mail_write'));

        $site   = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $curUser   = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $addressBookEntry = array();
        $writeTo = '';
        $templateSubject = '';
        $templateContent = '';

        $params  = $Mvc->getMvcParams();
        if(count($params)) {
            $lang               = (isset($params['lang'])    && $params['lang']    != '') ? $params['lang'] : $Core->i18n()->getCurrLang();
            $addressBookEntryId = (isset($params['to'])      && $params['to']      != '' && is_numeric($params['to'])) ? $params['to'] : 0;
            $templateCode       = (isset($params['tpl'])     && $params['tpl']     != '') ? $params['tpl']  : '';
            $dirId              = (isset($params['dirId'])   && $params['dirId']   != '' && is_numeric($params['dirId'])) ? $params['dirId'] : 0;

            if($lang != $Core->i18n()->getCurrLang()) {
                if(!$Core->i18n()->isMultilang()) {
                    $lang      = $Core->i18n()->getDefaultLang();
                } else {
                    $langArray = $Core->i18n()->getLang($lang);
                    $lang      = (count($langArray)) ? $langArray['code'] : $Core->i18n()->getCurrLang();
                }
            }

            if($addressBookEntryId !== 0) {
                $canSeeGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_addressbook_global'));

                $getAddressBooksByCurrentUser = $mailerClass->getAddressBooksByCurrentUser();
                $getAddressBook       = $getAddressBooksByCurrentUser['addressbook'];
                $getAddressBookGlobal = $getAddressBooksByCurrentUser['addressbookGlobal'];

                if(array_key_exists($addressBookEntryId, $getAddressBook) || array_key_exists($addressBookEntryId, $getAddressBookGlobal)) {
                    $addressBookEntry = (isset($getAddressBook[$addressBookEntryId])) ? $getAddressBook[$addressBookEntryId] : $getAddressBookGlobal[$addressBookEntryId];
                    $writeTo = implode(", ", $addressBookEntry['email']);
                }

                if(!count($addressBookEntry)) {
                    $note  = $Core->i18n()->translate('Adressbuch Eintrag konnte nicht gefunden werden');
                    $type  = 'warning';
                    $kind  = 'bs-alert';
                    $title = '';
                    $Core->setNote($note, $type, $kind, $title);
                }
            }

            if($templateCode != '') {
                $template = array();

                $canSeeGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_templates_global'));

                $templateByCode = $mailerClass->getTemplateByCode($templateCode, $lang);
                if(count($templateByCode)) {
                    $template = $templateByCode;
                }

                if(count($template) && $template['type'] < 2 && !$canSeeGlobal) {
                    $template = array();
                }

                if(count($template)) {
                    $templateSubject = (isset($template['subject'])) ? $template['subject'] : '';
                    $templateContent = (isset($template['content'])) ? $template['content'] : '';
                }

                if(!count($template)) {
                    $note  = $Core->i18n()->translate('Vorlage konnte nicht gefunden werden');
                    $type  = 'warning';
                    $kind  = 'bs-alert';
                    $title = '';
                    $Core->setNote($note, $type, $kind, $title);
                }
            }
        }

        $return = '';

        $ob_return = '';
        if($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    }
}

function sendAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mailerClass Mailer
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mailerClass = $Mvc->modelClass('Mailer');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('mailer_mail_send')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $sites = $Core->Sites();
        $curSite = (is_object($sites)) ? $sites->getSite() : array();
        $curSiteUrlKey = (array_key_exists('urlKey', $curSite)) ? $curSite['urlKey'] : '';
        $curSiteUrlSlug = ($curSiteUrlKey != '') ? $curSiteUrlKey . '/' : '';

        $curUser = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $return = '';
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $post = $Core->Request()->getPost();

            if(isset($post['action']) && $post['action'] == 'mail_send') {
                $redirectUrl = $Mvc->getModelUrl() . '/mail/write';

                $from            = (isset($post['from'])) ? $post['from'] : '';
                $abeIds          = (isset($post['abeIds'])) ? $post['abeIds'] : '';
                $to              = (isset($post['to'])) ? $post['to'] : '';
                $subject         = (isset($post['subject'])) ? $post['subject'] : '';
                $message         = (isset($post['message'])) ? $post['message'] : '';
                $message_prefix  = (isset($post['message_prefix'])) ? $post['message_prefix'] : '';
                $attachments     = (isset($post['attachments']) && is_array($post['attachments'])) ? $post['attachments'] : array();
                $cc              = (isset($post['cc'])) ? $post['cc'] : '';
                $bcc             = (isset($post['bcc'])) ? $post['bcc'] : '';
                $reply_to        = (isset($post['reply_to'])) ? $post['reply_to'] : '';
                $read_receipt_to = (isset($post['read_receipt_to'])) ? $post['read_receipt_to'] : '';
                $ishtml          = (isset($post['ishtml']) && $post['ishtml'] !== 1) ? false : true;
                $userId          = (isset($post['userId'])) ? $post['userId'] : $curUserId;
                $lang            = (isset($post['lang'])) ? $post['lang'] : $Core->i18n()->getCurrLang();
                $dirId           = (isset($post['dirId'])) ? $post['dirId'] : 0;
                $redirectPath    = (isset($post['redirectToPath'])) ? $post['redirectToPath'] : '';

                if($from != '' && ($to != '' || $abeIds != '') && $subject != '' && $message != '') {

                    if ($ishtml && $message_prefix == strip_tags($message_prefix)) {
                        $message_prefix = nl2br(trim($message_prefix));
                        $message = $message_prefix . '<br /><br />' . $message;
                    } else {
                        $message = $message_prefix . "\n\r\n\r" . $message;
                    }

                    $sendData['from']            = $from;
                    $sendData['abeIds']          = $abeIds;
                    $sendData['to']              = $to;
                    $sendData['subject']         = $subject;
                    $sendData['message']         = $message;
                    $sendData['attachments']     = $attachments;
                    $sendData['cc']              = $cc;
                    $sendData['bcc']             = $bcc;
                    $sendData['reply_to']        = $reply_to;
                    $sendData['read_receipt_to'] = $read_receipt_to;
                    $sendData['ishtml']          = $ishtml;
                    $sendData['userId']          = $userId;
                    $sendData['dirId']           = $dirId;
                    $sendData['lang']            = $lang;

                    $sendResult = $mailerClass->sendMail($sendData);
                    if(is_array($sendResult) && isset($sendResult['success'])) {
                        if($sendResult['success']) {
                            $note  = $sendResult['msg'];
                            $type  = 'success';
                            $kind  = 'bs-alert';
                            $title = '';
                        } else {
                            $note  = $sendResult['msg'];
                            $type  = 'danger';
                            $kind  = 'bs-alert';
                            $title = $Core->i18n()->translate('Fehler') . '!';
                        }
                    } else {
                        $note  = $Core->i18n()->translate('E-Mail konnte nicht versendet werden!');
                        $type  = 'danger';
                        $kind  = 'bs-alert';
                        $title = $Core->i18n()->translate('Fehler') . '!';
                    }
                } else {
                    $note  = $Core->i18n()->translate('Bitte alle Felder ausfüllen!');
                    $type  = 'danger';
                    $kind  = 'bs-alert';
                    $title = $Core->i18n()->translate('Fehler') . '!';
                }
                $Core->setNote($note, $type, $kind, $title);

                if($redirectPath !== '') {

                    $redirectUrl = $Core->getBaseUrl() . '/' . $curSiteUrlSlug . trim($redirectPath, '/');
                }
                $Core->Request()->redirect($redirectUrl);
            }

            $return .= '<pre>$post => ' . print_r($post, true) . '</pre>';
        }

        return $return;
    }
}
