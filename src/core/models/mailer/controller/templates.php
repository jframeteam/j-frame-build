<?php
/**
 * Mailer Model Controller Templates
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function indexAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mailerClass Mailer
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mailerClass = $Mvc->modelClass('Mailer');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('mailer_templates')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('mailer_templates') . ' - ' . $Core->i18n()->translate('mailer') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('mailer') . ' - ' . $Core->i18n()->translate('mailer_templates'));

        $site   = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $curUser   = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $canSeeGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_templates_global'));
        $canEditGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_templates_global_edit'));

        $getTemplatesByCurrentUser = $mailerClass->getTemplatesByCurrentUser();
        $getTemplates       = $getTemplatesByCurrentUser['templates'];
        $getTemplatesGlobal = $getTemplatesByCurrentUser['templatesGlobal'];
        $getTemplatesSystem = $getTemplatesByCurrentUser['templatesSystem'];

        $return = '';

        $ob_return = '';
        if($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    }
}

function createAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mailerClass Mailer
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mailerClass = $Mvc->modelClass('Mailer');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('mailer_templates')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('Vorlage erstellen') . ' - ' . $Core->i18n()->translate('mailer') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('mailer') . ' - ' . $Core->i18n()->translate('Vorlage erstellen'));

        $site   = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $curUser   = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $canSeeGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_templates_global'));
        $canEditGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_templates_global_edit'));

        $return = '';

        $template = array();
        $typeChooserArray = array();
        $params = $Mvc->getMvcParams();
        if(count($params)) {
            $lang               = (isset($params['lang'])    && $params['lang']    != '') ? $params['lang'] : $Core->i18n()->getCurrLang();
            $templateCode       = (isset($params['tpl'])     && $params['tpl']     != '') ? $params['tpl']  : '';

            if($lang != $Core->i18n()->getCurrLang()) {
                if(!$Core->i18n()->isMultilang()) {
                    $lang      = $Core->i18n()->getDefaultLang();
                } else {
                    $langArray = $Core->i18n()->getLang($lang);
                    $lang      = (count($langArray)) ? $langArray['code'] : $Core->i18n()->getCurrLang();
                }
            }


            if($templateCode != '') {
                $templateByCode = $mailerClass->getTemplateByCode($templateCode, $lang, false);
                if(count($templateByCode)) {

                    $systemTemplate   = (array_key_exists(0, $templateByCode)) ? $templateByCode[0] : array();
                    $globalTemplate   = (array_key_exists(1, $templateByCode)) ? $templateByCode[1] : array();
                    $personalTemplate = (array_key_exists(2, $templateByCode)) ? $templateByCode[2] : array();

                    $template = $systemTemplate;
                    if (count($systemTemplate)) {
                        $typeChooserArray = array();
                        if (!count($globalTemplate)) {
                            $typeChooserArray = array(1);
                        }

                        if (!count($personalTemplate) && $systemTemplate['type_max'] == 2) {
                            $typeChooserArray = (in_array(1, $typeChooserArray) ? array(1,2) : array(2));
                        }
                    }

                    if(count($template) && !$canEditGlobal) {
                        $template = array();
                    }

                    if(count($template) && $systemTemplate['type_max'] == 2 && (count($globalTemplate) || count($personalTemplate))) {
                        if(count($globalTemplate)) {
                            $note = $Core->i18n()->translate('System Vorlage bereits als globale Vorlage hinterlegt');
                            $type = 'warning';
                            $kind = 'bs-alert';
                            $title = '';

                            $Core->setNote($note, $type, $kind, $title);
                        }

                        if(count($personalTemplate)) {
                            $note = $Core->i18n()->translate('System Vorlage bereits als persönliche Vorlage hinterlegt');
                            $type = 'warning';
                            $kind = 'bs-alert';
                            $title = '';

                            $Core->setNote($note, $type, $kind, $title);
                        }

                        if(count($globalTemplate) && count($personalTemplate)) {
                            $template = array();

                            $redirectUrl = $Mvc->getModelUrl() . '/templates/create/';
                            $Core->Request()->redirect($redirectUrl);
                        }
                    }

                    if (count($template) && $systemTemplate['type_max'] == 1 && count($globalTemplate)) {
                        $note = $Core->i18n()->translate('System Vorlage bereits als globale Vorlage hinterlegt');
                        $type = 'warning';
                        $kind = 'bs-alert';
                        $title = '';

                        $Core->setNote($note, $type, $kind, $title);

                        $redirectUrl = $Mvc->getModelUrl() . '/templates/edit/' . $globalTemplate['id'];
                        $Core->Request()->redirect($redirectUrl);
                    }

                    if(!count($template)) {
                        $note  = $Core->i18n()->translate('Vorlage konnte nicht gefunden werden');
                        $type  = 'warning';
                        $kind  = 'bs-alert';
                        $title = '';
                        $Core->setNote($note, $type, $kind, $title);
                    }
                }
            }
        }

        $ob_return = '';
        if($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    }
}

function editAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mailerClass Mailer
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mailerClass = $Mvc->modelClass('Mailer');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('mailer_templates')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('Vorlage bearbeiten') . ' - ' . $Core->i18n()->translate('mailer') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('mailer') . ' - ' . $Core->i18n()->translate('Vorlage bearbeiten'));

        $site   = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $curUser   = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $template = array();
        $params = $Mvc->getMvcParams();
        $paramEntryId = 0;
        if(count($params)) {
            reset($params);
            $first_key = key($params);
            $paramEntryId = (int)$first_key;
            if($first_key == 'id') {
                $paramEntryId = (isset($params['id'])) ? (int)$params['id'] : 0;
            }

            $templateId = $paramEntryId;

            if($templateId > 0) {
                $canSeeGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_templates_global'));
                $canEditGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_templates_global_edit'));

                $getTemplate = $mailerClass->getTemplate($templateId);

                if(count($getTemplate) && $getTemplate['id_system_sites'] == $siteId) {

                    $templateByCode = $mailerClass->getTemplateByCode($getTemplate['code'], $getTemplate['lang'], false);
                    if(count($templateByCode)) {
                        $systemTemplate   = (array_key_exists(0, $templateByCode)) ? $templateByCode[0] : array();
                        $globalTemplate   = (array_key_exists(1, $templateByCode)) ? $templateByCode[1] : array();
                        $personalTemplate = (array_key_exists(2, $templateByCode)) ? $templateByCode[2] : array();

                        if (count($systemTemplate)) {
                            $typeChooserArray = array();
                            if (!count($globalTemplate)) {
                                $typeChooserArray = array(1);
                            }

                            if (!count($personalTemplate) && $systemTemplate['type_max'] == 2) {
                                $typeChooserArray = (in_array(1, $typeChooserArray) ? array(1,2) : array(2));
                            }
                        }
                    }

                    $template = $getTemplate;

                    if($getTemplate['type'] === 0) {
                        $template = array();
                    }

                    if($getTemplate['type'] === 1 && !$canEditGlobal) {
                        $template = array();
                    }
                }
            }
        }

        if(!count($template)) {
            $note = $Core->i18n()->translate('Vorlage konnte nicht gefunden werden');
            $type = 'danger';
            $kind = 'bs-alert';
            $title = $Core->i18n()->translate('Fehler') . '!';

            $Core->setNote($note, $type, $kind, $title);

            $redirectUrl = $Mvc->getModelUrl() . '/templates';
            $Core->Request()->redirect($redirectUrl);
        }

        $return = '';

        $ob_return = '';
        if($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    }
}

function saveAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mailerClass Mailer
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mailerClass = $Mvc->modelClass('Mailer');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('mailer_templates')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $user     = (is_object($accClass)) ? $accClass->getUser() : array();
        $userId   = (count($user)) ? $user['id'] : 0;

        $site   = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $canSeeGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_templates_global'));
        $canEditGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_templates_global_edit'));

        $return = '';
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $post = $Core->Request()->getPost();

            if(isset($post['action']) && $post['action'] == 'template_save') {
                $redirectUrl = $Mvc->getModelUrl() . '/templates';

                $entryId       = (isset($post['id'])) ? $post['id'] : false;
                $entryIshtml   = (isset($post['ishtml']) && $post['ishtml']) ? 1 : 0;
                $entryName     = (isset($post['name'])) ? $post['name'] : '';
                $entryCode     = (isset($post['code'])) ? $post['code'] : '';
                $entrySubject  = (isset($post['subject'])) ? $post['subject'] : '';
                $entryContent  = (isset($post['content'])) ? $post['content'] : '';
                $entryType     = (isset($post['type'])) ? $post['type'] : 2;
                $entryLang     = (isset($post['lang'])) ? $post['lang'] : $Core->i18n()->getCurrLang();
                $entryUserId   = $userId;
                $entryClientId = $siteId;

                $getEntry = ($entryId !== false) ? $mailerClass->getTemplate($entryId) : array();

                $isEntryPersonal = (count($getEntry) && array_key_exists('type',$getEntry) && $getEntry['type'] == 2);
                $isEntryGlobal   = (count($getEntry) && array_key_exists('type',$getEntry) && $getEntry['type'] === 1);
                $isEntrySystem   = (count($getEntry) && array_key_exists('type',$getEntry) && $getEntry['type'] === 0);

                $entryCode = (count($getEntry)) ? $getEntry['code'] : $entryCode;

                $canEdit = (!$isEntrySystem && (($isEntryGlobal && $canEditGlobal) || $isEntryPersonal));

                if ($entryName != '' && $entryContent != '') {

                    $saveData = array();

                    $saveData['name']             = $entryName;
                    $saveData['code']             = $entryCode;
                    $saveData['subject']          = $entrySubject;
                    $saveData['content']          = $entryContent;
                    $saveData['ishtml']           = $entryIshtml;
                    $saveData['id_acc_users']     = $entryUserId;
                    $saveData['type']             = $entryType;
                    $saveData['lang']             = $entryLang;
                    $saveData['id_system_sites'] = $entryClientId;

                    if($entryId !== false) {
                        $saveData['id'] = $entryId;
                        if(!$canEdit) {
                            $note = $Core->i18n()->translate('Keine Berechtigung zum ändern dieser Vorlage!');
                            $type = 'danger';
                            $kind = 'bs-alert';
                            $title = $Core->i18n()->translate('Fehler') . '!';

                            $Core->setNote($note, $type, $kind, $title);
                            $Core->Request()->redirect($redirectUrl);
                        }
                    }

                    $saveResult = $mailerClass->saveTemplate($saveData);
                    if (is_array($saveResult) && isset($saveResult['success'])) {
                        if ($saveResult['success']) {
                            $note = $saveResult['msg'];
                            $type = 'success';
                            $kind = 'bs-alert';
                            $title = '';
                        } else {
                            $note = $saveResult['msg'];
                            $type = 'danger';
                            $kind = 'bs-alert';
                            $title = $Core->i18n()->translate('Fehler') . '!';
                        }
                    } else {
                        $note = $Core->i18n()->translate('Vorlage konnte nicht gespeichert werden!');
                        $type = 'danger';
                        $kind = 'bs-alert';
                        $title = $Core->i18n()->translate('Fehler') . '!';
                    }
                } else {
                    $note = $Core->i18n()->translate('Bitte alle Felder ausfüllen!');
                    $type = 'danger';
                    $kind = 'bs-alert';
                    $title = $Core->i18n()->translate('Fehler') . '!';
                }
                $Core->setNote($note, $type, $kind, $title);
                $Core->Request()->redirect($redirectUrl);
            }

            $return .= '<pre>$post => ' . print_r($post, true) . '</pre>';

        }

        return $return;
    }
}

function deleteAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mailerClass Mailer
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mailerClass = $Mvc->modelClass('Mailer');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('mailer_mail_send')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $site   = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $curUser   = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $canSeeGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_templates_global'));
        $canEditGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_templates_global_edit'));

        $template = array();
        $params = $Mvc->getMvcParams();

        $templateId = 0;
        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id') {
                $paramEntryId = (isset($params['id'])) ? (int)$params['id'] : 0;
            } else {
                $paramEntryId = (int)$first_key;
            }

            $templateId = $paramEntryId;
        }

        if($templateId !== 0) {
            $template = $mailerClass->getTemplate($templateId);
        }

        $redirectUrl = $Mvc->getModelUrl() . '/templates';

        if(!count($template)) {
            $note = $Core->i18n()->translate('Vorlage konnte nicht gefunden werden');
            $type = 'danger';
            $kind = 'bs-alert';
            $title = $Core->i18n()->translate('Fehler') . '!';

            $Core->setNote($note, $type, $kind, $title);
            $Core->Request()->redirect($redirectUrl);
        }

        $canDelete = ($template['type'] > 0 && (($template['type'] == 2 && $template['id_acc_users'] == $curUserId) || ($template['type'] === 1 && $canEditGlobal)));

        if(!$canDelete) {
            $note = $Core->i18n()->translate('Keine Berechtigung zum löschen dieser Vorlage!');
            $type = 'danger';
            $kind = 'bs-alert';
            $title = $Core->i18n()->translate('Fehler') . '!';

            $Core->setNote($note, $type, $kind, $title);
            $Core->Request()->redirect($redirectUrl);
        } else {
            $saveResult = $mailerClass->deleteTemplate($template['id']);
            if (is_array($saveResult) && isset($saveResult['success'])) {
                if ($saveResult['success']) {
                    $note = $saveResult['msg'];
                    $type = 'success';
                    $kind = 'bs-alert';
                    $title = '';
                } else {
                    $note = $saveResult['msg'];
                    $type = 'danger';
                    $kind = 'bs-alert';
                    $title = $Core->i18n()->translate('Fehler') . '!';
                }
            } else {
                $note = $Core->i18n()->translate('Vorlage konnte nicht gelöscht werden!');
                $type = 'danger';
                $kind = 'bs-alert';
                $title = $Core->i18n()->translate('Fehler') . '!';
            }
            $Core->setNote($note, $type, $kind, $title);
            $Core->Request()->redirect($redirectUrl);
        }

        return null;
    }
}