<?php
/**
 * Mailer Model Controller AddressBook
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function indexAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mailerClass Mailer
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mailerClass = $Mvc->modelClass('Mailer');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('mailer_addressbook')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('mailer_addressbook') . ' - ' . $Core->i18n()->translate('mailer') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('mailer') . ' - ' . $Core->i18n()->translate('mailer_addressbook'));

        $site   = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $curUser   = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $canSeeGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_addressbook_global'));
        $canEditGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_addressbook_global_edit'));

        $getAddressBooksByCurrentUser = $mailerClass->getAddressBooksByCurrentUser();
        $getAddressBook = $getAddressBooksByCurrentUser['addressbook'];
        $getAddressBookGlobal = $getAddressBooksByCurrentUser['addressbookGlobal'];

        $return = '';

        $ob_return = '';
        if($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    }
}

function createAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mailerClass Mailer
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mailerClass = $Mvc->modelClass('Mailer');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('mailer_addressbook')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('Adressbuch Eintrag erstellen') . ' - ' . $Core->i18n()->translate('mailer') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('mailer') . ' - ' . $Core->i18n()->translate('Adressbuch Eintrag erstellen'));

        $site   = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $curUser   = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $canEditGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_addressbook_global_edit'));

        $return = '';

        $ob_return = '';
        if($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    }
}

function editAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mailerClass Mailer
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mailerClass = $Mvc->modelClass('Mailer');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('mailer_addressbook')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('Adressbuch Eintrag bearbeiten') . ' - ' . $Core->i18n()->translate('mailer') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('mailer') . ' - ' . $Core->i18n()->translate('Adressbuch Eintrag bearbeiten'));

        $site   = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $curUser   = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $addressBookEntry = array();
        $params = $Mvc->getMvcParams();
        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id') {
                $paramEntryId = (isset($params['id'])) ? (int)$params['id'] : 0;
            } else {
                $paramEntryId = (int)$first_key;
            }

            $addressBookEntryId = $paramEntryId;

            if($addressBookEntryId !== 0) {
                $canSeeGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_addressbook_global'));

                $getAddressBooksByCurrentUser = $mailerClass->getAddressBooksByCurrentUser();
                $getAddressBook = $getAddressBooksByCurrentUser['addressbook'];
                $getAddressBookGlobal = $getAddressBooksByCurrentUser['addressbookGlobal'];

                if(array_key_exists($addressBookEntryId, $getAddressBook) || array_key_exists($addressBookEntryId, $getAddressBookGlobal)) {
                    $addressBookEntry = (isset($getAddressBook[$addressBookEntryId])) ? $getAddressBook[$addressBookEntryId] : $getAddressBookGlobal[$addressBookEntryId];
                }
            }
        }

        if(!count($addressBookEntry)) {
            $note = $Core->i18n()->translate('Adressbuch Eintrag konnte nicht gefunden werden');
            $type = 'danger';
            $kind = 'bs-alert';
            $title = $Core->i18n()->translate('Fehler') . '!';

            $Core->setNote($note, $type, $kind, $title);

            $redirectUrl = $Mvc->getModelUrl() . '/addressbook';
            $Core->Request()->redirect($redirectUrl);
        }

        $canEditGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_addressbook_global_edit'));

        $return = '';

        $ob_return = '';
        if($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    }
}

function saveAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mailerClass Mailer
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mailerClass = $Mvc->modelClass('Mailer');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('mailer_mail_send')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $user     = (is_object($accClass)) ? $accClass->getUser() : array();
        $userId   = (count($user)) ? $user['id'] : 0;

        $site   = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $canSeeGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_addressbook_global'));
        $canEditGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_addressbook_global_edit'));

        $getAddressBooksByCurrentUser = $mailerClass->getAddressBooksByCurrentUser();
        $getAddressBook = $getAddressBooksByCurrentUser['addressbook'];
        $getAddressBookGlobal = $getAddressBooksByCurrentUser['addressbookGlobal'];

        $return = '';
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $post = $Core->Request()->getPost();

            if(isset($post['action']) && $post['action'] == 'addressbook_entry_save') {
                $redirectUrl = $Mvc->getModelUrl() . '/addressbook';

                $entryId       = (isset($post['id'])) ? $post['id'] : false;
                $entryName     = (isset($post['name'])) ? $post['name'] : '';
                $entrySurname  = (isset($post['surname'])) ? $post['surname'] : '';
                $entryEmail    = (isset($post['email'])) ? $post['email'] : '';
                $entryComment  = (isset($post['comment'])) ? $post['comment'] : '';
                $entryUserId   = ($canEditGlobal && isset($post['addressbooktype']) && $post['addressbooktype'] != 1) ? 0 : $userId;
                $entryClientId = $siteId;

                $isEntryIdPersonal = ($entryId !== false && array_key_exists($entryId,$getAddressBook));
                $isEntryIdGlobal = ($entryId !== false && array_key_exists($entryId,$getAddressBookGlobal));

                $canEdit = (($isEntryIdGlobal && $canEditGlobal) || $isEntryIdPersonal);

                if ($entryName != '' && $entrySurname != '' && $entryEmail != '') {

                    $saveData = array();

                    $saveData['name']             = $entryName;
                    $saveData['surname']          = $entrySurname;
                    $saveData['email']            = $entryEmail;
                    $saveData['comment']          = $entryComment;
                    $saveData['id_acc_users']     = $entryUserId;
                    $saveData['id_system_sites']  = $entryClientId;

                    if($entryId !== false) {
                        $saveData['id'] = $entryId;
                        if(!$canEdit) {
                            $note = $Core->i18n()->translate('Keine Berechtigung zum ändern dieses Eintrags!');
                            $type = 'danger';
                            $kind = 'bs-alert';
                            $title = $Core->i18n()->translate('Fehler') . '!';

                            $Core->setNote($note, $type, $kind, $title);
                            $Core->Request()->redirect($redirectUrl);
                        }
                    }

                    $saveResult = $mailerClass->saveAddressBookEntry($saveData);
                    if (is_array($saveResult) && isset($saveResult['success'])) {
                        if ($saveResult['success']) {
                            $note = $saveResult['msg'];
                            $type = 'success';
                            $kind = 'bs-alert';
                            $title = '';
                        } else {
                            $note = $saveResult['msg'];
                            $type = 'danger';
                            $kind = 'bs-alert';
                            $title = $Core->i18n()->translate('Fehler') . '!';
                        }
                    } else {
                        $note = $Core->i18n()->translate('Eintrag konnte nicht gespeichert werden!');
                        $type = 'danger';
                        $kind = 'bs-alert';
                        $title = $Core->i18n()->translate('Fehler') . '!';
                    }
                } else {
                    $note = $Core->i18n()->translate('Bitte alle Felder ausfüllen!');
                    $type = 'danger';
                    $kind = 'bs-alert';
                    $title = $Core->i18n()->translate('Fehler') . '!';
                }
                $Core->setNote($note, $type, $kind, $title);
                $Core->Request()->redirect($redirectUrl);
            }

            $return .= '<pre>$post => ' . print_r($post, true) . '</pre>';

        }

        return $return;
    }
}

function deleteAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mailerClass Mailer
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mailerClass = $Mvc->modelClass('Mailer');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('mailer_mail_send')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $site   = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $curUser   = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        $canSeeGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_addressbook_global'));
        $canEditGlobal = (!is_object($accClass) || $accClass->hasAccess('mailer_addressbook_global_edit'));

        $addressBookEntry = array();
        $params = $Mvc->getMvcParams();
        if(count($params)) {
            reset($params);
            $first_key = key($params);
            if($first_key == 'id') {
                $paramEntryId = (isset($params['id'])) ? (int)$params['id'] : 0;
            } else {
                $paramEntryId = (int)$first_key;
            }

            $addressBookEntryId = $paramEntryId;

            if($addressBookEntryId !== 0) {

                $getAddressBooksByCurrentUser = $mailerClass->getAddressBooksByCurrentUser();
                $getAddressBook = $getAddressBooksByCurrentUser['addressbook'];
                $getAddressBookGlobal = $getAddressBooksByCurrentUser['addressbookGlobal'];

                if(array_key_exists($addressBookEntryId, $getAddressBook) || array_key_exists($addressBookEntryId, $getAddressBookGlobal)) {
                    $addressBookEntry = (isset($getAddressBook[$addressBookEntryId])) ? $getAddressBook[$addressBookEntryId] : $getAddressBookGlobal[$addressBookEntryId];
                }
            }
        }

        if(!count($addressBookEntry)) {
            $note = $Core->i18n()->translate('Adressbuch Eintrag konnte nicht gefunden werden');
            $type = 'danger';
            $kind = 'bs-alert';
            $title = $Core->i18n()->translate('Fehler') . '!';

            $Core->setNote($note, $type, $kind, $title);

            $redirectUrl = $Mvc->getModelUrl() . '/addressbook';
            $Core->Request()->redirect($redirectUrl);
        }

        $redirectUrl = $Mvc->getModelUrl() . '/addressbook';

        $canDelete = (($addressBookEntry['id_system_sites'] == 0 && $canEditGlobal) || $addressBookEntry['id_system_sites'] == $siteId);

        if(!$canDelete) {
            $note = $Core->i18n()->translate('Keine Berechtigung zum löschen dieses Eintrags!');
            $type = 'danger';
            $kind = 'bs-alert';
            $title = $Core->i18n()->translate('Fehler') . '!';

            $Core->setNote($note, $type, $kind, $title);
            $Core->Request()->redirect($redirectUrl);
        } else {
            $saveResult = $mailerClass->deleteAddressBookEntry($addressBookEntry['id']);
            if (is_array($saveResult) && isset($saveResult['success'])) {
                if ($saveResult['success']) {
                    $note = $saveResult['msg'];
                    $type = 'success';
                    $kind = 'bs-alert';
                    $title = '';
                } else {
                    $note = $saveResult['msg'];
                    $type = 'danger';
                    $kind = 'bs-alert';
                    $title = $Core->i18n()->translate('Fehler') . '!';
                }
            } else {
                $note = $Core->i18n()->translate('Eintrag konnte nicht gelöscht werden!');
                $type = 'danger';
                $kind = 'bs-alert';
                $title = $Core->i18n()->translate('Fehler') . '!';
            }
            $Core->setNote($note, $type, $kind, $title);
            $Core->Request()->redirect($redirectUrl);
        }

        return null;
    }
}