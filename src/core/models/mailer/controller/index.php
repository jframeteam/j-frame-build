<?php
/**
 * Mailer Model Controller Index
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

function indexAction() {
    /**
     * @var $Core Core
     * @var $Mvc Mvc
     * @var $mailerClass Mailer
     * @var $accClass Acc
     */
    global $Core, $Mvc;

    $mailerClass = $Mvc->modelClass('Mailer');
    $accClass = $Mvc->modelClass('Acc');

    if(is_object($accClass) && !$accClass->hasAccess('mailer_mail')) {
        return $Core->i18n()->translate('Keine Zugriffsberechtigung für diesen Bereich...');
    } else {

        $oldMetaTitle = $Mvc->getMetaTitle();
        $Mvc->setMetaTitle($Core->i18n()->translate('Übersicht') . ' - ' . $Core->i18n()->translate('mailer') . ' - ' . $oldMetaTitle);
        $oldPageTitle = $Mvc->getPageTitle();
        $Mvc->setPageTitle($Core->i18n()->translate('mailer') . ' - ' . $Core->i18n()->translate('Übersicht'));

        $site   = $Core->Sites()->getSite();
        $siteId = (count($site)) ? $site['id'] : 0;

        $curUser   = (is_object($accClass)) ? $accClass->getUser() : array();
        $curUserId = (count($curUser)) ? $curUser['id'] : 0;

        /** get Mail Array for Counting */
        $getMailsFull = $mailerClass->getMails($siteId);
        $getMails = array();
        foreach($getMailsFull as $mailId => $mail) {
            if(array_key_exists('id_acc_users', $mail) && $mail['id_acc_users'] == $curUserId) {
                $getMails[$mailId] = $mail;
            }
        }
        $getMailCount = count($getMails);

        /** get Address Books for Counting */
        $getAddressBooksByCurrentUser = $mailerClass->getAddressBooksByCurrentUser();
        $getAddressBook       = $getAddressBooksByCurrentUser['addressbook'];
        $getAddressBookGlobal = $getAddressBooksByCurrentUser['addressbookGlobal'];
        $addressBookEntriesCount = count($getAddressBook) + count($getAddressBookGlobal);

        /** get E-Mail Templates for Counting */
        $getTemplatesByCurrentUser = $mailerClass->getTemplatesByCurrentUser();
        $getTemplates       = $getTemplatesByCurrentUser['templates'];
        $getTemplatesGlobal = $getTemplatesByCurrentUser['templatesGlobal'];
        $getTemplatesSystem = $getTemplatesByCurrentUser['templatesSystem'];
        $templatesCount     = count($getTemplates) + count($getTemplatesGlobal) + count($getTemplatesSystem);

        $return = '';

        $ob_return = '';
        if($Mvc->getMVCViewFilePath() !== false) {
            try {
                ob_start();
                include($Mvc->getMVCViewFilePath());
                $ob_return = ob_get_contents();
                ob_end_clean();
            } catch (Exception $e) {
                $ob_return = $e->getMessage();
            }
        } else {
            $return .= '<div class="alert alert-danger" role="alert">' . $Core->i18n()->translate('Controller View nicht gefunden.') . '</div>';
        }

        $return .= $ob_return;

        return $return;
    }
}