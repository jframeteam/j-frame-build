<?php
/**
 * Shell File
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/** @var $Core */

/** INIT File including */
require_once('init.php');

/** @var $Shell Shell */
$Shell = $Core->Shell();

$shellArgs = $_SERVER['argv'];
$scriptFilename = (array_key_exists('SCRIPT_FILENAME',$_SERVER)) ? $_SERVER['SCRIPT_FILENAME'] : basename(__FILE__);
if(in_array($scriptFilename,$shellArgs)) {
    foreach($shellArgs as $aKey => $arg) {
        if($arg == $scriptFilename) {
            unset($shellArgs[$aKey]);
            $shellArgs = array_values($shellArgs);
        }
    }
}

echo $Shell->exec($shellArgs);
