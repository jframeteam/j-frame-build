<?php
/**
 * Main Init File
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */
$is_cli = (!isset($_SERVER['SERVER_SOFTWARE']) && (php_sapi_name() == 'cli' || (array_key_exists('argv',$_SERVER) && is_numeric($_SERVER['argc']) && $_SERVER['argc'] > 0)));
if($is_cli) {
    $cliSessId = md5(__FILE__.date('Y-m-d'));
    session_id($cliSessId);
}
session_start();

define('DS'            , DIRECTORY_SEPARATOR);
define('JF_ROOT_DIR'   , str_replace(DS . 'core', '',  dirname(__FILE__)));
if($is_cli) {
    define('JF_SERVER_NAME', '');
    define('JF_BASE_URL', '');
} else {
    define('JF_SERVER_NAME', $_SERVER['SERVER_NAME']);
    define('JF_BASE_URL', str_replace(array('core' . DS . 'init.php', '\\'), array('', '/'), 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . rtrim($_SERVER['HTTP_HOST'], '/') . '/' . ltrim(substr(__FILE__, strlen($_SERVER['DOCUMENT_ROOT'])), '/')));
}
$corePath   = JF_ROOT_DIR . DS . 'core' . DS;
$configPath = JF_ROOT_DIR . DS . 'config' . DS;
$varPath    = JF_ROOT_DIR . DS . 'var' . DS;

/** Check PHP Version */
if (version_compare(PHP_VERSION, '5.6.30', '<')) {
    die('<pre>Minimum PHP Version 5.6.30 is required! (Current ' . PHP_VERSION . ')</pre>');
}

/** Check if IonCube Loader is loaded */
//$isIonCubeLoaded  = false; foreach(get_loaded_extensions() as $extensionName) { if(strpos(strtolower($extensionName), 'ioncube') !== false) { $isIonCubeLoaded = true; break; } }
//if (!$isIonCubeLoaded) {
//    die('<pre>The ionCube Loader is required!</pre>');
//}

/** Get User from Session */
$getUserFromSession = (array_key_exists('user', $_SESSION) && is_array($_SESSION['user'])) ? $_SESSION['user'] : array();
$isSu = (count($getUserFromSession) && array_key_exists('su', $getUserFromSession) && $getUserFromSession['su']);

/** Check for Maintenance Mode */
if(file_exists($varPath . 'lock' . DS . 'maintenance.lock') && !$isSu) {
    die('This Page is under maintenance. Please try again later.');
}

// Include Config
include($configPath . 'config.php');
// Load Classes
include($corePath . 'classes' . DS . '_classloader.php');

/** Instantiate Core Class */
/** @var $Core Core */
$Core = new Core();