/**
 * Core JavaScript
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

thisBaseUrl    = typeof thisBaseUrl    !== 'undefined' ? thisBaseUrl    : '';
thisCurrLang   = typeof thisCurrLang   !== 'undefined' ? thisCurrLang   : '';
getDefaultLang = typeof getDefaultLang !== 'undefined' ? getDefaultLang : '';
thisCurrUserId = (typeof thisCurrUserId !== 'undefined' ? thisCurrUserId : 0);

var langUrlSlug = (getDefaultLang != '' && thisCurrLang != getDefaultLang) ? thisCurrLang + '/' : '';

var core = {
    init: function() {

        $.fn.dataStartsWith = function(p) {
            var pCamel = p.replace(/-([a-z])/ig, function(m,$1) { return $1.toUpperCase(); });
            return this.filter(function(i, el){
                return Object.keys(el.dataset).some(function(v){
                    return v.indexOf(pCamel) > -1;
                });
            });
        };

        core.i18n.init();
        core.ays_autoload();
        core.datePicker.init();
        core.modalActions();

        if(typeof acc.user !== 'undefined') {
            /** Delay a second to load User Options */
            setTimeout(function(){ core.dataTable.init(); }, 1000);
        } else {
            core.dataTable.init();
        }

        /** Popover / Tooltip Init */
        $('body').popover({
            selector: '[data-toggle="popover"]',
            html: true,
            container: 'body',
            viewport: 'body'
        });

        $('body').tooltip({
            selector: '[data-toggle="tooltip"]',
            html: true,
            container: 'body',
            viewport: 'body'
        });

        $("[data-toggle='popover']").on('show.bs.popover', function(){
            $("[data-toggle='tooltip']").tooltip("hide");
        });

        $("[data-toggle='popover']").on('shown.bs.popover', function(){
            core.modalActions();
        });

        $("[data-toggle='popover']").on('hide.bs.popover', function(){
            $("[data-toggle='tooltip']").tooltip("hide");
        });

        /** Open BS Tab by URL Hash */
        var hash = window.location.hash;
        if(hash && $('.nav-tabs a[href="' + hash + '"]').length) {
            $('.nav-tabs a[href="' + hash + '"]').tab('show'); //or trigger('click')
        }

        /** Alert Autoclose */
        $('.alert.autoclose').each(function () {
            $(this).show().delay(5000).slideUp('slow', function () {
                $(this).remove();
            });
        });

        if(typeof Hyphenator !== 'undefined') {
            var hyphenatorClasses = [
                'page-header',
                'breadcrumb'
            ];

            var countHyphenated = 0;
            $.each(hyphenatorClasses, function(i,className) {
                if(core.helper.checkForHyphens('.' + className)) {
                    countHyphenated++;
                }
            });

            Hyphenator.config({
                classname: 'page-header',
                selectorfunction: function () {
                    var es=[],xs,l;
                    // iterate on each class to be hyphenated
                    $.each(hyphenatorClasses, function(i,ele){
                        xs=$('.'+ele);
                        if(xs.length) {
                            $.each(xs, function(subi,subele) {
                                es.push(subele);
                            });
                        }
                    });
                    return(es);
                }
            });

            if(countHyphenated < 1) {
                Hyphenator.run();
            }
        }
    },

    modalActions:  function() {
        /** Modal Actions */
        $(document).on('show.bs.modal', function(event) {
            $('[data-toggle="popover"]').popover('hide');
            $('[data-toggle="tooltip"]').tooltip('hide');

            var modal = $(event.target);
            var modalDialog = modal.find('.modal-dialog');
            modalDialog.append('<div class="modal-pre-loader"><div class="center-vertical text-center loader"><i class="fa fa-circle-o-notch fa-spin fa-5x fa-fw text-primary"></i><span class="sr-only">Loading...</span></div></div>')
        });

        $(document).on('shown.bs.modal', function(event) {
            setTimeout( function() {
                var modal = $(event.target);
                var modalPreLoader = modal.find('.modal-pre-loader');
                if(modalPreLoader.length){
                    modalPreLoader.fadeOut('fast', function() {
                        modalPreLoader.remove();
                    });
                }
                core.ays_autoload();

                if(modal.find('.table[data-datatable]').length) {
                    core.dataTable.init();
                }
            }, 500);

            core.datePicker.init();
        });

        $(document).on('hide.bs.modal', function(e) {
            e = e || event;
            var modal = $(e.target);
            var dtReload = ((typeof modal.data('dtreload') !== 'undefined') && (typeof modal.attr('data-dtreload') !== 'undefined'));
            if(dtReload) {
                core.dataTable.reload();
            }
            $('.modal').css('overflowY', 'auto');
        });

        /** Run Ajax Modal Manually */
        if($('[data-toggle="modal"]').length){
            $.each($('[data-toggle="modal"]'), function(i,ele) {
                var modalLoaderEle = $(ele);
                var targetModalSelector = (typeof modalLoaderEle.data('target') !== 'undefined') ? modalLoaderEle.data('target') : '';
                var targetModal_href = (typeof modalLoaderEle.attr('href') !== 'undefined') ? modalLoaderEle.attr('href') : '';
                var targetModal_ajaxtype = (typeof modalLoaderEle.data('ajaxtype') !== 'undefined') ? modalLoaderEle.data('ajaxtype') : '';

                if(targetModalSelector !== '' && targetModal_href !== '') {

                    modalLoaderEle.unbind('click').unbind('touchstart').on('click touchstart', function (e) {
                        e.preventDefault();
                        e.stopPropagation();

                        if(targetModal_ajaxtype === 'post') {
                            var postData = {};

                            modalLoaderEle.dataStartsWith('post').each(function(i, el){
                                $.each(el.dataset, function(label, value){
                                    if(label.indexOf('post') !== -1) {
                                        var newLabel = label.toLowerCase().replace('post','');
                                        postData[newLabel] = value;
                                    }
                                });
                            });

                            $.post(targetModal_href, postData)
                                .done(function (data) {
                                    if (data.success === true) {
                                        $(targetModalSelector).find('.modal-content').html(data.result);
                                    } else {
                                        $(targetModalSelector).find('.modal-content').html('<div class="alert alert-danger" role="alert">' + data + '</div>');
                                    }
                                    $(targetModalSelector).modal('show', modalLoaderEle);
                                });

                        } else {
                            $.get(targetModal_href, function (data) {
                                if (data.success === true) {
                                    $(targetModalSelector).find('.modal-content').html(data.result);
                                } else {
                                    $(targetModalSelector).find('.modal-content').html('<div class="alert alert-danger" role="alert">' + data + '</div>');
                                }
                                $(targetModalSelector).modal('show', modalLoaderEle);
                            });
                        }
                    });

                }
            });
        }

        /** Modal Autoshow */
        $('.modal.autoshow').each(function () {
            $('#' + $(this).attr('id')).modal('show');
        });
    },

    checkAjaxCall: function(jqxhr) {
        jqxhr = (typeof jqxhr !== 'undefined') ? jqxhr : '';
        if(jqxhr !== '') {
            var xhr_responseJSON = (typeof jqxhr.responseJSON !== 'undefined') ? jqxhr.responseJSON : '';
            var xhr_responseText = (typeof jqxhr.responseText !== 'undefined') ? jqxhr.responseText : '';
            var isError = false;
            if(xhr_responseJSON === '' && xhr_responseText !== '') {
                var xhr_responseTextInJSON = {};
                try {
                    xhr_responseTextInJSON = (xhr_responseText.indexOf('{') !== -1) ? $.parseJSON(xhr_responseText) : xhr_responseText;
                } catch (e) {
                    console.log(e.name+':', e.message);
                }
                if(typeof xhr_responseTextInJSON !== 'object') {
                    isError = true;
                } else {
                    var main_success = (typeof xhr_responseTextInJSON.success !== 'undefined') ? xhr_responseTextInJSON.success : false;
                    var main_result  = (typeof xhr_responseTextInJSON.result  !== 'undefined') ? xhr_responseTextInJSON.result  : '';
                    if(main_success) {
                        if(typeof main_result === 'object') {
                            var result_success = (typeof main_result.success !== 'undefined') ? main_result.success : false;
                            if (!result_success) {
                                isError = true;
                            }
                        }
                    } else {
                        isError = true;
                    }
                }
            }
            if(isError) {
                if (!$('.jBsAlerts--dynamicAdded.ajaxErrorAlert').length) {
                    var alertMessage = 'Ajax Error!';
                    if (xhr_responseText.indexOf('login-wrapper') !== -1) {
                        alertMessage = core.i18n.translate('Nicht eingeloggt!');
                    }
                    if (xhr_responseText.indexOf('Core Error Log') !== -1) {
                        alertMessage = xhr_responseText;
                    }
                    core.bs_alert.danger(alertMessage, 'ajaxErrorAlert autoclose');
                }
                console.warn('CFM checkAjaxCall xhr_responseText:', xhr_responseText);
                return false;
            } else {
                return true;
            }
        }
    },

    ays:           function(options) {
        var settings = $.extend({
            areYouSureTxt: core.i18n.translate('Ganz sicher?'),
            confirmTxt: core.i18n.translate('Ja'),
            declineTxt: core.i18n.translate('Nein'),
            cancelTxt: core.i18n.translate('Abbrechen'),
            confirmCallback: null,
            declineCallback: null,
            cancelCallback: null
        }, options);

        var areYouSureTxt   = settings.areYouSureTxt;
        var confirmTxt      = settings.confirmTxt;
        var declineTxt      = settings.declineTxt;
        var cancelTxt       = settings.cancelTxt;
        var confirmCallback = settings.confirmCallback;
        var declineCallback = settings.declineCallback;
        var cancelCallback  = settings.cancelCallback;

        var modalWrapperBefore = '<div id="confirm" class="modal fade ays-modal"><div class="modal-dialog"><div class="modal-content">';

        var modalBody = '<div class="modal-body">' + areYouSureTxt + '</div>';
        var modalFooter = '<div class="modal-footer">' +
            '<button type="button" data-dismiss="modal" class="btn btn-danger" id="confirm_ays">' + confirmTxt + '</button>' +
            '<button type="button" data-dismiss="modal" class="btn" id="decline_ays">' + declineTxt + '</button>';

        if(cancelCallback !== null) {
            modalFooter += '<button type="button" data-dismiss="modal" class="btn btn-default" id="cancel_ays">' + cancelTxt + '</button>';
        }

        modalFooter += '</div>';

        var modalContent = modalBody + modalFooter;

        var modalWrapperAfter = '</div></div></div>';

        var modalHtml = modalWrapperBefore + modalContent + modalWrapperAfter;

        var aysModal = $('.ays-modal');

        if(!aysModal.length) {
            $('body').append(modalHtml);
            aysModal = $('.ays-modal');
        } else {
            aysModal.modal('hide');
            aysModal.find('.modal-content').html(modalContent);
        }

        aysModal.modal({
            backdrop: 'static',
            keyboard: false
        })
            .one('click', '#confirm_ays', function (e) {
                aysModal.trigger('core.ays.confirmed');
                if (confirmCallback && typeof(confirmCallback) === "function") {
                    confirmCallback();
                }
            })
            .one('click', '#decline_ays', function (e) {
                aysModal.trigger('core.ays.declined');
                if (declineCallback && typeof(declineCallback) === "function") {
                    declineCallback();
                }
            })
            .one('click', '#cancel_ays', function (e) {
                aysModal.trigger('core.ays.cancel');
                if (cancelCallback && typeof(cancelCallback) === "function") {
                    cancelCallback();
                }
            });

        aysModal.on('shown.bs.modal', function (event) {
            $(this).find('#cancel_ays').focus();
        });

        aysModal.on('hide.bs.modal', function (event) {
            $('.modal').css('overflowY', 'auto');
        });

        return aysModal;
    },

    ays_autoload:  function() {
        if($('[data-action*="delete"], [data-ays]').length) {
            $('[data-action*="delete"], [data-ays]').each( function(i,ele) {
                var aysEle = $(ele);
                aysEle.unbind();
                aysEle.on('click', function (e) {
                    e.preventDefault();

                    var href = '';
                    if(typeof aysEle.attr('href') !== 'undefined'){
                        href = aysEle.attr('href');
                    }
                    var $form = aysEle.closest('form');

                    /** Trigger ays Modal */

                    var trigger_areYouSureTxt   = (typeof aysEle.data('aystext')         !== 'undefined' && aysEle.data('aystext')         !== '') ? aysEle.data('aystext')         : '';
                    var trigger_confirmTxt      = (typeof aysEle.data('confirmtext')     !== 'undefined' && aysEle.data('confirmtext')     !== '') ? aysEle.data('confirmtext')     : '';
                    var trigger_declineTxt      = (typeof aysEle.data('declinetxt')      !== 'undefined' && aysEle.data('declinetxt')      !== '') ? aysEle.data('declinetxt')      : '';
                    var trigger_cancelTxt       = (typeof aysEle.data('canceltext')      !== 'undefined' && aysEle.data('canceltext')      !== '') ? aysEle.data('canceltext')      : '';
                    var trigger_confirmCallback = (typeof aysEle.data('confirmcallback') !== 'undefined' && aysEle.data('confirmcallback') !== '') ? aysEle.data('confirmcallback') : '';
                    var trigger_declineCallback = (typeof aysEle.data('declinecallback') !== 'undefined' && aysEle.data('declinecallback') !== '') ? aysEle.data('declinecallback') : '';
                    var trigger_cancelCallback  = (typeof aysEle.data('cancelcallback')  !== 'undefined' && aysEle.data('cancelcallback')  !== '') ? aysEle.data('cancelcallback')  : '';

                    var aysData = {};
                    if(trigger_areYouSureTxt !== '') {
                        aysData.areYouSureTxt = trigger_areYouSureTxt;
                    }
                    if(trigger_confirmTxt !== '') {
                        aysData.confirmTxt = trigger_confirmTxt;
                    }
                    if(trigger_declineTxt !== '') {
                        aysData.declineTxt = trigger_declineTxt;
                    }
                    if(trigger_cancelTxt !== '') {
                        aysData.cancelTxt = trigger_cancelTxt;
                    }
                    if(trigger_confirmCallback !== '' && typeof(window[trigger_confirmCallback]) === "function") {
                        aysData.confirmCallback = function () {
                            window[trigger_confirmCallback](e,aysEle);
                        }
                    } else {
                        aysData.confirmCallback = function () {
                            if (href !== '') {
                                window.location.href = href;
                            } else {
                                $form.trigger('submit');
                            }
                        };
                    }
                    if(trigger_declineCallback !== '' && typeof(window[trigger_declineCallback]) === "function") {
                        aysData.declineCallback = function () {
                            window[trigger_declineCallback](e,aysEle);
                        }
                    }
                    if(trigger_cancelCallback !== '' && typeof(window[trigger_cancelCallback]) === "function") {
                        aysData.cancelCallback = function () {
                            window[trigger_cancelCallback](e,aysEle);
                        }
                    }

                    core.ays(aysData);
                });
            });
        }
    },

    bs_alert: {
        info:    function(message, addclass, targetSelector, toast) {
            addclass = typeof addclass !== 'undefined' ? addclass : '';
            core.bs_alert.add('info', message, addclass, targetSelector, toast);
        },
        success: function(message, addclass, targetSelector, toast) {
            addclass = typeof addclass !== 'undefined' ? addclass + ' autoclose' : 'autoclose';
            core.bs_alert.add('success', message, addclass, targetSelector, toast);
        },
        warning: function(message, addclass, targetSelector, toast) {
            addclass = typeof addclass !== 'undefined' ? addclass : '';
            core.bs_alert.add('warning', message, addclass, targetSelector, toast);
        },
        danger:  function(message, addclass, targetSelector, toast) {
            addclass = typeof addclass !== 'undefined' ? addclass : '';
            core.bs_alert.add('danger', message, addclass, targetSelector, toast);
        },

        add:     function(bsa_type, bsa_message, addclass, targetSelector, toast) {
            bsa_type = typeof bsa_type !== 'undefined' ? bsa_type : 'info';
            addclass = typeof addclass !== 'undefined' ? addclass.trim() : '';
            targetSelector = typeof targetSelector !== 'undefined' ? targetSelector.trim() : '.main-content';
            toast = typeof toast !== 'undefined' ? toast : false;

            var auto_close = (addclass.indexOf('autoclose') >= 0);

            var options = {
                type:      bsa_type,
                message:   core.i18n.translate(bsa_message),
                addClass:  addclass,
                autoClose: auto_close,
                acDelay:   5000,
                acSpeed:   'slow',
                toast:     toast
            };

            $(targetSelector).jBsAlerts(options);
        }
    },

    dataTable: {
        init:   function() {
            var dtEles     = $('.table[data-datatable]');

            if(dtEles.length && (typeof $.fn.dataTable !== 'undefined')) {

                /** Get User Options */
                var accUser              = (typeof acc.user !== 'undefined') ? acc.user : false;
                var curUserId            = thisCurrUserId;

                var uo_pageLength        = 50;
                var uo_currOrderData_obj = {};

                if (accUser !== false && curUserId > 0) {
                    accUser.options.setData();

                    uo_pageLength = (core.helper.isNumeric(accUser.options.get('dt_pageLength'))) ? parseInt(accUser.options.get('dt_pageLength')) : uo_pageLength;
                    uo_currOrderData_obj = (core.helper.isObject(accUser.options.get('dt_orderData'))) ? accUser.options.get('dt_orderData') : uo_currOrderData_obj;
                }

                $.each(dtEles, function(i,ele) {

                    var curUrlHash = core.helper.md5(core.Request.getCurrUrl(false));
                    var dtEle_uid  = curUrlHash + '_' + i;

                    /** Get User Options */
                    var uo_orderData = [];

                    if(uo_currOrderData_obj[dtEle_uid] !== 'undefined' && core.helper.isArray(uo_currOrderData_obj[dtEle_uid])) {
                        uo_orderData = uo_currOrderData_obj[dtEle_uid];
                    }

                    if(uo_orderData.length) {
                        var uo_orderData_array = [];
                        uo_orderData_array.push(uo_orderData);
                        uo_orderData = uo_orderData_array;
                    }

                    var dtEle = $(ele);
                    var dtConfig = {};
                    var dtAjaxUrl = typeof dtEle.data('dtsource') !== 'undefined' ? dtEle.data('dtsource') : '';
                    var dtButtons = typeof dtEle.data('dtbuttons') !== 'undefined' ? dtEle.data('dtbuttons') : '';
                    var dtPageLength = typeof dtEle.data('dtpagelength') !== 'undefined' ? dtEle.data('dtpagelength') : uo_pageLength;

                    dtConfig.language = {
                        'sProcessing': '<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'
                    };
                    if (thisCurrLang == 'de') {
                        dtConfig.language = {
                            'url': thisBaseUrl + 'core/components/dataTables/plug-ins/i18n/German.json',
                            'sProcessing': '<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>'
                        };
                    }

                    dtConfig.pagingType = 'full_numbers';

                    dtConfig.pageLength = dtPageLength;

                    /** User Option Order */
                    if(uo_orderData.length) {
                        dtConfig.order = uo_orderData;
                    }

                    var dtColumns = dtEle.find('thead th');

                    var dtColumnsOption = [];

                    dtColumns.each(function (i, ele) {
                        var thEle = $(ele);

                        var dataSearchable = typeof thEle.data('dtsearchable') !== 'undefined' ? thEle.data('dtsearchable') : '';

                        var dataSortable = typeof thEle.data('dtsortable') !== 'undefined' ? thEle.data('dtsortable') : '';

                        var dataDefaultSort = typeof thEle.data('dtdefaultsort') !== 'undefined' ? thEle.data('dtdefaultsort') : false;

                        var dtColumn = {};

                        if (dataSearchable === 0) {
                            dtColumn.searchable = false;
                        }

                        if (dataDefaultSort !== false && !uo_orderData.length) {
                            var dtDSId = i;
                            var dtDS = (dataDefaultSort !== '') ? dataDefaultSort : 'asc';

                            dtConfig.order = [[dtDSId, dtDS]];
                        }

                        if (dataSortable === 0) {
                            dtColumn.orderable = false;
                        }

                        dtColumnsOption.push(dtColumn);
                    });

                    if (dtColumnsOption.length) {
                        dtConfig.columns = dtColumnsOption;
                    }

                    if (dtAjaxUrl !== '') {
                        dtConfig.processing = true;
                        dtConfig.serverSide = true;
                        dtConfig.ajax = dtAjaxUrl;
                        // dtConfig.ajax       = {'url':dtAjaxUrl,'dataSrc':''};
                    }

                    /** Prevent DataTables Error Alerts */
                    $.fn.dataTable.ext.errMode = 'none';

                    /** Add Buttons */

                    if (dtButtons !== '') {
                        $.fn.dataTable.ext.buttons.refresh = {
                            text: '<i class="fa fa-refresh" aria-hidden="true" title="' + core.i18n.translate('Aktualisieren') + '" data-toggle="tooltip"></i>',
                            action: function (e, dt, node, config) {
                                dt.clear().draw();
                                dt.ajax.reload();
                            },
                            className: 'btn btn-default btn-sm buttons-refresh'
                        };

                        dtConfig.dom = 'l<"clear">Bfrtip';
                        dtConfig.buttons = {
                            buttons: [
                                'refresh',
                                {
                                    extend: 'copy',
                                    text: '<i class="fa fa-clipboard" aria-hidden="true" title="' + core.i18n.translate('Kopieren') + '" data-toggle="tooltip"></i>',
                                },
                                {
                                    extend: 'csv',
                                    text: '<i class="fa fa-file-excel-o" aria-hidden="true" title="Export: CSV" data-toggle="tooltip"></i>',
                                },
                                {
                                    extend: 'excel',
                                    text: '<i class="fa fa-file-excel-o" aria-hidden="true" title="Export: Excel" data-toggle="tooltip"></i>',
                                },
                                {
                                    extend: 'pdf',
                                    text: '<i class="fa fa-file-pdf-o" aria-hidden="true" title="Export: PDF" data-toggle="tooltip"></i>',
                                },
                                {
                                    extend: 'print',
                                    text: '<i class="fa fa-print" aria-hidden="true" title="' + core.i18n.translate('Drucken') + '" data-toggle="tooltip"></i>',
                                }
                            ],
                            dom: {
                                container: {
                                    tag: 'div',
                                    className: 'dt-buttons btn-group btn-group-sm'
                                },
                                button: {
                                    className: 'dt-button btn btn-default btn-sm'
                                }
                            }
                        };
                    }

                    /** Init Data Table */
                    var dataTable = null;
                    if ( !$.fn.dataTable.isDataTable( dtEle ) ) {
                        dataTable = dtEle.DataTable(dtConfig);

                        if(dataTable !== null) {
                            // new $.fn.dataTable.Buttons( dataTable, {
                            //     name: 'commands',
                            //     buttons: [ "refresh"]
                            // } );
                            //
                            // dataTable.buttons( 0, null ).containers().prependTo( dtEle.closest('.tab-pane') );

                            /** On Error, show in Console */
                            dataTable.on('error.dt', function (e, settings, techNote, message) {
                                console.error('An error has been reported by DataTables: ', message);
                            });

                            /** On Processing, add Processing Element */
                            dataTable.on('processing.dt', function () {
                                var dtEleProcessing = dtEle.closest('.dataTables_wrapper').find('.dataTables_processing');
                                if (dtEleProcessing.length) {
                                    if (!dtEleProcessing.find('.fa').length) {
                                        dtEleProcessing.html('<i class="fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>');
                                    }
                                }
                            });

                            /** On Draw, Add Advanced Pagination */
                            dataTable.on('draw.dt', function () {
                                var dtElePaginate = dtEle.closest('.dataTables_wrapper').find('.dataTables_paginate');
                                if (dtElePaginate.length) {
                                    if (dtElePaginate.find('.paginate_button').not('.first').not('.last').not('.previous').not('.next').length < 2) {
                                        dtElePaginate.hide();
                                    } else {
                                        dtElePaginate.show();
                                    }

                                    if (dtElePaginate.find('.paginate_button.first a').length) {
                                        dtElePaginate.find('.paginate_button.first a').html('<span class="fa fa-angle-double-left" aria-hidden="true"></span>');
                                    }
                                    if (dtElePaginate.find('.paginate_button.previous a').length) {
                                        dtElePaginate.find('.paginate_button.previous a').html('<span class="fa fa-angle-left" aria-hidden="true"></span>');
                                    }
                                    if (dtElePaginate.find('.paginate_button.next a').length) {
                                        dtElePaginate.find('.paginate_button.next a').html('<span class="fa fa-angle-right" aria-hidden="true"></span>');
                                    }
                                    if (dtElePaginate.find('.paginate_button.last a').length) {
                                        dtElePaginate.find('.paginate_button.last a').html('<span class="fa fa-angle-double-right" aria-hidden="true"></span>');
                                    }
                                }
                            });

                            dataTable.on('length.dt', function (e, settings, len) {
                                if (dtConfig.pageLength !== len || dtPageLength !== len) {
                                    if (typeof accUser.options !== 'undefined') {
                                        accUser.options.set('dt_pageLength', len);
                                    }
                                }
                            });

                            dataTable.on( 'order.dt', function () {
                                var order = dataTable.order();

                                var newUoOrderData_obj = uo_currOrderData_obj;

                                accUser.options.setData();
                                var oldUoOrderData_obj = accUser.options.get('dt_orderData');
                                var uo_oldOrderData = (typeof oldUoOrderData_obj[dtEle_uid] !== 'undefined') ? oldUoOrderData_obj[dtEle_uid] : [];
                                if(uo_oldOrderData.length) {
                                    var uo_oldOrderData_array = [];
                                    uo_oldOrderData_array.push(uo_oldOrderData);
                                    uo_oldOrderData = uo_oldOrderData_array;
                                }

                                if(core.helper.isArray(order)) {
                                    $.each(order, function (i,oData) {
                                        var oDataArray = [];
                                        var orderColNr = oData[0];
                                        var orderColSort = oData[1];

                                        oDataArray.push(orderColNr);
                                        oDataArray.push(orderColSort);

                                        newUoOrderData_obj[dtEle_uid] = oDataArray;
                                    });

                                    if(
                                        typeof newUoOrderData_obj[dtEle_uid] !== 'undefined' &&
                                        typeof accUser.options !== 'undefined'
                                    ) {

                                        var uo_newOrderData = newUoOrderData_obj[dtEle_uid];
                                        if(uo_newOrderData.length) {
                                            var uo_newOrderData_array = [];
                                            uo_newOrderData_array.push(uo_newOrderData);
                                            uo_newOrderData = uo_newOrderData_array;
                                        }
                                        if (!core.helper.arraysEqual(uo_newOrderData[0], uo_oldOrderData[0])) {
                                            accUser.options.set('dt_orderData', newUoOrderData_obj);
                                        }
                                    }
                                }
                            } );
                        }
                    }
                });
            }
        },

        reload: function() {
            if($('.table[data-datatable]').length) {
                $('.table[data-datatable]').each(function (i, ele) {
                    var dtEle = $(this);
                    var dataTable = dtEle.DataTable();

                    if (!dtEle.closest('.modal-body').has(dtEle).length) {
                        var dtEleProcessing = dtEle.closest('.dataTables_wrapper').find('.dataTables_processing');
                        if (dtEleProcessing.css('display') !== 'block') {
                            $("[data-toggle='tooltip']").tooltip("hide");
                            $("[data-toggle='popover']").tooltip("hide");

                            dataTable.clear().draw();
                            dataTable.ajax.reload();
                        }
                    }
                });
            }
        }
    },

    datePicker: {
        init: function() {
            if($('input.datepicker,.input-group.datepicker').length) {
                var datepicker_elements = $('input.datepicker,.input-group.datepicker');

                var datePickerConfig = {};

                datePickerConfig.disableTouchKeyboard = true;
                datePickerConfig.todayHighlight = true;
                datePickerConfig.clearBtn = true;
                datePickerConfig.autoclose = true;

                if(thisCurrLang !== '' && thisCurrLang === 'de') {
                    datePickerConfig.format = 'dd.mm.yyyy';
                    datePickerConfig.language = 'de';
                    $.fn.datepicker.dates['de'].clear = 'Datum entfernen';
                }

                datepicker_elements.each(function(i,ele) {

                    var dpEle = $(this);
                    var start = (typeof dpEle.attr('data-start') !== 'undefined') ? dpEle.attr('data-start') : '';
                    var end = (typeof dpEle.attr('data-end') !== 'undefined') ? dpEle.attr('data-end') : '';

                    var startdate = '';
                    if(start !== '') {
                        var currentDate = new Date();

                        var day   = currentDate.getDate();
                        var month = currentDate.getMonth() + 1;
                        var year  = currentDate.getFullYear();

                        if(start === 'today') {
                            startdate = month + '/' + day + '/' + year;
                            if(thisCurrLang !== '' && thisCurrLang === 'de') {
                                startdate = day + '.' + month + '.' + year;
                            }
                        }
                        if(start === 'tomorrow') {
                            currentDate.setDate(currentDate.getDate() + 1);

                            day   = currentDate.getDate();
                            month = currentDate.getMonth() + 1;
                            year  = currentDate.getFullYear();

                            startdate = month + '/' + day + '/' + year;
                            if(thisCurrLang !== '' && thisCurrLang === 'de') {
                                startdate = day + '.' + month + '.' + year;
                            }
                        }
                    }

                    var enddate = (end !== '') ? end : '';

                    if(startdate !== '') {
                        datePickerConfig.startDate = startdate;
                    }
                    if(enddate !== '') {
                        datePickerConfig.endDate = enddate;
                    }

                    dpEle.datepicker(datePickerConfig);
                });
            }
        }
    },

    helper: {
        trim:          function(s, c) {
            if (c === "]") c = "\\]";
            if (c === "\\") c = "\\\\";
            return s.replace(new RegExp(
                "^[" + c + "]+|[" + c + "]+$", "g"
            ), "");
        },

        htmlEntities:  function(str) {
            return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        },

        sprintf:       function() {
            var counter = 1;
            var args    = arguments;
            var string  = args[0];

            return string.replace(/%s/g, function() {
                return args[counter++];
            });
        },

        isNumeric:     function(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        },

        isArray:       function(v) {
            return (Array.isArray) ? Array.isArray(v) : v instanceof Array;
        },

        isObject:      function(v) {
            return (v !== null && !core.helper.isArray(v) && typeof v === 'object');
        },

        md5:           function (string) {

            function RotateLeft(lValue, iShiftBits) {
                return (lValue<<iShiftBits) | (lValue>>>(32-iShiftBits));
            }

            function AddUnsigned(lX,lY) {
                var lX4,lY4,lX8,lY8,lResult;
                lX8 = (lX & 0x80000000);
                lY8 = (lY & 0x80000000);
                lX4 = (lX & 0x40000000);
                lY4 = (lY & 0x40000000);
                lResult = (lX & 0x3FFFFFFF)+(lY & 0x3FFFFFFF);
                if (lX4 & lY4) {
                    return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
                }
                if (lX4 | lY4) {
                    if (lResult & 0x40000000) {
                        return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
                    } else {
                        return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
                    }
                } else {
                    return (lResult ^ lX8 ^ lY8);
                }
            }

            function F(x,y,z) { return (x & y) | ((~x) & z); }
            function G(x,y,z) { return (x & z) | (y & (~z)); }
            function H(x,y,z) { return (x ^ y ^ z); }
            function I(x,y,z) { return (y ^ (x | (~z))); }

            function FF(a,b,c,d,x,s,ac) {
                a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
                return AddUnsigned(RotateLeft(a, s), b);
            };

            function GG(a,b,c,d,x,s,ac) {
                a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
                return AddUnsigned(RotateLeft(a, s), b);
            };

            function HH(a,b,c,d,x,s,ac) {
                a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
                return AddUnsigned(RotateLeft(a, s), b);
            };

            function II(a,b,c,d,x,s,ac) {
                a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
                return AddUnsigned(RotateLeft(a, s), b);
            };

            function ConvertToWordArray(string) {
                var lWordCount;
                var lMessageLength = string.length;
                var lNumberOfWords_temp1=lMessageLength + 8;
                var lNumberOfWords_temp2=(lNumberOfWords_temp1-(lNumberOfWords_temp1 % 64))/64;
                var lNumberOfWords = (lNumberOfWords_temp2+1)*16;
                var lWordArray=Array(lNumberOfWords-1);
                var lBytePosition = 0;
                var lByteCount = 0;
                while ( lByteCount < lMessageLength ) {
                    lWordCount = (lByteCount-(lByteCount % 4))/4;
                    lBytePosition = (lByteCount % 4)*8;
                    lWordArray[lWordCount] = (lWordArray[lWordCount] | (string.charCodeAt(lByteCount)<<lBytePosition));
                    lByteCount++;
                }
                lWordCount = (lByteCount-(lByteCount % 4))/4;
                lBytePosition = (lByteCount % 4)*8;
                lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80<<lBytePosition);
                lWordArray[lNumberOfWords-2] = lMessageLength<<3;
                lWordArray[lNumberOfWords-1] = lMessageLength>>>29;
                return lWordArray;
            };

            function WordToHex(lValue) {
                var WordToHexValue="",WordToHexValue_temp="",lByte,lCount;
                for (lCount = 0;lCount<=3;lCount++) {
                    lByte = (lValue>>>(lCount*8)) & 255;
                    WordToHexValue_temp = "0" + lByte.toString(16);
                    WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length-2,2);
                }
                return WordToHexValue;
            };

            function Utf8Encode(string) {
                string = string.replace(/\r\n/g,"\n");
                var utftext = "";

                for (var n = 0; n < string.length; n++) {

                    var c = string.charCodeAt(n);

                    if (c < 128) {
                        utftext += String.fromCharCode(c);
                    }
                    else if((c > 127) && (c < 2048)) {
                        utftext += String.fromCharCode((c >> 6) | 192);
                        utftext += String.fromCharCode((c & 63) | 128);
                    }
                    else {
                        utftext += String.fromCharCode((c >> 12) | 224);
                        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                        utftext += String.fromCharCode((c & 63) | 128);
                    }

                }

                return utftext;
            };

            var x=Array();
            var k,AA,BB,CC,DD,a,b,c,d;
            var S11=7, S12=12, S13=17, S14=22;
            var S21=5, S22=9 , S23=14, S24=20;
            var S31=4, S32=11, S33=16, S34=23;
            var S41=6, S42=10, S43=15, S44=21;

            string = Utf8Encode(string);

            x = ConvertToWordArray(string);

            a = 0x67452301; b = 0xEFCDAB89; c = 0x98BADCFE; d = 0x10325476;

            for (k=0;k<x.length;k+=16) {
                AA=a; BB=b; CC=c; DD=d;
                a=FF(a,b,c,d,x[k+0], S11,0xD76AA478);
                d=FF(d,a,b,c,x[k+1], S12,0xE8C7B756);
                c=FF(c,d,a,b,x[k+2], S13,0x242070DB);
                b=FF(b,c,d,a,x[k+3], S14,0xC1BDCEEE);
                a=FF(a,b,c,d,x[k+4], S11,0xF57C0FAF);
                d=FF(d,a,b,c,x[k+5], S12,0x4787C62A);
                c=FF(c,d,a,b,x[k+6], S13,0xA8304613);
                b=FF(b,c,d,a,x[k+7], S14,0xFD469501);
                a=FF(a,b,c,d,x[k+8], S11,0x698098D8);
                d=FF(d,a,b,c,x[k+9], S12,0x8B44F7AF);
                c=FF(c,d,a,b,x[k+10],S13,0xFFFF5BB1);
                b=FF(b,c,d,a,x[k+11],S14,0x895CD7BE);
                a=FF(a,b,c,d,x[k+12],S11,0x6B901122);
                d=FF(d,a,b,c,x[k+13],S12,0xFD987193);
                c=FF(c,d,a,b,x[k+14],S13,0xA679438E);
                b=FF(b,c,d,a,x[k+15],S14,0x49B40821);
                a=GG(a,b,c,d,x[k+1], S21,0xF61E2562);
                d=GG(d,a,b,c,x[k+6], S22,0xC040B340);
                c=GG(c,d,a,b,x[k+11],S23,0x265E5A51);
                b=GG(b,c,d,a,x[k+0], S24,0xE9B6C7AA);
                a=GG(a,b,c,d,x[k+5], S21,0xD62F105D);
                d=GG(d,a,b,c,x[k+10],S22,0x2441453);
                c=GG(c,d,a,b,x[k+15],S23,0xD8A1E681);
                b=GG(b,c,d,a,x[k+4], S24,0xE7D3FBC8);
                a=GG(a,b,c,d,x[k+9], S21,0x21E1CDE6);
                d=GG(d,a,b,c,x[k+14],S22,0xC33707D6);
                c=GG(c,d,a,b,x[k+3], S23,0xF4D50D87);
                b=GG(b,c,d,a,x[k+8], S24,0x455A14ED);
                a=GG(a,b,c,d,x[k+13],S21,0xA9E3E905);
                d=GG(d,a,b,c,x[k+2], S22,0xFCEFA3F8);
                c=GG(c,d,a,b,x[k+7], S23,0x676F02D9);
                b=GG(b,c,d,a,x[k+12],S24,0x8D2A4C8A);
                a=HH(a,b,c,d,x[k+5], S31,0xFFFA3942);
                d=HH(d,a,b,c,x[k+8], S32,0x8771F681);
                c=HH(c,d,a,b,x[k+11],S33,0x6D9D6122);
                b=HH(b,c,d,a,x[k+14],S34,0xFDE5380C);
                a=HH(a,b,c,d,x[k+1], S31,0xA4BEEA44);
                d=HH(d,a,b,c,x[k+4], S32,0x4BDECFA9);
                c=HH(c,d,a,b,x[k+7], S33,0xF6BB4B60);
                b=HH(b,c,d,a,x[k+10],S34,0xBEBFBC70);
                a=HH(a,b,c,d,x[k+13],S31,0x289B7EC6);
                d=HH(d,a,b,c,x[k+0], S32,0xEAA127FA);
                c=HH(c,d,a,b,x[k+3], S33,0xD4EF3085);
                b=HH(b,c,d,a,x[k+6], S34,0x4881D05);
                a=HH(a,b,c,d,x[k+9], S31,0xD9D4D039);
                d=HH(d,a,b,c,x[k+12],S32,0xE6DB99E5);
                c=HH(c,d,a,b,x[k+15],S33,0x1FA27CF8);
                b=HH(b,c,d,a,x[k+2], S34,0xC4AC5665);
                a=II(a,b,c,d,x[k+0], S41,0xF4292244);
                d=II(d,a,b,c,x[k+7], S42,0x432AFF97);
                c=II(c,d,a,b,x[k+14],S43,0xAB9423A7);
                b=II(b,c,d,a,x[k+5], S44,0xFC93A039);
                a=II(a,b,c,d,x[k+12],S41,0x655B59C3);
                d=II(d,a,b,c,x[k+3], S42,0x8F0CCC92);
                c=II(c,d,a,b,x[k+10],S43,0xFFEFF47D);
                b=II(b,c,d,a,x[k+1], S44,0x85845DD1);
                a=II(a,b,c,d,x[k+8], S41,0x6FA87E4F);
                d=II(d,a,b,c,x[k+15],S42,0xFE2CE6E0);
                c=II(c,d,a,b,x[k+6], S43,0xA3014314);
                b=II(b,c,d,a,x[k+13],S44,0x4E0811A1);
                a=II(a,b,c,d,x[k+4], S41,0xF7537E82);
                d=II(d,a,b,c,x[k+11],S42,0xBD3AF235);
                c=II(c,d,a,b,x[k+2], S43,0x2AD7D2BB);
                b=II(b,c,d,a,x[k+9], S44,0xEB86D391);
                a=AddUnsigned(a,AA);
                b=AddUnsigned(b,BB);
                c=AddUnsigned(c,CC);
                d=AddUnsigned(d,DD);
            }

            var temp = WordToHex(a)+WordToHex(b)+WordToHex(c)+WordToHex(d);

            return temp.toLowerCase();
        },

        arraysEqual:   function (arr1, arr2) {
            arr1 = (typeof arr1 !== 'undefined' ? arr1 : []);
            arr2 = (typeof arr2 !== 'undefined' ? arr2 : []);

            if(arr1.length !== arr2.length)
                return false;
            for(var i = arr1.length; i--;) {
                if(arr1[i] !== arr2[i])
                    return false;
            }

            return true;
        },

        checkForHyphens: function(selector) {
            selector = typeof selector    !== 'undefined' ? selector    : '';
            var hasHyphens = false;
            if($(selector).length) {
                ele = $(selector);
                eleHtml = ele.html();

                hasHyphens = (eleHtml !== eleHtml.replace(/\u00AD/g, '&shy;'));
            }
            return hasHyphens;
        },

        checkEmail:    function(email) {
            email = typeof email !== 'undefined' ? email : '';
            var result = true,
                regX;

            if (email == '') {
                result = false
            }
            else if (typeof(RegExp) == 'function') {
                regX = new RegExp('^([a-zA-Z0-9\\-\\.\\_]+)(\\@)([a-zA-Z0-9\\-\\.]+)(\\.)([a-zA-Z]{2,4})$');
                if (!regX.test(email)) {
                    result = false
                }
            }
            return result;
        },

        checkEmailInput: function() {
            var returning = true;
            if($('.form-group .multiple-email-input').length) {
                $('.form-group .multiple-email-input').each( function(i,ele) {

                    var checkEle          = $(ele);
                    var checkEleId        = checkEle.attr('id');
                    var checkEleFormGroup = checkEle.closest('.form-group');
                    var checkEleLabel     = checkEleFormGroup.find('label');

                    var email      = checkEle.val();
                    var emails     = [];

                    $('.alert.email-error.' + checkEleId).remove();
                    checkEleFormGroup.removeClass('has-error');

                    if(email !== '') {

                        var errorTxt = core.helper.sprintf(core.i18n.translate('Bitte eine gültige E-Mail Adresse bei \'%s\' eingeben!'), core.helper.htmlEntities(checkEleLabel.text()));
                        var errorTxtEmailList = '';

                        if (email.indexOf(',') >= 0) {
                            emails = checkEle.val().split(',');
                        }

                        if (emails.length) {
                            errorTxt = core.helper.sprintf(core.i18n.translate('Bitte folgende E-Mail Adresse(n) bei \'%s\' prüfen'), core.helper.htmlEntities(checkEleLabel.text())) + ':<ul>';
                            emails.forEach(function (email) {
                                if (!core.helper.checkEmail(email.trim())) {
                                    errorTxtEmailList += '<li>' + core.helper.htmlEntities(email.trim()) + '</li>';
                                }
                            });

                            if (errorTxtEmailList !== '') {
                                core.bs_alert.danger(errorTxt + errorTxtEmailList + '</ul>', 'email-error ' + checkEleId);
                                checkEleFormGroup.addClass('has-error')
                                returning = false;
                            }
                        } else {
                            if (!core.helper.checkEmail(core.helper.htmlEntities(email))) {
                                core.bs_alert.danger(errorTxt, 'email-error ' + checkEleId);
                                checkEleFormGroup.addClass('has-error')
                                returning = false;
                            }
                        }
                    }
                });
            }
            return returning;
        },

        checkPasswordValid: function() {
            var returning = true;
            if($('[type="password"].validatePassword').length) {
                $('[type="password"].validatePassword').each( function (i,ele) {
                    var passEle         = $(ele);
                    var passValue       = passEle.val();
                    var regExPattern    = /^(?=.{8,})(?=.*[a-z]{1,})(?=.*[0-9]{1,}).+/i;
                    var passValueRegEx  = passValue.match(regExPattern);

                    if(!passValueRegEx) {
                        passEle.closest('.form-group').addClass('has-error');
                        if(!$('.alert-danger.dynamicadded').length) {
                            core.bs_alert.danger('Passwörter müssen mindestens eine länge von 8 Zeichen haben sowie Buchstaben und Zahlen beinhalten!');
                        }
                        returning = false;
                    } else {
                        passEle.closest('.form-group').removeClass('has-error');
                        if($('.alert-danger.dynamicadded').length) {
                            $('.alert-danger.dynamicadded').remove();
                        }
                    }
                });
            }
            return returning;
        },

        checkRepeatedPassword: function() {
            var returning = true;
            if($('#pass').length && $('#repeat_pass').length) {

                var passEle         = $('#pass');
                var passValue       = passEle.val();
                var repeatPassValue = $('#repeat_pass').val();

                if(passValue !== repeatPassValue) {
                    passEle.closest('.form-group').addClass('has-error');
                    if(!$('.alert-danger.dynamicadded').length) {
                        core.bs_alert.danger('Passwörter sind nicht identisch!');
                    }
                    returning = false;
                } else {
                    passEle.closest('.form-group').removeClass('has-error');
                    if($('.alert-danger.dynamicadded').length) {
                        $('.alert-danger.dynamicadded').remove();
                    }
                }
            }
            return returning;
        }
    },

    i18n: {
        translations:    null,
        languages:       null,

        init:            function() {
            core.i18n.getLanguages();
            core.i18n.getTranslations();
        },

        getAjaxUrl:      function() {
            var AjaxUrl = '';
            if(thisBaseUrl != '') {
                var baseUrl = thisBaseUrl + 'ajax/' + langUrlSlug;
                var i18nUrlPath = 'system/i18n';

                AjaxUrl = baseUrl + i18nUrlPath;
            }
            return AjaxUrl;
        },

        getLanguages:    function() {
            if(core.i18n.languages === null) {
                var AjaxUrl = core.i18n.getAjaxUrl();
                var i18nUrlGetParam = 'getlanguages';
                var getUrl = AjaxUrl + '/' + i18nUrlGetParam;

                $.get(getUrl, function( data ) {
                    if (data.success) {
                        var result = data.result;
                        if (result.success) {
                            var getLanguages = result.getlanguages;
                            core.i18n.languages = getLanguages;
                        }
                    }
                });
            } else {
                return core.i18n.languages;
            }
        },

        getCurrLang:     function() {
            var currLang = {};
            if(core.i18n.languages !== null && thisCurrLang !== '') {
                currLang = core.i18n.languages[thisCurrLang];
            }
            return currLang;
        },

        getTranslations: function() {
            if(core.i18n.translations === null) {
                var AjaxUrl = core.i18n.getAjaxUrl();
                var i18nUrlGetParam = 'gettranslations';
                var getUrl = AjaxUrl + '/' + i18nUrlGetParam;
                $.get(getUrl, function( data ) {
                    if (data.success) {
                        var result = data.result;
                        if (result.success) {
                            var getTranslations = result.gettranslations;
                            core.i18n.translations = getTranslations;
                        }
                    }
                });
            } else {
                return core.i18n.translations;
            }
        },

        translate:       function(string, langCode) {
            string = typeof string !== 'undefined' ? string : '';
            langCode = typeof langCode !== 'undefined' ? langCode : thisCurrLang;

            var getLanguages    = core.i18n.getLanguages();
            var getTranslations = core.i18n.getTranslations();

            if(
                typeof getLanguages !== 'undefined' &&
                typeof getTranslations !== 'undefined'
            ) {
                var translatedString = string;
                var currLang = core.i18n.getCurrLang();
                var currLangLocale = currLang.locale;

                var useLangLocale = currLangLocale;

                if(langCode !== '') {
                    useLangLocale = getLanguages[langCode].locale;
                }

                if(useLangLocale !== '') {
                    var localeTranslations = getTranslations[useLangLocale];

                    $.each(localeTranslations, function(i,ele) {
                        if(typeof ele[string] !== 'undefined') {
                            translatedString = ele[string];
                        }
                    });

                    return translatedString;
                }

            } else {
                return string;
            }
        }
    },

    Request: {
        getCurrUrl: function(withParams) {

            withParams = typeof withParams !== 'undefined' ? withParams : true;

            var curUrl = window.location.href;

            if(!withParams) {
                var urlWithParams    = curUrl;
                var urlWithOutParams = urlWithParams;
                var index            = urlWithParams.indexOf('?');

                if(index == -1){
                    index = urlWithParams.indexOf('#');
                }
                
                if(index != -1){
                    urlWithOutParams = urlWithParams.substring(0, index);
                }

                curUrl = urlWithOutParams;
            }
            return curUrl;
        }
    }
};

$( document ).ajaxComplete(function( event, jqxhr, settings ) {
    if ( settings.url.indexOf('ajax') !== -1 ) {

        /** Check if Ajax Response is Valid */
        var checkAjaxCall =  core.checkAjaxCall(jqxhr);

        if(!checkAjaxCall) {
            jqxhr.abort();
        } else {
            if (settings.url.toLowerCase().indexOf('dtsource') !== -1) {
                core.init();
            } else {
                core.ays_autoload();
                core.datePicker.init();
                core.modalActions();
            }
        }
    }
});

$( document ).ready(function() {
    core.init();
});