/**
 * A jquery plugin to add Bootstrap 3 Alerts on the fly
 * Author: Jan Doll (https://www.xing.com/profile/Jan_Doll2)
 * Basic Script & Idea: http://jsfiddle.net/periklis/7ATLS/1/
 * For documentation see: https://components.jfra.me/jBsAlerts/
 * Licensed under the MIT license
 * Version: 1.2.1
 */

if(typeof jQuery === 'undefined') {
    console.warn('jBsAlerts Warning:', 'Make sure jQuery is included before jquery.jBsAlerts.js');
}

(function ( $ ) {

    'use strict';

    String.prototype.replaceAll = function(search, replacement) {
        var target = this;
        return target.replace(new RegExp(search, 'g'), replacement);
    };

    var settings;

    $.fn.jBsAlerts = function ( options ) {
        settings = $.extend({
            type: 'info',        // 'info', 'success', 'warning' or 'danger'
            message: '',         // String
            position: 'prepend', // 'prepend' or 'append'
            addClass: '',        // String
            autoClose: false,    // true or false, with Typ 'success' this is always 'true'
            acDelay: 2000,       // Integer
            acSpeed: 'fast',     // 'fast', 'slow' or Integer , see http://api.jquery.com/slideUp/
            toast: false         // true or false, adding Toast functionality, position bottom right.
        }, options);

        var thisAlertObj = $(this);

        if(
            settings.type === 'info' ||
            settings.type === 'success' ||
            settings.type === 'warning' ||
            settings.type === 'danger'
        ) {
            if(settings.type === 'success') {
                settings.autoClose = true;
            }

            settings.addClass = settings.addClass.trim();

            var alertClass = 'jBsAlerts--dynamicAdded ' + settings.addClass;

            alertClass = alertClass.replaceAll('undefined', '').trim();

            if(settings.autoClose) { alertClass += ' jBsAlerts--autoClose'; }

            var addTo = thisAlertObj;
            var toastAlertStylesAttr = '';

            if(settings.toast) {
                var toastContainer = $('<div class="jBsAlerts--toasts-wrapper" style="position: fixed; bottom: 15px; right: 15px; min-width: 250px;" />');
                var toastWrapperEle = $('.jBsAlerts--toasts-wrapper');
                var addToToast = toastWrapperEle;
                if(!toastWrapperEle.length) {
                    $( 'body' ).append( toastContainer );
                    addToToast = toastContainer;
                }
                toastAlertStylesAttr = ' style="-webkit-box-shadow: 0 0 10px 0 rgba(0,0,0,0.7); box-shadow: 0 0 10px 0 rgba(0,0,0,0.7);"';
                addTo = addToToast;
            }

            var alertHtml = '<div class="alert alert-' + settings.type + ' ' + alertClass + '"' + toastAlertStylesAttr + '><a class="close" data-dismiss="alert">&times;</a><span>'+settings.message+'</span></div>';
            if(settings.position === 'prepend') {
                addTo.prepend(alertHtml);
            } else {
                addTo.append(alertHtml);
            }

            if(settings.autoClose) {
                $('.alert.jBsAlerts--dynamicAdded.jBsAlerts--autoClose').each( function() {
                    $(this).show().delay(settings.acDelay).slideUp(settings.acSpeed, function(){
                        $(this).remove();
                    });
                });
            }
        } else {
            console.error('jBsAlerts Error:','Wrong bsaType! Use \'info\', \'success\', \'warning\' or \'danger\' instead.');
        }
    }
} (jQuery));