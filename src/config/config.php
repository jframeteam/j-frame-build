<?php
/**
 * Main Config File
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */

/**
 * Include Cache Config File
 */
$cacheConfFilePath = JF_ROOT_DIR . DS . 'config' . DS . 'config.cache.php';
require_once($cacheConfFilePath);

/**
 * Include Database Config File
 */
$dbConfFilePath       = JF_ROOT_DIR . DS . 'config' . DS . 'config.db.php';
$dbConfFilePath_local = JF_ROOT_DIR . DS . 'config' . DS . 'config.db.local.php';
$dbConfFile = (file_exists($dbConfFilePath_local)) ? $dbConfFilePath_local : $dbConfFilePath;

/** Check for DB Configuration File */
if(!file_exists($dbConfFile)) {
    die('<pre>J&bull;Frame Error: DB Config File not found!</pre>');
}

require_once($dbConfFile);