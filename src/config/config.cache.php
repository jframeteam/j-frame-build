<?php
/**
 * Cache Config File
 *
 *
 * @system J•Frame
 * @author Jan Doll <jan_doll@gmx.de>
 * @copyright since 2008 by Jan Doll
 * All Rights Reserved
 */

/**
 * NOTICE OF LICENSE
 *
 * Unauthorized copying, sharing, adaptation, publishing, commercial usage, and/or distribution of the Software,
 * its derivatives and/or successors, via any medium, is strictly prohibited.
 *
 * The Software is deemed proprietary and confidential.
 *
 * Any intellectual property, patents and/or trademarks used in the Software are retained by their respective authors.
 */


if(php_sapi_name() !== 'cli') {
    // Cache-Lifetime (in Minutes)
    $cLifetime = 15;
    $exp_gmt = gmdate('D, d M Y H:i:s', time() + $cLifetime * 60) . ' GMT';
    $mod_gmt = gmdate('D, d M Y H:i:s', getlastmod()) . ' GMT';

    header('Expires: ' . $exp_gmt);
    header('Last-Modified: ' . $mod_gmt);
    header('Cache-Control: private, max-age=' . $cLifetime * 60);
    // MSIE 5 separate Cache Control
    header('Cache-Control: pre-check=' . $cLifetime * 60, FALSE);
}